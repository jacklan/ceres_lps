<?php

ini_set('display_errors', 1);
ini_set('display_startup_erros', 1);
error_reporting(E_ALL);

class LoginForm extends TPage {

    protected $form;

    function __construct() {
        parent::__construct();

        $table = new TTable;
        $table->width = '100%';

        $this->form = new TForm('form_User');

        $this->form->class = 'login-page-container';

        $this->form->add($table);

        $login = new TEntry('login');
        $login->class = "form-control";

        $senha = new TPassword('senha');
        $senha->class = "form-control";

        $login->setSize(60);
        $senha->setSize(60);

        $login->style = 'height: 35px;font-size: 14px;float: left; border-radius: 0px 5px 0px 0px;';
        $senha->style = 'height: 35px;font-size: 14px;float: left;margin-bottom: 15px;border-radius: 0px 0px 5px 0px;';

        $login->placeholder = 'Usuário';
        $senha->placeholder = 'Senha';

      //  $login->setMaxLength(12);

        $login->addValidation('Usuário', new TRequiredValidator);
        $senha->addValidation('Senha', new TRequiredValidator);

        function array_random($arr, $num = 1) {
            shuffle($arr);

            $r = array();
            for ($i = 0; $i < $num; $i++) {
                $r[] = $arr[$i];
            }
            return $num == 1 ? $r[0] : $r;
        }
        $bg_img = ["bom-jesus", "campo-grande2", "triunfo-potiguar", "serra-sao-bento", "coronel-ezequiel", "dix-sept", "pais", "campo14", "campo-grande", "xique-xique", "caraubas"];

        $user = '<span style="float:left;width:35px;margin-left:35px;height:35px; border-bottom-left-radius: 5px;border-top-left-radius: 5px" class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>';
        $locker = '<span style="float:left;width:35px;margin-left:35px;height:35px; border-bottom-left-radius: 5px;" class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>';

        $script = "<style type='text/css'>
                        body.login-page {
                            background:url('app/templates/theme2/images/bg/" . array_random($bg_img) . ".jpg') top left no-repeat transparent;
                            background-size:cover;
                        }
                        /*
                        .msg-pagina {
                            position: absolute;
                            top: 120px;
                            left: 0px;
                            width: 100%;
                            height: 80%;
                            z-index: 9999997;
                            /* transparência compatível com os navegadores comuns.*/
                            opacity:0.65;
                            -moz-opacity: 0.65;
                            filter: alpha(opacity=65);
                            background: black;
                            text-align: center;
                         } */
                   </style>";

        $title = "
                <div class='msg-pagina'></div>
                <div class='login-title text-center'> <p>&nbsp;</p>
                    <img src='app/images/logo-ceres-150.png' />
                 </div>";

        $user = '<span style="float:left;margin-left:35px;height:35px;border-radius: 5px 0px 0px 0px;border-right: 1px solid #cccccc !important;" class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>';
        $locker = '<span style="float:left;margin-left:35px;height:35px;border-radius: 0px 0px 0px 5px;" class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>';

       

        $login_button = new TButton('loginb');
        //$recovery_button = new TActionLink('Esqueci o login ou senha', new TAction(['RecoveryForm', 'onReload']), 'black', 8);
        //$recovery_button->style = 'margin-top: 8px; margin-left: 6px; text-decoration: underline; font-weight: bold;';

        $login_action = new TAction(['SenhaForm', 'onLogin']);
        $login_action->setParameter('doLogin', true);

        $login_button->setAction($login_action, 'Fazer Login');

        $login_button->class = 'btn btn-lg btn-success';
        $login_button->style = 'width:265px;font-size:18px';

        $container1 = new TElement('div');
        $container1->class = "boxed animated flipInY input-group";

        $container2 = new TElement('div');
        $container2->class = "inner";
        $container2->add($title);
        $container2->add($script);

        $container3 = new TElement('div');
        $container3->class = "input-group";
        $container3->add($user);
        $container3->add($login);
        $container3->add($locker);
        $container3->add($senha);
        $container3->add($login_button);
        //$container3->add($recovery_button);

        $container6 = new TElement('div');
        $footer = "<p class='footer'><img src='app/images/logo/logo_fundase2.png' style='width: 30%;' /><img src='app/images/logo/logo_rn_sape.png' style='width: 30%;' /></p>";

        $container2->add($container3);
        $container2->add($container6);

        $container1->add($container2);

        $row = $table->addRow();
        $row->addCell($container1)->colspan = 2;

        $this->form->setFields([$login, $senha, $login_button]);
        parent::add($this->form);
    }

    function onLogout() {
        TSession::freeSession();
        if (isset($_GET['static'])) TScript::create("__adianti_goto_page('index.php?class=LoginForm');");
    }

}
