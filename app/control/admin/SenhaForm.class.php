<?php

ini_set('display_errors', 1);
ini_set('display_startup_erros', 1);
error_reporting(E_ALL);

use Adianti\Database\TTransaction;
use Adianti\Database\TRepository;

class SenhaForm extends TPage {
    public function onLogin() {
        TTransaction::open('pg_ceres');

        $repository = new TRepository('vw_usuarioRecord');

        $login = preg_replace('/[^[:alnum:]_]/', '', $_REQUEST['login']);
        $senha = preg_replace('/[^[:alnum:]_]/', '', $_REQUEST['senha']);

        $criteria = new TCriteria;
        $criteria->add(new TFilter('login', '=', strtoupper($login)));
        $criteria->add(new TFilter('upper(senha)', '=', strtoupper($senha)));

        $nurows = $repository->count($criteria);
        if ($nurows == 1) {
            $usuarios = $repository->load($criteria);
            if ($usuarios) {
                foreach ($usuarios as $usuario) {
                    if (session_status() == PHP_SESSION_NONE) session_start();
                   // TSession::setValue('paginas', $usuario->getPaginas());
                    TSession::setValue('login', $login);
                    $_SESSION["usuario"] = $usuario->login;
                    $_SESSION["validacao"] = "1";
                    $_SESSION["servidor_id"] = $usuario->servidor_id;
                    $_SESSION["municipio_id"] = $usuario->municipio_id;
                    $_SESSION["setor_id"] = $usuario->setor_id;

                    $mun = new MunicipioRecord($usuario->municipio_id);
                    $_SESSION["cptec_id"] = $mun->cptec_id;
                    $_SESSION["nomemunicipio"] = $mun->cptec_id;

                    $objUsuario = new UsuarioRecord($usuario->usuario_id);
                    $_SESSION["crn_funcionario_id"] = $objUsuario->crn_funcionario_id;

                    $_SESSION["unidadeoperativa_id"] = $usuario->unidadeoperativa_id;
                    $_SESSION["nome"] = $usuario->login;
                    $_SESSION["tipousuario"] = $usuario->tipo;
                    $_SESSION["matricula"] = $usuario->tipo;
                    $_SESSION["usuario_id"] = $usuario->usuario_id;
                    $_SESSION["laticinio_id"] = $usuario->laticinio_id;
                    $_SESSION["nometa"] = $usuario->nome;
                    $_SESSION["cpfta"] = $usuario->cpf;
                    $_SESSION["matriculata"] = $usuario->matricula;
                    $_SESSION["regional_id"] = $usuario->regional_id;
                    $_SESSION['modulo'] = 'EMATER';
                    $_SESSION["empresa_id"] = $usuario->empresa_id;
                    $_SESSION["abrangencia"] = $usuario->abrangencia;

                    TSession::setValue('logged', true);
                    TSession::setValue('pos_login', true);
                    TSession::setValue('frontpage', 'Inicio');
                    TScript::create("__adianti_goto_page('index.php');");
                }
            } else {
                new TMessage('error', '<b style="color: red;">Usuário ou senha inválidos.</b><br><br>Tente novamente! Se o erro persistir entre em contato com a <b>equipe</b> de <b>Tecnologia da Informa&ccedil;&atilde;o(TI)</b> pelo telefone: (84) 3232-1162 ou pelo e-mail tisapern@gmail.com');
                TSession::freeSession();
            }
        } else {
            new TMessage('error', '<b style="color: red;">Usuário ou senha inválidos.</b><br><br>Tente novamente! Se o erro persistir entre em contato com a <b>equipe</b> de <b>Tecnologia da Informa&ccedil;&atilde;o(TI)</b> pelo telefone: (84) 3232-1162 ou pelo e-mail tisapern@gmail.com', new TAction(['LoginForm', 'onLogout']), 'Pedido não processado com êxito');
            TSession::freeSession();
        }

        TTransaction::close();
    }
}