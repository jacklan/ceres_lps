<?php

/*
 * classe ServidorporHabilidadesGraf
 * Grafico de Cargos por Servidor: Contem o Grafico atraves da view vw_servidores_por_cargos
 */

//ini_set('display_errors', 1);
//ini_set('display_startup_erros', 1);
//error_reporting(E_ALL);

include_once 'app/lib/FusionCharts/FusionCharts.php';

class ServidorporHabilidadesGraf extends TPage {
    /*
     * metodo construtor
     * Cria a pagina e o Grafico
     */

    public function __construct() {
        parent::__construct();
    }

    /*
     * metodo onReload()
     * Carrega a DataGrid com os objetos do banco de dados
     */

    function onReload() {
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instancia um repositorio para Carros
        $repository = new TRepository('Vw_servidores_por_habilidadesRecord');

        // cria um criterio de selecao, ordenado pelo id
        $criteria = new TCriteria;
        $criteria->add(new TFilter('empresa_id', '=', $_SESSION['empresa_id']));
        $criteria->setProperty('order', 'habilidade');
        // carrega os objetos de acordo com o criterio
        $cadastros = $repository->load($criteria);

        echo "<script LANGUAGE='Javascript' SRC='app/lib/FusionCharts/FusionCharts.js'></script>";

        $strXML = "<chart id='chart1' caption='Grafico de Servidores por Competências' subCaption='Servidores Ativos' pieSliceDepth='30' showBorder='1' formatNumberScale='0' numberSuffix='' animation='1' bgColor='99CCFF,FFFFFF' >";

        if ($cadastros) {
            // percorre os objetos retornados
            foreach ($cadastros as $reg) {
                //antes de armazenar verifica se algum campo eh requerido e nao foi informado
                $dados = $reg->toArray();

                $strXML .= "<set label='" . $dados['habilidade'] . "' value='" . $dados['quantidade'] . "' />";
                //link='n-app.pdf/RelatorioCargoPDF.php?cargo=".$dados['cargo']."%26regional=".$regiao."'
            }
        }
        //Finally, close <chart> element
        $strXML .= "</chart>";

        echo $strXML;

        //Create the chart - Column3D Chart with data from $strXML
        echo "<div class='col-xs-12 col-sm-12 col-md-12 col-lg-12' style='margin-top:50px'>";
        echo renderChart("app/lib/FusionCharts/Column3D.swf", "", $strXML, "Chart1", "100%", 500, false, false);
        echo "</div>";

        // finaliza a transacao
        TTransaction::close();
        $this->loaded = true;
    }

    /*
     * metodo show()
     * Exibe a pagina
     */

    function show() {
        // se a listagem ainda nao foi carregada
        if (!$this->loaded) {
            $this->onReload();
        }
        parent::show();
    }

}

?>