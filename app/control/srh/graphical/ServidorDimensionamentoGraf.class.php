<?php

/*
 * classe ServidorDimensionamentoGraf
 * Grafico de Servidor: Contem o Grafico atraves da view vw_servidores_por_cargos
 */

include_once 'app/lib/FusionCharts/FusionCharts.php';

class ServidorDimensionamentoGraf extends TPage {
    /*
     * metodo construtor
     * Cria a pagina e o Grafico
     */

    public function __construct() {
        parent::__construct();
    }

    /*
     * metodo onReload()
     * Carrega a DataGrid com os objetos do banco de dados
     */

    function onReload() {
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        $regiao = $_REQUEST['regiao'];

        // instancia um repositorio para Carros
        $repository = new TRepository('vw_dimensionamento_servidoresRecord');

        // cria um criterio de selecao, ordenado pelo id
        $criteria = new TCriteria;
        $criteria->add(new TFilter('empresa_id', '=', $_SESSION['empresa_id']));
        if ($regiao == '') {
            
        } else {
            if ($regiao == 'TODAS') {
                //filtra pelo campo selecionado pelo usu�rio / mes
                $criteria->setProperty('order', 'regional, unidadeoperativa');
            } else {
                //  $criteria->add(new TFilter('regiao', '=', $regiao));
                //adiciona o criterio
                $criteria->setProperty('order', 'regional, unidadeoperativa');
                $criteria->add(new TFilter('regional', '=', $regiao));
            }
        }
        $cadastros = $repository->load($criteria);

        echo "<script LANGUAGE='Javascript' SRC='app/lib/FusionCharts/FusionCharts.js'></script>";
        echo "<h3 style='text-align: center;'> Regional: <b style='color: red;'>$regiao</b> </h3>";

        //$strXML = "<chart id='chart' caption='Dimensionamento de Servidores por Município' subCaption='' xAxisName='Municipio' yAxisName='Qtd Agricultores' pieSliceDepth='30' showBorder='1' formatNumberScale='0' numberSuffix='' animation='1' bgColor='99CCFF,FFFFFF' >";
        $strXML = "<chart caption='Dimensionamento de Servidores' xAxisName='Municipio' yAxisName='Quantidade' showValues='1' exportEnabled='1' exportShowMenuItem='1' exportAtClient='1' exportAction='download' exportTargetWindow='_blank' exportFileName='dimensionamento' exportFormats='PDF=Exportar como PDF|PNG=Exportar como imagem PNG|JPG=Exportar como imagem JPG' exportHandler='fcExporter1'>";
        //$strXML = "<chart id='chart' >";

        if ($cadastros) {
            $strXML .="<categories>";
            // percorre os objetos retornados
            foreach ($cadastros as $reg) {
                //antes de armazenar verifica se algum campo eh requerido e nao foi informado
                $dados = $reg->toArray();

                $strXML .="<category label='" . $dados['unidadeoperativa'] . "' />";
            }
            $strXML .="</categories>";
            $strXML .="<dataset seriesName='Qtd. Servidores Atual'>";
            // percorre os objetos retornados
            foreach ($cadastros as $reg) {
                //antes de armazenar verifica se algum campo eh requerido e nao foi informado
                $dados = $reg->toArray();

                $strXML .= "<set value='" . $dados['qtdservidores'] . "' />";
            }
            $strXML .="</dataset>";
            $strXML .="<dataset seriesName='Qtd Servidores Necessária'>";
            // percorre os objetos retornados
            foreach ($cadastros as $reg) {
                //antes de armazenar verifica se algum campo eh requerido e nao foi informado
                $dados = $reg->toArray();

                $strXML .= "<set value='" . $dados['media'] . "'/>";
            }
            $strXML .="</dataset>";
        }


        //Finally, close <chart> element
        $strXML .= "</chart>";

        echo "<div class='col-xs-12 col-sm-12 col-md-12 col-lg-12' style='margin-top:50px'>";
        echo renderChart("app/lib/FusionCharts/MSBar3D.swf", "", $strXML, "chart", "100%", 2200, false, false);
        echo "</div>";

        // finaliza a transacao
        TTransaction::close();
        $this->loaded = true;
    }

    /*
     * metodo show()
     * Exibe a pagina
     */

//    function show() {
//        // se a listagem ainda nao foi carregada
//        if (!$this->loaded) {
//            $this->onReload();
//        }
//        parent::show();
//    }
}

?>