<?php

/*
 * classe ServidorporCargoGraf
 * Grafico de Cargos por Servidor: Contem o Grafico atraves da view vw_servidores_por_cargos
 */

include_once 'app/lib/FusionCharts/FusionCharts.php';

class ServidorporCargoGraf extends TPage {
    /*
     * metodo construtor
     * Cria a pagina e o Grafico
     */

    public function __construct() {
        parent::__construct();
    }

    /*
     * metodo onReload()
     * Carrega a DataGrid com os objetos do banco de dados
     */

    function onReload() {
        $regiao = $_REQUEST['regiao'];
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');


        // cria um criterio de selecao, ordenado pelo id
        $criteria = new TCriteria;

        if ($regiao <> 'TODAS') {
            // instancia um repositorio para Carros
            $repository = new TRepository('vw_servidores_por_cargos_por_regiaoRecord');
            //filtra pelo campo selecionado pelo usuário
            $criteria->add(new TFilter('regional', '=', $regiao));
            $criteria->setProperty('order', 'cargo');
        } else {
            // instancia um repositorio para Carros
            $repository = new TRepository('Vw_servidores_por_cargosRecord');
            $criteria->setProperty('order', 'cargo');
        }

        $criteria->add(new TFilter('empresa_id', '=', $_SESSION['empresa_id']));
        // carrega os objetos de acordo com o criterio
        $cadastros = $repository->load($criteria);

        echo "<script LANGUAGE='Javascript' SRC='app/lib/FusionCharts/FusionCharts.js'></script>";

        if ($regiao <> 'TODAS') {
            $strXML = "<chart id='chart' caption='Grafico de Servidores / Cargos' subCaption='" . $regiao . "' pieSliceDepth='30' showBorder='1' formatNumberScale='0' numberSuffix='' animation='1' bgColor='99CCFF,FFFFFF' >";
        } else {
            $strXML = "<chart id='chart' caption='Grafico de Servidores / Cargos' subCaption='Servidores Ativos' pieSliceDepth='30' showBorder='1' formatNumberScale='0' numberSuffix='' animation='1' bgColor='99CCFF,FFFFFF' >";
        }

        if ($cadastros) {
            // percorre os objetos retornados
            foreach ($cadastros as $reg) {
                //antes de armazenar verifica se algum campo eh requerido e nao foi informado
                $dados = $reg->toArray();

                $strXML .= "<set label='" . $dados['cargo'] . "' value='" . $dados['quantidade'] . "'  link='n-index.php?class=RelatorioCargoPDF%26cargo=" . $dados['cargo'] . "%26regional=" . $regiao . "' />";
            }
        }
        //Finally, close <chart> element
        $strXML .= "</chart>";

        echo $strXML;
        if ($_REQUEST['grafico'] == 'PIZZA 3D') {

            echo renderChart("app/lib/FusionCharts/Pie3D.swf", "", $strXML, "chart", "100%", 500, false, false);
        } else {

            echo renderChart("app/lib/FusionCharts/Column3D.swf", "", $strXML, "chart", "100%", 800, false, false);
        }



        // finaliza a transacao
        TTransaction::close();
        $this->loaded = true;
    }

    /*
     * metodo show()
     * Exibe a pagina
     */

    function show() {
        // carrega os dados no datagrid
        $this->onReload();
        //chama o metodo show da super classe
        parent::show();
    }

}

?>