<?php

/*
 * classe ServidorporSolicitacaoGraf
 * Grafico de Cargos por Servidor: Contem o Grafico atraves da view vw_servidores_por_cargos
 */

include_once 'app/lib/FusionCharts/FusionCharts.php';

class ServidorporRegionalGraf extends TPage {
    /*
     * metodo construtor
     * Cria a pagina e o Grafico
     */

    public function __construct() {
        parent::__construct();
    }

    /*
     * metodo onReload()
     * Carrega a DataGrid com os objetos do banco de dados
     */

    function onReload() {

        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instancia um repositorio para Carros
        $repository = new TRepository('vw_servidores_por_regiaoRecord');

        //parametros do form
        $regional_id = $_REQUEST['regional_id'];
        $nome_regional = (new RegionalRecord($regional_id))->nome;
        $grafico = $_REQUEST['grafico'];
        // cria um criterio de selecao, ordenado pelo id
        $criteria = new TCriteria;

        if ($regional_id == 0) {
            //filtra pelo campo selecionado pelo usuário / mes
            $criteria->setProperty('order', 'regiao');
        } else {
            $criteria->add(new TFilter('regiao_id', '=', $regional_id));
        }

        $criteria->add(new TFilter('empresa_id', '=', $_SESSION['empresa_id']));
        $criteria->add(new TFilter('sitatual', '=', 'ATIVOS'));

        $criteria->setProperty('order', 'municipio');
        // carrega os objetos de acordo com o criterio
        $cadastros = $repository->load($criteria);

        echo "<script LANGUAGE='Javascript' SRC='app/lib/FusionCharts/FusionCharts.js'></script>";
        
        echo "<h3 style=\'text-align: center;\'> Regional: <b style=\'color: red;\'>" . ($nome_regional != null ? $nome_regional : "TODAS") . "</b> </h3>";
        
        //$strXML will be used to store the entire XML document generated
        //Generate the chart element
        if ($regional_id == 'TODAS') {
            $strXML = "<chart id='TODAS' caption='Servidores Ativos por Municipio' pieSliceDepth='30' showBorder='1' formatNumberScale='0' numberSuffix='' animation='1' bgColor='99CCFF,FFFFFF' >";
        } else {
            $strXML = "<chart id='" . $nome_regional . "' caption='Servidores Ativos por Regional' pieSliceDepth='30' showBorder='1' formatNumberScale='0' numberSuffix='' animation='1' bgColor='99CCFF,FFFFFF' >";
        }

        //$strXML = "<chart id='emtramitacao' caption='Servidores por Solicitação' subCaption='EM TRAMITAÇÃO' pieSliceDepth='30' showBorder='1' formatNumberScale='0' numberSuffix='' animation='1' bgColor='99CCFF,FFFFFF' >";
        //$strXML = "<chart id='chart' caption='Gráfico de Servidores / Solicitações xAxisName='Tipo Solicitação' yAxisName='Quantidade' showValues='0' showBorder='1' formatNumberScale='0' numberSuffix='' animation='1' bgColor='99CCFF,FFFFFF'>";

        if ($cadastros) {
            // percorre os objetos retornados
            foreach ($cadastros as $reg) {
                //antes de armazenar verifica se algum campo eh requerido e nao foi informado
                $dados = $reg->toArray();
                if ($regional_id == 'TODAS') {
                    $strXML .= "<set label='" . $dados['municipio'] . "' value='" . $dados['qtd'] . "'  link='n-index.php?class=RelatorioServidorMunicipioPDF%26municipio=" . $dados['municipio'] . "' />";
                } else {
                    $strXML .= "<set label='" . $dados['municipio'] . "' value='" . $dados['qtd'] . "'  link='n-index.php?class=RelatorioServidorMunicipioPDF%26municipio=" . $dados['municipio'] . "' />";
                }
            }
            if (empty($dados['municipio'])) {
                $msg = 'Nao existem dados para os criterios selecionados';
                $icone = 'info';
                new TMessage($icone, $msg);
            }

            //Finally, close <chart> element
            $strXML .= "</chart>";


            echo "<div class='col-xs-12 col-sm-12 col-md-12 col-lg-12' style='margin-top:50px'>";
            if ($grafico == 'COLUNA 3D') {
                echo renderChart("app/lib/FusionCharts/Column3D.swf", "", $strXML, "chart", "100%", 800, false, false);
            } else {
                if ($grafico == 'PIZZA 3D') {
                    echo renderChart("app/lib/FusionCharts/Pie3D.swf", "", $strXML, "chart", "100%", 800, false, false);
                }
            }
            echo "</div>";

            // finaliza a transacao
            TTransaction::close();
            $this->loaded = true;
        }
    }

}
