<?php

/*
 * classe ProjecaoAposentadoriaGraf
 * Grafico de Projecao Aposentadoria: Contem o Grafico atraves da view vw_total_aposentadoria
 */

include_once 'app/lib/FusionCharts/FusionCharts.php';

class ProjecaoAposentadoriaGraf extends TPage {
    /*
     * metodo construtor
     * Cria a pagina e o Grafico
     */

    public function __construct() {
        parent::__construct();
    }

    /*
     * metodo onReload()
     * Carrega a DataGrid com os objetos do banco de dados
     */

    function onReload() {
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instancia um repositorio para Carros
        $repository = new TRepository('vw_total_aposentadoriaRecord');

        // cria um criterio de selecao, ordenado pelo id
        $criteria = new TCriteria;
        $criteria->add(new TFilter('empresa_id', '=', $_SESSION['empresa_id']) );
        $criteria->setProperty('order', 'situacao, total desc');
        // carrega os objetos de acordo com o criterio
        $cadastros = $repository->load($criteria);

        echo "<script LANGUAGE='Javascript' SRC='app/lib/FusionCharts/FusionCharts.js'></script>";

        //$strXML will be used to store the entire XML document generated
        //Generate the chart element

        $strXML = "<chart id='chart1' caption='Gráfico de Projeção para Aposentadoria' subCaption='' xAxisName='Situação' yAxisName='Quantidade de Servidores'  exportEnabled='1' exportAtClient='1' exportHandler='fcExporter1'>";

        if ($cadastros) {
            // percorre os objetos retornados
            foreach ($cadastros as $reg) {
                //antes de armazenar verifica se algum campo eh requerido e nao foi informado
                $dados = $reg->toArray();

                //Generate <set label='..' value='..' />
                $strXML .= "<set label='" . $dados['situacao'] . "' value='" . $dados['total'] . "'  link='n-index.php?class=RelatorioProjecaoAposentadoriaPDF%26situacao=" . $dados['situacao'] . "' />";
            }
        }
        //Finally, close <chart> element
        $strXML .= "</chart>";

        echo $strXML;

        echo "<div class='col-xs-12 col-sm-12 col-md-12 col-lg-12' style='margin-top:50px'>";
        echo renderChart("app/lib/FusionCharts/Column3D.swf", "", $strXML, "chart", "100%", 500, false, false);
        echo "</div>";

        // finaliza a transacao
        TTransaction::close();
        $this->loaded = true;
    }

    /*
     * metodo show()
     * Exibe a pagina
     */

    function show() {
        // se a listagem ainda nao foi carregada
        if (!$this->loaded) {
            $this->onReload();
        }
        parent::show();
    }

}

?>