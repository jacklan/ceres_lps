<?php

/*
 * classe GeraRelatorioServidoresEmFeriasMes
 * Relatorio de GeraRelatorioServidoresEmFeriasMes: Contem o filtro para seleção do relatório dos servidores em ferias por mes
 * Autor:Jackson Meires
 * Data:07/07/2016
 */

use Lib\Funcoes\Util;

//ini_set('display_errors', 1);
//ini_set('display_startup_erros', 1);
//error_reporting(E_ALL);

class GeraRelatorioServidoresExpertise extends TPage {

    private $form;     // formulario de cadastro

    /*
     * metodo construtor
     * Cria a pagina, o formulario e a listagem
     */

    public function __construct() {
        parent::__construct();

        // instancia um formulario
        $this->form = new TForm('form_busca_geraRelatorioServidoresExpertise');

        // instancia uma tabela
        $panel = new TPanelForm(700, 100);
        $panel->setColuna(250);

        // adiciona a tabela ao formulario
        $this->form->add($panel);

        // cria um r�tulo para o t�tulo
        $titulo = new TLabel('Relat&oacute;rio de Expertise dos Servidores');
        $titulo->setFontFace('Arial');
        $titulo->setFontColor('red');
        $titulo->setFontSize(12);

        // adiciona o campo Titulo
        $panel->put($titulo, $panel->getColuna(), $panel->getLinha());

        // cria os campos do formulario
        $expertise = new TDBEntry('expertise', 'pg_ceres', 'ExpertiseRecord', 'nome');
        // $expertise_id = new TDBCombo('expertise_id', 'pg_ceres', 'ExpertiseRecord', 'id', 'nome');
        //$capacitacao_id = new TDBCombo('capacitacao_id', 'pg_ceres', 'CapacitacaoRecord', 'id', 'nome');
        $expertise->setSize(40);
        $expertise->setProperty('style', 'text-transform: uppercase');
        $expertise->setProperty('placeholder', 'EX: CAJUCULTURA, APICULTURA');
        // $capacitacao_id->setSize(40);
        // cria um botao de acao (cadastrar)
        $new_button = new TButton('gerarelatorio');

        // define a acao do botao cadastrar
        $new_button->setAction(new TAction(array($this, 'onGenerate')), 'Gerar Relat&oacute;rio');

        // adiciona campo datafim
        $panel->putCampo($expertise, 'Informe sua pesquisa', 50, 1);
        //$panel->putCampo($capacitacao_id, 'Capacitacao', 0, 1);
        // adiciona campo opcao tipo funcionario
        $panel->putCampo(NULL, null, 0, 1);

        //adiciona botao gerar grafico
        $panel->putCampo($new_button, null, 50, 1);

        // define quais sao os campos do formulario
        $this->form->setFields(array($new_button,
            //     $capacitacao_id,
            $expertise));

        // monta a paina atraves de uma tabela
        $panel = new TPanelForm(700, 500);
        $panel->put($this->form, 0, 0);

        // adiciona a tabela a pagina
        parent::add($panel);
    }

    /*
     * metodo onGenerate()
     * Carrega a pagina do relatorio
     */

    function onGenerate() {

        new TSession ();

        $dados = $this->form->getData();
        $this->form->setData($dados);

        // TSession::setValue('datainicio', $dados->datainicio);
        //TSession::setValue('datafim', $dados->datafim);

        new RelatorioServidoresExpertisePDF();
    }

}
