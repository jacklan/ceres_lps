<?php

/*
 * classe GeraRelatorioAniversariantesList
 * Cadastro de GeraRelatorioAniversariantesList: Contem a listagem e o formulario de busca
 * Autor: Jackson Meires
 * Data: 19/11/2015
 */
    
//ini_set('display_errors', 1);
//ini_set('display_startup_erros', 1);
//error_reporting(E_ALL);

class GeraRelatorioAniversariantesList extends TPage {

    private $form;     // formulario de cadastro
    /*
     * metodo construtor
     * Cria a pagina, o formulario e a listagem
     */

    public function __construct() {
        parent::__construct();

        // instancia um formulario
        $this->form = new TForm('form_busca_GeraRelatorioServidorRecord');
        //$this->form->setNewWindow(true);
        // instancia uma tabela
        $panel = new TPanelForm(700, 100);
        $panel->setColuna(150);

        // adiciona a tabela ao formulario
        $this->form->add($panel);

        // cria um r�tulo para o t�tulo
        $titulo = new TLabel('Relatório de Aniversariantes do Mês');
        $titulo->setFontFace('Arial');
        $titulo->setFontColor('red');
        $titulo->setFontSize(12);

        // adiciona o campo Titulo
        $panel->put($titulo, $panel->getColuna(), $panel->getLinha());

        // cria os campos do formulario
        $mes = new TCombo('mes');
        $opcao = new TCombo('opcao');
        $regional = new TCombo('regional');

        //Cria um vetor com as opcoes da combo situacao
        $items2 = array();
        $items2['SIM'] = 'SIM';
        $items2['NAO'] = 'NÃO';
        // adiciona as opcoes na combo
        $opcao->addItems($items2);
        //coloca o valor padrao no combo
        $opcao->setValue('NAO');

        //Cria um vetor com as opcoes da combo situacao
        $items = array();
        $items[1] = 'JANEIRO';
        $items[2] = 'FEVEREIRO';
        $items[3] = 'MARÇO';
        $items[4] = 'ABRIL';
        $items[5] = 'MAIO';
        $items[6] = 'JUNHO';
        $items[7] = 'JULHO';
        $items[8] = 'AGOSTO';
        $items[9] = 'SETEMBRO';
        $items[10] = 'OUTUBRO';
        $items[11] = 'NOVEMBRO';
        $items[12] = 'DEZEMBRO';

        // adiciona as opcoes na combo
        $mes->addItems($items);
        //coloca o valor padrao no combo
        $mes->setValue(date("m"));
        $mes->setSize(40);
        $opcao->setSize(40);
        $regional->setSize(40);

        //CRIA A RELACAO DA TABELA ESTRANGEIRA PARA MODELO       
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $repository = new TRepository('RegionalRecord');
        $criteria = new TCriteria;
        $criteria->setProperty('order', 'nome');
        // carrega todos os objetos
        $collection = $repository->load($criteria);
        //adiciona os objetos no combo
        $items[0] = 'TODOS';
        foreach ($collection as $object) {
            $items[$object->id] = $object->nome;
        }

        // adiciona as opcoes na combo/  
        $regional->addItems($items);
        //coloca o valor padrao no combo
        $regional->setValue($_SESSION['regional_id']);

        TTransaction::close();

        // cria um botao de acao (cadastrar)
        $new_button = new TButton('gerarelatorio');

        //  $obj = new RelatorioAniversariantesPDF();
        // define a acao do botao cadastrar
        $new_button->setAction($action = new TAction(array($this, 'onGenerate')), 'Gerar Relatório');
        $new_button->setProperty('target', "_blank");


        $panel->setLinha(50);
        // adiciona o campo
        $panel->setColuna2(120);
        // adiciona campo opcao
        $panel->putCampo($mes, 'Escolha o mês', 0, 0);

        $panel->putCampo($opcao, 'Somente Ativos', 0, 1);

        $panel->putCampo($regional, 'Região', 0, 1);

        //adiciona botao gerar grafico
        $panel->putCampo($new_button, null, 0, 1);
        // $panel->put($new_button, $panel->getColuna()+270,$panel->getLinha());
        // define quais sao os campos do formulario
        $this->form->setFields(array($opcao, $mes, $regional, $new_button));

        // monta a paina atraves de uma tabela
        $panel = new TPanelForm(700, 500);
        $panel->put($this->form, 0, 0);

        // adiciona a tabela a pagina
        parent::add($panel);
    }

    function onGenerate() {

        new RelatorioAniversariantesPDF();

    }

}

?>