<?php
ini_set('display_errors', 1);
ini_set('display_startup_erros', 1);
error_reporting(E_ALL);


use Adianti\Database\TTransaction;
use Adianti\Database\TRepository;
use Adianti\Database\TCriteria;
use Adianti\Database\TFilter;
use Lib\Funcoes\Util;


//define o diretorio de fontes que so utilizadas pelo FPDF
define('FPDF_FONTPATH', 'app/lib/fpdf/font/');

//$ano = $_REQUEST['ano'];

class RelatorioSolicitacaoServidorAposentadoriaPDF extends FPDF {

//Page header
    function Header() {

        //endereco da imagem,posicao X(horizontal),posicao Y(vertical), tamanho altura, tamanho largura
        $this->Image("app/images/logo_relatorio.jpg", 8, 11, 26, 18);

        //Arial bold 15
        $this->SetFont('Arial', 'B', 8);
        $this->SetY("12");
        $this->SetX("35");
        $this->Cell(0, 5, utf8_decode("GOVERNO DO ESTADO DO RIO GRANDE DO NORTE"), 0, 1, 'J');

        $this->SetY("17");
        $this->SetX("35");
        $this->Cell(0, 5, utf8_decode("INSTITUTO DE ASSISTENCIA TECNICA E EXTENSAO RURAL - EMATER"), 0, 1, 'J');

        $this->SetY("22");
        $this->SetX("35");
        $this->Cell(0, 5, utf8_decode("UNIDADE INSTRUMENTAL DE RECURSOS HUMANOS - UIRH"), 0, 1, 'J');
        //define a fonte a ser usada
        $this->SetFont('Arial', 'B', 10);

        $this->SetY("29");
        $this->SetX("75");
        $titulo = "SOLICITAÇÃO DE APOSENTADORIA";
        $this->Cell(0, 5, utf8_decode($titulo), 0, 0, 'J');

        $this->Ln(3);

        $this->ColumnHeader();
    }

    function ColumnHeader() {
        //define a fonte a ser usada
        $this->SetFont('arial', 'B', 9);
        //$this->Cell(0, 0, '', 1, 1, 'L');
        $this->Ln();
    }

    function ColumnDetail() {

        $servidor_id = 0;
        $matricula = $_REQUEST['matricula'];
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        $repositorio = new TRepository('ServidorRecord');
        $condicao = new TCriteria();
        $condicao->add(new TFilter('matricula', '=', $matricula));
        $linhas = $repositorio->load($condicao);
        if ($linhas) {
            foreach ($linhas as $linha) {
                $servidor_id = $linha->id;
            }
        }
        // cria um criterio de selecao, ordenado pelo id
        $criteria = new TCriteria;
        // instancia um repositorio para Carros
        $repository = new TRepository('vw_relatorio_solicitacao_aposentadoriaRecord');
        //filtra pelo campo selecionado pelo usuário
        $criteria->add(new TFilter('servidor_id', '=', $servidor_id));
        //  $criteria->setProperty('order', 'nomeservidor');
        // carrega os objetos de acordo com o criterio
        $repository->load($criteria);
        // cria um criterio de selecao, ordenado pelo id
        // $criteria->add(new TFilter('nomeservidor', '=', $nomeservidor));

        $this->SetY(42);

        $i = 0;

        // carrega os objetos de acordo com o criterio
        $rows = $repository->load($criteria);
//        var_dump($rows);

        if ($rows) {
            // percorre os objetos retornados
            foreach ($rows as $row) {

                //define a fonte a ser usada
                $this->SetFont('arial', '', 9);
                $titulo02 = "1. Nº DO PROCESSO: ";
                $this->SetY("10");
                $this->SetX("150");
                $this->Cell(0, 5, utf8_decode($titulo02), 0, 0, 'J');

                $this->SetY("15");
                $this->SetX("150");
                $this->Cell(0, 5, utf8_decode("2.DATA:___/___/___"), 0, 0, 'J');

                //define a fonte a ser usada
                $this->SetFont('arial', '', 9);

                $this->SetY("35");
                $this->SetX("10");
                //define multiCelula
                $this->MultiCell(0, 5, utf8_decode("3. Tipo de Aposentadoria \n      ( ) Por tempo de contribuição                               ( ) Por por invalidez                          ( ) Por Compusória"), 1);

                $this->SetY("47");
                $this->SetX("10");
                $this->Cell(0, 5, utf8_decode("4. Autoridade a que se dirige: "), 1, 0, 'J');

                $this->SetY("55");
                $this->SetX("10");
                $this->Cell(0, 5, utf8_decode("5. Identificação"), 0, 0, 'J');

                $this->SetY("60");
                $this->SetX("10");
                $this->Cell(0, 5, utf8_decode("Nome do Servidor: " . $row->nomeservidor), 0, 0, 'J');

                $this->SetY("60");
                $this->SetX("155");
                $this->Cell(0, 5, utf8_decode("Data nascimento: " . TDate::date2br($row->nascimento)), 0, 0, 'J');

                $this->SetY("65");
                $this->SetX("10");
                $this->Cell(0, 5, utf8_decode("Admissão: " . TDate::date2br($row->admissao)), 0, 0, 'J');

                $this->SetY("65");
                $this->SetX("45");
                $this->Cell(0, 5, utf8_decode("Matricula: " . $row->matricula), 0, 0, 'J');

                $this->SetY("65");
                $this->SetX("75");
                $this->Cell(0, 5, utf8_decode("Cargo: " . $row->cargonome), 0, 0, 'J');

                $this->SetY("65");
                $this->SetX("162");
                $this->Cell(0, 5, utf8_decode("Categ. Prof./nível: " . $row->nivelsalarial), 0, 0, 'J');

                $this->SetY("70");
                $this->SetX("10");
                $this->Cell(0, 5, utf8_decode("Função: " . substr($row->descformacao, 0, 46)), 0, 0, 'J');

                $this->SetY("70");
                $this->SetX("110");
                $this->Cell(0, 5, utf8_decode("Formação: " . $row->nivel), 0, 0, 'J');
                $this->SetY("70");

                $this->SetX("162");
                $this->Cell(0, 5, utf8_decode("Lotação: " . $row->lotacao), 0, 0, 'J');

                $this->SetY("75");
                $this->SetX("10");
                $this->Cell(0, 5, utf8_decode("Endereço para corrempondencia: " . $row->endereco), 0, 1, 'J');

                // $this->SetY("80");
                $this->SetX("10");
                $this->MultiCell(0, 5, utf8_decode("6. Requerimento de aposentadoria por tempo de serviço \n Requer aposentadoria na forma da Lei, em ___/___/___ \n Assinatura do Requerente:___________________________________"), 1);

                $this->SetY("96");
                $this->SetX("10");
                $this->Cell(0, 5, utf8_decode("7. Requerimento de aposentadoria por invalidez:"), 0, 0, 'J');

                $this->SetY("100");
                $this->SetX("10");
                $this->MultiCell(0, 5, utf8_decode("A pedido________________________________________ Ex office: __________________________________________________\nEm, ___/___/___   Chefe Imediato:___________________________________ Junta médica:_______________________________"), 0);

                $this->SetY("113");
                $this->SetX("10");
                $this->MultiCell(0, 5, utf8_decode("________________________________________     ________________________________________\n                   Assinatura do Servidor                                                             Assinatura"), 0);

                $this->SetY("125");
                $this->SetX("80");
                $this->Cell(0, 5, utf8_decode("EXAME DA JUNTA MÉDICA"), 0, 0, 'J');

                $this->SetY("130");
                $this->SetX("10");
                $this->Cell(0, 5, utf8_decode("Examinando-se o servidor acima, esta Junta Médica constatou que:"), 0, 0, 'J');

                $this->SetY("135");
                $this->SetX("10");
                $this->Cell(0, 5, utf8_decode("Não faz jus ao benefício"), 0, 0, 'J');

                $this->SetY("140");
                $this->SetX("10");
                $this->MultiCell(0, 5, utf8_decode("Sendo portador de___________________________________________________________________________________________\nfaz jus ao benefício nos termos do Art.___________________________________________________________________________\n____________________________________________________________________________________________________________________________________________________________________________________________________________________"), 0);

                $this->SetY("165");
                $this->SetX("10");
                $this->Cell(0, 5, utf8_decode("Devolva-se ao órgão de origem, em ___/___/___"), 0, 0, 'J');

                $this->SetY("173");
                $this->SetX("25");
                $this->MultiCell(0, 5, utf8_decode("_____________________________________                                 ____________________________________\n               Presidente da Junta Médica                                                              Membro da Junta Médica"), 0);

                $this->SetY("185");
                $this->SetX("25");
                $this->MultiCell(0, 5, utf8_decode("_____________________________________                                 ____________________________________\n               Membro da Junta Médica                                                                  Membro da Junta Médica"), 0);

                $this->SetY("198");
                $this->SetX("10");
                $this->Cell(0, 5, utf8_decode("8.Comunicação de Aposentadoria Compulsória"), 0, 0, 'J');

                $this->SetY("203");
                $this->SetX("10");
                $this->Cell(0, 5, utf8_decode("Comunicação que o servidor acima identificado, atingiu a idade, conforme certidão anexa, estabelecida para aposentadoria compulsória."), 0, 0, 'J');

                $this->SetY("208");
                $this->SetX("10");
                $this->Cell(0, 5, utf8_decode("Anexada certidão: de nascimento ( ) de casamento ( )"), 0, 0, 'J');

                $this->SetY("215");
                $this->SetX("25");
                $this->MultiCell(0, 5, utf8_decode("_____________________________________                                 ____________________________________\n               Nome do Chefe Imediato                                                             Unidade de Lotação do Chefe Imediato"), 0);

                $this->SetY("225");
                $this->SetX("25");
                $this->MultiCell(0, 5, utf8_decode("_____________________________________                                 _______________________,___/___/___\n               Local e Data                                                                                Assinatura do Chefe Imediato"), 0);

                $this->SetY("235");
                $this->SetX("10");
                $this->Cell(0, 5, utf8_decode("9.Despacho da Autoridade Competente"), 0, 0, 'J');

                $this->SetY("240");
                $this->SetX("25");
                $this->MultiCell(0, 5, utf8_decode("À Unidade de Pessoal________________________________________________________________________________\n________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________"), 0);

                $this->SetY("266");
                $this->SetX("10");
                $this->MultiCell(0, 5, utf8_decode("Em,___/___/___                                                                                             __________________________________\n                                                                                                                                   Assinatura do Chefe Imediato"), 0);

                #### Inicio calculo da certidão de tempo de serviço ####
           /*     
                TTransaction::open('pg_ceres');
                $criteria = new TCriteria;
                // instancia um repositorio para Carros
                $repository = new TRepository('vw_servidor_certidaotemposervico');
                //filtra pelo campo selecionado pelo usuário
                $criteria->add(new TFilter('servidor_id', '=', $servidor_id));
                //   var_dump($servidor_id);
                // carrega os objetos de acordo com o criterio
                $rows02 = $repository->load($criteria);
                // finaliza a transacao
                TTransaction::close();

                $this->SetY(42);

                $i = 0;
                $faltas = 0;
                $suspencao = 0;
                $licenca = 0;
                $licencaPremio = 0;
                $averbacao = 0;
                $funcao = 0;

                if ($rows02) {
                    // percorre os objetos retornados
                    foreach ($rows02 as $row02) {

                        // adiciona os dados do perfil do usuario no menu
                        if ($row02->nomeservidor == '' || $row02->nomeservidor == null) {
                            $this->SetFont('arial', '', 15);
                            $this->SetX("15");
                            $this->Cell(0, 5, utf8_decode("Nao existem dados cadastrados para os parametros desta consulta"), 0, 1, 'J');
                            $this->Ln();
                        }

                        //define a fonte a ser usada
                        $this->SetFont('arial', '', 9);

                        $this->SetY("275");
                        $this->SetX("10");
                        if ($row02->dataaposentadoria <> null) {
                            $this->Cell(0, 5, utf8_decode("10.Comprovanção de tempo de serviço referente ao período " . formatar_data($row02->admissao) . " a " . formatar_data($row02->dataaposentadoria)), 0, 1, 'J');
                        } else {
                            $this->Cell(0, 5, utf8_decode("10.Comprovanção de tempo de serviço referente ao período " . formatar_data($row02->admissao) . " a até atual momento."), 0, 1, 'J');
                        }

                        //$this->SetY("280");
                        $this->SetX("10");
                        $this->MultiCell(0, 5, utf8_decode("  ANO  |    TEMPO BRUTO    |    FALTAS    |     LICENÇAS   |    SUSPENSÕES    |    OUTRAS     |      SOMA    |      TEMPO L�?QUIDO"), 1);

                        ###### inicio impressão prineira linha do item 10 ######
                        $inicio = $row02->ano_inicio;
                        if (empty($row02->dataaposentadoria)) {
                            $fim = $row02->ano_fim;
                        } else {
                            $fim = substr(formatar_data($row02->dataaposentadoria), 6, 4);
                            $this->Cell(0, 5, $fim, 0, 0, 'J');
                        }

                        $residuolicencas = 0;
                        $residuosuspensao = 0;
                        $somames = 0;
                        $liquidomes = 0;
                        $liquidosubtotal = 0;

                        $this->SetX("10");
                        $this->Cell(0, 5, utf8_decode("| " . $row02->ano_inicio . "  | "), 0, 0, 'J');
                        //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
                        $this->Cell(0, 0, '', 1, 1, 'L');

                        $this->SetX("30");
                        $this->Cell(0, 5, utf8_decode($row02->diasinicio . "                  | "), 0, 0, 'J');
                        //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
                        $this->Cell(0, 0, '', 1, 1, 'L');
                        //imprime as faltas no ano
                        $faltas_d = $this->faltas($servidor_id, $inicio);
                        $this->SetX("60");
                        if ($faltas_d > 0) {
                            $this->Cell(0, 5, $faltas_d, 0, 0, 'J');
                            $faltas += $faltas_d;
                            $somames += $faltas_d;
                        } else {
                            $this->Cell(0, 5, '-            | ', 0, 0, 'J');
                        }

                        //imprimir licencas no ano
                        $nlicenca = $this->licencas($servidor_id, $inicio);
                        $residuolicencas += $nlicenca;
                        $this->SetX("80");
                        if ($residuolicencas > 0) {
                            //existe licencas
                            //verifica se é maior do que os dias ano
                            if ($residuolicencas > $row02->diasinicio) {
                                $residuolicencas -= $row02->diasinicio;
                                $this->Cell(0, 5, "| " . $row02->diasinicio . " | ", 0, 0, 'J');
                                //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
                                $this->Cell(0, 0, '', 1, 1, 'L');
                                $licenca += $row02->diasinicio;
                                $somames += $row02->diasinicio;
                            } else {
                                //coloca o restante para o ano
                                $this->Cell(0, 5, "| " . $residuolicencas . " | ", 0, 0, 'J');
                                //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
                                $this->Cell(0, 0, '', 1, 1, 'L');
                                $licenca += $residuolicencas;
                                $somames += $residuolicencas;
                                $residuolicencas = 0;
                            }
                        } else {
                            $this->Cell(0, 5, '-                | ', 0, 0, 'J');
                            //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
                            $this->Cell(0, 0, '', 1, 1, 'L');
                        }

                        //imprimir suspensao
                        $nsuspensao = $this->suspensao($servidor_id, $inicio);
                        $residuosuspensao += $nsuspensao;
                        $this->SetX("110");

                        if ($residuosuspensao > 0) {
                            //existe licencas
                            //verifica se é maior do que os dias ano
                            if ($residuosuspensao > $row02->diasinicio) {
                                $residuosuspensao -= $row02->diasinicio;
                                $this->Cell(0, 5, $row02->diasinicio . "      ", 0, 0, 'J');
                                //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
                                $this->Cell(0, 0, '', 1, 1, 'L');
                                $suspencao += $row02->diasinicio;
                                $somames += $row02->diasinicio;
                            } else {
                                //coloca o restante para o ano
                                $this->Cell(0, 5, $residuosuspensao, 0, 0, 'J');
                                $suspencao += $residuosuspensao;
                                $somames += $residuosuspensao;
                                $residuosuspensao = 0;
                            }
                        } else {
                            $this->Cell(0, 5, '-                | ', 0, 0, 'J');
                            //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
                            $this->Cell(0, 0, '', 1, 1, 'L');
                        }
                        ###### fim impressão prineira linha do item 10 ######
                        ###### inicio impressão intervalo do item 10 ######
                        $liquidomes = ($row02->diasinicio - $somames);
                        $liquidosubtotal = $liquidomes;
                        $this->SetX("147");
                        $this->Cell(0, 5, "|         " . $somames . "          |", 0, 0, 'J');
                        //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
                        $this->Cell(0, 0, '', 1, 1, 'L');

                        $this->SetX("190");
                        $this->Cell(0, 5, $liquidomes . "   |", 0, 1, 'J');
                        //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
                        $this->Cell(0, 0, '', 1, 1, 'L');

                        $inicio++;
                        $somadias = ($faltas + $licenca + $suspencao);
                        $servidor_id = $row02->servidor_id;
                        while ($inicio < $fim) {
                            $somames = 0;
                            $liquidomes = 0;
                            $diasano = 0;
                            //verificar se ano e bissexto
                            if (( (($inicio % 4) == 0 && ($inicio % 100) != 0) || ($inicio % 400) == 0)) {
                                $diasano = 366;
                            } else {
                                $diasano = 365;
                            }

                            $this->SetX("10");
                            $this->Cell(0, 5, utf8_decode("| " . $inicio . "  | "), 0, 0, 'J');
                            //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
                            $this->Cell(0, 0, '', 1, 1, 'L');

                            $this->SetX("30");
                            $this->Cell(0, 5, utf8_decode($diasano . "                  | "), 0, 0, 'J');
                            //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
                            $this->Cell(0, 0, '', 1, 1, 'L');

                            //imprime as faltas no ano
                            $faltas_d = $this->faltas($servidor_id, $inicio);
                            $this->SetX("60");
                            if ($faltas_d > 0) {
                                $this->Cell(0, 5, $faltas_d, 0, 0, 'J');
                                //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
                                $this->Cell(0, 0, '', 1, 1, 'L');
                                $faltas += $faltas_d;
                                $somames += $faltas_d;
                            } else {
                                $this->Cell(0, 5, '-            | ', 0, 0, 'J');
                                //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
                                $this->Cell(0, 0, '', 1, 1, 'L');
                            }

                            //imprimir licencas no ano
                            $nlicenca = $this->licencas($servidor_id, $inicio);
                            $residuolicencas += $nlicenca;
                            $this->SetX("80");
                            if ($residuolicencas > 0) {
                                //existe licencas
                                //verifica se é maior do que os dias ano
                                if ($residuolicencas > $diasano) {
                                    $residuolicencas -= $diasano;
                                    $this->Cell(0, 5, $diasano . '           | ', 0, 0, 'J');
                                    //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
                                    $this->Cell(0, 0, '', 1, 1, 'L');
                                    $licenca += $diasano;
                                    $somames += $diasano;
                                } else {
                                    //coloca o restante para o ano
                                    $this->Cell(0, 5, $residuolicencas . '           | ', 0, 0, 'J');
                                    //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
                                    $this->Cell(0, 0, '', 1, 1, 'L');
                                    $licenca += $residuolicencas;
                                    $somames += $residuolicencas;
                                    $residuolicencas = 0;
                                }
                            } else {
                                $this->Cell(0, 5, '-                | ', 0, 0, 'J');
                                //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
                                $this->Cell(0, 0, '', 1, 1, 'L');
                            }

                            //imprimir suspensao
                            $nsuspensao = $this->suspensao($servidor_id, $inicio);
                            $residuosuspensao += $nsuspensao;
                            $this->SetX("110");

                            if ($residuosuspensao > 0) {
                                //existe licencas
                                //verifica se é maior do que os dias ano
                                if ($residuosuspensao > $diasano) {
                                    $residuosuspensao -= $diasano;
                                    $this->Cell(0, 5, $diasano, 0, 0, 'J');
                                    $suspencao += $diasano;
                                    $somames += $diasano;
                                } else {
                                    //coloca o restante para o ano
                                    $this->Cell(0, 5, $residuosuspensao, 0, 0, 'J');
                                    $suspencao += $residuosuspensao;
                                    $somames += $residuosuspensao;
                                    $residuosuspensao = 0;
                                }
                            } else {
                                $this->Cell(0, 5, '-                | ', 0, 0, 'J');
                                //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
                                $this->Cell(0, 0, '', 1, 1, 'L');
                            }

                            $liquidomes = ($diasano - $somames);
                            $this->SetX("147");
                            if ($somames == 0) {
                                $this->Cell(0, 5, "|         " . $somames . "          |", 0, 0, 'J');
                                //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
                                $this->Cell(0, 0, '', 1, 1, 'L');
                            } else {
                                $this->Cell(0, 5, "|         " . $somames . "      |", 0, 0, 'J');
                                //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
                                $this->Cell(0, 0, '', 1, 1, 'L');
                            }

                            $this->SetX("190");
                            if ($liquidomes == 0) {
                                $this->Cell(0, 5, $liquidomes . "       |", 0, 1, 'J');
                                //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
                                $this->Cell(0, 0, '', 1, 1, 'L');
                            } else {
                                $this->Cell(0, 5, $liquidomes . "   |", 0, 1, 'J');
                                //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
                                $this->Cell(0, 0, '', 1, 1, 'L');
                            }

                            $somadias = ($faltas + $licenca + $suspencao);
                            $liquidosubtotal += $liquidomes;
                            $inicio++;
                        }
                        ###### fim impressão intervalo do item 10 ######
                        ###### inicio impressão ultima linha do item 10 ######
                        $this->SetX("10");
                        $this->Cell(0, 5, utf8_decode("| " . $fim . "  |"), 0, 0, 'J');
                        //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
                        $this->Cell(0, 0, '', 1, 1, 'L');

                        $this->SetX("30");
                        //verifica data da solicitacao da aposentadoria
                        if ($row->tiposolicitacao == 'APOSENTADORIA' && $row->situacao == 'CONCLUIDA' || $row->situacao == 'EM TRAMITACAO') {
                            //calcula numero de dias da data da solicitacao aposentadoria com a datafim(data atual) vw_sertidaotempode servico
                            $dadafimsolicitacao = diasDeDiferenca(formatar_data($row->database), formatar_data($row02->datafim));
                            $diasfim = $row02->diasfim - $dadafimsolicitacao;
                        } else {
                            $diasfim = $row02->diasfim;
                        }

                        $this->Cell(0, 5, utf8_decode($diasfim . "                  |"), 0, 0, 'J');
                        //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
                        $this->Cell(0, 0, '', 1, 1, 'L');

                        $somames = 0;
                        $liquidomes = 0;
//                        $liquidosubtotal = 0;
                        //imprime as faltas no ano
                        $faltas_d = $this->faltas($servidor_id, $inicio);
                        $this->SetX("60");
                        if ($faltas_d > 0) {
                            $this->Cell(0, 5, $faltas_d . "       |", 0, 0, 'J');
                            //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
                            $this->Cell(0, 0, '', 1, 1, 'L');
                            $faltas += $faltas_d;
                            $somames += $faltas_d;
                        } else {
                            $this->Cell(0, 5, '-            | ', 0, 0, 'J');
                            //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
                            $this->Cell(0, 0, '', 1, 1, 'L');
                        }

                        //imprimir licencas no ano
                        $nlicenca = $this->licencas($servidor_id, $inicio);
                        $residuolicencas += $nlicenca;
                        $this->SetX("80");
                        if ($residuolicencas > 0) {
                            //existe licencas
                            //verifica se é maior do que os dias ano
                            if ($residuolicencas > $diasfim) {
                                $residuolicencas -= $diasfim;
                                $this->Cell(0, 5, $diasfim . "               |", 0, 0, 'J');
                                //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
                                $this->Cell(0, 0, '', 1, 1, 'L');
                                $licenca += $diasfim;
                                $somames += $diasfim;
                            } else {
                                //coloca o restante para o ano
                                $this->Cell(0, 5, $residuolicencas . "           |", 0, 0, 'J');
                                //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
                                $this->Cell(0, 0, '', 1, 1, 'L');
                                $residuolicencas -= $diasfim;
                                $licenca += $diasfim;
                                $somames += $diasfim;
                                $residuolicencas = 0;
                            }
                        } else {
                            $this->Cell(0, 5, '-                | ', 0, 0, 'J');
                            //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
                            $this->Cell(0, 0, '', 1, 1, 'L');
                        }
                        //imprimir suspensao
                        $nsuspensao = $this->suspensao($servidor_id, $inicio);
                        $residuosuspensao += $nsuspensao;
                        $this->SetX("110");

                        if ($residuosuspensao > 0) {
                            //existe licencas
                            //verifica se é maior do que os dias ano
                            if ($residuosuspensao > $diasfim) {
                                $residuosuspensao -= $diasfim;
                                $this->Cell(0, 5, $diasfim . "       |", 0, 0, 'J');
                                //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
                                $this->Cell(0, 0, '', 1, 1, 'L');
                                $suspencao += $diasfim;
                                $somames += $diasfim;
                            } else {
                                //coloca o restante para o ano
                                $this->Cell(0, 5, $residuosuspensao . "     |", 0, 0, 'J');
                                //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
                                $this->Cell(0, 0, '', 1, 1, 'L');
                                $suspencao += $residuosuspensao;
                                $somames += $residuosuspensao;
                                $residuosuspensao = 0;
                            }
                        } else {
                            $this->Cell(0, 5, '-                | ', 0, 0, 'J');
                            //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
                            $this->Cell(0, 0, '', 1, 1, 'L');
                        }
                        //verificar se valor por zero atribuir dias fim para dias fim não ficar negativo
                        if ($somames == 0) {
                            $liquidomes = $diasfim;
                        } else {
                            $liquidomes = ($somames - $diasfim);
                        }

                        $this->SetX("147");
                        if ($somames == 0) {
                            $this->Cell(0, 5, "|         " . $somames . "          |", 0, 0, 'J');
                            //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
                            $this->Cell(0, 0, '', 1, 1, 'L');
                        } else {
                            $this->Cell(0, 5, "|         " . $somames . "      |", 0, 0, 'J');
                            //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
                            $this->Cell(0, 0, '', 1, 1, 'L');
                        }

                        $this->SetX("190");
                        if ($liquidomes == 0) {
                            $this->Cell(0, 5, $liquidomes . "       |", 0, 1, 'J');
                            //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
                            $this->Cell(0, 0, '', 1, 1, 'L');
                        } else {
                            $this->Cell(0, 5, $liquidomes . "   |", 0, 1, 'J');
                            //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
                            $this->Cell(0, 0, '', 1, 1, 'L');
                        }
                        ###### fim impressão ultima linha do item 10 ######
                        //imprimir licença premio
                        TTransaction::open('pg_ceres');
                        $criteria1 = new TCriteria;
                        // instancia um repositorio para Carros
                        $repository1 = new TRepository('vw_servidor_certidaotemposervico_licencas_premio');
                        //filtra pelo campo selecionado pelo usuário
                        $criteria1->add(new TFilter('servidor_id', '=', $servidor_id));
                        // carrega os objetos de acordo com o criterio
                        $rows1 = $repository1->load($criteria1);
                        // finaliza a transacao
                        TTransaction::close();

                        $this->Ln(3);
                        $this->SetFont('arial', 'B', 9);
                        $this->SetX("10");
                        $this->Cell(0, 5, utf8_decode('AVERBAÇÃO DE LICENÇA PRÊMIO'), 0, 1, 'J');
                        $this->SetX("10");
                        $this->MultiCell(0, 5, utf8_decode(" ANO |                                                                                                                                                                           | TEMPO L�?QUIDO"), 1);

                        if ($rows1) {
                            foreach ($rows1 as $row1) {
                                $this->SetX("10");
                                $this->Cell(0, 5, substr(formatar_data($row1->datainicio), 6, 4) . "/" . substr(formatar_data($row1->datafim), 6, 4), 0, 0, 'J');
                                $this->SetX("190");
                                $this->Cell(0, 5, $row1->dias, 0, 1, 'J');
                                $licencaPremio += $row1->dias;
                            }
                        } else {
                            $this->SetX("70");
                            $this->Cell(0, 5, utf8_decode('Não consta nenhuma licença premio averbada'), 0, 1, 'J');
                        }
                        //calcular SubTotal sem averbação
                        $somadias = ($faltas + $licenca + $suspencao);
                        $liquidosubtotal += $liquidomes;

                        //Calcular Total com averbação
                        $total = $liquidosubtotal + $licencaPremio;
                        $total = $liquidosubtotal + $licencaPremio + $averbacao;

                        $this->SetFont('arial', 'B', 9);
                        $this->Ln(5);
                        $this->SetX("172");
                        $this->Cell(0, 5, "SubTotal: " . $liquidosubtotal . $t, 0, 1, 'J');

                        $this->SetX("10");
                        $this->Cell(0, 5, utf8_decode("11.Conta com " . $total . " ou seja, " . time1text($total) . " de serviço."), 0, 1, 'J');
                        //linha
                        $this->Cell(0, 0, '', 1, 1, 'L');

                        $this->SetX("10");
                        $this->MultiCell(0, 5, utf8_decode("12.Demostrativo do tempo de serviço prestado na administração pública e na atividade privada, rural e urbana (União, Estado, Município)."), 0);

                        //imprimir tempo de serviço averbação
                        TTransaction::open('pg_ceres');
                        $criteria1 = new TCriteria;
                        // instancia um repositorio para Carros
                        $repository1 = new TRepository('vw_servidor_certidaotemposervico_averbacao');
                        //filtra pelo campo selecionado pelo usuário
                        $criteria1->add(new TFilter('servidor_id', '=', $servidor_id));
                        // carrega os objetos de acordo com o criterio
                        $rows1 = $repository1->load($criteria1);
                        // finaliza a transacao
                        TTransaction::close();
                        $this->Ln();

                        $this->SetX("10");
                        $this->MultiCell(0, 5, utf8_decode(" ORGÃO/ENTIDADE |                                                                                                                                                           | Anos / Dias"), 1);

                        if ($rows1) {
                            $orgao = "";
                            foreach ($rows1 as $row1) {
                                $this->SetX("10");
                                //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
                                $this->Cell(0, 0, '', 1, 1, 'L');
                                $this->MultiCell(190, 5, utf8_decode($row1->nomeorgao . "\n" . $row1->observacao));

                                $this->SetX("180");
                                //ver a questao de mais de um ano
                                $inicial = $row1->ano;
                                $final = $row1->anofim;
//                                if ($orgao == $row1->nomeorgao) {
//                                    $row1->diasinicio = 0;
//                                    $row1->diasfim = 0;
//                                }

                                while ($inicial <= $final) {
                                    $this->SetX("178");
                                    if ($inicial == $row1->ano) {
                                        $this->Cell(0, 5, " | " . $inicial . " / " . $row1->diasinicio . "  |", 0, 1, 'J');
                                        //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
//                                        $this->Cell(0, 0, '', 1, 1, 'L');
                                    } else {
                                        if ($inicial == $final) {
                                            if ($row1->diasnaoaverbados == null) {
                                                $this->Cell(0, 5, " | " . $inicial . " / " . $row1->diasfim . "  |", 0, 1, 'J');
                                            } else {
                                                $this->Cell(0, 5, " | " . $inicial . " / " . ($row1->diasfim - $row1->diasnaoaverbados) . "  |", 0, 1, 'J');
                                            }
                                            //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
//                                            $this->Cell(0, 0, '', 1, 1, 'L');
                                        } else {
                                            if (bissexto($inicial) == true) {
                                                $this->Cell(0, 5, " | " . $inicial . " / " . '366' . "  |", 0, 1, 'J');
                                            } else {
                                                $this->Cell(0, 5, " | " . $inicial . " / " . '365' . "  |", 0, 1, 'J');
                                            }
                                            //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
//                                            $this->Cell(0, 0, '', 1, 1, 'L');
                                        }
                                    }
                                    $inicial++;
                                }
                                //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
                                $this->Cell(0, 0, '', 1, 1, 'L');
//                                if ($row1->diasnaoaverbados != null) {
//                                    $this->Cell(0, 5, "Total: " . $row1->dias . " Dias averbados. Total de dias não averbados: " . $row1->diasnaoaverbados, 0, 1, 'J');
//                                } else {
                                $this->Cell(0, 5, "Total: " . $row1->dias . " Dias averbados.", 0, 1, 'J');
//                                }
                                $orgao = $row1->nomeorgao;
                                $averbacao += $row1->dias;
                            }
                        } else {
                            $this->SetX("74");
                            $this->Cell(0, 5, utf8_decode('Não consta nenhuma averbação de tempo de serviço'), 0, 1, 'J');
                        }
                        $this->SetX("10");
                        //converter dias em os anos meses e dias
                        $this->Cell(0, 5, utf8_decode("13.Conta com " . $averbacao . " dias averbados ou seja, " . time1text($averbacao) . " de serviço."), 0, 1, 'J');
                        $this->Cell(0, 0, '', 1, 1, 'L');

                        $this->SetX("10");
                        $this->Cell(0, 5, utf8_decode("14.Demostrativo de tempo de serviço referente cargos em comissão e/ou funções gratificadas"), 0, 1, 'J');

                        //imprimir tempo de serviço averbação
                        TTransaction::open('pg_ceres');
                        $criteria1 = new TCriteria;
                        // instancia um repositorio para Carros
                        $repository1 = new TRepository('vw_servidor_por_funcao');
                        //filtra pelo campo selecionado pelo usuário
                        $criteria1->add(new TFilter('servidor_id', '=', $servidor_id));
                        // carrega os objetos de acordo com o criterio
                        $rows1 = $repository1->load($criteria1);
                        // finaliza a transacao
                        TTransaction::close();

                        $this->SetX("10");
                        $this->MultiCell(0, 5, utf8_decode(" DENOMINAÇÃO |                                                                                                                                                           | Anos / Dias"), 1);

                        if ($rows1) {
                            foreach ($rows1 as $row1) {
                                $this->SetX("10");
                                $this->Cell(0, 5, utf8_decode($row1->nome_funcao), 0, 0, 'J');
                                if ($row1->datafim == null) {
                                    $this->SetX("153");
                                    $this->Cell(0, 5, utf8_decode(formatar_data($row1->datainicio) . " / até atual momento"), 0, 1, 'J');
                                    // Calcula a diferença de dias
                                    $dias = diasDeDiferenca(formatar_data($row1->datainicio), date("d/m/Y"));
                                    $funcao += $dias;
                                } else {
                                    $this->SetX("165");
                                    $this->Cell(0, 5, formatar_data($row1->datainicio) . " / " . formatar_data($row1->datafim), 0, 1, 'J');
                                    $funcao += $row1->dias;
                                }
                            }
                        } else {
                            $this->SetX("74");
                            $this->Cell(0, 5, utf8_decode('Não consta nenhuma averbação de tempo de serviço'), 0, 1, 'J');
                        }
                        //converter dias em os anos meses e dias
                        $this->Cell(0, 5, utf8_decode("15.Conta com " . time1text($funcao) . " de serviço."), 0, 1, 'J');
                    }
                }

                TTransaction::close();

                #### Fim calculo da certidão de tempo de serviço ####
                // $this->SetY("235");

                $this->Cell(0, 0, '', 1, 1, 'L');
                //Variaveis dos campos
                $this->SetX("10");
                $this->Cell(0, 5, utf8_decode(""), 0, 0, 'J');
                $this->Cell(0, 0, '', 1, 1, 'L');

                $this->SetX("10");
                $this->Cell(0, 5, utf8_decode("16.Informações complementares"), 0, 1, 'J');
//                $this->Cell(0, 0, '', 1, 1, 'L');
            */    /*if ($row->inf_complementares != null) {
                    $this->MultiCell(190, 5, utf8_decode($row->inf_complementares));
                    $this->Ln(5);
                } else {
                    $this->SetX("60");
                    $this->Cell(0, 5, utf8_decode("O servidor não possuie informações complementares."), 0, 1, 'J');
                    $this->Ln(5);
                }*/
/*
                //inserir dias de averbação ao total
                $total+=$averbacao;

                $this->SetX("10");
                $this->MultiCell(0, 5, utf8_decode("17.Certifico que o tempo de serviço computado para aposentadoria corresponde a " . $total . " ou seja, " . time1text($total) . " de serviço."), 0);
                $this->Cell(0, 0, '', 1, 1, 'L');

                $this->SetX("10");
                $this->Cell(0, 5, utf8_decode("Natal-RN,       /      /                                                           Confere_______________________________________"), 0, 1, 'J');

                $this->SetX("90");
                $this->MultiCell(0, 5, utf8_decode("_____________________________________________\nNatal-RN,      /       /      "), 0);

                $this->SetX("10");
                $this->MultiCell(0, 5, utf8_decode("_____________________________________________                 _____________________________________________\n            Responsável pelas Informações/Matrículas                                     Responsável pelas Informações/Matrículas"), 0);
                $this->Ln(10);

                $this->SetX("10");
                $this->Cell(0, 5, utf8_decode("18.Despacho da Unidade de Pessoal"), 0, 1, 'J');

                $this->SetX("10");
                $this->Cell(0, 5, utf8_decode("À Assessoria Jurídica_______________________________________________________________________________________"), 0, 1, 'J');
                $this->SetX("10");
                $this->MultiCell(0, 5, utf8_decode("__________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________"), 0);
                $this->Ln(5);
                $this->SetX("10");
                $this->MultiCell(0, 5, utf8_decode("Natal-RN,  " . date("d/m/Y") . ".                                                                          ________________________________________________\n                                                                                                                         Responsável pelas Informações/Matrículas "), 0);

                $this->SetX("10");
                $this->Cell(0, 5, utf8_decode("19.Parecer da Assessoria Jurídica"), 0, 1, 'J');

                $this->SetX("10");
                $this->Cell(0, 5, utf8_decode("O servidor ( ) faz jus ( ) não faz jus\n ao benefício conforme instrução processual, nos temos do(s) Art(s)__________________"), 0, 1, 'L');
                $this->Cell(0, 5, utf8_decode("__________________________________________________________________________________________________________"), 0, 1, 'L');

                $this->SetX("10");
                $this->Cell(0, 5, utf8_decode("Diante do exposto somos pelo ( ) deferimento ( )Inderefimento"), 0, 1, 'J');
                $this->Cell(0, 5, utf8_decode("Encaminha-se o processo para _______________________________________________________________________________"), 0, 1, 'J');
                $this->MultiCell(0, 5, utf8_decode("____________________________________________________________________________________________________________________________________________________________________________________________________________________"), 0);
                $this->Ln(5);

                //$this->SetY("210");
                $this->SetX("10");
                $this->MultiCell(0, 5, utf8_decode("Natal-RN,       /      /                                                                              ________________________________________________\n                                                                                                                                          Assesoria Jurídica "), 0);
                $this->Ln(5);
                //linha
                $this->Cell(0, 0, '', 1, 1, 'L');
                //$this->SetY("222");
                $this->SetX("10");
                $this->MultiCell(0, 5, utf8_decode("20.Aprovo. Publíque-se e providencie-se\nempenho\nEm,"), 0);

                //$this->SetY("222");
                $this->SetX("120");
                $this->MultiCell(0, 5, utf8_decode("21.Resolução Nº____ de ____/____/____\nPublicado no Diário Oficial____/____/____\nÀ Unidade de Pessoal para apostilar.\nEm,____/____/____"), 0);

                $this->SetX("10");
                $this->Cell(0, 5, utf8_decode("22.Cálculo dos Proventos"), 0, 1, 'J');
                $this->Cell(0, 0, '', 1, 1, 'L');

                $this->SetX("70");
                $this->Cell(0, 5, utf8_decode("Discriminação                                                                            |          Valor    "), 0, 1, 'J');
                $this->Cell(0, 0, '', 1, 1, 'L');

                //imprimir informacoes sobre provento
                TTransaction::open('pg_ceres');
                $criteria01 = new TCriteria;
                // instancia um repositorio para Carros
                $repository01 = new TRepository('ServidorProvento');
                //filtra pelo campo selecionado pelo usuário
                $criteria01->add(new TFilter('servidor_id', '=', $servidor_id));

                // carrega os objetos de acordo com o criterio
                // finaliza a transacao
                $rows01 = $repository01->load($criteria01);
                // var_dump($rows01." - ".$servidor_id);
                TTransaction::close();
                if ($rows01) {
                    foreach ($rows01 as $row01) {
                        $this->SetX("10");
                        $this->MultiCell(150, 5, utf8_decode($row01->provento));

                        $this->SetX("160");

                        $this->Cell(0, 5, "R$ " . $row01->valor, 0, 1, 'J');
                        $totalProvento+=$row01->valor;
                    }
                    $this->Cell(0, 0, '', 1, 1, 'L');
                    $this->SetX("10");
                    $this->Cell(0, 5, utf8_decode("Total de Proventos"), 0, 0, 'J');
                    $this->SetX("159");
                    $this->Cell(0, 5, utf8_decode("| R$ " . formatar_moeda($totalProvento)), 0, 1, 'J');
                    $this->Cell(0, 0, '', 1, 1, 'L');
                } else {
                    $this->SetX("50");
                    $this->Cell(0, 5, utf8_decode("O servidor não possuie proventos a serem calculados."), 0, 1, 'J');

                    $this->Ln(5);
                    $this->Cell(0, 0, '', 1, 1, 'L');
                    $this->SetX("10");
                    $this->Cell(0, 5, utf8_decode("Total de Proventos                                              |                        "), 0, 1, 'J');
                    $this->Cell(0, 0, '', 1, 1, 'L');
                }

            

                //$this->SetY("266");
                $this->SetX("10");
                $this->MultiCell(0, 5, utf8_decode("Apostilado. À Unidade Financeira para empenho. Em,     ____/____/___"), 0);
                $this->MultiCell(0, 5, utf8_decode("                                                                                                                                            ____________________________________\n                                                                                                                                               Responsável pela Unidade de Pessoal"), 0);
                // $this->SetY("222");
                $this->Ln(5);
                $this->SetX("10");
                $this->MultiCell(0, 5, utf8_decode("23.Empenho nº________________________________                           24.Diante do Parecer favorável do Tribunal de Contas\n Ao Tribunal de Contas do Estado para fins de registro.                      do Estado, Encaminhe-se a Unidade de Pessoal para Em,____/____/____                                                                                    implantação.\n                                                                                                                     Em,____/____/____ "), 0, "L", false);
                $this->Ln(5);

                $this->SetX("10");
                $this->MultiCell(0, 5, utf8_decode("_________________________________________                                     ____________________________________________\n           Responsavel pela Unidade Financeira                                                   Responsável pela Unidade Financeira "), 0);
                $this->Ln(8);

                //  $this->SetY("70");
                $this->SetX("10");
                $this->MultiCell(0, 5, utf8_decode("25.Recebi a Resolução                                                                              26.Implantação. Em,____/____/____\nEm,____/____/____                                                                                    Arquive-se. Em,____/____/____ "), 0);
                $this->Ln(8);

                $this->SetX("10");
                $this->MultiCell(0, 5, utf8_decode("____________________________________                                     __________________________________________\n                          Aposentado                                                                            Responsável pela Unidade de Pessoal "), 0);

                $this->Ln();

                //contador para gerar total
                $i = ($i + 1);
                $t = ($i);
            }
        } else {

            $this->SetFont('arial', 'B', 14);
            $this->SetX("15");
            $this->Cell(0, 5, utf8_decode("Não foi possivel efetuar a consulta, favor verificar os seguintes itens."), 0, 1, 'C');
            $this->Ln(5);
            $this->SetFont('arial', '', 12);
            $this->MultiCell(190, 5, utf8_decode("1. Se a matricula informada esta correta.\n2. Se existem dados registrados referentes a formação titulação do servidor consultado. \n3. Se existem dados registrados referentes a lotação do servidor consultado. \n4. Se existem dados registrados referentes a solicitação de aposentadoria do servidor consultado\n5. Verificar o status da situação do servidor se o mesmo já encontra-se aposentado."), 0, "C");

            TTransaction::close();
        }
    }
*/

       /* function faltas($servidor, $ano) {
        //imprimir faltas
        TTransaction::open('pg_ceres');
        // cria um criterio de selecao, ordenado pelo id
        $criteria_falta = new TCriteria;
        // instancia um repositorio para Carros
        $repository_falta = new TRepository('vw_servidor_certidaotemposervico_faltas');
        //filtra pelo campo selecionado pelo usuário
        $criteria_falta->add(new TFilter('servidor_id', '=', $servidor));
        $criteria_falta->add(new TFilter('ano', '<=', $ano));
        $criteria_falta->add(new TFilter('anofim', '>=', $ano));

        // carrega os objetos de acordo com o criterio
        $rows_falta = $repository_falta->load($criteria_falta);
        // finaliza a transacao
        TTransaction::close();

        $nfalta = 0;
        if ($rows_falta) {
            foreach ($rows_falta as $row_falta) {
                if ($ano == $row_falta->ano) {
                    $nfalta += $row_falta->diasinicio;
                } else {
                    if ($ano == $row_falta->anofim) {
                        $nfalta += $row_falta->diasfim;
                    }
                }
            }
        }
        return $nfalta;
    }

    function licencas($servidor, $ano) {
        //imprimir licencas
        // cria um criterio de selecao, ordenado pelo id
        TTransaction::open('pg_ceres');
        $criteria_licencas = new TCriteria;
        // instancia um repositorio para Carros
        $repository_licencas = new TRepository('vw_servidor_certidaotemposervico_licencas');
        //filtra pelo campo selecionado pelo usuário
        $criteria_licencas->add(new TFilter('servidor_id', '=', $servidor));
        $criteria_licencas->add(new TFilter('ano', '<=', $ano));
        $criteria_licencas->add(new TFilter('anofim', '>=', $ano));

        // carrega os objetos de acordo com o criterio
        $rows_licencas = $repository_licencas->load($criteria_licencas);
        // finaliza a transacao
        TTransaction::close();

        $nlicenca = 0;


        if ($rows_licencas) {
            foreach ($rows_licencas as $row_licenca) {
                if ($row_licenca->tipolicenca_id == 1 || $row_licenca->tipolicenca_id == 8 || $row_licenca->tipolicenca_id == 9) {

                    if ($ano == $row_licenca->ano) {
                        $nlicenca += $row_licenca->diasinicio;
                    } else {
                        if ($ano == $row_licenca->anofim) {
                            $nlicenca += $row_licenca->diasfim;
                        } else {
                            $nlicenca += 365;
                            //verificar se ano e bissexto
                            if (( (($ano % 4) == 0 && ($ano % 100) != 0) || ($ano % 400) == 0)) {
                                $nlicenca += 1;
                            }
                        }
                    }
                }
            }
        }
        return $nlicenca;
    }

    function suspensao($servidor, $ano) {
        //imprimir suspensao
        // cria um criterio de selecao, ordenado pelo id
        TTransaction::open('pg_ceres');
        $criteria_suspensao = new TCriteria;
        // instancia um repositorio para Carros
        $repository_suspensao = new TRepository('vw_servidor_certidaotemposervico_suspensao');
        //filtra pelo campo selecionado pelo usuário
        $criteria_suspensao->add(new TFilter('servidor_id', '=', $servidor));
        $criteria_suspensao->add(new TFilter('ano', '<=', $ano));
        $criteria_suspensao->add(new TFilter('anofim', '>=', $ano));

        // carrega os objetos de acordo com o criterio
        $rows_suspensao = $repository_suspensao->load($criteria_suspensao);
        // finaliza a transacao
        TTransaction::close();

        $nsuspensao = 0;
        if ($rows_suspensao) {
            foreach ($rows_suspensao as $row_suspensao) {
//                $nsuspensao += $row_suspensao->dias;
                 if ($ano == $row_suspensao->ano) {
                    $nsuspensao += $row_suspensao->diasinicio;
                } else {
                    if ($ano == $row_suspensao->anofim) {
                        $nsuspensao += $row_suspensao->diasfim;
                    }
                }
            }
        }
        //var_dump($nsuspensao);
        return $nsuspensao;
    }*/

//Page footer
    function Footer() {
        //Position at 1.5 cm from bottom
        $this->SetY(-15);
        //Arial italic 8
        $this->SetFont('Arial', 'I', 8);
        //Page number
        //data atual
        $data = date("d/m/Y H:i:s");
        $conteudo = "impresso em " . $data;
        $texto = "http://www.emater.rn.gov.br";
        //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
        $this->Cell(0, 0, '', 1, 1, 'L');

        //imprime uma celula... largura,altura, texto,borda,quebra de linha, alinhamento
        $this->Cell(0, 5, $texto, 0, 0, 'L');
        $this->Cell(0, 5, 'Pag. ' . $this->PageNo() . ' de ' . '{nb}' . ' - ' . $conteudo, 0, 0, 'R');
        $this->Ln();
    }

}

        }
    }
}


//Instanciation of inherited class
$pdf = new RelatorioSolicitacaoServidorAposentadoriaPDF("P", "mm", "A4");
//$pdf=new PDF();
//instacia a classe FPDF
//$pdf= new FPDF("P","mm","A4");
//define o titulo
$pdf->SetTitle(utf8_decode("Relatorio Solicitação de Aposentadoria - EMATER-RN"));

//assunto
$pdf->SetSubject(utf8_decode("Relatorio Solicitação de Aposentadoria - EMATER-RN"));

$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times', '', 12);
$pdf->ColumnDetail();

$file = "app/reports/RelatorioSolicitacaoServidorAposentadoriaPDF" . $_SESSION['servidor_id'] . ".pdf";
//abrir pdf
$pdf->Output($file);
$pdf->openFile($file);
?>