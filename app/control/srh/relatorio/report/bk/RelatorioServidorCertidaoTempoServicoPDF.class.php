<?php

use Adianti\Database\TTransaction;
use Adianti\Database\TRepository;
use Adianti\Database\TCriteria;
use Adianti\Database\TFilter;
use Lib\Funcoes\Util;

class RelatorioServidorCertidaoTempoServicoPDF extends FPDF {

//Page header
    function Header() {

        //endereco da imagem,posicao X(horizontal),posicao Y(vertical), tamanho altura, tamanho largura
        $this->Image("app/images/logo_relatorio.jpg", 8, 11, 26, 18);

        //Arial bold 15
        $this->SetFont('Arial', 'B', 8);
        $this->SetY("12");
        $this->SetX("35");
        $this->Cell(0, 5, utf8_decode("GOVERNO DO ESTADO DO RIO GRANDE DO NORTE"), 0, 1, 'J');

        $this->SetY("17");
        $this->SetX("35");
        $this->Cell(0, 5, utf8_decode("INSTITUTO DE ASSISTENCIA TECNICA E EXTENSAO RURAL - EMATER"), 0, 1, 'J');

        $this->SetY("22");
        $this->SetX("35");
        $this->Cell(0, 5, utf8_decode("UNIDADE INSTRUMENTAL DE RECURSOS HUMANOS - UIRH"), 0, 1, 'J');

        $this->Ln();
        $this->SetFont('Arial', 'B', 10);
        $this->SetX("10");
        $this->Cell(0, 5, utf8_decode("CERTIDÃO DE TEMPO DE SERVIÇO"), 0, 1, 'C');

        $this->ColumnHeader();
    }

    function ColumnHeader() {
        //define a fonte a ser usada
        $this->SetFont('arial', 'B', 9);
    }

    function ColumnDetail() {

        $matricula = $_REQUEST['matricula'];

        TTransaction::open('pg_ceres');
        $criteria = new TCriteria;
        // instancia um repositorio para Carros
        $repository = new TRepository('vw_servidor_certidaotemposervicoRecord');
        //filtra pelo campo selecionado pelo usuário
        $criteria->add(new TFilter('matricula', '=', $matricula));
        $criteria->add(new TFilter('empresa_id', '=', $_SESSION['empresa_id']));
        // carrega os objetos de acordo com o criterio
        $rows02 = $repository->load($criteria);
        // finaliza a transacao
        TTransaction::close();

        $this->SetY(42);

        $i = 0;
        $faltas = 0;
        $suspencao = 0;
        $licenca = 0;
        $licencaPremio = 0;
        $averbacao = 0;

        if ($rows02) {
            // percorre os objetos retornados
            foreach ($rows02 as $row02) {

                // adiciona os dados do perfil do usuario no menu
                if ($row02->nomeservidor == '' || $row02->nomeservidor == null) {
                    $this->SetFont('arial', '', 15);
                    $this->SetX("15");
                    $this->Cell(0, 5, utf8_decode("Nao existem dados cadastrados para os parametros desta consulta"), 0, 1, 'J');
                    $this->Ln();
                }

                //define a fonte a ser usada
                $this->SetFont('arial', '', 9);
                $this->SetX("10");
                $this->Cell(0, 5, utf8_decode('Nome Servidor: ' . $row02->nomeservidor), 0, 0, 'J');
                
               // $datanascimento = TDate($row02->nascimento);
                
                $this->SetX("130");
                $this->Cell(0, 5, utf8_decode('Matrícula: ' . $row02->matricula . ' Idade(Anos): ' . Util::calcularIdade($row02->nascimento)), 0, 1, 'J');

                $this->Cell(0, 5, utf8_decode("Cargo: " . $row02->cargonome . " Classe: " . $row02->classecargo . " Nivel: " . $row02->nivelsalarial . " Unidade Lotação: " . $row02->lotacao), 0, 1, 'J');
                if ($row02->dataaposentadoria <> null) {
                    $this->Cell(0, 5, utf8_decode("Tempo de Serviço no Órgão:" . TDate::date2br($row02->admissao) . " a " . TDate::date2br($row02->dataaposentadoria)), 0, 1, 'J');
                } else {
                    $this->Cell(0, 5, utf8_decode("Tempo de Serviço no Órgão:" . TDate::date2br($row02->admissao) . " a até atual momento"), 0, 1, 'J');
                }

                $this->SetX("10");
                $this->Cell(0, 5, utf8_decode('Ano'), 0, 0, 'J');

                $this->SetX("20");
                $this->Cell(0, 5, utf8_decode('Tempo'), 0, 0, 'J');

                $this->SetX("80");
                $this->Cell(0, 5, utf8_decode('Deduções'), 0, 0, 'J');

                $this->SetX("150");
                $this->Cell(0, 5, utf8_decode('Tempo'), 0, 1, 'J');

                $this->SetX("20");
                $this->Cell(0, 5, utf8_decode('Bruto'), 0, 0, 'J');

                $this->SetX("40");
                $this->Cell(0, 5, utf8_decode('Faltas'), 0, 0, 'J');

                $this->SetX("60");
                $this->Cell(0, 5, utf8_decode('Licenças'), 0, 0, 'J');

                $this->SetX("80");
                $this->Cell(0, 5, utf8_decode('Suspenção'), 0, 0, 'J');

                $this->SetX("103");
                $this->Cell(0, 5, utf8_decode('Outras'), 0, 0, 'J');

                $this->SetX("120");
                $this->Cell(0, 5, utf8_decode('Soma'), 0, 0, 'J');

                $this->SetX("170");
                $this->Cell(0, 5, utf8_decode('Líquido'), 0, 1, 'J');

                $inicio = $row02->ano_inicio;
                $fim = $row02->ano_fim;
                $somames = 0;
                $liquidomes = 0;
                $liquidosubtotal = 0;

                $this->SetX("10");
                $this->Cell(0, 5, utf8_decode($row02->ano_inicio), 0, 0, 'J');

                $this->SetX("20");
                $this->Cell(0, 5, utf8_decode($row02->diasinicio), 0, 0, 'J');

                //imprimir faltas
                TTransaction::open('pg_ceres');
                // cria um criterio de selecao, ordenado pelo id
                $criteria1 = new TCriteria;
                // instancia um repositorio para Carros
                $repository1 = new TRepository('vw_servidor_certidaotemposervico_faltasRecord');
                //filtra pelo campo selecionado pelo usuário
                $criteria1->add(new TFilter('servidor_id', '=', $servidor_id));
                $criteria1->add(new TFilter('ano', '=', $inicio));

                // carrega os objetos de acordo com o criterio
                $rows1 = $repository1->load($criteria1);
                if ($rows1) {
                    foreach ($rows1 as $row1) {
                        $this->SetX("40");
                        $this->Cell(0, 5, $row1->dias, 0, 0, 'J');
                        $faltas += $row1->dias;
                        $somames += $row1->dias;
                    }
                } else {
                    $this->SetX("40");
                    $this->Cell(0, 5, '-', 0, 0, 'J');
                }

                //imprimir licencas
                // cria um criterio de selecao, ordenado pelo id
                $criteria1 = new TCriteria;
                // instancia um repositorio para Carros
                $repository1 = new TRepository('vw_servidor_certidaotemposervico_licencasRecord');
                //filtra pelo campo selecionado pelo usuário
                $criteria1->add(new TFilter('servidor_id', '=', $servidor_id));
                $criteria1->add(new TFilter('ano', '=', $inicio));

                // carrega os objetos de acordo com o criterio
                $rows1 = $repository1->load($criteria1);
                if ($rows1) {
                    foreach ($rows1 as $row1) {
                        $this->SetX("60");
                        $this->Cell(0, 5, $row1->dias, 0, 0, 'J');
                        $licenca += $row1->dias;
                        $somames += $row1->dias;
                    }
                } else {
                    $this->SetX("60");
                    $this->Cell(0, 5, '-', 0, 0, 'J');
                }

                //imprimir suspensao
                // cria um criterio de selecao, ordenado pelo id
                $criteria1 = new TCriteria;
                // instancia um repositorio para Carros
                $repository1 = new TRepository('vw_servidor_certidaotemposervico_suspensaoRecord');
                //filtra pelo campo selecionado pelo usuário
                $criteria1->add(new TFilter('servidor_id', '=', $servidor_id));
                $criteria1->add(new TFilter('ano', '=', $inicio));

                // carrega os objetos de acordo com o criterio
                $rows1 = $repository1->load($criteria1);
                if ($rows1) {
                    foreach ($rows1 as $row1) {
                        $this->SetX("80");
                        $this->Cell(0, 5, $row1->dias, 0, 0, 'J');
                        $suspencao += $row1->dias;
                        $somames += $row1->dias;
                    }
                } else {
                    $this->SetX("80");
                    $this->Cell(0, 5, '-', 0, 0, 'J');
                }
                // finaliza a transacao
                TTransaction::close();

                $liquidomes = ($row02->diasinicio - $somames);
                $liquidosubtotal = $liquidomes;
                $this->SetX("120");
                $this->Cell(0, 5, $somames, 0, 0, 'J');

                $this->SetX("170");
                $this->Cell(0, 5, $liquidomes, 0, 1, 'J');


                $inicio++;
                $somadias = ($faltas + $licenca + $suspencao);
                $servidor_id = $row02->servidor_id;

                while ($inicio < $fim) {
                    $somames = 0;
                    $liquidomes = 0;
                    $diasano = 0;
                    //verificar se ano e bissexto
                    if (( (($inicio % 4) == 0 && ($inicio % 100) != 0) || ($inicio % 400) == 0)) {
                        $diasano = 366;
                    } else {
                        $diasano = 365;
                    }

                    $this->SetX("10");
                    $this->Cell(0, 5, utf8_decode($inicio), 0, 0, 'J');

                    $this->SetX("20");
                    $this->Cell(0, 5, utf8_decode($diasano), 0, 0, 'J');

                    //imprimir faltas
                    TTransaction::open('pg_ceres');
                    // cria um criterio de selecao, ordenado pelo id
                    $criteria1 = new TCriteria;
                    // instancia um repositorio para Carros
                    $repository1 = new TRepository('vw_servidor_certidaotemposervico_faltasRecord');
                    //filtra pelo campo selecionado pelo usuário
                    $criteria1->add(new TFilter('servidor_id', '=', $servidor_id));
                    $criteria1->add(new TFilter('ano', '=', $inicio));

                    // carrega os objetos de acordo com o criterio
                    $rows1 = $repository1->load($criteria1);
                    if ($rows1) {
                        foreach ($rows1 as $row1) {
                            $this->SetX("40");
                            $this->Cell(0, 5, $row1->dias, 0, 0, 'J');
                            $faltas += $row1->dias;
                            $somames += $row1->dias;
                        }
                    } else {
                        $this->SetX("40");
                        $this->Cell(0, 5, '-', 0, 0, 'J');
                    }

                    //imprimir licencas
                    // cria um criterio de selecao, ordenado pelo id
                    $criteria1 = new TCriteria;
                    // instancia um repositorio para Carros
                    $repository1 = new TRepository('vw_servidor_certidaotemposervico_licencasRecord');
                    //filtra pelo campo selecionado pelo usuário
                    $criteria1->add(new TFilter('servidor_id', '=', $servidor_id));
                    $criteria1->add(new TFilter('ano', '=', $inicio));

                    // carrega os objetos de acordo com o criterio
                    $rows1 = $repository1->load($criteria1);
                    if ($rows1) {
                        foreach ($rows1 as $row1) {
                            $this->SetX("60");
                            $this->Cell(0, 5, $row1->dias, 0, 0, 'J');
                            $licenca += $row1->dias;
                            $somames += $row1->dias;
                        }
                    } else {
                        $this->SetX("60");
                        $this->Cell(0, 5, '-', 0, 0, 'J');
                    }

                    //imprimir suspensao
                    // cria um criterio de selecao, ordenado pelo id
                    $criteria1 = new TCriteria;
                    // instancia um repositorio para Carros
                    $repository1 = new TRepository('vw_servidor_certidaotemposervico_suspensaoRecord');
                    //filtra pelo campo selecionado pelo usuário
                    $criteria1->add(new TFilter('servidor_id', '=', $servidor_id));
                    $criteria1->add(new TFilter('ano', '=', $inicio));

                    // carrega os objetos de acordo com o criterio
                    $rows1 = $repository1->load($criteria1);
                    if ($rows1) {
                        foreach ($rows1 as $row1) {
                            $this->SetX("80");
                            $this->Cell(0, 5, $row1->dias, 0, 0, 'J');
                            $suspencao += $row1->dias;
                            $somames += $row1->dias;
                        }
                    } else {
                        $this->SetX("80");
                        $this->Cell(0, 5, '-', 0, 0, 'J');
                    }
                    // finaliza a transacao
                    TTransaction::close();

                    $liquidomes = ($diasano - $somames);
                    $this->SetX("120");
                    $this->Cell(0, 5, $somames, 0, 0, 'J');

                    $this->SetX("170");
                    $this->Cell(0, 5, $liquidomes, 0, 1, 'J');


                    $inicio++;
                    $somadias = ($faltas + $licenca + $suspencao);
                    $liquidosubtotal += $liquidomes;
                }

                $this->SetX("10");
                $this->Cell(0, 5, utf8_decode($fim), 0, 0, 'J');

                $this->SetX("20");
                $this->Cell(0, 5, utf8_decode($row02->diasfim), 0, 0, 'J');
                //imprimir faltas
                TTransaction::open('pg_ceres');
                // cria um criterio de selecao, ordenado pelo id
                $criteria1 = new TCriteria;
                // instancia um repositorio para Carros
                $repository1 = new TRepository('vw_servidor_certidaotemposervico_faltasRecord');
                //filtra pelo campo selecionado pelo usuário
                $criteria1->add(new TFilter('servidor_id', '=', $servidor_id));
                $criteria1->add(new TFilter('ano', '=', $inicio));

                // carrega os objetos de acordo com o criterio
                $rows1 = $repository1->load($criteria1);
                if ($rows1) {
                    foreach ($rows1 as $row1) {
                        $this->SetX("40");
                        $this->Cell(0, 5, $row1->dias, 0, 0, 'J');
                        $faltas += $row1->dias;
                        $somames += $row1->dias;
                    }
                } else {
                    $this->SetX("40");
                    $this->Cell(0, 5, '-', 0, 0, 'J');
                }

                //imprimir licencas
                // cria um criterio de selecao, ordenado pelo id
                $criteria1 = new TCriteria;
                // instancia um repositorio para Carros
                $repository1 = new TRepository('vw_servidor_certidaotemposervico_licencasRecord');
                //filtra pelo campo selecionado pelo usuário
                $criteria1->add(new TFilter('servidor_id', '=', $servidor_id));
                $criteria1->add(new TFilter('ano', '=', $inicio));

                // carrega os objetos de acordo com o criterio
                $rows1 = $repository1->load($criteria1);
                if ($rows1) {
                    foreach ($rows1 as $row1) {
                        $this->SetX("60");
                        $this->Cell(0, 5, $row1->dias, 0, 0, 'J');
                        $licenca += $row1->dias;
                        $somames += $row1->dias;
                    }
                } else {
                    $this->SetX("60");
                    $this->Cell(0, 5, '-', 0, 0, 'J');
                }

                //imprimir suspensao
                // cria um criterio de selecao, ordenado pelo id
                $criteria1 = new TCriteria;
                // instancia um repositorio para Carros
                $repository1 = new TRepository('vw_servidor_certidaotemposervico_suspensaoRecord');
                //filtra pelo campo selecionado pelo usuário
                $criteria1->add(new TFilter('servidor_id', '=', $servidor_id));
                $criteria1->add(new TFilter('ano', '=', $inicio));

                // carrega os objetos de acordo com o criterio
                $rows1 = $repository1->load($criteria1);
                if ($rows1) {
                    foreach ($rows1 as $row1) {
                        $this->SetX("80");
                        $this->Cell(0, 5, $row1->dias, 0, 0, 'J');
                        $suspencao += $row1->dias;
                        $somames += $row1->dias;
                    }
                } else {
                    $this->SetX("80");
                    $this->Cell(0, 5, '-', 0, 0, 'J');
                }

                $liquidomes = ($row02->diasfim - $somames);
                $this->SetX("120");
                $this->Cell(0, 5, $somames, 0, 0, 'J');

                $this->SetX("170");
                $this->Cell(0, 5, $liquidomes, 0, 1, 'J');

                $somadias = ($faltas + $licenca + $suspencao);
                $liquidosubtotal += $liquidomes;

                $this->SetFont('arial', 'B', 9);
                $this->SetX("152");
                $this->Cell(0, 5, "SubTotal: " . $liquidosubtotal . $t, 0, 1, 'J');

                $this->Ln();
                //imprimir licença premio
                $criteria1 = new TCriteria;
                // instancia um repositorio para Carros
                $repository1 = new TRepository('vw_servidor_certidaotemposervico_licencas_premioRecord');
                //filtra pelo campo selecionado pelo usuário
                $criteria1->add(new TFilter('servidor_id', '=', $servidor_id));
                // carrega os objetos de acordo com o criterio
                $rows1 = $repository1->load($criteria1);
                $this->SetX("10");
                $this->Cell(0, 5, utf8_decode('Averbação de licença prêmio com fundamento no § 2º Art:102, da Lei Complementar 122/94.'), 0, 1, 'J');

                if ($rows1) {
                    $this->SetX("10");
                    $this->Cell(0, 5, utf8_decode("Licença(s) Prêmio(s) não Gozadas(s) - LC 122 de 30.06.94"), 0, 1, 'J');
                    foreach ($rows1 as $row1) {
                        $this->SetX("10");
                        $this->Cell(0, 5, utf8_decode("Data inicio: " . TDate::date2br($row1->datainicio) . " Data Fim: " . TDate::date2br($row1->datafim)), 0, 1, 'J');
                        $licencaPremio += $row1->dias;
                    }
                } else {
                    $this->SetX("70");
                    $this->Cell(0, 5, utf8_decode('Não consta nenhuma licença premio averbada'), 0, 1, 'J');
                }

                $total = $liquidosubtotal + $licencaPremio;
                //imprimir tempo de serviço averbação
                $criteria1 = new TCriteria;
                // instancia um repositorio para Carros
                $repository1 = new TRepository('vw_servidor_certidaotemposervico_averbacaoRecord');
                //filtra pelo campo selecionado pelo usuário
                $criteria1->add(new TFilter('servidor_id', '=', $servidor_id));
                // carrega os objetos de acordo com o criterio
                $rows1 = $repository1->load($criteria1);
                $this->Ln();
                $this->SetX("10");
                $this->Cell(0, 5, utf8_decode("DISCRIMINAÇÃO DO TEMPO DE SERVIÇO AVERBADO"), 0, 1, 'J');
                $this->SetX("10");
                $this->Cell(0, 5, utf8_decode("Orgão/Entidade"), 0, 0, 'J');
                $this->SetX("160");
                $this->Cell(0, 5, utf8_decode("Anos / Dias"), 0, 1, 'J');

                if ($rows1) {
                    foreach ($rows1 as $row1) {
                        $this->SetX("10");
                        $this->Cell(0, 5, $row1->nomeorgao, 0, 0, 'J');
                        $this->SetX("160");
                        $this->Cell(0, 5, $row1->ano . " / " . $row1->dias, 0, 1, 'J');

                        $averbacao += $row1->dias;
                    }
                } else {
                    $this->SetX("74");
                    $this->Cell(0, 5, utf8_decode('Não consta nenhuma averbação de tempo de serviço'), 0, 1, 'J');
                }
            }

            $this->Ln();
            $this->SetX("10");
            //converter dias em os anos meses e dias
            $this->Cell(0, 5, utf8_decode("Tempo Trabalho na EMATER/RN conta com: " . Util::time1text($liquidosubtotal)), 0, 1, 'J');

            $this->SetX("10");
            //converter dias em os anos meses e dias
            $this->Cell(0, 5, utf8_decode("Averbação conta com: " . Util::time1text($averbacao)), 0, 1, 'J');

            $total = $liquidosubtotal + $licencaPremio + $averbacao;
            $this->SetFont('arial', 'B', 9);
            $this->SetX("128");
            $this->Cell(0, 5, "TEMPO TOTAL APURADO: " . $total . $t, 0, 1, 'J');
            $this->Ln();

            $this->Cell(0, 5, utf8_decode("CERTIFICO, face ao apurado, que o interessado conta nesta data, com o tempo total líquido de serviço de:"), 0, 1, 'J');
            $this->Cell(0, 5, utf8_decode($total . " " . diasPorExtenso($total) . " ou seja, " . time1text($total)), 0, 1, 'J');
            $this->Cell(0, 0, '', 1, 1, 'L');
            $this->Cell(0, 5, utf8_decode("Observações: Conclusão da Certidão no Verso da Folha, devidamente assinada pelos responsáveis. "), 0, 1, 'J');
        }

        TTransaction::close();
    }

//Page footer
    function Footer() {
        //Position at 1.5 cm from bottom
        $this->SetY(-15);
        //Arial italic 8
        $this->SetFont('Arial', 'I', 8);
        //Page number
        //data atual
        $data = date("d/m/Y H:i:s");
        $conteudo = "impresso em " . $data;
        $texto = "http://www.emater.rn.gov.br";
        //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
        $this->Cell(0, 0, '', 1, 1, 'L');

        //imprime uma celula... largura,altura, texto,borda,quebra de linha, alinhamento
        $this->Cell(0, 5, $texto, 0, 0, 'L');
        $this->Cell(0, 5, 'Pag. ' . $this->PageNo() . ' de ' . '{nb}' . ' - ' . $conteudo, 0, 0, 'R');
        $this->Ln();
    }

}

//Instanciation of inherited class
$pdf = new RelatorioServidorCertidaoTempoServicoPDF("P", "mm", "A4");

//define o titulo
$pdf->SetTitle("Certidao Tempo de Servico - EMATER-RN");

//assunto
$pdf->SetSubject("Certidao Tempo de Servico - EMATER-RN");

$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times', '', 12);
$pdf->ColumnDetail();
$file = "app/reports/RelatorioServidorCertidaoTempoServicoPDF" . $_SESSION['servidor_id'] . ".pdf";
//abrir pdf
$pdf->Output($file);
$pdf->openFile($file);
?>