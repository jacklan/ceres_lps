<?php

use Adianti\Database\TTransaction;
use Adianti\Database\TRepository;
use Adianti\Database\TCriteria;
use Adianti\Database\TFilter;

$ano = $_REQUEST['ano'];

class RelatorioServidorMunicipioPDF extends FPDF {

//Page header
    function Header() {
        //endereco da imagem,posicao X(horizontal),posicao Y(vertical), tamanho altura, tamanho largura
        $this->Image("app/images/logo_relatorio.jpg", 8, 11, 26, 18);

        //Arial bold 15
        $this->SetFont('Arial', 'B', 12);
        $this->SetY("12");
        $this->SetX("35");
        $this->Cell(0, 5, utf8_decode("GOVERNO DO ESTADO DO RIO GRANDE DO NORTE"), 0, 1, 'J');

        $this->SetY("17");
        $this->SetX("35");
        $this->Cell(0, 5, utf8_decode($_SESSION['empresa_nome']), 0, 1, 'J');

        $titulo = "Servidores por Municipio - " . $_REQUEST['municipio'];
        $this->SetY("27");
        $this->SetX("35");
        $this->Cell(0, 5, utf8_decode($titulo), 0, 1, 'J');

        $this->Ln(3);

        $this->ColumnHeader();
    }

    function ColumnHeader() {
        //define a fonte a ser usada
        $this->SetFont('arial', 'B', 9);
        $this->SetY("35");
        $this->SetX("5");
        $this->Cell(0, 5, "Matricula", 0, 1, 'J');
        $this->SetY("35");
        $this->SetX("20");
        $this->Cell(0, 5, "Nome", 0, 1, 'J');
        $this->SetY("35");
        $this->SetX("80");
        $this->Cell(0, 5, "CPF", 0, 1, 'J');
        $this->SetY("35");
        $this->SetX("100");
        $this->Cell(0, 5, "Cargo", 0, 1, 'J');
        $this->SetY("35");
        $this->SetX("150");
        $this->Cell(0, 5, "E-Mail", 0, 1, 'J');
        $this->Cell(0, 0, '', 1, 1, 'L');
        $this->Ln();
    }

    function ColumnDetail() {

        $municipio = $_REQUEST['municipio'];

        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instancia um repositorio para Carros
        $repository = new TRepository('Vw_servidores_por_regiao_listagemRecord');

        // cria um criterio de selecao, ordenado pelo id
        $criteria = new TCriteria;
        
        $criteria->add(new TFilter('municipio', '=', $municipio));
        $criteria->add(new TFilter('empresa_id', '=', $_SESSION['empresa_id']));

        $this->SetY(42);

        $i = 0;

        // carrega os objetos de acordo com o criterio
        $rows = $repository->load($criteria);

        if ($rows) {
            // percorre os objetos retornados
            foreach ($rows as $row) {

                // adiciona os dados do perfil do usuario no menu
                if ($row->municipio == '') {
                    $this->SetFont('arial', '', 15);
                    $this->SetX("15");
                    $this->Cell(0, 5, utf8_decode("Nao existem dados cadastrados para os  parametros desta consulta"), 0, 1, 'J');
                    $this->Ln();
                }

                //define a fonte a ser usada
                $this->SetFont('arial', '', 9);

                $this->SetX("5");
                $this->Cell(0, 5, utf8_decode($row->matricula), 0, 0, 'J');

                $this->SetX("20");
                $this->Cell(0, 5, utf8_decode(substr($row->nome, 0, 30)), 0, 0, 'J');

                $this->SetX("80");
                $this->Cell(0, 5, utf8_decode($row->cpf), 0, 0, 'J');

                $this->SetX("100");
                $this->Cell(0, 5, utf8_decode($row->cargo), 0, 0, 'J');

                $this->SetX("150");
                $this->Cell(0, 5, utf8_decode($row->email), 0, 0, 'J');
                $this->Ln();

                //contador para gerar total
                $i = ($i + 1);
                $t = ($i);
            }

            $this->SetFont('arial', 'B', 9);
            $this->SetX("10");
            $this->Cell(0, 5, "Total de Servidores: " . $t, 0, 1, 'J');
        }
        TTransaction::close();
    }

//Page footer
    function Footer() {
        //Position at 1.5 cm from bottom
        $this->SetY(-15);
        //Arial italic 8
        $this->SetFont('Arial', 'I', 8);
        //Page number
        //data atual
        $data = date("d/m/Y H:i:s");
        $conteudo = "impresso em " . $data;
        $texto = $_SESSION['empresa_sitio'];
        //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
        $this->Cell(0, 0, '', 1, 1, 'L');

        //imprime uma celula... largura,altura, texto,borda,quebra de linha, alinhamento
        $this->Cell(0, 5, $texto, 0, 0, 'L');
        $this->Cell(0, 5, 'Pag. ' . $this->PageNo() . ' de ' . '{nb}' . ' - ' . $conteudo, 0, 0, 'R');
        $this->Ln();
    }

}

//Instanciation of inherited class
$pdf = new RelatorioServidorMunicipioPDF("P", "mm", "A4");

//define o titulo
$pdf->SetTitle("Relatorio dos Servidores por Municipio - EMATER-RN");

//assunto
$pdf->SetSubject("Relatorio do Servidor por Municipio  - EMATER-RN");

$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times', '', 12);
$pdf->ColumnDetail();
$file = "app/reports/RelatorioServidorMunicipioPDF" . $_SESSION['servidor_id'] . ".pdf";
//abrir pdf
$pdf->Output($file);
$pdf->openFile($file);
?>
