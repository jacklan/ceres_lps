<?php

use Adianti\Database\TTransaction;
use Adianti\Database\TRepository;
use Adianti\Database\TCriteria;
use Adianti\Database\TFilter;

class RelatorioConselhodeClasseServidorPDF extends FPDF {

//Page header
    function Header() {
        $conselho = $_REQUEST['conselho'];

//endereco da imagem,posicao X(horizontal),posicao Y(vertical), tamanho altura, tamanho largura
        $this->Image("app/images/logo_relatorio.jpg", 8, 11, 18, 18);

//Arial bold 15
        $this->SetFont('Arial', 'B', 12);
        $this->SetY("12");
        $this->SetX("25");
        $this->Cell(0, 5, utf8_decode("GOVERNO DO ESTADO DO RIO GRANDE DO NORTE"), 0, 1, 'C');

        $this->SetY("17");
        $this->SetX("25");
        $this->Cell(0, 5, utf8_decode( $_SESSION['empresa_nome'] ), 0, 1, 'C');

        $titulo = "RELATÓRIO DE SERVIDORES COM CONSELHOS DE CLASSE";
        $this->SetY("25");
        $this->SetX("25");
        $this->Cell(0, 5, utf8_decode($titulo), 0, 1, 'C');



//$this->Ln(10);
        $this->Ln(3);

        $this->ColumnHeader();
    }

    function ColumnHeader() {
        $opcao = $_REQUEST['opcao'];
//define a fonte a ser usada
        $this->SetFont('Arial', 'B', 10);
        $this->SetX("5");
        $this->Cell(0, 5, utf8_decode("Matrícula"), 0, 0, 'L');

        $this->SetX("23");
        $this->Cell(0, 5, utf8_decode("Nome"), 0, 0, 'L');
        $this->SetX("85");
        $this->Cell(0, 5, utf8_decode("CPF"), 0, 0, 'L');

        $this->SetX("108");
        $this->Cell(0, 5, utf8_decode("Cargo Atual"), 0, 0, 'L');

        $this->SetX("150");
        $this->Cell(0, 5, utf8_decode("Formação"), 0, 0, 'L');

        $this->SetX("190");
        $this->Cell(0, 5, utf8_decode("Lotação"), 0, 0, 'L');

        $this->SetX("230");
        $this->Cell(0, 5, utf8_decode("Conselho"), 0, 0, 'L');

        $this->SetX("260");
        $this->Cell(0, 5, utf8_decode("Número Conselho"), 0, 1, 'L');
    }

    function ColumnDetail() {
//parametros do form
        $conselho = $_REQUEST['conselho'];
        $vinculo = $_REQUEST['vinculo'];
        $situacao = $_REQUEST['situacao'];
        $regiao = $_REQUEST['regiao'];
        $tipo = $_REQUEST['tipo'];
        $ordem = $_REQUEST['ordem'];
// $key = $_REQUEST['key'];

        $this->SetX("20");

// inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

// instancia um repositorio para Carros
        $repository = new TRepository('vw_servidores_relatorioRecord');
// cria um criterio de selecao, ordenado pelo id
        $criteria = new TCriteria;
        $criteria->add(new TFilter('empresa_id', '=', $_SESSION['empresa_id']));
        if ($tipo == '') {
            
        } else {
            if ($tipo == 'TODOS') {
//filtra pelo campo selecionado pelo usu�rio / mes
            } else {
                if ($tipo == 'ATIVOS') {
                    $criteria->add(new TFilter('situacao', '<>', 'APOSENTADO(A)'));
                    $criteria->add(new TFilter('situacao', '<>', 'RESCINDIDO'));
                } else {
                    if ($tipo == 'INATIVOS') {
                        $criteria->add(new TFilter('situacao', '=', 'APOSENTADO(A)', 'and', 'situacao', '=', 'RESCINDIDO'));
// $criteria->add(new TFilter('situacao', '=', 'RESCINDIDO'));
                    }
                }
            }
        }


        if ($regiao == '') {
            
        } else {
            if ($regiao == 'TODAS') {
//filtra pelo campo selecionado pelo usu�rio / mes
                $criteria->setProperty('order', 'regional,nome');
            } else {
//  $criteria->add(new TFilter('regiao', '=', $regiao));
//adiciona o criterio
                $criteria->add(new TFilter('regional', '=', $regiao));
            }
        }


        if ($vinculo == '') {
            
        } else {
            if ($vinculo == 'TODAS') {
//filtra pelo campo selecionado pelo usu�rio / mes
                $criteria->setProperty('order', 'regional, nome,vinculo');
            } else {
//  $criteria->add(new TFilter('regiao', '=', $regiao));
//adiciona o criterio
                $criteria->add(new TFilter('vinculo', '=', $vinculo));
            }
        }

        if ($situacao == '') {
            
        } else {
            if ($situacao == 'TODAS') {
//filtra pelo campo selecionado pelo usu�rio / mes
                $criteria->setProperty('order', 'regional, nome,vinculo');
            } else {
//  $criteria->add(new TFilter('regiao', '=', $regiao));
//adiciona o criterio
                $criteria->add(new TFilter('situacao', '=', $situacao));
            }
        }

        if ($conselho == 'TODAS') {
            if ($ordem == 1) {
                $criteria->add(new TFilter('siglaconselho', 'is not', null));
                $criteria->setProperty('order', 'regional, conselho, nome');
            } else {
                if ($ordem == 2) {
                    $criteria->add(new TFilter('siglaconselho', 'is not', null));
                    $criteria->setProperty('order', 'regional, nome, conselho');
                } else {
                    if ($ordem == 3) {
                        $criteria->add(new TFilter('siglaconselho', 'is not', null));
                        $criteria->setProperty('order', 'regional, matricula, nome, conselho');
                    }
                }
            }
        } else {
            if ($ordem == 1) {
                $criteria->add(new TFilter('siglaconselho', '=', $conselho));
                $criteria->setProperty('order', 'regional, conselho, nome');
            } else {
                if ($ordem == 2) {
                    $criteria->add(new TFilter('siglaconselho', '=', $conselho));
                    $criteria->setProperty('order', 'regional, nome, conselho');
                } else {
                    if ($ordem == 3) {
                        $criteria->add(new TFilter('siglaconselho', '=', $conselho));
                        $criteria->setProperty('order', 'regional, matricula, nome, conselho');
                    }
                }
            }
        }
        $i = 0;

// carrega os objetos de acordo com o criterio
        $rows = $repository->load($criteria);

        if ($rows) {
// percorre os objetos retornados
            if ($regiao = $rows->regional) {
//define a fonte a ser usada
                $this->SetFont('Arial', 'B', 10);
                $this->SetFillColor(162, 234, 150);
                $regiao = $row->regional;
                $posicao;
                $this->SetX("5");
                $this->Cell(0, 5, utf8_decode("Região:      " . ($row->regional)), 1, 1, 'J', 1);
            }
            foreach ($rows as $row) {
//  if ($row->regional == ''){
//  $this->SetFont('arial','',15);
//     $this->SetX("20");
//     $this->Cell(0,5,utf8_decode("Nao existem dados cadastrados para os  parametros desta consulta"),0,1,'J');
//  }
                if ($regiao != $row->regional) {
//define a fonte a ser usada
                    $this->SetFont('Arial', 'B', 10);
                    $this->SetFillColor(162, 234, 150);

                    $regiao = $row->regional;
                    $posicao;
                    $this->SetX("5");
                    $this->Cell(0, 5, utf8_decode("Região:      " . ($row->regional)), 1, 1, 'J', 1);
                }

//define a fonte a ser usada
                $this->SetFont('Arial', '', 9);

                if ($i % 2 == 0) {
                    $this->SetFillColor(235, 235, 235);
                } else {
                    $this->SetFillColor(255, 255, 255);
                }

                $this->SetX("5");
                $this->Cell(0, 5, utf8_decode($row->matricula), 1, 0, 'L', 1);

                $this->SetX("23");
                $this->Cell(0, 5, utf8_decode(substr($row->nome, 0, 30)), 1, 0, 'L', 1);

                $this->SetX("85", 1);
                $this->Cell(0, 5, utf8_decode(substr($row->cpf, 0, 11)), 1, 0, 'L', 1);

                $this->SetX("108", 1);
                $this->Cell(0, 5, utf8_decode(substr($row->cargonovo, 0, 20)), 1, 0, 'L', 1);

                $this->SetX("150", 1);
                $this->Cell(0, 5, utf8_decode(substr($row->formacaorequisito, 0, 20)), 1, 0, 'L', 1);

                $this->SetX("190", 1);
                $this->Cell(0, 5, utf8_decode(substr($row->lotacao, 0, 17)), 1, 0, 'L', 1);

                $this->SetX("230", 1);
                $this->Cell(0, 5, utf8_decode($row->conselho), 1, 0, 'L', 1);

                $this->SetX("260", 1);
                $this->Cell(0, 5, utf8_decode($row->numconselho), 1, 1, 'L', 1);




//contador para gerar total
                $i = ($i + 1);
                $t = ($i);
// $this->AddPage();
            }
        }

        $this->SetFont('arial', 'B', 9);
        $this->SetX("10");
        $this->Cell(0, 5, "Total de Funcionarios: " . $t, 0, 1, 'J');



        TTransaction::close();
    }

//Page footer

    function Footer() {
//Position at 1.5 cm from bottom
        $this->SetY(-15);
//Arial italic 8
        $this->SetFont('Arial', 'I', 8);
//Page number
//data atual
        $data = date("d/m/Y H:i:s");
        $conteudo = "impresso em " . $data;
        $texto = $_SESSION['empresa_sitio'];
//imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
        $this->Cell(0, 0, '', 1, 1, 'L');

//imprime uma celula... largura,altura, texto,borda,quebra de linha, alinhamento
        $this->Cell(0, 5, $texto, 0, 0, 'L');
        $this->Cell(0, 5, $conteudo, 0, 0, 'R');
        $this->Ln();
    }

}

//Instanciation of inherited class
$pdf = new RelatorioConselhodeClasseServidorPDF("L", "mm", "A4");

//define o titulo
$pdf->SetTitle("Relatorio de Servidores");

//assunto
$pdf->SetSubject("Relatorio de Servidores  ");

$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times', '', 12);
$pdf->ColumnDetail();
$file = "app/reports/RelatorioConselhodeClasseServidorPDF" . $_SESSION['servidor_id'] . ".pdf";
//abrir pdf
$pdf->Output($file);
$pdf->openFile($file);
?>
