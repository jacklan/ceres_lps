<?php

use Adianti\Database\TTransaction;
use Adianti\Database\TRepository;
use Adianti\Database\TCriteria;
use Adianti\Database\TFilter;
use Lib\Funcoes\Util;

ini_set('display_errors', 1);
ini_set('display_startup_erros', 1);
error_reporting(E_ALL);

header('Content-Type: text/html; charset=utf-8');
/*
 * classe CertificadoCursoPDF
 * Gerar Certificado particilacao de Curso
 * Autor: Jackson Meires
 * Data:30/09/2016
 */

//class CertificadoCursoPDF extends FPDF {
class CertificadoCursoPDF extends PDF_HTML {

    private $srh_certificado_servidor_id;
    private $servidor_id;
    private $nome_servidor;
    private $codigoverificacao;
    private $conteudo;
    private $dataemissao;
    private $opcao_imagem_certificado;

    //Page header
    function Header() {
        $this->opcao_imagem_certificado = !empty($_REQUEST['opcao_imagem_certificado']) ? $_REQUEST['opcao_imagem_certificado'] : 'off';

        TTransaction::open('pg_ceres'); // inicia transacao com o banco 
        if (empty($_REQUEST['key'])) {

            // instancia um repositorio para Carros
            $repository = new TRepository('SolicitacaoCertificadoRecord');
            $criteria = new TCriteria;
            //filtra pelo campo selecionado pelo usuário
            $criteria->add(new TFilter('id', '=', TSession::getValue('srh_solicitacao_certificado_id')));

            // carrega os objetos de acordo com o criterio
            $rows = $repository->load($criteria);

            if ($rows) {
                // percorre os objetos retornados
                foreach ($rows as $row) {
                    $this->srh_certificado_servidor_id = $row->srh_certificado_servidor_id;
                    $this->servidor_id = $_REQUEST['fk'];
                    $this->nome_servidor = (new ServidorRecord($this->servidor_id))->nome;
                    $this->dataemissao = TDate::date2br($row->dataemissao);
                    $this->codigoverificacao = $row->codigoverificacao;
                }
            }
        } else {
            //verifica se a key vem do MeuCertificado ou se vem da Validacao de documentos
            //$this->srh_certificado_servidor_id = isset($_REQUEST['key']) ? $_REQUEST['key'] : TSession::getValue('srh_solicitacao_certificado_id');
            $this->srh_certificado_servidor_id = $_REQUEST['key'];

            $this->servidor_id = $_REQUEST['fk'];
            $this->nome_servidor = (new ServidorRecord($this->servidor_id))->nome;

            $objectSC = new SolicitacaoCertificadoRecord();

            $objectSC->srh_certificado_servidor_id = $this->srh_certificado_servidor_id;
            $objectSC->matricula = (new ServidorRecord($this->servidor_id))->matricula;
            $this->dataemissao = date('d/m/Y');
            $objectSC->dataemissao = $this->dataemissao;
            $this->codigoverificacao = Util::gerarCodigoAleatorio(10, true, true);
            $objectSC->codigoverificacao = $this->codigoverificacao;
            $objectSC->usuarioalteracao = $_SESSION['usuario'];
            $objectSC->dataalteracao = date("d/m/Y H:i:s");

            $objectSC->store();
        }
        TTransaction::close(); // finalizar transacao com o banco
    }

    function pageDescricao() {


        if ($this->opcao_imagem_certificado == 'on') {
            //endereco da imagem,posicao X(horizontal),posicao Y(vertical), tamanho altura, tamanho largura
//            $this->Image("app/images/certificado/certificado_emater_em_branco_frente.jpg", 1, 1, 297, 210);
        } else {
            //endereco da imagem,posicao X(horizontal),posicao Y(vertical), tamanho altura, tamanho largura
            $this->Image("app/images/certificado/certificado_emater_em_branco_frente.jpg", 1, 1, 297, 210);
        }
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        $object = new CertificadoServidorRecord($this->srh_certificado_servidor_id); //record
        // cria um criterio de selecao, ordenado pelo id
        $criteria = new TCriteria;

        // instancia um repositorio para Carros
        $repository = new TRepository('CertificadoRecord');
        //filtra pelo campo selecionado pelo usuário
        $criteria->add(new TFilter('id', '=', $object->srh_certificado_id));
        $criteria->setProperty('order', 'id');

        // carrega os objetos de acordo com o criterio
        $rows = $repository->load($criteria);

        if ($rows) {
            $content_html = '';

            // percorre os objetos retornados
            foreach ($rows as $row) {

                //define a fonte a ser usada
                $this->SetFont('arial', '', 16);

                $content_html .= utf8_decode($row->descricao);
                $this->conteudo .= utf8_decode($row->conteudo);

                $this->setXY(10, 70);
                $this->Multicell(190, 8, str_replace('{participante}', $this->nome_servidor, $content_html), 'J');

                $this->Image("app/images/assinaturas_certificados/assinatura_catia_diretora_emater.jpg", 20, 135, 65, 25);
                $this->setXY(16, 148);
                $this->Multicell(70, 7, utf8_decode(" __________________ \n Diretora da Emater-RN "), 'C');

                if ($row->assinatura_coordenador == "LEILA MESQUITA") {
                    
                    $this->Image("app/images/assinaturas_certificados/assinatura_leila_mesquita.jpg", 122, 135, 65, 25);
                    $this->setXY(115, 148);
                    $this->Multicell(140, 7, utf8_decode("        __________________ \nLeila Daniele F.da Silva Mesquita "), 'C');
                    
                } else if ($row->assinatura_coordenador == "ADRIANA AMERICO DE SOUZA") {
                    
                    $this->Image("app/images/assinaturas_certificados/assinatura_adriana_americo.jpg", 130, 135, 65, 25);
                    $this->setXY(125, 148);
                    $this->Multicell(140, 7, utf8_decode("    ____________________ \n   Adriana Americo de Souza "), 'C');
                    
                } else if ($row->assinatura_coordenador == "ELTON DANTAS DE OLIVEIRA") {
                    
                    $this->Image("app/images/assinaturas_certificados/assinatura_elton_oliveira.jpg", 130, 135, 65, 25);
                    $this->setXY(125, 148);
                    $this->Multicell(140, 7, utf8_decode("    ____________________ \n   Elton Dantas de Oliveira "), 'C');
              
                } else if ($row->assinatura_coordenador == "ARIAMELIA BANDEIRA CRUZ FEITOSA") {
                    
                    $this->Image("app/images/assinaturas_certificados/assinatura_ariamelia_bandeira_cruz_feitosa.jpg", 130, 137, 65, 25);
                    $this->setXY(125, 148);
                    $this->Multicell(140, 7, utf8_decode("    ____________________ \nAriamelia Bandeira Cruz Feitosa "), 'C');
                    
                }

                $this->SetFont('arial', 'B', 8);
                $this->setXY(75, 170);
                $this->Multicell(70, 4, utf8_decode(" Código de verificação: " . $this->codigoverificacao . "\n Data de emissão: " . $this->dataemissao), '1', 'C');

                $this->Ln(5);
                $this->setXY(30, 180);
                $this->Multicell(160, 4, utf8_decode("Para verificar a autenticidade deste documento acesse http://ceres.rn.gov.br/certificado, informando o código de verificação."), 0, 'C');
                // $this->Multicell(160, 4, utf8_decode("Para verificar a autenticidade deste documento acesse http://ceres.rn.gov.br/v2/validar, informando a matrícula, data de emissão do documento e o código de verificação."), 0, 'C');
            }
        } else {
            $this->SetFont('arial', 'b', 15);
            $this->setXY(10, 90);
            $this->Cell(0, 5, utf8_decode("Nao certificado existem para o  parametro desta consulta"), 0, 1, 'J');
            $this->Ln();
        }

        TTransaction::close();
    }

    function pageConteudo() {

        if ($this->opcao_imagem_certificado == 'on') {
            //endereco da imagem,posicao X(horizontal),posicao Y(vertical), tamanho altura, tamanho largura
//            $this->Image("app/images/certificado/certificado_emater_em_branco_frente.jpg", 1, 1, 297, 210);
        } else {
            //endereco da imagem,posicao X(horizontal),posicao Y(vertical), tamanho altura, tamanho largura
            $this->Image("app/images/certificado/certificado_emater_em_branco_verso.jpg", 1, 1, 297, 210);
        }
        $this->setXY(15, 45);
        $this->Multicell(0, 5, $this->WriteHtmlCell(200, $this->conteudo), 'J');
    }

//Page footer
    function Footer() {
        
    }

}

//echo $this->content;
$pdf = new CertificadoCursoPDF('L', 'mm', 'A4');
$pdf->AddPage();
$pdf->SetFont('Arial');
//$pdf->WriteHTML2($pdf->pageDescricao(), true);
$pdf->pageDescricao();
$pdf->AddPage();
$pdf->pageConteudo();

$file = "app/reports/CertificadoCursoPDF" . $_SESSION['servidor_id'] . ".pdf";
//abrir pdf
$pdf->Output($file);
$pdf->openFile($file);
