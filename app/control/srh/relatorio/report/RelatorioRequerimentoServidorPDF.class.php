<?php

//ini_set('display_errors', 1);
//ini_set('display_startup_erros', 1);
//error_reporting(E_ALL);

/*
 * classe RelatorioRequerimentoServidorPDF
 * Autor: Jackson Meires
 * Data:08/12/2016
 */

use Adianti\Database\TTransaction;
use Adianti\Database\TRepository;
use Adianti\Database\TCriteria;
use Adianti\Database\TFilter;

class RelatorioRequerimentoServidorPDF extends FPDF {

    //Page header
    function Header() {
        $this->SetFont('Arial', 'B', 14);

        $this->SetX("10");
        $this->Cell(0, 7, utf8_decode("REQUERIMENTO DO SERVIDOR"), 1, 1, 'C');
    }

    function ColumnDetail() {

        $matricula = $_REQUEST['matricula'];
        $empresa_id = $_SESSION['empresa_id'];

        TTransaction::open('pg_ceres');
        $repository = new TRepository('ServidorRecord');
        $criteria = new TCriteria;
        if (is_numeric($matricula)) {
            $criteria->add(new TFilter('matricula', '=', $matricula));
            $criteria->add(new TFilter('situacao', '=', 'EM ATIVIDADE'));
            $criteria->add(new TFilter('empresa_id', '=', $_SESSION['empresa_id']));
        }

        $objects = $repository->load($criteria);

        if ($objects) {

            foreach ($objects as $object) {

                $this->SetFont('Arial', '', 12);

                $text = utf8_decode("Autoridade a que é dirigida:\n" . $_SESSION['empresa_nome']);
                $this->MultiCell(0, 6, $text, 1, 'J');

                $this->SetFont('Arial', '', 10);
                $this->SetX(10);
                $this->MultiCell(140, 7, utf8_decode("Requerente:\n" . $object->nome), 1, 'L');

                $this->SetXY(150, 29);
                $this->MultiCell(50, 7, utf8_decode("Matricula:\n" . $object->matricula . " "), 1, 'L');

                $this->SetXY(10, 43);
                $this->MultiCell(190, 7, utf8_decode("Endereço:\n" . $object->endereco . " "), 1, 'L');

                $this->SetXY(10, 57);
                $this->MultiCell(140, 7, utf8_decode("Cargo ou Função\n" . $object->nome_cargonovo . " "), 1, 'L');

                $this->SetXY(150, 57);
                $this->MultiCell(50, 7, utf8_decode("Data Admissão:\n" . TDate::date2br($object->dataadmissao) . " "), 1, 'L');

                $this->SetXY(10, 71);
                $this->MultiCell(140, 7, utf8_decode("Objetivo Requerimento:"), 1, 'L');
                $this->SetXY(150, 71);
                $this->MultiCell(50, 7, utf8_decode("Base Legal:"), 1, 'L');

                $this->SetXY(10, 78);
                $this->MultiCell(190, 7, utf8_decode("Outros dados:\n"), 0, 'L');

                $this->SetXY(10, 78);
                $this->MultiCell(190, 40, utf8_decode(""), 1, 'L');

                $this->SetXY(10, 118);
                $this->MultiCell(100, 5, utf8_decode("Tendo anexado os documentos para o devido processamento solicito a concessão do pedido constante do presente requerimento"), 0, 'J');

                $this->SetXY(10, 118);
                $this->MultiCell(100, 40, utf8_decode(""), 1, 'L');

                $this->SetFont('Arial', 'b', 12);
                $this->SetXY(110, 118);
                $this->MultiCell(90, 7, utf8_decode("RESERVADO AO CHEFE IMEDIATO"), 0, 'C');

                $this->SetFont('Arial', '', 10);
                $this->SetXY(110, 118);
                $this->MultiCell(90, 40, utf8_decode(""), 1, 'L');

                $this->SetXY(120, 125);
                $this->Image("app/images/icons/quadrado.jpg", 115, 126, 5, 5);
                $this->MultiCell(90, 7, utf8_decode("Nada a por"), 0, 'L');
                $this->SetXY(120, 132);
                $this->Image("app/images/icons/quadrado.jpg", 115, 133, 5, 5);
                $this->MultiCell(90, 7, utf8_decode("Discordo _____________"), 0, 'L');

                $this->SetXY(110, 139);
                $this->MultiCell(90, 7, utf8_decode("Natal       /    /   "), 0, 'C');

                $this->SetXY(110, 148);
                $this->MultiCell(90, 5, utf8_decode("________________________________\nAssinatura"), 0, 'C');

                $this->SetXY(33, 131);
                $this->MultiCell(50, 5, utf8_decode("NESTES TERMOS PEDE DEFERIMENTO"), 0, 'C');

                $this->SetXY(10, 140);
                $this->MultiCell(90, 7, utf8_decode("Natal       /    /   "), 0, 'C');

                $this->SetXY(10, 148);
                $this->MultiCell(90, 5, utf8_decode("________________________________\nAssinatura"), 0, 'C');

                $this->SetFont('Arial', 'b', 12);
                $this->SetXY(10, 173);
                $this->MultiCell(90, 7, utf8_decode("AUTORIZAÇÃO CHEFE DE GABINETE"), 0, 'C');

                $this->SetXY(107, 173);
                if ($empresa_id == 1) {
                    $this->MultiCell(90, 7, utf8_decode("DESPACHO DA UIRH/EMATER"), 0, 'C');
                } elseif ($empresa_id == 5) {
                    $this->MultiCell(90, 7, utf8_decode("DESPACHO DA UIAG/SAPE"), 0, 'C');
                }

                $this->SetFont('Arial', '', 10);
                $this->SetXY(107, 179);
                if ($empresa_id == 1) {
                    $this->MultiCell(90, 5, utf8_decode("Ao setor de Recursos Humanos da Enater para providencias."), 0, 'C');
                } elseif ($empresa_id == 5) {
                    $this->MultiCell(90, 5, utf8_decode("Ao setor de Recursos Humanos da Sape para providencias."), 0, 'C');
                }

                $this->SetLineWidth(0.2);
                $this->Line(12, 210, 103, 210);
                $this->SetXY(10, 210);

                if ($empresa_id == 1) {
                    $this->MultiCell(90, 7, utf8_decode("Chefe de Gabinete da EMATER"), 0, 'C');
                } elseif ($empresa_id == 5) {
                    $this->MultiCell(90, 7, utf8_decode("Chefe de Gabinete da SAPE"), 0, 'C');
                }

                $this->Line(110, 210, 198, 210);
                $this->SetXY(110, 210);

                if ($empresa_id == 1) {
                    $this->MultiCell(90, 7, utf8_decode("Chefe do UIRH/EMATER"), 0, 'C');
                } elseif ($empresa_id == 5) {
                    $this->MultiCell(90, 7, utf8_decode("Chefe da UIAG/SAPE"), 0, 'C');
                }

                $this->SetXY(60, 222);
                $this->MultiCell(90, 7, utf8_decode("Observações"), 0, 'C');
            }
        } else {
            $this->SetFont('Arial', '', 12);
            $this->SetXY(30, 20);
            $this->MultiCell(150, 5, utf8_decode("Não existe nenhum dado com o parametro da pesquisa informado, por favor tente novamente com outra matricula."), 0, 'C');
        }
        TTransaction::close();
    }

}

$pdf = new RelatorioRequerimentoServidorPDF("P", "mm", "A4");

$pdf->SetTitle("Requerimento do Servidor");
$pdf->SetSubject("Requerimento do Servidor");

$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times', '', 12);
$pdf->ColumnDetail();

$file = "app/reports/RelatorioRequerimentoServidorPDF" . $_SESSION['servidor_id'] . ".pdf";

$pdf->Output($file);
$pdf->openFile($file);
