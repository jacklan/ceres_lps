<?php

use Adianti\Database\TTransaction;
use Adianti\Database\TRepository;
use Adianti\Database\TCriteria;
use Adianti\Database\TFilter;

//ini_set('display_errors', 1);
//ini_set('display_startup_erros', 1);
//error_reporting(E_ALL);

class RelatorioAniversariantesPDF extends FPDF {

    //Page header
    function Header() {

        $mes = $_REQUEST['mes'];

        //endereco da imagem,posicao X(horizontal),posicao Y(vertical), tamanho altura, tamanho largura
        $this->Image("app/images/logo_relatorio.jpg", 8, 11, 18, 18);

        //Arial bold 15
        $this->SetFont('Arial', 'B', 12);
        $this->SetY("12");
        $this->SetX("25");
        $this->Cell(0, 5, utf8_decode("GOVERNO DO ESTADO DO RIO GRANDE DO NORTE"), 0, 1, 'C');

        $this->SetY("17");
        $this->SetX("25");
        $this->Cell(0, 5, utf8_decode($_SESSION['empresa_nome']), 0, 1, 'C');

        if ($mes == 1) {
            $titmes = "ANIVERSARIANTES DO MÊS DE JANEIRO";
        } elseif ($mes == 2) {
            $titmes = "ANIVERSARIANTES DO MÊS DE FEVEREIRO";
        } elseif ($mes == 3) {
            $titmes = "ANIVERSARIANTES DO MÊS DE MARÇO";
        } elseif ($mes == 4) {
            $titmes = "ANIVERSARIANTES DO MÊS DE ABRIL";
        } elseif ($mes == 5) {
            $titmes = "ANIVERSARIANTES DO MÊS DE MAIO";
        } elseif ($mes == 6) {
            $titmes = "ANIVERSARIANTES DO MÊS DE JUNHO";
        } elseif ($mes == 7) {
            $titmes = "ANIVERSARIANTES DO MÊS DE JULHO";
        } elseif ($mes == 8) {
            $titmes = "ANIVERSARIANTES DO MÊS DE AGOSTO";
        } elseif ($mes == 9) {
            $titmes = "ANIVERSARIANTES DO MÊS DE SETEMBRO";
        } elseif ($mes == 10) {
            $titmes = "ANIVERSARIANTES DO MÊS DE OUTUBRO";
        } elseif ($mes == 11) {
            $titmes = "ANIVERSARIANTES DO MÊS DE NOVEMBRO";
        } elseif ($mes == 12) {
            $titmes = "ANIVERSARIANTES DO MÊS DE DEZEMBRO";
        }

        $regional = $_REQUEST['regional'];
        $nomeregiao = '';
        if ($regional > 0) {

            TTransaction::open('pg_ceres');
            $repository = new TRepository('RegionalRecord');
            $criteria = new TCriteria;
            $criteria->add(new TFilter('id', '=', $regional));
            $collection = $repository->load($criteria);
            foreach ($collection as $object) {
                $nomeregiao = $object->nome;
            }
            //coloca o valor padrao no combo
            TTransaction::close();

            //colocar no titulo o nome da regiao
            $titmes = ' REGIONAL:' . $nomeregiao;
        }

        //define a fonte a ser usada
        $this->SetFont('Courier', 'B', 18);
        $titulo = $titmes;
        $this->SetY("25");
        $this->SetX("15");
        $this->Cell(0, 5, utf8_decode($titulo), 0, 1, 'C');
        $this->Ln(3);

        $this->ColumnHeader();
    }

    function ColumnHeader() {

        //define a fonte a ser usada
        $this->SetFont('Arial', 'B', 12);
        $this->SetFillColor(162, 234, 150);

        $this->SetX("7");
        $this->Cell(0, 5, utf8_decode("Dia"), 1, 0, 'L', 1);


        $this->SetX("15");
        $this->Cell(0, 5, utf8_decode("Nome"), 1, 0, 'L', 1);


        $this->SetX("110");
        $this->Cell(0, 5, utf8_decode("Cidade"), 1, 0, 'L', 1);

        $this->SetX("190");
        $this->Cell(0, 5, utf8_decode("Telefone"), 1, 0, 'L', 1);

        $this->SetX("220");
        $this->Cell(0, 5, utf8_decode("Email"), 1, 1, 'L', 1);
    }

    function ColumnDetail() {
        $mes = $_REQUEST['mes'];
        $opcao = $_REQUEST['opcao'];
        $regional = $_REQUEST['regional'];

        $this->SetX("20");

        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instancia um repositorio para Carros
        $repository = new TRepository('vw_aniversariantesRecord');
        // cria um criterio de selecao, ordenado pelo id
        $criteria = new TCriteria;
        $criteria->add(new TFilter('empresa_id', '=', $_SESSION['empresa_id']));
        $criteria->add(new TFilter('mes', '=', $mes));

        if ($opcao == 'SIM') {
            $criteria->add(new TFilter('situacao', '=', 'EM ATIVIDADE'));
            //$criteria->add(new TFilter('tipofuncionario', '=', 'EFETIVO'));
        }
        if ($regional != 0) {
            $criteria->add(new TFilter('regional_id', '=', $regional));
        }
        $criteria->setProperty('order', 'dia, nome');

        // carrega os objetos de acordo com o criterio
        $rows = $repository->load($criteria);
        $i = 0;
        if ($rows) {
            // percorre os objetos retornados
            foreach ($rows as $row) {
                if ($i % 2 == 0) {
                    $this->SetFillColor(235, 235, 235);
                } else {
                    $this->SetFillColor(255, 255, 255);
                }
                $this->SetX("7");
                $this->Cell(0, 5, utf8_decode($row->dia), 1, 0, 'L', 1);

                $this->SetX("15");
                $this->Cell(0, 5, utf8_decode(substr($row->nome, 0, 35)), 1, 0, 'L', 1);

                $this->SetX("110");
                $this->Cell(0, 5, utf8_decode(substr($row->cidade, 0, 25)), 1, 0, 'L', 1);

                $this->SetX("190");
                $this->Cell(0, 5, utf8_decode(substr($row->telefone, 0, 15)), 1, 0, 'L', 1);

                $this->SetX("220");
                $this->Cell(0, 5, utf8_decode(substr($row->email, 0, 30)), 1, 1, 'L', 1);
                $i++;
            }
        }


        TTransaction::close();
    }

    //Page footer
    function Footer() {

        //Position at 1.5 cm from bottom
        $this->SetY(-15);
        //Arial italic 8
        $this->SetFont('Arial', 'I', 8);
        //Page number
        //data atual
        $data = date("d/m/Y H:i:s");
        $conteudo = "impresso em " . $data;
        $texto = $_SESSION['empresa_sitio'];
        //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
        $this->Cell(0, 0, '', 1, 1, 'L');

        //imprime uma celula... largura,altura, texto,borda,quebra de linha, alinhamento
        $this->Cell(0, 5, $texto, 0, 0, 'L');
        $this->Cell(0, 5, $conteudo, 0, 0, 'R');
        $this->Ln();
    }

}

$pdf = new RelatorioAniversariantesPDF("L", "mm", "A4");

//define o titulo
$pdf->SetTitle(utf8_decode("Relatorio dos Aniversariantes"));

//assunto
$pdf->SetSubject(utf8_decode("Relatorio dos Aniversariantes"));

$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times', '', 12);
$pdf->ColumnDetail();
$file = "app/reports/RelatorioAniversariantesPDF" . $_SESSION['servidor_id'] . ".pdf";

//abrir pdf
$pdf->Output($file);
$pdf->openFile($file);
?>