<?php

ini_set('display_errors', 1);
ini_set('display_startup_erros', 1);
error_reporting(E_ALL);

use Adianti\Database\TTransaction;
use Adianti\Database\TRepository;
use Adianti\Database\TCriteria;
use Adianti\Database\TFilter;

class RelatorioDashboardAposentadoriaMunicipioFormacaoPDF extends FPDF {

//Page header
    function Header() {
        $requisito_cargo = $_REQUEST['nome_requisito_cargo'];
        //endereco da imagem,posicao X(horizontal),posicao Y(vertical), tamanho altura, tamanho largura
        $this->Image("app/images/logo_relatorio.jpg", 8, 11, 26, 18);

        //Arial bold 15
        $this->SetFont('Arial', 'B', 12);
        $this->SetY("12");
        $this->SetX("35");
        $this->Cell(0, 5, utf8_decode("GOVERNO DO ESTADO DO RIO GRANDE DO NORTE"), 0, 1, 'J');

        $this->SetY("17");
        $this->SetX("35");
        $this->Cell(0, 5, utf8_decode("INSTITUTO DE ASSISTENCIA TECNICA E EXTENSAO RURAL - EMATER"), 0, 1, 'J');

        $this->SetY("22");
        $this->SetX("35");
        $this->Cell(0, 5, utf8_decode("UNIDADE INSTRUMENTAL DE RECURSOS HUMANOS - UIRH"), 0, 1, 'J');




        $this->Ln(3);

        $this->ColumnHeader();
    }

    function ColumnHeader() {
        //define a fonte a ser usada
        $this->SetFont('arial', 'B', 9);
        $this->SetY("35");
        $this->SetX("8");
        $this->Cell(0, 5, "MATRICULA", 0, 1, 'J');
        $this->SetY("35");
        $this->SetX("30");
        $this->Cell(0, 5, "NOME", 0, 1, 'J');
        $this->SetY("35");
        $this->SetX("105");
        $this->Cell(0, 5, "CPF", 0, 1, 'J');
        $this->SetY("35");
        $this->SetX("130");
        $this->Cell(0, 5, "TELEFONE", 0, 1, 'J');
        $this->SetY("35");
        $this->SetX("150");
        $this->Cell(0, 5, "E-MAIL", 0, 1, 'J');

        $this->Cell(0, 0, '', 1, 1, 'L');
        $this->Ln();
    }

    function ColumnDetail() {

        $municipio_id = $_REQUEST['municipio_id'];
        $requisitocargo_id = $_REQUEST['requisitocargo_id'];

        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // cria um criterio de selecao, ordenado pelo id
        $criteria = new TCriteria;

        // instancia um repositorio 
        $repository = new TRepository('vw_dashboard_servidores_lotacao_atual_municipio_form_relatorioRecord');
        $criteria->add(new TFilter('requisitocargo_id', '=', $requisitocargo_id));
        $criteria->add(new TFilter('municipio_id', '=', $municipio_id));
        $criteria->setProperty('order', 'nome');
        // carrega os objetos de acordo com o criterio
        $cadastros = $repository->load($criteria);
//        }


        $this->SetY(42);

        $i = 0;

        // carrega os objetos de acordo com o criterio
        $rows = $repository->load($criteria);

        if ($rows) {
            // percorre os objetos retornados
            foreach ($rows as $row) {

                // adiciona os dados do perfil do usuario no menu
                if ($row->nome == '') {
                    $this->SetFont('arial', '', 15);
                    $this->SetX("15");
                    $this->Cell(0, 5, utf8_decode("Nao existem dados cadastrados para os  parametros desta consulta"), 0, 1, 'J');
                    $this->Ln();
                }

                //define a fonte a ser usada
                $this->SetFont('arial', '', 9);


                $this->SetX("8");
                $this->Cell(0, 5, utf8_decode($row->matricula), 0, 0, 'J');

                $this->SetX("30");
                $this->Cell(0, 5, utf8_decode(substr($row->nome, 0, 35)), 0, 0, 'J');

                $this->SetX("105");
                $this->Cell(0, 5, utf8_decode($row->cpf), 0, 0, 'J');

                $this->SetX("130");
                $this->Cell(0, 5, utf8_decode(substr($row->telefone, 0, 9)), 0, 0, 'J');

                $this->SetX("150");
                $this->Cell(0, 5, utf8_decode(substr($row->email, 0, 30)), 0, 0, 'J');


                $this->Ln();

                //contador para gerar total
                $i = ($i + 1);
                $t = ($i);
                $nome_mun = $row->nome_municipio;
                $nome_req = $row->nome_requisitocargo;
            }
            $this->Ln(5);
            $this->Cell(0, 0, '', 1, 1, 'L');
            $this->Ln();
            $this->SetFont('arial', 'B', 9);
            $this->SetX("10");
            $this->Cell(0, 5, "TOTAL: " . $t . " servidor(es) apto(s) a aposentadoria. ", 0, 1, 'J');
        }

        TTransaction::close();



        $this->SetFont('arial', 'B', 12);
        $this->SetY("27");
        $this->SetX("35");
        $this->Cell(0, 5, utf8_decode("MUNICIPIO: " . $nome_mun . ". FORMACAO: " . $nome_req . "."), 0, 1, 'J');
    }

//Page footer
    function Footer() {
        //Position at 1.5 cm from bottom
        $this->SetY(-15);
        //Arial italic 8
        $this->SetFont('Arial', 'I', 8);
        //Page number
        //data atual
        $data = date("d/m/Y H:i:s");
        $conteudo = "impresso em " . $data;
        $texto = "http://www.emater.rn.gov.br";
        //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
        $this->Cell(0, 0, '', 1, 1, 'L');

        //imprime uma celula... largura,altura, texto,borda,quebra de linha, alinhamento
        $this->Cell(0, 5, $texto, 0, 0, 'L');
        $this->Cell(0, 5, 'Pag. ' . $this->PageNo() . ' de ' . '{nb}' . ' - ' . $conteudo, 0, 0, 'R');
        $this->Ln();
    }

}

//Instanciation of inherited class
$pdf = new RelatorioDashboardAposentadoriaMunicipioFormacaoPDF("P", "mm", "A4");
//$pdf=new PDF();
//instacia a classe FPDF
//$pdf= new FPDF("P","mm","A4");
//define o titulo
$pdf->SetTitle("Relatorio Aposentadoria Por Municipio e Cargo - EMATER-RN");

//assunto
$pdf->SetSubject("Relatorio Aposentadoria Por Municipio e Cargo - EMATER-RN");

$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times', '', 12);
$pdf->ColumnDetail();
$file = "app/reports/RelatorioDashboardAposentadoriaMunicipioFormacaoPDF" . $_SESSION['servidor_id'] . ".pdf";
//abrir pdf
$pdf->Output($file);
$pdf->openFile($file);

