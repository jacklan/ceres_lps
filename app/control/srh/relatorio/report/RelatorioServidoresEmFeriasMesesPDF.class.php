<?php

/*
 * class: RelatorioServidoresEmFeriasMesesPDF
 * Autor:Jackson Meires
 * Data:04/02/2016
 */

use Adianti\Database\TTransaction;
use Adianti\Database\TRepository;
use Adianti\Database\TCriteria;
use Adianti\Database\TFilter;

class RelatorioServidoresEmFeriasMesesPDF extends FPDF {

//Page header
    function Header() {

        //endereco da imagem,posicao X(horizontal),posicao Y(vertical), tamanho altura, tamanho largura
        $this->Image("app/images/logo_relatorio.jpg", 8, 11, 18, 18);

        $regional_id = $_REQUEST['regional_id'];
        if ($regional_id == 0) {
            $nomeregional = "TODAS";
        } else {
            // inicia transacao com o banco 'pg_ceres'
            TTransaction::open('pg_ceres');
            $nomeregional = (new RegionalRecord($regional_id))->nome;
            // finalizar transacao com o banco 'pg_ceres'
            TTransaction::close();
        }

        //Arial bold 15
        $this->SetFont('Arial', 'B', 12);
        $this->SetY("12");
        $this->SetX("25");
        $this->Cell(0, 5, utf8_decode("GOVERNO DO ESTADO DO RIO GRANDE DO NORTE"), 0, 1, 'C');

        $this->SetY("17");
        $this->SetX("25");
        $this->Cell(0, 5, utf8_decode($_SESSION['empresa_nome']), 0, 1, 'C');

        //define a fonte a ser usada
        $this->SetFont('Courier', 'B', 18);
        $titulo = "SERVIDORES EM FÉRIAS " . $_REQUEST['datainicio'] . " - " . $_REQUEST['datafim'] . " REGIONAL: " . $nomeregional;

        $this->SetY("25");
        $this->SetX("15");
        $this->Cell(0, 5, utf8_decode($titulo), 0, 1, 'C');

        $this->SetX("15");
        $this->Cell(0, 5, utf8_decode('EXERCICIO: ' . $_REQUEST['ano']), 0, 1, 'C');
        $this->Ln(3);

        $this->ColumnHeader();
    }

    function ColumnHeader() {

        //define a fonte a ser usada
        $this->SetFont('Arial', 'B', 12);
        $this->SetFillColor(162, 234, 150);
        $this->SetX("7");
        $this->Cell(0, 5, utf8_decode("Matrícula"), 1, 0, 'L', 1);

        $this->SetX("33");
        $this->Cell(0, 5, utf8_decode("Nome"), 1, 0, 'L', 1);

        $this->SetX("130");
        $this->Cell(0, 5, utf8_decode("Lotação"), 1, 0, 'L', 1);

        $this->SetX("220");
        $this->Cell(0, 5, utf8_decode("Início Gozo"), 1, 0, 'L', 1);

        $this->SetX("245");
        $this->Cell(0, 5, utf8_decode("Fim Gozo"), 1, 1, 'L', 1);

//        $this->SetX("245");
//        $this->Cell(0, 5, utf8_decode("Exercício"), 1, 1, 'L');
    }

    function ColumnDetail() {

        $ano = $_REQUEST['ano'];
        $regional_id = $_REQUEST['regional_id'];

        //pegar datas da sessao converter para padrao america e remover o tra�o da data
        $datainicio = str_replace("-", "", TDate::date2us($_REQUEST['datainicio']));
        $datafim = str_replace("-", "", TDate::date2us($_REQUEST['datafim']));

        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instancia um repositorio para Carros
        $repository = new TRepository('vw_servidores_em_ferias_mesRecord');
        // cria um criterio de selecao, ordenado pelo id
        $criteria = new TCriteria;
        $criteria->add(new TFilter('exercicio', '=', $ano));
        $criteria->add(new TFilter('empresa_id', '=', $_SESSION['empresa_id']));

        if ($regional_id > 0) {
            $criteria->add(new TFilter('regional_id', '=', $regional_id));
        }

        $criteria->add(new TFilter("inicio", ">=", $datainicio), TExpression::AND_OPERATOR);
        $criteria->add(new TFilter("fim", "<=", $datafim), TExpression::AND_OPERATOR);

        //$criteria->setProperty('order', 'nome, fimgozo desc');
        $criteria->setProperty('order', 'nome, mesfim desc, iniciogozo');

        // carrega os objetos de acordo com o criterio
        $rows = $repository->load($criteria);

        if ($rows) {
            $i = 0;
            // percorre os objetos retornados
            foreach ($rows as $row) {

                if ($i % 2 == 0) {
                    $this->SetFillColor(235, 235, 235);
                } else {
                    $this->SetFillColor(255, 255, 255);
                }
                $this->SetX("7");
                $this->Cell(0, 5, utf8_decode($row->matricula), 1, 0, 'L', 1);

                $this->SetX("33");
                $this->Cell(0, 5, utf8_decode(substr($row->nome, 0, 35)), 1, 0, 'L', 1);

                $this->SetX("130");
                $this->Cell(0, 5, utf8_decode(substr($row->lotacao, 0, 30)), 1, 0, 'L', 1);

                $this->SetX("220");
                $this->Cell(0, 5, utf8_decode(substr($row->iniciogozo, 0, 14)), 1, 0, 'L', 1);

                $this->SetX("245");
                $this->Cell(0, 5, utf8_decode(substr($row->fimgozo, 0, 14)), 1,1, 'L', 1);

//                $this->SetX("245");
//                $this->Cell(0, 5, utf8_decode(substr($row->exercicio, 0, 4)), 1, 1, 'L');
                $i++;
            }
            $this->SetX("7");
            //imprimindo a linha
            $this->Cell(0, 0, '', 1, 1, 'L');
            $this->SetFont('Arial', 'B', 13);
            $this->SetX("7");
            $this->Cell(0, 10, utf8_decode("TOTAL: "), 0, 0, 'L');

            $this->SetX("25");
            $this->Cell(0, 10, utf8_decode(substr($i, 0, 4)), 0, 0, 'L');
        }


        TTransaction::close();
    }

//Page footer
    function Footer() {
        //Position at 1.5 cm from bottom
        $this->SetY(-15);
        //Arial italic 8
        $this->SetFont('Arial', 'I', 8);
        //Page number
        //data atual
        $data = date("d/m/Y H:i:s");
        $conteudo = "impresso em " . $data;
        $texto = $_SESSION['empresa_sitio'];
        //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
        $this->Cell(0, 0, '', 1, 1, 'L');

        //imprime uma celula... largura,altura, texto,borda,quebra de linha, alinhamento
        $this->Cell(0, 5, $texto, 0, 0, 'L');
        $this->Cell(0, 5, $conteudo, 0, 0, 'R');
        $this->Ln();
    }

}

//Instanciation of inherited class
$pdf = new RelatorioServidoresEmFeriasMesesPDF("L", "mm", "A4");

//define o titulo
$pdf->SetTitle("Relatorio de Servidores em F&eacute;rias ");

//assunto
$pdf->SetSubject("Relatorio de Servidores em F&eacute;rias ");

$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times', '', 12);
$pdf->ColumnDetail();
$file = "app/reports/RelatorioServidoresEmFeriasMesesPDF" . $_SESSION['servidor_id'] . ".pdf";
//abrir pdf
$pdf->Output($file);
$pdf->openFile($file);
