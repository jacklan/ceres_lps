<?php

use Adianti\Database\TTransaction;
use Adianti\Database\TRepository;
use Adianti\Database\TCriteria;
use Adianti\Database\TFilter;

class RelatorioServidoresLicencaMesPDF extends FPDF {

//Page header
    function Header() {
        //endereco da imagem,posicao X(horizontal),posicao Y(vertical), tamanho altura, tamanho largura
        $this->Image("app/images/logo_relatorio.jpg", 8, 11, 18, 18);

        //Arial bold 15
        $this->SetFont('Arial', 'B', 12);
        $this->SetY("12");
        $this->SetX("25");
        $this->Cell(0, 5, utf8_decode("GOVERNO DO ESTADO DO RIO GRANDE DO NORTE"), 0, 1, 'C');

        $this->SetY("17");
        $this->SetX("25");
        $this->Cell(0, 5, utf8_decode( $_SESSION['empresa_nome'] ), 0, 1, 'C');

        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        $tipolicenca = new TipoLicencaRecord($_REQUEST['tipolicenca_id']);
        $nometipolicenca = $tipolicenca->nome;

        TTransaction::close();

        //define a fonte a ser usada
        $this->SetFont('Courier', 'B', 18);
        $titulo = "SERVIDORES DE LICENÇA - " . $_REQUEST['mes'] . "/" . $_REQUEST['ano'] . " - " . $nometipolicenca;
        $this->SetY("25");
        $this->SetX("15");
        $this->Cell(0, 5, utf8_decode($titulo), 0, 1, 'C');
        $this->Ln(3);

        $this->ColumnHeader();
    }

    function ColumnHeader() {

        //define a fonte a ser usada
        $this->SetFont('Arial', 'B', 12);
        $this->SetFillColor(162, 234, 150);

        $this->SetX("7");
        $this->Cell(0, 5, utf8_decode("Matrícula"), 1, 0, 'L', 1);


        $this->SetX("33");
        $this->Cell(0, 5, utf8_decode("Nome"), 1, 0, 'L', 1);


        $this->SetX("110");
        $this->Cell(0, 5, utf8_decode("Lotação"), 1, 0, 'L', 1);

        $this->SetX("165");
        $this->Cell(0, 5, utf8_decode("Início"), 1, 0, 'L', 1);

        $this->SetX("190");
        $this->Cell(0, 5, utf8_decode("Fim"), 1, 0, 'L', 1);


        $this->SetX("215");
        $this->Cell(0, 5, utf8_decode("Tipo Licenca"), 1, 1, 'L', 1);
    }

    function ColumnDetail() {

        $this->SetX("20");

        $ano = $_REQUEST['ano'];
        $mes = $_REQUEST['mes'];
        $tipolicenca = $_REQUEST['tipolicenca_id'];

        $anomes = $_REQUEST['ano'] . $_REQUEST['mes'];
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instancia um repositorio para Carros
        $repository = new TRepository('vw_servidores_licencaRecord');
        // cria um criterio de selecao, ordenado pelo id
        $criteria = new TCriteria;
        if ($tipolicenca != '0') {
            $criteria->add(new TFilter('tipolicenca_id', '=', $tipolicenca));
        }

        $criteria->add(new TFilter('anomesinicio', '<=', $anomes));
        $criteria->add(new TFilter('anomesfim', '>=', $anomes));
        $criteria->add(new TFilter('empresa_id', '=', $_SESSION['empresa_id']));

        // carrega os objetos de acordo com o criterio
        $rows = $repository->load($criteria);

        if ($rows) {
            $i = 0;
            // percorre os objetos retornados
            foreach ($rows as $row) {
                // adiciona os dados do perfil do usuario no menu
                if ($row) {

                    if ($i % 2 == 0) {
                        $this->SetFillColor(235, 235, 235);
                    } else {
                        $this->SetFillColor(255, 255, 255);
                    }

                    $this->SetFont('Arial', '', 9);
                    $this->SetX("7");
                    $this->Cell(0, 5, utf8_decode($row->matricula), 1, 0, 'L', 1);

                    $this->SetX("33");
                    $this->Cell(0, 5, utf8_decode(substr($row->nome_servidor, 0, 35)), 1, 0, 'L', 1);

                    $this->SetX("110");
                    $this->Cell(0, 5, utf8_decode(substr($row->lotacao, 0, 30)), 1, 0, 'L', 1);

                    $this->SetX("165");
                    $this->Cell(0, 5, utf8_decode(substr($row->datainicio, 0, 14)), 1, 0, 'L', 1);

                    $this->SetX("190");
                    $this->Cell(0, 5, utf8_decode(substr($row->datafim, 0, 14)), 1, 0, 'L', 1);


                    $this->SetX("215");
                    $this->Cell(0, 5, utf8_decode(substr($row->nome_tipolicenca, 0, 40)), 1, 1, 'L', 1);

                    $i++;
                } else {
                    $this->SetFont('arial', '', 15);
                    $this->SetX("15");
                    $this->Cell(0, 5, utf8_decode("Nao existem dados cadastrados para os  parametros desta consulta"), 0, 1, 'J');
                    $this->Ln();
                }
            }
            $this->SetX("7");
            $this->Cell(0, 0, '', 1, 1, 'L');
            $this->SetFont('Arial', 'B', 13);
            $this->SetX("7");
            $this->Cell(0, 10, utf8_decode("TOTAL: "), 0, 0, 'L');

            $this->SetX("25");
            $this->Cell(0, 10, utf8_decode(substr($i, 0, 4)), 0, 0, 'L');
        }


        TTransaction::close();
    }

//Page footer

    function Footer() {
        //Position at 1.5 cm from bottom
        $this->SetY(-15);
        //Arial italic 8
        $this->SetFont('Arial', 'I', 8);
        //Page number
        //data atual
        $data = date("d/m/Y H:i:s");
        $conteudo = "impresso em " . $data;
        $texto = $_SESSION['empresa_sitio'];
        //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
        $this->Cell(0, 0, '', 1, 1, 'L');

        //imprime uma celula... largura,altura, texto,borda,quebra de linha, alinhamento
        $this->Cell(0, 5, $texto, 0, 0, 'L');
        $this->Cell(0, 5, $conteudo, 0, 0, 'R');
        $this->Ln();
    }

}

//Instanciation of inherited class
$pdf = new RelatorioServidoresLicencaMesPDF("L", "mm", "A4");

//define o titulo
$pdf->SetTitle("Relatorio de Servidores de Licenca ");

//assunto
$pdf->SetSubject("Relatorio de Servidores de Licenca ");


$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times', '', 12);
$pdf->ColumnDetail();
$file = "app/reports/RelatorioServidoresLicencaMesPDF" . $_SESSION['servidor_id'] . ".pdf";
//abrir pdf
$pdf->Output($file);
$pdf->openFile($file);
?>