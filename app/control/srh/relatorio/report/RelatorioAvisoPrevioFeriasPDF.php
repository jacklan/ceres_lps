<?php

/*
 * classe RelatorioAvisoPrevioFeriasPDF
 * Autor: Jackson Meires
 * Data:08/12/2016
 */
use Adianti\Database\TTransaction;
use Adianti\Database\TRepository;
use Adianti\Database\TCriteria;
use Adianti\Database\TFilter;
use Lib\Funcoes\Util;

class RelatorioAvisoPrevioFeriasPDF extends FPDF {

    private $possuiDados = true;

    //Page header
    function Header() {
        $this->ColumnDetail();
        if ($this->possuiDados) {
            $this->ColumnDetail(2);
        } else {
            $this->SetXY(30,30);
            $this->MultiCell(0, 5, utf8_decode("teste"), 0);
            $this->ln(10);
        }
    }

    function ColumnHeader() {
        //define a fonte a ser usada
        $this->SetFont('arial', 'B', 9);
        $this->Ln();
    }

    function ColumnDetail($repeat = null) {

        $matricula = $_REQUEST['matricula'];
        $datainicio = Util::dataExtenso($_REQUEST['datainicio']);
        $datafinal = Util::dataExtenso($_REQUEST['datafinal']);
        $datavolta = Util::dataExtenso($_REQUEST['datavolta']);
        $ano = $_REQUEST['ano'];

        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        $criteria = new TCriteria();
        $repository = new TRepository('ServidorRecord');
        $criteria->add(new TFilter('matricula', '=', $matricula));
        $repository->load($criteria);

        // carrega os objetos de acordo com o criterio
        $rows = $repository->load($criteria);
        
        if ($rows) {
            $this->possuiDados = true;
            if ($repeat == 2) {
                $this->Ln(20);
                $this->Image("app/images/logo_relatorio.jpg", 8, 152, 18, 18);
            } else {
                $this->Image("app/images/logo_relatorio.jpg", 8, 10, 18, 18);
            }
 
            $this->SetFont('Arial', 'B', 11);
            $this->SetX("25");
            $this->Cell(0, 5, utf8_decode("GOVERNO DO ESTADO DO RIO GRANDE DO NORTE"), 0, 1, 'C');

            $this->SetFont('Arial', 'B', 11);
            $this->SetX("25");
            $this->Cell(0, 5, utf8_decode($_SESSION['empresa_nome']), 0, 1, 'C');

            $empresa = $_SESSION['empresa_id'];

            $this->SetX("35");
            if ($empresa == 5) {
                $this->Cell(0, 5, utf8_decode("UNIDADE INSTRUMENTAL DE ADMINISTRAÇÃO GERAL - USAG"), 0, 1, 'C');
            } else if ($empresa == 1) {
                $this->Cell(0, 5, utf8_decode("UNIDADE INSTRUMENTAL DE RECURSOS HUMANOS - UIRH"), 0, 1, 'C');
            }

            //define a fonte a ser usada
            $this->SetFont('Arial', 'B', 11);
            $this->Ln(5);

            $this->SetX("80");
            $this->Cell(0, 5, utf8_decode("AVISO PRÉVIO DE FÉRIAS"), 0, 0, 'J');

            $this->Ln(3);

            $this->ColumnHeader();
            
            // percorre os objetos retornados
            foreach ($rows as $row) {

                //define a fonte a ser usada
                $this->SetFont('arial', '', 11);
                $this->ln(5);

                $this->SetX("10");
                $this->Cell(0, 5, utf8_decode("De acordo com o artigo 84 da Lei nº 122/94 "), 0, 0, 'J');

                $this->ln(5);
                $this->SetX("140");
                $mes = array('', 'Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro');
                $this->MultiCell(80, 6, utf8_decode(" Natal/RN, " . date('d') . " de " . $mes[date('m')] . " de " . date("Y") . "."), 'C');

                //define a fonte a ser usada
                $this->SetFont('arial', '', 11);
                $this->ln(5);
                $this->SetX("10");
                $this->Cell(0, 5, utf8_decode("Ilmo.(a) Sr.(a)"), 0, 0, 'J');

                $this->ln(10);
                $this->SetX("10");
                $this->MultiCell(0, 5, utf8_decode($row->nome . ". Cargo: " . $row->nome_cargo . ". Matrícula: " . $row->matricula . ". Pelo presente comunico-lhe que, de acordo com a Escala de Férias desta Pasta, concedidas as férias relativas ao período de " . $ano . " férias que serão gozadas a partir do dia " . $datainicio . " até o dia " . $datafinal . " retornando V.Sª às atividades no dia " . $datavolta));

                $this->ln(5);
                $this->SetX("85");
                $this->Cell(0, 5, utf8_decode("Atenciosamente,"), 0, 0, 'J');
                $this->ln(20);

                $this->SetX("10");
                $this->MultiCell(0, 5, utf8_decode("________________________________________     ________________________________________\n                   Assinatura do Empregado                                          Assinatura do Empregador"), 0);
                $this->ln(10);
            }
        } else {
            $this->possuiDados = false;
        }
    }

    function Footer() {
        //Position at 1.5 cm from bottom
        $this->SetY(-15);
        //Arial italic 8
        $this->SetFont('Arial', 'I', 8);
        //Page number
        //data atual
        $data = date("d/m/Y H:i:s");
        $conteudo = "impresso em " . $data;
        $texto = "http://ceres.rn.gov.br";
        //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
        $this->Cell(0, 0, '', 1, 1, 'L');

        //imprime uma celula... largura,altura, texto,borda,quebra de linha, alinhamento
        $this->Cell(0, 5, $texto, 0, 0, 'L');
        $this->Cell(0, 5, 'Pag. ' . $this->PageNo() . ' de ' . '{nb}' . ' - ' . $conteudo, 0, 0, 'R');
        $this->Ln();
    }

}

//Instanciation of inherited class
$pdf = new RelatorioAvisoPrevioFeriasPDF("P", "mm", "A4");

//define o titulo
$pdf->SetTitle(utf8_decode("Relatório Aviso prévio de férias"));

//assunto
$pdf->SetSubject(utf8_decode("Relatório Aviso prévio de férias"));

$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times', '', 12);
$file = "app/reports/RelatorioAvisoPrevioFeriasPDF" . $_SESSION['servidor_id'] . ".pdf";
//abrir pdf
$pdf->Output($file);
$pdf->openFile($file);
