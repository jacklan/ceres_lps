<?php

use Adianti\Database\TTransaction;
use Adianti\Database\TRepository;
use Adianti\Database\TCriteria;
use Adianti\Database\TFilter;

class ServidorCurriculoDetalhePDF extends FPDF {

	//Page header
    function Header() {
        //endereco da imagem,posicao X(horizontal),posicao Y(vertical), tamanho altura, tamanho largura
        $this->Image("app/images/logo_relatorio.jpg", 8, 11, 18, 18);

        //Arial bold 15
        $this->SetFont('Arial', 'B', 12);
        $this->SetY("12");
        $this->SetX("25");
        $this->Cell(0, 5, utf8_decode("GOVERNO DO ESTADO DO RIO GRANDE DO NORTE"), 0, 1, 'C');

        $this->SetFont('Arial', 'B', 11);
        $this->SetY("17");
        $this->SetX("25");
        $this->Cell(0, 5, utf8_decode( $_SESSION['empresa_nome'] ), 0, 1, 'C');

        //define a fonte a ser usada
        $this->SetFont('Courier', 'B', 18);

        $this->SetY("25");
        $this->SetX("15");
        $this->Cell(0, 5, utf8_decode("CURRICULO SERVIDOR"), 0, 1, 'C');
        $this->Ln(3);

        $this->ColumnHeader();
    }

    function ColumnHeader() {
        
    }

    function ColumnDetail() {

        if (isset($_GET['key'])) {
            $servidor_id = $_GET['key'];
        } else {
            // inicia transacao com o banco 'pg_ceres'
            TTransaction::open('pg_ceres');
            $repository = new TRepository('ServidorRecord');
            // cria um criterio de selecao, ordenado pelo id
            $criteria = new TCriteria;
            //filtrar pelo id do servidor
            $criteria->add(new TFilter('matricula', '=', $_REQUEST['matricula']));
            $criteria->add(new TFilter('empresa_id', '=', $_SESSION['empresa_id']));
            // carrega os objetos de acordo com o criterio
            $rows = $repository->load($criteria);
            // percorre os objetos retornados
            if ($rows) {
                foreach ($rows as $row) {
                    $servidor_id = $row->id;
                }
            } else {
                $this->SetX("10");
                $this->Cell(0, 5, utf8_decode("Nenhum resultado encontrado com a Matricula informada: " . $_REQUEST['matricula']), 0, 1, 'C');
            }
            TTransaction::close();
        }
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia um repositorio para Carros
        $repository = new TRepository('vw_servidoresRecord');
        // cria um criterio de selecao, ordenado pelo id
        $criteria = new TCriteria;
        //filtrar pelo id do servidor
        $criteria->add(new TFilter('id', '=', $servidor_id));
        $criteria->add(new TFilter('empresa_id', '=', $_SESSION['empresa_id']));
        //ordenar por nome
        //  $criteria->setProperty('order', 'nome');
        // carrega os objetos de acordo com o criterio
        $rows = $repository->load($criteria);
        if ($rows) {
            $i = 0;
            // percorre os objetos retornados
            foreach ($rows as $row) {

                $this->SetX("10");
                //mostra a foto do servidor      
                if (file_exists('app/images/servidor/servidor_' . $row->id . '.jpg')) {
                    $this->Image('app/images/servidor/servidor_' . $row->id . '.jpg', 10, 40, 30, 30);
                } else {
                    $this->Image('app/images/servidor/servidor_sem_foto.jpg', 10, 40, 30, 30);
                }

                $this->SetY("40");
                $this->SetX("40");
                $this->Cell(0, 5, utf8_decode("Matricula: " . $row->matricula), 0, 1, 'L');

                $this->SetX("40");
                $this->Cell(0, 5, utf8_decode(substr("Nome: " . $row->nome, 0, 80)), 0, 1, 'L');

                $this->SetX("40");
                $this->Cell(0, 5, utf8_decode(substr("E-mail: " . $row->email, 0, 35)), 0, 1, 'L');

                $this->SetX("40");
                $this->Cell(0, 5, utf8_decode(substr("Setor: " . $row->setor, 0, 35)), 0, 0, 'L');

                // $i = $i + 1;
            }

            $this->Ln(20);
            $this->Cell(0, 0, '', 1, 1, 'L');
            $this->Ln(5);

            // instancia um repositorio para Carros
            $repository = new TRepository('FormacaoServidorRecord');
            // cria um criterio de selecao, ordenado pelo id
            $criteria = new TCriteria;
            //filtrar pelo id do servidor
            $criteria->add(new TFilter('servidor_id', '=', $servidor_id));
            //ordenar por nome
            //  $criteria->setProperty('order', 'nome');
            // carrega os objetos de acordo com o criterio
            $rows = $repository->load($criteria);
            if ($rows) {
                $i = 0;
                // percorre os objetos retornados
                $this->SetFont('Arial', 'B', 10);
                $this->SetX("10");
                $this->Cell(0, 5, utf8_decode("Formação acadêmica/titulação "), 0, 1, 'L');

                foreach ($rows as $row) {

                    //define a fonte a ser usada
                    $this->SetFont('Arial', '', 10);
                    $this->SetX("10");
                    $this->Cell(0, 5, utf8_decode($row->nome_formacao), 0, 1, 'L');

                    $this->SetX("10");
                    $this->Cell(0, 5, utf8_decode(substr($row->descricao, 0, 35)), 0, 1, 'L');

                    $this->SetX("10");
                    $this->Cell(0, 5, utf8_decode("Data da Conclusão: " . \Adianti\Widget\Form\TDate::date2br($row->dataconclusao)), 0, 1, 'L');

                    $this->SetX("10");
                    $this->Cell(0, 5, utf8_decode(substr("Situação: " . $row->situacao, 0, 80)), 0, 1, 'L');
                }
                $this->Ln(5);
                $this->Cell(0, 0, '', 1, 1, 'L');
                $this->Ln(5);
            }

            // instancia um repositorio para Carros
            $repository = new TRepository('ExperienciaRecord');
            // cria um criterio de selecao, ordenado pelo id
            $criteria = new TCriteria;
            //filtrar pelo id do servidor
            $criteria->add(new TFilter('servidor_id', '=', $servidor_id));
            //ordenar por nome
            //  $criteria->setProperty('order', 'nome');
            // carrega os objetos de acordo com o criterio
            $rows = $repository->load($criteria);
            if ($rows) {
                $i = 0;
                // percorre os objetos retornados
                $this->SetFont('Arial', 'B', 10);
                $this->SetX("10");
                $this->Cell(0, 5, utf8_decode("Atuação Profissional "), 0, 1, 'L');

                foreach ($rows as $row) {

                    //define a fonte a ser usada
                    $this->SetFont('Arial', '', 10);
                    $this->SetX("10");
                    $this->Cell(0, 5, utf8_decode("Empresa/Orgão: " . $row->empresa), 0, 1, 'L');

                    $this->SetX("10");
                    $this->Cell(0, 5, utf8_decode("Cargo: " . $row->cargo), 0, 1, 'L');

                    $this->SetX("10");
                    $this->Cell(0, 5, utf8_decode("Periodo: " . \Adianti\Widget\Form\TDate::date2br($row->datainicio) . " - " . \Adianti\Widget\Form\TDate::date2br($row->datafim)), 0, 1, 'L');

                    $this->SetX("10");
                    $this->Cell(0, 5, utf8_decode(substr("Função: " . $row->observacao, 0, 80)), 0, 1, 'L');

                    $this->Ln(5);
                }

                $this->Cell(0, 0, '', 1, 1, 'L');
                $this->Ln(5);
            }


            // instancia um repositorio para Carros
            $repository = new TRepository('ServidorCapacitacaoRecord');
            // cria um criterio de selecao, ordenado pelo id
            $criteria = new TCriteria;
            //filtrar pelo id do servidor
            $criteria->add(new TFilter('servidor_id', '=', $servidor_id));
            //ordenar por nome
            //  $criteria->setProperty('order', 'nome');
            // carrega os objetos de acordo com o criterio
            $rows = $repository->load($criteria);
            if ($rows) {
                $i = 0;
                // percorre os objetos retornados
                $this->SetFont('Arial', 'B', 10);
                $this->SetX("10");
                $this->Cell(0, 5, utf8_decode("�?rea de atuação/Capacitação: "), 0, 1, 'L');

                foreach ($rows as $row) {

                    //define a fonte a ser usada
                    $this->SetFont('Arial', '', 10);
                    $this->SetX("10");
                    $this->Cell(0, 5, utf8_decode($row->nome_capacitacao), 0, 1, 'L');

                    $this->SetX("10");
                    $this->Cell(0, 5, utf8_decode("Situação: " . $row->situacao), 0, 1, 'L');

                    $this->Ln(5);
                    // $i = $i + 1;
                }

                $this->Cell(0, 0, '', 1, 1, 'L');
                $this->Ln(5);
            }

            // instancia um repositorio para Carros
            $repository = new TRepository('ProducaoRecord');
            // cria um criterio de selecao, ordenado pelo id
            $criteria = new TCriteria;
            //filtrar pelo id do servidor
            $criteria->add(new TFilter('servidor_id', '=', $servidor_id));
            //ordenar por nome
            //  $criteria->setProperty('order', 'nome');
            // carrega os objetos de acordo com o criterio
            $rows = $repository->load($criteria);
            if ($rows) {
                $i = 0;
                // percorre os objetos retornados
                $this->SetFont('Arial', 'B', 10);
                $this->SetX("10");
                $this->Cell(0, 5, utf8_decode("Produções "), 0, 1, 'L');

                foreach ($rows as $row) {

                    //define a fonte a ser usada
                    $this->SetFont('Arial', '', 10);
                    $this->SetX("10");
                    $this->Cell(0, 5, utf8_decode($row->titulo), 0, 1, 'L');

                    $this->SetX("10");
                    $this->Cell(0, 5, utf8_decode("Instituição: " . $row->instituicao), 0, 1, 'L');

                    $this->SetX("10");
                    $this->Cell(0, 5, utf8_decode("Descrição: " . $row->descricao), 0, 1, 'L');

                    $this->SetX("10");
                    $this->Cell(0, 5, utf8_decode("Data Produção: " . \Adianti\Widget\Form\TDate::date2br($row->dataproducao) . " Ano: " . $row->ano), 0, 1, 'L');

                    $this->Ln(5);
                    // $i = $i + 1;
                }
            }
        }

        TTransaction::close();
    }

//Page footer
    function Footer() {
        //Position at 1.5 cm from bottom
        $this->SetY(-15);
        //Arial italic 8
        $this->SetFont('Arial', 'I', 8);
        //Page number
        //data atual
        $data = date("d/m/Y H:i:s");
        $conteudo = "impresso em " . $data;
        $texto = $_SESSION['empresa_sitio'];
        //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
        $this->Cell(0, 0, '', 1, 1, 'L');

        //imprime uma celula... largura,altura, texto,borda,quebra de linha, alinhamento
        $this->Cell(0, 5, $texto, 0, 0, 'L');
        $this->Cell(0, 5, $conteudo, 0, 0, 'R');
        $this->Ln();
    }

}

//Instanciation of inherited class
$pdf = new ServidorCurriculoDetalhePDF("P", "mm", "A4");


//define o titulo
$pdf->SetTitle(utf8_decode("Servidor Curriculo" ));

//assunto
$pdf->SetSubject(utf8_decode("Servidor Curriculo" ));

$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times', '', 12);
$pdf->ColumnDetail();
$file = "app/reports/ServidorCurriculoDetalhePDF" . $_SESSION['servidor_id'] . ".pdf";
//abrir pdf
$pdf->Output($file);
$pdf->openFile($file);