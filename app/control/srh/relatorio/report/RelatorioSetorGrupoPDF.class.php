<?php

use FPDF;
use Adianti\Database\TTransaction;
use Adianti\Database\TRepository;
use Adianti\Database\TCriteria;
use Adianti\Database\TFilter;

class RelatorioSetorGrupoPDF extends FPDF {

//Page header
    function Header() {
        $conselho = $_REQUEST['conselho'];

//endereco da imagem,posicao X(horizontal),posicao Y(vertical), tamanho altura, tamanho largura
        $this->Image("app/images/logo_relatorio.jpg", 8, 11, 18, 18);

//Arial bold 15
        $this->SetFont('Arial', 'B', 12);
        $this->SetY("12");
        $this->SetX("25");
        $this->Cell(0, 5, utf8_decode("GOVERNO DO ESTADO DO RIO GRANDE DO NORTE"), 0, 1, 'C');

        $this->SetY("17");
        $this->SetX("25");
        $this->Cell(0, 5, utf8_decode( $_SESSION['empresa_nome'] ), 0, 1, 'C');
        
        $titulo = "RELATÓRIO GRUPO DE SETOR";
        $this->SetY("25");
        $this->SetX("25");
        $this->Cell(0, 5, utf8_decode($titulo), 0, 1, 'C');



//$this->Ln(10);
        $this->Ln(3);

        $this->ColumnHeader();
    }

    function ColumnHeader() {
        $opcao = $_REQUEST['opcao'];
        $this->SetFillColor(100, 180, 118);
//define a fonte a ser usada
        $this->SetFont('Arial', 'B', 10);
        $this->SetX("10");
        $this->Cell(0, 5, utf8_decode("Setor/Servidor"), 1, 0, 'L', 1);

//        $this->SetX("25");
//        $this->Cell(0, 5, utf8_decode("Servidor"), 1, 0, 'L',1);

        $this->SetX("105");
        $this->Cell(0, 5, utf8_decode("Matricula"), 1, 1, 'L', 1);
    }

    function ColumnDetail() {
        //parametros do form
        $nomesetorgrupo = $_REQUEST['nomesetorgrupo'];
        //  $nomeservidor = $_REQUEST['nomeservidor'];
        $nomesetor = $_REQUEST['nomesetor'];

        $this->SetX("20");


        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instancia um repositorio para Carros
        $repository = new TRepository('vw_setorgrupoRecord');
        // cria um criterio de selecao, ordenado pelo id
        $criteria = new TCriteria;

        if ($nomesetorgrupo == 'TODOS') {
            //filtra pelo campo selecionado pelo usu�rio / mes
        } else {
            $criteria->add(new TFilter('nomesetorgrupo', '=', $nomesetorgrupo));
        }

        if ($nomesetor == 'TODOS') {
            //filtra pelo campo selecionado pelo usu�rio / mes
        } else {
            //adiciona o criterio
            $criteria->add(new TFilter('nomesetor', '=', $nomesetor));
        }
        $criteria->add(new TFilter('empresa_id', '=', $_SESSION['empresa_id']));
//        $repository1 = new TRepository('vw_servidorsetor');
//        // cria um criterio de selecao, ordenado pelo id
//        $criteria1 = new TCriteria;
//
//        $criteria1->add(new TFilter('setor_id', '=', $setor_id));
        //$rows1 = $repository->load($criteria1);

        $i = 0;
        $i2 = 0;
        // carrega os objetos de acordo com o criterio
        $rows = $repository->load($criteria);

        if ($rows) {
            $nomeservidor = '';

            foreach ($rows as $row) {
                // percorre os objetos retornados
//                if ($nomesetor == 'TODAS') {
                if ($i % 2 == 0) {
                    $this->SetFillColor(235, 235, 235);
                } else {
                    $this->SetFillColor(255, 255, 255);
                }

                if ($nomeservidor != $row->nomeservidor) {
                    $nomeservidor = $row->nomeservidor;
                    //define a fonte a ser usada
                    $this->SetFont('Arial', 'B', 10);
                    $this->SetFillColor(162, 234, 150);

                    //  $nomesetorgrupo = $row->nomesetorgrupo;
                    $this->SetX("15");
                    $this->Cell(0, 5, utf8_decode("Setor: " . $row->nomesetorgrupo), 1, 0, 'L', 1);
                    $this->SetX("162");
                    $this->Cell(0, 5, utf8_decode("Matricula: " . $row->matricula), 1, 0, 'L', 1);
                    $this->SetX("50");
                    $this->Cell(0, 5, utf8_decode("Responsavel: " . $row->nomeservidor), 1, 0, 'L', 1);
                    //   $this->Ln();
                    $this->Ln();
                }
                if ($setor != $row->nomesetor) {
                    $setor = $row->nomesetor;
                    //define a fonte a ser usada
                    //  $setor = $row->nomesetor;
                    $this->SetFont('Arial', 'B', 9);
                    $this->SetFillColor(100, 180, 118);
                    $this->SetX("10");
                    $this->Cell(0, 5, utf8_decode($row->nomesetor . " - " . $row->nome_setorgrupo), 1, 0, 'L', 1);
                    $this->Ln();
                }

                $repository1 = new TRepository('vw_servidorsetorRecord');
                // cria um criterio de selecao, ordenado pelo id
                $criteria1 = new TCriteria;

                $criteria1->add(new TFilter('setor_id', '=', $row->setor_id));
                $rows1 = $repository1->load($criteria1);
                foreach ($rows1 as $row1) {

                    if ($i % 2 == 0) {
                        $this->SetFillColor(235, 235, 235);
                    } else {
                        $this->SetFillColor(255, 255, 255);
                    }

                    $this->SetFont('Arial', '', 9);
                    $this->SetX("15");
                    $this->Cell(0, 5, utf8_decode($row1->nome), 1, 0, 'L', 1);
                    $this->SetX("105");
                    $this->Cell(0, 5, utf8_decode($row1->matricula), 1, 0, 'L', 1);
                    $this->SetX("125");
                    $this->Cell(0, 5, utf8_decode($row1->telefone), 1, 0, 'L', 1);
                    $this->SetX("150");
                    $this->Cell(0, 5, utf8_decode((substr($row1->email, 0, 42))), 1, 0, 'L', 1);
                    $this->Ln();
                    $i++;
                    $t = ($i);
                }
//contador para gerar total
                $i2 = ($i2 + 1);
                $t2 = ($i2);
// $this->AddPage();
            }
            TTransaction::close();
        }

        $this->SetFont('arial', 'B', 9);
        $this->SetX("10");
        $this->Cell(0, 5, "Total Servidor: " . $t, 0, 1, 'J');
        $this->Cell(0, 5, "Total Setor: " . $t2, 0, 1, 'J');
    }

//Page footer

    function Footer() {
//Position at 1.5 cm from bottom
        $this->SetY(-15);
//Arial italic 8
        $this->SetFont('Arial', 'I', 8);
//Page number
//data atual
        $data = date("d/m/Y H:i:s");
        $conteudo = "impresso em " . $data;
        $texto = $_SESSION['empresa_sitio'];
//imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
        $this->Cell(0, 0, '', 1, 1, 'L');

//imprime uma celula... largura,altura, texto,borda,quebra de linha, alinhamento
        $this->Cell(0, 5, $texto, 0, 0, 'L');
        $this->Cell(0, 5, $conteudo, 0, 0, 'R');
        $this->Ln();
    }

}

//Instanciation of inherited class
$pdf = new RelatorioSetorGrupoPDF("L", "mm", "A4");

//define o titulo
$pdf->SetTitle("Relatorio Grupo Setor");

//assunto
$pdf->SetSubject("Relatorio Grupo Setor");

$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times', '', 12);
$pdf->ColumnDetail();
$file = "app/reports/RelatorioSetorGrupoPDF" . $_SESSION['servidor_id'] . ".pdf";
//abrir pdf
$pdf->Output($file);
$pdf->openFile($file);
?>
