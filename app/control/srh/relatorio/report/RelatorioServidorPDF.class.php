<?php

ini_set('display_errors', 1);
ini_set('display_startup_erros', 1);
error_reporting(E_ALL);

use Adianti\Database\TTransaction;
use Adianti\Database\TRepository;
use Adianti\Database\TCriteria;
use Adianti\Database\TFilter;

class RelatorioServidorPDF extends FPDF {

    //Page header
    function Header() {
        // posicao vertical no caso -1.. e o limite da margem
        //$this->SetY("-1");
//        $this->Cell(0, 0, '', 0, 0, 'L');
//        $this->Ln();
        //inclui cabe�alho
        //endereco da imagem,posicao X(horizontal),posicao Y(vertical), tamanho altura, tamanho largura
        $this->Image("app/images/logo_relatorio.jpg", 8, 11, 26, 18);
        $this->SetFont('Arial', 'B', 12);
        $this->SetY("12");
        $this->SetX("35");
        $this->Cell(0, 5, utf8_decode("GOVERNO DO ESTADO DO RIO GRANDE DO NORTE"), 0, 1, 'J');

        $this->SetY("17");
        $this->SetX("35");
        $this->Cell(0, 5, utf8_decode("INSTITUTO DE ASSISTENCIA TECNICA E EXTENSAO RURAL - EMATER"), 0, 1, 'J');

        // $this->SetY("27");
        $this->SetX("35");
        $this->Cell(0, 5, utf8_decode("UNIDADE INSTRUMENTAL DE RECURSOS HUMANOS - UIRH"), 0, 1, 'J');
        $this->SetX("35");
        $this->Cell(0, 5, "Relat�rio do Servidores - EMATER-RN", 0, 1, 'J');

        $this->ColumnHeader();
    }

    function ColumnHeader() {
        //$this->Header();
    }

    function ColumnDetail() {

        $this->ColumnHeader();

        //parametros do form
        $tipo = $_REQUEST['tipo'];
        $regiaosl = $_REQUEST['regiao'];
        $tipofuncionario = $_REQUEST['tipofuncionario'];

        //define a fonte a ser usada
        $this->SetFont('arial', 'B', 9);

        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instancia um repositorio para Carros
        $repository = new TRepository('vw_servidoresRecord');

        // cria um criterio de selecao, ordenado pelo id
        $criteria = new TCriteria;
        $criteria->setProperty('order', 'regiao,municipio,nome');

        //   $v = 35;
        //  $posicao = $this->SetY($v);

        if ($tipo != '') {

            if ($tipo != 'TODOS') {

                if ($tipo == 'ATIVOS') {

                    $criteria->add(new TFilter('situacao', '<>', 'APOSENTADO(A)'));
                    $criteria->add(new TFilter('situacao', '<>', 'RESCINDIDO'));
                } else {
                    if ($tipo == 'INATIVOS') {
                        $criteria->add(new TFilter('situacao', '=', 'APOSENTADO(A)', 'and', 'situacao', '=', 'RESCINDIDO'));
                    }
                }
            }
        }
        if ($regiaosl != '') {
            if ($regiaosl == 'TODAS') {
                //filtra pelo campo selecionado pelo usu�rio / mes
                $criteria->setProperty('order', 'regiao,municipio,nome');
            } else {
                //adiciona o criterio
                $criteria->add(new TFilter('regiao', '=', $regiaosl));
            }
        }
        if ($tipofuncionario != '') {

            if ($tipofuncionario == 'TODOS') {
                //filtra pelo campo selecionado pelo usu�rio / mes
                $criteria->setProperty('order', 'regiao,municipio,nome,tipofuncionario');
            } else {
                //adiciona o criterio
                $criteria->add(new TFilter('tipofuncionario', '=', $tipofuncionario));
            }
        }
        // carrega os objetos de acordo com o criterio
        $rows = $repository->load($criteria);

        $regiao = "";
        $municipio = "";
        $count = 0;
        $t = 0;

        if ($rows) {
            // percorre os objetos retornados
            foreach ($rows as $row) {

                // adiciona os dados do perfil do usuario no menu
                if ($row->regiao == '') {

                    $this->SetFont('arial', '', 15);

                    $this->SetX("20");
                    $this->Cell(0, 5, utf8_decode("Nao existem dados cadastrados para os  parametros desta consulta"), 0, 1, 'J');
                }
                if ($regiao != $row->regiao) {
                    //define a fonte a ser usada
                    $this->SetFont('arial', 'B', 9);

                    $regiao = $row->regiao;
                    $municipio = "";

                    $this->SetX("10");
                    $this->Cell(0, 5, utf8_decode("Regiao:      " . ($row->regiao)), 0, 1, 'J');
                }

                if ($municipio != $row->municipio) {

                    //define a fonte a ser usada
                    if ($count != 0) {
                        $this->SetFont('arial', 'B', 9);
                        $this->SetX("10");
                        $this->Cell(0, 5, "Total: " . ($count), 0, 1, 'J');
                    }

                    //define a fonte a ser usada
                    $this->SetFont('arial', 'B', 9);

                    $municipio = $row->municipio;
                    $setor = "";

                    $this->SetX("10");
                    $this->Cell(0, 5, utf8_decode("Municipio: " . ($row->municipio)), 0, 1, 'J');
                    $count = 0;
                }

                if ($setor != $row->setor) {

                    //define a fonte a ser usada
                    $this->SetFont('arial', 'B', 9);

                    $setor = $row->setor;

                    $this->SetX("10");
                    $this->Cell(0, 5, utf8_decode("Setor: " . ($row->setor)), 0, 1, 'J');

                    //define a fonte a ser usada
                    $this->SetFont('arial', 'B', 9);

                    // Cabecalho dos dados
                    $this->SetX("15");
                    $this->Cell(0, 5, utf8_decode("Matricula"), 0, 0, 'J');

                    $this->SetX("30");
                    $this->Cell(0, 5, utf8_decode("Nome"), 0, 0, 'J');

                    $this->SetX("95");
                    $this->Cell(0, 5, utf8_decode("Telefone"), 0, 0, 'J');

                    $this->SetX("117");
                    $this->Cell(0, 5, utf8_decode("Celular"), 0, 0, 'J');

                    $this->SetX("135");
                    $this->Cell(0, 5, utf8_decode("Email"), 0, 0, 'J');

                    $this->SetX("175");
                    $this->Cell(0, 5, utf8_decode("Tipo Funcionario"), 0, 1, 'J');

                    $this->Cell(0, 0, '', 1, 1, 'L');
                    $this->Ln(1);
                }
                //define a fonte a ser usada
                $this->SetFont('arial', '', 9);

                $this->SetX("15");
                $this->Cell(0, 5, utf8_decode($row->matricula), 0, 0, 'J');

                $this->SetX("30");
                $this->Cell(0, 5, utf8_decode(substr($row->nome, 0, 32)), 0, 0, 'J');

                $this->SetX("95");
                $this->Cell(0, 5, utf8_decode(substr($row->telefone, 0, 10)), 0, 0, 'J');

                $this->SetX("117");
                $this->Cell(0, 5, utf8_decode(substr($row->celular, 0, 10)), 0, 0, 'J');

                $this->SetX("135");
                $this->Cell(0, 5, utf8_decode($row->tipofuncionario), 0, 1, 'J');

                $this->SetX("180");
                $this->Cell(0, 5, utf8_decode(substr($row->email, 0, 20)), 0, 0, 'J');

                //contador para gerar total
                $count++;
                $t++;
                //Adiciona uma linha
                $this->Cell(0, 0, '', 1, 1, 'L');
                $this->Ln(1);
            }
            if ($row != '') {

                $this->SetFont('arial', 'B', 9);
                //    $posicao = $posicao + 5;
                $this->SetX("10");
                $this->Cell(0, 5, "TOTAL: " . $count, 0, 1, 'J');
                $this->SetX("10");
                $this->Cell(0, 5, "TOTAL GERAL: " . $t, 0, 1, 'J');
            }
        } else {
            $this->SetFont('arial', '', 15);
            $this->SetX("20");
            $this->Cell(0, 5, utf8_decode("Nao existem dados cadastrados para os  parametros desta consulta"), 0, 1, 'J');
        }
        TTransaction::close();  // close the transaction
    }

    //Page footer
    function Footer() {
        //Position at 1.5 cm from bottom
        $this->SetY(-15);
        //Arial italic 8
        $this->SetFont('Arial', 'I', 8);
        //Page number
        //data atual
        $data = date("d/m/Y H:i:s");
        $conteudo = "impresso em " . $data;
        $texto = "http://www.emater.rn.gov.br";
        //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
        $this->Cell(0, 0, '', 1, 1, 'L');

        //imprime uma celula... largura,altura, texto,borda,quebra de linha, alinhamento
        $this->Cell(0, 5, $texto, 0, 0, 'L');
        $this->Cell(0, 5, 'Pag. ' . $this->PageNo() . ' de ' . '{nb}' . ' - ' . $conteudo, 0, 0, 'R');
        $this->Ln();
    }

}

//instacia a classe FPDF
$pdf = new RelatorioServidorPDF("P", "mm", "A4");

//define o titulo
$pdf->SetTitle("Relat�rio dos Servidores - EMATER-RN");

//assunto
$pdf->SetSubject("Relat�rio do Servidor  - EMATER-RN");

$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times', '', 12);
$pdf->ColumnDetail();
$file = "app/reports/RelatorioServidorPDF" . $_SESSION['servidor_id'] . ".pdf";

//abrir pdf
$pdf->Output($file);
$pdf->openFile($file);
?>