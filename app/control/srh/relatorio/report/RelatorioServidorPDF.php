<?php

use Adianti\Database\TTransaction;
use Adianti\Database\TRepository;
use Adianti\Database\TCriteria;
use Adianti\Database\TFilter;

//parametros do form
$tipo = $_REQUEST['tipo'];
$regiaosl = $_REQUEST['regiao'];
$tipofuncionario = $_REQUEST['tipofuncionario'];
class RelatorioServidorPDF extends FPDF {


// posicao vertical no caso -1.. e o limite da margem
$this->SetY("-1");

$titulo = "Relat�rio do Servidores - EMATER-RN";


$this->Cell(0, 0, '', 0, 0, 'L');
$this->Ln();


//inclui cabe�alho
//endereco da imagem,posicao X(horizontal),posicao Y(vertical), tamanho altura, tamanho largura
$this->Image("app.images/logo_relatorio.jpg", 8, 11, 26, 18);

$this->SetY("12");
$this->SetX("35");
$this->Cell(0, 5, utf8_decode("GOVERNO DO ESTADO DO RIO GRANDE DO NORTE"), 0, 1, 'J');


$this->SetY("17");
$this->SetX("35");
$this->Cell(0, 5, utf8_decode("INSTITUTO DE ASSISTENCIA TECNICA E EXTENSAO RURAL - EMATER"), 0, 1, 'J');


$this->SetY("22,7");
$this->SetX("35");
$this->Cell(0, 5, utf8_decode("UNIDADE INSTRUMENTAL DE RECURSOS HUMANOS - UIRH"), 0, 1, 'J');


//define a fonte a ser usada
$this->SetFont('arial', 'B', 9);

// inicia transacao com o banco 'pg_ceres'
TTransaction::open('pg_ceres');

// instancia um repositorio para Carros
$repository = new TRepository('vw_servidores');

// cria um criterio de selecao, ordenado pelo id
$criteria = new TCriteria;
$criteria->setProperty('order', 'regiao,municipio,nome');


$v = 35;
$posicao = $this->SetY($v);

$i = 0;

if ($tipo == '') {
    
} else {
    if ($tipo == 'TODOS') {
        //filtra pelo campo selecionado pelo usu�rio / mes
    } else {
        if ($tipo == 'ATIVOS') {
            $criteria->add(new TFilter('situacao', '<>', 'APOSENTADO(A)'));
            $criteria->add(new TFilter('situacao', '<>', 'RESCINDIDO'));
        } else {
            if ($tipo == 'INATIVOS') {
                $criteria->add(new TFilter('situacao', '=', 'APOSENTADO(A)', 'and', 'situacao', '=', 'RESCINDIDO'));
                // $criteria->add(new TFilter('situacao', '=', 'RESCINDIDO'));
            }
        }
    }
}

if ($regiaosl == '') {
    
} else {
    if ($regiaosl == 'TODAS') {
        //filtra pelo campo selecionado pelo usu�rio / mes
        $criteria->setProperty('order', 'regiao,municipio,nome');
    } else {
        //  $criteria->add(new TFilter('regiao', '=', $regiaosl));
        //adiciona o criterio
        $criteria->add(new TFilter('regiao', '=', $regiaosl));
    }


    if ($tipofuncionario == '') {
        
    } else {
        if ($tipofuncionario == 'TODOS') {
            //filtra pelo campo selecionado pelo usu�rio / mes
            $criteria->setProperty('order', 'regiao,municipio,nome,tipofuncionario');
        } else {
            //  $criteria->add(new TFilter('regiao', '=', $regiaosl));
            //adiciona o criterio
            $criteria->add(new TFilter('tipofuncionario', '=', $tipofuncionario));
        }
    }

    // carrega os objetos de acordo com o criterio
    $rows = $repository->load($criteria);

    $regiao = "";
    $municipio = "";

    if ($rows) {
        // percorre os objetos retornados
        foreach ($rows as $row) {
            // adiciona os dados do perfil do usuario no menu
            if ($row->regiao == '') {
                $this->SetFont('arial', '', 15);
                $posicao;
                $this->SetX("20");
                $this->Cell(0, 5, utf8_decode("Nao existem dados cadastrados para os  parametros desta consulta"), 0, 1, 'J');
            }


            if ($regiao != $row->regiao) {
                //define a fonte a ser usada
                $this->SetFont('arial', 'B', 9);


                $regiao = $row->regiao;
                $municipio = "";
                $posicao;
                $this->SetX("10");
                $this->Cell(0, 5, utf8_decode("Regiao:      " . ($row->regiao)), 0, 1, 'J');
            }

            if ($municipio != $row->municipio) {
                //define a fonte a ser usada
                $this->SetFont('arial', 'B', 9);

                $municipio = $row->municipio;
                $setor = "";
                $posicao;
                $this->SetX("10");
                $this->Cell(0, 5, utf8_decode("Municipio: " . ($row->municipio)), 0, 1, 'J');
            }

            if ($setor != $row->setor) {
                //define a fonte a ser usada
                $this->SetFont('arial', 'B', 9);

                $setor = $row->setor;
                $posicao;
                $this->SetX("10");
                $this->Cell(0, 5, utf8_decode("Setor: " . ($row->setor)), 0, 1, 'J');

                //define a fonte a ser usada
                $this->SetFont('arial', 'B', 9);

                // Cabecalho dos dados
                $header1 = "Matricula";
                $posicao;
                $this->SetX("23");
                $this->Cell(0, 5, utf8_decode($header1), 0, 0, 'J');

                $header2 = "Nome";
                $posicao;
                $this->SetX("40");
                $this->Cell(0, 5, utf8_decode($header2), 0, 0, 'J');

                $header3 = "Telefone";
                $posicao;
                $this->SetX("100");
                $this->Cell(0, 5, utf8_decode($header3), 0, 0, 'J');

                $header4 = "Celular";
                $posicao;
                $this->SetX("117");
                $this->Cell(0, 5, utf8_decode($header4), 0, 0, 'J');

                $header5 = "Email";
                $posicao;
                $this->SetX("135");
                $this->Cell(0, 5, utf8_decode($header5), 0, 0, 'J');


                $header6 = "Tipo Funcionario";
                $posicao;
                $this->SetX("175");
                $this->Cell(0, 5, utf8_decode($header6), 0, 1, 'J');


                $this->Cell(0, 0, '', 1, 1, 'L');
                $this->Ln(1);
            }

            //define a fonte a ser usada
            $this->SetFont('arial', '', 9);


            $posicao;
            $this->SetX("23");
            $this->Cell(0, 5, utf8_decode($row->matricula), 0, 0, 'J');

            $posicao;
            $this->SetX("40");
            $this->Cell(0, 5, utf8_decode($row->nome), 0, 0, 'J');

            $posicao;
            $this->SetX("100");
            $this->Cell(0, 5, utf8_decode($row->telefone), 0, 0, 'J');

            $posicao;
            $this->SetX("117");
            $this->Cell(0, 5, utf8_decode($row->celular), 0, 0, 'J');

            $posicao;
            $this->SetX("135");
            $this->Cell(0, 5, utf8_decode($row->email), 0, 0, 'J');

            $posicao;
            $this->SetX("180");
            $this->Cell(0, 5, utf8_decode($row->tipofuncionario), 0, 1, 'J');

            //contador para gerar total
            $i = ($i + 1);
            $t = ($i);

            // $this->Cell(0,0,'',1,1,'L');
            //  $this->Ln(1);
        }
    }
    //Adiciona uma linha
    $this->Cell(0, 0, '', 1, 1, 'L');
    $this->Ln(1);

    if ($row != '') {
        $this->SetFont('arial', 'B', 9);
        $posicao = $posicao + 5;
        $this->SetX("10");
        $this->Cell(0, 5, "TOTAL: " . $t, 0, 1, 'J');
    }


    //posiciona verticalmente 270mm

    $this->SetY("270");

    //data atual
    $data = date("d/m/Y");
    $conteudo = "impresso em " . $data;
    $texto = "http://www.emater.rn.gov.br";

    //imprime uma celula... largura,altura, texto,borda,quebra de linha, alinhamento
    $this->Cell(0, 0, '', 1, 1, 'L');

    //imprime uma celula... largura,altura, texto,borda,quebra de linha, alinhamento
    $this->Cell(0, 5, $texto, 0, 0, 'L');

    //imprime uma celula... largura,altura, texto,borda,quebra de linha, alinhamento
    $this->Cell(0, 5, $conteudo, 0, 1, 'R');

    //n�mero da pagina
    $this->SetY("4");
    $this->SetX("193");
    //Vai para 1.5 cm da parte inferior
    // $this->SetY(-15);
    //Seleciona a fonte Arial it�lico 8
    $this->SetFont('Arial', 'I', 8);
    //Imprime o n�mero da p�gina corrente e o total de p�ginas
    $this->Cell(0, 5, utf8_decode('P�g. ' . $this->PageNo() . ''));

    TTransaction::close();

    //hora do conteudo do artigo
    $this->SetFont('arial', '', 10);

    //endereco da imagem,posicao X(horizontal),posicao Y(vertical), tamanho altura, tamanho largura
    $this->Image("app.images/logo_relatorio.jpg", 8, 11, 26, 18);

    $this->Cell(0, 0, '', 0, 10, 'L');
    //$this->Ln(1);
    //imprime a saida do arquivo..
    $this->Output("arquivo", "I");
}
}

//instacia a classe FPDF
$pdf = new RelatorioServidorPDF("P", "mm", "A4");

//define o titulo
$pdf->SetTitle("Relat�rio dos Servidores - EMATER-RN");

//assunto
$pdf->SetSubject("Relat�rio do Servidor  - EMATER-RN");

$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times', '', 12);
$pdf->ColumnDetail();
$file = "app/reports/RelatorioServidorPDF" . $_SESSION['servidor_id'] . ".pdf";
//abrir pdf
$pdf->Output($file);
$pdf->openFile($file);
?>
