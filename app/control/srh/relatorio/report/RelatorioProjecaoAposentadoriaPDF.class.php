<?php


use FPDF;
use Adianti\Database\TTransaction;
use Adianti\Database\TRepository;
use Adianti\Database\TCriteria;
use Adianti\Database\TFilter;
use AceitaplanoGraf;

class RelatorioProjecaoAposentadoriaPDF extends FPDF {



//Page header
    function Header() {
        //endereco da imagem,posicao X(horizontal),posicao Y(vertical), tamanho altura, tamanho largura
        $this->Image("app/images/logo_relatorio.jpg", 8,11,26,18);

        //Arial bold 15
        $this->SetFont('Arial','B',12);
        $this->SetY("12");
        $this->SetX("35");
        $this->Cell(0,5,utf8_decode("GOVERNO DO ESTADO DO RIO GRANDE DO NORTE"),0,1,'J');

        $this->SetY("17");
        $this->SetX("35");
        $this->Cell(0,5,utf8_decode( $_SESSION['empresa_nome'] ),0,1,'J');

         if ($_REQUEST['situacao'] == 'TODOS') {
         $titulo = " RELATÓRIO SERVIDORES PARA APOSENTADORIA";
        $this->SetY("27");
        $this->SetX("35");
        $this->Cell(0,5,utf8_decode($titulo),0,1,'J');
         }else{
        $titulo = " RELATÓRIO SERVIDORES ".$_REQUEST['situacao']." PARA APOSENTADORIA";
        $this->SetY("27");
        $this->SetX("35");
        $this->Cell(0,5,utf8_decode($titulo),0,1,'J');
         }

        $this->Ln(3);

        $this->ColumnHeader();
    }

    function ColumnHeader() {
        //define a fonte a ser usada
        $this->SetFont('arial','B',9);
        $this->SetY("35");
        $this->SetX("10");
        $this->Cell(0,5,  utf8_decode("Matrícula"),0,1,'J');
        $this->SetY("35");
        $this->SetX("25");
        $this->Cell(0,5,"CPF",0,1,'J');
        $this->SetY("35");
        $this->SetX("48");
        $this->Cell(0,5,"Nome",0,1,'J');
        $this->SetY("35");
        $this->SetX("120");
        $this->Cell(0,5,"Idade",0,1,'J');
        $this->SetY("35");
        $this->SetX("130");
        $this->Cell(0,5,utf8_decode("Dias Contribuição"),0,1,'J');
        $this->SetY("35");
        $this->SetX("163");
        $this->Cell(0,5,utf8_decode("Dias Licença"),0,1,'J');
        $this->SetY("35");
        $this->SetX("187");
        $this->Cell(0,5,"Dias Falta",0,1,'J');
        $this->SetY("35");
        $this->SetX("207");
        $this->Cell(0,5,"T.Integral / Dias",0,1,'J');
        $this->Cell(0,0,'',1,1,'L');
        $this->SetY("35");
        $this->SetX("240");
        $this->Cell(0,5,"T.Integral / Anos",0,1,'J');
        $this->Cell(0,0,'',1,1,'L');
           $this->SetY("35");
        $this->SetX("268");
        $this->Cell(0,5,utf8_decode("Situação"),0,1,'J');
        $this->Cell(0,0,'',1,1,'L');
        $this->Ln();
    }

    function ColumnDetail() {

      
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instancia um repositorio para Carros
        $repository = new TRepository('vw_aposentadoriaRecord');

        // cria um criterio de selecao, ordenado pelo id
        $criteria = new TCriteria;
        
        $criteria->add(new TFilter('empresa_id', '=', $_SESSION['empresa_id']));

        if ($_REQUEST['situacao'] == 'TODOS') {

        }else {
        if ($_REQUEST['situacao'] == 'APTOS') {
       $criteria->add(new TFilter('situacao', '=', substr($_REQUEST['situacao'], 0, 4)));
       $criteria->setProperty('order', 'nome, situacao');
        }else{
       $criteria->add(new TFilter('situacao', '=', substr($_REQUEST['situacao'], 0, 6)));
       $criteria->setProperty('order', 'nome, situacao');   
        }
       }
       

        $this->SetY(42);

        $i = 0;

        // carrega os objetos de acordo com o criterio
        $rows = $repository->load($criteria);

        if ($rows) {
            // percorre os objetos retornados
            foreach ($rows as $row) {

                // adiciona os dados do perfil do usuario no menu
                if ($row->matricula == '') {
                    $this->SetFont('arial','',15);
                    $this->SetX("15");
                    $this->Cell(0,5,utf8_decode("Nao existem dados cadastrados para os  parametros desta consulta"),0,1,'J');
                    $this->Ln();
                }

                //define a fonte a ser usada
                $this->SetFont('arial','',9);

                $this->SetX("10");
                $this->Cell(0,5,utf8_decode($row->matricula),0,0,'J');

                $this->SetX("25");
                $this->Cell(0,5,utf8_decode($row->cpf),0,0,'J');

                $this->SetX("48");
                $this->Cell(0,5,utf8_decode(substr($row->nome, 0, 35)),0,0,'J');


                $this->SetX("120");
                $this->Cell(0,5,utf8_decode($row->idade),0,0,'J');

                $this->SetRightMargin("150");
                $this->SetX("130");
                $this->Cell(0,5,utf8_decode($row->diascontribuicao),0,0,'R');

                $this->SetRightMargin("120");
                $this->SetX("163");
                $this->Cell(0,5,utf8_decode($row->diaslicenca),0,0,'R');

                $this->SetRightMargin("100");
                $this->SetX("187");
                $this->Cell(0,5,utf8_decode($row->diasfalta),0,0,'R');

                $this->SetRightMargin("70");
                $this->SetX("207");
                $this->Cell(0,5,utf8_decode($row->tempointegraldias),0,0,'R');
               
                $this->SetRightMargin("40");
                $this->SetX("240");
                $this->Cell(0,5,utf8_decode($row->tempointegral),0,0, 'R');
               
                $this->SetX("268");
                $this->Cell(0,5,utf8_decode($row->situacao),0,0,'J');

                $this->Ln();
   $this->SetRightMargin("10");
                //contador para gerar total
                $i = ($i + 1);
                $t = ($i);

            }

            $this->SetFont('arial','B',9);
            $this->SetX("10");
            $this->Cell(0,5,"Total de Servidores: ".$t,0,1,'J');


        }

        TTransaction::close();


    }

//Page footer
    function Footer() {
        //Position at 1.5 cm from bottom
        $this->SetY(-15);
        //Arial italic 8
        $this->SetFont('Arial','I',8);
        //Page number
        //data atual
        $data=date("d/m/Y H:i:s");
        $conteudo="impresso em ".$data;
        $texto=$_SESSION['empresa_sitio'];
        //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
        $this->Cell(0,0,'',1,1,'L');

        //imprime uma celula... largura,altura, texto,borda,quebra de linha, alinhamento
        $this->Cell(0,5,$texto,0,0,'L');
        $this->Cell(0,5,'Pag. '.$this->PageNo().' de '.'{nb}'.' - '.$conteudo,0,0,'R');
        $this->Ln();

    }
}

//Instanciation of inherited class
$pdf=new RelatorioProjecaoAposentadoriaPDF("L","mm","A4");

//define o titulo
$pdf->SetTitle("Relatorio de projeção para aposentadoria");

//assunto
$pdf->SetSubject("Relatorio de projeção para aposentadoria");

$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times', '', 12);
$pdf->ColumnDetail();
$file = "app/reports/RelatorioProjecaoAposentadoriaPDF".$_SESSION['servidor_id'].".pdf";
//abrir pdf
$pdf->Output($file);
$pdf->openFile($file);

?>
