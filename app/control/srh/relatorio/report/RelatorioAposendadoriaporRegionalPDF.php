<?php

use FPDF;
use Adianti\Database\TTransaction;
use Adianti\Database\TRepository;
use Adianti\Database\TCriteria;
use Adianti\Database\TFilter;

include_once 'app/lib/funcdate.php';

class RelatorioAposendadoriaporRegionalPDF extends FPDF {

//Page header
    function Header() {
        $regional = $_REQUEST['nome_regional'];
        //endereco da imagem,posicao X(horizontal),posicao Y(vertical), tamanho altura, tamanho largura
        $this->Image("app/images/logo_relatorio.jpg", 8, 11, 26, 18);

        //Arial bold 15
        $this->SetFont('Arial', 'B', 12);
        $this->SetY("12");
        $this->SetX("35");
        $this->Cell(0, 5, utf8_decode("GOVERNO DO ESTADO DO RIO GRANDE DO NORTE"), 0, 1, 'J');

        $this->SetY("17");
        $this->SetX("35");
        $this->Cell(0, 5, utf8_decode("INSTITUTO DE ASSISTENCIA TECNICA E EXTENSAO RURAL - EMATER"), 0, 1, 'J');

        $this->SetY("22");
        $this->SetX("35");
        $this->Cell(0, 5, utf8_decode("UNIDADE INSTRUMENTAL DE RECURSOS HUMANOS - UIRH"), 0, 1, 'J');


        $titulo = "RELATÓRIO DE APOSENTADORIA POR REGIONAL";
        $regional = "Regional: " . $_REQUEST['nome_regional'];
        $this->SetY("27");
        $this->SetX("35");
        $this->Cell(0, 5, utf8_decode($titulo), 0, 1, 'J');

        $this->SetY("34");
        $this->SetX("5");
        $this->Cell(0, 5, utf8_decode($regional), 0, 1, 'J');
        
        $this->Ln(5);

        $this->ColumnHeader();
    }

     function ColumnHeader() {
        //define a fonte a ser usada
        $this->SetFont('arial', 'B', 9);
        $this->SetY("40");
        $this->SetX("8");
        $this->Cell(0, 5, "MATRICULA", 0, 1, 'J');
        $this->SetY("40");
        $this->SetX("30");
        $this->Cell(0, 5, "NOME", 0, 1, 'J');
        $this->SetY("40");
        $this->SetX("105");
        $this->Cell(0, 5, utf8_decode("MUNICIPIO"), 0, 1, 'J');
        $this->SetY("40");
        $this->SetX("150");
        $this->Cell(0, 5, utf8_decode('FORMAÇÃO'), 0, 1, 'J');

        //$this->SetX("4");
        $this->Cell(0, 0, '', 1, 1, 'L');
        $this->Ln(5);
    }


    function ColumnDetail() {

        $regional = $_REQUEST['nome_regional'];

        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // cria um criterio de selecao, ordenado pelo id
        $criteria = new TCriteria;

        // instancia um repositorio 
        $repository = new TRepository('vw_aposentadoria_por_regional_relatorioRecord');
        $criteria->add(new TFilter('nome_regional', '=', $regional));
        $criteria->add(new TFilter('empresa_id', '=', $_SESSION['empresa_id']));
        $criteria->setProperty('order', 'nome');
        // carrega os objetos de acordo com o criterio
        $cadastros = $repository->load($criteria);
        $this->SetY(45);
        $i = 0;
        // carrega os objetos de acordo com o criterio
        $rows = $repository->load($criteria);

        if ($rows) {
            // percorre os objetos retornados
            foreach ($rows as $row) {

                // adiciona os dados do perfil do usuario no menu
                if ($row->nome == '') {
                    $this->SetFont('arial', '', 15);
                    $this->SetX("15");
                    $this->Cell(0, 5, utf8_decode("Nao existem dados cadastrados para os  parametros desta consulta"), 0, 1, 'J');
                    $this->Ln();
                }

                //define a fonte a ser usada
                $this->SetFont('arial', '', 9);
                
                if ($row->nome != $nome_servidor) {
                $this->SetX("8");
                $this->Cell(0, 5, utf8_decode($row->matricula), 0, 0, 'J');

                $this->SetX("30");
                $this->Cell(0, 5, utf8_decode(substr($row->nome, 0, 35)), 0, 0, 'J');

                $this->SetX("105");
                $this->Cell(0, 5, utf8_decode(substr($row->nome_municipio, 0, 23)), 0, 0, 'J');

                $this->SetX("150");
                $this->Cell(0, 5, utf8_decode(substr($row->nome_formacao, 0, 26)), 0, 0, 'J');
                    $nome_servidor = $row->nome;
                } elseif ($row->nome == $nome_servidor) {
                    $this->SetX("30");
                    $this->Cell(0, 5, utf8_decode("Formação: " . substr($row->nome_formacao, 0, 28)), 0, 0, 'J');
                    $nome_servidor = $row->nome;
                    $i--;
                }
                $this->Ln();

                //contador para gerar total
                $i = ($i + 1);
                $t = ($i);
            }

            $this->SetFont('arial', 'B', 9);
            $this->SetX("10");
            $this->Cell(0, 5, "Total de Servidores: " . $t, 0, 1, 'J');
        }

        TTransaction::close();
    }

//Page footer
    function Footer() {
        //Position at 1.5 cm from bottom
        $this->SetY(-15);
        //Arial italic 8
        $this->SetFont('Arial', 'I', 8);
        //Page number
        //data atual
        $data = date("d/m/Y H:i:s");
        $conteudo = "impresso em " . $data;
        $texto = "http://www.emater.rn.gov.br";
        //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
        $this->Cell(0, 0, '', 1, 1, 'L');

        //imprime uma celula... largura,altura, texto,borda,quebra de linha, alinhamento
        $this->Cell(0, 5, $texto, 0, 0, 'L');
        $this->Cell(0, 5, 'Pag. ' . $this->PageNo() . ' de ' . '{nb}' . ' - ' . $conteudo, 0, 0, 'R');
        $this->Ln();
    }

}

//Instanciation of inherited class
$pdf = new RelatorioAposendadoriaporRegionalPDF("P", "mm", "A4");

//define o titulo
$pdf->SetTitle("Relatorio dos Servidores Ativos por Cargo - EMATER-RN");

//assunto
$pdf->SetSubject("Relatorio dos Servidores Ativos por Cargo - EMATER-RN");

$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times', '', 12);
$pdf->ColumnDetail();
$file = "app/reports/RelatorioAposendadoriaporRegionalPDF" . $_SESSION['servidor_id'] . ".pdf";
//abrir pdf
$pdf->Output($file);
$pdf->openFile($file);

?>