<?php

use Adianti\Database\TTransaction;
use Adianti\Database\TRepository;
use Adianti\Database\TCriteria;
use Adianti\Database\TFilter;

class RelatorioPersonalizadoServidorPDF extends FPDF {

//Page header
    function Header() {
        //parametros do form
        $opcao = $_REQUEST['opcao'];
        $vinculo = $_REQUEST['vinculo'];
        $situacao = $_REQUEST['situacao'];
        $regiao = $_REQUEST['regiao'];
        $tipo = $_REQUEST['tipo'];

        //endereco da imagem,posicao X(horizontal),posicao Y(vertical), tamanho altura, tamanho largura
        $this->Image("app/images/logo_relatorio.jpg", 8, 11, 18, 18);

        //Arial bold 15
        $this->SetFont('Arial', 'B', 12);
        $this->SetY("12");
        $this->SetX("25");
        $this->Cell(0, 5, utf8_decode("GOVERNO DO ESTADO DO RIO GRANDE DO NORTE"), 0, 1, 'C');

        $this->SetY("17");
        $this->SetX("25");
        $this->Cell(0, 5, utf8_decode( $_SESSION['empresa_nome'] ), 0, 1, 'C');

        if ($opcao == '') {
        	
        	$titulo = "RELATÓRIO DE SERVIDORES";
        	$this->SetY("25");
        	$this->SetX("25");
        	$this->Cell(0, 5, utf8_decode($titulo), 0, 1, 'C');
            
        } else {
            if ($opcao == 1) {
//define a fonte a ser usada
                if ($situacao == 'TODAS') {
                    $titulo = "RELATÓRIO DE SERVIDORES EM CESSÃO";
                    $this->SetY("30");
                    $this->SetX("25");
                    $this->Cell(0, 5, utf8_decode($titulo), 0, 1, 'C');
                } else {
                    $titulo = "RELATÓRIO DE SERVIDORES EM CESSÃO - " . $situacao . "";
                    $this->SetY("30");
                    $this->SetX("25");
                    $this->Cell(0, 5, utf8_decode($titulo), 0, 1, 'C');
                }
            } else {
                if ($opcao == 2) {
                    if ($situacao == 'TODAS') {
                        $titulo = "RELATÓRIO DE SERVIDORES COM FORMAÇÃO";
                        $this->SetY("30");
                        $this->SetX("25");
                        $this->Cell(0, 5, utf8_decode($titulo), 0, 1, 'C');
                    } else {
                        $titulo = "RELATÓRIO DE SERVIDORES COM FORMAÇÃO - " . $situacao . "";
                        $this->SetY("30");
                        $this->SetX("25");
                        $this->Cell(0, 5, utf8_decode($titulo), 0, 1, 'C');
                    }
                } else {
                    if ($opcao == 3) {
                        if ($situacao == 'TODAS') {
                            $titulo = "RELATÓRIO DE SERVIDORES DO QUADRO SUPLEMENTAR";
                            $this->SetY("30");
                            $this->SetX("25");
                            $this->Cell(0, 5, utf8_decode($titulo), 0, 1, 'C');
                        } else {
                            $titulo = "RELATÓRIO DE SERVIDORES DO QUADRO SUPLEMENTAR - " . $situacao . "";
                            $this->SetY("30");
                            $this->SetX("25");
                            $this->Cell(0, 5, utf8_decode($titulo), 0, 1, 'C');
                        }
                    } else {
                        if ($opcao == 4) {
                            if ($situacao == 'TODAS') {
                                $titulo = "RELATÓRIO DE SERVIDORES REDISTRIBUIDOS";
                                $this->SetY("30");
                                $this->SetX("25");
                                $this->Cell(0, 5, utf8_decode($titulo), 0, 1, 'C');
                            } else {
                                $titulo = "RELATÓRIO DE SERVIDORES REDISTRIBUIDOS - " . $situacao . "";
                                $this->SetY("30");
                                $this->SetX("25");
                                $this->Cell(0, 5, utf8_decode($titulo), 0, 1, 'C');
                            }
                        } else {
                            if ($opcao == 5) {
                                if ($situacao == 'TODAS') {
                                    $titulo = "RELATÓRIO DE SERVIDORES RELOTADOS";
                                    $this->SetY("30");
                                    $this->SetX("25");
                                    $this->Cell(0, 5, utf8_decode($titulo), 0, 1, 'C');
                                } else {
                                    $titulo = "RELATÓRIO DE SERVIDORES RELOTADOS " . $situacao . "";
                                    $this->SetY("30");
                                    $this->SetX("25");
                                    $this->Cell(0, 5, utf8_decode($titulo), 0, 1, 'C');
                                }
                            } else {
                                if ($opcao == 6) {
                                    if ($situacao == 'TODAS') {
                                        $titulo = "RELATÓRIO DE SERVIDORES POR FUNÇÃO";
                                        $this->SetY("30");
                                        $this->SetX("25");
                                        $this->Cell(0, 5, utf8_decode($titulo), 0, 1, 'C');
                                    } else {
                                        $titulo = "RELATÓRIO DE SERVIDORES POR FUNÇÃO - " . $situacao . "";
                                        $this->SetY("30");
                                        $this->SetX("25");
                                        $this->Cell(0, 5, utf8_decode($titulo), 0, 1, 'C');
                                    }
                                } else {
                                    if ($opcao == 7) {
                                        if ($situacao == 'TODAS') {
                                            $titulo = "RELATÓRIO DE SERVIDORES POR CARGO";
                                            $this->SetY("30");
                                            $this->SetX("25");
                                            $this->Cell(0, 5, utf8_decode($titulo), 0, 1, 'C');
                                        } else {
                                            $titulo = "RELATÓRIO DE SERVIDORES POR CARGO - " . $situacao . "";
                                            $this->SetY("30");
                                            $this->SetX("25");
                                            $this->Cell(0, 5, utf8_decode($titulo), 0, 1, 'C');
                                        }
                                    } else {
                                        if ($opcao == 8) {
                                            if ($situacao == 'TODAS') {
                                                $titulo = "RELATÓRIO DE SERVIDORES COM CONSELHOS DE CLASSE";
                                                $this->SetY("30");
                                                $this->SetX("25");
                                                $this->Cell(0, 5, utf8_decode($titulo), 0, 1, 'C');
                                            } else {
                                                $titulo = "RELATÓRIO DE SERVIDORES COM CONSELHOS DE CLASSE - " . $situacao . "";
                                                $this->SetY("30");
                                                $this->SetX("25");
                                                $this->Cell(0, 5, utf8_decode($titulo), 0, 1, 'C');
                                            }
                                        } else {
                                            if ($opcao == 9) {
                                                if ($situacao == 'TODAS') {
                                                    $titulo = "RELATÓRIO DE SERVIDORES COM LOTAÇÃO";
                                                    $this->SetY("30");
                                                    $this->SetX("25");
                                                    $this->Cell(0, 5, utf8_decode($titulo), 0, 1, 'C');
                                                } else {
                                                    $titulo = "RELATÓRIO DE SERVIDORES COM LOTAÇÃO - " . $situacao . "";
                                                    $this->SetY("30");
                                                    $this->SetX("25");
                                                    $this->Cell(0, 5, utf8_decode($titulo), 0, 1, 'C');
                                                }
                                            } else {
                                                if ($opcao == 10) {
                                                    if ($situacao == 'TODAS') {
                                                        $titulo = "RELATÓRIO DE SERVIDORES COM PISPASEP E DATA ADMISSÃO";
                                                        $this->SetY("30");
                                                        $this->SetX("25");
                                                        $this->Cell(0, 5, utf8_decode($titulo), 0, 1, 'C');
                                                    } else {
                                                        $titulo = "RELATÓRIO DE SERVIDORES COM PISPASEP E DATA ADMISSÃO - " . $situacao . "";
                                                        $this->SetY("30");
                                                        $this->SetX("25");
                                                        $this->Cell(0, 5, utf8_decode($titulo), 0, 1, 'C');
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        //$this->Ln(10);
        $this->Ln(3);

        $this->ColumnHeader();
    }

    function ColumnHeader() {
        $opcao = $_REQUEST['opcao'];
        //define a fonte a ser usada
        $this->SetFont('Arial', 'B', 10);
        $this->SetFillColor(162, 234, 150);
        $this->SetX("5");
        $this->Cell(0, 5, utf8_decode("Matrícula"), 1, 0, 'L', 1);

        $this->SetX("23");
        $this->Cell(0, 5, utf8_decode("Nome"), 1, 0, 'L', 1);

        $this->SetX("85");
        $this->Cell(0, 5, utf8_decode("Formação"), 1, 0, 'L', 1);



        if ($opcao == '') {
            $this->SetX("108");
            $this->Cell(0, 5, utf8_decode("Telefone"), 1, 0, 'L', 1);

            $this->SetX("125");
            $this->Cell(0, 5, utf8_decode("Celular"), 1, 0, 'L', 1);

            $this->SetX("150");
            $this->Cell(0, 5, utf8_decode("Email"), 1, 0, 'L', 1);
            $this->Ln(5);
        } else {
            if ($opcao == 1) {
                $this->SetX("108");
                $this->Cell(0, 5, utf8_decode("Vinculo"), 1, 0, 'L', 1);

                $this->SetX("125");
                $this->Cell(0, 5, utf8_decode("Lotação"), 1, 0, 'L', 1);


                $this->SetX("165");
                $this->Cell(0, 5, utf8_decode("Orgão"), 1, 0, 'L', 1);

                $this->SetX("215");
                $this->Cell(0, 5, utf8_decode("Data Inicio"), 1, 0, 'L', 1);

                $this->SetX("240");
                $this->Cell(0, 5, utf8_decode("Data Fim"), 1, 0, 'L', 1);

                $this->SetX("260");
                $this->Cell(0, 5, utf8_decode("Documento"), 1, 0, 'L', 1);
            } else {
                if ($opcao == 2) {
                    $this->SetX("108");
                    $this->Cell(0, 5, utf8_decode("Lotação"), 1, 0, 'L', 1);

                    $this->SetX("150");
                    $this->Cell(0, 5, utf8_decode("Titulação"), 1, 0, 'L', 1);

                    $this->SetX("182");
                    $this->Cell(0, 5, utf8_decode("Instituição"), 1, 0, 'L', 1);

                    $this->SetX("210");
                    $this->Cell(0, 5, utf8_decode("Conclusão"), 1, 0, 'L', 1);

                    $this->SetX("230");
                    $this->Cell(0, 5, utf8_decode("Descrição"), 1, 0, 'L', 1);
                } else {
                    if ($opcao == 4) {
                        $this->SetX("125");
                        $this->Cell(0, 5, utf8_decode("Email"), 1, 0, 'L', 1);

                        $this->SetX("185");
                        $this->Cell(0, 5, utf8_decode("Data Redistribuição"), 1, 0, 'L', 1);

                        $this->SetX("220");
                        $this->Cell(0, 5, utf8_decode("Documento Redistribuição"), 1, 0, 'L', 1);
                    } else {
                        if ($opcao == 5) {
                            $this->SetX("125");
                            $this->Cell(0, 5, utf8_decode("Email"), 1, 0, 'L', 1);
                        } else {
                            if ($opcao == 6) {
                                $this->SetX("125");
                                $this->Cell(0, 5, utf8_decode("Email"), 1, 0, 'L', 1);

                                $this->SetX("175");
                                $this->Cell(0, 5, utf8_decode("Função Atual"), 1, 0, 'L', 1);
                            } else {
                                if ($opcao == 7) {
                                    $this->SetX("108");
                                    $this->Cell(0, 5, utf8_decode("CPF"), 1, 0, 'L', 1);

                                    $this->SetX("138");
                                    $this->Cell(0, 5, utf8_decode("Cargo Atual"), 1, 0, 'L', 1);

                                    $this->SetX("200");
                                    $this->Cell(0, 5, utf8_decode("Cargo Antigo"), 1, 0, 'L', 1);

                                    $this->SetX("245");
                                    $this->Cell(0, 5, utf8_decode("N. Remun."), 1, 0, 'L', 1);

                                    $this->SetX("265");
                                    $this->Cell(0, 5, utf8_decode("N. Remun. Ant."), 1, 0, 'L', 1);
                                } else {
                                    if ($opcao == 8) {
                                        $this->SetX("108");
                                        $this->Cell(0, 5, utf8_decode("Cargo Atual"), 1, 0, 'L', 1);

                                        $this->SetX("150");
                                        $this->Cell(0, 5, utf8_decode("Formação"), 1, 0, 'L', 1);

                                        $this->SetX("190");
                                        $this->Cell(0, 5, utf8_decode("Lotação"), 1, 0, 'L', 1);

                                        $this->SetX("230");
                                        $this->Cell(0, 5, utf8_decode("Conselho"), 1, 0, 'L', 1);

                                        $this->SetX("260");
                                        $this->Cell(0, 5, utf8_decode("Número Conselho"), 1, 0, 'L', 1);
                                    } else {
                                        if ($opcao == 9) {
                                            $this->SetX("108");
                                            $this->Cell(0, 5, utf8_decode("Cargo Atual"), 1, 0, 'L', 1);

                                            $this->SetX("185");
                                            $this->Cell(0, 5, utf8_decode("Lotação"), 1, 0, 'L', 1);

                                            $this->SetX("240");
                                            $this->Cell(0, 5, utf8_decode("Vinculo"), 1, 0, 'L', 1);

                                            $this->SetX("265");
                                            $this->Cell(0, 5, utf8_decode("Data Admissão"), 1, 0, 'L', 1);
                                        } else {
                                            if ($opcao == 10) {
                                                $this->SetX("125");
                                                $this->Cell(0, 5, utf8_decode("CPF"), 1, 0, 'L', 1);

                                                $this->SetX("150");
                                                $this->Cell(0, 5, utf8_decode("Telefone"), 1, 0, 'L', 1);

                                                $this->SetX("175");
                                                $this->Cell(0, 5, utf8_decode("Celular"), 1, 0, 'L', 1);

                                                $this->SetX("200");
                                                $this->Cell(0, 5, utf8_decode("Email"), 1, 0, 'L', 1);

                                                $this->SetX("250");
                                                $this->Cell(0, 5, utf8_decode("PIS/PASEP"), 1, 0, 'L', 1);

                                                $this->SetX("275");
                                                $this->Cell(0, 5, utf8_decode("Data Adm."), 1, 0, 'L', 1);
                                            } else {
                                                if ($opcao == 3) {
                                                    $this->SetX("125");
                                                    $this->Cell(0, 5, utf8_decode("Email"), 1, 0, 'L', 1);

                                                    $this->SetX("130");
                                                    $this->Cell(0, 5, utf8_decode("CPF"), 1, 0, 'L', 1);

                                                    $this->SetX("150");
                                                    $this->Cell(0, 5, utf8_decode("Telefone"), 1, 0, 'L', 1);

                                                    $this->SetX("175");
                                                    $this->Cell(0, 5, utf8_decode("Celular"), 1, 0, 'L', 1);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            $this->Ln(5);
        }
    }

    function ColumnDetail() {
//parametros do form
        $opcao = $_REQUEST['opcao'];
        $vinculo = $_REQUEST['vinculo'];
        $situacao = $_REQUEST['situacao'];
        $regiao = $_REQUEST['regiao'];
        $tipo = $_REQUEST['tipo'];
        $ordem = $_REQUEST['ordem'];
        $formacao = $_REQUEST['formacao'];
        // $key = $_REQUEST['key'];

        $this->SetX("20");

        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instancia um repositorio para Carros
        $repository = new TRepository('vw_servidores_relatorioRecord');
        // cria um criterio de selecao, ordenado pelo id
        $criteria = new TCriteria;
        $criteria->add(new TFilter('empresa_id', '=', $_SESSION['empresa_id']));

        if ($tipo == '') {
            
        } else {
            if ($tipo == 'TODOS') {
                //filtra pelo campo selecionado pelo usu�rio / mes
            } else {
                if ($tipo == 'ATIVOS') {
                    $criteria->add(new TFilter('situacao', '<>', 'APOSENTADO(A)'));
                    $criteria->add(new TFilter('situacao', '<>', 'RESCINDIDO'));
                } else {
                    if ($tipo == 'INATIVOS') {
                        if ($tipo == 'INATIVOS' && $situacao == 'APOSENTADO(A)') {
                            $criteria->add(new TFilter('situacao', '=', 'APOSENTADO(A)'));
                        }
                        if ($tipo == 'INATIVOS' && $situacao == 'RESCINDIDO(A)') {
                            $criteria->add(new TFilter('situacao', '=', 'RESCINDIDO(A)'));
                        }
                    }
                }
            }
        }


        if ($regiao == '') {
            
        } else {
            if ($regiao == 'TODAS') {
                //filtra pelo campo selecionado pelo usu�rio / mes
                $criteria->setProperty('order', 'regional,nome');
            } else {
                //  $criteria->add(new TFilter('regiao', '=', $regiao));
                //adiciona o criterio
                $criteria->add(new TFilter('regional', '=', $regiao));
            }


            if ($vinculo == '') {
                
            } else {
                if ($vinculo == 'TODAS') {
                    //filtra pelo campo selecionado pelo usu�rio / mes
                    $criteria->setProperty('order', 'regional, nome,vinculo');
                } else {
                    //  $criteria->add(new TFilter('regiao', '=', $regiao));
                    //adiciona o criterio
                    $criteria->add(new TFilter('vinculo', '=', $vinculo));
                }
            }

            if ($situacao == '') {
                
            } else {
                if ($situacao == 'TODAS') {
                    //filtra pelo campo selecionado pelo usu�rio / mes
                    $criteria->setProperty('order', 'regional, nome,vinculo');
                } else {
                    //  $criteria->add(new TFilter('regiao', '=', $regiao));
                    //adiciona o criterio


                    $criteria->add(new TFilter('situacao', '=', $situacao));
                }
            }

            if ($formacao == '') {
                
            } else {
                if ($formacao == 'TODAS') {
                    
                } else {
                    //  $criteria->add(new TFilter('regiao', '=', $regiao));
                    //adiciona o criterio


                    $criteria->add(new TFilter('formacaorequisito', '=', $formacao));
                }
            }

            if ($opcao == '') {
                
            } else {
                if ($opcao == 1) {
                    if ($ordem == 1) {
                        $criteria->add(new TFilter('situacao', 'IN', array('A DISPOSICAO', 'CEDIDO(A)')));

                        $criteria->setProperty('order', 'regional, orgao, nome');
                    } else {
                        if ($ordem == 2) {
                            $criteria->add(new TFilter('orgao', 'is not', null));
                            $criteria->setProperty('order', 'regional, nome, orgao');
                        } else {
                            if ($ordem == 3) {
                                $criteria->add(new TFilter('orgao', 'is not', null));
                                $criteria->setProperty('order', 'regional, matricula, nome, orgao');
                            }
                        }
                    }
                } else {
                    if ($opcao == 2) {
                        if ($ordem == 1) {
                            $criteria->add(new TFilter('formacao', 'is not', null));
                            //filtra pelo campo selecionado pelo usu�rio / mes
                            $criteria->setProperty('order', 'regional, formacao, nome');
                        } else {
                            if ($ordem == 2) {
                                $criteria->add(new TFilter('formacao', 'is not', null));
                                $criteria->setProperty('order', 'regional, nome, formacao');
                            } else {
                                if ($ordem == 3) {
                                    $criteria->add(new TFilter('formacao', 'is not', null));
                                    $criteria->setProperty('order', 'regional, matricula, nome, formacao');
                                }
                            }
                        }
                    } else {
                        if ($opcao == 3) {
                            if ($ordem == 1) {
                                $criteria->add(new TFilter('quadrosuplementar', '=', 'SIM'));
                                $criteria->setProperty('order', 'regional, nome');
                            } else {
                                if ($ordem == 2) {
                                    $criteria->add(new TFilter('quadrosuplementar', '=', 'SIM'));
                                    $criteria->setProperty('order', 'regional, nome');
                                } else {
                                    if ($ordem == 3) {
                                        $criteria->add(new TFilter('quadrosuplementar', '=', 'SIM'));
                                        $criteria->setProperty('order', 'regional, matricula');
                                    }
                                }
                            }
                        } else {
                            if ($opcao == 4) {
                                if ($ordem == 1) {
                                    $criteria->add(new TFilter('redistribuido', '=', 'SIM'));
                                    $criteria->setProperty('order', 'regional, nome');
                                } else {
                                    if ($ordem == 2) {
                                        $criteria->add(new TFilter('redistribuido', '=', 'SIM'));
                                        $criteria->setProperty('order', 'regional, nome');
                                    } else {
                                        if ($ordem == 3) {
                                            $criteria->add(new TFilter('redistribuido', '=', 'SIM'));
                                            $criteria->setProperty('order', 'regional, matricula');
                                        }
                                    }
                                }
                            } else {
                                if ($opcao == 5) {
                                    if ($ordem == 1) {
                                        $criteria->add(new TFilter('relotado', '=', 'SIM'));
                                        $criteria->setProperty('order', 'regional, nome');
                                    } else {
                                        if ($ordem == 2) {
                                            $criteria->add(new TFilter('relotado', '=', 'SIM'));
                                            $criteria->setProperty('order', 'regional, nome');
                                        } else {
                                            if ($ordem == 3) {
                                                $criteria->add(new TFilter('relotado', '=', 'SIM'));
                                                $criteria->setProperty('order', 'regional, matricula');
                                            }
                                        }
                                    }
                                } else {
                                    if ($opcao == 6) {
                                        if ($ordem == 1) {
                                            $criteria->add(new TFilter('funcaoatual', 'is not', null));
                                            $criteria->setProperty('order', 'regional, funcaoatual, nome ');
                                        } else {
                                            if ($ordem == 2) {
                                                $criteria->add(new TFilter('funcaoatual', 'is not', null));
                                                $criteria->setProperty('order', 'regional, nome, funcaoatual');
                                            } else {
                                                if ($ordem == 3) {
                                                    $criteria->add(new TFilter('funcaoatual', 'is not', null));
                                                    $criteria->setProperty('order', 'regional, matricula,nome, funcaoatual');
                                                }
                                            }
                                        }
                                    } else {
                                        if ($opcao == 7) {
                                            if ($ordem == 1) {
                                                $criteria->add(new TFilter('cargonovo', 'is not', null));
                                                $criteria->setProperty('order', 'regional, cargonovo, nome');
                                            } else {
                                                if ($ordem == 2) {
                                                    $criteria->add(new TFilter('cargonovo', 'is not', null));
                                                    $criteria->setProperty('order', 'regional, nome, cargonovo');
                                                } else {
                                                    if ($ordem == 3) {
                                                        $criteria->add(new TFilter('cargonovo', 'is not', null));
                                                        $criteria->setProperty('order', 'regional, matricula, nome, cargonovo');
                                                    }
                                                }
                                            }
                                        } else {
                                            if ($opcao == 8) {
                                                if ($ordem == 1) {
                                                    //  $criteria->add(new TFilter('conselho', 'is not', null));
                                                    $criteria->setProperty('order', 'regional, conselho, nome');
                                                } else {
                                                    if ($ordem == 2) {
                                                        // $criteria->add(new TFilter('conselho', 'is not', null));
                                                        $criteria->setProperty('order', 'regional, nome, conselho');
                                                    } else {
                                                        if ($ordem == 3) {
                                                            //   $criteria->add(new TFilter('conselho', 'is not', null));
                                                            $criteria->setProperty('order', 'regional, matricula, nome, conselho');
                                                        }
                                                    }
                                                }
                                            } else {
                                                if ($opcao == 9) {
                                                    if ($ordem == 1) {
                                                        $criteria->add(new TFilter('lotacao', 'is not', null));
                                                        $criteria->setProperty('order', 'regional, lotacao, nome');
                                                    } else {
                                                        if ($ordem == 2) {
                                                            $criteria->add(new TFilter('lotacao', 'is not', null));
                                                            $criteria->setProperty('order', 'regional, nome, lotacao');
                                                        } else {
                                                            if ($ordem == 3) {
                                                                $criteria->add(new TFilter('lotacao', 'is not', null));
                                                                $criteria->setProperty('order', 'regional,matricula, nome, lotacao');
                                                            }
                                                        }
                                                    }
                                                } else {
                                                    if ($opcao == 10) {

                                                        if ($ordem == 1) {
                                                            $criteria->add(new TFilter('pispasep', 'is not', null));
                                                            $criteria->setProperty('order', 'regional, pispasep, nome');
                                                        } else {
                                                            if ($ordem == 2) {
                                                                $criteria->add(new TFilter('pispasep', 'is not', null));
                                                                $criteria->setProperty('order', 'regional, nome, pispasep');
                                                            } else {
                                                                if ($ordem == 3) {
                                                                    $criteria->add(new TFilter('pispasep', 'is not', null));
                                                                    $criteria->setProperty('order', 'regional, matricula,   nome, pispasep');
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            $i = 0;

            // carrega os objetos de acordo com o criterio
            $rows = $repository->load($criteria);

            if ($rows) {
                // percorre os objetos retornados
                if ($regiao = $rows->regional) {
                    //define a fonte a ser usada
                    $this->SetFont('Arial', 'B', 10);
                    $this->SetFillColor(162, 234, 150);

                    $regiao = $row->regional;
                    $posicao;
                    $this->SetX("5");
                    $this->Cell(0, 5, utf8_decode("Região:      " . ($row->regional)), 1, 1, 'J', 1);
                }
                foreach ($rows as $row) {

                    //  if ($row->regional == ''){
                    //  $this->SetFont('arial','',15);
                    //     $this->SetX("20");
                    //     $this->Cell(0,5,utf8_decode("Nao existem dados cadastrados para os  parametros desta consulta"),0,1,'J');
                    //  }

                    if ($i % 2 == 0) {
                        $this->SetFillColor(235, 235, 235);
                    } else {
                        $this->SetFillColor(255, 255, 255);
                    }

                    if ($regiao != $row->regional) {
                        //define a fonte a ser usada
                        $this->SetFont('Arial', 'B', 10);


                        $regiao = $row->regional;
                        $posicao;
                        $this->SetX("5");
                        $this->Cell(0, 5, utf8_decode("Região:      " . ($row->regional)), 1, 1, 'J', 1);
                    }

                    //define a fonte a ser usada
                    $this->SetFont('Arial', '', 9);


                    $this->SetX("5");
                    $this->Cell(0, 5, utf8_decode($row->matricula), 1, 0, 'L', 1);


                    $this->SetX("23");
                    $this->Cell(0, 5, utf8_decode(substr($row->nome, 0, 30)), 1, 0, 'L', 1);

                    if ($opcao == 10) {
                        $this->SetX("85");
                        $this->Cell(0, 5, utf8_decode(substr($row->formacaorequisito, 0, 18)), 1, 0, 'L', 1);
                    } else {
                        $this->SetX("85");
                        $this->Cell(0, 5, utf8_decode(substr($row->formacaorequisito, 0, 10)), 1, 0, 'L', 1);
                    }

                    if ($opcao == '') {
                        $this->SetX("108");
                        $this->Cell(0, 5, utf8_decode(substr($row->telefone, 0, 9)), 1, 0, 'L', 1);

                        $this->SetX("125");
                        $this->Cell(0, 5, utf8_decode(substr($row->celular, 0, 9)), 1, 0, 'L', 1);

                        $this->SetX("150");
                        $this->Cell(0, 5, utf8_decode(substr($row->email, 0, 35)), 1, 1, 'L', 1);
                    } else {
                        if ($opcao == 1) {

                            $this->SetX("108");
                            $this->Cell(0, 5, utf8_decode(substr($row->vinculo, 0, 7)), 1, 0, 'L', 1);

                            $this->SetX("125");
                            $this->Cell(0, 5, utf8_decode(substr($row->lotacao, 0, 19)), 1, 0, 'L', 1);

                            $this->SetX("165");
                            $this->Cell(0, 5, utf8_decode(substr($row->orgao, 0, 23)), 1, 0, 'L', 1);

                            $this->SetX("215");
                            $this->Cell(0, 5, utf8_decode($row->datainiciocessao), 1, 0, 'L', 1);

                            $this->SetX("240");
                            $this->Cell(0, 5, utf8_decode($row->datafimcessao), 1, 0, 'L', 1);

                            $this->SetX("260");
                            $this->Cell(0, 5, utf8_decode(substr($row->documento, 0, 17)), 1, 1, 'L', 1);
                        } else {
                            if ($opcao == 2) {
                                $this->SetX("108");
                                $this->Cell(0, 5, utf8_decode(substr($row->lotacao, 0, 20)), 1, 0, 'L', 1);


                                $this->SetX("150");
                                $this->Cell(0, 5, utf8_decode($row->formacao), 1, 0, 'L', 1);

                                $this->SetX("182");
                                $this->Cell(0, 5, utf8_decode(substr($row->formacaoinstituicao, 0, 13)), 1, 0, 'L', 1);

                                $this->SetX("210");
                                $this->Cell(0, 5, utf8_decode($row->formacaodataconclusao), 1, 0, 'L', 1);

                                $this->SetX("230");
                                $this->Cell(0, 5, utf8_decode(substr($row->formacaodescricao, 0, 30)), 1, 1, 'L', 1);
                            } else {
                                if ($opcao == 3) {
                                    $this->SetX("125");
                                    $this->Cell(0, 5, utf8_decode(substr($row->email, 0, 35)), 1, 1, 'L', 1);
                                } else {
                                    if ($opcao == 4) {

                                        $this->SetX("125");
                                        $this->Cell(0, 5, utf8_decode(substr($row->email, 0, 35)), 1, 0, 'L', 1);

                                        $this->SetX("185");
                                        $this->Cell(0, 5, utf8_decode($row->dataredistribuicao), 1, 0, 'L', 1);

                                        $this->SetX("220");
                                        $this->Cell(0, 5, utf8_decode($row->documentoredistribuicao), 1, 1, 'L', 1);
                                    } else {
                                        if ($opcao == 5) {
                                            $this->SetX("125");
                                            $this->Cell(0, 5, utf8_decode(substr($row->email, 0, 35)), 1, 1, 'L', 1);
                                        } else {
                                            if ($opcao == 6) {

                                                $this->SetX("125");
                                                $this->Cell(0, 5, utf8_decode(substr($row->email, 0, 35)), 1, 0, 'L', 1);

                                                $this->SetX("175");
                                                $this->Cell(0, 5, utf8_decode($row->funcaoatual), 1, 1, 'L', 1);
                                            } else {
                                                if ($opcao == 7) {
                                                    $this->SetX("108");
                                                    $this->Cell(0, 5, utf8_decode(substr($row->cpf, 0, 15)), 1, 0, 'L', 1);

                                                    $this->SetX("138");
                                                    $this->Cell(0, 5, utf8_decode(substr($row->cargonovo, 0, 32)), 1, 0, 'L', 1);

                                                    $this->SetX("200");
                                                    $this->Cell(0, 5, utf8_decode(substr($row->cargoantigo, 0, 22)), 1, 0, 'L', 1);

                                                    $this->SetX("245");
                                                    $this->Cell(0, 5, utf8_decode($row->nivelsalarial), 1, 0, 'L', 1);

                                                    $this->SetX("265");
                                                    $this->Cell(0, 5, utf8_decode(substr($row->nivelsalarialantigo, 0, 19)), 1, 1, 'L', 1);
                                                } else {
                                                    if ($opcao == 8) {
                                                        $this->SetX("108");
                                                        $this->Cell(0, 5, utf8_decode(substr($row->cargonovo, 0, 20)), 1, 0, 'L', 1);

                                                        $this->SetX("150");
                                                        $this->Cell(0, 5, utf8_decode(substr($row->formacaorequisito, 0, 20)), 1, 0, 'L', 1);

                                                        $this->SetX("190");
                                                        $this->Cell(0, 5, utf8_decode(substr($row->lotacao, 0, 17)), 1, 0, 'L', 1);


                                                        $this->SetX("230");
                                                        $this->Cell(0, 5, utf8_decode($row->conselho), 1, 0, 'L', 1);

                                                        $this->SetX("260");
                                                        $this->Cell(0, 5, utf8_decode($row->numconselho), 1, 1, 'L', 1);
                                                    } else {
                                                        if ($opcao == 9) {
                                                            $this->SetX("108");
                                                            $this->Cell(0, 5, utf8_decode(substr($row->cargonovo, 0, 40)), 1, 0, 'L', 1);

                                                            $this->SetX("185");
                                                            $this->Cell(0, 5, utf8_decode(substr($row->lotacao, 0, 29)), 1, 0, 'L', 1);

                                                            $this->SetX("240");
                                                            $this->Cell(0, 5, utf8_decode(substr($row->vinculo, 0, 16)), 1, 0, 'L', 1);

                                                            $this->SetX("265");
                                                            $this->Cell(0, 5, utf8_decode($row->dtadm), 1, 1, 'L', 1);
                                                        } else {
                                                            if ($opcao == 10) {
                                                                $this->SetX("125");
                                                                $this->Cell(0, 5, utf8_decode(substr($row->cpf, 0, 35)), 1, 0, 'L', 1);

                                                                $this->SetX("150");
                                                                $this->Cell(0, 5, utf8_decode(substr($row->telefone, 0, 9)), 1, 0, 'L', 1);

                                                                $this->SetX("175");
                                                                $this->Cell(0, 5, utf8_decode(substr($row->celular, 0, 9)), 1, 0, 'L', 1);

                                                                $this->SetX("200");
                                                                $this->Cell(0, 5, utf8_decode(substr($row->email, 0, 26)), 1, 0, 'L', 1);

                                                                $this->SetX("250");
                                                                $this->Cell(0, 5, utf8_decode($row->pispasep), 1, 0, 'L', 1);

                                                                $this->SetX("275");
                                                                $this->Cell(0, 5, utf8_decode($row->dtadm), 1, 1, 'L', 1);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if ($situacao == 'EM VACANCIA') {


                        // instancia um repositorio para Carros
                        $repository1 = new TRepository('VacanciaRecord');
                        // cria um criterio de selecao, ordenado pelo id
                        $criteria1 = new TCriteria;

                        $criteria1->add(new TFilter('servidor_id', '=', $row->id));
                        $criteria1->add(new TFilter('datafim', '>=', date("d/m/y")));

                        // carrega os objetos de acordo com o criterio
                        $rows1 = $repository1->load($criteria1);

                        if ($rows1) {


                            foreach ($rows1 as $row1) {
                                $this->SetFont('Arial', 'B', 10);
                                $this->SetX("5");
                                $this->Cell(0, 5, utf8_decode('Dados Vacância: '), 0, 0, 'L');
                                $this->SetFont('Arial', '', 10);
                                $this->SetX("35");
                                $this->Cell(0, 5, utf8_decode('Data Início: ' . formatar_data($row1->datainicio)), 0, 0, 'L');
                                $this->SetX("75");
                                $this->Cell(0, 5, utf8_decode('Data Fim: ' . formatar_data($row1->datafim)), 0, 0, 'L');
                                $this->SetX("115");
                                $this->Cell(0, 5, utf8_decode('Obs.: ' . $row1->observacao), 1, 1, 'L', 1);
                            }
                        }
                    }

                    if ($situacao == 'LICENCIADO(A)') {


                        // instancia um repositorio para Carros
                        $repository1 = new TRepository('vw_licencasRecord');
                        // cria um criterio de selecao, ordenado pelo id
                        $criteria1 = new TCriteria;

                        $criteria1->add(new TFilter('servidor_id', '=', $row->id));
                        $criteria1->add(new TFilter('datafim', '>=', date("d/m/y")));

                        // carrega os objetos de acordo com o criterio
                        $rows1 = $repository1->load($criteria1);

                        if ($rows1) {
                            foreach ($rows1 as $row1) {


                                $this->SetFont('Arial', 'B', 10);
                                $this->SetX("5");
                                $this->Cell(0, 5, utf8_decode('Dados Licença: '), 0, 0, 'L');
                                $this->SetFont('Arial', '', 10);
                                $this->SetX("35");
                                $this->Cell(0, 5, utf8_decode('Data Início: ' . formatar_data($row1->datainicio)), 0, 0, 'L');
                                $this->SetX("75");
                                $this->Cell(0, 5, utf8_decode('Data Fim: ' . formatar_data($row1->datafim)), 1, 0, 'L', 1);
                                $this->SetX("115");
                                $this->Cell(0, 5, utf8_decode('Documento: ' . ($row1->documento)), 1, 0, 'L', 1);
                                $this->SetX("200");
                                $this->Cell(0, 5, utf8_decode('Data Documento: ' . formatar_data($row1->datadocumento)), 1, 1, 'L', 1);
                                $this->SetFont('Arial', 'B', 10);
                                $this->SetX("5");
                                $this->Cell(0, 5, utf8_decode('Tipo Licença: '), 1, 0, 'L', 1);
                                $this->SetFont('Arial', '', 10);
                                $this->SetX("35");
                                $this->Cell(0, 5, utf8_decode($row1->tipolicenca), 1, 1, 'L', 1);
                            }
                        }
                    }

                    //contador para gerar total
                    $i++;
                    $t = ($i);
                    // $this->AddPage();
                }

                $this->SetFont('arial', 'B', 9);
                $this->SetX("10");
                $this->Cell(0, 5, "Total de Funcionarios: " . $t, 0, 1, 'J');
            }


            TTransaction::close();
        }
    }

//Page footer

    function Footer() {
        //Position at 1.5 cm from bottom
        $this->SetY(-15);
        //Arial italic 8
        $this->SetFont('Arial', 'I', 8);
        //Page number
        //data atual
        $data = date("d/m/Y H:i:s");
        $conteudo = "impresso em " . $data;
        $texto = $_SESSION['empresa_sitio'];
        //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
        $this->Cell(0, 0, '', 1, 1, 'L');

        //imprime uma celula... largura,altura, texto,borda,quebra de linha, alinhamento
        $this->Cell(0, 5, $texto, 0, 0, 'L');
        $this->Cell(0, 5, $conteudo, 0, 0, 'R');
        $this->Ln();
    }

}

//Instanciation of inherited class
$pdf = new RelatorioPersonalizadoServidorPDF("L", "mm", "A4");

//define o titulo
$pdf->SetTitle("Relatorio de Servidores");

//assunto
$pdf->SetSubject("Relatorio de Servidores  ");

$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times', '', 12);
$pdf->ColumnDetail();
$file = "app/reports/RelatorioPersonalizadoServidorPDF" . $_SESSION['servidor_id'] . ".pdf";
//abrir pdf
$pdf->Output($file);
$pdf->openFile($file);
?>