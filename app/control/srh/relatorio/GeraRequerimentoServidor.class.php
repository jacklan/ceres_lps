<?php

/*
 * classe GeraRequerimentoServidor
 * Autor: Jackson Meires
 * Data:08/12/2016
 */
include_once 'app/lib/funcdate.php';

//ini_set('display_errors', 1);
//ini_set('display_startup_erros', 1);
//error_reporting(E_ALL);

class GeraRequerimentoServidor extends TPage {

    private $form;     // formulario de cadastro

    /*
     * metodo construtor
     * Cria a pagina, o formulario e a listagem
     */

    public function __construct() {
        parent::__construct();

        // instancia um formulario
        $this->form = new TForm('form_busca_GeraCurriculoServidor');

        // instancia uma tabela
        $panel = new TPanelForm(700, 100);
        $panel->setColuna(150);

        // adiciona a tabela ao formulario
        $this->form->add($panel);

        // cria um r�tulo para o t�tulo
        $titulo = new TLabel('Requerimento Servidor');
        $titulo->setFontFace('Arial');
        $titulo->setFontColor('red');
        $titulo->setFontSize(12);

        // adiciona o campo Titulo
        $panel->put($titulo, $panel->getColuna(), $panel->getLinha());

        // cria os campos do formulario
        $matricula = new TEntry('matricula');
        $matricula->setSize(20);

        // cria um botao de acao (cadastrar)
        $new_button = new TButton('gerarelatorio');

        // define a acao do botao cadastrar
        $new_button->setAction($action = new TAction(array($this, 'onGenerate')), 'Gerar Relat&oacute;rio');
        $new_button->setProperty('target', "_blank");

        $panel->setLinha(50);
        // adiciona o campo
        $panel->setColuna2(120);
        // adiciona campo opcao
        $panel->putCampo($matricula, 'Matr&iacute;cula', 0, 0);

        //adiciona botao gerar grafico
        $panel->putCampo($new_button, null, 0, 1);
        // define quais sao os campos do formulario
        $this->form->setFields(array($matricula, $new_button));

        // monta a paina atraves de uma tabela
        $panel = new TPanelForm(700, 500);
        $panel->put($this->form, 0, 0);

        // adiciona a tabela a pagina
        parent::add($panel);
    }

    function onGenerate() {
        new RelatorioRequerimentoServidorPDF();
    }
    function onLoad() {
    }

}
