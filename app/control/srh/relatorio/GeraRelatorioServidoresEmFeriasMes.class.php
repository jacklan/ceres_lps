<?php

/*
 * classe GeraRelatorioServidoresEmFeriasMes
 * Relatorio de GeraRelatorioServidoresEmFeriasMes: Contem o filtro para seleção do relatório dos servidores em ferias por mes
 * Autor:Jackson Meires
 * Data:13/12/2016
 */

use Lib\Funcoes\Util;

//ini_set('display_errors', 1);
//ini_set('display_startup_erros', 1);
//error_reporting(E_ALL);

class GeraRelatorioServidoresEmFeriasMes extends TPage {

    private $form;     // formulario de cadastro

    /*
     * metodo construtor
     * Cria a pagina, o formulario e a listagem
     */

    public function __construct() {
        parent::__construct();

        // instancia um formulario
        $this->form = new TQuickForm;
        $this->form->setFormTitle('<font color="red" size="3" face="Arial"><b>Relatorio de Férias dos Servidores</b></font>');

        // cria os campos do formulario
        $regional_id = new TCombo('regional_id');
        $ano = new TCombo('ano');
        $datainicio = new TDate('datainicio');
        $datafim = new TDate('datafim');

        //add anos
        $ano->addItems(Util::retornaAnosemZero());
        $ano->setValue(date('Y'));

        $ano->setSize(10);
        $datainicio->setSize(10);
        $datafim->setSize(10);
        $regional_id->setSize(40);

        //cria a colecao da tabela estrangeira para a area conhecimento
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $repository = new TRepository('RegionalRecord');

        $criteria = new \Adianti\Database\TCriteria();
        $criteria->setProperty('order', 'nome');

        // carrega todos os objetos
        $collection = $repository->load($criteria);
        $items['0'] = 'TODAS';
        //adiciona os objetos no combo
        foreach ($collection as $object) {
            $items[$object->id] = $object->nome;
        }

        // adiciona as opcoes na combo
        $regional_id->addItems($items);
        $regional_id->setValue('0');

        // finaliza a transacao
        TTransaction::close();

        // cria um rotulo para o titulo
        $titulo = new TLabel('<div style="position:floatval; width: 200px;"> <b>* Campo obrigatorio</b></div>');
        $titulo->setFontFace('Arial');
        $titulo->setFontColor('red');
        $titulo->setFontSize(10);

        //campos obrigarorios
        $ano->addValidation('Ano', new TRequiredValidator); // required field
        $datainicio->addValidation('Data Inicio', new TRequiredValidator); // required field
        $datafim->addValidation('Data Fim', new TRequiredValidator); // required field
        
        // define os campos
        $this->form->addQuickField("Exercicio <font color=red><b>*</b></font>", $ano, 10);
        $this->form->addQuickField("In&iacute;cio <font color=red><b>*</b></font>", $datainicio, 14);
        $this->form->addQuickField("Fim <font color=red><b>*</b></font>", $datafim, 14);
        $this->form->addQuickField("Regional <font color=red><b>*</b></font>", $regional_id, 20);
        $this->form->addQuickField(null, $titulo, 50);

        //Define o auto-sugerir
        $ano->setProperty('placeholder', '2000');
        $datainicio->setProperty('placeholder', '20/01/2000');
        $datafim->setProperty('placeholder', '20/01/2000');

        // cria um botao de acao
        $this->form->addQuickAction('Gerar Relat&oacute;rio', new TAction(array($this, 'onGenerate')));

        // adiciona a tabela a pagina
        parent::add($this->form);
    }

    /*
     * metodo onGenerate()
     * Carrega a pagina do relatorio
     */

    function onGenerate() {

        try {
            $dados = $this->form->getData();

            // form validation
            $this->form->validate();
            
            $this->form->setData($dados);
            new RelatorioServidoresEmFeriasMesesPDF();
        } catch (Exception $ex) {
            
        }
    }

}
