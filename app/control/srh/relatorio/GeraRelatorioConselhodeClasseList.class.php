<?php

/*
 * classe GeraRelatorioSolicitacaoList
 * Cadastro de GeraRelatorioSolicitacao: Contem a listagem e o formulario de busca
 */

include_once 'app/lib/funcdate.php';

//ini_set('display_errors', 1);
//ini_set('display_startup_erros', 1);
//error_reporting(E_ALL);


class GeraRelatorioConselhodeClasseList extends TPage {

    private $form;     // formulario de cadastro
    private $datagrid; // listagem

    public function __construct() {
        parent::__construct();


        // instancia um formulario
        $this->form = new TForm('form_busca_GeraRelatorioConselhodeClasseServidor');
        //$this->form->setNewWindow(false);
        //$this->form->setNewWindow(true);
        // instancia uma tabela
        $panel = new TPanelForm(700, 100);
        $panel->setColuna(150);



        // adiciona a tabela ao formulario
        $this->form->add($panel);

        // cria um r�tulo para o t�tulo
        $titulo = new TLabel('Relat&oacute;rio de Conselho de Classe');
        $titulo->setFontFace('Arial');
        $titulo->setFontColor('red');
        $titulo->setFontSize(12);

        // adiciona o campo Titulo
        $panel->put($titulo, $panel->getColuna(), $panel->getLinha());


        // cria os campos do formulario
        $conselho = new TCombo('conselho');
        $tipo = new TCombo('tipo');
        $regiaosl = new TCombo('regiao');
        $vinculo = new TCombo('vinculo');
        $situacao = new TCombo('situacao');
        $ordem = new TCombo('ordem');



        //Cria um vetor com as opcoes da combo situacao
        $items = array();
        $items['INATIVOS'] = 'ATIVOS';
        $items['ATIVO'] = 'INATIVOS';
        $items['TODOS'] = 'TODOS';

        // adiciona as opcoes na combo
        $tipo->addItems($items);
        //coloca o valor padrao no combo
        $tipo->setValue('ATIVO');
        $tipo->setSize(40);

//Cria um vetor com as opcoes da combo situacao
        $items5 = array();
        $items5['1'] = 'CAMPO ESPECIFICO';
        $items5['2'] = 'NOME';
        $items5['3'] = 'MATRICULA';

        // adiciona as opcoes na combo
        $ordem->addItems($items5);
        //coloca o valor padrao no combo
        $ordem->setValue('1');
        $ordem->setSize(40);

        //cria a colecao da tabela estrangeira
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $repository = new TRepository('vw_conselhos_de_classeRecord');
        // cria um criterio de selecao, ordenado pelo id
        $criteria = new TCriteria;
        // carrega os objetos de acordo com o criterio
        $cadastros = $repository->load($criteria);
        //adiciona os objetos no combo

        foreach ($cadastros as $object) {
            $items6[$object->conselho] = $object->conselho;
        }
        TTransaction::close();
        $items6['TODAS'] = 'TODAS';

        //cria os campos com opcoes de grafico
        $conselho->addItems($items6);
        //coloca o valor padrão
        $conselho->setValue('TODAS');
        $conselho->setSize(40);

        //cria a colecao da tabela estrangeira
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $repository = new TRepository('vw_regional_servidorRecord');
        // cria um criterio de selecao, ordenado pelo id
        $criteria = new TCriteria;
        // carrega os objetos de acordo com o criterio
        $cadastros = $repository->load($criteria);
        //adiciona os objetos no combo

        foreach ($cadastros as $object) {
            $items2[$object->regional] = $object->regional;
        }
        TTransaction::close();
        $items2['TODAS'] = 'TODAS';

        //cria os campos com opcoes de grafico
        $regiaosl->addItems($items2);
        //coloca o valor padrão
        $regiaosl->setValue('TODAS');
        $regiaosl->setSize(40);

        //cria a colecao da tabela estrangeira
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $repository = new TRepository('vw_vinculo_servidorRecord');
        // cria um criterio de selecao, ordenado pelo id
        $criteria = new TCriteria;
        // carrega os objetos de acordo com o criterio
        $cadastros = $repository->load($criteria);
        //adiciona os objetos no combo

        foreach ($cadastros as $object) {
            $items3[$object->vinculo] = $object->vinculo;
        }
        TTransaction::close();
        $items3['TODAS'] = 'TODAS';

        //cria os campos com opcoes de grafico
        $vinculo->addItems($items3);
        //coloca o valor padrão
        $vinculo->setValue('TODAS');
        $vinculo->setSize(40);


        //cria a colecao da tabela estrangeira
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $repository = new TRepository('vw_situacao_servidorRecord');
        // cria um criterio de selecao, ordenado pelo id
        $criteria = new TCriteria;
        // carrega os objetos de acordo com o criterio
        $cadastros = $repository->load($criteria);
        //adiciona os objetos no combo

        foreach ($cadastros as $object) {
            if ($object->situacao == 'CEDIDO(A)') {
                $items4[$object->situacao] = 'CEDIDO A OUTROS ORGÃOS';
            } else {
                if ($object->situacao == 'A DISPOSICAO') {
                    $items4[$object->situacao] = 'CEDIDO A EMATER';
                } else {
                    $items4[$object->situacao] = $object->situacao;
                }
            }
        }
        TTransaction::close();
        $items4['TODAS'] = 'TODAS';

        //cria os campos com opcoes de grafico
        $situacao->addItems($items4);
        //coloca o valor padrão
        $situacao->setValue('TODAS');
        $situacao->setSize(40);


        $panel->setLinha(50);
        // adiciona o campo

        $panel->setColuna2(160);

        // adiciona campo opcao
        $panel->putCampo($tipo, 'Tipo', 0, 0);

        // adiciona campo opcao
        $panel->putCampo($regiaosl, 'Regional', 0, 1);


        // adiciona campo opcao tipo funcionario
        $panel->putCampo($vinculo, 'V&iacute;nculo', 0, 1);

        // adiciona campo opcao tipo funcionario
        $panel->putCampo($situacao, 'Situa&ccedil;&atilde;o', 0, 1);

        // adiciona campo opcao conselho
        $panel->putCampo($conselho, 'Conselho de Classe', 0, 1);

        // adiciona campo opcao tipo funcionario
        $panel->putCampo($ordem, 'Ordenar por', 0, 1);

        // cria um botao de acao (cadastrar)
        $new_button = new TButton('gerarelatorio');

        $new_button->setAction(new TAction(array($this, 'onGenerate')), 'Gerar Relat&oacute;rio');

        //adiciona botao gerar grafico
        $panel->putCampo($new_button, null, 0, 1);

        // define quais sao os campos do formulario
        $this->form->setFields(array($new_button));

        // monta a paina atraves de uma tabela
        $panel = new TPanelForm(700, 500);
        $panel->put($this->form, 0, 0);


        // adiciona a tabela a pagina
        parent::add($panel);
    }

    function onGenerate() {

        new RelatorioConselhodeClasseServidorPDF();
    }

}

?>