<?php

/*
 * classe GeraRelatorioSolicitacaoList
 * Cadastro de GeraRelatorioSolicitacao: Contem a listagem e o formulario de busca
 */
include_once 'app.library/funcdate.php';
include_once 'app.library/fpdf/fpdf.php';

//include_once 'app.pdf/RelatorioServidorPDF.php';

class GeraRelatorioSetorGrupoList extends TPage 
{

    private $form;     // formulario de cadastro
    private $datagrid; // listagem

    /*
     * metodo construtor
     * Cria a pagina, o formulario e a listagem
     */
    public function __construct() 
    {
    	
        parent::__construct();

        // instancia um formulario
        $this->form = new TForm('form_busca_GeraRelatorioConselhodeClasseServidor');
        //$this->form->setNewWindow(false);
        //$this->form->setNewWindow(true);
        // instancia uma tabela
        $panel = new TPanelForm(700, 100);
        $panel->setColuna(150);

        // adiciona a tabela ao formulario
        $this->form->add($panel);

        // cria um r�tulo para o t�tulo
        $titulo = new TLabel('Relat&oacute;rio Personalizado de Servidores');
        $titulo->setFontFace('Arial');
        $titulo->setFontColor('red');
        $titulo->setFontSize(12);

        // adiciona o campo Titulo
        $panel->put($titulo, $panel->getColuna(), $panel->getLinha());

        // cria os campos do formulario
        $setorgrupo = new TCombo('nomesetorgrupo');
        // $nomeservidor = new TCombo('nomeservidor');
        $nomesetor = new TCombo('nomesetor');

        //cria a colecao da tabela estrangeira
        // inicia transacao com o banco 'pg_ceres'
        $items = array();
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $repository = new TRepository('vw_setorgrupoRecord');
        // cria um criterio de selecao, ordenado pelo id
        $criteria = new TCriteria;
        
        $criteria->setProperty('order', 'nomesetorgrupo');
        $criteria->add( new TFilter( 'empresa_id', '=', $_SESSION['empresa_id'] ) );
        
        // carrega os objetos de acordo com o criterio
        $cadastros = $repository->load($criteria);
        //adiciona os objetos no combo

        foreach ($cadastros as $object) {
            $items[$object->nomesetorgrupo] = $object->nomesetorgrupo;
        }
        TTransaction::close();
        $items['TODOS'] = 'TODOS';

        //cria os campos com opcoes de grafico
        $setorgrupo->addItems($items);
        //coloca o valor padrão
        $setorgrupo->setValue('TODOS');
        $setorgrupo->setSize(40);

        //cria a colecao da tabela estrangeira
        // inicia transacao com o banco 'pg_ceres'
        $items3 = array();
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $repository = new TRepository('vw_setorgrupoRecord');
        // cria um criterio de selecao, ordenado pelo id
        $criteria = new TCriteria;
        
        $criteria->setProperty('order', 'nomesetor');
        $criteria->add( new TFilter( 'empresa_id', '=', $_SESSION['empresa_id'] ) );
        
        // carrega os objetos de acordo com o criterio
        $cadastros = $repository->load($criteria);
        //adiciona os objetos no combo

        foreach ($cadastros as $object) {
            $items3[$object->nomesetor] = $object->nomesetor;
        }
        TTransaction::close();
        $items3['TODOS'] = 'TODOS';

        //cria os campos com opcoes de grafico
        $nomesetor->addItems($items3);
        //coloca o valor padrão
        $nomesetor->setValue('TODOS');
        $nomesetor->setSize(40);

        $panel->setLinha(50);
        // adiciona o campo

        $panel->setColuna2(160);

        // adiciona campo opcao
        //$panel->putCampo($nomeservidor, 'Servidor', 0, 1);
        // adiciona campo opcao tipo funcionario
        $panel->putCampo($nomesetor, 'Setor', 0, 1);

        // adiciona campo opcao conselho
        $panel->putCampo($setorgrupo, 'Setor Grupo', 0, 1);

        // cria um botao de acao (cadastrar)
        $new_button = new TButton('gerarelatorio');

        //$new_button->setAction(new TAction(array('RelatorioSetorGrupoPDF', '')), 'Gerar Relat&oacute;rio');
        $new_button->setAction(new TAction(array($this, 'onGenerate')), 'Gerar Relat&oacute;rio');

        //adiciona botao gerar grafico
        $panel->putCampo($new_button, null, 0, 1);
        // define quais sao os campos do formulario
        $this->form->setFields(array($new_button, /* $nomeservidor , */ $nomesetor, $setorgrupo));

        // monta a paina atraves de uma tabela
        $panel = new TPanelForm(700, 500);
        $panel->put($this->form, 0, 0);

        // adiciona a tabela a pagina
        parent::add($panel);
        
    }

    function onGenerate() 
    {

        new RelatorioSetorGrupoPDF();
        
    }

}
?>