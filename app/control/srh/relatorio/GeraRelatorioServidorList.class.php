<?php

ini_set('display_errors', 1);
ini_set('display_startup_erros', 1);
error_reporting(E_ALL);
/*
 * classe GeraRelatorioServidorList
 * Cadastro de GeraRelatorioServidorList: Contem a listagem e o formulario de busca
 * Autor:Jackson Meires
 * Data:02/12/2015
 */

class GeraRelatorioServidorList extends TPage {

    private $form;     // formulario de cadastro

    /*
     * metodo construtor
     * Cria a pagina, o formulario e a listagem
     */

    public function __construct() {
        parent::__construct();

        // instancia um formulario
        $this->form = new TForm('form_busca_GeraRelatorioServidor');
//        $this->form->setNewWindow(true);
        // instancia uma tabela
        $panel = new TPanelForm(700, 100);
        $panel->setColuna(150);

        // adiciona a tabela ao formulario
        $this->form->add($panel);

        // cria um r�tulo para o t�tulo
        $titulo = new TLabel('Gerar Relatorio de Servidores');
        $titulo->setFontFace('Arial');
        $titulo->setFontColor('red');
        $titulo->setFontSize(18);

        // adiciona o campo Titulo
        $panel->put($titulo, $panel->getColuna(), $panel->getLinha());

        // cria os campos do formulario
        $tipo = new TCombo('tipo');
        $regiaosl = new TCombo('regiao');
        $tipofuncionario = new TCombo('tipofuncionario');

        //Cria um vetor com as opcoes da combo situacao
        $items = array();
        $items['ATIVOS'] = 'ATIVOS';
        $items['INATIVOS'] = 'INATIVOS';
        $items['TODOS'] = 'TODOS';

        // adiciona as opcoes na combo
        $tipo->addItems($items);
        //coloca o valor padrao no combo
        $tipo->setValue('ATIVOS');
        $tipo->setSize(100);

        // cria vetor com as opcoes da combo mes
        $items2 = array();
        $items2['ASSU'] = 'ASSU';
        $items2['CAICO'] = 'CAICO';
        $items2['CURRAIS NOVOS'] = 'CURRAIS NOVOS';
        $items2['JOAO CAMARA'] = 'JOAO CAMARA';
        $items2['MOSSORO'] = 'MOSSORO';
        $items2['NATAL'] = 'NATAL';
        $items2['PAU DOS FERROS'] = 'PAU DOS FERROS';
        $items2['SANTA CRUZ'] = 'SANTA CRUZ';
        $items2['SAO JOSE DO MIPIBU'] = 'SAO JOSE DO MIPIBU';
        $items2['SAO PAULO DO POTENGI'] = 'SAO PAULO DO POTENGI';
        $items2['UMARIZAL'] = 'UMARIZAL';
        $items2['CENTERN'] = 'CENTERN';
        $items2['TODAS'] = 'TODAS';

        //cria os campos com opcoes de grafico
        $regiaosl->addItems($items2);
        //coloca o valor padrão
        $regiaosl->setValue('TODAS');
        $regiaosl->setSize(150);

        // cria vetor com as opcoes da combo tipo funcionario
        $items3 = array();
        $items3['ESTAGIARIO'] = 'ESTAGIARIO';
        $items3['FUNCIONARIO'] = 'FUNCIONARIO';
        $items3['BOLSISTA'] = 'BOLSISTA';
        $items3['COMISSIONADO'] = 'COMISSIONADO';
        $items3['TODOS'] = 'TODOS';

        //cria os campos com opcoes de grafico
        $tipofuncionario->addItems($items3);
        //coloca o valor padrão
        $tipofuncionario->setValue('TODOS');
        $tipofuncionario->setSize(150);

        // cria um botao de acao (cadastrar)
        $new_button = new TButton('gerarelatorio');

        // define a acao do botao cadastrar
        $new_button->setAction(new TAction(array($this, 'onGenerate')), 'Gerar Relatorio');


//        $panel->setLinha(50);
        // adiciona tamanho aos campos
        $tipo->setSize(20);
        $regiaosl->setSize(20);
        $tipofuncionario->setSize(20);
        // adiciona campo opcao
        $panel->putCampo($tipo, 'Tipo', 0, 0);

        // adiciona campo opcao
        $panel->putCampo($regiaosl, 'Regiao', 0, 1);

        // adiciona campo opcao tipo funcionario
        $panel->putCampo($tipofuncionario, 'Vinculo', 0, 1);

        //adiciona botao gerar grafico
        $panel->putCampo($new_button, null, 0, 1);
        //$panel->put($new_button, $panel->getColuna()+270,$panel->getLinha());
        // define quais sao os campos do formulario
        $this->form->setFields(array($new_button));

        // monta a paina atraves de uma tabela
        $panel = new TPanelForm(700, 500);
        $panel->put($this->form, 0, 0);

        // adiciona a tabela a pagina
        parent::add($panel);
    }

    function onGenerate() {
        $this->form->setData($this->form->getData());
        new RelatorioServidorPDF();
    }

}

?>