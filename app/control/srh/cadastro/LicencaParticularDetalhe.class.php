<?php

//ini_set('display_errors', 1);
//ini_set('display_startup_erros', 1);
//error_reporting(E_ALL);
/*
 * classe LicencaparticularDetalhe
 * Cadastro de Licencaparticular: Contem a listagem e o formulario de busca
 */

use Adianti\Widget\Datagrid\TDatagridTables;

class LicencaParticularDetalhe extends TPage {

    private $form;     // formulario de cadastro
    private $datagrid; // listagem

    /*
     * metodo construtor
     * Cria a pagina, o formulario e a listagem
     */

    public function __construct() {
        parent::__construct();

        // instancia um formulario
        $this->form = new TQuickForm;
        $this->form->class = 'form_licenca';
        $this->form->setFormTitle('<font color="red" size="3" face="Arial"><b>Detalhe de Servidores em Licen&ccedil;a</b></font>');

		// cria um rotulo para o titulo
        $titulo = new TLabel('<div style="position:floatval; width: 200px;"> <b>* Campo obrigatorio</b></div>');
        $titulo->setFontFace('Arial');
        $titulo->setFontColor('red');
        $titulo->setFontSize(10);
		
        // cria os campos do formulario
        $codigo = new THidden('id');
        $datainicio = new TDate('datainicio');
        $datafim = new TDate('datafim');
        $documento = new TEntry('documento');
        $datadocumento = new TDate('datadocumento');
        $tipodocumento = new TCombo('tipolicenca_id');
        $datainicioperiodo = new TDate('datainicioperiodo');
        $datafimperiodo = new TDate('datafimperiodo');
        $observacao = new TEntry('observacao');

        $usuarioalteracao = new THidden('usuarioalteracao');
        $dataalteracao = new THidden('dataalteracao');

        //cria a colecao da tabela estrangeira do Cabecalho
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $cadastro = new ServidorRecord(filter_input(INPUT_GET, 'fk'));
        // cria um criterio de selecao, ordenado pelo id do servidor
        //adiciona os objetos na tela com as informacoes do servidor
        if ($cadastro) {
            $nome = new TLabel($cadastro->nome);
            $matricula = new TLabel($cadastro->matricula);
        }
        // finaliza a transacao
        TTransaction::close();

        //cria a colecao da tabela estrangeira do Cabecalho
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $cadastro = new ServidorRecord($_SESSION['servidor_id']);
        // cria um criterio de selecao, ordenado pelo id do servidor
        //adiciona os objetos na tela com as informacoes do servidor
        if ($cadastro) {
            //$nome = new TLabel($cadastro->nome);
            //$usuarioalteracao->setValue($cadastro->id);
            $usuarioalteracao->setValue($_SESSION['usuario']);
            $matricula2 = new TLabel($cadastro->id);
            $matricula2->setValue($cadastro->id);
            $cadastrador_id = new TLabel($cadastro->id);
        }
        // finaliza a transacao
        TTransaction::close();

        //cria a colecao da tabela estrangeira
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $repository = new TRepository('TipoLicencaRecord');
        // cria um criterio de selecao, ordenado pelo id
        $criteria = new TCriteria;
        $criteria->setProperty('order','nome');
        // carrega os objetos de acordo com o criterio
        $cadastros = $repository->load($criteria);
        //adiciona os objetos no combo

        foreach ($cadastros as $object) {

            $items[$object->id] = $object->nome;
        }
        TTransaction::close();

        //cria os campos com opcoes de grafico
        $tipodocumento->addItems($items);

        //coloca o valor do relacionamento
        $id = new THidden('servidor_id');
        $id->setValue(filter_input(INPUT_GET, 'fk'));

        // define os campos
        $this->form->addQuickField(null, $id, 0);
        $this->form->addQuickField(null, $usuarioalteracao, 0);
        $this->form->addQuickField(null, $dataalteracao, 0);

        $this->form->addQuickField('C&oacute;digo', $codigo, 10);
        $this->form->addQuickField('Nome Servidor', $nome, 300);
        $this->form->addQuickField('Matr&iacute;cula', $matricula, 40);
        $this->form->addQuickField('Data In&iacute;cio <font color=red><b>*</b></font>', $datainicio, 20);
        $this->form->addQuickField('Data Fim <font color=red><b>*</b></font>', $datafim, 20);
        $this->form->addQuickField('Documento', $documento, 30);
        $this->form->addQuickField('Data Doc.', $datadocumento, 20);
        $this->form->addQuickField('Tipo Licen&ccedil;a', $tipodocumento, 40);
        
        // cria um rotulo para o titulo2
        $titulo2 = new TLabel('Periodo Aquisitivo');
        $titulo2->setFontFace('Arial');
        $titulo2->setFontColor('red');
        $titulo2->setFontSize(14);

        // adiciona o campos ao form
        $this->form->addQuickField(null, $titulo2, 300);
        $this->form->addQuickField('Inicio Periodo', $datainicioperiodo, 20);
        $this->form->addQuickField('Fim Periodo', $datafimperiodo, 20);
        $this->form->addQuickField('Observa&ccedil;&atilde;o', $observacao, 60);
		$this->form->addQuickField(null, $titulo, 40);

        // cria um botao salvar
        $action1 = new TAction(array($this, 'onSave'));
        $action1->setParameter('fk', filter_input(INPUT_GET, 'fk'));

        $this->form->addQuickAction('Salvar', $action1, 'ico_save.png');

        // prepara o botao para chamar o form anterior
        $action2 = new TAction(array("ServidorForm", 'onEdit'));
        $action2->setParameter('fk', filter_input(INPUT_GET, 'fk'));

        $this->form->addQuickAction('Voltar', $action2, 'ico_datagrid.gif');

        // instancia objeto DataGrid
        $this->datagrid = new TDatagridTables;

        // instancia as colunas da DataGrid
        $dgdatainicio = new TDataGridColumn('datainicio', 'Dt. In&iacute;cio', 'left', 80);
        $dgdatafim = new TDataGridColumn('datafim', 'Dt. Fim', 'left', 80);
        $dgdocumento = new TDataGridColumn('documento', 'Documento', 'left', 80);
        $dgdatadocumento = new TDataGridColumn('datadocumento', 'Dt. Documento', 'left', 80);
        $dgtipolicenca = new TDataGridColumn('nome_tipolicenca', 'Tipo', 'left', 200);
        $dgdatainicioperiodo = new TDataGridColumn('datainicioperiodo', 'Inicio P.A.', 'left', 80);
        $dgdatafimperiodo = new TDataGridColumn('datafimperiodo', 'Fim P.A.', 'left', 80);
        $dgobservacao = new TDataGridColumn('observacao', 'Observa&ccedil;&atilde;o', 'left', 80);
        $dgusuarioalteracao = new TDataGridColumn('usuarioalteracao', 'Usu&aacute;rio Altera&ccedil;&atilde;o', 'left', 80);

        // adiciona as colunas a DataGrid
        $this->datagrid->addColumn($dgdatainicio);
        $this->datagrid->addColumn($dgdatafim);
        $this->datagrid->addColumn($dgdocumento);
        $this->datagrid->addColumn($dgdatadocumento);
        $this->datagrid->addColumn($dgtipolicenca);
        $this->datagrid->addColumn($dgdatainicioperiodo);
        $this->datagrid->addColumn($dgdatafimperiodo);
        $this->datagrid->addColumn($dgobservacao);
        $this->datagrid->addColumn($dgusuarioalteracao);

        // instancia duas acoes da DataGrid
        $action1 = new TDataGridAction(array($this, 'onEdit'));
        $action1->setLabel('Editar');
        $action1->setImage('ico_edit.png');
        $action1->setField('id');
        $action1->setFk('servidor_id');

        $action2 = new TDataGridAction(array($this, 'onDelete'));
        $action2->setLabel('Deletar');
        $action2->setImage('ico_delete.png');
        $action2->setField('id');
        $action2->setFk('servidor_id');

        // adiciona as acoes a DataGrid
        $this->datagrid->addAction($action1);
        $this->datagrid->addAction($action2);

        // cria o modelo da DataGrid, montando sua estrutura
        $this->datagrid->createModel();

        // monta a paina atraves de uma tabela
        $panel = new TPanelForm(700, 500);
        $panel->put($this->form, 0, 0);
        $panel->put($this->datagrid, 150, 355);


        // adiciona a tabela a pagina
        parent::add($panel);
    }

    /*
     * metodo onReload()
     * Carrega a DataGrid com os objetos do banco de dados
     */

    function onReload() {

        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instancia um repositorio da Classe
        $repository = new TRepository('LicencaParticularRecord');

        // cria um criterio de selecao
        $criteria = new TCriteria;

        //filtra pelo campo selecionado pelo usuário
        $criteria->add(new TFilter('servidor_id', '=', filter_input(INPUT_GET, 'fk')));

        //verifica quantos registros a consulta vai retornar
        $criteria->setProperty('order', 'datainicio desc');
        // carrega os objetos de acordo com o criterio
        $cadastros = $repository->load($criteria);

        $this->datagrid->clear();
        if ($cadastros) {
            // percorre os objetos retornados
            foreach ($cadastros as $cadastro) {
                //converter datas do form para formato Brasileiro
                $cadastro->datainicio = TDate::date2br($cadastro->datainicio);
                $cadastro->datafim = TDate::date2br($cadastro->datafim);
                $cadastro->datadocumento = TDate::date2br($cadastro->datadocumento);
                $cadastro->datainicioperiodo = TDate::date2br($cadastro->datainicioperiodo);
                $cadastro->datafimperiodo = TDate::date2br($cadastro->datafimperiodo);

                // adiciona o objeto na DataGrid
                $this->datagrid->addItem($cadastro);
            }
        }
        // finaliza a transacao
        TTransaction::close();
        $this->loaded = true;
    }

    /*
     * metodo onDelete()
     * Executada quando o usuario clicar no botao excluir da datagrid
     * Pergunta ao usuario se deseja realmente excluir um registro
     */

    function onDelete($param) {
        // obtem o parametro $key
        $key = $param['key'];

        // define duas acoes
        $action1 = new TAction(array($this, 'Delete'));

        // define os parametros de cada acao
        $action1->setParameter('key', $key);

        //encaminha a chave estrangeira
        $action1->setParameter('fk', filter_input(INPUT_GET, 'fk'));
        // exibe um dialogo ao usuario
        new TQuestion('Deseja realmente excluir o registro ?', $action1);
    }

    /*
     * metodo Delete()
     * Exclui um registro
     */

    function Delete($param) {
        // obtem o parametro $key
        $key = $param['key'];
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instanicia objeto Record
        $cadastro = new LicencaparticularRecord($key);

        try {
            // deleta objeto do banco de dados
            $cadastro->delete();

            // finaliza a transacao
            TTransaction::close();
        } catch (Exception $e) { // em caso de exceção
            // exibe a mensagem gerada pela exceção
            new TMessage('error', $e->getMessage());
            // desfaz todas alterações no banco de dados
            TTransaction::rollback();
        }

        // re-carrega a datagrid
        $this->onReload();
        // exibe mensagem de sucesso
        new TMessage('info', "Registro Exclu&iacute;do com sucesso");
    }

    /*
     * metodo show()
     * Exibe a pagina
     */

    function show() {
        // carrega os dados no datagrid
        $this->onReload();
        //chama o metodo show da super classe
        parent::show();
    }

    /*
     * metodo onSave()
     * Executada quando o usuario clicar no botao scealvar do formulario
     */

    function onSave() {
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // obtem os dados no formulario em um objeto Record da Classe
        $cadastro = $this->form->getData('LicencaparticularRecord');

		//lanca o default
        $cadastro->usuarioalteracao = $_SESSION['usuario'];
        $cadastro->dataalteracao = date("d/m/Y H:i:s");
		
        //antes de armazenar verifica se algum campo eh requerido e nao foi informado
        $dados = $cadastro->toArray();

        $msg = '';
        $icone = 'info';

        if (empty($dados['datainicio'])) {
            $msg .= 'A data de in&iacute;cio deve ser informada';
        }

        if (empty($dados['datafim'])) {
            $msg .= 'A data de fim deve ser informada';
        }

        try {

            if ($msg == '') {
                // armazena o objeto
                $cadastro->store();
                $msg = 'Dados armazenados com sucesso';

                // finaliza a transacao
                TTransaction::close();
            } else {
                $icone = 'error';
            }

            if ($icone == 'error') {
                // exibe mensagem de erro
                new TMessage($icone, $msg);
				
				$this->form->setData($cadastro);
				
            } else {
                // define array acoes
                $param = array();
                $param['fk'] = $dados['servidor_id'];

                // exibe um dialogo ao usuario
                new TMessage("info", "Registro salvo com sucesso!");
                TApplication::gotoPage('LicencaParticularDetalhe', 'onReload', $param); // reload
            }
        } catch (Exception $e) { // em caso de exc
            // exibe a mensagem gerada pela exceção
            new TMessage('error', $e->getMessage());
            // desfaz todas alterações no banco de dados
            TTransaction::rollback();
			
			$this->form->setData($cadastro);

        }
    }

    /*
     * metodo onEdit()
     * Edita os dados de um registro
     */

    function onEdit($param) {
        try {
            if (isset($param['key'])) {

                // get the parameter $key
                $key = $param['key'];

                TTransaction::open('pg_ceres');   // open a transaction with database 'samples'

                $cadastro = new LicencaparticularRecord($key);

                //converter datas do form para formato Brasileiro
                $cadastro->datainicio = TDate::date2br($cadastro->datainicio);
                $cadastro->datafim = TDate::date2br($cadastro->datafim);
                $cadastro->datainicioperiodo = TDate::date2br($cadastro->datainicioperiodo);
                $cadastro->datafimperiodo = TDate::date2br($cadastro->datafimperiodo);
                $cadastro->datadocumento = TDate::date2br($cadastro->datadocumento);

                // 
                $this->form->setData($cadastro);   // fill the form with the active record data

                TTransaction::close();           // close the transaction
            } else {
                $this->form->clear();
            }
        } catch (Exception $e) { // in case of exception
            // shows the exception error message
            new TMessage('error', '<b>Error</b> ' . $e->getMessage());
            // undo all pending operations
            TTransaction::rollback();
        }
    }

}

?>
