<?php

/*
 * classe AreaProducaoDetalhe
 * Cadastro de AreaProducao: Contem a listagem e o formulario de busca
 */

use Adianti\Widget\Datagrid\TDatagridTables;

class AreaProducaoDetalhe extends TPage {

    private $form;     // formulario de cadastro
    private $datagrid; // listagem

    /*
     * metodo construtor
     * Cria a pagina, o formulario e a listagem
     */

    public function __construct() {
        parent::__construct();

        // instancia um formulario
        $this->form = new TQuickForm;
        $this->form->class = 'form_AreaProducao';
        $this->form->setFormTitle('<font color="red" size="3" face="Arial"><b>Detalhe de &Aacute;rea da Produ&ccedil;&atilde;o</b></font>');

        // cria os campos do formulario
        $codigo = new THidden('id');
        $codigo->setEditable(false);
        $areaconhecimento_id = new TCombo('areaconhecimento_id');

        //Campos Hidden
        $usuarioalteracao = new THidden('usuarioalteracao');
        $dataalteracao = new THidden('dataalteracao');

        //cria a colecao da tabela estrangeira para a area conhecimento
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $repository = new TRepository('AreaConhecimentoRecord');
        // carrega todos os objetos
        $collection = $repository->load(new TCriteria);
        //adiciona os objetos no combo
        foreach ($collection as $object) {
            $items[$object->id] = $object->nome;
        }

        // adiciona as opcoes na combo
        $areaconhecimento_id->addItems($items);

        // finaliza a transacao
        TTransaction::close();


        //cria a colecao da tabela estrangeira do Cabecalho
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $cadastro = new ProducaoRecord(filter_input(INPUT_GET, 'fk'));
        // cria um criterio de selecao, ordenado pelo id do servidor
        //adiciona os objetos na tela com as informacoes do servidor
        if ($cadastro) {
            $titulo = new TLabel($cadastro->titulo);
        }
        // finaliza a transacao
        TTransaction::close();

        //coloca o valor do relacionamento
        $id = new THidden('producao_id');
        $id->setValue(filter_input(INPUT_GET, 'fk'));

        //cria a colecao da tabela estrangeira do Cabecalho
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $cadastro = new ServidorRecord(filter_input(INPUT_GET, 'did'));
        // cria um criterio de selecao, ordenado pelo id do servidor
        //adiciona os objetos na tela com as informacoes do servidor
        if ($cadastro) {
            $nome = new TLabel($cadastro->nome);
            $matricula = new TLabel($cadastro->matricula);
        }
        // finaliza a transacao
        TTransaction::close();

        // define os campos
        $this->form->addQuickField(null, $id, 1);
        $this->form->addQuickField(null, $codigo, 10);
        $this->form->addQuickField(null, $usuarioalteracao, 10);
        $this->form->addQuickField(null, $dataalteracao, 10);

        $this->form->addQuickField('Nome Servidor', $nome, 300);
        $this->form->addQuickField('Matr&iacute;cula', $matricula, 40);
        $this->form->addQuickField('Produ&ccedil;&atilde;o', $titulo, 40);
        $this->form->addQuickField('&Aacute;rea Conhec.', $areaconhecimento_id, 40);

        // cria um botao de acao
        $action1 = new TAction(array($this, 'onSave'));
        $action1->setParameter('key', filter_input(INPUT_GET, 'fk'));
        $action1->setParameter('fk', filter_input(INPUT_GET, 'fk'));
        $action1->setParameter('did', filter_input(INPUT_GET, 'did'));

        $this->form->addQuickAction('Salvar', $action1, 'ico_save.png')->class = 'btn btn-info btnleft';

        // prepara o botao para chamar o form anterior
        $action2 = new TAction(array("ProducaoDetalhe", 'onReload'));
        $action2->setParameter('key', filter_input(INPUT_GET, 'fk'));
        $action2->setParameter('fk', filter_input(INPUT_GET, 'did'));

        $this->form->addQuickAction('Voltar', $action2, 'ico_datagrid.gif');

        // instancia objeto DataGrid
        $this->datagrid = new TDatagridTables;

        // instancia as colunas da DataGrid
        $dgproducao = new TDataGridColumn('nome_producao', 'Produ&ccedil;&atilde;o', 'left', 600);
        $dgareaconhecimento = new TDataGridColumn('nome_areaconhecimento', '&Aacute;rea Conhecimento', 'left', 500);

        // adiciona as colunas a DataGrid
        $this->datagrid->addColumn($dgproducao);
        $this->datagrid->addColumn($dgareaconhecimento);

        // instancia duas acoes da DataGrid
        $action1 = new TDataGridAction(array($this, 'onEdit'));
        $action1->setLabel('Editar');
        $action1->setImage('ico_edit.png');
        $action1->setField('id');
        $action1->setFk('producao_id');
        $action1->setParameter('did', filter_input(INPUT_GET, 'did'));

        $action2 = new TDataGridAction(array($this, 'onDelete'));
        $action2->setLabel('Deletar');
        $action2->setImage('ico_delete.png');
        $action2->setField('id');
        $action2->setFk('producao_id');
        $action2->setParameter('did', filter_input(INPUT_GET, 'did'));

        // adiciona as acoes a DataGrid
        $this->datagrid->addAction($action1);
        $this->datagrid->addAction($action2);

        // cria o modelo da DataGrid, montando sua estrutura
        $this->datagrid->createModel();

        // monta a paina atraves de uma tabela
        $panel = new TPanelForm(700, 500);
        $panel->put($this->form, 0, 0);
        $panel->put($this->datagrid, 150, 150);


        // adiciona a tabela a pagina
        parent::add($panel);
    }

    /*
     * metodo onReload()
     * Carrega a DataGrid com os objetos do banco de dados
     */

    function onReload() {

        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instancia um repositorio da Classe
        $repository = new TRepository('AreaProducaoRecord');

        // cria um criterio de selecao
        $criteria = new TCriteria;

        //filtra pelo campo selecionado pelo usuário
        $criteria->add(new TFilter('producao_id', '=', filter_input(INPUT_GET, 'fk')));

        // carrega os objetos de acordo com o criterio
        $cadastros = $repository->load($criteria);

        $this->datagrid->clear();
        if ($cadastros) {
            // percorre os objetos retornados
            foreach ($cadastros as $cadastro) {
                // adiciona o objeto na DataGrid
                $this->datagrid->addItem($cadastro);
            }
        }


        // finaliza a transacao
        TTransaction::close();
        $this->loaded = true;
    }

    /*
     * metodo onDelete()
     * Executada quando o usuario clicar no botao excluir da datagrid
     * Pergunta ao usuario se deseja realmente excluir um registro
     */

    function onDelete($param) {
        // obtem o parametro $key
        $key = $param['key'];

        // define duas acoes
        $action1 = new TAction(array($this, 'Delete'));

        // define os parametros de cada acao
        $action1->setParameter('key', $key);

        //encaminha a chave estrangeira
        $action1->setParameter('fk', filter_input(INPUT_GET, 'fk'));
        $action1->setParameter('did', filter_input(INPUT_GET, 'did'));
        // exibe um dialogo ao usuario
        new TQuestion('Deseja realmente excluir o registro ?', $action1);
    }

    /*
     * metodo Delete()
     * Exclui um registro
     */

    function Delete($param) {
        // obtem o parametro $key
        $key = $param['key'];
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instanicia objeto Record
        $cadastro = new AreaProducaoRecord($key);

        try {
            // deleta objeto do banco de dados
            $cadastro->delete();

            // finaliza a transacao
            TTransaction::close();
        } catch (Exception $e) { // em caso de exce��o
            // exibe a mensagem gerada pela exce��o
            new TMessage('error', $e->getMessage());
            // desfaz todas altera��es no banco de dados
            TTransaction::rollback();
        }

        // re-carrega a datagrid
        $this->onReload();
        // exibe mensagem de sucesso
        new TMessage('info', "Registro Excluido com sucesso");
    }

    /*
     * metodo show()
     * Exibe a pagina
     */

    function show() {
        // carrega os dados no datagrid
        $this->onReload();
        //chama o metodo show da super classe
        parent::show();
    }

    /*
     * metodo onSave()
     * Executada quando o usuario clicar no botao salvar do formulario
     */

    function onSave() {
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // obtem os dados no formulario em um objeto Record da Classe
        $cadastro = $this->form->getData('AreaProducaoRecord');
        //lanca o default
        $cadastro->usuarioalteracao = $_SESSION['usuario'];
        $cadastro->dataalteracao = date("d/m/Y H:i:s");
        //antes de armazenar verifica se algum campo eh requerido e nao foi informado
        $dados = $cadastro->toArray();

        $msg = '';
        $icone = 'info';

        if (empty($dados['areaconhecimento_id'])) {
            $msg .= 'A �?rea de Conhecimento deve ser informada';
        }

        try {

            if ($msg == '') {
                // armazena o objeto
                $cadastro->store();
                $msg = 'Dados armazenados com sucesso';

                // finaliza a transacao
                TTransaction::close();
            } else {
                $icone = 'error';
            }

            if ($icone == 'error') {
                // exibe mensagem de erro
                new TMessage($icone, $msg);
            } else {
                // define array acoes
                $param = array();
                $param['fk'] = $dados['producao_id'];
                $param['did'] = filter_input(INPUT_GET, 'did');

                // exibe um dialogo ao usuario
                new TMessage("info", "Registro salvo com sucesso!");
                TApplication::gotoPage('AreaProducaoDetalhe', 'onReload', $param); // reload
            }
        } catch (Exception $e) { // em caso de exc
            // exibe a mensagem gerada pela exce��o
            new TMessage('error', $e->getMessage());
            // desfaz todas altera��es no banco de dados
            TTransaction::rollback();
        }
    }

    /*
     * metodo onEdit()
     * Edita os dados de um registro
     */

    function onEdit($param) {
        try {

            if (isset($param['key'])) {

                // get the parameter $key
                $key = $param['key'];

                TTransaction::open('pg_ceres');   // open a transaction with database 'samples'

                $object = new AreaProducaoRecord($key);        // instantiates object City
                //converter datas do form para formato Brasileiro
                $object->datainicio = TDate::date2br($object->datainicio);
                $object->datafim = TDate::date2br($object->datafim);

                $this->form->setData($object);   // fill the form with the active record data

                TTransaction::close();           // close the transaction
            } else {

                $this->form->clear();
            }
        } catch (Exception $e) {

            // in case of exception
            // shows the exception error message
            new TMessage('error', '<b>Error</b> ' . $e->getMessage());

            // undo all pending operations
            TTransaction::rollback();
        }
    }

}

?>