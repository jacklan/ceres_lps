<?php

/*
 * classe UnidadeOperativaForm
 * Cadastro de UnidadeOperativa: Contem o formularo
 */
include_once 'app.library/funcdate.php';

class UnidadeOperativaSituacaoInternetForm extends TPage {

    private $form;     // formulario de cadastro

    /*
     * metodo construtor
     * Cria a pagina do formulario
     */

    public function __construct() {
        parent::__construct();

        // instancia um formulario
        $this->form = new TQuickForm;
        $this->form->class = 'form_UnidadeOperativa';
        $this->form->setFormTitle('<font color="red" size="3" face="Arial"><b>SITUA&Ccedil;&Atilde;O INTERNET Unidade Operativa</b></font>');

        // cria os campos do formulario
        $codigo = new THidden('id');
        $codigo->setEditable(false);
        $situacaointernet = new TCombo('situacaointernet');
        $velocidade = new TEntry('velocidade');

        $usuarioalteracao = new THidden('usuarioalteracao');
        $dataalteracao = new THidden('dataalteracao');

        // carrega o combo
        $items = array();
        $items['FUNCIONANDO'] = 'FUNCIONANDO';
        $items['PROBLEMAS'] = 'PROBLEMAS';
        $items['CORTADO'] = 'CORTADO';
        $items['SEM CONTRATO'] = 'SEM CONTRATO';
        $items['SEM PAGAMENTO'] = 'SEM PAGAMENTO';
        $items['SEM INTERNET'] = 'SEM INTERNET';

        $situacaointernet->setValue('FUNCIONANDO');
        $situacaointernet->addItems($items);

        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        $unidadeOperativaTemp = new UnidadeOperativaRecord(filter_input(INPUT_GET, 'key'));
        $nome = new TLabel($unidadeOperativaTemp->nome);

        // finaliza a transacao
        TTransaction::close();

        // define os tamanhos dos campos
        $codigo->setSize(40);
        $nome->setSize(40);
        $situacaointernet->setSize(40);
        $velocidade->setSize(40);
        //Define o auto-sugerir
        $velocidade->setProperty('placeholder', 'Ex:100kb apenas números');

        //campos obrigarorios
        $nome->addValidation('Nome', new TRequiredValidator); // required field
        $situacaointernet->addValidation('Situacao Internet', new TRequiredValidator); // required field
        
        // define os campos
        $this->form->addQuickField(null, $codigo, 10);
        $this->form->addQuickField(null, $usuarioalteracao, 10);
        $this->form->addQuickField(null, $dataalteracao, 10);
        $this->form->addQuickField("Unidade Operativa <font color=red><b>*</b></font>", $nome, 200);
        $this->form->addQuickField("Situacao Internet <font color=red><b>*</b></font>", $situacaointernet, 50);
        $this->form->addQuickField('Velocidade Internet', $velocidade, 50);

        // cria um botao de acao
        $this->form->addQuickAction('Salvar', new TAction(array($this, 'onSave')), 'ico_save.png')->class = 'btn btn-info';
        $this->form->addQuickAction('Voltar', new TAction(array('UnidadeOperativaList', 'onReload')), 'ico_datagrid.gif');

        parent::add($this->form);
    }

    /*
     * metodo onSave()
     * Executada quando o usuario clicar no botao salvar do formulario
     */

    function onSave() {
        try {
            // inicia transacao com o banco 'pg_ceres'
            TTransaction::open('pg_ceres');
            // obtem os dados no formulario em um objeto Record
            $cadastro = $this->form->getData('UnidadeOperativaRecord');

            $cadastro->usuarioalteracao = $_SESSION['usuario'];
            $cadastro->dataalteracao = date("d/m/Y H:i:s");

            //antes de armazenar verifica se algum campo eh requerido e nao foi informado
            $dados = $cadastro->toArray();

            $msg = '';
            $icone = 'info';

            if (empty($dados['situacaointernet'])) {
                $msg .= 'A Situação da Internet deve ser informado.';
            }

            // form validation
            $this->form->validate();
            
            if ($msg == '') {
                // armazena o objeto
                $cadastro->store();
                $msg = 'Dados armazenados com sucesso';

                // finaliza a transacao
                TTransaction::close();
            } else {
                $icone = 'error';
            }

            if ($icone == 'error') {
                // exibe mensagem de erro
                new TMessage($icone, $msg);
                $this->form->setData($cadastro);   // fill the form with the active record data
            } else {

                new TMessage("info", "Registro salvo com sucesso!");
                TApplication::gotoPage('UnidadeOperativaList', 'onReload'); // reload
            }
        } catch (Exception $e) { // em caso de exceção
            // exibe a mensagem gerada pela exceção
            new TMessage('error', $e->getMessage());
            $this->form->setData($this->form->getData());   // fill the form with the active record data
            // desfaz todas alterações no banco de dados
            TTransaction::rollback();
        }
    }

    /*
     * metodo onEdit()
     * Edita os dados de um registro
     */

    function onEdit($param) {
        // obtem o parametro $key
        $key = $param['key'];
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $cadastro = new UnidadeOperativaRecord($key);

        //lanca o default
        $cadastro->usuarioalteracao = $_SESSION['usuario'];
        $cadastro->dataalteracao = date("d/m/Y H:i:s");

        // lanca os dados no formulario
        $this->form->setData($cadastro);

        // finaliza a transacao
        TTransaction::close();
    }

}

?>