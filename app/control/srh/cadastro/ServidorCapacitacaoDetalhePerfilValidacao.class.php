<?php

/*
 * classe ServidorCapacitacaoDetalhePerfilValidacao
 * Cadastro de ServidorCapacitacao: Contem a listagem e o formulario de busca
 */

//ini_set('display_errors', 1);
//ini_set('display_startup_erros', 1);
//error_reporting(E_ALL);

use Adianti\Database\TFilter1;
use Adianti\Widget\Datagrid\TDatagridTables;

class ServidorCapacitacaoDetalhePerfilValidacao extends TPage {

    private $form;     // formulario de cadastro
    private $datagrid; // listagem

    /*
     * metodo construtor
     * Cria a pagina, o formulario e a listagem
     */
    public function __construct() {
        parent::__construct();
		
		// instancia um formulario
        $this->form = new TQuickForm;
        $this->form->class = 'form_ServidorCapacitacaoPerfilValidacao';
        $this->form->setFormTitle('<font color="red" size="3" face="Arial"><b>Detalhe de Capacita&ccedil;&otilde;es dos Servidores Perfil Valida&ccedil;&atilde;o</b></font>');

        // cria um rotulo para o titulo
        $titulo = new TLabel('<div style="position:floatval; width: 200px;"> <b>* Campo obrigatorio</b></div>');
        $titulo->setFontFace('Arial');
        $titulo->setFontColor('red');
        $titulo->setFontSize(10);

        // cria os campos do formulario
        $codigo = new THidden('id');
		
        $datainicio = new TEntry('datainicio');
        $datainicio->setEditable(false);
		
        $datafim = new TEntry('datafim');
        $datafim->setEditable(false);
		
        $situacao = new TEntry('situacao');
		$situacao->setEditable(false);
		
        $datasituacao = new TDate('datasituacao');
        $datasituacao->setEditable(false);
        $datasituacao->setValue(date('d/m/Y'));
		
        $capacitacao_id = new THidden('capacitacao_id');
		
        $status = new TCombo('status');

        //campos hidden
        $usuarioalteracao = new THidden('usuarioalteracao');
        $dataalteracao = new THidden('dataalteracao');

        $statusalteracao = new THidden('statusalteracao');
        $datastatus = new THidden('datastatus');
		
        //Cria um vetor com as opcoes da combo
        $items2 = array();
        $items2['VALIDADO'] = 'VALIDADO';
        $items2['INVALIDADO'] = 'INVALIDADO';

        // adiciona as opcoes na combo situacao
        $status->addItems($items2);

        //cria a colecao da tabela estrangeira do Cabecalho
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $cadastro = new ServidorRecord(filter_input ( INPUT_GET, 'fk' ));
        // cria um criterio de selecao, ordenado pelo id do servidor
        //adiciona os objetos na tela com as informacoes do servidor
        if ($cadastro) {
            $nome = new TLabel($cadastro->nome);
            $matricula = new TLabel($cadastro->matricula);
        }
        // finaliza a transacao
        TTransaction::close();

        //coloca o valor do relacionamento
        $id = new THidden('servidor_id');
        $id->setValue(filter_input ( INPUT_GET, 'fk' ));
		
		// define os campos
		$this->form->addQuickField("Nome Servidor", $nome, 300);
		$this->form->addQuickField('Matr&iacute;cula', $matricula, 50 );	
		
        //verificar se usuario clicou em editar
        if ($_REQUEST['method'] == 'onEdit') 
		{
			
			// inicia transacao com o banco 'pg_ceres'
			TTransaction::open('pg_ceres');
		
			//pega a formacao do servidor
			$servidorCapacitacaoTemp = new ServidorCapacitacaoRecord( filter_input ( INPUT_GET, 'key' ) );
		
			//Set os dados
			$capacitacao_nome = new TLabel( $servidorCapacitacaoTemp->nome_capacitacao );
		
			// finaliza a transacao
			TTransaction::close();
			
			$this->form->addQuickField('Capacita&ccedil;&atilde;o', $capacitacao_nome, 400 );

        }
		
		$this->form->addQuickField('Situa&ccedil;&atilde;o', $situacao, 20);
		$this->form->addQuickField('Data In&iacute;cio', $datainicio, 20);
		$this->form->addQuickField('Data Fim', $datafim, 20);
		$this->form->addQuickField('Status <font color=red><b>*</b></font>', $status, 30);
		
		$this->form->addQuickField(null, $id, 10);
		$this->form->addQuickField(null, $usuarioalteracao, 10);
        $this->form->addQuickField(null, $dataalteracao, 10);
        $this->form->addQuickField(null, $capacitacao_id, 10);
		$this->form->addQuickField(null, $titulo, 50);
		$this->form->addQuickField(null, $codigo, 50);
		$this->form->addQuickField(null, $statusalteracao, 50 );		
		$this->form->addQuickField(null, $datastatus, 50 );		
		
		if ($_REQUEST['method'] == 'onEdit') 
		{
			
			// define a acao do botao
			$action1 = new TAction(array($this, 'onSave'));
			$action1->setParameter('fk', filter_input ( INPUT_GET, 'fk' ));
			$action1->setParameter('key', filter_input ( INPUT_GET, 'key' ));
			
			// cria um botao de acao
			$this->form->addQuickAction('Salvar', $action1, 'ico_save.png');
		
		}
		
        $this->form->addQuickAction('Voltar', new TAction(array('ServidorCurriculoValidacaoList', 'onReload')), 'ico_datagrid.gif');

        // instancia objeto DataGrid
        $this->datagrid = new TDataGridTables;

        // instancia as colunas da DataGrid
        $dgServidorCapacitacao = new TDataGridColumn('nome_capacitacao', 'Capacita&ccedil;&atilde;o', 'left', 700);
        $dgsituacao = new TDataGridColumn('situacao', 'Situa&ccedil;&atilde;o', 'left', 200);
        $dgdatainicio = new TDataGridColumn('datainicio', 'Data In&iacute;cio', 'left', 80);
        $dgdatafim = new TDataGridColumn('datafim', 'Data Fim', 'left', 80);
        $dgstatus = new TDataGridColumn('status', 'Status', 'left', 280);

        // adiciona as colunas a DataGrid
        //$this->datagrid->addColumn($dgnome);
        $this->datagrid->addColumn($dgServidorCapacitacao);
        $this->datagrid->addColumn($dgsituacao);
        $this->datagrid->addColumn($dgdatainicio);
        $this->datagrid->addColumn($dgdatafim);
        $this->datagrid->addColumn($dgstatus);

        // instancia duas acoes da DataGrid
        $action1 = new TDataGridAction(array($this, 'onEdit'));
        $action1->setLabel('Editar');
        $action1->setImage('ico_edit.png');
        $action1->setField('id');
        $action1->setFk('servidor_id');

        $action2 = new TDataGridAction(array($this, 'onDelete'));
        $action2->setLabel('Deletar');
        $action2->setImage('ico_delete.png');
        $action2->setField('id');
        $action2->setFk('servidor_id');

        // adiciona as acoes a DataGrid
        $this->datagrid->addAction($action1);
        $this->datagrid->addAction($action2);

        // cria o modelo da DataGrid, montando sua estrutura
        $this->datagrid->createModel();

        // monta a paina atraves de uma tabela
        $panel = new TPanelForm(900, 100);
        $panel->put($this->form, 0, 0);
        if ($_REQUEST['method'] == 'onEdit') {
            $panel->put($this->datagrid, 150, 290);
        } else {
            $panel->put($this->datagrid, 150, 110);
        }

        // adiciona a tabela a pagina
        parent::add($panel);
    }

    /*
     * metodo onReload()
     * Carrega a DataGrid com os objetos do banco de dados
     */

    function onReload() {

        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instancia um repositorio da Classe
        $repository = new TRepository('ServidorCapacitacaoRecord');

        // cria um criterio de selecao
        $criteria = new TCriteria;

        //filtra pelo campo selecionado pelo usuário
        $criteria->add(new TFilter('servidor_id', '=', filter_input ( INPUT_GET, 'fk' )));

        // carrega os objetos de acordo com o criterio
        $cadastros = $repository->load($criteria);

        $this->datagrid->clear();
        if ($cadastros) {
            // percorre os objetos retornados
            foreach ($cadastros as $cadastro) {
				
				//converter datas do form para formato Brasileiro
                $cadastro->datainicio = TDate::date2br($cadastro->datainicio);
                $cadastro->datafim = TDate::date2br($cadastro->datafim);
				
                // adiciona o objeto na DataGrid
                $this->datagrid->addItem($cadastro);
            }
        }


        // finaliza a transacao
        TTransaction::close();
        $this->loaded = true;
    }

    /*
     * metodo onDelete()
     * Executada quando o usuario clicar no botao excluir da datagrid
     * Pergunta ao usuario se deseja realmente excluir um registro
     */
    function onDelete($param) {
        // obtem o parametro $key
        $key = $param['key'];
       
        // define duas acoes
        $action1 = new TAction(array($this, 'Delete'));

        // define os parametros de cada acao
        $action1->setParameter('key', $key);

        //encaminha a chave estrangeira
        $action1->setParameter('fk', filter_input ( INPUT_GET, 'fk' ));

        // exibe um dialogo ao usuario
        new TQuestion('Deseja realmente excluir o registro ?', $action1);
    }

    /*
     * metodo Delete()
     * Exclui um registro
     */

    function Delete($param) {
        // obtem o parametro $key
        $key = $param['key'];
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instanicia objeto Record
        $cadastro = new ServidorCapacitacaoRecord($key);

        try {
            // deleta objeto do banco de dados
            $cadastro->delete();

            // finaliza a transacao
            TTransaction::close();
        } catch (Exception $e) { // em caso de exceção
            // exibe a mensagem gerada pela exceção
            new TMessage('error', $e->getMessage());
            // desfaz todas alterações no banco de dados
            TTransaction::rollback();
        }

        // re-carrega a datagrid
        $this->onReload();
        // exibe mensagem de sucesso
        new TMessage('info', "Registro Excluido com sucesso");
		
    }

    /*
     * metodo show()
     * Exibe a pagina
     */
    function show() {
        // carrega os dados no datagrid
        $this->onReload();
        //chama o metodo show da super classe
        parent::show();
    }

    /*
     * metodo onSave()
     * Executada quando o usuario clicar no botao salvar do formulario
     */
    function onSave() {
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // obtem os dados no formulario em um objeto Record da Classe
        $cadastro = $this->form->getData('ServidorCapacitacaoRecord');

		//lanca o default
        $cadastro->usuarioalteracao = $_SESSION['usuario'];
        $cadastro->dataalteracao = date("d/m/Y H:i:s");
		
		//lanca o default
        $cadastro->statusalteracao = $_SESSION['usuario'];
        $cadastro->datastatus = date("d/m/Y H:i:s");
		
        //antes de armazenar verifica se algum campo eh requerido e nao foi informado
        $dados = $cadastro->toArray();

        $msg = '';
        $icone = 'info';

        if (empty($dados['status'])) {
            $msg .= 'O Status deve ser informado.';
        }

        try {

            if ($msg == '') {
                // armazena o objeto
                $cadastro->store();
                $msg = 'Dados armazenados com sucesso';

                // finaliza a transacao
                TTransaction::close();
            } else {
                $icone = 'error';
            }

            if ($icone == 'error') {
				
                // exibe mensagem de erro
                new TMessage($icone, $msg);
				
				$this->form->setData($cadastro);   // fill the form with the active record data
				
            } else {
				
				$param = array();
				$param ['fk'] = $dados['servidor_id'];
				
                //chama o formulario com o grid
                // exibe um dialogo ao usuario
                new TMessage("info", "Registro salvo com sucesso!");
				TApplication::gotoPage('ServidorCapacitacaoDetalhePerfilValidacao','onReload', $param); // reload

            }
        } catch (Exception $e) { // em caso de exc
            // exibe a mensagem gerada pela exceção
            new TMessage('error', $e->getMessage());
            // desfaz todas alterações no banco de dados
            TTransaction::rollback();
			
			$this->form->setData($cadastro);   // fill the form with the active record data
			
        }
    }

    /*
     * metodo onEdit()
     * Edita os dados de um registro
     */

    function onEdit($param) {
		
		try {
            if (isset($param['key'])) {
                // get the parameter $key
                $key = $param['key'];

                TTransaction::open('pg_ceres');   // open a transaction with database 'samples'
                $object = new ServidorCapacitacaoRecord($key);        // instantiates object City
				
                //converter datas do form para formato Brasileiro
                $object->datainicio = TDate::date2br($object->datainicio);
				$object->datafim = TDate::date2br($object->datafim);

                $this->form->setData($object);   // fill the form with the active record data
				
                TTransaction::close();           // close the transaction
            } else {
                $this->form->clear();
            }
        } catch (Exception $e) { // in case of exception
            // shows the exception error message
            new TMessage('error', '<b>Error</b> ' . $e->getMessage());
            // undo all pending operations
            TTransaction::rollback();
        }
		
    }

}

?>