<?php
/*
 * classe FaltaservidorDetalhe
 * Cadastro de Faltaservidor: Contem a listagem e o formulario de busca
 */

    //ini_set('display_errors', 1);
    //ini_set('display_startup_erros', 1);
    //error_reporting(E_ALL);


use Adianti\Database\TFilter1;
use Adianti\Widget\Datagrid\TDatagridTables;

class MeuPerfilFerias extends TPage {
    private $form;     // formulario de cadastro
    private $datagrid; // listagem

    /*
     * metodo construtor
     * Cria a pagina, o formulario e a listagem
     */
    public function __construct() {
        parent::__construct();

        // instancia um formulario
        $this->form = new TForm('form_Ferias');

        // instancia um Panel
        $panel = new TPanelForm(900, 100);

        // adiciona o panel ao formulario
        $this->form->add($panel);

        // cria um rotulo para o titulo
        $titulo = new TLabel('Cadastro de Férias dos Servidores');
        $titulo->setFontFace('Arial');
        $titulo->setFontColor('red');
        $titulo->setFontSize(12);

        // cria os campos do formulario


        $codigo = new THidden('id');
        $codigo->setEditable(false);
        $datainicio = new TDate('iniciogozo');
        $datafim = new TDate('fimgozo');
        $exercicio = new TCombo('exercicio');
        $usuarioAlteracao = new TEntry('usuarioalteracao');
        $usuarioAlteracao->setEditable(false);
        $dataAlteracao = new TDate('dataalteracao');
        $dataAlteracao->setEditable(false);

        //cria a colecao da tabela estrangeira do Cabecalho
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $cadastro = new ServidorRecord($_GET['fk']);
        // cria um criterio de selecao, ordenado pelo id do servidor
        //adiciona os objetos na tela com as informacoes do servidor
        if ($cadastro) {
            $nome = new TLabel($cadastro->nome);
            $matricula = new TLabel($cadastro->matricula);
        }
        // finaliza a transacao
        TTransaction::close();

        //Cria um vetor com as opcoes da combo
        $items= array();
        $items['2005'] = '2005';
        $items['2006'] = '2006';
        $items['2007'] = '2007';
        $items['2008']= '2008';
        $items['2009']= '2009';
        $items['2010']= '2010';
        $items['2011']= '2011';
        $items['2012']= '2012';
        $items['2013']= '2013';
        $items['2014']= '2014';
        $items['2015']= '2015';

        // adiciona as opcoes na combo situacao
        $exercicio->addItems($items);


        //coloca o valor do relacionamento
        $id = new THidden('servidor_id');
        $id->setValue($_GET['fk']);

        // define os tamanhos dos campos

        $codigo->setSize(40);
        $datainicio->setSize(80);
        $datafim->setSize(80);
        $exercicio->setSize(80);
       

        $panel->setColuna2(140);

        // adiciona o campo titulo
        $panel->put($titulo,$panel->getColuna(),$panel->getLinha());

        // adiciona o campo do id da fk
        $panel->putCampo($id, null, 0, 0);

        // adiciona o campo Nome do Servidor
        $panel->putCampo($nome, 'Nome Servidor', 0, 1);


        // adiciona o campo Matricula do Servidor
        $panel->putCampo($matricula, 'Matr&iacute;cula', 0, 1);
        /*
        // adiciona o campo de orgao codigo da tabela
        $panel->putCampo($codigo, 'C&oacute;digo' , 0 , 1);

        // adiciona o campo data inicio
        $panel->putCampo($datainicio, 'Data In&iacute;cio Gozo', 0, 1);

        $image = new TImage('app.images/ew_calendar.gif');
        $image->__set('alt', 'Selecione uma data');
        $image->__set('style', 'cursor:pointer;');
        $image->__set('id', 'cxdatainicio');
        $panel->putCampo($image, null, 105, 0);
        $script = new TScriptCalendar('scriptCalendar', 'iniciogozo', 'cxdatainicio');
        $panel->putCampo($script, null, 105, 0);

        // adiciona o campo data fim
        $panel->putCampo($datafim, 'Data Fim Gozo', 0, 1);

        $image = new TImage('app.images/ew_calendar.gif');
        $image->__set('alt', 'Selecione uma data');
        $image->__set('style', 'cursor:pointer;');
        $image->__set('id', 'cxdatafim');
        $panel->putCampo($image, null, 105, 0);
        $script = new TScriptCalendar('scriptCalendar', 'fimgozo', 'cxdatafim');
        $panel->putCampo($script, null, 105, 0);

        // adiciona o campo  inicio periodo
        $panel->putCampo($exercicio, 'Exercicio', 0, 1);

       */
       // cria um botao de acao (salvar)
        $save_button= new TButton('save');
        $cancel_button= new TButton('cancel');

        // define a acao do botao
        $action1 = new TAction(array($this, 'onSave'));
        $action1->setParameter('fk', $_GET['fk']);
        $action1->setParameter('key', $_GET['key']);
        $save_button->setAction($action1, 'Salvar');

        // prepara o botao para chamar o form anterior
        //$lista = new UsuarioServidorList;
        $action2 = new TAction(array($this, 'onCancel'));
        $action2->setParameter('fk', $_GET['fk']);
        $action2->setParameter('key', $_GET['key']);
        $cancel_button->setAction($action2, 'Voltar');

        // adiciona a acao do formulario
        //$panel->putCampo($save_button, null, -100, 1);
        $panel->putCampo($cancel_button, null, 100, 0);

        // define quais sao os campos do formulario
        $this->form->setFields(array($save_button, $cancel_button,$codigo, $nome, $matricula, $exercicio ,$datainicio,$datafim));


        // instancia objeto DataGrid
        $this->datagrid = new TDatagridTables;


        // instancia as colunas da DataGrid
        $dgdatainicio = new TDataGridColumn('iniciogozo', 'Data In&iacute;cio', 'left', 80);
        $dgdatafim = new TDataGridColumn('fimgozo', 'Data Fim', 'left', 80);
        $exercicio = new TDataGridColumn('exercicio', 'Exercicio', 'left', 80);
        

        // adiciona as colunas a DataGrid
        $this->datagrid->addColumn($dgdatainicio);
        $this->datagrid->addColumn($dgdatafim);
        $this->datagrid->addColumn($exercicio);



        //formata as datas
       //$dgdatainicio->setTransformer('formatar_data');
       //$dgdatafim->setTransformer('formatar_data');


        // instancia duas acoes da DataGrid
        $action1 = new TDataGridAction(array($this, 'onEdit'));
        $action1->setLabel('Editar');
        $action1->setImage('ico_edit.png');
        //$action1->setAlt('Editar');
        $action1->setField('id');
        $action1->setFk('servidor_id');


        // adiciona as acoes a DataGrid
        // $this->datagrid->addAction($action1);

        // cria o modelo da DataGrid, montando sua estrutura
        $this->datagrid->createModel();

        // monta a paina atraves de uma tabela
        $panel = new TPanelForm(700, 500);
        $panel->put($this->form, 0, 0);
        $panel->put($this->datagrid, 150, 100);


        // adiciona a tabela a pagina
        parent::add($panel);
    }

    /*
     * metodo onCancel()
     * Executada quando o usuario clicar no botao cancelar do form
     * Exibe msg ao usuario informano que a operacao foi cancelada
     */

    function onCancel(){
        // define a acao de cancelamento

        $params = array();
		$params['fk'] = filter_input ( INPUT_GET, 'fk' );
		$params['key'] = filter_input ( INPUT_GET, 'key' );
		
        TApplication::gotoPage('MeuPerfilList','onReload', $params); // reload
    }

    /*
     * metodo onReload()
     * Carrega a DataGrid com os objetos do banco de dados
     */
    function onReload() {

    // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instancia um repositorio da Classe
        $repository = new TRepository('FeriasRecord');

        // cria um criterio de selecao
        $criteria = new TCriteria;
        $criteria->setProperty('order', 'iniciogozo desc');
                    
       //filtra pelo campo selecionado pelo usuário
        $criteria->add(new TFilter('servidor_id', '=', $_SESSION['servidor_id']));


        // carrega os objetos de acordo com o criterio
        $cadastros = $repository->load($criteria);

        $this->datagrid->clear();
        if ($cadastros)
        {
            // percorre os objetos retornados
            foreach ($cadastros as $cadastro)
            {
                // format date
                $cadastro->iniciogozo = TDate::date2br( $cadastro->iniciogozo );
                $cadastro->fimgozo = TDate::date2br( $cadastro->fimgozo );

                // adiciona o objeto na DataGrid
                $this->datagrid->addItem($cadastro);
            }
        }


        // finaliza a transacao
        TTransaction::close();
        $this->loaded = true;
    }


    /*
     * metodo onDelete()
     * Executada quando o usuario clicar no botao excluir da datagrid
     * Pergunta ao usuario se deseja realmente excluir um registro
     */
    function onDelete($param) {
    // obtem o parametro $key
        $key=$param['key'];

        // define duas acoes
        $action1 = new TAction(array($this, 'Delete'));
        $action2 = new TAction(array($this, 'NaoDelete'));

        // define os parametros de cada acao
        $action1->setParameter('key', $key);
        $action2->setParameter('key', $key);

        //encaminha a chave estrangeira
        $action1->setParameter('fk', $_GET['fk']);
        $action2->setParameter('fk', $_GET['fk']);
        // exibe um dialogo ao usuario
        new TQuestion('Deseja realmente excluir o registro ?', $action1, $action2);
    }

    /*
     * metodo Delete()
     * Exclui um registro
     */
    function Delete($param) {
    // obtem o parametro $key
        $key=$param['key'];
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instanicia objeto Record
        $cadastro = new FeriasRecord($key);

        try {
        // deleta objeto do banco de dados
            $cadastro->delete();

            // finaliza a transacao
            TTransaction::close();
        }
        catch (Exception $e) // em caso de exceção
        {
        // exibe a mensagem gerada pela exceção
            new TMessage('error', $e->getMessage());
            // desfaz todas alterações no banco de dados
            TTransaction::rollback();
        }

        // re-carrega a datagrid
        $this->onReload();
        // exibe mensagem de sucesso
        new TMessage('info', "Registro Exclu&iacute;do com sucesso");
    }

    /*
     * metodo show()
     * Exibe a pagina
     */
    function show() {
    // carrega os dados no datagrid
        $this->onReload();
        //chama o metodo show da super classe
        parent::show();

    }


    /*
     * metodo onSave()
     * Executada quando o usuario clicar no botao salvar do formulario
     */
    function onSave() {
    // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // obtem os dados no formulario em um objeto Record da Classe
        $cadastro = $this->form->getData('FeriasRecord');

        //antes de armazenar verifica se algum campo eh requerido e nao foi informado
        $dados = $cadastro->toArray();

        $msg = '';
        $icone = 'info';

      

        if (empty ($dados['iniciogozo'])) {
            $msg .= 'A data de in&iacute;cio deve ser informada<br/>';
        }

        if (empty ($dados['fimgozo'])) {
            $msg .= 'A data de fim deve ser informada<br/>';
        }

        if (empty ($dados['exercicio'])) {
            $msg .= 'O exercicio deve ser informado<br/>';
        }


        try {

            if ($msg == '') {
            // armazena o objeto
                $cadastro->store();
                $msg = 'Dados armazenados com sucesso';

                // finaliza a transacao
                TTransaction::close();

            }else {
                $icone = 'error';
            }

            if ($icone == 'error') {
            // exibe mensagem de erro
                new TMessage($icone, $msg);
            }else {
            //chama o formulario com o grid
            // define duas acoes
                $action1 = new TAction(array('FeriasServidorDetalhe', 'onReload'));
                $action1->setParameter('fk', $dados['servidor_id']);
                // exibe um dialogo ao usuario
                new TConfirmation($msg, $action1);
            // re-carrega listagem
            //                $this->onReload();
            }
        }
        catch (Exception $e) // em caso de exc
        {
        // exibe a mensagem gerada pela exceção
            new TMessage('error', $e->getMessage());
            // desfaz todas alterações no banco de dados
            TTransaction::rollback();
        }

    }


    /*
     * metodo onEdit()
     * Edita os dados de um registro
     */
    function onEdit($param) {
    // obtem o parametro $key
        $key=$param['key'];
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $cadastro = new FeriasRecord($key);

          //lanca o default
        $cadastro->usuarioalteracao = $_SESSION['usuario'];
        $cadastro->dataalteracao = date("d/m/Y H:i:s");
        
        // lanca os dados no formulario
        $this->form->setData($cadastro);

        // finaliza a transacao
        TTransaction::close();
    }


}
?>