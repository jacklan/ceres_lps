<?php

/*
 * classe SolicitacaoCertificadoFormList
 * Cadastro de Certificado: Contem a listagem e o formulario de busca
 * Autor: Jackson Meires
 * Data:22/09/2016
 */

use Adianti\Widget\Datagrid\TDatagridTables;

class SolicitacaoCertificadoFormList extends TPage {

    private $form;     // formulario de cadastro
    private $datagrid; // listagem

    /*
     * metodo construtor
     * Cria a pagina, o formulario e a listagem
     */

    public function __construct() {
        parent::__construct();

        // instancia um formulario
        $this->form = new TQuickForm;
        // $this->form->class = 'form_acidente';
        $this->form->setFormTitle('<font color="red" size="3" face="Arial"><b>Vincular Certificado ao Servidor</b></font>');

        // cria um rotulo para o titulo
        $lb_campo_obrigatorio = new TLabel('<div style="position:floatval; width: 200px;"> <b>* Campo obrigatorio</b></div>');
        $lb_campo_obrigatorio->setFontFace('Arial');
        $lb_campo_obrigatorio->setFontColor('red');
        $lb_campo_obrigatorio->setFontSize(10);

        // cria os campos do formulario
        $codigo = new THidden('id');
        $srh_certificado_id = new THidden('srh_certificado_id');
        $srh_certificado_id->setValue(filter_input(INPUT_GET, 'fk'));
        $dataemissao = new TDate('dataemissao');
        $servidor_id = new TDBMultiSearch('servidor_id', 'pg_ceres', 'ServidorRecord', 'id', 'nome');
        $servidor_id->setMinLength(3);
        $servidor_id->setOperator('like');

        $usuarioalteracao = new THidden('usuarioalteracao');
        $dataalteracao = new THidden('dataalteracao');

        //cria a colecao da tabela estrangeira do Cabecalho
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $cadastro = new CertificadoRecord(filter_input(INPUT_GET, 'fk'));
        // cria um criterio de selecao, ordenado pelo id do servidor
        //adiciona os objetos na tela com as informacoes do servidor
        if ($cadastro) {
            $curso = new TLabel($cadastro->curso);
        }
        // finaliza a transacao
        TTransaction::close();

        // define os campos
        $this->form->addQuickField(null, $codigo, 10);
        $this->form->addQuickField(null, $srh_certificado_id, 10);
        $this->form->addQuickField(null, $usuarioalteracao, 10);
        $this->form->addQuickField(null, $dataalteracao, 10);
        $this->form->addQuickField('Nome Curso', $curso, 300);
        $this->form->addQuickField('Data Emissão <font color=red><b>*</b></font>', $dataemissao, 20);
        $this->form->addQuickField('Servidor <font color=red><b>*</b></font>', $servidor_id, 20);
        $this->form->addQuickField("<font color=red><b>OBS: </b></font>", new TLabel("<font color=red style='font-size: 16px;'><b>Pesquisar nome produtor em caixa alta, \"CAPS LOCK\" Habilitado</b></font>"), 600);

        $servidor_id->setProperty('style', 'text-transform: uppercase');
        $servidor_id->setSize(300, 70);
        $this->form->addQuickField(null, $lb_campo_obrigatorio, 40);

        // cria um botao de acao
        $action1 = new TAction(array($this, 'onSave'));
        $action1->setParameter('fk', filter_input(INPUT_GET, 'fk'));

        $this->form->addQuickAction('Salvar', $action1, 'ico_save.png');

        $this->form->addQuickAction('Voltar', new TAction(array("CertificadoList", 'onReload')), 'ico_datagrid.gif');

        // instancia objeto DataGrid
        $this->datagrid = new TDatagridTables;

        // instancia as colunas da DataGrid
        $dgnome_servidor = new TDataGridColumn('nome_servidor', 'Servidor', 'left', 500);
        $dgdataemissao = new TDataGridColumn('dataemissao', 'Data Emissão', 'left', 800);

        // adiciona as colunas a DataGrid
        $this->datagrid->addColumn($dgnome_servidor);
        $this->datagrid->addColumn($dgdataemissao);

        $action2 = new TDataGridAction(array($this, 'onDelete'));
        $action2->setLabel('Deletar');
        $action2->setImage('ico_delete.png');
        $action2->setField('id');
        //$action2->setFk('servidor_id');
        // adiciona as acoes a DataGrid
        $this->datagrid->addAction($action2);

        // cria o modelo da DataGrid, montando sua estrutura
        $this->datagrid->createModel();

        // monta a paina atraves de uma tabela
        $panel = new TPanelForm(700, 500);
        $panel->put($this->form, 0, 0);
        $panel->put($this->datagrid, 150, 330);

        // adiciona a tabela a pagina
        parent::add($panel);
    }

    /*
     * metodo onReload()
     * Carrega a DataGrid com os objetos do banco de dados
     */

    function onReload() {

        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instancia um repositorio da Classe
        $repository = new TRepository('CertificadoServidorRecord');

        // cria um criterio de selecao
        $criteria = new TCriteria;

        //filtra pelo campo selecionado pelo usu�rio
        $criteria->add(new TFilter('srh_certificado_id', '=', filter_input(INPUT_GET, 'fk')));

        //verifica quantos registros a consulta vai retornar
        $criteria->setProperty('order', 'id');

        // carrega os objetos de acordo com o criterio
        $cadastros = $repository->load($criteria);

        $this->datagrid->clear();
        if ($cadastros) {
            // percorre os objetos retornados
            foreach ($cadastros as $object) {
                //converter datas do form para formato Brasileiro
                $object->dataemissao = TDate::date2br($object->dataemissao);
                // adiciona o objeto na DataGrid
                $this->datagrid->addItem($object);
            }
        }
        // finaliza a transacao
        TTransaction::close();
        $this->loaded = true;
    }

    /*
     *
     * metodo onDelete()
     * Executada quando o usuario clicar no botao excluir da datagrid
     * Pergunta ao usuario se deseja realmente excluir um registro
     */

    function onDelete($param) {
        // obtem o parametro $key
        $key = $param['key'];

        // define duas acoes
        $action1 = new TAction(array($this, 'Delete'));

        // define os parametros de cada acao
        $action1->setParameter('key', $key);

        //encaminha a chave estrangeira
        $action1->setParameter('fk', filter_input(INPUT_GET, 'fk'));

        // exibe um dialogo ao usuario
        new TQuestion('Deseja realmente excluir o registro ?', $action1);
    }

    /*
     * metodo Delete()
     * Exclui um registro
     */

    function Delete($param) {
        // obtem o parametro $key
        $key = $param['key'];
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instanicia objeto Record
        $cadastro = new CertificadoServidorRecord($key);

        try {
            // deleta objeto do banco de dados
            $cadastro->delete();

            // finaliza a transacao
            TTransaction::close();
        } catch (Exception $e) { // em caso de exce��o
            // exibe a mensagem gerada pela exce��o
            new TMessage('error', $e->getMessage());
            // desfaz todas altera��es no banco de dados
            TTransaction::rollback();
        }

        // re-carrega a datagrid
        $this->onReload();
        // exibe mensagem de sucesso
        new TMessage('info', "Registro Exclu&iacute;do com sucesso");
    }

    /*
     * metodo show()
     * Exibe a pagina
     */

//    function show() {
//        // carrega os dados no datagrid
//        $this->onReload();
//        //chama o metodo show da super classe
//        parent::show();
//    }

    /*
     * metodo onSave()
     * Executada quando o usuario clicar no botao salvar do formulario
     */

    function onSave() {
        try {

            // inicia transacao com o banco 'pg_ceres'
            TTransaction::open('pg_ceres');
            // obtem os dados no formulario em um objeto Record da Classe
            $object = $this->form->getData('CertificadoServidorRecord');

            // form validation
            $this->form->validate();
            $object->srh_certificado_id = filter_input(INPUT_GET, 'fk');
            $object->usuarioalteracao = $_SESSION['usuario'];
            $object->dataalteracao = date("d/m/Y H:i:s");

            $msg = '';
            $icone = 'info';

            var_dump($object);
            //  exit;

            if (!empty($object->servidor_id)) {
                foreach ($object->servidor_id as $key => $item) {
                    $objCS = new CertificadoServidorRecord();
                    $objCS->srh_certificado_id = $object->srh_certificado_id;
                    $objCS->servidor_id = $key;
                    $objCS->dataemissao = $object->dataemissao;
                    $objCS->usuarioalteracao = $object->usuarioalteracao;
                    $objCS->dataalteracao = $object->dataalteracao;
                    $objCS->store();
                }
            }

            if ($msg == '') {
                // armazena o objeto
                $object->store();
                $msg = 'Dados armazenados com sucesso';
            } else {
                $icone = 'error';
            }

            if ($icone == 'error') {
                // exibe mensagem de erro
                new TMessage($icone, $msg);
                $this->form->setData($object);   // fill the form with the active record data
            } else {
                $action = new TAction(array($this, 'onReload'));
                // define os parametros de cada acao
                $action->setParameter('fk', $object->srh_certificado_id);

                // exibe um dialogo ao usuario
                new TMessage("info", "Registro salvo com sucesso!", $action);
            }
            // finaliza a transacao
            TTransaction::close();
        } catch (Exception $e) { // em caso de exc
            // exibe a mensagem gerada pela exce��o
            new TMessage('error', $e->getMessage());
            // desfaz todas altera��es no banco de dados
            TTransaction::rollback();
            $this->form->setData($object);   // fill the form with the active record data
        }
    }

    /*
     * metodo onEdit()
     * Edita os dados de um registro
     */

    function onEdit($param) {
        try {

            if (isset($param['key'])) {

                // get the parameter $key
                $key = $param['key'];

                TTransaction::open('pg_ceres');   // open a transaction with database 'samples'

                $object = new CertificadoServidorRecord($key);        // instantiates object CertificadoServidor
                //converter datas do form para formato Brasileiro
                $object->dataemissao = TDate::date2br($object->dataemissao);

                $this->form->setData($object);   // fill the form with the active record data

                TTransaction::close();           // close the transaction
            } else {

                $this->form->clear();
            }
        } catch (Exception $e) {

            // in case of exception
            // shows the exception error message
            new TMessage('error', '<b>Error</b> ' . $e->getMessage());

            // undo all pending operations
            TTransaction::rollback();
        }
    }

}
