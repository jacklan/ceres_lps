<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of EntidadeList
 *
 * @author Bruno
 */
//ini_set('display_errors', 1);
//ini_set('display_startup_erros', 1);
//error_reporting(E_ALL);

use Adianti\Widget\Datagrid\TDatagridTables;

class MeuPerfilList extends TPage {

    //put your code here
    private $form;     // formulario de cadastro
    private $datagrid; // listagem

    /*
     * metodo construtor
     * Cria a pagina, o formulario e a listagem
     */

    public function __construct() {
        parent::__construct();

        // instancia um formulario
        $this->form = new TForm('form_Entidade');

        // instancia uma tabela
        $panel = new TPanelForm(1000, 100);

        // adiciona a tabela ao formulario
        $this->form->add($panel);

        // cria um r�tulo para o t�tulo
        $titulo = new TLabel('Meu Perfil');
        $titulo->setFontFace('Arial');
        $titulo->setFontColor('red');
        $titulo->setFontSize(18);

        // adiciona o campo Titulo
        $panel->put($titulo, $panel->getColuna(), $panel->getLinha());

        $new_button = new TButton('novo');

        // define a acao do botao cadastrar
        $new_button->setAction(new TAction(array('AlteraSenhaForm', 'onEdit')), 'Alterar Senha');

        // adiciona o campo
        $panel->put($new_button, $panel->getColuna(), $panel->getLinha());

        // define quais sao os campos do formulario
        $this->form->setFields(array($new_button));

        // instancia objeto DataGrid
        $this->datagrid = new TDatagridTables;

        // instancia as colunas da DataGrid
        $dgnome = new TDataGridColumn('nome', 'Nome', 'left', 1200);
        $dgcargo = new TDataGridColumn('nome_cargo', 'Cargo', 'left', 200);
        $dgcargonovo = new TDataGridColumn('nome_cargonovo', 'Cargo Novo', 'left', 200);
        $dgcpf = new TDataGridColumn('cpf', 'CPF', 'left', 100);
        $dgmatricula = new TDataGridColumn('matricula', 'Matr&iacute;cula', 'left', 100);

        // adiciona as colunas a DataGrid
        $this->datagrid->addColumn($dgnome);
        $this->datagrid->addColumn($dgcargo);
        $this->datagrid->addColumn($dgcargonovo);
        $this->datagrid->addColumn($dgcpf);
        $this->datagrid->addColumn($dgmatricula);

        ######################### ACTIONS SERVIDOR ######################
        // instancia duas acoes da DataGrid
        $action_edit = new TDataGridAction(array('MeuPerfilForm', 'onEdit'));
        $action_edit->setLabel('Editar');
        $action_edit->setImage('ico_edit.png');
        $action_edit->setField('id');
        $action_edit->setFk('id');

        //instancia acao para o cadastro da foto do servidor
        $action_foto = new TDataGridAction(array('ServidorFormFotoPerfil', 'onEdit'));
        $action_foto->setLabel('Foto');
        $action_foto->setImage('ico_foto.png');
        $action_foto->setField('id');
        $action_foto->setFk('id');

        ######################### ACTIONS SERVIDOR #######################
        ######################### ACTIONS FUNCIONAL ######################
        // create an action servidor (lotacao)
        $action_servidor_lotacao = new TDataGridAction(array('MeuPerfilLotacao', 'onReload'));
        $action_servidor_lotacao->setLabel('Lotação');
        $action_servidor_lotacao->setField('id');
        $action_servidor_lotacao->setFk('id');

        // create an action (Ferias)
        $action_ferias = new TDataGridAction(array('MeuPerfilFerias', 'onReload'));
        $action_ferias->setLabel('Ferias');
        $action_ferias->setField('id');
        $action_ferias->setFk('id');

        ######################### ACTIONS FUNCIONAL #######################
        ######################### ACTIONS PESSOAL #########################
        // create an action (Disposicao)
        $action_disposicao = new TDataGridAction(array('DisposicaoDetalhe', 'onReload'));
        $action_disposicao->setLabel('Ferias');
        $action_disposicao->setField('id');
        $action_disposicao->setFk('id');

        // create an action (Experiencia)
        $action_experiencia = new TDataGridAction(array('ExperienciaDetalhePerfil', 'onReload'));
        $action_experiencia->setLabel('Disposição');
        $action_experiencia->setField('id');
        $action_experiencia->setFk('id');

        // create an action (Formacao)
        $action_formacao_servidor = new TDataGridAction(array('FormacaoServidorDetalhePerfil', 'onReload'));
        $action_formacao_servidor->setLabel('Titulação');
        $action_formacao_servidor->setField('id');
        $action_formacao_servidor->setFk('id');

        // create an action (Cargo)
        $action_cargo = new TDataGridAction(array('CargoServidorDetalhe', 'onReload'));
        $action_cargo->setLabel('Formação');
        $action_cargo->setField('id');
        $action_cargo->setFk('id');

        // create an action (Habilidade)
        $action_servidor_habilidade = new TDataGridAction(array('ServidorHabilidadeDetalhe', 'onReload'));
        $action_servidor_habilidade->setLabel('Habilidade');
        $action_servidor_habilidade->setField('id');
        $action_servidor_habilidade->setFk('id');

        // create an action (mandatoeletivo)
        $action_mandato_eletivo = new TDataGridAction(array('MandatoEletivoDetalhe', 'onReload'));
        $action_mandato_eletivo->setLabel('Mandato Eletivo');
        $action_mandato_eletivo->setField('id');
        $action_mandato_eletivo->setFk('id');

        // create an action  (Concessao)
        $action_concessao = new TDataGridAction(array('ConcessaoDetalhe', 'onReload'));
        $action_concessao->setLabel('Concessão');
        $action_concessao->setField('id');
        $action_concessao->setFk('id');

        // create an action  (LicencaParticular)
        $action_licenca_particular = new TDataGridAction(array('LicencaParticularDetalhe', 'onReload'));
        $action_licenca_particular->setLabel('Licença Particular');
        $action_licenca_particular->setField('id');
        $action_licenca_particular->setFk('id');

        // create an action (faltaservidor)
        $action_faltas_servidor = new TDataGridAction(array('FaltaServidorDetalhe', 'onReload'));
        $action_faltas_servidor->setLabel('Faltas');
        $action_faltas_servidor->setField('id');
        $action_faltas_servidor->setFk('id');

        // create an action (Requisicao Servidor)
        $action_requisicao_servidor = new TDataGridAction(array('RequisicaoServidorDetalhe', 'onReload'));
        $action_requisicao_servidor->setLabel('Requisição');
        $action_requisicao_servidor->setField('id');
        $action_requisicao_servidor->setFk('id');

        // create an action (ServAposentadoria)
        $action_solicServAposentadoria = new TDataGridAction(array($this, 'onGenerateRSSA'));
        $action_solicServAposentadoria->setLabel('Aposentadoria');
        $action_solicServAposentadoria->setField('id');
        $action_solicServAposentadoria->setFk('id');

        // create an action (Acidente)
        $action_acidente = new TDataGridAction(array('AcidenteDetalhe', 'onReload'));
        $action_acidente->setLabel('Acidente');
        $action_acidente->setField('id');
        $action_acidente->setFk('id');

        // create an action (Producao)
        $action_producao = new TDataGridAction(array('ProducaoDetalhePerfil', 'onReload'));
        $action_producao->setLabel('Produção Acadêmica');
        $action_producao->setField('id');
        $action_producao->setFk('id');

        // create an action (Servidor Capacitacao)
        $action_capacitacao = new TDataGridAction(array('ServidorCapacitacaoDetalhePerfil', 'onReload'));
        $action_capacitacao->setLabel('Capacitação');
        $action_capacitacao->setField('id');
        $action_capacitacao->setFk('id');

        // create an action (Servidor Expertise)
        $action_expertise = new TDataGridAction(array('ServidorExpertiseDetalhePerfil', 'onReload'));
        $action_expertise->setLabel('Expertise');
        $action_expertise->setField('id');
        $action_expertise->setFk('id');

        // create an action (Solicitacao)
        $action_solicitacao = new TDataGridAction(array('SolicitacaoDetalhe', 'onReload'));
        $action_solicitacao->setLabel('Solicitações');
        $action_solicitacao->setField('id');
        $action_solicitacao->setFk('id');

        // create an action (Averbacao)
        $action_averbacao = new TDataGridAction(array('AverbacaoDetalhe', 'onReload'));
        $action_averbacao->setLabel('Averbação');
        $action_averbacao->setField('id');
        $action_averbacao->setFk('id');

        // create an action (Suspensao)
        $action_suspensao = new TDataGridAction(array('SuspensaoServidorDetalhe', 'onReload'));
        $action_suspensao->setLabel('Suspenção');
        $action_suspensao->setField('id');
        $action_suspensao->setFk('id');

        // create an action (Funcao)
        $action_funcao = new TDataGridAction(array('FuncaoServidorDetalhe', 'onReload'));
        $action_funcao->setLabel('Função');
        $action_funcao->setField('id');
        $action_funcao->setFk('id');

        // create an action (Vacancia)
        $action_vacancia = new TDataGridAction(array('VacanciaDetalhe', 'onReload'));
        $action_vacancia->setLabel('Vacancia');
        $action_vacancia->setField('id');
        $action_vacancia->setFk('id');

        // create an action (Provento)
        $action_provento = new TDataGridAction(array('ServidorProventoDetalhe', 'onReload'));
        $action_provento->setLabel('Provento');
        $action_provento->setField('id');
        $action_provento->setFk('id');

        // create an action (Meus Certificados)
        $action_meus_certificados = new TDataGridAction(array('MeusCertificadosList', 'onReload'));
        $action_meus_certificados->setLabel('Meus Certificados');
        $action_meus_certificados->setField('id');
        $action_meus_certificados->setFk('id');
        $action_meus_certificados->setParameter('back', '' . filter_input(INPUT_GET, 'class') . '');

        // create an action (Validar Documentos) 
        $action_validacao_documentos = new TDataGridAction(array('ValidacaoDocumentosForm', 'onLoad'));
        $action_validacao_documentos->setLabel('Validar Documentos');
        $action_validacao_documentos->setField('id');
        $action_validacao_documentos->setFk('id');
        $action_validacao_documentos->setParameter('back', '' . filter_input(INPUT_GET, 'class') . '');

        // instancia duas acoes da DataGrid
        $action_curriculum = new TDataGridAction(array($this, 'onGenerateCurriculum'));
        $action_curriculum->setLabel('Ver Curriculo');
        //$action_curriculum->setImage('ico_fichacatalografica.png');
        $action_curriculum->setField('id');
        $action_curriculum->setFk('id');
        
        // instancia duas acoes da DataGrid
        $action_requerimento = new TDataGridAction(array($this, 'onGenerateRequerimento'));
        $action_requerimento->setLabel('Requerimento');
        //$action_curriculum->setImage('ico_fichacatalografica.png');
        $action_requerimento->setField('id');
        $action_requerimento->setFk('id');
        
        // instancia duas acoes da DataGrid
        $action_ramal = new TDataGridAction(array($this, 'onGenerateRamal'));
        $action_ramal->setLabel('Ramal');
        //$action_curriculum->setImage('ico_fichacatalografica.png');
        $action_ramal->setField('id');
        $action_ramal->setFk('id');

        ######################### ACTIONS PESSOAL #########################
        //Inclui o combo para as actions
        $action_group = new TDataGridActionGroup('A&ccedil;&otilde;es Servidor', 'bs:align-justify');
        $action_group->addHeader('Pessoal');
        $action_group->addAction($action_edit);
        $action_group->addAction($action_foto);
        $action_group->addSeparator();
        $action_group->addHeader('Funcional');
        $action_group->addAction($action_servidor_lotacao);
        $action_group->addAction($action_ferias);
        ######################### ACTIONS PESSOAL #########################
        //Inclui o combo para as actions
        $action_groupPessoal = new TDataGridActionGroup('A&ccedil;&otilde;es Pessoal', 'bs:align-justify');

        //$action_group->addHeader('Available Options');
        $action_groupPessoal->addHeader('Forma&ccedil;&otilde;es');
        $action_groupPessoal->addAction($action_formacao_servidor);
        $action_groupPessoal->addAction($action_experiencia);
        $action_groupPessoal->addAction($action_capacitacao);
        $action_groupPessoal->addAction($action_producao);
        $action_groupPessoal->addAction($action_expertise);
        $action_groupPessoal->addAction($action_curriculum);
        $action_groupPessoal->addAction($action_meus_certificados);
        $action_groupPessoal->addSeparator();
        $action_groupPessoal->addHeader('Solicita&ccedil;&otilde;es');
        $action_groupPessoal->addAction($action_solicServAposentadoria);
        $action_groupPessoal->addAction($action_requerimento);
        $action_groupPessoal->addAction($action_validacao_documentos);
        $action_groupPessoal->addSeparator();
        $action_groupPessoal->addHeader('Uteis');
        $action_groupPessoal->addAction($action_ramal);
        // add the actions to the datagrid
        $this->datagrid->addActionGroup($action_group);
        $this->datagrid->addActionGroup($action_groupPessoal);

        // cria o modelo da DataGrid, montando sua estrutura
        $this->datagrid->createModel();

        //cria uma div para aumentar o espaco entre o grid e o final do form
        $div = new TElement('div');
        $div->style = 'margin-top: 650px';

        // monta a paina atraves de uma tabela
        $panel = new TPanelForm(900, 500);
        $panel->put($this->form, 0, 0);
        $panel->put($this->datagrid, 20, 70);
        $panel->put($div, 0, 0);

        // adiciona a tabela a pagina
        parent::add($panel);
    }

    /*
     * metodo onReload()
     * Carrega a DataGrid com os objetos do banco de dados
     */

    function onReload() {
        // inicia transRaca com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instancia um repositorio para Classe
        $repository = new TRepository('ServidorRecord');

        // cria um criterio de selecao, ordenado pelo id
        $criteria = new TCriteria;
        $criteria->setProperty('order', 'nome');

        //filtra pelo servidor
        $criteria->add(new TFilter('id', '=', $_SESSION["servidor_id"]));

        // carrega os objetos de acordo com o criterio
        $cadastros = $repository->load($criteria);

        $this->datagrid->clear();
        if ($cadastros) {
            // percorre os objetos retornados
            foreach ($cadastros as $cadastro) {
                // adiciona o objeto na DataGrid
                $this->datagrid->addItem($cadastro);
            }
        }


        // finaliza a transRaca
        TTransaction::close();
        $this->loaded = true;
    }

    function onSearch() {
        // inicia transRaca com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instancia um repositorio para Classe
        $repository = new TRepository('ServidorRecord');

        // cria um criterio de selecao, ordenado pelo id
        $criteria = new TCriteria;
        $criteria->setProperty('order', 'nome');

        //obtem os dados do formulario de busca
        $campo = $this->form->getFieldData('opcao');
        $dados = $this->form->getFieldData('nome');


        //verifica se o usuario preencheu o formulario
        if ($dados) {
            if (is_numeric($dados)) {
                $operador = '=';
                $valor = strtoupper($dados);
            } else {
                $operador = 'like';
                $valor = strtoupper("%{$dados}%");
            }
            //filtra pelo campo selecionado pelo usuario
            $criteria->add(new TFilter($campo, $operador, $valor));
        }

        //filtra pelo municipio
        if (!$_REQUEST['method']) {
            $criteria->add(new TFilter('id', '=', $_SESSION["servidor_id"]));
        }

        // carrega os objetos de acordo com o criterio
        $cadastros = $repository->load($criteria);

        $this->datagrid->clear();
        if ($cadastros) {
            // percorre os objetos retornados
            foreach ($cadastros as $cadastro) {
                // adiciona o objeto na DataGrid
                $this->datagrid->addItem($cadastro);
            }
        }


        // finaliza a transRaca
        TTransaction::close();
        $this->loaded = true;
    }

    /*
     * metodo onDelete()
     * Executada quando o usuario clicar no botao excluir da datagrid
     * Pergunta ao usuario se deseja realmente excluir um registro
     */

    function onDelete($param) {
        // obtem o parametro $key
        $key = $param['key'];

        // define duas acoes
        $action1 = new TAction(array($this, 'Delete'));
        $action2 = new TAction(array($this, 'onReload'));

        // define os parametros de cada acao
        $action1->setParameter('key', $key);
        $action2->setParameter('key', $key);

        // exibe um dialogo ao usuario
        new TQuestion('Deseja realmente excluir o registro ?', $action1, $action2);
    }

    /*
     * metodo Delete()
     * Exclui um registro
     */

    function Delete($param) {
        try {
            // obtem o parametro $key
            $key = $param['key'];
            // inicia transacao com o banco 'pg_ceres'
            TTransaction::open('pg_ceres');

            // instanicia objeto Record
            $cadastro = new EntidadeRecord($key);

            // deleta objeto do banco de dados
            $cadastro->delete();

            // finaliza a transacao
            TTransaction::close();
        } catch (Exception $e) { // em caso de exceção
            // exibe a mensagem gerada pela exceção
            new TMessage('error', $e->getMessage());
            // desfaz todas alterações no banco de dados
            TTransaction::rollback();
        }

        // re-carrega a datagrid
        $this->onReload();
        // exibe mensagem de sucesso
        new TMessage('info', "Registro Excluido com sucesso");
    }

    /**
     * method onGenerateRSSA()
     * Executed whenever the user clicks at the edit button report
     */
    function onGenerateRSSA($param) {

        TTransaction::open('pg_ceres');   // open a transaction with database 'pg_ceres'
        ##### Passa a matricula para o formulario #####
        $_REQUEST['matricula'] = (new ServidorRecord($param['fk']))->matricula;
        TTransaction::close();           // close the transaction
        #### chama o relatorio ####
        new RelatorioSolicitacaoServidorAposentadoriaPDF();

        #### carrega a pagina novamente ####
        $paramReload = array();
        $paramReload['fk'] = $param['fk'];
        TApplication::gotoPage('MeuPerfilList', 'onReload', $paramReload); // reload
    }

    /**
     * method onGenerateCurriculum()
     * Executed whenever the user clicks at the edit button report
     */
    function onGenerateCurriculum($param) {

        TTransaction::open('pg_ceres');   // open a transaction with database 'pg_ceres'
        ##### Passa a matricula para o formulario #####
        $objectServidor = (new ServidorRecord($param['fk']));
        $_REQUEST['matricula'] = $objectServidor->matricula;
        $_REQUEST['empresa_id'] = $objectServidor->empresa_id;
        TTransaction::close();           // close the transaction
        #### chama o relatorio ####
        new ServidorCurriculoDetalhePDF();

        #### carrega a pagina novamente ####
        $paramReload = array();
        $paramReload['matricula'] = $_REQUEST['matricula'];
        $paramReload['empresa_id'] = $_REQUEST['empresa_id'];
        TApplication::gotoPage('MeuPerfilList', 'onReload', $paramReload); // reload
    }
    /**
     * method onGenerateRequerimento()
     * Executed whenever the user clicks at the edit button report
     */
    function onGenerateRequerimento($param) {

        TTransaction::open('pg_ceres');   // open a transaction with database 'pg_ceres'
        ##### Passa a matricula para o formulario #####
        $objectServidor = (new ServidorRecord($param['fk']));
        $_REQUEST['matricula'] = $objectServidor->matricula;
        $_REQUEST['empresa_id'] = $objectServidor->empresa_id;
        TTransaction::close();           // close the transaction
        #### chama o relatorio ####
        new RelatorioRequerimentoServidorPDF();

        #### carrega a pagina novamente ####
        $paramReload = array();
        $paramReload['matricula'] = $_REQUEST['matricula'];
        $paramReload['empresa_id'] = $_REQUEST['empresa_id'];
        TApplication::gotoPage('MeuPerfilList', 'onReload', $paramReload); // reload
    }
     /**
     * method onGenerateRanal()
     * Executed whenever the user clicks at the edit button report
     */
    function onGenerateRamal($param) {

        TTransaction::open('pg_ceres');   // open a transaction with database 'pg_ceres'
        ##### Passa a matricula para o formulario #####
        $objectServidor = (new ServidorRecord($param['fk']));
        $_REQUEST['matricula'] = $objectServidor->matricula;
        $_REQUEST['empresa_id'] = $objectServidor->empresa_id;
        TTransaction::close();           // close the transaction
        #### chama o relatorio ####
        new RelatorioListagemRamalPDF();

        #### carrega a pagina novamente ####
        $paramReload = array();
        $paramReload['matricula'] = $_REQUEST['matricula'];
        $paramReload['empresa_id'] = $_REQUEST['empresa_id'];
        TApplication::gotoPage('MeuPerfilList', 'onReload', $paramReload); // reload
    }

    /*
     * metodo show()
     * Exibe a pagina
     */

    function show() {
        // carrega os dados no datagrid
        $this->onReload();

        //chama o metodo show da super classe
        parent::show();
    }

}
