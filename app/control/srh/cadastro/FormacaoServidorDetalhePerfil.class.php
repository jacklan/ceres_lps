<?php

/*
 * classe FormacaoServidorDetalhePerfil
 * Cadastro de FormacaoServidorDetalhePerfil: Contem a listagem e o formulario de busca
 * Data:08/09/2015
 * Autor: Jackson Meires
 */

use Adianti\Widget\Datagrid\TDatagridTables;

class FormacaoServidorDetalhePerfil extends TPage {

    private $form;     // formulario de cadastro
    private $datagrid; // listagem

    /*
     * metodo construtor
     * Cria a pagina, o formulario e a listagem
     */

    public function __construct() {
        parent::__construct();

        // instancia um formulario
        $this->form = new TQuickForm;
        $this->form->class = 'form_formacaoServidor';
        $this->form->setFormTitle('<font color="red" size="3" face="Arial"><b>Detalhe de Servidor &raquo; N&iacute;vel de Titula&ccedil;&atilde;o</b></font>');

        // cria os campos do formulario
        $codigo = new THidden('id');
        $dataconclusao = new TDate('dataconclusao');
        $situacao = new TCombo('situacao');
        $datasituacao = new TDate('datasituacao');
        $datasituacao->setValue(date('d/m/Y'));
        $formacao_id = new TCombo('formacao_id');
        $instituicaoensino_id = new TCombo('instituicaoensino_id');
        $descricao = new TText('descricao');

        $usuarioalteracao = new THidden('usuarioalteracao');
        $dataalteracao = new THidden('dataalteracao');
        
        $items = [];
        //cria a colecao da tabela estrangeira
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $repository = new TRepository('FormacaoRecord');
        // cria um criterio de selecao, ordenado pelo id
        $criteria = new TCriteria;
        $criteria->setProperty('order', 'nome');
        // carrega os objetos de acordo com o criterio
        $cadastros = $repository->load($criteria);
        //adiciona os objetos no combo
        foreach ($cadastros as $object) {
            $items[$object->id] = $object->nome;
        }
        // adiciona as opcoes na combo
        $formacao_id->addItems($items);

        // finaliza a transacao
        TTransaction::close();
        
        $items1 = [];
        //cria a colecao da tabela estrangeira
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $repository = new TRepository('InstituicaoEnsinoRecord');
        // cria um criterio de selecao, ordenado pelo id
        $criteria = new TCriteria;
        $criteria->setProperty('order', 'nome');
        // carrega os objetos de acordo com o criterio
        $cadastros = $repository->load($criteria);
        //adiciona os objetos no combo
        foreach ($cadastros as $object) {
            $items1[$object->id] = $object->nome;
        }

        // adiciona as opcoes na combo
        $instituicaoensino_id->addItems($items1);

        // finaliza a transacao
        TTransaction::close();

        //Cria um vetor com as opcoes da combo
        $items = array();
        $items['CURSANDO'] = 'CURSANDO';
        $items['CONCLUIDA'] = 'CONCLUIDA';
        $items['REPROVADA'] = 'REPROVADA';
        $items['CANCELADA'] = 'CANCELADA';

        // adiciona as opcoes na combo situacao
        $situacao->addItems($items);

        //cria a colecao da tabela estrangeira do Cabecalho
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $cadastro = new ServidorRecord(filter_input(INPUT_GET, 'fk'));
        // cria um criterio de selecao, ordenado pelo id do servidor
        //adiciona os objetos na tela com as informacoes do servidor
        if ($cadastro) {
            $nome = new TLabel($cadastro->nome);
            $matricula = new TLabel($cadastro->matricula);
        }
        // finaliza a transacao
        TTransaction::close();


        //coloca o valor do relacionamento
        $id = new THidden('servidor_id');
        $id->setValue(filter_input(INPUT_GET, 'fk'));

        // define os campos
        $this->form->addQuickField(null, $id, 0);
        $this->form->addQuickField(null, $codigo, 0);
        $this->form->addQuickField(null, $usuarioalteracao, 0);
        $this->form->addQuickField(null, $dataalteracao, 0);

        $this->form->addQuickField('Matr&iacute;cula', $matricula, 40);
        $this->form->addQuickField('Nome Servidor', $nome, 300);
        $this->form->addQuickField('Institui&ccedil;&atilde;o', $instituicaoensino_id, 40);
        $this->form->addQuickField('Forma&ccedil;&atilde;o', $formacao_id, 40);
        $this->form->addQuickField('Situa&ccedil;&atilde;o', $situacao, 40);
        $this->form->addQuickField('Data Conclus&atilde;o', $dataconclusao, 40);
        $this->form->addQuickField('Descri&ccedil;&atilde;o', $descricao, 40);

        // cria um botao salvar
        $action1 = new TAction(array($this, 'onSave'));
        $action1->setParameter('fk', filter_input(INPUT_GET, 'fk'));

        $this->form->addQuickAction('Salvar', $action1, 'ico_save.png')->class = 'btn btn-info btnleft';

        // prepara o botao para chamar o form anterior
        $action2 = new TAction(array("MeuPerfilList", 'onReload'));
//        $action2->setParameter('fk', filter_input(INPUT_GET, 'fk'));

        $this->form->addQuickAction('Voltar', $action2, 'ico_datagrid.gif');

        // instancia objeto DataGrid
        $this->datagrid = new TDatagridTables;

        // instancia as colunas da DataGrid
        $dginstituicao = new TDataGridColumn('nome_instituicao', 'Nome Institui&ccedil;&atilde;o', 'left', 500);
        $dgFormacaoServidor = new TDataGridColumn('nome_formacao', 'Forma&ccedil;&atilde;o', 'left', 300);
        $dgsituacao = new TDataGridColumn('situacao', 'Situa&ccedil;&atilde;o', 'left', 200);
        $dgdataconclusao = new TDataGridColumn('dataconclusao', 'Data Conclus&atilde;o', 'left', 80);
        $dgcorrelacao_plano_carreira = new TDataGridColumn('correlacao_plano_carreira', 'Correla&ccedil;&atilde;o', 'left', 80);
        $dgporcentagem= new TDataGridColumn('nporcentagem', 'Porcentagem', 'left', 80);
        $dgdescricao = new TDataGridColumn('descricao', 'Descri&ccedil;&atilde;o', 'left', 200);

        // adiciona as colunas a DataGrid
        $this->datagrid->addColumn($dgFormacaoServidor);
        $this->datagrid->addColumn($dgdescricao);
        $this->datagrid->addColumn($dgsituacao);
        $this->datagrid->addColumn($dgdataconclusao);
       // $this->datagrid->addColumn($dgcorrelacao_plano_carreira);
       // $this->datagrid->addColumn($dgporcentagem);
        $this->datagrid->addColumn($dginstituicao);

        // instancia duas acoes da DataGrid
        $action1 = new TDataGridAction(array($this, 'onEdit'));
        $action1->setLabel('Editar');
        $action1->setImage('ico_edit.png');
        $action1->setField('id');
        $action1->setFk('servidor_id');

        $action2 = new TDataGridAction(array($this, 'onDelete'));
        $action2->setLabel('Deletar');
        $action2->setImage('ico_delete.png');
        $action2->setField('id');
        $action2->setFk('servidor_id');

        // adiciona as acoes a DataGrid
        $this->datagrid->addAction($action1);
        $this->datagrid->addAction($action2);

        // cria o modelo da DataGrid, montando sua estrutura
        $this->datagrid->createModel();

        // monta a paina atraves de uma tabela
        $panel = new TPanelForm(700, 500);
        $panel->put($this->form, 0, 0);
        $panel->put($this->datagrid, 150, 325);

        // adiciona a tabela a pagina
        parent::add($panel);
    }

    /*
     * metodo onReload()
     * Carrega a DataGrid com os objetos do banco de dados
     */

    function onReload() {

        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instancia um repositorio da Classe
        $repository = new TRepository('FormacaoServidorRecord');

        // cria um criterio de selecao
        $criteria = new TCriteria;

        //filtra pelo campo selecionado pelo usuário
        $criteria->add(new TFilter('servidor_id', '=', filter_input(INPUT_GET, 'fk')));

        // carrega os objetos de acordo com o criterio
        $cadastros = $repository->load($criteria);

        $this->datagrid->clear();
        if ($cadastros) {
            // percorre os objetos retornados
            foreach ($cadastros as $cadastro) {
                //converter datas do form para formato Brasileiro
                $cadastro->dataconclusao = TDate::date2br($cadastro->dataconclusao);
                $cadastro->datasituacao = TDate::date2br($cadastro->datasituacao);

                // adiciona o objeto na DataGrid
                $this->datagrid->addItem($cadastro);
            }
        }


        // finaliza a transacao
        TTransaction::close();
        $this->loaded = true;
    }

    /*
     * metodo onDelete()
     * Executada quando o usuario clicar no botao excluir da datagrid
     * Pergunta ao usuario se deseja realmente excluir um registro
     */

    function onDelete($param) {
        // obtem o parametro $key
        $key = $param['key'];

        // define duas acoes
        $action1 = new TAction(array($this, 'Delete'));
        $action2 = new TAction(array($this, 'onReload'));

        // define os parametros de cada acao
        $action1->setParameter('key', $key);
        $action2->setParameter('key', $key);

        //encaminha a chave estrangeira
        $action1->setParameter('fk', filter_input(INPUT_GET, 'fk'));
        $action2->setParameter('fk', filter_input(INPUT_GET, 'fk'));
        // exibe um dialogo ao usuario
        new TQuestion('Deseja realmente excluir o registro ?', $action1, $action2);
    }

    /*
     * metodo Delete()
     * Exclui um registro
     */

    function Delete($param) {
        // obtem o parametro $key
        $key = $param['key'];
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instanicia objeto Record
        $cadastro = new FormacaoServidorRecord($key);

        try {
            // deleta objeto do banco de dados
            $cadastro->delete();

            // finaliza a transacao
            TTransaction::close();
        } catch (Exception $e) { // em caso de exceção
            // exibe a mensagem gerada pela exceção
            new TMessage('error', $e->getMessage());
            // desfaz todas alterações no banco de dados
            TTransaction::rollback();
        }

        // re-carrega a datagrid
        $this->onReload();
        //exibe mensagem de sucesso
        new TMessage('info', "Registro Exclu&iacute;do com sucesso");
    }

    /*
     * metodo show()
     * Exibe a pagina
     */

    function show() {
        // carrega os dados no datagrid
        $this->onReload();
        //chama o metodo show da super classe
        parent::show();
    }

    /*
     * metodo onSave()
     * Executada quando o usuario clicar no botao salvar do formulario
     */

    function onSave() {
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // obtem os dados no formulario em um objeto Record da Classe
        $cadastro = $this->form->getData('FormacaoServidorRecord');

        //antes de armazenar verifica se algum campo eh requerido e nao foi informado
        $dados = $cadastro->toArray();

        $msg = '';
        $icone = 'info';

        if (empty($dados['instituicaoensino_id'])) {
            $msg .= 'A Institui&ccedil;&atilde;o deve ser informada';
        }
        if (empty($dados['formacao_id'])) {
            $msg .= 'A forma&ccedil;&atilde;o deve ser informada';
        }

        if (empty($dados['situacao'])) {
            $msg .= 'A situa&ccedil;&atilde;o deve ser informada';
        }
        if (empty($dados['dataconclusao'])) {
            $msg .= 'A Data de Conclusao deve ser informada';
        }


        try {

            if ($msg == '') {
                // armazena o objeto
                $cadastro->store();
                $msg = 'Dados armazenados com sucesso';

                // finaliza a transacao
                TTransaction::close();
            } else {
                $icone = 'error';
            }

            if ($icone == 'error') {
                // exibe mensagem de erro
                new TMessage($icone, $msg);
                $this->form->setData($cadastro);   // fill the form with the active record data
            } else {
                // define array acoes
                $param = array();
                $param['fk'] = $dados['servidor_id'];

                // exibe um dialogo ao usuario
                new TMessage("info", "Registro salvo com sucesso!");
                TApplication::gotoPage('FormacaoServidorDetalhePerfil', 'onReload', $param); // reload
            }
        } catch (Exception $e) { // em caso de exc
            // exibe a mensagem gerada pela exceção
            new TMessage('error', $e->getMessage());
            // desfaz todas alterações no banco de dados
            TTransaction::rollback();
            $this->form->setData($cadastro);   // fill the form with the active record data
        }
    }

    /*
     * metodo onEdit()
     * Edita os dados de um registro
     */

    function onEdit($param) {
        try {
            if (isset($param['key'])) {

                // get the parameter $key
                $key = $param['key'];

                TTransaction::open('pg_ceres');   // open a transaction with database 'samples'

                $object = new FormacaoServidorRecord($key);        // instantiates object City
                //converter datas do form para formato Brasileiro
                $object->dataconclusao = TDate::date2br($object->dataconclusao);
                $object->datasituacao = TDate::date2br($object->datasituacao);

                $this->form->setData($object);   // fill the form with the active record data

                TTransaction::close();           // close the transaction
            } else {
                $this->form->clear();
            }
        } catch (Exception $e) { // in case of exception
            // shows the exception error message
            new TMessage('error', '<b>Error</b> ' . $e->getMessage());
            // undo all pending operations
            TTransaction::rollback();
        }
    }

}