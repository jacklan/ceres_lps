<?php

//ini_set('display_errors', 1);
//ini_set('display_startup_erros', 1);
//error_reporting(E_ALL);

/*
 * classe PropriedadeDetalhe
 * Cadastro de Propriedade: Contem a listagem e o formulario de busca
 */
use Adianti\Database\TFilter1;
use Adianti\Widget\Datagrid\TDatagridTables;

class BairroDetalhe extends TPage {

    private $form;
    private $datagrid; // listagem

    /*
     * metodo construtor
     * Cria a pagina, o formulario e a listagem
     */

    public function __construct() {
        parent::__construct();

        // instancia um formulario
        $this->form = new TQuickForm;
        $this->form->class = 'form_BairroDetalhe';
        $this->form->setFormTitle('<font color="red" size="3" face="Arial"><b>Cadastro de Bairros</b></font>');

        // cria um rotulo para o titulo
        $titulo = new TLabel('<div style="position:floatval; width: 200px;"> <b>* Campo obrigatorio</b></div>');
        $titulo->setFontFace('Arial');
        $titulo->setFontColor('red');
        $titulo->setFontSize(10);

        // cria os campos do formulario
        $codigo = new THidden('id');
        $codigo->setEditable(false);
        $municipio_id = new THidden('municipio_id');
        $municipio_id->setValue(filter_input(INPUT_GET, 'fk'));
        $nome = new TEntry('nome');
        $codigo_correios = new TEntry('codigo_correios');

        $usuarioalteracao = new THidden('usuarioalteracao');
        $dataalteracao = new THidden('dataalteracao');

        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instancia objeto da Classe Record
        $object = new MunicipioRecord(filter_input(INPUT_GET, 'fk'));

        $nome_municipio = new TLabel($object->nome);

        // finaliza a transacao
        TTransaction::close();

        // define os campos
        $this->form->addQuickField(null, $codigo, 10);
        $this->form->addQuickField('Município', $nome_municipio, 10);
        $this->form->addQuickField(null, $usuarioalteracao, 10);
        $this->form->addQuickField(null, $dataalteracao, 10);
        $this->form->addQuickField(null, $municipio_id, 40);
        $this->form->addQuickField('Nome <font color=red><b>*</b></font>', $nome, 50);
        $this->form->addQuickField('C&oacute;digo Correios', $codigo_correios, 50);
        $this->form->addQuickField(null, $titulo, 50);

        // define a acao do botao
        $action1 = new TAction(array($this, 'onSave'));
        $action1->setParameter('key', filter_input(INPUT_GET, 'key'));
        $action1->setParameter('fk', filter_input(INPUT_GET, 'fk'));

        // cria um botao de acao
        $this->form->addQuickAction('Salvar', $action1, 'ico_save.png');
        $this->form->addQuickAction('Voltar', new TAction(array('MunicipioList', 'onReload')), 'ico_datagrid.gif');

        // instancia objeto DataGrid
        $this->datagrid = new TDatagridTables;

        // instancia as colunas da DataGrid
        $dglatitude = new TDataGridColumn('nome', 'Bairro', 'left', 1350);
        $dglongitude = new TDataGridColumn('codigo_correios', 'Codigo Correios', 'left', 1350);

        // adiciona as colunas a DataGrid
        $this->datagrid->addColumn($dglatitude);
        $this->datagrid->addColumn($dglongitude);

        // instancia duas acoes da DataGrid
        $action1 = new TDataGridAction(array($this, 'onEdit'));
        $action1->setLabel('Editar');
        $action1->setImage('ico_edit.png');
        $action1->setField('id');
        $action1->setFk('municipio_id');

        $action2 = new TDataGridAction(array($this, 'onDelete'));
        $action2->setLabel('Deletar');
        $action2->setImage('ico_delete.png');
        $action2->setField('id');
        $action2->setFk('municipio_id');
        
        $action4 = new TDataGridAction(array('BairroCoordenadasDetalhe', 'onReload'));
        $action4->setLabel('Coordenadas');
        $action4->setImage('ico_coordenadas.png');
        $action4->setField('id');
        $action4->setFK('id');
        $action4->setDid('municipio_id');

        // adiciona as acoes a DataGrid
        $this->datagrid->addAction($action2);
        $this->datagrid->addAction($action1);
        $this->datagrid->addAction($action4);

        // cria o modelo da DataGrid, montando sua estrutura
        $this->datagrid->createModel();

        // monta a paina atraves de uma tabela
        $panel = new TPanelForm(700, 500);
        $panel->put($this->form, 0, 0);
        $panel->put($this->datagrid, 150, 210);

        // adiciona a tabela a pagina
        parent::add($panel);
    }

    /*
     * metodo onReload()
     * Carrega a DataGrid com os objetos do banco de dados
     */

    function onReload() {

        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instancia um repositorio da Classe
        $repository = new TRepository('BairroRecord');

        // cria um criterio de selecao
        $criteria = new TCriteria;

        //filtra pelo campo selecionado pelo usu�rio
        $criteria->add(new TFilter('municipio_id', '=', filter_input(INPUT_GET, 'fk')));

        //verifica quantos registros a consulta vai retornar
        $registros = $repository->count($criteria);
        $criteria->setProperty('order', 'nome');
        $cadastros = $repository->load($criteria);

        $this->datagrid->clear();
        if ($cadastros) {
            // percorre os objetos retornados
            foreach ($cadastros as $cadastro) {
                // adiciona o objeto na DataGrid
                $this->datagrid->addItem($cadastro);
            }
        }


        // finaliza a transacao
        TTransaction::close();
        $this->loaded = true;
    }

    /*
     * metodo onDelete()
     * Executada quando o usuario clicar no botao excluir da datagrid
     * Pergunta ao usuario se deseja realmente excluir um registro
     */

    function onDelete($param) {
        // obtem o parametro $key
        $key = $param['key'];

        // define duas acoes
        $action1 = new TAction(array($this, 'Delete'));
        $action2 = new TAction(array($this, 'onReload'));

        // define os parametros de cada acao
        $action1->setParameter('key', $key);
        $action2->setParameter('key', $key);

        //encaminha a chave estrangeira
        $action1->setParameter('fk', filter_input(INPUT_GET, 'fk'));
        $action2->setParameter('fk', filter_input(INPUT_GET, 'fk'));
        // exibe um dialogo ao usuario
        new TQuestion('Deseja realmente excluir o registro ?', $action1, $action2);
    }

    /*
     * metodo Delete()
     * Exclui um registro
     */

    function Delete($param) {
        // obtem o parametro $key
        $key = $param['key'];
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instanicia objeto Record
        $cadastro = new BairroRecord($key);

        try {
            // deleta objeto do banco de dados
            $cadastro->delete();

            // finaliza a transacao
            TTransaction::close();
        } catch (Exception $e) { // em caso de exce??o
            // exibe a mensagem gerada pela exce??o
            new TMessage('error', $e->getMessage());
            // desfaz todas altera??es no banco de dados
            TTransaction::rollback();
        }

        // re-carrega a datagrid
        $this->onReload();
        // exibe mensagem de sucesso
        new TMessage('info', "Registro Excluido com sucesso");
    }

    /*
     * metodo show()
     * Exibe a pagina
     */

    function show() {
        // carrega os dados no datagrid
        $this->onReload();
        //chama o metodo show da super classe
        parent::show();
    }

    /*
     * metodo onSave()
     * Executada quando o usuario clicar no botao salvar do formulario
     */

    function onSave() {
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // obtem os dados no formulario em um objeto CarroRecord
        $cadastro = $this->form->getData('BairroRecord');

        //lanca o default
        $cadastro->usuarioalteracao = $_SESSION['usuario'];
        $cadastro->dataalteracao = date("d/m/Y H:i:s");

        //antes de armazenar verifica se algum campo eh requerido e nao foi informado
        $dados = $cadastro->toArray();
        $obj = implode(" , ", $dados); //converte os dados do array para string

        $msg = '';
        $icone = 'info';

        try {

            if ($msg == '') {
                $cadastro->store();
                $msg = 'Dados armazenados com sucesso';
                TTransaction::close();
            } else {
                $icone = 'error';
            }

            if ($icone == 'error') {
                // exibe mensagem de erro
                new TMessage($icone, $msg);
                $this->form->setData($cadastro);
            } else {
                $param = array();
                $param['fk'] = $dados['municipio_id'];

                new TMessage("info", "Registro salvo com sucesso!");
                TApplication::gotoPage('BairroDetalhe', 'onReload', $param);
            }
        } catch (Exception $e) {
            new TMessage('error', $e->getMessage());
            TTransaction::rollback();
            $this->form->setData($cadastro);
        }
    }

    function onEdit($param) {
        $key = $param['key'];
        TTransaction::open('pg_ceres');
        $cadastro = new BairroRecord($key);
        $this->form->setData($cadastro);
        TTransaction::close();
    }

}

?>