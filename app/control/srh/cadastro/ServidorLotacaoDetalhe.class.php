<?php

//ini_set('display_errors', 1);
//ini_set('display_startup_erros', 1);
//error_reporting(E_ALL);

/**
 * ServidorLotacaoDetalhe
 *
 * @version    2.0
 * @package    ceres
 * @subpackage app/control
 * @author     Jackson Meires
 * @copyright  Copyright (c) 2006-2014 Adianti Solutions Ltd. (http://www.adianti.com.br)
 * @license    http://www.adianti.com.br/framework-license
 */

use Adianti\Widget\Datagrid\TDatagridTables;

class ServidorLotacaoDetalhe extends TPage {

    private $form;     // formulario de cadastro
    private $datagrid; // listagem

    /*
     * metodo construtor
     * Cria a pagina, o formulario e a listagem
     */

    public function __construct() {
        parent::__construct();

        // instancia um formulario
        $this->form = new TQuickForm;
        $this->form->class = 'form_ServidorLotacao';
        $this->form->setFormTitle('<font color="red" size="3" face="Arial"><b>Detalhe de Lota&ccedil;&atilde;o do Servidor</b></font>');

        // cria um rotulo para o titulo
        $titulo = new TLabel('<div style="position:floatval; width: 200px;"> <b>* Campo obrigatorio</b></div>');
        $titulo->setFontFace('Arial');
        $titulo->setFontColor('red');
        $titulo->setFontSize(10);

        // cria os campos do formulario
        $codigo = new THidden('id');
        $datainicio = new TDate('datainicio');
        $datafim = new TDate('datafim');
        $documento = new TEntry('documento');
        $unidadeoperativa_id = new TCombo('unidadeoperativa_id');
        $setor_id = new TCombo('setor_id');

        $datapublicacao = new TDate('datapublicacao');
        $ndiariooficial = new TEntry('numerodiariooficial');

        $usuarioalteracao = new THidden('usuarioalteracao');
        $dataalteracao = new THidden('dataalteracao');
        //Campos obrigatórios
        $setor_id->setProperty('required', 'required');
        $unidadeoperativa_id->setProperty('required', 'required');
        $datainicio->setProperty('required', 'required');

        //cria a colecao da tabela estrangeira
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $repository = new TRepository('UnidadeOperativaRecord');
        // cria um criterio de selecao, ordenado pelo id
        $criteria = new TCriteria;
        $criteria->add(new TFilter('empresa_id', '=', $_SESSION['empresa_id']));
        $criteria->setProperty('order', 'nome');
        // carrega os objetos de acordo com o criterio
        $cadastros = $repository->load($criteria);
        //adiciona os objetos no combo
        foreach ($cadastros as $object) {
            $items1[$object->id] = $object->nome;
        }
        // adiciona as opcoes na combo
        $unidadeoperativa_id->addItems($items1);

        // finaliza a transacao
        TTransaction::close();

        //cria a colecao da tabela estrangeira
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $repository = new TRepository('SetorRecord');

        // cria um criterio de selecao, ordenado pelo id
        $criteria = new TCriteria;
        $criteria->add(new TFilter('empresa_id', '=', $_SESSION['empresa_id']));
        $criteria->setProperty('order', 'nome');
        // carrega os objetos de acordo com o criterio
        $cadastros = $repository->load($criteria);
        //adiciona os objetos no combo
        foreach ($cadastros as $object) {
            $items[$object->id] = $object->nome;
        }
        // adiciona as opcoes na combo
        $setor_id->addItems($items);

        // finaliza a transacao
        TTransaction::close();

        //cria a colecao da tabela estrangeira do Cabecalho
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $cadastro = new ServidorRecord(filter_input(INPUT_GET, 'fk'));
        // cria um criterio de selecao, ordenado pelo id do servidor
        //adiciona os objetos na tela com as informacoes do servidor
        if ($cadastro) {
            $nome = new TLabel($cadastro->nome);
            $matricula = new TLabel($cadastro->matricula);
        }
        // finaliza a transacao
        TTransaction::close();

        //coloca o valor do relacionamento
        $servidor_id = new THidden('servidor_id');
        $servidor_id->setValue(filter_input(INPUT_GET, 'fk'));

        // define os campos
        $this->form->addQuickField(null, $servidor_id, 1);
        $this->form->addQuickField(null, $codigo, 10);
        $this->form->addQuickField(null, $usuarioalteracao, 10);
        $this->form->addQuickField(null, $dataalteracao, 10);

        $this->form->addQuickField('Nome Servidor', $nome, 300);
        $this->form->addQuickField('Matr&iacute;cula', $matricula, 40);
        $this->form->addQuickField('Unidade Operativa <font color=red><b>*</b></font>', $unidadeoperativa_id, 40);
        $this->form->addQuickField('Setor <font color=red><b>*</b></font>', $setor_id, 40);
        $this->form->addQuickField('Data Inicio <font color=red><b>*</b></font>', $datainicio, 40);
        $this->form->addQuickField('Data Fim', $datafim, 40);
        $this->form->addQuickField('Documento', $documento, 40);

        $this->form->addQuickField('Data Publicação', $datapublicacao, 40);
        $this->form->addQuickField('Numero Diario Oficial', $ndiariooficial, 40);

        $this->form->addQuickField(null, $titulo, 50);

        $unidadeoperativa_id->addValidation('Unidade Operativa', new TRequiredValidator); // required field
        $setor_id->addValidation('Setor', new TRequiredValidator); // required field
        $datainicio->addValidation('Data Inicio', new TRequiredValidator); // required field

        // cria um botao de acao
        $action1 = new TAction(array($this, 'onSave'));
        $action1->setParameter('fk', filter_input(INPUT_GET, 'fk'));

        $this->form->addQuickAction('Salvar', $action1, 'ico_save.png')->class = 'btn btn-info btnleft';

        // prepara o botao para chamar o form anterior
        $action2 = new TAction(array("ServidorForm", 'onEdit'));
        $action2->setParameter('fk', filter_input(INPUT_GET, 'fk'));

        $this->form->addQuickAction('Voltar', $action2, 'ico_datagrid.gif');

        // instancia objeto DataGrid
        $this->datagrid = new TDatagridTables;

        // instancia as colunas da DataGrid
        $dgunidade = new TDataGridColumn('nome_unidadeoperativa', 'Local', 'left', 500);
        $dgsetor = new TDataGridColumn('nome_setor', 'Setor', 'left', 500);
        $dgdocumento = new TDataGridColumn('documento', 'Documento', 'left', 200);
        $dgdatainicio = new TDataGridColumn('datainicio', 'Data In&iacute;cio', 'left', 80);
        $dgdatafim = new TDataGridColumn('datafim', 'Data Fim', 'left', 80);

        // adiciona as colunas a DataGrid
        $this->datagrid->addColumn($dgunidade);
        $this->datagrid->addColumn($dgsetor);
        $this->datagrid->addColumn($dgdocumento);
        $this->datagrid->addColumn($dgdatainicio);
        $this->datagrid->addColumn($dgdatafim);

        // instancia duas acoes da DataGrid
        $action1 = new TDataGridAction(array($this, 'onEdit'));
        $action1->setLabel('Editar');
        $action1->setImage('ico_edit.png');
        $action1->setField('id');
        $action1->setFk('servidor_id');

        $action2 = new TDataGridAction(array($this, 'onDelete'));
        $action2->setLabel('Deletar');
        $action2->setImage('ico_delete.png');
        $action2->setField('id');
        $action2->setFk('servidor_id');

        // adiciona as acoes a DataGrid
        $this->datagrid->addAction($action1);
        $this->datagrid->addAction($action2);


        // cria o modelo da DataGrid, montando sua estrutura
        $this->datagrid->createModel();

        // monta a paina atraves de uma tabela
        $panel = new TPanelForm(700, 500);
        $panel->put($this->form, 0, 0);
        $panel->put($this->datagrid, 150, 275);

        // adiciona a tabela a pagina
        parent::add($panel);
    }

    /*
     * metodo onDelete()
     * Executada quando o usuario clicar no botao excluir da datagrid
     * Pergunta ao usuario se deseja realmente excluir um registro
     */

    function onDelete($param) {
        // obtem o parametro $key
        $key = $param['key'];

        $action1 = new TAction(array($this, 'Delete'));
        $action2 = new TAction(array($this, 'onReload'));

        // define os parametros de cada acao
        $action1->setParameter('key', $key);
        $action2->setParameter('key', $key);

        //encaminha a chave estrangeira
        $action1->setParameter('fk', filter_input(INPUT_GET, 'fk'));
        $action2->setParameter('fk', filter_input(INPUT_GET, 'fk'));
        // exibe um dialogo ao usuario
        new TQuestion('Deseja realmente excluir o registro ?', $action1, $action2);
    }

    /*
     * metodo Delete()
     * Exclui um registro
     */

    function Delete($param) {
        // obtem o parametro $key
        $key = $param['key'];
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instanicia objeto Record
        $cadastro = new UnidadeServidorRecord($key);

        try {
            // deleta objeto do banco de dados
            $cadastro->delete();

            // finaliza a transacao
            TTransaction::close();
        } catch (Exception $e) { // em caso de exceção
            // exibe a mensagem gerada pela exceção
            new TMessage('error', $e->getMessage());
            // desfaz todas alterações no banco de dados
            TTransaction::rollback();
        }

        // re-carrega a datagrid
        $this->onReload();
        //exibe mensagem de sucesso
        new TMessage('info', "Registro Exclu&iacute;do com sucesso");
    }

    /*
     * metodo onReload()
     * Carrega a DataGrid com os objetos do banco de dados
     */

    function onReload() {
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instancia um repositorio da Classe
        $repository = new TRepository('UnidadeServidorRecord');

        // cria um criterio de selecao
        $criteria = new TCriteria;

        //filtra pelo campo selecionado pelo usuario
        $criteria->add(new TFilter('servidor_id', '=', $_GET['fk']));

        $criteria->setProperty('order', 'datainicio desc');

        // carrega os objetos de acordo com o criterio
        $cadastros = $repository->load($criteria);

        $this->datagrid->clear();
        if ($cadastros) {
            // percorre os objetos retornados
            foreach ($cadastros as $cadastro) {

                //converter datas do form para formato Brasileiro
                $cadastro->datainicio = TDate::date2br($cadastro->datainicio);
                $cadastro->datafim = TDate::date2br($cadastro->datafim);

                // adiciona o objeto na DataGrid
                $this->datagrid->addItem($cadastro);
            }
        }
        // finaliza a transacao
        TTransaction::close();
        $this->loaded = true;
    }

    /*
     * metodo onSave()
     * Executada quando o usuario clicar no botao salvar do formulario
     */

    function onSave() {
        try {
            // inicia transacao com o banco 'pg_ceres'
            TTransaction::open('pg_ceres');

            // obtem os dados no formulario em um objeto Record da Classe
            $object = $this->form->getData('UnidadeServidorRecord');

            $object->usuarioalteracao = $_SESSION['usuario'];
            $object->dataalteracao = date("d/m/Y H:i:s");

            //verifica se existe uma datafim em aberto
            $criteria = new TCriteria;
            $criteria->add(new TFilter('servidor_id', '=', filter_input(INPUT_GET, 'fk')));
            $criteria->add(new TFilter('datafim', 'is', null));

            $repository = new TRepository('UnidadeServidorRecord');
            $count_data_em_aberto = $repository->count($criteria);

            // run form validation
            $this->form->validate();

            if ($count_data_em_aberto == 0 || !empty($object->id)) {

                // armazena o objeto
                $object->store();

                // define array acoes
                $param = array();
                $param['fk'] = $object->servidor_id;

                // exibe um dialogo ao usuario
                new TMessage("info", "Registro salvo com sucesso!",$action);
                TApplication::gotoPage('ServidorLotacaoDetalhe', 'onReload', $param); // reload

            } else {
                new TMessage("info", "Existe uma datafim em aberto, informe primeiro a datafim da ultima<br>lotação para poder inserir uma nova");
                $this->form->setData($this->form->getData());
            }
            TTransaction::close();
        } catch (Exception $e) {

            // em caso de exc
            // exibe a mensagem gerada pela exceção
            new TMessage('error', $e->getMessage());

            // lanca os dados no formulario
            $this->form->setData($this->form->getData());

            // desfaz todas alterações no banco de dados
            TTransaction::rollback();
        }
    }

    /*
     * metodo onEdit()
     * Edita os dados de um registro
     */

    function onEdit($param) {

        try {

            if (isset($param['key'])) {

                // get the parameter $key
                $key = $param['key'];

                TTransaction::open('pg_ceres');   // open a transaction with database 'samples'

                $object = new UnidadeServidorRecord($key);        // instantiates object City
                //converter datas do form para formato Brasileiro
                $object->datainicio = TDate::date2br($object->datainicio);
                $object->datafim = TDate::date2br($object->datafim);
                $object->datapublicacao = TDate::date2br($object->datapublicacao);

                $this->form->setData($object);   // fill the form with the active record data

                TTransaction::close();           // close the transaction
            } else {

                $this->form->clear();
            }
        } catch (Exception $e) {

            // in case of exception
            // shows the exception error message
            new TMessage('error', '<b>Error</b> ' . $e->getMessage());

            // undo all pending operations
            TTransaction::rollback();
        }
    }

    /*
     * metodo show()
     * Exibe a pagina
     */

    function show() {

        // carrega os dados no datagrid
        $this->onReload();
        //chama o metodo show da super classe
        parent::show();
    }

}

?>