<?php

//ini_set('display_errors', 1);
//ini_set('display_startup_erros', 1);
//error_reporting(E_ALL);

/*
 * classe PropriedadeDetalhe
 * Cadastro de Propriedade: Contem a listagem e o formulario de busca
 */
use Adianti\Database\TFilter1;
use Adianti\Widget\Datagrid\TDatagridTables;

class MunicipioCoordenadasDetalhe extends TPage {

    private $form;
     // formulario de cadastro
    private $datagrid; // listagem

    /*
     * metodo construtor
     * Cria a pagina, o formulario e a listagem
     */
    public function __construct() {
        parent::__construct();

        // instancia um formulario
		$this->form = new TQuickForm;
        $this->form->class = 'form_MunicipioCoordenadas';
        $this->form->setFormTitle('<font color="red" size="3" face="Arial"><b>Detalhe das Coordenadas</b></font>');
		
       // cria um rotulo para o titulo
        $titulo = new TLabel('<div style="position:floatval; width: 200px;"> <b>* Campo obrigatorio</b></div>');
        $titulo->setFontFace('Arial');
        $titulo->setFontColor('red');
        $titulo->setFontSize(10);

        // cria os campos do formulario
        $codigo = new THidden('id');
        $codigo->setEditable(false);
        $municipio_id = new THidden('municipio_id');
        $municipio_id->setValue(filter_input(INPUT_GET, 'fk'));
        $latitude = new TEntry('latitude');
        $longitude = new TEntry('longitude');
		
		$usuarioalteracao = new THidden('usuarioalteracao');
        $dataalteracao = new THidden('dataalteracao');
        
        //------------------Pedro Henrique----------------------
        $grausLat = new TEntry('grausLat');
        $minutosLat = new TEntry('minutosLat');
        $segundosLat = new TEntry('segundosLat');
        $grausLon = new TEntry('grausLon');
        $minutosLon = new TEntry('minutosLon');
        $segundosLon = new TEntry('segundosLon');
        $tipoCoord = new TRadioGroup('tipoCoord');
        
        $grausLat->setSize(95);
        $minutosLat->setSize(95);
        $segundosLat->setSize(95);
        $grausLon->setSize(95);
        $minutosLon->setSize(95);
        $segundosLon->setSize(95);
        
        $grausLat->setMask('00');
        $minutosLat->setMask('00');
        $segundosLat->setMask('00.000');
        $grausLon->setMask('000');
        $minutosLon->setMask('00');
        $segundosLon->setMask('00.000');
        
        $grausLat->disableField($this->form->getName(), 'grausLat');
        $minutosLat->disableField($this->form->getName(), 'minutosLat');
        $segundosLat->disableField($this->form->getName(), 'segundosLat');
        $grausLon->disableField($this->form->getName(), 'grausLon');
        $minutosLon->disableField($this->form->getName(), 'minutosLon');
        $segundosLon->disableField($this->form->getName(), 'segundosLon');
        
        $grausLat->setProperty('placeholder', 'Ex.: 0&#176; a 90&#176;');
        $minutosLat->setProperty('placeholder', 'Ex.: 0m a 59m');
        $segundosLat->setProperty('placeholder', 'Ex.: 0s a 59s');
        $grausLon->setProperty('placeholder', 'Ex.: 0&#176; a 180&#176;');
        $minutosLon->setProperty('placeholder', 'Ex.: 0m a 59m');
        $segundosLon->setProperty('placeholder', 'Ex.: 0s a 59s');
        
        $ajuda = new TLabel('<a href="https://support.google.com/maps/answer/2533464?hl=pt-BR" target="_blank">'
        		. '<font style="font-size:15px">Como preencher as coordenadas em '
        		. '<font style="color:#FF0000"><b>DD ou DMS</b></font></font></a>');
        $ajuda->setFontFace('Arial');
        $ajuda->setFontSize(10);
        
        $obrigatorio = new TLabel('<div style="position:floatval; width: 200px;">'
        		. '<b>* Campos obrigat&oacute;rios</b></div>');
        $obrigatorio->setFontFace('Arial');
        $obrigatorio->setFontColor('red');
        $obrigatorio->setFontSize(10);
        
        $tipoCoord->addItems(array(
        		'1' => 'Coordenadas DD (Graus decimais)',
        		'2' => 'Coordenadas DMS (Graus, minutos e segundos)'
        ));
        $tipoCoord->setLayout('horizontal');
        $tipoCoord->setValue('1');
        //------------------------------------------------------
        
        

        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
		
        // instancia objeto da Classe Record
        $object = new MunicipioRecord( filter_input(INPUT_GET, 'fk') );
		
        $nome_municipio = new TLabel($object->nome);

        // finaliza a transacao
        TTransaction::close();
        
        $latitude->setProperty('placeholder', 'Ex.: -5,828999');
        $longitude->setProperty('placeholder', 'Ex.: -35,215468');
		
		 // define os campos
		$this->form->addQuickField(null, $codigo, 10 );
		$this->form->addQuickField('Município', $nome_municipio, 400);
		$this->form->addQuickField(null, $usuarioalteracao, 10);
        $this->form->addQuickField(null, $dataalteracao, 10);
		$this->form->addQuickField(null, $municipio_id, 100);
		
		//------------------Pedro Henrique----------------------
		$this->form->addQuickField(null, $ajuda, 400);
		$this->form->addQuickField(null, $tipoCoord, 10);
		$this->form->addQuickField("Latitude", $latitude, 20);
		$this->form->addQuickField("Longitude", $longitude, 20);
		$this->form->addQuickFields('Latitude', array(
				$grausLat, new TLabel('<font size="4">&#176;</font>'),
				$minutosLat, new TLabel('<font size="4">&#39;</font>'),
				$segundosLat, new TLabel('<font size="4">&#34;S</font>')
		));
		$this->form->addQuickFields('Longitude', array(
				$grausLon, new TLabel('<font size="4">&#176;</font>'),
				$minutosLon, new TLabel('<font size="4">&#39;</font>'),
				$segundosLon, new TLabel('<font size="4">&#34;W</font>')
		));
		$this->form->addQuickField(null, $obrigatorio, 50);
		
		$acaoRadio = new TAction(array($this, 'onChangeRadio'));
		$acaoRadio->setParameter('formName', $this->form->getName());
		$tipoCoord->setChangeAction($acaoRadio);
		//------------------------------------------------------
		

		 // define a acao do botao
        $action1 = new TAction(array($this, 'onSave'));
        $action1->setParameter( 'key', filter_input( INPUT_GET, 'key' ) );
        $action1->setParameter( 'fk', filter_input( INPUT_GET, 'fk' ) );
		
		// cria um botao de acao
		$this->form->addQuickAction( 'Salvar', $action1, 'ico_save.png' );
		$this->form->addQuickAction( 'Voltar', new TAction( array( 'MunicipioList', 'onReload' ) ), 'ico_datagrid.gif' );

        // instancia objeto DataGrid
        $this->datagrid = new TDatagridTables;

        // instancia as colunas da DataGrid
        $dglatitude = new TDataGridColumn('latitude', 'Latitude', 'left', 1350);
        $dglongitude = new TDataGridColumn('longitude', 'Longitude', 'left', 1350);

        // adiciona as colunas a DataGrid
        $this->datagrid->addColumn($dglatitude);
        $this->datagrid->addColumn($dglongitude);

        // instancia duas acoes da DataGrid
        $action1 = new TDataGridAction(array($this, 'onEdit'));
        $action1->setLabel('Editar');
        $action1->setImage('ico_edit.png');
        $action1->setField('id');
        $action1->setFk('municipio_id');

        $action2 = new TDataGridAction(array($this, 'onDelete'));
        $action2->setLabel('Deletar');
        $action2->setImage('ico_delete.png');
        $action2->setField('id');
        $action2->setFk('municipio_id');

        // adiciona as acoes a DataGrid
        $this->datagrid->addAction($action1);
        $this->datagrid->addAction($action2);

        // cria o modelo da DataGrid, montando sua estrutura
        $this->datagrid->createModel();

        // monta a paina atraves de uma tabela
        $panel = new TPanelForm(700, 500);
        $panel->put($this->form, 0, 0);
        $panel->put($this->datagrid, 150, 210);

        // adiciona a tabela a pagina
        parent::add($panel);
		
    }
    
    //------------------Pedro Henrique----------------------
    public static function onChangeRadio($param) {
    
    	switch ($param['tipoCoord']) {
    
    		case '1':
    			TEntry::clearField($param['formName'], 'grausLat');
    			TEntry::clearField($param['formName'], 'minutosLat');
    			TEntry::clearField($param['formName'], 'segundosLat');
    			TEntry::clearField($param['formName'], 'grausLon');
    			TEntry::clearField($param['formName'], 'minutosLon');
    			TEntry::clearField($param['formName'], 'segundosLon');
    
    			TEntry::disableField($param['formName'], 'grausLat');
    			TEntry::disableField($param['formName'], 'minutosLat');
    			TEntry::disableField($param['formName'], 'segundosLat');
    			TEntry::disableField($param['formName'], 'grausLon');
    			TEntry::disableField($param['formName'], 'minutosLon');
    			TEntry::disableField($param['formName'], 'segundosLon');
    
    			TEntry::enableField($param['formName'], 'latitude');
    			TEntry::enableField($param['formName'], 'longitude');
    			break;
    
    		case '2':
    			TEntry::clearField($param['formName'], 'latitude');
    			TEntry::clearField($param['formName'], 'longitude');
    
    			TEntry::disableField($param['formName'], 'latitude');
    			TEntry::disableField($param['formName'], 'longitude');
    
    			TEntry::enableField($param['formName'], 'grausLat');
    			TEntry::enableField($param['formName'], 'minutosLat');
    			TEntry::enableField($param['formName'], 'segundosLat');
    			TEntry::enableField($param['formName'], 'grausLon');
    			TEntry::enableField($param['formName'], 'minutosLon');
    			TEntry::enableField($param['formName'], 'segundosLon');
    			break;
    	}
    }
    //------------------------------------------------------

    /*
     * metodo onReload()
     * Carrega a DataGrid com os objetos do banco de dados
     */
    function onReload() {

        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instancia um repositorio da Classe
        $repository = new TRepository('MunicipioCoordenadasRecord');

        // cria um criterio de selecao
        $criteria = new TCriteria;

        //filtra pelo campo selecionado pelo usu�rio
        $criteria->add(new TFilter( 'municipio_id', '=', filter_input( INPUT_GET, 'fk' ) ) );

        //verifica quantos registros a consulta vai retornar
        $registros = $repository->count($criteria);

        // carrega os objetos de acordo com o criterio
        $cadastros = $repository->load($criteria);

        $this->datagrid->clear();
        if ($cadastros) {
            // percorre os objetos retornados
            foreach ($cadastros as $cadastro) {
                // adiciona o objeto na DataGrid
                $this->datagrid->addItem($cadastro);
            }
        }


        // finaliza a transacao
        TTransaction::close();
        $this->loaded = true;
    }

    /*
     * metodo onDelete()
     * Executada quando o usuario clicar no botao excluir da datagrid
     * Pergunta ao usuario se deseja realmente excluir um registro
     */

    function onDelete($param) {
        // obtem o parametro $key
        $key = $param['key'];

        // define duas acoes
        $action1 = new TAction(array($this, 'Delete'));
        $action2 = new TAction(array($this, 'onReload'));

        // define os parametros de cada acao
        $action1->setParameter('key', $key);
        $action2->setParameter('key', $key);

        //encaminha a chave estrangeira
        $action1->setParameter( 'fk', filter_input( INPUT_GET, 'fk' ) );
        $action2->setParameter( 'fk', filter_input( INPUT_GET, 'fk' ) );
        // exibe um dialogo ao usuario
        new TQuestion('Deseja realmente excluir o registro ?', $action1, $action2);
    }

    /*
     * metodo Delete()
     * Exclui um registro
     */

    function Delete($param) {
        // obtem o parametro $key
        $key = $param['key'];
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instanicia objeto Record
        $cadastro = new MunicipioCoordenadasRecord($key);

        try {
            // deleta objeto do banco de dados
            $cadastro->delete();

            // finaliza a transacao
            TTransaction::close();
        } catch (Exception $e) { // em caso de exce??o
            // exibe a mensagem gerada pela exce??o
            new TMessage('error', $e->getMessage());
            // desfaz todas altera??es no banco de dados
            TTransaction::rollback();
        }

        // re-carrega a datagrid
        $this->onReload();
        // exibe mensagem de sucesso
        new TMessage('info', "Registro Excluido com sucesso");
    }

    /*
     * metodo show()
     * Exibe a pagina
     */
    function show() {
        // carrega os dados no datagrid
        $this->onReload();
        //chama o metodo show da super classe
        parent::show();
    }

    /*
     * metodo onSave()
     * Executada quando o usuario clicar no botao salvar do formulario
     */
    function onSave() {
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // obtem os dados no formulario em um objeto CarroRecord
        $cadastro = $this->form->getData('MunicipioCoordenadasRecord');

         //lanca o default
        $cadastro->usuarioalteracao = $_SESSION['usuario'];
        $cadastro->dataalteracao = date("d/m/Y H:i:s");
        
        //antes de armazenar verifica se algum campo eh requerido e nao foi informado
        $dados = $cadastro->toArray();
        $obj = implode(" , ",$dados); //converte os dados do array para string
        
        $msg = '';
        $icone = 'info';


        if (empty ($dados['municipio_id'])){
           $msg .= 'O municipio deve ser informado.<br/>';
        } 
        
        if (empty ($dados['latitude'])){
           $msg .= ' A latitude deve ser informada.<br/>';
        }
        
        if (empty ($dados['longitude'])){
           $msg .= ' A longitude deve ser informada.<br/>';
        }

        try {

            if ($msg == '') {
            	
            	if ($cadastro->tipoCoord === '2') {
            		$cadastro->latitude = convertCoord('DECIMAIS_LAT',
            				$cadastro->grausLat, $cadastro->minutosLat, $cadastro->segundosLat);
            		$cadastro->longitude = convertCoord('DECIMAIS_LON',
            				$cadastro->grausLon, $cadastro->minutosLon, $cadastro->segundosLon);
            	}
            	unset($cadastro->grausLat);
            	unset($cadastro->minutosLat);
            	unset($cadastro->segundosLat);
            	unset($cadastro->grausLon);
            	unset($cadastro->minutosLon);
            	unset($cadastro->segundosLon);
            	unset($cadastro->tipoCoord);
                // armazena o objeto
                $cadastro->store();
                $msg = 'Dados armazenados com sucesso';
                
                // finaliza a transacao
                TTransaction::close();
            } else {
                $icone = 'error';
            }

            if ($icone == 'error') {
                // exibe mensagem de erro
                new TMessage($icone, $msg);
                $this->form->setData($cadastro);   // fill the form with the active record data
            } else {
                // exibe um dialogo ao usuario
                $param = array();
                $param['fk'] = $dados['municipio_id'];

                new TMessage("info", "Registro salvo com sucesso!");
                TApplication::gotoPage('MunicipioCoordenadasDetalhe','onReload',$param); // reload
            }
        } catch (Exception $e) { // em caso de exce��o
            // exibe a mensagem gerada pela exce��o
            new TMessage('error', $e->getMessage());
            // desfaz todas altera��es no banco de dados
            TTransaction::rollback();
            
            self::onChangeRadio(array(
            		'tipoCoord' => $cadastro->tipoCoord,
            		'formName' => $this->form->getName()
            ));
            $this->form->setData($cadastro);   // fill the form with the active record data
        }
    }

    /*
     * metodo onEdit()
     * Edita os dados de um registro
     */

    function onEdit($param) {
        // obtem o parametro $key
        $key = $param['key'];
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $cadastro = new MunicipioCoordenadasRecord($key);
        // lanca os dados no formulario
        $this->form->setData($cadastro);

        // finaliza a transacao
        TTransaction::close();
    }

}
?>