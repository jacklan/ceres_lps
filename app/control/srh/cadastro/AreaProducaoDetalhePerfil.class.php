<?php

/*
 * classe AreaProducaoDetalhe
 * Cadastro de AreaProducao: Contem a listagem e o formulario de busca
 */
use Adianti\Database\TFilter1;
use Adianti\Widget\Datagrid\TDatagridTables;

class AreaProducaoDetalhePerfil extends TPage {

    private $form;     // formulario de cadastro
    private $datagrid; // listagem

    /*
     * metodo construtor
     * Cria a pagina, o formulario e a listagem
     */

    public function __construct() {
        parent::__construct();
		
		$this->form = new TQuickForm;
        $this->form->class = 'form_AreaProducao';
        $this->form->setFormTitle('<font color="red" size="3" face="Arial"><b>Meu Perfil &raquo; Produ&ccedil;&atilde;o dos Servidores &raquo; &Aacute;rea da Produ&ccedil;&atilde;o</b></font>');

        // cria os campos do formulario
        $codigo = new THidden('id');
		$servidor_id = new THidden('servidor_id');
        $codigo->setEditable(false);
        $areaconhecimento_id = new TCombo('areaconhecimento_id');

        //Campos Hidden
        $usuarioalteracao = new THidden('usuarioalteracao');
        $dataalteracao = new THidden('dataalteracao');
		
		// cria um rotulo para o titulo
        $titulo = new TLabel('<div style="position:floatval; width: 200px;"> <b>* Campo obrigatorio</b></div>');
        $titulo->setFontFace('Arial');
        $titulo->setFontColor('red');
        $titulo->setFontSize(10);

        //cria a colecao da tabela estrangeira para a area conhecimento
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $repository = new TRepository('AreaConhecimentoRecord');
        // carrega todos os objetos
        $collection = $repository->load(new TCriteria);
        //adiciona os objetos no combo
        foreach ($collection as $object) {
            $items[$object->id] = $object->nome;
        }

        // adiciona as opcoes na combo
        $areaconhecimento_id->addItems($items);

        // finaliza a transacao
        TTransaction::close();
        
        
        //cria a colecao da tabela estrangeira do Cabecalho
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $cadastro = new ServidorRecord($_SESSION['servidor_id']);
        // cria um criterio de selecao, ordenado pelo id do servidor
        //adiciona os objetos na tela com as informacoes do servidor
        if ($cadastro) {
            $nome = new TLabel($cadastro->nome);
            $matricula = new TLabel($cadastro->matricula);
        }
        // finaliza a transacao
        TTransaction::close();

		/*
        //cria a colecao da tabela estrangeira do Cabecalho
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
	    // instancia objeto da Classe Record
        $repository = new TRepository('ProducaoRecord');
		$criteria = new TCriteria();
		 //filtra pelo campo selecionado pelo usuário
        $criteria->add(new TFilter('servidor_id', '=', filter_input ( INPUT_GET, 'fk' )));
		
        // carrega todos os objetos
        $collection = $repository->load($criteria);
		
		if ($collection) {
        //adiciona os objetos no combo
        foreach ($collection as $object) {
            //$items[$object->id] = $object->titulo;
           $titulo = new TLabel($object->titulo);
			}
        }
		
        // instancia objeto da Classe Record
		//  $cadastro1 = new ProducaoRecord(filter_input(INPUT_GET,'fk'));
        // cria um criterio de selecao, ordenado pelo id do servidor
        //adiciona os objetos na tela com as informacoes do servidor
		
		        // finaliza a transacao
        TTransaction::close();
		*/
		
        //coloca o valor do relacionamento
        $id = new THidden('producao_id');
        $id->setValue(filter_input(INPUT_GET,'key'));

        // define os tamanhos dos campos

        $codigo->setSize(40);
        $areaconhecimento_id->setSize(500);
		
		// define os campos
		$this->form->addQuickField(null, $codigo, 1);
		//$this->form->addQuickField(null, $servidor_id, 1);
		$this->form->addQuickField("Nome", $nome, 300);
		$this->form->addQuickField("Matrícula", $matricula, 200);
		$this->form->addQuickField(null, $id, 1);
		$this->form->addQuickField('Titulo', $areaconhecimento_id, 10);
				
	    //$actionSave = new TAction(array($this, 'onSave'));
	//	$actionSave->setParameter( 'key', filter_input ( INPUT_GET, 'key' ) );
	//	$actionSave->setParameter( 'fk', filter_input ( INPUT_GET, 'fk' ) );

	    $actionVoltar =	new TAction(array('ProducaoDetalhePerfil', 'onReload'));
		//$actionVoltar->setParameter( 'key', filter_input ( INPUT_GET, 'key' ) );
		//$actionVoltar->setParameter( 'fk', filter_input ( INPUT_GET, 'fk' ) );
		
        // define a acao do botao
        $this->form->addQuickAction('Salvar', new TAction(array($this, 'onSave')), 'ico_save.png');
	    $this->form->addQuickAction('Voltar', $actionVoltar , 'ico_datagrid.gif');

        // instancia objeto DataGrid
        $this->datagrid = new TDataGridTables;

        // instancia as colunas da DataGrid
        $dgareaconhecimento = new TDataGridColumn('nome_areaconhecimento', '&Aacute;rea Conhecimento', 'left', 800);

        // adiciona as colunas a DataGrid
        $this->datagrid->addColumn($dgareaconhecimento);
		
		// instancia duas acoes da DataGrid
        $action1 = new TDataGridAction(array($this, 'onEdit'));
        $action1->setLabel('Editar');
        $action1->setImage('ico_edit.png');
        $action1->setField('id');
        $action1->setFk('producao_id');

        $action2 = new TDataGridAction(array($this, 'onDelete'));
        $action2->setLabel('Deletar');
        $action2->setImage('ico_delete.png');
        $action2->setField('id');
        $action2->setFk('producao_id');
        
        // adiciona as acoes a DataGrid
        $this->datagrid->addAction($action1);
        $this->datagrid->addAction($action2);

        // cria o modelo da DataGrid, montando sua estrutura
        $this->datagrid->createModel();

        // monta a paina atraves de uma tabela
        $panel = new TPanelForm(700, 500);
        $panel->put($this->form, 0, 0);
        $panel->put($this->datagrid, 150, 220);


        // adiciona a tabela a pagina
        parent::add($panel);
    }

    /*
     * metodo onReload()
     * Carrega a DataGrid com os objetos do banco de dados
     */

    function onReload() {

        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instancia um repositorio da Classe
        $repository = new TRepository('AreaProducaoRecord');

        // cria um criterio de selecao
        $criteria = new TCriteria;

        //filtra pelo campo selecionado pelo usuário
        $criteria->add(new TFilter('producao_id', '=', filter_input ( INPUT_GET, 'key' )));

        // carrega os objetos de acordo com o criterio
        $cadastros = $repository->load($criteria);

        $this->datagrid->clear();
        if ($cadastros) {
            // percorre os objetos retornados
            foreach ($cadastros as $cadastro) {
                // adiciona o objeto na DataGrid
                $this->datagrid->addItem($cadastro);
            }
        }

        // finaliza a transacao
        TTransaction::close();
        $this->loaded = true;
    }

    /*
     * metodo onDelete()
     * Executada quando o usuario clicar no botao excluir da datagrid
     * Pergunta ao usuario se deseja realmente excluir um registro
     */

    function onDelete($param) {
        // obtem o parametro $key
        $key = $param['key'];

        // define duas acoes
        $action1 = new TAction(array($this, 'Delete'));
        
        // define os parametros de cada acao
        $action1->setParameter('key', $key);
		//$action1->setParameter('msg', 'delete');

		//encaminha a chave estrangeira
        $action1->setParameter('fk', filter_input ( INPUT_GET, 'fk' ));
        //encaminha a chave estrangeira
        //$action1->setParameter('fk', $_GET['fk']);
        // exibe um dialogo ao usuario
        new TQuestion('Deseja realmente excluir o registro ?', $action1, $action2);
    }

    /*
     * metodo Delete()
     * Exclui um registro
     */

    function Delete($param) {
        // obtem o parametro $key
        $key = $param['key'];
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instanicia objeto Record
        $cadastro = new AreaProducaoRecord($key);

        try {
            // deleta objeto do banco de dados
            $cadastro->delete();
			
			// exibe um dialogo ao usuario
            new TMessage("info", "Registro deletado com sucesso!");

            // finaliza a transacao
            TTransaction::close();
        } catch (Exception $e) { // em caso de exce��o
            // exibe a mensagem gerada pela exce��o
            new TMessage('error', $e->getMessage());
            // desfaz todas altera��es no banco de dados
            TTransaction::rollback();
        }

        // re-carrega a datagrid
        $this->onReload();
        
    }

    /*
     * metodo show()
     * Exibe a pagina
     */

    function show() {
        // carrega os dados no datagrid
        $this->onReload();
        //chama o metodo show da super classe
        parent::show();
    }

    /*
     * metodo onSave()
     * Executada quando o usuario clicar no botao salvar do formulario
     */

    function onSave() {
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // obtem os dados no formulario em um objeto Record da Classe
        $cadastro = $this->form->getData('AreaProducaoRecord');

        //antes de armazenar verifica se algum campo eh requerido e nao foi informado
        $dados = $cadastro->toArray();

        $msg = '';
        $icone = 'info';

        if (empty($dados['areaconhecimento_id'])) {
            $msg .= 'A área de Conhecimento deve ser informada';
        }

        try {

            if ($msg == '') {
                // armazena o objeto
                $cadastro->store();
                $msg = 'Dados armazenados com sucesso';

                // finaliza a transacao
                TTransaction::close();
            } else {
                $icone = 'error';
            }

            if ($icone == 'error') {
                // exibe mensagem de erro
                new TMessage($icone, $msg);
            } else {
				
				$param = array();
				$param['fk'] = $dados['servidor_id'];
				$param['key'] = $dados['producao_id'];
				
				// exibe um dialogo ao usuario
                new TMessage("info", "Registro salvo com sucesso!");
				TApplication::gotoPage('AreaProducaoDetalhePerfil','onReload', $param); // reload
                //chama o formulario com o grid
                //msgAlert('AreaProducaoDetalhePerfil', 'onReload', 'key=' . $_GET['key'] . '&fk=' . $_GET['fk'], 'sucess');
            }
        } catch (Exception $e) { // em caso de exc
            // exibe a mensagem gerada pela exce��o
            new TMessage('error', $e->getMessage());
            // desfaz todas altera��es no banco de dados
            TTransaction::rollback();
        }
    }

    /*
     * metodo onEdit()
     * Edita os dados de um registro
     */

    function onEdit($param) {
        
		try {
            if (isset($param['key'])) {

                // get the parameter $key
                $key = $param['key'];

                TTransaction::open('pg_ceres');   // open a transaction with database 'samples'

                $object = new AreaProducaoRecord($key);        // instantiates object City
				
				$object->dataconclusao = TDate::date2br( $object->dataconclusao );
				
                $this->form->setData($object);   // fill the form with the active record data

                TTransaction::close();           // close the transaction
            } else {
                $this->form->clear();
            }
        } catch (Exception $e) { // in case of exception
            // shows the exception error message
            new TMessage('error', '<b>Error</b> ' . $e->getMessage());
            // undo all pending operations
            TTransaction::rollback();
        }		
		
    }

}

?>