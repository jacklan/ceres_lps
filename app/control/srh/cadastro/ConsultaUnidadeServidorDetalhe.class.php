<?php

/*
 * classe UnidadeServidorDetalhe
 * Cadastro de UnidadeServidor: Contem a listagem e o formulario de busca
 * Autor: Jackson Meires
 * Data:14/10/2016
 */

use Adianti\Widget\Datagrid\TDatagridTables;

class ConsultaUnidadeServidorDetalhe extends TWindow {

    private $datagrid; // listagem

    /*
     * metodo construtor
     * Cria a pagina, o formulario e a listagem
     */

    public function __construct() {
        parent::__construct();

        // instancia objeto DataGrid
        $this->datagrid = new \Adianti\Widget\Datagrid\TDataGrid;

        // instancia as colunas da DataGrid
        $dgservidor = new TDataGridColumn('nome_servidor', 'Servidor', 'left', 200);
        $dgsetor = new TDataGridColumn('nome_setor', 'Setor', 'left', 100);
        $dgdatainicio = new TDataGridColumn('datainicio', 'Data In&iacutecio', 'left', 80);

        // adiciona as colunas a DataGrid
        $this->datagrid->addColumn($dgservidor);
        $this->datagrid->addColumn($dgsetor);
        $this->datagrid->addColumn($dgdatainicio);

        // cria o modelo da DataGrid, montando sua estrutura
        $this->datagrid->createModel();

        // monta a paina atraves de uma tabela
        $panel = new TPanelForm(300, 500);
        $panel->put($this->datagrid, 150, 250);

        // adiciona a tabela a pagina
        parent::add($panel);
    }

    /*
     * metodo onReload()
     * Carrega a DataGrid com os objetos do banco de dados
     */

    function onReload() {
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        $conn = TTransaction::get(); // obtém a conexão 
        //custom query
        $sth = $conn->prepare("select s.nome as nome_servidor, st.nome as nome_setor, us.datainicio
                    from unidadeservidor us
                    inner join servidor s on s.id= us.servidor_id
                    inner join setor st on st.id =us.setor_id where us.unidadeoperativa_id={$_GET['fk']} and us.datafim is null order by s.nome ");

        $sth->execute();

        $this->datagrid->clear();
        if ($sth) {
            // percorre os objetos retornados
            foreach ($sth as $item) {

                $object = new stdClass();

                $object->nome_servidor = $item['nome_servidor'];
                $object->nome_setor = $item['nome_setor'];
                $object->datainicio = TDate::date2br($item['datainicio']);

                // adiciona o objeto na DataGrid
                $this->datagrid->addItem($object);
            }
        }
        // finaliza a transacao
        TTransaction::close();
        $this->loaded = true;
    }

    /*
     * metodo show()
     * Exibe a pagina
     */

    function show() {
        // carrega os dados no datagrid
        $this->onReload();
        //chama o metodo show da super classe
        parent::show();
    }

}
