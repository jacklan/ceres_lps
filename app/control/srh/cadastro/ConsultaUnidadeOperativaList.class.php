<?php

//ini_set('display_errors', 1);
//ini_set('display_startup_erros', 1);
//error_reporting(E_ALL);

use Adianti\Database\TFilter1;
use Adianti\Widget\Datagrid\TDatagridTables;

/*
 * classe ConsultaUnidadeOperativaList
 * Cadastro de Consulta Unidade Operativa: Contem a listagem e o formulario de busca
 * Autor: Jackson Meires
 * Data:14/10/2016
 */

class ConsultaUnidadeOperativaList extends TPage {

    private $form;     // formulario de cadastro
    private $datagrid; // listagem   

    /*
     * metodo construtor
     * Cria a pagina, o formulario e a listagem
     */

    public function __construct() {
        parent::__construct();

        // instancia um formulario
        $this->form = new TForm('form_busca_UnidadeOperativa');

        // instancia uma tabela
        $panel = new TPanelForm(900, 100);

        // adiciona a tabela ao formulario
        $this->form->add($panel);

        // cria um rótulo para o título
        $titulo = new TLabel('Listagem Unidades Operativas');
        $titulo->setFontFace('Arial');
        $titulo->setFontColor('red');
        $titulo->setFontSize(12);

        // adiciona o campo Titulo
        $panel->put($titulo, $panel->getColuna(), $panel->getLinha());

        // cria os campos do formulario
        $opcao = new TCombo('opcao');

        // cria um vetor com as opcoes da combo
        $items = array();
        $items['nome'] = 'Nome';

        // adiciona as opcoes na combo
        $opcao->addItems($items);
        //coloca o valor padrao no combo
        $opcao->setValue('nome');
        $opcao->setSize(40);

        $nome = new TEntry('nome');
        $nome->setSize(40);

        // cria um botao de acao (buscar)
        $find_button = new TButton('busca');

        // define a acao do botao buscar
        $find_button->setAction(new TAction(array($this, 'onSearch')), 'Buscar');

        // adiciona o campo
        $panel->putCampo(null, 'Selecione o campo:', 0, 0);
        $panel->put($opcao, $panel->getColuna(), $panel->getLinha());
        $panel->put(new TLabel('Informe o valor da busca:'), $panel->getColuna(), $panel->getLinha());
        $panel->put($nome, $panel->getColuna(), $panel->getLinha());
        $panel->put($find_button, $panel->getColuna(), $panel->getLinha());

        // define quais sao os campos do formulario
        $this->form->setFields(array($nome, $opcao, $find_button));

        // instancia objeto DataTables Resposivo
        $this->datagrid = new TDatagridTables;

        // instancia as colunas da DataGrid
        // $dgcodigo = new TDataGridColumn('id', 'C&oacute;digo', 'center', 80);
        $dgmunicipio = new TDataGridColumn('nome', 'Munic&iacute;pio', 'left', 350);
        $dgregional = new TDataGridColumn('nome_regional', 'Regional', '$align', 'left', 350);
        $dgtelefone = new TDataGridColumn('telefone', 'Telefone', 'left', 60);
        $dgendereco = new TDataGridColumn('endereco', 'Endere&ccedil;o', 'left', 200);
        $dgresp = new TDataGridColumn('nome_responsavel', 'Responsavel', 'left', 200);

        // adiciona as colunas a DataGrid
        //$this->datagrid->addColumn($dgcodigo);
        $this->datagrid->addColumn($dgmunicipio);
        $this->datagrid->addColumn($dgregional);
        $this->datagrid->addColumn($dgtelefone);
        $this->datagrid->addColumn($dgendereco);
        $this->datagrid->addColumn($dgresp);

        // instancia duas acoes da DataGrid
        $action = new TDataGridAction(array('ConsultaUnidadeServidorDetalhe', 'onReload'));
        $action->setLabel('Unidade do Servidor');
        $action->setImage('ico_lotacao.png');
        $action->setField('id');
        $action->setFk('id');

        // adiciona as acoes a DataGrid
        $this->datagrid->addAction($action);

        // cria o modelo da DataGrid, montando sua estrutura
        $this->datagrid->createModel();

        // monta a paina atraves de uma tabela
        $panel = new TPanelForm(700, 500);
        $panel->put($this->form, 0, 0);
        $panel->put($this->datagrid, 150, 115);

        // adiciona a tabela a pagina
        parent::add($panel);
    }

    /*
     * metodo onReload()
     * Carrega a DataGrid com os objetos do banco de dados
     */

    function onReload() {
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instancia um repositorio para Carros
        $repository = new TRepository('UnidadeOperativaRecord');

        $criteria = new TCriteria;
        $criteria->add(new TFilter('empresa_id', '=', $_SESSION['empresa_id']));
        $criteria->setProperty('order', 'nome');

        // carrega os objetos de acordo com o criterio
        $cadastros = $repository->load($criteria);

        $this->datagrid->clear();
        if ($cadastros) {
            // percorre os objetos retornados
            foreach ($cadastros as $cadastro) {
                // adiciona o objeto na DataGrid
                $this->datagrid->addItem($cadastro);
            }
        }
        // finaliza a transacao
        TTransaction::close();
        $this->loaded = true;
    }

    /*
     * metodo onSearch()
     * Carrega a DataGrid com os objetos do banco de dados
     */

    function onSearch() {
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instancia um repositorio para Carros
        $repository = new TRepository('UnidadeOperativaRecord');

        //obtem os dados do formulario de busca
        $data = $this->form->getData();
        //obtem os dados do formulario de busca
        //obtem os dados do formulario de busca
        $campo = $data->opcao;
        $dados = $data->nome;

        if ((!$dados) || (!$campo)) {
            //pega os dados da url
            $campo = ''; //$_GET['campo'];
            $dados = ''; //$_GET['dados'];
        }

        // cria um criterio de selecao, ordenado pelo id
        $criteria = new TCriteria;
        $criteria->setProperty('order', 'nome');

        //verifica se o usuario preencheu o formulario
        if ($dados) {
            if (is_numeric($dados)) {
                $criteria->add(new TFilter($campo, 'like', '%' . $dados . '%'));
            } else {
                //filtra pelo campo selecionado pelo campo ignore case
                $criteria->add(new TFilter1('special_like(' . $campo . ",'" . $dados . "')"));
            }
        }

        // carrega os objetos de acordo com o criterio
        $cadastros = $repository->load($criteria);

        $this->datagrid->clear();
        if ($cadastros) {
            // percorre os objetos retornados
            foreach ($cadastros as $cadastro) {
                // adiciona o objeto na DataGrid
                $this->datagrid->addItem($cadastro);
            }
        }

        // finaliza a transacao
        TTransaction::close();
        $this->loaded = true;
    }

    /*
     * metodo show()
     * Exibe a pagina
     * 
     */

    function show() {
        // carrega os dados no datagrid
        $this->onReload();
        //chama o metodo show da super classe
        parent::show();
    }

}
