<?php
/**
 * Search Box
 *
 * @version    1.0
 * @package    samples
 * @subpackage tutor
 * @author     Pablo Dall'Oglio
 * @copyright  Copyright (c) 2006-2014 Adianti Solutions Ltd. (http://www.adianti.com.br)
 * @license    http://www.adianti.com.br/framework-license
 */

//ini_set('display_errors', 1);
//ini_set('display_startup_erros', 1);
//error_reporting(E_ALL);

class SearchBoxServidor extends TPage
{
    private $form;
    
    /**
     * Constructor method
     */
    public function __construct()
    {
        parent::__construct('serach_box');
        
        $this->form = new TForm('search_box');
        $table = new TTable;
        $table->style = 'float:right';
        
        $row = $table->addRow();
        $input = new TMultiSearch('input');
        $input->setSize(240,28);
        $input->addItems( $this->getPrograms() );
        $input->setMinLength(1);
        $input->setMaxSize(1);
        
        $button = new TButton('search');
        $button->style = 'margin-top:0px; height:30px;';
        $button->setImage('bs:search green');
        $button->setAction( new TAction(array($this, 'loadProgram')));
        
        $this->form->setFields(array($input, $button));
        $row->addCell($input);
        $row->addCell($button);
        $this->form->add($table);
        parent::add($this->form);
    }
    
    /**
     * Returns an indexed array with all programs
     */
    public function getPrograms()
    {
        $xml = simplexml_load_file('menu.xml');
        foreach ($xml as $xmlElement)
        {
            $atts   = $xmlElement->attributes();
            $index  = (string) $atts['label'];
            
            if ($xmlElement->menu)
            {
                foreach ($xmlElement->menu->menuitem as $subXmlElement)
                {
                    $subindex = (string) $subXmlElement['label'];
                    $subatts   = $subXmlElement->attributes();
                    if ($subXmlElement->menu)
                    {
                        foreach ($subXmlElement->menu->menuitem as $option)
                        {
                            $optatts   = $option->attributes();
                            $label  = (string) $option['label'];
                            $action = (string) $option-> action;
                            $icon   = (string) $option-> icon;
                            $items[ $action ] = $label;
                        }
                    }
                }
            }
        }
        return $items;
    }
    
    /**
     * Load an specific program
     */
    public function loadProgram($param)
    {
        $data = $this->form->getData();
        $programs = array_keys($data->input);
        $program = $programs[0];
        TApplication::loadPage($program);
    }
}
