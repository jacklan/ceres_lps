<?php

use Lib\Funcoes\Util;

/**
 * ExpertiseWindowForm
 *
 * @version    1.0
 * @package    control
 * @subpackage sgt
 * @author     Jackson Meires
 * @copyright  Copyright (c) 2006-2014 Adianti Solutions Ltd. (http://www.adianti.com.br)
 * @license    http://www.adianti.com.br/framework-license
 */
class ExpertiseWindowForm extends TWindow {

    private $form;

    /**
     * Class constructor
     * Creates the page
     */
    function __construct() {
        parent::__construct();
        parent::setSize(570, 200);

        // create the form using TQuickForm class
        $this->form = new TQuickForm;
        // $this->form->class = 'tform';
        $this->form->setFormTitle('<font color=red><b>Expertise</b></font>');
        $this->form->style = 'width: 500px';

        // create the form fields
        $code = new THidden('id');
        $nome = new TEntry('nome');

        $nome->setProperty('style', 'text-transform: uppercase');

        // add the fields inside the form
        $this->form->addQuickField('Codigo', $code, 0);
        $this->form->addQuickField('Name', $nome, 80);

        $action = new TAction(array($this, 'onSave'));
        $action->setParameter('fk', filter_input(INPUT_GET, 'fk'));

        // define the form action 
        $this->form->addQuickAction('Salvar', $action, 'ico_save.png')->class = 'btn btn-info';

        parent::add($this->form);
    }

    public function onSave() {
        try {
            // inicia transacao com o banco 'pg_ceres'
            TTransaction::open('pg_ceres');
            // obtem os dados no formulario em um objeto Record da Classe
            $object = $this->form->getData('ExpertiseRecord');

            $object->nome = Util::removerAcentuacaoUpper($object->nome);

            $object->usuarioalteracao = $_SESSION['usuario'];
            $object->dataalteracao = date("d/m/Y H:i:s");

            $msg = '';
            $icone = 'info';

            if ($msg == '') {
                // armazena o objeto
                $object->store();
                $msg = 'Dados armazenados com sucesso';

                // finaliza a transacao
                TTransaction::close();
            } else {
                $icone = 'error';
            }

            if ($icone == 'error') {
                // exibe mensagem de erro
                new TMessage($icone, $msg);
                $this->form->setData($object);   // fill the form with the active record data
            } else {

                $action = new TAction(array('ExpertiseFormSeek', 'onReload'));
                $action->setParameter('fk', filter_input(INPUT_GET, 'fk'));

                // exibe um dialogo ao usuario
                new TMessage("info", "Registro salvo com sucesso!", $action);
            }
        } catch (Exception $e) { // em caso de exc
            // exibe a mensagem gerada pela exceção
            new TMessage('error', $e->getMessage());
            // desfaz todas alterações no banco de dados
            TTransaction::rollback();
            $this->form->setData($this->form->getData());   // fill the form with the active record data
        }
    }

    public function onLoad() {
        
    }

}
