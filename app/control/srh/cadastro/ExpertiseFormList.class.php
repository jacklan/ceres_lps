<?php

use Lib\Funcoes\Util;
use Adianti\Widget\Datagrid\TDatagridTables;

/**
 * ExpertiseFormList
 *
 * @version    1.0
 * @package    control
 * @subpackage SRH
 * @author     Jackson Meires
 * @copyright  Copyright (c) 2006-2014 Adianti Solutions Ltd. (http://www.adianti.com.br)
 * @license    http://www.adianti.com.br/framework-license
 */
class ExpertiseFormList extends TPage {

    private $form;
    private $datagrid; // listagem

    /**
     * Class constructor
     * Creates the page
     */

    function __construct() {
        parent::__construct();

        // create the form using TQuickForm class
        $this->form = new TQuickForm;
        // $this->form->class = 'tform';
        $this->form->setFormTitle('<font color=red><b>Formul&aacute;rio de Expertise</b></font>');
        $this->form->style = 'width: 500px';

        // create the form fields
        $code = new THidden('id');
        $nome = new TEntry('nome');

        $nome->setProperty('style', 'text-transform: uppercase');

        // add the fields inside the form
        $this->form->addQuickField('Codigo', $code, 0);
        $this->form->addQuickField('Name', $nome, 80);

        $action = new TAction(array($this, 'onSave'));
        // $action->setParameter('fk', filter_input(INPUT_GET, 'fk'));
        // define the form action 
        $this->form->addQuickAction('Salvar', $action, 'ico_save.png')->class = 'btn btn-info';

        // instancia objeto DataGrid
        $this->datagrid = new TDatagridTables;

        // instancia as colunas da DataGrid
        $dgnome = new TDataGridColumn('nome', 'Nome', 'left', 1200);

        // adiciona as colunas a DataGrid
        $this->datagrid->addColumn($dgnome);

        // instancia duas acoes da DataGrid
        $action1 = new TDataGridAction(array($this, 'onEdit'));
        $action1->setLabel('Editar');
        $action1->setImage('ico_edit.png');
        $action1->setField('id');

        $action2 = new TDataGridAction(array($this, 'onDelete'));
        $action2->setLabel('Deletar');
        $action2->setImage('ico_delete.png');
        $action2->setField('id');

        // adiciona as acoes a DataGrid
        $this->datagrid->addAction($action1);
        $this->datagrid->addAction($action2);

        // cria o modelo da DataGrid, montando sua estrutura
        $this->datagrid->createModel();

        // monta a paina atraves de uma tabela
        $panel = new TPanelForm(700, 500);
        $panel->put($this->form, 0, 0);
        $panel->put($this->datagrid, 150, 115);

        parent::add($panel);
    }

    public function onSave() {
        try {
            // inicia transacao com o banco 'pg_ceres'
            TTransaction::open('pg_ceres');
            // obtem os dados no formulario em um objeto Record da Classe
            $object = $this->form->getData('ExpertiseRecord');

            $object->nome = Util::removerAcentuacaoUpper($object->nome);

            $object->usuarioalteracao = $_SESSION['usuario'];
            $object->dataalteracao = date("d/m/Y H:i:s");

            $msg = '';
            $icone = 'info';

            if ($msg == '') {
                // armazena o objeto
                $object->store();
                $msg = 'Dados armazenados com sucesso';

                // finaliza a transacao
                TTransaction::close();
            } else {
                $icone = 'error';
            }

            if ($icone == 'error') {
                // exibe mensagem de erro
                new TMessage($icone, $msg);
                $this->form->setData($object);   // fill the form with the active record data
            } else {

                $action = new TAction(array('ExpertiseFormList', 'onReload'));
                //    $action->setParameter('fk', filter_input(INPUT_GET, 'fk'));
                // exibe um dialogo ao usuario
                new TMessage("info", "Registro salvo com sucesso!", $action);
            }
        } catch (Exception $e) { // em caso de exc
            // exibe a mensagem gerada pela exceção
            new TMessage('error', $e->getMessage());
            // desfaz todas alterações no banco de dados
            TTransaction::rollback();
            $this->form->setData($this->form->getData());   // fill the form with the active record data
        }
    }

    /*
     * metodo onReload()
     * Carrega a DataGrid com os objetos do banco de dados
     */

    function onReload() {

        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instancia um repositorio da Classe
        $repository = new TRepository('ExpertiseRecord');

        // cria um criterio de selecao
        $criteria = new TCriteria;

        //filtra pelo campo selecionado pelo usu�rio
        //    $criteria->add(new TFilter('servidor_id', '=', $_GET['fk']));
        //verifica quantos registros a consulta vai retornar
        $criteria->setProperty('order', 'nome');
        // carrega os objetos de acordo com o criterio
        $cadastros = $repository->load($criteria);

        $this->datagrid->clear();
        if ($cadastros) {
            // percorre os objetos retornados
            foreach ($cadastros as $cadastro) {

                // adiciona o objeto na DataGrid
                $this->datagrid->addItem($cadastro);
            }
        }

        // finaliza a transacao
        TTransaction::close();
        $this->loaded = true;
    }

    /*
     * metodo onDelete()
     * Executada quando o usuario clicar no botao excluir da datagrid
     * Pergunta ao usuario se deseja realmente excluir um registro
     */

    function onDelete($param) {
        // obtem o parametro $key
        $key = $param['key'];

        // define duas acoes
        $action1 = new TAction(array($this, 'Delete'));

        // define os parametros de cada acao
        $action1->setParameter('key', $key);

        //encaminha a chave estrangeira
        // $action1->setParameter('fk', filter_input(INPUT_GET, 'fk'));
        // exibe um dialogo ao usuario
        new TQuestion('Deseja realmente excluir o registro ?', $action1);
    }

    /*
     * metodo Delete()
     * Exclui um registro
     */

    function Delete($param) {
        // obtem o parametro $key
        $key = $param['key'];
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instanicia objeto Record
        $cadastro = new ExpertiseRecord($key);

        try {
            // deleta objeto do banco de dados
            $cadastro->delete();

            // finaliza a transacao
            TTransaction::close();
        } catch (Exception $e) { // em caso de exce��o
            // exibe a mensagem gerada pela exce��o
            new TMessage('error', $e->getMessage());
            // desfaz todas altera��es no banco de dados
            TTransaction::rollback();
        }

        // re-carrega a datagrid
        $this->onReload();
    }

    /*
     * metodo onEdit()
     * Edita os dados de um registro
     */

    function onEdit($param) {
        // obtem o parametro $key
        $key = $param['key'];
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $cadastro = new ExpertiseRecord($key);

        // lanca os dados no formulario
        $this->form->setData($cadastro);

        // finaliza a transacao
        TTransaction::close();
    }

    /*
     * metodo show()
     * Exibe a pagina
     */

    function show() {
        // carrega os dados no datagrid
        $this->onReload();
        //chama o metodo show da super classe
        parent::show();
    }

}
