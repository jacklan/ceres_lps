<?php

/*
 * classe SetorGrupoForm
 * Cadastro de Setor Grupo: Contem o formularo
 */

class UnidadeOperativaForm extends TPage {

    private $form;     // formulario de cadastro

    /*
     * metodo construtor
     * Cria a pagina do formulario
     */

    public function __construct() {
        parent::__construct();

        // instancia um formulario
        $this->form = new TQuickForm;
        $this->form->class = 'form_empresa';
        $this->form->setFormTitle('<font color="red" size="3" face="Arial"><b>Formul&aacute;rio Unidade Operativa</b></font>');

        // cria os campos do formulario
        $codigo = new THidden('id');
        $codigo->setEditable(false);
        $nome = new TEntry('nome');
        $endereco = new TEntry('endereco');
        $telefone = new TEntry('telefone');
        $tipo = new TCombo('tipo');
        $situacao = new TCombo('situacao');
        $biblioteca = new TCombo('biblioteca');
        $regional_id = new TCombo('regional_id');
        $responsavel_id = new TCombo('responsavel_id');
        $municipio_id = new TCombo('municipio_id');
        $latitude = new TEntry('latitude');
        $longitude = new TEntry('longitude');
        $tipopropriedade = new TCombo('tipopropriedade');

        $usuarioalteracao = new THidden('usuarioalteracao');
        $dataalteracao = new THidden('dataalteracao');

        // cria um rotulo para o titulo
        $titulo = new TLabel('<div style="position:floatval; width: 200px;"> <b>* Campo obrigatorio</b></div>');
        $titulo->setFontFace('Arial');
        $titulo->setFontColor('red');
        $titulo->setFontSize(10);

        // carrega o combo
        $items2 = array();
        $items2['NORMAL'] = 'NORMAL';
        $items2['ASSISTIDA'] = 'ASSISTIDA';

        $tipo->setValue('NORMAL');
        $tipo->addItems($items2);

        // carrega o combo
        $items1 = array();
        $items1['SIM'] = 'SIM';
        $items1['NAO'] = 'NAO';

        $biblioteca->addItems($items1);

        // carrega o combo
        $items3 = array();
        $items3['PROPRIA'] = 'PROPRIA';
        $items3['ALUGADA'] = 'ALUGADA';
        $items3['CEDIDA'] = 'CEDIDA';

        $tipopropriedade->addItems($items3);


        //cria a colecao da tabela estrangeira
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $repository = new TRepository('ServidorRecord');
        // carrega todos os objetos
        $criteria = new TCriteria;
        $criteria->setProperty('order', 'nome');
        // carrega os objetos de acordo com o criterio
        $collection = $repository->load($criteria);
        //adiciona os objetos no combo
        foreach ($collection as $object) {
            $items5[$object->id] = $object->nome;
        }

        // adiciona as opcoes na combo
        $responsavel_id->addItems($items5);

        // finaliza a transacao
        TTransaction::close();


        // carrega o combo
        $items1 = array();
        $items1['ATIVO'] = 'ATIVO';
        $items1['INATIVO'] = 'INATIVO';

        $situacao->addItems($items1);

        //cria a colecao da tabela estrangeira
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $repository = new TRepository('MunicipioRecord');
        // carrega todos os objetos
        $collection = $repository->load(new TCriteria);
        //adiciona os objetos no combo
        foreach ($collection as $object) {
            $items6[$object->id] = $object->nome;
        }

        // adiciona as opcoes na combo
        $municipio_id->addItems($items6);

        // finaliza a transacao
        TTransaction::close();

        //cria a colecao da tabela estrangeira
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $repository = new TRepository('RegionalRecord');
        // carrega todos os objetos
        $collection = $repository->load(new TCriteria);
        //adiciona os objetos no combo
        foreach ($collection as $object) {
            $items8[$object->id] = $object->nome;
        }

        // adiciona as opcoes na combo
        $regional_id->addItems($items8);

        // finaliza a transacao
        TTransaction::close();

        // define os campos
        $this->form->addQuickField(null, $codigo, 10);
        $this->form->addQuickField(null, $usuarioalteracao, 10);
        $this->form->addQuickField(null, $dataalteracao, 10);

        $this->form->addQuickField('Nome <font color=red><b>*</b></font>', $nome, 50);
        $this->form->addQuickField('Regional <font color=red><b>*</b></font>', $regional_id, 50);
        $this->form->addQuickField('Munic&iacute;pio <font color=red><b>*</b></font>', $municipio_id, 50);
        $this->form->addQuickField('Endere&ccedil;o', $endereco, 50);
        $this->form->addQuickField('Telefone', $telefone, 50);
        $this->form->addQuickField('Respons&aacute;vel', $responsavel_id, 50);
        $this->form->addQuickField('Tipo <font color=red><b>*</b></font>', $tipo, 50);
        $this->form->addQuickField('Situa&ccedil;&atilde;o', $situacao, 50);
        $this->form->addQuickField('Biblioteca', $biblioteca, 50);
        $this->form->addQuickField('Tipo Propriedade', $tipopropriedade, 50);
        $this->form->addQuickField('Latitude', $latitude, 50);
        $this->form->addQuickField('Longitude', $longitude, 50);
        $this->form->addQuickField(null, $titulo, 50);

        $latitude->setProperty('required', 'required');
        $latitude->setProperty('placeholder', 'Ex.: -6.5421');
        $longitude->setProperty('required', 'required');
        $longitude->setProperty('placeholder', 'Ex.: -6.5421');

        // cria um botao de acao
        $this->form->addQuickAction('Salvar', new TAction(array($this, 'onSave')), 'ico_save.png')->class = 'btn btn-info btnleft';
        $this->form->addQuickAction('Voltar', new TAction(array('UnidadeOperativaList', 'onReload')), 'ico_datagrid.gif');

        // adiciona a tabela a pagina
        parent::add($this->form);
    }

    /*
     * metodo onSave()
     * Executada quando o usuario clicar no botao salvar do formulario
     */

    function onSave() {
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // obtem os dados no formulario em um objeto CarroRecord
        $cadastro = $this->form->getData('UnidadeOperativaRecord');

        //lanca o default
        $cadastro->usuarioalteracao = $_SESSION['usuario'];
        $cadastro->dataalteracao = date("d/m/Y H:i:s");
        $cadastro->empresa_id = $_SESSION['empresa_id'];
        
        //antes de armazenar verifica se algum campo eh requerido e nao foi informado
        $dados = $cadastro->toArray();
        $obj = implode(" , ", $dados); //converte os dados do array para string

        $msg = '';
        $icone = 'info';


        if (empty($dados['nome'])) {
            $msg .= 'O Nome deve ser informado.<br/>';
        }

        if (empty($dados['regional_id'])) {
            $msg .= 'A Regional deve ser informado.<br/>';
        }

        if (empty($dados['municipio_id'])) {
            $msg .= 'O Munic&iacute;pio deve ser informado.<br/>';
        }

        if (empty($dados['tipo'])) {
            $msg .= 'O Tipo deve ser informado.<br/>';
        }

        try {

            if ($msg == '') {
                // armazena o objeto
                $cadastro->store();
                $msg = 'Dados armazenados com sucesso';

                //criando log 
//                TTransaction::setLogger(new TLoggerTXT('tmp/log.txt'));
//                TTransaction::Log(' ---- Insert ---- '. $obj);
                // finaliza a transacao
                TTransaction::close();
            } else {
                $icone = 'error';
            }

            if ($icone == 'error') {
                // exibe mensagem de erro
                new TMessage($icone, $msg);
                $this->form->setData($cadastro);   // fill the form with the active record data
            } else {
                // exibe um dialogo ao usuario
                new TMessage("info", "Registro salvo com sucesso!");
                TApplication::gotoPage('UnidadeOperativaList', 'onReload'); // reload
            }
        } catch (Exception $e) { // em caso de exce��o
            // exibe a mensagem gerada pela exce��o
            new TMessage('error', $e->getMessage());
            // desfaz todas altera��es no banco de dados
            TTransaction::rollback();
            $this->form->setData($cadastro);   // fill the form with the active record data
        }
    }

    /*
     * metodo onEdit()
     * Edita os dados de um registro
     */

    function onEdit($param) {
        try {
            if (isset($param['key'])) {

                // get the parameter $key
                $key = $param['key'];

                TTransaction::open('pg_ceres');   // open a transaction with database 'samples'

                $object = new UnidadeOperativaRecord($key);        // instantiates object City

                $this->form->setData($object);   // fill the form with the active record data

                TTransaction::close();           // close the transaction
            } else {
                $this->form->clear();
            }
        } catch (Exception $e) { // in case of exception
            // shows the exception error message
            new TMessage('error', '<b>Error</b> ' . $e->getMessage());
            // undo all pending operations
            TTransaction::rollback();
        }
    }

}

?>