<?php
//ini_set('display_errors', 1);
//ini_set('display_startup_erros', 1);
//error_reporting(E_ALL);
/*
 * classe ServidorCurriculoValidacao
 * Listagem de Validacao Curriculo: Contem a listagem e o formulario de busca
 */
use Adianti\Widget\Datagrid\TDatagridTables;

class ServidorCurriculoValidacaoList extends TPage {

    private $form;     // formulario de cadastro
    private $datagrid; // listagem

    /*
     * metodo construtor
     * Cria a pagina, o formulario e a listagem
     */

    public function __construct() {
        parent::__construct();

        // instancia um formulario
        $this->form = new TForm('form_ServidorCurriculoValidacao');

        // instancia um Panel
        $panel = new TPanelForm(1000, 100);

        // adiciona o panel ao formulario
        $this->form->add($panel);

        // cria um rotulo para o titulo
        $titulo1 = new TLabel('Curriculo Servidor Valida&ccedil;&atilde;o');
        $titulo1->setFontFace('Arial');
        $titulo1->setFontColor('red');
        $titulo1->setFontSize(18);

        // adiciona o campo titulo da pagina
        $panel->put($titulo1, $panel->getColuna(), $panel->getLinha());

        $opcao = new TCombo('opcao');

        // cria um vetor com as opcoes da combo
        $items = array();
        $items['nome'] = 'Nome';
        $items['matricula'] = 'Matricula';
        // adiciona as opcoes na combo
        $opcao->addItems($items);
        //coloca o valor padrao no combo
        $opcao->setValue('nome');
        $opcao->setSize(150);

        $nome = new TEntry('nome');
        $nome->setSize(170);

        // cria um botao de acao (buscar)
        $find_button = new TButton('busca');

        // define a acao do botao buscar
        $find_button->setAction(new TAction(array($this, 'onReload')), 'Buscar');

        $panel->setLinha(50);
        // adiciona o campo
        //$panel->putCampo(null, 'Selecione o Campo:', 0, 0);
        //$panel->put(new TLabel('Informe o Valor da Busca:'), $panel->getColuna()+160,$panel->getLinha());
        //$panel->setLinha(70);
        //$panel->put($opcao, $panel->getColuna(),$panel->getLinha());
        //$panel->put($nome, $panel->getColuna()+160,$panel->getLinha());
        //$panel->put($find_button, $panel->getColuna()+350,$panel->getLinha());
        //$panel->put($new_button, $panel->getColuna()+420,$panel->getLinha());
        // define quais sao os campos do formulario
        $this->form->setFields(array($nome, $find_button));


        // instancia objeto DataGrid
        $this->datagrid = new TDatagridTables;

        // instancia as colunas da DataGrid
        $dgnome_servidor = new TDataGridColumn('nome_servidor', 'Servidor', 'left', 1200);
        $dgmatricula = new TDataGridColumn('matricula', 'Matricula', 'left', 50);
        $dgqtd_formacao = new TDataGridColumn('link_formacao', 'Forma&ccedil;&atilde;o', 'left', 40);
        $dgqtd_experiencia = new TDataGridColumn('link_experiencia', 'Experiência', 'left', 40);
        $dgqtd_capacitacao = new TDataGridColumn('link_capacitacao', 'Capacita&ccedil;&atilde;o', 'left', 40);
        $dgqtd_producao = new TDataGridColumn('link_producao', 'Produ&ccedil;&atilde;o', 'left', 40);

        // adiciona as colunas a DataGrid
        $this->datagrid->addColumn($dgnome_servidor);
        $this->datagrid->addColumn($dgmatricula);
        $this->datagrid->addColumn($dgqtd_formacao);
        $this->datagrid->addColumn($dgqtd_experiencia);
        $this->datagrid->addColumn($dgqtd_capacitacao);
        $this->datagrid->addColumn($dgqtd_producao);
		/*
        // instancia acoes ao DataGrid
        $action1 = new TDataGridAction(array('ServidorCurriculoDetalhePDF', 'onEdit'), '');
        $action1->setLabel('Ver Curriculo');
        $action1->setImage('ico_fichacatalografica.png');
        $action1->setField('servidor_id');
		
        // adiciona as acoes a DataGrid
         $this->datagrid->addAction($action1);
		*/
        // cria o modelo da DataGrid, montando sua estrutura
        $this->datagrid->createModel();

        // monta a paina atraves de uma tabela
        $panel = new TPanelForm(900, 500);
        $panel->put($this->form, 0, 0);
        $panel->put($this->datagrid, 150, 50);


        // adiciona a tabela a pagina
        parent::add($panel);
    }

    /*
     * metodo onReload()
     * Carrega a DataGrid com os objetos do banco de dados
     */
    function onReload() {
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instancia um repositorio da Classe
        $repository = new TRepository('vw_servidor_curriculo_validacaoRecord');

        // cria um criterio de selecao
        $criteria = new TCriteria;

        //filtra pelo campo selecionado pelo usuário
        $criteria->add( new TFilter( 'empresa_id', '=', $_SESSION['empresa_id'] ) );
        $criteria->setProperty( 'order', 'nome_servidor' );

        // carrega os objetos de acordo com o criterio
        $cadastros = $repository->load($criteria);

        $this->datagrid->clear();
        if ($cadastros) {
            // percorre os objetos retornados
            foreach ($cadastros as $cadastro) {
                // adiciona o objeto na DataGrid
                $this->datagrid->addItem($cadastro);
            }
        }

        // finaliza a transacao
        TTransaction::close();
        $this->loaded = true;
    }

    /*
     * metodo show()
     * Exibe a pagina
     */

    function show() {
        // carrega os dados no datagrid
        $this->onReload();
        //chama o metodo show da super classe
        parent::show();
    }

}

?>