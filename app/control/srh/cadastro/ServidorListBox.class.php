<?php

/*
 * classe ServidorList
 * Cadastro de Servidor: Contem a listagem e o formulario de busca
 */
//ini_set('display_errors', 1);
//ini_set('display_startup_erros', 1);
//error_reporting(E_ALL);

use Adianti\Database\TFilter1;
use Adianti\Widget\Datagrid\TDatagridTables;

class ServidorListBox extends TPage {

    private $form;     // formulario de cadastro
    private $datagrid; // listagem

    /*
     * metodo construtor
     * Cria a pagina, o formulario e a listagem
     */

    public function __construct() {
        parent::__construct();
        // instancia um formulario
        $this->form = new TForm('form_busca_Servidor');
        //$this->form->setAction('ServidorList');
        // instancia um Panel
        $panel = new TPanelForm(900, 100);

        // adiciona o panel ao formulario
        $this->form->add($panel);

        // cria um rótulo para o título
        $titulo = new TLabel('Cadastro de Servidores');
        $titulo->setFontFace('Arial');
        $titulo->setFontColor('red');
        $titulo->setFontSize(18);

        // adiciona o campo Titulo
        $panel->put($titulo, $panel->getColuna(), $panel->getLinha());

        // cria os campos do formulario
        $opcao = new TCombo('opcao');
        $estado = new TCombo('estado');

        $input = new TMultiSearch('input');
        $input->setSize(240,28);
        $input->addItems( $this->getPrograms() );
        $input->setMinLength(1);
        $input->setMaxSize(1);
        
        $button = new TButton('search');
        $button->style = 'margin-top:0px; height:30px;';
        $button->setImage('bs:search green');
        $button->setAction( new TAction(array($this, 'loadProgram')));
        
        // cria um vetor com as opcoes da combo
        $items = array();
        $items['matricula'] = 'Matr&iacute;cula';
        $items['cpf'] = 'CPF';
        $items['nome'] = 'Nome';
        $items['nome_cargo'] = 'Cargo';
        $items['nome_cargonovo'] = 'Cargo Novo';
        $items['cidade'] = 'Cidade';
        $items['id'] = 'C&oacute;digo';

        // adiciona as opcoes na combo
        $opcao->addItems($items);
        //coloca o valor padrao no combo
        $opcao->setValue('nome');
        $opcao->setSize(20);

        // cria um vetor com as opcoes da combo
        $items0 = array();
        $items0['TODOS'] = 'TODOS';
        $items0['EM ATIVIDADE'] = 'EM ATIVIDADE';
        $items0['A DISPOSICAO'] = 'A DISPOSICAO';
        $items0['APONSENTADO(A)'] = 'APONSENTADO(A)';
        $items0['LICENCIADO'] = 'LICENCIADO';
        $items0['RESCINDIDO'] = 'RESCINDIDO';

        // adiciona as opcoes na combo
        $estado->addItems($items0);
        //coloca o valor padrao no combo
        $estado->setValue('TODOS');
        $estado->setSize(20);

        $nome = new TEntry('nome');
        $nome->setSize(40);

        // cria um botao de acao (buscar)
        $find_button = new TButton('busca');
        // cria um botao de acao (cadastrar)
        $new_button = new TButton('novo');

        // define a acao do botao buscar
        $find_button->setAction(new TAction(array($this, 'onSearch')), 'Buscar');

        //define a acao do botao cadastrar
        $new_button->setAction(new TAction(array('ServidorForm', 'onEdit')), 'Novo');

        // adiciona o campo
        $panel->putCampo(null, 'Selecione o Campo:', 0, 0);
        $panel->put($opcao, $panel->getColuna(), $panel->getLinha());
        $panel->put(new TLabel('Informe o Valor da Busca:'), $panel->getColuna(), $panel->getLinha());
        $panel->put($nome, $panel->getColuna(), $panel->getLinha());
        $panel->put(new TLabel('Situa&ccedil;&atilde;o:'), $panel->getColuna(), $panel->getLinha());
//        $panel->put($estado, $panel->getColuna(), $panel->getLinha());
//        $panel->put($find_button, $panel->getColuna(), $panel->getLinha());
//        $panel->put($new_button, $panel->getColuna(), $panel->getLinha());
        $panel->put($input, $panel->getColuna(), $panel->getLinha());
        $panel->put($button, $panel->getColuna(), $panel->getLinha());

        // define quais sao os campos do formulario
        $this->form->setFields(array($nome,$opcao, $find_button, $new_button, 
            $estado,$input,$button ));

        // instancia objeto DataTables Resposivo
        $this->datagrid = new TDatagridTables;

        // instancia as colunas da DataGrid
        $dgcodigo = new TDataGridColumn('id', 'C&oacute;digo', 'right', 50);
        $dgnome = new TDataGridColumn('nome', 'Nome', 'left', 300);
        $dgcargo = new TDataGridColumn('nome_cargo', 'Cargo', 'left', 200);
        $dgcargonovo = new TDataGridColumn('nome_cargonovo', 'Cargo Novo', 'left', 200);
        $dgnivel = new TDataGridColumn('nome_nivel', 'N&iacute;vel', 'left', 200);
        $dgcpf = new TDataGridColumn('cpf', 'CPF', 'left', 100);
        $dgmatricula = new TDataGridColumn('matricula', 'Matr&iacute;cula', 'left', 100);
        $dgadmissao = new TDataGridColumn('dataadmissao', 'Admiss&atilde;o', 'left', 100);
        $dgsituacao = new TDataGridColumn('situacao', 'Situa&ccedil;&atilde;o', 'left', 100);

//        $dgadmissao->setTransformer('formatar_data');
        // adiciona as colunas a DataGrid
       // $this->datagrid->addColumn($dgcodigo);
        $this->datagrid->addColumn($dgnome);
        $this->datagrid->addColumn($dgcargo);
        $this->datagrid->addColumn($dgcargonovo);
        $this->datagrid->addColumn($dgnivel);
        $this->datagrid->addColumn($dgcpf);
        $this->datagrid->addColumn($dgmatricula);
        $this->datagrid->addColumn($dgadmissao);
        $this->datagrid->addColumn($dgsituacao);

        // instancia duas acoes da DataGrid
        $action1 = new TDataGridAction(array('ServidorForm', 'onEdit'));
        $action1->setLabel('Editar');
        $action1->setImage('ico_edit.png');
        $action1->setField('id');
        $action1->setFk('id');


        $action2 = new TDataGridAction(array($this, 'onDelete'));
        $action2->setLabel('Deletar');
        $action2->setImage('ico_delete.png');
        $action2->setField('id');

        //instancia acao para o cadastro da foto do servidor
        $action3 = new TDataGridAction(array('ServidorFormFoto', 'onEdit'));
        $action3->setLabel('Foto');
        $action3->setImage('ico_foto.png');
        $action3->setField('id');
        $action3->setFk('id');


        // adiciona as acoes a DataGrid
        $this->datagrid->addAction($action1);
        $this->datagrid->addAction($action2);
        $this->datagrid->addAction($action3);
        // cria o modelo da DataGrid, montando sua estrutura
        $this->datagrid->createModel();

        // monta a paina atraves de uma tabela
        $panel = new TPanelForm(800, 700);
        $panel->put($this->form, 0, 0);
        $panel->put($this->datagrid, 150, 130);


        // adiciona a tabela a pagina
        parent::add($panel);
    }

     /**
     * Returns an indexed array with all programs
     */
    public function getPrograms()
    {
        $teste = "teste";
        echo $teste;
        /*
        $xml = simplexml_load_file('menu.xml');
        foreach ($xml as $xmlElement)
        {
            $atts   = $xmlElement->attributes();
            $index  = (string) $atts['label'];
            
            if ($xmlElement->menu)
            {
                foreach ($xmlElement->menu->menuitem as $subXmlElement)
                {
                    $subindex = (string) $subXmlElement['label'];
                    $subatts   = $subXmlElement->attributes();
                    if ($subXmlElement->menu)
                    {
                        foreach ($subXmlElement->menu->menuitem as $option)
                        {
                            $optatts   = $option->attributes();
                            $label  = (string) $option['label'];
                            $action = (string) $option-> action;
                            $icon   = (string) $option-> icon;
                            $items[ $action ] = $label;
                        }
                    }
                }
            }
        }
        return $items;
        */
        return $teste;
    }
    
    /**
     * Load an specific program
     */
    public function loadProgram($param)
    {
        $data = $this->form->getData();
        $programs = array_keys($data->input);
        $program = $programs[0];
        TApplication::loadPage($program);
    }
    /*
     * metodo onReload()
     * Carrega a DataGrid com os objetos do banco de dados
     */

    function onReload() {
        /*
          // inicia transacao com o banco 'pg_ceres'
          TTransaction::open('pg_ceres');

          // instancia um repositorio para Carros
          $repository = new TRepository('ServidorRecord');

          // cria um criterio de selecao
          $criteria = new TCriteria;

          $criteria->setProperty('order', 'id');
          // carrega os objetos de acordo com o criterio
          $cadastros = $repository->load($criteria);

          $this->datagrid->clear();
          if ($cadastros) {
          // percorre os objetos retornados
          foreach ($cadastros as $cadastro) {
          // adiciona o objeto na DataGrid
          //converter datas do form para formato Brasileiro
          $cadastro->dataadmissao = TDate::date2br($cadastro->dataadmissao);
          $this->datagrid->addItem($cadastro);
          }
          }
          // finaliza a transacao
          TTransaction::close();
          $this->loaded = true;
         */
    }

    /*
     * metodo onSearch()
     * Carrega a DataGrid com os objetos da busca no banco de dados
     */

    function onSearch() {
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instancia um repositorio para Carros
        $repository = new TRepository('ServidorRecord');

        //obtem os dados do formulario de busca
        $campo = $this->form->getFieldData('opcao');
        $dados = $this->form->getFieldData('nome');
        $dados1 = $this->form->getFieldData('estado');

        //verifica se foi informado um valor para consulta
//        if ( ! $dados){
//           if ($_SESSION['buscaservidor']){
//               $dados = $_SESSION['buscaservidor'];
//               $field1 = $this->form->getField('nome');
//               $field1->setValue($_SESSION['buscaservidor']);
//           }
//        }else{
//            //$_SESSION['buscaservidor'] = $dados;
//           // $campo = ('nome');
//        }

        if (!$dados1) {

            //pega os dados da url
            $dados1 = filter_input(INPUT_GET, 'estado');
        }


        if ((!$dados) || (!$campo)) {
            //pega os dados da url
            $campo = filter_input(INPUT_GET, 'campo');
            $dados = filter_input(INPUT_GET, 'dados');
        }
        if (!$campo) {
            $campo = 'nome';
        }

        // cria um criterio de selecao
        $criteria = new TCriteria;
        //  $criteria1 = new TCriteria;

        if ($dados1 <> "TODOS") {
            $criteria->add(new TFilter('situacao', '=', "$dados1"));
        }

        //verifica se o usuario preencheu o formulario
        if ($dados) {
            if (is_numeric($dados)) {
                $criteria->add(new TFilter($campo, '=', $dados));
            } else {
                //filtra pelo campo selecionado pelo campo ignore case
                $criteria->add(new TFilter1('special_like(' . $campo . ",'" . $dados . "')"));
            }
        }
        $criteria->setProperty('order', $campo);
        // carrega os objetos de acordo com o criterio
        $cadastros = $repository->load($criteria);

        $this->datagrid->clear();
        if ($cadastros) {
            // percorre os objetos retornados
            foreach ($cadastros as $cadastro) {
                //converter datas do form para formato Brasileiro
                $cadastro->dataadmissao = TDate::date2br($cadastro->dataadmissao);
                // adiciona o objeto na DataGrid
                $this->datagrid->addItem($cadastro);
            }
        }

        // finaliza a transacao
        TTransaction::close();
        $this->loaded = true;
    }

    /*
     *
     *
     * metodo onReload()
     * Carrega a DataGrid com os objetos do banco de dados

      function onReload() {
      // inicia transacao com o banco 'pg_ceres'
      TTransaction::open('pg_ceres');

      // instancia um repositorio da Classe
      $repository = new TRepository('Servidor');

      //obtem os dados do formulario de busca
      $campo = $this->form->getFieldData('opcao');
      $dados = $this->form->getFieldData('nome');

      // cria um criterio de selecao, ordenado pelo id
      // $criteria = new TCriteria;
      //$criteria->setProperty('order', $campo);

      //verifica se o usuario preencheu o formulario
      if (($dados) && ($_GET['method'] != '')) {
      if (is_numeric($dados)) {
      $operador = '=';
      $valor = strtoupper($dados);
      }else {
      $operador = 'like';
      $valor = strtoupper("%{$dados}%");
      }
      //filtra pelo campo selecionado pelo usuario
      $criteria->add(new TFilter($campo, $operador, $valor));
      }

      $criteria->setProperty('order', $campo);


      // carrega os objetos de acordo com o criterio
      $cadastros = $repository->load($criteria);

      $this->datagrid->clear();
      if ($cadastros) {
      // percorre os objetos retornados
      foreach ($cadastros as $cadastro) {
      // adiciona o objeto na DataGrid
      $this->datagrid->addItem($cadastro);
      }
      }


      // finaliza a transacao
      TTransaction::close();
      $this->loaded = true;
      }

     */
    /*
     * metodo onDelete()
     * Executada quando o usuario clicar no botao excluir da datagrid
     * Pergunta ao usuario se deseja realmente excluir um registro
     */

    function onDelete($param) {
        // obtem o parametro $key
        $key = $param['key'];

        // define duas acoes
        $action1 = new TAction(array($this, 'Delete'));

        // define os parametros de cada acao
        $action1->setParameter('key', $key);

        // exibe um dialogo ao usuario
        new TQuestion('Deseja realmente excluir o registro ?', $action1);
    }

    /*
     * metodo Delete()
     * Exclui um registro
     */

    function Delete($param) {
        // obtem o parametro $key
        $key = $param['key'];
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instanicia objeto Record
        $cadastro = new ServidorRecord($key);

        try {
            // deleta objeto do banco de dados
            $cadastro->delete();

            // finaliza a transacao
            TTransaction::close();
        } catch (Exception $e) { // em caso de exceção
            // exibe a mensagem gerada pela exceção
            new TMessage('error', $e->getMessage());
            // desfaz todas alterações no banco de dados
            TTransaction::rollback();
        }

        // re-carrega a datagrid
        $this->onReload();
        // exibe mensagem de sucesso
        new TMessage('info', "Registro Exclu&iacute;do com sucesso");
    }

    /*
     * metodo show()
     * Exibe a pagina
     */

    function show() {
        // carrega os dados no datagrid
        $this->onReload();
        //chama o metodo show da super classe
        parent::show();
    }

}

?>