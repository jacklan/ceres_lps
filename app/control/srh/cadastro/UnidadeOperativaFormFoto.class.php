<?php

/*
 * classe ServidorFormFoto
 * Cadastro de Servidor: Contem o formularo da Foto
 */
include_once 'app.library/funcdate.php';

class UnidadeOperativaFormFoto extends TPage {

    private $form;     // formulario de cadastro

    /*
     * metodo construtor
     * Cria a pagina do formulario
     */

    public function __construct() {
        parent::__construct();

        // instancia um formulario
        $this->form = new TForm('form_unidadeOperativa_foto');
        //$this->form->setEnctype("multipart/form-data");

        $table = new TTable;
        $table->width = '100%';
        $table_buttons = new TTable;

        // add the table inside the form
        $this->form->add($table);

        // cria um rotulo para o titulo
        $titulo = new TLabel('<b>Inclus&atilde;o de Foto de Unidade Operativa</b>');
        $titulo->setFontFace('Arial');
        $titulo->setFontColor('red');
        $titulo->setFontSize(12);

        // cria os campos do formulario
        $id = new THidden('unidadeoperativa_id');
        $id->setValue(filter_input(INPUT_GET, 'fk'));
        $foto = new TFile('foto');
        $foto->setProperty("accept", "image/jpg");

        $foto2 = new TImage('app/img/unidade/unidade_' . filter_input(INPUT_GET, 'fk') . '.jpg', 'foto', 150, 150);
        //$fotoimg  = new TInputImage('fotoimg');

        $usuarioalteracao = new THidden('usuarioalteracao');
        $dataalteracao = new THidden('dataalteracao');

        //cria a colecao da tabela estrangeira do Cabecalho
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $cadastro = new UnidadeOperativaRecord(filter_input(INPUT_GET, 'fk'));
        // cria um criterio de selecao, ordenado pelo id do servidor
        //adiciona os objetos na tela com as informacoes do servidor
        if ($cadastro) {
            $nome = new TLabel($cadastro->nome);
        }
        // finaliza a transacao
        TTransaction::close();

        // define os tamanhos dos campos
        $foto->setSize(350);
        //$fotoimg->setHeight(100);
        //$fotoimg->setSize(80);
        // add the form fields
        ################ Pagina 01 - Dados Pessoais ################
        $table->addRowSet('', $id);
        $table->addRowSet('', $usuarioalteracao);
        $table->addRowSet('', $dataalteracao);

        // adiciona campos
//        $table->addRowSet('', $titulo);
        //  $table->addRowSet(new TLabel('Matricula:'), $matricula);
        $table->addRowSet("<b>Nome <font color=red>*</font></b>", $nome);
        $table->addRowSet(new TLabel('Foto'), $foto);

        //mostra a foto do servidor se o metodo eh onEdit
        if (filter_input(INPUT_GET, 'method') == 'onEdit') {
            $foto2 = new TImage('app/images/unidade/unidade_' . filter_input(INPUT_GET, 'fk') . '.jpg', 'foto', 150, 150);
            $foto2->style = "width: 150px; height: 200px;";
            $table->addRowSet('', $foto2);
        }

        //Define o auto-sugerir
        $nome->setProperty('placeholder', 'Informe o Nome');

        // create an action button (save)
        $save_button = new TButton('save');

        // create an action servidor (save)
        $action = new TAction(array($this, 'onSave'));
        $action->setParameter('fk', '' . filter_input(INPUT_GET, 'fk') . '');
        $save_button->setAction($action, 'Save');
        $save_button->class = 'btn btn-info';
        $save_button->setImage('ico_save.png');

        // create an action button (new)
        $new_button = new TButton('list');
        $new_button->setAction(new TAction(array("UnidadeOperativaList", 'onReload')), 'Voltar');
        $new_button->setImage('ico_datagrid.gif');

        // add a row for the form action
        $row = $table_buttons->addRow();
        $row->addCell($save_button);
        $row->addCell($new_button);

        // add a row for the form action
        $row = $table->addRow();
        $row->class = 'tformaction';
        $cell = $row->addCell($table_buttons);
        $cell->colspan = 2;
        // define wich are the form fields
        $this->form->setFields(array($id, $foto, $save_button, $new_button));

        // wrap the page content using vertical box
        $vbox = new TVBox;
        $vbox->add($titulo);
        $vbox->add($this->form);

        parent::add($vbox);
    }

    /*
     * metodo onSave()
     * Executada quando o usuario clicar no botao salvar do formulario
     */

    function onSave() {

        // pegar dados do form
        $object = $this->form->getData();

        $source_file = 'tmp/' . $object->foto;
        $target_file = 'app/images/unidade/unidade_' . $object->unidadeoperativa_id . ".jpg";

        $extensao = pathinfo($object->foto, PATHINFO_EXTENSION);

        //if the user uploaded a source file
//        if (file_exists($source_file) && $finfo->file($source_file) == 'image/png') {
        if (file_exists($source_file) && ($extensao == 'jpg' || $extensao == 'png' || $extensao == 'jpeg')) {
            try {
                // move to the target directory
                // update the foto
                rename($source_file, $target_file);
                //   copy($image, $target_file);
                //$object->foto = 'images/' . $object->foto;
                $param = array();
                $param['fk'] = $object->unidadeoperativa_id;

                new TMessage("info", "Registro salvo com sucesso!");
                TApplication::gotoPage('UnidadeOperativaFormFoto', 'onEdit', $param); // reload
            } catch (Exception $e) { // in case of exception
                new TMessage('error', '<b>Error</b> ' . $e->getMessage());
                TTransaction::rollback();
            }
        } else {
            new TMessage("error", "Erro ao salvar a foto! Apenas aquivo do tipo JPG");
        }
        /*
          //   $arqtmp = $_FILES["foto"]["tmp_name"];
          $id = $_POST['unidadeoperativa_id'];
          $msg = "";
          /* Defina aqui o tamanho m?ximo do arquivo em bytes: */
        /*
          if($_FILES['foto']['size'] > 1024000) {
          $msg = "O Arquvio deve no maximo 1Mb";
          }else{
          /* Defina aqui o diret?rio destino do upload */
        /*
          if (!empty($_FILES['foto']['tmp_name']) and is_file($_FILES['foto']['tmp_name'])) {
          $caminho='app.img/unidade/unidade_'.$id.'.jpg';

          /* Defina aqui o tipo de arquivo suportado */
        /*
          if (eregi(".jpg$", $_FILES['foto']['name'])){
          copy($_FILES['foto']['tmp_name'],$caminho);
          $msg = 'OK';
          }else{
          $msg = 'Apenas aquivo do tipo JPG';
          }
          }else{
          $msg = 'Caminho ou nome de arquivo Invalido!';
          }
          }
         * 
         */


        //  $caminho = $_FILES["foto"]["tmp_name"];
        //$tipo = $_FILES["foto"]["type"];
        //$destino = 'app.img/servidor/servidor_'.$id.'.jpg';
        //move_uploaded_file($caminho, $destino);
        /*
          array(5) {
          ["name"]=&gt;
          string(16) "hidel_emater.jpg"
          ["type"]=&gt;
          string(10) "image/jpeg"
          ["tmp_name"]=&gt;
          string(25) "C:\php\upload\phpA2BA.tmp"
          ["error"]=&gt;
          int(0)
          ["size"]=&gt;
          int(141151)
          }
         *
         */

        /*
          $caminho = $_FILES["foto"]["tmp_name"];
          //var_dump($caminho);
          $conn = pg_connect("host='localhost' dbname='porteiranet' user='porteira' password='ematerrn' sslmode='disabled' ");
          //$conteudo=addslashes(fread(fopen($caminho, "r"), filesize($caminho)));
          $conteudo=fread(fopen($caminho, "r"), filesize($caminho));
          //$sql = "UPDATE servidor set foto = pg_escape_bytea('" . addslashes($conteudo) ."' where id = ".$id." ;";
          $sql = "UPDATE servidor set foto = pg_escape_bytea('" .$conteudo."') where id = ".$id." ;";
          var_dump($sql);
          $exec=pg_query ( $conn, $sql);
          pg_close($conn);
         */
        /*

          // --------- OPEN CONN ---
          $conn = pg_connect("host='localhost' dbname='porteiranet' user='porteira' password='ematerrn'");


          // --------- OPEN FILE ---
          $fp = fopen($arqtmp, "r");
          $buffer = fread($fp, filesize($arqtmp));
          fclose($fp);

          // --------- CREATE - UPDATE OID ---

          pg_exec($conn, "begin");

          $oid = pg_locreate($conn);
          $caminho = $_FILES["foto"]["tmp_name"];

          $conteudo=addslashes(fread(fopen($caminho, "r"), filesize($caminho)));
          $rs = pg_exec($conn,"UPDATE servidor foto = lo_import('" . addslashes($arq) ." where id = ".$id." ;");

          $msg = "UPDATE servidor foto = $handle where id = $id ;";
          $handle = pg_loopen ($conn, $oid, "w");
          pg_lowrite ($handle, $caminho);
          pg_loclose ($handle);
          $rs = pg_exec($conn,"UPDATE servidor foto = lo_import('" . addslashes($arq) ." where id = ".$id." ;");

          pg_exec($conn, "commit");

          pg_close();
         */
        /*
        //chama o formulario com o grid
        // define duas acoes
        $action1 = new TAction(array('UnidadeOperativaList', ''));
        // exibe um dialogo ao usuario
        //$msg = 'Dados armazenados com sucesso';
        if ($msg == 'OK') {
            new TConfirmation("Dados gravados com sucesso", $action1);
        } else {
            new TConfirmation($msg, $action1);
        }
         * 
         */
    }

    /*
     * metodo onEdit()
     * Edita os dados de um registro
     */

    function onEdit($param) {
        // obtem o parametro $key
        $key = $param['fk'];
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $cadastro = new UnidadeOperativaRecord($key);
        // lanca os dados no formulario
        $this->form->setData($cadastro);

//        $fotoimg  = new TImage('app.img/servidor/servidor_'.$key.'.jpg','foto',50,50);
//        $this->form->add($fotoimg);
        // finaliza a transacao
        TTransaction::close();
    }

}

?>