<?php

//ini_set('display_errors', 1);
//ini_set('display_startup_erros', 1);
//error_reporting(E_ALL);

/*
 * classe ValidacaoDocumentosForm
 * Cadastro de Averbacao: Contem a listagem e o formulario de busca
 * Autor: Jackson Meires
 * Data:30/09/2016
 */

class ValidacaoDocumentosForm extends TPage {

    private $form;     // formulario de cadastro

    /*
     * metodo construtor
     * Cria a pagina, o formulario e a listagem
     */

    public function __construct() {
        parent::__construct();

        // instancia um formulario
        $this->form = new TQuickForm;
        //  $this->form->class = 'form_Averbacao';
        $this->form->setFormTitle('<font color="red" size="3" face="Arial"><b>Valida&ccedil;&atilde;o de Documentos</b></font>');

        // cria um rotulo para o titulo
        $lb_obrigatorio = new TLabel('<div style="position:floatval; width: 200px;"> <b>* Campo obrigatorio</b></div>');
        $lb_obrigatorio->setFontFace('Arial');
        $lb_obrigatorio->setFontColor('red');
        $lb_obrigatorio->setFontSize(10);

        // cria os campos do formulario
        $srh_certificado_servidor_id = new THidden('srh_certificado_servidor_id');
        $dataemissao = new TDate('dataemissao');
        $matricula = new TEntry('matricula');
        $codigoverificacao = new TEntry('codigoverificacao');
        $opcaoImageCertificado = new TCheckButton('opcao_imagem_certificado');

        $usuarioalteracao = new THidden('usuarioalteracao');
        $dataalteracao = new THidden('dataalteracao');

        // validacao dos campos
        $matricula->addValidation('Matricula', new TRequiredValidator); // required field
        $dataemissao->addValidation('Data Emiss&atilde;o', new TRequiredValidator); // required field
        $codigoverificacao->addValidation('C&oacute;digo de Verifica&ccedil;&atilde;o', new TRequiredValidator); // required field

        $opcaoImageCertificado->setValue(array(''));

        // adicionando botoes ao form
        $this->form->addQuickField(null, $srh_certificado_servidor_id, 0);
        $this->form->addQuickField(null, $dataalteracao, 0);
        $this->form->addQuickField(null, $usuarioalteracao, 0);
        //   $this->form->addQuickField('Matr&iacutecula <font color=red><b>*</b></font>', $matricula, 40);
        //   $this->form->addQuickField('Data Emiss&atilde;o <font color=red><b>*</b></font>', $dataemissao, 50);
        $this->form->addQuickField('C&oacute;digo de Verifica&ccedil;&atilde;o <font color=red><b>*</b></font>', $codigoverificacao, 70);
        $this->form->addQuickField('Imprimir Com Fundo Branco', $opcaoImageCertificado, 40);
        $this->form->addQuickField(null, $lb_obrigatorio, 0);

        $action1 = new TAction(array($this, 'onGenerate'));
        $fk = !empty(filter_input(INPUT_GET, 'fk')) ? filter_input(INPUT_GET, 'fk') : $_SESSION['servidor_id'];
        $action1->setParameter('fk', $fk);
        $action1->setParameter('back', filter_input(INPUT_GET, 'back'));

        $this->form->addQuickAction('Gerar', $action1, 'ico_save.png')->class = 'btn btn-info btnleft';

        if (filter_input(INPUT_GET, 'back') == 'MeuPerfilList') {
            // prepara o botao para chamar o form anterior
            $action2 = new TAction(array("MeuPerfilList", 'onReload'));
            $action2->setParameter('fk', $fk);
        } else {
            $action2 = new TAction(array("CertificadoList", 'onReload'));
            $action2->setParameter('fk', $fk);
        }

        if (filter_input(INPUT_GET, 'method')) {
            $this->form->addQuickAction('Voltar', $action2, 'ico_datagrid.gif');
        }
        // monta a paina atraves de uma tabela
        // $panel = new TPanelForm(700, 200);
        // $panel->put($this->form, 0, 0);
        // wrap the page content using vertical box
        $vbox = new TVBox;
        $vbox->add($this->form);

        // adiciona a tabela a pagina
        parent::add($vbox);
    }

    /*
     * metodo onGenerate()
     * Executada quando o usuario clicar no botao salvar do formulario
     */

    function onGenerate() {
        try {
            // obtem os dados no formulario em um objeto Record da Classe
            // $data = $this->form->getData('CertificadoServidorRecord'); // optional parameter: active record class
            $data = $this->form->getData(); // optional parameter: active record class
            // inicia transacao com o banco 'pg_ceres'
            TTransaction::open('pg_ceres');

            // instancia um repositorio
            $repository = new TRepository('SolicitacaoCertificadoRecord');

            //criteria
            $criteria = new TCriteria();
            //filtro
            //  $criteria->add(new TFilter('matricula', '=', $data->matricula));
            //  $criteria->add(new TFilter('dataemissao', '=', $data->dataemissao));
            $criteria->add(new TFilter('codigoverificacao', '=', $data->codigoverificacao));

            // carrega os objetos de acordo com o criterio
            $rows = $repository->load($criteria);

            if ($rows) {
                // percorre os objetos retornados
                foreach ($rows as $row) {
                    TSession::setValue('srh_solicitacao_certificado_id', $row->id);
                }
            }
            $msg = '';
            /*
              if (empty($data->matricula)) {
              $msg .= 'A data de início deve ser informada.<br/>';
              }

              if (empty($data->dataemissao)) {
              $msg .= 'O orgão deve ser informado.<br/>';
              }
             */

            if (empty($data->codigoverificacao)) {
                $msg .= 'C&oacute;digo de Verifica&ccedil;&atilde;o deve ser informado<br/>';
            }
            if ($msg === '') {
                // show the message
                new TMessage('info', 'Certificado Validado com sucesso!');
                new CertificadoCursoPDF();
            } else {
                // show the message
                new TMessage('info', $msg);
            }
            TTransaction::close();           // close the transaction

            $this->form->setData($data);
        } catch (Exception $e) {
            // exibe a mensagem gerada pela exce??o
            new TMessage('error', $e->getMessage());
            // desfaz todas altera??es no banco de dados
            TTransaction::rollback();
            $this->form->setData($this->form->getData());
        }
    }

    /*
     * metodo onEdit()
     * Edita os dados de um registro
     */

    function onEdit($param) {
        try {
            if (isset($param['key'])) {

                // get the parameter $key
                $key = $param['key'];

                TTransaction::open('pg_ceres');   // open a transaction with database 'samples'

                $object = new SolicitacaoCertificadoRecord($key);        // instantiates object City

                $this->form->setData($object);   // fill the form with the active record data

                TTransaction::close();           // close the transaction
            } else {
                $this->form->clear();
            }
        } catch (Exception $e) { // in case of exception
            // shows the exception error message
            new TMessage('error', '<b>Error</b> ' . $e->getMessage());
            // undo all pending operations
            TTransaction::rollback();
        }
    }

    function onLoad() {
        
    }

}
