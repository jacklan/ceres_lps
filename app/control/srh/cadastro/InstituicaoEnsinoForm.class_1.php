<?php

/*
 * classe InstituicaoEnsinoForm
 * Cadastro de InstituicaoEnsino: Contem o formularo
 */

class InstituicaoEnsinoForm extends TPage {

    private $form;     // formulario de cadastro

    /*
     * metodo construtor
     * Cria a pagina do formulario
     */

    public function __construct() {
        parent::__construct();

        // instancia um formulario
        $this->form = new TQuickForm;
        $this->form->class = 'form_InstituicaoEnsino';
        $this->form->setFormTitle('<font color="red" size="3" face="Arial"><b>Formul&aacute;rio de Institui&ccedil;&atilde;o de Ensino</b></font>');

        // cria um rotulo para o titulo
        $titulo = new TLabel('<div style="position:floatval; width: 200px;"> <b>* Campo obrigatorio</b></div>');
        $titulo->setFontFace('Arial');
        $titulo->setFontColor('red');
        $titulo->setFontSize(10);

        // cria os campos do formulario
        $codigo = new THidden('id');
        $codigo->setEditable(false);
        $nome = new TEntry('nome');
        $sigla = new TEntry('sigla');
        $uf = new TCombo('uf');
        $esfera = new TCombo('esferaadministrativa');

        $usuarioalteracao = new THidden('usuarioalteracao');
        $dataalteracao = new THidden('dataalteracao');

        // cria um vetor com as opcoes da combo estado
        $items = array();
        $items['AC'] = 'AC';
        $items['AL'] = 'AL';
        $items['AM'] = 'AM';
        $items['AP'] = 'AP';
        $items['BA'] = 'BA';
        $items['CE'] = 'CE';
        $items['DF'] = 'DF';
        $items['ES'] = 'ES';
        $items['GO'] = 'GO';
        $items['MA'] = 'MA';
        $items['MG'] = 'MG';
        $items['MS'] = 'MS';
        $items['MT'] = 'MT';
        $items['PA'] = 'PA';
        $items['PB'] = 'PB';
        $items['PE'] = 'PE';
        $items['PI'] = 'PI';
        $items['PR'] = 'PR';
        $items['RJ'] = 'RJ';
        $items['RN'] = 'RN';
        $items['RO'] = 'RO';
        $items['RS'] = 'RS';
        $items['SC'] = 'SC';
        $items['SE'] = 'SE';
        $items['SP'] = 'SP';
        $items['TO'] = 'TO';

        // adiciona as opcoes na combo
        $uf->addItems($items);

        $items1 = array();
        $items1['FEDERAL'] = 'FEDERAL';
        $items1['ESTADUAL'] = 'ESTADUAL';
        $items1['MUNICIPAL'] = 'MUNICIPAL';
        $items1['PRIVADA'] = 'PRIVADA';

        $esfera->addItems($items1);

        //Campos obrigatórios
        $nome->setProperty('required', 'required');
        $sigla->setProperty('required', 'required');

        // define os campos
        $this->form->addQuickField(null, $codigo, 10);
        $this->form->addQuickField(null, $usuarioalteracao, 10);
        $this->form->addQuickField(null, $dataalteracao, 10);
        $this->form->addQuickField("Nome <font color=red><b>*</b></font>", $nome, 50);
        $this->form->addQuickField("Sigla", $sigla, 50);
        $this->form->addQuickField("UF <font color=red><b>*</b></font>", $uf, 50);
        $this->form->addQuickField("Esfera Administrativa <font color=red><b>*</b></font>", $esfera, 50);
        $this->form->addQuickField(null, $titulo, 50);

        // cria um botao de acao
        $this->form->addQuickAction('Salvar', new TAction(array($this, 'onSave')), 'ico_save.png');
        $this->form->addQuickAction('Voltar', new TAction(array('InstituicaoEnsinoList', 'onReload')), 'ico_datagrid.gif');

        // adiciona a tabela a pagina
        parent::add($this->form);
    }

    /*
     * metodo onSave()
     * Executada quando o usuario clicar no botao salvar do formulario
     */

    function onSave() {
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // obtem os dados no formulario em um objeto CarroRecord
        $cadastro = $this->form->getData('InstituicaoEnsinoRecord');

        //lanca o default
        $cadastro->usuarioalteracao = $_SESSION['usuario'];
        $cadastro->dataalteracao = date("d/m/Y H:i:s");

        //antes de armazenar verifica se algum campo eh requerido e nao foi informado
        $dados = $cadastro->toArray();

        $msg = '';
        $icone = 'info';

        if (empty($dados['nome'])) {
            $msg .= 'O Nome deve ser informado.<br/>';
        }

        if (empty($dados['uf'])) {
            $msg .= 'A UF deve ser informada.<br/>';
        }

        if (empty($dados['esferaadministrativa'])) {
            $msg .= 'A esfera administrativa deve ser informada.<br/>';
        }

        try {

            if ($msg == '') {
                // armazena o objeto
                $cadastro->store();
                $msg = 'Dados armazenados com sucesso';

                // finaliza a transacao
                TTransaction::close();
            } else {
                $icone = 'error';
            }

            if ($icone == 'error') {
                // exibe mensagem de erro
                new TMessage($icone, $msg);

                $this->form->setData($cadastro);   // fill the form with the active record data
            } else {

                // exibe um dialogo ao usuario
                new TMessage("info", "Registro salvo com sucesso!");
                TApplication::gotoPage('InstituicaoEnsinoList', 'onReload'); // reload
            }
        } catch (Exception $e) { // em caso de exc
            // exibe a mensagem gerada pela exce??o
            new TMessage('error', $e->getMessage());
            // desfaz todas altera??es no banco de dados
            TTransaction::rollback();

            $this->form->setData($cadastro);   // fill the form with the active record data
        }
    }

    /*
     * metodo onEdit()
     * Edita os dados de um registro
     */

    function onEdit($param) {

        try {
            if (isset($param['key'])) {

                // get the parameter $key
                $key = $param['key'];

                TTransaction::open('pg_ceres');   // open a transaction with database 'samples'

                $object = new InstituicaoEnsinoRecord($key);        // instantiates object City

                $this->form->setData($object);   // fill the form with the active record data

                TTransaction::close();           // close the transaction
            } else {
                $this->form->clear();
            }
        } catch (Exception $e) { // in case of exception
            // shows the exception error message
            new TMessage('error', '<b>Error</b> ' . $e->getMessage());
            // undo all pending operations
            TTransaction::rollback();
        }
    }

}

?>