<?php

/*
 * classe ServidorFormFoto
 * Cadastro de Servidor: Contem o formularo da Foto
 */
//ini_set('display_errors', 1);
//ini_set('display_startup_erros', 1);
//error_reporting(E_ALL);

//use Image\WideImage;
//use Image\WideImage\Exception\WideImage_Exception;

class ServidorFormFoto extends TPage {

    private $form;     // formulario de cadastro

    /*
     * metodo construtor
     * Cria a pagina do formulario
     */

    public function __construct() {
        parent::__construct();

        // instancia um formulario
        // creates the form
        $this->form = new TForm('form_Servidor');
        //$this->form = new TQuickForm;
        //   $this->form->setEnctype("multipart/form-data");
        $this->form->class = 'form_Servidor_foto';
        //$this->form->setEnctype("multipart/form-data");

        $table = new TTable;
        $table->width = '100%';
        $table_buttons = new TTable;

        // add the table inside the form
        $this->form->add($table);

        // creates a label with the title
        $titulo = new TLabel('Formulario Servidor');
        $titulo->setFontSize(18);
        $titulo->setFontFace('Arial');
        $titulo->setFontColor('red');

        // cria os campos do formulario
        $id = new THidden('servidor_id');
        $id->setValue(filter_input(INPUT_GET, 'fk'));
        $foto = new TFile('foto');
        $foto->setProperty("accept", "image/jpg");

        $usuarioalteracao = new THidden('usuarioalteracao');
        $dataalteracao = new THidden('dataalteracao');

        //cria a colecao da tabela estrangeira do Cabecalho
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $cadastro = new ServidorRecord(filter_input(INPUT_GET, 'fk'));
        // cria um criterio de selecao, ordenado pelo id do servidor
        //adiciona os objetos na tela com as informacoes do servidor
        if ($cadastro) {
            $nome = new TLabel($cadastro->nome);
            $matricula = new TLabel($cadastro->matricula);
        }
        // finaliza a transacao
        TTransaction::close();

        // define os tamanhos dos campos
        $foto->setSize(400);

        // add the form fields
        ################ Pagina 01 - Dados Pessoais ################
        $table->addRowSet('', $id);
        $table->addRowSet('', $usuarioalteracao);
        $table->addRowSet('', $dataalteracao);

        // adiciona campos
        $table->addRowSet('', $titulo);
        $table->addRowSet(new TLabel('Matricula:'), $matricula);
        $table->addRowSet("<b>Nome <font color=red>*</font></b>", $nome);
        $table->addRowSet(new TLabel('Foto'), $foto);

        //mostra a foto do servidor se o metodo eh onEdit
        if (filter_input(INPUT_GET, 'method') == 'onEdit') {
            $foto2 = new TImage('app/images/servidor/servidor_' . filter_input(INPUT_GET, 'fk') . '.jpg', 'foto', 150, 150);
            $foto2->style="width: 150px; height: 200px;";
            $table->addRowSet('', $foto2);
        }

        //Define o auto-sugerir
        $nome->setProperty('placeholder', 'Informe o Nome');

        // create an action button (save)
        $save_button = new TButton('save');

        // create an action servidor (save)
        $action = new TAction(array($this, 'onSave'));
        $action->setParameter('fk', '' . filter_input(INPUT_GET, 'fk') . '');
        $save_button->setAction($action, 'Save');

        $save_button->setImage('ico_save.png');

        // create an action button (new)
        $new_button = new TButton('list');
        $new_button->setAction(new TAction(array("ServidorList", 'onReload')), 'Voltar');
        $new_button->setImage('ico_datagrid.png');

        // add a row for the form action
        $row = $table_buttons->addRow();
        $row->addCell($save_button);
        $row->addCell($new_button);

        // add a row for the form action
        $row = $table->addRow();
        $row->class = 'tformaction';
        $cell = $row->addCell($table_buttons);
        $cell->colspan = 2;
        // define wich are the form fields
        $this->form->setFields(array($id, $foto, $save_button, $new_button));

        // wrap the page content using vertical box
        $vbox = new TVBox;
        $vbox->add($this->form);

        parent::add($vbox);
    }

    /*
     * metodo onSave()
     * Executada quando o usuario clicar no botao salvar do formulario
     */

    function onSave() {

        // pegar dados do form
        $object = $this->form->getData();

        $source_file = 'tmp/' . $object->foto;
        $target_file = 'app/images/servidor/servidor_' . $object->servidor_id . ".jpg";

        $extensao = strtolower(pathinfo(strtolower($object->foto), PATHINFO_EXTENSION));

        if (file_exists($source_file) && ($extensao == 'jpg' || $extensao == 'png' || $extensao == 'jpeg')) {
            try { 
                // move to the target directory
                // update the foto
                rename($source_file, $target_file);
             //   copy($image, $target_file);
                //$object->foto = 'images/' . $object->foto;
                $param = array();
                $param['fk'] = $object->servidor_id;
                TTransaction::open('pg_ceres');
                $obj = new ServidorRecord( $object->servidor_id );
                $obj->temfoto = '1';
                $obj->store();
                TTransaction::close(); 

                new TMessage("info", "Registro salvo com sucesso!");
                TApplication::gotoPage('ServidorFormFoto', 'onEdit', $param); // reload
            } catch (Exception $e) { // in case of exception
                new TMessage('error', '<b>Error</b> ' . $e->getMessage());
                TTransaction::rollback();
            }
        } else {
            new TMessage("error", "Erro ao salvar a foto! Apenas aquivo do tipo JPG");
        }

    }

    /*
     * metodo onEdit()
     * Edita os dados de um registro
     */

    function onEdit($param) {
        try {
            if (isset($param['fk'])) {

                // get the parameter $key
                $key = $param['fk'];

                TTransaction::open('pg_ceres');   // open a transaction with database 'samples'

                $object = new ServidorRecord($key);        // instantiates object Servidor

                $this->form->setData($object);   // fill the form with the active record data

                TTransaction::close();           // close the transaction
            } else {
                $this->form->clear();
            }
        } catch (Exception $e) { // in case of exception
            // shows the exception error message
            new TMessage('error', '<b>Error</b> ' . $e->getMessage());
            // undo all pending operations
            TTransaction::rollback();
        }
    }

}