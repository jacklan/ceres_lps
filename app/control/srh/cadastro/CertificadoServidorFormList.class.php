<?php
ini_set('display_errors', 1);
ini_set('display_startup_erros', 1);
error_reporting(E_ALL);
/*
 * classe CertificadoCertificadoServidorFormListList
 * Cadastro de Certificado: Contem a listagem e o formulario de busca
 * Autor: Jackson Meires
 * Data:22/09/2016
 */

use Adianti\Widget\Datagrid\TDatagridTables;

class CertificadoServidorFormList extends TPage {

    private $form;     // formulario de cadastro
    private $datagrid; // listagem

    /*
     * metodo construtor
     * Cria a pagina, o formulario e a listagem
     */

    public function __construct() {
        parent::__construct();

        // instancia um formulario
        $this->form = new TQuickForm('form_certificado_participante');
        // $this->form->class = 'form_acidente';
        $this->form->setFormTitle('<font color="red" size="3" face="Arial"><b>Vincular Certificado ao Servidor</b></font>');

        // cria um rotulo para o titulo
        $lb_campo_obrigatorio = new TLabel('<div style="position:floatval; width: 200px;"> <b>* Campo obrigatorio</b></div>');
        $lb_campo_obrigatorio->setFontFace('Arial');
        $lb_campo_obrigatorio->setFontColor('red');
        $lb_campo_obrigatorio->setFontSize(10);

        // cria os campos do formulario
        $codigo = new THidden('id');
        $srh_certificado_id = new THidden('srh_certificado_id');
        $srh_certificado_id->setValue(filter_input(INPUT_GET, 'fk'));
        $dataemissao = new TDate('dataemissao');

        /* ------------------ MultiSearch ------------------ */
        // cria um criterio de selecao
        $criteria = new TCriteria;
        $criteria->add(new TFilter('situacao', '=', 'EM ATIVIDADE'), TExpression::OR_OPERATOR);
        $criteria->add(new TFilter('situacao', '=', 'A DISPOSICAO'), TExpression::OR_OPERATOR);
        $criteria->add(new TFilter('empresa_id', '=', $_SESSION['empresa_id']));

        $criteria3 = new TCriteria;
        $criteria3->add($criteria);
        // $criteria3->setProperty('order', 'nome');
        //  var_dump($criteria3->dump());
        //$servidor_id = new TDBMultiSearch('servidor_id', 'pg_ceres', 'ServidorRecord', 'id', 'nome');
        $servidor_id = new TDBMultiSearch('servidor_id', 'pg_ceres', 'vw_servidor_multisearchRecord', 'servidor_id', 'nome_matricula_servidor', 'nome_matricula_servidor', $criteria3);
        $servidor_id->style = "text-transform: uppercase;";
        $servidor_id->setProperty('placeholder', 'Nome ou Matricula');

        /* ------------------ MultiSearch Servidor ------------------ */


        /* ------------------ MultiSearch Produtor ------------------ */
        $criteria_produtor = new TCriteria;
        $criteria_produtor->add(new TFilter('municipio_id', '=', $_SESSION['municipio_id']));
        $criteria_produtor->setProperty('order', 'nome');
        #### produtor_id TDBMultiSearch ####
        $produtor_id = new TDBMultiSearch('produtor_id', 'pg_ceres', 'vw_produtor_multisearchRecord', 'produtor_id', 'nome_cpf_produtor');
        $produtor_id->setProperty('placeholder', 'Nome ou CPF');
        /* ------------------ MultiSearch Produtor ------------------ */
        #############
        /*
          $srh_convidado_id = new TSeekButton('srh_convidado_id');
          $obj = new ConvidadoFormSeek;
          $action = new TAction(array($obj, 'onReload'));
          $action->setParameter('fk', filter_input(INPUT_GET, 'fk'));
          $srh_convidado_id->setAction($action);

          $nome_convidado = new TEntry('nome_convidado');
          $nome_convidado->setEditable(false);
         */

        #### produtor_id TDBMultiSearch ####
        $srh_convidado_id = new TDBMultiSearch('srh_convidado_id', 'pg_ceres', 'ConvidadoRecord', 'id', 'nome');
        $srh_convidado_id->setProperty('placeholder', 'Nome');
        /* ------------------ MultiSearch Produtor ------------------ */
        ##############

        $usuarioalteracao = new THidden('usuarioalteracao');
        $dataalteracao = new THidden('dataalteracao');

        //cria a colecao da tabela estrangeira do Cabecalho
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $cadastro = new CertificadoRecord(filter_input(INPUT_GET, 'fk'));
        // cria um criterio de selecao, ordenado pelo id do servidor
        //adiciona os objetos na tela com as informacoes do servidor
        if ($cadastro) {
            $curso = new TLabel($cadastro->curso);
        }
        // finaliza a transacao
        TTransaction::close();

        // validacao dos campos
        $dataemissao->addValidation('Data Emiss&atilde;o', new TRequiredValidator); // required field

        $srh_convidado_id->style = "margin-bottom:-10px;";
        //  $nome_convidado->style = "margin-bottom:-10px;";
        // define os campos
        $this->form->addQuickField(null, $codigo, 10);
        $this->form->addQuickField(null, $srh_certificado_id, 10);
        $this->form->addQuickField(null, $usuarioalteracao, 10);
        $this->form->addQuickField(null, $dataalteracao, 10);
        $this->form->addQuickField('Nome Curso', $curso, 300);
        $this->form->addQuickField('Data Emissão <font color=red><b>*</b></font>', $dataemissao, 20);
        $this->form->addQuickField('Servidores ', $servidor_id, 280);
        $this->form->addQuickField('Produtor ', $produtor_id, 280);

        // create an action button (save)
        $new_btn_convidado = new TButton('novo');
        $new_btn_convidado->setAction(new TAction(array('ConvidadoWindowForm', 'onLoad')), 'Novo');
        $new_btn_convidado->setImage('ico_save.png');

        $new_btn_convidado->style = "margin-right:50px;";

        $this->form->addQuickFields('Convidado', array($srh_convidado_id, $new_btn_convidado));
//        $this->form->addQuickField('Convidado ', $srh_convidado_id, 300);
        $this->form->addQuickField("<font color=red><b>OBS: </b></font>", new TLabel("<font color=red style='font-size: 16px;'><b>Pesquisar nome produtor em caixa alta, \"CAPS LOCK\" Habilitado</b></font>"), 600);

        $servidor_id->setSize(280, 70);
        $produtor_id->setSize(280, 70);
        $srh_convidado_id->setSize(280, 70);

        $this->form->addQuickField(null, $lb_campo_obrigatorio, 40);

        // cria um botao de acao
        $action1 = new TAction(array($this, 'onSave'));
        $action1->setParameter('fk', filter_input(INPUT_GET, 'fk'));

        $this->form->addQuickAction('Salvar', $action1, 'ico_save.png')->class = 'btn btn-info btnleft';

        $this->form->addQuickAction('Voltar', new TAction(array("CertificadoList", 'onReload')), 'ico_datagrid.gif');

        // instancia objeto DataGrid
        $this->datagrid = new TDatagridTables;

        // instancia as colunas da DataGrid
        $dgnome_servidor = new TDataGridColumn('nome_participante', 'Participante', 'left', 800);
        $dgcpf_participante = new TDataGridColumn('cpf_participante', 'CPF', 'left', 100);
        $dgdataemissao = new TDataGridColumn('dataemissao', 'Data Emissão', 'left', 100);
        $dgtipo_participante = new TDataGridColumn('tipo', 'TIPO', 'left', 100);

        // adiciona as colunas a DataGrid
        $this->datagrid->addColumn($dgnome_servidor);
        $this->datagrid->addColumn($dgcpf_participante);
        $this->datagrid->addColumn($dgdataemissao);
        $this->datagrid->addColumn($dgtipo_participante);

        $action2 = new TDataGridAction(array($this, 'onDelete'));
        $action2->setLabel('Deletar');
        $action2->setImage('ico_delete.png');
        $action2->setField('id');
        $action2->setParameter('fk', filter_input(INPUT_GET, 'fk'));

        // adiciona as acoes a DataGrid
        $this->datagrid->addAction($action2);

        // cria o modelo da DataGrid, montando sua estrutura
        $this->datagrid->createModel();

        // monta a paina atraves de uma tabela
        $panel = new TPanelForm(700, 500);
        $panel->put($this->form, 0, 0);
        $panel->put($this->datagrid, 150, 330);

        // adiciona a tabela a pagina
        parent::add($panel);
    }

    /*
     * metodo onSave()
     * Executada quando o usuario clicar no botao salvar do formulario
     */

    function onSave() {
        try {

            // inicia transacao com o banco 'pg_ceres'
            TTransaction::open('pg_ceres');
            // obtem os dados no formulario em um objeto Record da Classe
            $object = $this->form->getData('CertificadoServidorRecord');

            // form validation
            $this->form->validate();
            $object->srh_certificado_id = filter_input(INPUT_GET, 'fk');
            $object->usuarioalteracao = $_SESSION['usuario'];
            $object->dataalteracao = date("d/m/Y H:i:s");
            unset($object->nome_convidado);

            $msg = '';
            $icone = 'info';
            if ($msg == '') {

                //percore o array de produtor_id e inseri no objeto AtividadeDerProdutor
                if (!empty($object->servidor_id)) {
                    foreach ($object->servidor_id as $key => $item) {
                        $objCS = new CertificadoServidorRecord();
                        $objCS->srh_certificado_id = $object->srh_certificado_id;
                        $objCS->servidor_id = $key;
                        $objCS->tipo = 'SERVIDOR';
                        $objCS->dataemissao = $object->dataemissao;
                        $objCS->usuarioalteracao = $object->usuarioalteracao;
                        $objCS->dataalteracao = $object->dataalteracao;
                        $objCS->store(); // armazena o objeto
                    }
                }
                if (!empty($object->produtor_id)) {
                    foreach ($object->produtor_id as $key => $item) {
                        $objAtivDerProd = new CertificadoServidorRecord();
                        $objAtivDerProd->srh_certificado_id = $object->srh_certificado_id;
                        $objAtivDerProd->produtor_id = $key;
                        $objAtivDerProd->tipo = 'PRODUTOR';
                        $objAtivDerProd->dataemissao = $object->dataemissao;
                        $objAtivDerProd->usuarioalteracao = $object->usuarioalteracao;
                        $objAtivDerProd->dataalteracao = $object->dataalteracao;
                        $objAtivDerProd->store(); // armazena o objeto
                    }
                }
                if (!empty($object->srh_convidado_id)) {
                    foreach ($object->srh_convidado_id as $key => $item) {
                        $objCSConvidado = new CertificadoServidorRecord();
                        $objCSConvidado->srh_certificado_id = $object->srh_certificado_id;
                        $objCSConvidado->srh_convidado_id = $key;
                        $objCSConvidado->tipo = 'CONVIDADO';
                        $objCSConvidado->dataemissao = $object->dataemissao;
                        $objCSConvidado->usuarioalteracao = $object->usuarioalteracao;
                        $objCSConvidado->dataalteracao = $object->dataalteracao;
                        $objCSConvidado->store(); // armazena o objeto
                    }
                }
                //  var_dump($objCS);
                ///  exit;
                // $object->store();
                $msg = 'Dados armazenados com sucesso';
            } else {
                $icone = 'error';
            }

            if ($icone == 'error') {
                // exibe mensagem de erro
                new TMessage($icone, $msg);
                $this->form->setData($object);   // fill the form with the active record data
            } else {
                $param = array();
                // define os parametros de cada acao
                $param['fk'] = $object->srh_certificado_id;

                $action = new Adianti\Control\TAction(array('CertificadoServidorFormList', 'onReload'));
                $action->setParameter('fk', $object->srh_certificado_id);
                // exibe um dialogo ao usuario
                new Adianti\Widget\Dialog\TMessage("info", "Registro salvo com sucesso!");
                TApplication::gotoPage('CertificadoServidorFormList', 'onReload', $param); // reload
                TApplication::gotoPage('CertificadoServidorFormList', 'onReload', $param); // reload
                //  $this->onReload($param);
            }
            // finaliza a transacao
            TTransaction::close();
        } catch (Exception $e) { // em caso de exc
            // exibe a mensagem gerada pela exce��o
            new TMessage('error', $e->getMessage());
            // desfaz todas altera��es no banco de dados
            TTransaction::rollback();
            $this->form->setData($this->form->getData());   // fill the form with the active record data
        }
    }

    /*
     * metodo onEdit()
     * Edita os dados de um registro
     */

    function onEdit($param) {
        try {

            if (isset($param['key'])) {

                // get the parameter $key
                $key = $param['key'];

                TTransaction::open('pg_ceres');   // open a transaction with database 'samples'

                $object = new CertificadoServidorRecord($key);        // instantiates object CertificadoServidor
                //converter datas do form para formato Brasileiro
                $object->dataemissao = TDate::date2br($object->dataemissao);

                $this->form->setData($object);   // fill the form with the active record data

                TTransaction::close();           // close the transaction
            } else {

                $this->form->clear();
            }
        } catch (Exception $e) {

            // in case of exception
            // shows the exception error message
            new TMessage('error', '<b>Error</b> ' . $e->getMessage());

            // undo all pending operations
            TTransaction::rollback();
        }
    }

    /*
     * metodo onReload()
     * Carrega a DataGrid com os objetos do banco de dados
     */

    function onReload($param = null) {

        $fk = filter_input(INPUT_GET, 'fk');

        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instancia um repositorio da Classe
        $repository = new TRepository('CertificadoServidorRecord');

        // cria um criterio de selecao
        $criteria = new TCriteria;

        //filtra pelo campo selecionado pelo usu�rio
        $criteria->add(new TFilter('srh_certificado_id', '=', $fk));

        //verifica quantos registros a consulta vai retornar
        $criteria->setProperty('order', 'id');

        // carrega os objetos de acordo com o criterio
        $cadastros = $repository->load($criteria);

        $this->datagrid->clear();
        if ($cadastros) {
            // percorre os objetos retornados
            foreach ($cadastros as $object) {
                //converter datas do form para formato Brasileiro
                $object->dataemissao = TDate::date2br($object->dataemissao);
                // adiciona o objeto na DataGrid
                $this->datagrid->addItem($object);
            }
        }
        // finaliza a transacao
        TTransaction::close();
        $this->loaded = true;
    }

    /*
     *
     * metodo onDelete()
     * Executada quando o usuario clicar no botao excluir da datagrid
     * Pergunta ao usuario se deseja realmente excluir um registro
     */

    function onDelete($param) {
        // obtem o parametro $key
        $key = $param['key'];

        // define duas acoes
        $action1 = new TAction(array($this, 'Delete'));

        // define os parametros de cada acao
        $action1->setParameter('key', $key);
        $action1->setParameter('fk', filter_input(INPUT_GET, 'fk'));

        // define duas acoes
        $action2 = new TAction(array($this, 'onReload'));

        // define os parametros de cada acao
        $action2->setParameter('key', $key);
        $action2->setParameter('fk', filter_input(INPUT_GET, 'fk'));

        //encaminha a chave estrangeira
        $action1->setParameter('fk', filter_input(INPUT_GET, 'fk'));

        // exibe um dialogo ao usuario
        new TQuestion('Deseja realmente excluir o registro ?', $action1, $action2);
    }

    /*
     * metodo Delete()
     * Exclui um registro
     */

    function Delete($param) {
        // obtem o parametro $key
        $key = $param['key'];
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instanicia objeto Record
        $cadastro = new CertificadoServidorRecord($key);

        try {
            // deleta objeto do banco de dados
            $cadastro->delete();

            // finaliza a transacao
            TTransaction::close();
        } catch (Exception $e) { // em caso de exce��o
            // exibe a mensagem gerada pela exce��o
            new TMessage('error', $e->getMessage());
            // desfaz todas altera��es no banco de dados
            TTransaction::rollback();
        }

        // re-carrega a datagrid
        $this->onReload();
        // exibe mensagem de sucesso
        //  new TMessage('info', "Registro Exclu&iacute;do com sucesso");
    }

    /*
     * metodo show()
     * Exibe a pagina
     */

    function show() {
        // carrega os dados no datagrid
        $this->onReload();
        //chama o metodo show da super classe
        parent::show();
    }
}
