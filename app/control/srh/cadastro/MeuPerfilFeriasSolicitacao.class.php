<?php
/*
 * classe FaltaservidorDetalhe
 * Cadastro de Faltaservidor: Contem a listagem e o formulario de busca
 */

    //ini_set('display_errors', 1);
    //ini_set('display_startup_erros', 1);
    //error_reporting(E_ALL);


use Adianti\Database\TFilter1;
use Adianti\Widget\Datagrid\TDatagridTables;

class MeuPerfilFeriasSolicitacao extends TPage {
    
    private $form;     
    private $datagrid; 


    public function __construct() {
    	parent::__construct();

    	$this->form = new TQuickForm;
        $this->form->class = 'form_myferias';
        $this->form->setFormTitle('<font color="red" size="3" face="Arial"><b>Soliciatar Férias</b></font>');

        TTransaction::open('pg_ceres');
        $cadastro = new ServidorRecord($_GET['fk']);
        if ($cadastro) {
            $nome = new TLabel($cadastro->nome);
            $matricula = new TLabel($cadastro->matricula);
        }
        $a = new TLabel();

        TTransaction::close();
       
       	$id = new THidden('servidor_id');
        $id->setValue($_GET['fk']);


        $datasolicitacao = new TEntry('datasolicitacao');
        $datasolicitacao->setValue(date('d/m/Y'));
        $datasolicitacao->setEditable(false);
        $situacao 		 = new TCombo('situacao');
        $datasituacao 	 = new TDate('datasituacao');
        $datasituacao->setValue(date('d/m/Y'));
       	$motivo = new TEntry('motivo');
        $datainicio = new TDate('datainicio');
        $datafim = new TDate('datafim');
        $observacao = new TText('observacao');
        $database = new TDate('database');

        $items = array();
        $items['EM TRAMITACAO'] = 'EM TRAMITACAO';
        $items['CONCLUIDA'] = 'CONCLUIDA';
        $items['REPROVADA'] = 'REPROVADA';
        $items['CANCELADA'] = 'CANCELADA';

        $situacao->addItems($items);

        $this->form->addQuickField(null, $id, 10);
        $this->form->addQuickField("Matricula ", $matricula, 10);
        $this->form->addQuickField('Data de Solicita&ccedil;&atilde;o', $datasolicitacao, 20);
        $this->form->addQuickField('Situa&ccedil;&atilde;o <font color=red><b>*</b></font>', $situacao, 40);
        $this->form->addQuickField('Motivo', $motivo, 40);
        $this->form->addQuickField('Data In&iacute;cio', $datasituacao, 20);
        $this->form->addQuickField('Data Fim', $datafim, 20);
        $this->form->addQuickField('Data Base <font color=red><b>*</b></font>', $database, 20);
        $this->form->addQuickField('Observa&ccedil;&atilde;o', $observacao, 60);
        

        $action2 = new TAction(array($this, 'onCancel'));
        $action2->setParameter('fk', $_GET['fk']);
        $action2->setParameter('key', $_GET['key']);
        $this->form->addQuickAction('Voltar', $action2, null);

        $action = new TAction(array($this, 'onSolicitar'));
        $action->setParameter('fk', $_GET['fk']);
        $action->setParameter('key', $_GET['key']);
        $this->form->addQuickAction('Solicitar ferias', $action, null);

        $this->datagrid = new TDatagridTables;

		$dgdatainicio = new TDataGridColumn('iniciogozo', 'Data In&iacute;cio', 'left', 80);
        $dgdatafim    = new TDataGridColumn('fimgozo',    'Data Fim',	        'left', 80);
        $exercicio    = new TDataGridColumn('exercicio',  'Exercicio',	  		'left', 80);
        
		$this->datagrid->addColumn($dgdatainicio);
        $this->datagrid->addColumn($dgdatafim);
        $this->datagrid->addColumn($exercicio);

		$this->datagrid->createModel();

        $panel = new TPanelForm(700, 500);
        $panel->put($this->form, 0, 0);
        $panel->put($this->datagrid, 150, 100);

        parent::add($panel);

    }



    function onReload() {

        TTransaction::open('pg_ceres');
        $repository = new TRepository('FeriasRecord');
        $criteria = new TCriteria;
        $criteria->setProperty('order', 'iniciogozo desc');
        $criteria->add(new TFilter('servidor_id', '=', $_SESSION['servidor_id']));
    
        $cadastros = $repository->load($criteria);

        $this->datagrid->clear();
        if ($cadastros)
        {    
            foreach ($cadastros as $cadastro)
            {
                $cadastro->iniciogozo = TDate::date2br( $cadastro->iniciogozo );
                $cadastro->fimgozo = TDate::date2br( $cadastro->fimgozo );

                $this->datagrid->addItem($cadastro);
            }
        }
        TTransaction::close();
        $this->loaded = true;
    }

    function onCancel(){

        $params = array();
		$params['fk'] = filter_input ( INPUT_GET, 'fk' );
		$params['key'] = filter_input ( INPUT_GET, 'key' );
		
        TApplication::gotoPage('MeuPerfilFerias','onEdit', $params); // reload
    }

    function onSolicitar(){

        $params = array();
        $params['fk'] = filter_input ( INPUT_GET, 'fk' );
        $params['key'] = filter_input ( INPUT_GET, 'key' );
        
        TApplication::gotoPage('MeuPerfilFerias','onEdit', $params); // reload
    }

}
?>