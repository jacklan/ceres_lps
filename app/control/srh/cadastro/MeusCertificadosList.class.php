<?php

/*
 * classe MeusCertificadosList
 * Listagem dos Certificado: Contem a listagem e o formulario de busca
 * Autor: Jackson Meires
 * Data:30/09/2016
 */
ini_set('display_errors', 1);
ini_set('display_startup_erros', 1);
error_reporting(E_ALL);

use Adianti\Widget\Datagrid\TDatagridTables;

class MeusCertificadosList extends TPage {

    private $form;     // formulario de cadastro
    private $datagrid; // listagem
    private $loaded;

    /*
     * metodo construtor
     * Cria a pagina, o formulario e a listagem
     */

    public function __construct() {
        parent::__construct();

        // instancia um formulario
        $this->form = new TQuickForm;
        // $this->form->class = 'form_acidente';
        $this->form->setFormTitle('<font color="red" size="3" face="Arial"><b>Meus Certificados</b></font>');

        TTransaction::open('pg_ceres');   // inicia transacao com o banco 'pg_ceres'
        // cria um rotulo para o titulo
        $lb_nome_servidor = new TLabel("<div style='position:floatval; width: 500px;'>Servidor: <b style='color:red;'>" . (new ServidorRecord($_SESSION['servidor_id']))->nome . "</b></div>");
        TTransaction::close(); // finaliza a transacao
        $lb_nome_servidor->setFontFace('Arial');
        $lb_nome_servidor->setFontSize(10);

        $this->form->addQuickField(null, $lb_nome_servidor, 200);

        if (filter_input(INPUT_GET, 'back') === 'MeuPerfilList') {
            // prepara o botao para chamar o form anterior
            $action1 = new TAction(array("MeuPerfilList", 'onReload'));
            $action1->setParameter('fk', filter_input(INPUT_GET, 'fk'));
        } else {
            // prepara o botao para chamar o form anterior
            $action1 = new TAction(array("ServidorForm", 'onEdit'));
            $action1->setParameter('fk', filter_input(INPUT_GET, 'fk'));
        }

        $this->form->addQuickAction('Voltar', $action1, 'ico_datagrid.gif');

        // creates one datagrid
        $this->datagrid = new TDatagridTables;
        $this->datagrid->disableDefaultClick(); // important!
        // instancia as colunas da DataGrid
        $dgcurso = new TDataGridColumn('nome_curso', 'Curso', 'left', 350);
        $dgdatainicio = new TDataGridColumn('datainicio', 'Data Inicio', 'left', 80);
        $dgdatafim = new TDataGridColumn('datainicio', 'Data Inicio', 'left', 80);
        $dgcargahoraria = new TDataGridColumn('cargahoraria', 'Carga Hor&aacute;ria', 'left', 60);
        $dgnome_municipio = new TDataGridColumn('nome_municipio', 'Munic&iacute;pio', 'left', 300);

        // adiciona as colunas a DataGrid
        //$this->datagrid->addColumn($dgcodigo);
        $this->datagrid->addColumn($dgcurso);
        $this->datagrid->addColumn($dgdatainicio);
        $this->datagrid->addColumn($dgdatafim);
        $this->datagrid->addColumn($dgcargahoraria);
        $this->datagrid->addColumn($dgnome_municipio);

        $action1 = new TDataGridAction(array($this, 'onGerar'));
        $action1->setLabel('Gerar Certificado');
        $action1->setImage('ico_fichacatalografica.png');
        $action1->setField('id');
        $action1->setFk('servidor_id');
        $action1->setParameter('back', 'MeuPerfilList');

        // adiciona as acoes a DataGrid
        $this->datagrid->addAction($action1);
        // cria o modelo da DataGrid, montando sua estrutura
        $this->datagrid->createModel();

        // monta a paina atraves de uma tabela
        $panel = new TPanelForm(700, 500);
        $panel->put($this->form, 0, 0);
        $panel->put($this->datagrid, 150, 330);

        // adiciona a tabela a pagina
        parent::add($panel);
    }

    /**
     * Open an input dialog
     */
    public function onGerar($param) {
        $form = new TQuickForm('input_form');
        $form->style = 'padding:20px';

        $opcaoImageCertificado = new TCheckButton('opcao_imagem_certificado');
        $opcaoImageCertificado->setValue(array('off'));
        $form->addQuickField('Imprimir Com Fundo Branco', $opcaoImageCertificado);

        //actions
        $action1 = new TAction(array($this, 'onConfirm1'));
        $action1->setParameter('key', filter_input(INPUT_GET, 'key'));
        $action1->setParameter('fk', filter_input(INPUT_GET, 'fk'));
        $action1->setParameter('back', 'MeuPerfilList');

        $form->addQuickAction('Gerar', $action1, 'ico_fichacatalografica.png');

        // show the input dialog
        new TInputDialog('Gerar Certificado', $form);
    }

    /**
     * Show the input dialog data
     */
    public function onConfirm1($param) {
        new CertificadoCursoPDF();
        $this->onReload();
    }

    /*
     * metodo onReload()
     * Carrega a DataGrid com os objetos do banco de dados
     */

    function onReload($param = NULL) {
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instancia um repositorio da Classe
        $repository = new TRepository('CertificadoServidorRecord');

        // cria um criterio de selecao
        $criteria = new TCriteria;

        //filtra pelo campo selecionado pelo usu�rio
        $criteria->add(new TFilter('servidor_id', '=', $_SESSION['servidor_id']));

        //verifica quantos registros a consulta vai retornar
        $criteria->setProperty('order', 'id');

        // carrega os objetos de acordo com o criterio
        $cadastros = $repository->load($criteria);

        $this->datagrid->clear();
        if ($cadastros) {
            // percorre os objetos retornados
            foreach ($cadastros as $object) {
                // adiciona o objeto na DataGrid
                $this->datagrid->addItem($object);
            }
        }
        // finaliza a transacao
        TTransaction::close();
        $this->loaded = true;
    }

    /*
     * metodo show()
     * Exibe a pagina
     */

    function show() {
        // check if the datagrid is already loaded
        if (!$this->loaded) {
            $this->onReload(func_get_arg(0));
        }
        parent::show();
    }

}
