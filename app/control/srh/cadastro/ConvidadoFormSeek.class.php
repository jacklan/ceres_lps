<?php

use Adianti\Database\TFilter1;

/**
 * ConvidadoRecord Seek
 *
 * @version    2.0
 * @package    pg_ceres
 * @subpackage der
 * @author     Jackson Meires
 * @copyright  Copyright (c) 2006-2014 Adianti Solutions Ltd. (http://www.adianti.com.br)
 * @license    http://www.adianti.com.br/framework-license
 */
class ConvidadoFormSeek extends TWindow {

    private $form;      // form
    private $datagrid;  // datagrid
    private $pageNavigation;
    private $loaded;

    /**
     * Class constructor
     * Creates the page, the search form and the listing
     */
    public function __construct() {
        parent::__construct();
        parent::setSize(700, 500);
        parent::setTitle('Pesquisar record');
        new TSession;

        // creates the form
        $this->form = new TQuickForm('form_search_convidado');
        // $this->form->class = 'tform';
        $this->form->setFormTitle('Convidado');

        // create the form fields
        $nome = new TEntry('nome');
        $nome->setValue(TSession::getValue('convidado_nome'));

        // add the form fields
        $this->form->addQuickField('Nome', $nome, 60);

        // define the form action
        $this->form->addQuickAction('Pesquisar', new TAction(array($this, 'onSearch')), 'ico_find.png');
        
        $action = new TAction(array('ConvidadoWindowForm', 'onLoad'));
        $action->setParameter('fk', filter_input(INPUT_GET, 'fk'));
        
        $this->form->addQuickAction('Novo', $action, 'ico_add.png');

        // creates a DataGrid
        $this->datagrid = new TQuickGrid;
        $this->datagrid->style = 'width: 100%';
        $this->datagrid->setHeight(230);
        $this->datagrid->enablePopover('Nome', 'Nome {nome}');

        // creates the datagrid columns
        $this->datagrid->addQuickColumn('Id', 'id', 'right', 40);
        $this->datagrid->addQuickColumn('Nome', 'nome', 'left', 340);

        // creates two datagrid actions
        $this->datagrid->addQuickAction('Selecionar', new TDataGridAction(array($this, 'onSelect')), 'id', 'ico_apply.png');

        // create the datagrid model
        $this->datagrid->createModel();

        // creates the page navigation
        $this->pageNavigation = new TPageNavigation;
        $this->pageNavigation->setAction(new TAction(array($this, 'onReload')));
        $this->pageNavigation->setWidth($this->datagrid->getWidth());

        // creates a container
        $container = new TVBox;
        $container->style = 'width: 100%';
        $container->add($this->form);
        $container->add($this->datagrid);
        $container->add($this->pageNavigation);

        // add the container inside the page
        parent::add($container);
    }

    /**
     * method onSearch()
     * Register the filter in the session when the user performs a search
     */
    function onSearch() {
        // get the search form data
        $data = $this->form->getData();

        $campo = 'nome';

        // check if the user has filled the form
        if (isset($data->nome)) {
            // creates a filter using what the user has typed
            $filter = new TFilter1('special_like(' . $campo . ",'" . $data->nome . "')");
            // stores the filter in the session
            TSession::setValue('Convidado_filter', $filter);
            TSession::setValue('convidado_nome', $data->nome);

            // fill the form with data again
            $this->form->setData($data);
        }

        // redefine the parameters for reload method
        $param = array();
        $param['offset'] = 0;
        $param['first_page'] = 1;
        $this->onReload($param);
    }

    /**
     * Load the datagrid with the database objects
     */
    function onReload($param = NULL) {
        try {
            // open a transaction with database 'pg_ceres'
            TTransaction::open('pg_ceres');

            // creates a repository for ConvidadoRecord
            $repository = new TRepository('ConvidadoRecord');
            $limit = 10;
            // creates a criteria
            $criteria = new TCriteria;

            // default order
            if (!isset($param['order'])) {
                $param['order'] = 'id';
            }

            $criteria->setProperties($param); // order, offset
            $criteria->setProperty('limit', $limit);

            //filtra pelo campo selecionado pelo usuario
            // $criteria->add(new TFilter('tipoConvidado', '=', 'TITULAR'));

            if (TSession::getValue('Convidado_filter')) {
                // add the filter stored in the session to the criteria
                $criteria->add(TSession::getValue('Convidado_filter'));
            }

            // load the objects according to the criteria
            $objects = $repository->load($criteria);
            $this->datagrid->clear();
            if ($objects) {
                foreach ($objects as $object) {
                    // add the object inside the datagrid
                    $this->datagrid->addItem($object);
                }
            }

            // reset the criteria for record count
            $criteria->resetProperties();
            $count = $repository->count($criteria);

            $this->pageNavigation->setCount($count); // count of records
            $this->pageNavigation->setProperties($param); // order, page
            $this->pageNavigation->setLimit($limit); // limit
            // close the transaction
            TTransaction::close();
            $this->loaded = true;
        } catch (Exception $e) { // in case of exception
            // shows the exception error message
            new TMessage('error', $e->getMessage());
            // undo all pending operations
            TTransaction::rollback();
        }
    }

    /**
     * Executed when the user chooses the record
     */
    public function onSelect($param) {
        try {
            $key = $param['key'];
            TTransaction::open('pg_ceres');

            // load the active record
            $convidado = new ConvidadoRecord($key);

            // closes the transaction
            TTransaction::close();

            $object = new StdClass;
            $object->srh_convidado_id = $convidado->id;
            $object->nome_convidado = $convidado->nome;

            TForm::sendData('form_certificado_participante', $object);
            parent::closeWindow(); // closes the window
        } catch (Exception $e) { // em caso de exce��o
            // clear fields
            $object = new StdClass;
            $object->srh_convidado_id = '';
            $object->nome_convidado = '';
            TForm::sendData('form_certificado_participante', $object);

            // undo pending operations
            TTransaction::rollback();
        }
    }

    /**
     * Shows the page
     */
    function show() {
        // if the datagrid was not loaded yet
        if (!$this->loaded) {
            $this->onReload();
        }
        parent::show();
    }

}
