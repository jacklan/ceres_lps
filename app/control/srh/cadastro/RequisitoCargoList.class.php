<?php
//ini_set('display_errors', 1);
//ini_set('display_startup_erros', 1);
//error_reporting(E_ALL);
/*
 * classe RequisitoCargoList
 * Cadastro de RequisitoCargo: Contem a listagem e o formulario de busca
 */
use Adianti\Database\TFilter1;
use Adianti\Widget\Datagrid\TDatagridTables;

class RequisitoCargoList extends TPage
{
    private $form;     // formulario de cadastro
    private $datagrid; // listagem


    /*
     * metodo construtor
     * Cria a pagina, o formulario e a listagem
     */
    public function __construct()
    {
        parent::__construct();

        // instancia um formulario
        $this->form = new TForm('form_busca_RequisitoCargo');

        // instancia uma tabela
        $panel = new TPanelForm(900, 100);

        // adiciona a tabela ao formulario
        $this->form->add($panel);

        // cria um r�tulo para o t�tulo
        $titulo = new TLabel('Listagem Requisitos de Cargos');
        $titulo->setFontFace('Arial');
        $titulo->setFontColor('red');
        $titulo->setFontSize(12);

        // adiciona o campo Titulo
        $panel->put($titulo,$panel->getColuna(),$panel->getLinha());

        // cria os campos do formulario
        $opcao = new TCombo('opcao');

        // cria um vetor com as opcoes da combo
        $items= array();
        $items['nome'] = 'Nome';
        $items['nome_abreviado'] = 'Abreviacao';
        
        // adiciona as opcoes na combo
        $opcao->addItems($items);
        //coloca o valor padrao no combo
        $opcao->setValue('nome');
        $opcao->setSize(50);

        $nome  = new TEntry('nome');
        $nome->setSize(50);

        // cria um botao de RequisitoCargo (buscar)
        $find_button=new TButton('busca');
        // cria um botao de RequisitoCargo (cadastrar)
        $new_button=new TButton('novo');

        // define a RequisitoCargo do botao buscar
        $find_button->setAction(new TAction(array($this, 'onSearch')), 'Buscar');

        $obj = new RequisitoCargoForm;
        // define a RequisitoCargo do botao cadastrar
        $new_button->setAction(new TAction(array($obj, 'onEdit')), 'Novo');

        // adiciona o campo
        $panel->putCampo(null, 'Selecione o Campo:', 0, 0);
		$panel->put($opcao, $panel->getColuna(),$panel->getLinha());
        $panel->put(new TLabel('Informe o Valor da Busca:'), $panel->getColuna(),$panel->getLinha());
        $panel->put($nome, $panel->getColuna(),$panel->getLinha());
        $panel->put($find_button, $panel->getColuna(),$panel->getLinha());
        $panel->put($new_button, $panel->getColuna(),$panel->getLinha());

        // define quais sao os campos do formulario
        $this->form->setFields(array($nome, $opcao, $find_button, $new_button));

        // instancia objeto DataGrid
        $this->datagrid = new TDatagridTables;

        // instancia as colunas da DataGrid
        $dgnome      = new TDataGridColumn('nome',    'Nome',    'left', 1200);
        $dgnome_abreviado = new TDataGridColumn('nome_abreviado',   'Abreviacao',  'left',  150);
        

        // adiciona as colunas a DataGrid
        $this->datagrid->addColumn($dgnome);
        $this->datagrid->addColumn($dgnome_abreviado);
      
      

        // instancia duas acoes da DataGrid
        $action1 = new TDataGridAction(array($obj, 'onEdit'));
        $action1->setLabel('Editar');
        $action1->setImage('ico_edit.png');
        $action1->setField('id');

        $action2 = new TDataGridAction(array($this, 'onDelete'));
        $action2->setLabel('Deletar');
        $action2->setImage('ico_delete.png');
        $action2->setField('id');

        // adiciona as acoes a DataGrid
        $this->datagrid->addAction($action1);
        $this->datagrid->addAction($action2);

        // cria o modelo da DataGrid, montando sua estrutura
        $this->datagrid->createModel();

        // monta a paina atraves de uma tabela
        $panel = new TPanelForm(700, 500);
        $panel->put($this->form, 0, 0);
        $panel->put($this->datagrid, 150, 115);

        // adiciona a tabela a pagina
        parent::add($panel);
    }

    /*
     * metodo onReload()
     * Carrega a DataGrid com os objetos do banco de dados
     */
    function onReload() {
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instancia um repositorio para Carros
        $repository = new TRepository('RequisitoCargoRecord');

        $criteria = new TCriteria;
        $criteria->setProperty('order', 'nome');

        // carrega os objetos de acordo com o criterio
        $cadastros = $repository->load($criteria);

        $this->datagrid->clear();
        if ($cadastros) {
            // percorre os objetos retornados
            foreach ($cadastros as $cadastro) {
                // adiciona o objeto na DataGrid
                $this->datagrid->addItem($cadastro);
            }
        }
        // finaliza a transacao
        TTransaction::close();
        $this->loaded = true;
    }
	
	function onSearch() {
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instancia um repositorio para Carros
        $repository = new TRepository('RequisitoCargoRecord');

	//obtem os dados do formulario de busca
        $data = $this->form->getData();
        //obtem os dados do formulario de busca
        
        //obtem os dados do formulario de busca
        $campo = $data->opcao;
        $dados = $data->nome;
        
        if ((! $dados) || (! $campo) ) {
        	//pega os dados da url
        	$campo = ''; //$_GET['campo'];
        	$dados = ''; //$_GET['dados'];
        
        }

        // cria um criterio de selecao, ordenado pelo id
        $criteria = new TCriteria;
        $criteria->setProperty('order', 'id DESC');

        //verifica se o usuario preencheu o formulario
        if ($dados) {
            if (is_numeric($dados)) {
                $criteria->add(new TFilter($campo, 'like', '%'.$dados.'%'));
            } else {
                //filtra pelo campo selecionado pelo campo ignore case
                $criteria->add(new TFilter1('special_like(' . $campo . ",'" . $dados . "')"));
            }
        }

        // carrega os objetos de acordo com o criterio
        $cadastros = $repository->load($criteria);

        $this->datagrid->clear();
        if ($cadastros) {
            // percorre os objetos retornados
            foreach ($cadastros as $cadastro) {
                // adiciona o objeto na DataGrid
                $this->datagrid->addItem($cadastro);
            }
        }

        // finaliza a transacao
        TTransaction::close();
        $this->loaded = true;
    }


    /*
     * metodo onDelete()
     * Executada quando o usuario clicar no botao excluir da datagrid
     * Pergunta ao usuario se deseja realmente excluir um registro
     */
    function onDelete($param)
    {
        // obtem o parametro $key
        $key=$param['key'];

        // define duas acoes
        $action1 = new TAction(array($this, 'Delete'));

        // define os parametros de cada RequisitoCargo
        $action1->setParameter('key', $key);

        // exibe um dialogo ao usuario
        new TQuestion('Deseja realmente excluir o registro ?', $action1);
    }

    /*
     * metodo Delete()
     * Exclui um registro
     */
    function Delete($param)
    {
        // obtem o parametro $key
        $key=$param['key'];
        // inicia transRequisitoCargo com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instanicia objeto Record
        $cadastro = new RequisitoCargoRecord($key);

        try{
            // deleta objeto do banco de dados
            $cadastro->delete();
			// exibe um dialogo ao usuario
            new TMessage("info", "Registro deletado com sucesso!");

            // finaliza a transRequisitoCargo
            TTransaction::close();
        }
        catch (Exception $e) // em caso de exce��o
        {
            // exibe a mensagem gerada pela exce��o
            new TMessage('error', $e->getMessage());
            // desfaz todas altera��es no banco de dados
            TTransaction::rollback();
        }

        // re-carrega a datagrid
        $this->onReload();
      
    }

    /*
     * metodo show()
     * Exibe a pagina
     */
    function show()
    {
        // carrega os dados no datagrid
        $this->onReload();
        //chama o metodo show da super classe
        parent::show();

    }
}
?>