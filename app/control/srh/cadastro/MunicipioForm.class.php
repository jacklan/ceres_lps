<?php

/*
 * classe MunicipioForm
 * Cadastro de Municipio: Contem o formularo
 */



class MunicipioForm extends TPage {

    private $form;     // formulario de cadastro

    /*
     * metodo construtor
     * Cria a pagina do formulario
     */

    public function __construct() {
        parent::__construct();

        // instancia um formulario
        $this->form = new TQuickForm;
        $this->form->class = 'form_Municipio';
        $this->form->setFormTitle('<font color="red" size="3" face="Arial"><b>Formul&aacute;rio do Município</b></font>');
        
        // cria os campos do formulario
        $codigo     = new THidden('id');
        $codigo->setEditable(false);
        $nome  = new TEntry('nome');
        $qtdagricultores = new TEntry('qtdagricultores');
        $territorio_id  = new TCombo('territorio_id');
        $modeloeixo_id = new TCombo('modeloeixo_id');
        $situacaoconcentrado = new TCombo('concentrado');
        $latitude = new TEntry('latitude');
        $longitude = new TEntry('longitude');

        $usuarioalteracao = new THidden('usuarioalteracao');
        $dataalteracao = new THidden('dataalteracao');
        
        // cria um rotulo para o titulo
        $titulo = new TLabel('<div style="position:floatval; width: 200px;"> <b>* Campo obrigatorio</b></div>');
        $titulo->setFontFace('Arial');
        $titulo->setFontColor('red');
        $titulo->setFontSize(10);


        //cria a colecao da tabela estrangeira
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $repository = new TRepository('TerritorioRecord');
        // carrega todos os objetos
        $collection = $repository->load(new TCriteria);
        //adiciona os objetos no combo
        foreach ($collection as $object){
            $items[$object->id] = $object->nome;
        }

        // adiciona as opcoes na combo
        $territorio_id->addItems($items);
        //coloca o valor padrao no combo
        $territorio_id->setValue('NATAL');

        // finaliza a transacao
        TTransaction::close();

        //cria a colecao da tabela estrangeira
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $repository = new TRepository('ModeloEixoRecord');
        // carrega todos os objetos
        $collection = $repository->load(new TCriteria);
        //adiciona os objetos no combo
        foreach ($collection as $object){
            $items2[$object->id] = $object->nome;
        }

        // adiciona as opcoes na combo
        $modeloeixo_id->addItems($items2);

        // finaliza a transacao
        TTransaction::close();

        // cria um vetor com as opcoes da combo
        $items= array();
        $items['SIM'] = 'SIM';
        $items['NÃO'] = 'NÃO';

        // adiciona as opcoes na combo
        $situacaoconcentrado->addItems($items);
        
        //coloca o valor padrao no combo
        //$uf->setValue('RN');

        //Campos obrigatórios
        $nome->setProperty('required', 'required');
        $qtdagricultores->setProperty('required', 'required');
        $latitude->setProperty('required', 'required');
        $longitude->setProperty('required', 'required');
                
        // Auto-preenchimento
        $qtdagricultores->setProperty('placeholder', 'ex.:1.250');
        $latitude->setProperty('placeholder', 'ex.:-27,32541');
        $longitude->setProperty('placeholder', 'ex.:-6.54785');

        // define os campos
        $this->form->addQuickField(null, $codigo, 10);
        $this->form->addQuickField(null, $usuarioalteracao, 10);
        $this->form->addQuickField(null, $dataalteracao, 10);
        $this->form->addQuickField('Nome <font color=red><b>*</b></font>', $nome, 50);
        $this->form->addQuickField('Territorio <font color=red><b>*</b></font>', $territorio_id, 50);
        $this->form->addQuickField('Qtd Agricultores <font color=red><b>*</b></font>', $qtdagricultores, 50);
        $this->form->addQuickField('Modelo Eixo', $modeloeixo_id, 50);
        $this->form->addQuickField('Concentrado', $situacaoconcentrado, 50);
        $this->form->addQuickField('Latitude <font color=red><b>*</b></font>', $latitude, 50);
        $this->form->addQuickField('Longitude <font color=red><b>*</b></font>', $longitude, 50);
        $this->form->addQuickField(null, $titulo, 50);
        
         // cria um botao de acao
        $this->form->addQuickAction('Salvar', new TAction(array($this, 'onSave')), 'ico_save.png');
        $this->form->addQuickAction('Voltar', new TAction(array('MunicipioList', 'onReload')), 'ico_datagrid.gif');

        // adiciona a tabela a pagina
        parent::add($this->form);
    }

    /*
     * metodo onSave()
     * Executada quando o usuario clicar no botao salvar do formulario
     */

    function onSave() {
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // obtem os dados no formulario em um objeto CarroRecord
        $cadastro = $this->form->getData('MunicipioRecord');

         //lanca o default
        $cadastro->usuarioalteracao = $_SESSION['usuario'];
        $cadastro->dataalteracao = date("d/m/Y H:i:s");
        
        //antes de armazenar verifica se algum campo eh requerido e nao foi informado
        $dados = $cadastro->toArray();
        $obj = implode(" , ",$dados); //converte os dados do array para string
        
        $msg = '';
        $icone = 'info';

        if (empty($dados['nome'])) {
            $msg .= 'O Nome deve ser informado.<br/>';
        }

        if (empty($dados['territorio_id'])) {
            $msg .= 'O Territorio deve ser informado.<br/>';
        }

        if (empty($dados['qtdagricultores'])) {
            $msg .= 'A Qtd Agricultores deve ser informada.<br/>';
        }

        if (empty($dados['latitude'])) {
            $msg .= 'A Latitude deve ser informada.<br/>';
        }

        if (empty($dados['longitude'])) {
            $msg .= 'A Longitude deve ser informada.<br/>';
        }

        try {

            if ($msg == '') {
                // armazena o objeto
                $cadastro->store();
                $msg = 'Dados armazenados com sucesso';
                
                 //criando log 
//                TTransaction::setLogger(new TLoggerTXT('tmp/log.txt'));
//                TTransaction::Log(' ---- Insert ---- '. $obj);
                
                // finaliza a transacao
                TTransaction::close();
            } else {
                $icone = 'error';
            }

            if ($icone == 'error') {
                // exibe mensagem de erro
                new TMessage($icone, $msg);
                $this->form->setData($cadastro);   // fill the form with the active record data
            } else {
                // exibe um dialogo ao usuario
                new TMessage("info", "Registro salvo com sucesso!");
                TApplication::gotoPage('MunicipioList','onReload'); // reload
            }
        } catch (Exception $e) { // em caso de exce��o
            // exibe a mensagem gerada pela exce��o
            new TMessage('error', $e->getMessage());
            // desfaz todas altera��es no banco de dados
            TTransaction::rollback();
            $this->form->setData($cadastro);   // fill the form with the active record data
        }
    }

    /*
     * metodo onEdit()
     * Edita os dados de um registro
     */

    function onEdit($param) {
        try {
            if (isset($param['key'])) {

                // get the parameter $key
                $key = $param['key'];

                TTransaction::open('pg_ceres');   // open a transaction with database 'samples'

                $object = new MunicipioRecord($key);        // instantiates object City

                $this->form->setData($object);   // fill the form with the active record data

                TTransaction::close();           // close the transaction
            } else {
                $this->form->clear();
            }
        } catch (Exception $e) { // in case of exception
            // shows the exception error message
            new TMessage('error', '<b>Error</b> ' . $e->getMessage());
            // undo all pending operations
            TTransaction::rollback();
        }
    }


}

?>