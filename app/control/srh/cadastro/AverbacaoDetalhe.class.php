<?php

//ini_set('display_errors', 1);
//ini_set('display_startup_erros', 1);
//error_reporting(E_ALL);

/*
 * classe AverbacaoDetalhe
 * Autor: Jackson Meires
 * Data:07/12/2016
 * Cadastro de Averbacao: Contem a listagem e o formulario de busca
 */
use Lib\Util\TUtil;
use Adianti\Widget\Datagrid\TDatagridTables;

class AverbacaoDetalhe extends TPage {

    private $form;     // formulario de cadastro
    private $datagrid; // listagem

    /*
     * metodo construtor
     * Cria a pagina, o formulario e a listagem
     */

    public function __construct() {
        parent::__construct();

        // instancia um formulario
        $this->form = new TQuickForm('form_averbacao');
        //   $this->form->class = 'form_Averbacao';
        $this->form->setFormTitle('<font color="red" size="3" face="Arial"><b>Detalhe de Servidor Averba&ccedil;&atilde;o</b></font>');

        // cria um rotulo para o titulo
        $campos_obrigatorios = new TLabel('<div style="position:floatval; width: 200px;"> <b>* Campo obrigatorio</b></div>');
        $campos_obrigatorios->setFontFace('Arial');
        $campos_obrigatorios->setFontColor('red');
        $campos_obrigatorios->setFontSize(10);

        // cria os campos do formulario
        $codigo = new THidden('id');
        $codigo->setEditable(false);
        $servidor_id = new THidden('servidor_id');
        $servidor_id->setValue(filter_input(INPUT_GET, 'fk'));
        $datainicio = new TDate('datainicio');
        $datafim = new TDate('datafim');
        $orgao_id = new TDBCombo('orgao_id', 'pg_ceres', 'OrgaoRecord', 'id', 'nome', 'nome');
        $tempoficto = new TCombo('tempoficto');
        $numerodias = new TEntry('numerodias');
        //$numerodias->setEditable(false);
        $diasvaoaverbados = new TEntry('diasnaoaverbados');
        $usuarioalteracao = new THidden('usuarioalteracao');
        $usuarioalteracao->setEditable(false);
        $dataalteracao = new THidden('dataalteracao');
        $dataalteracao->setEditable(false);
        $observacao = new TText('observacao');

        // adiciona as opcoes na combo situacao
        $items = array();
        $items['SIM'] = 'SIM';
        $items['NAO'] = 'NAO';

        // adiciona as opcoes na combo
        $tempoficto->addItems($items);
        $tempoficto->setValue('NAO');

        //cria a colecao da tabela estrangeira do Cabecalho
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $cadastro = new ServidorRecord(filter_input(INPUT_GET, 'fk'));
        // cria um criterio de selecao, ordenado pelo id do servidor
        //adiciona os objetos na tela com as informacoes do servidor
        if ($cadastro) {
            $nome = new TLabel($cadastro->nome);
            $matricula = new TLabel($cadastro->matricula);
        }
        // finaliza a transacao
        TTransaction::close();

        // set action for numerodias
        $action_numdias = new TAction(array($this, 'onNumDiasAction'));
        $datafim->setExitAction($action_numdias);

        // define os campos
        $this->form->addQuickField(null, $codigo, 1);
        $this->form->addQuickField(null, $servidor_id, 1);
        $this->form->addQuickField('Nome', $nome, 400);
        $this->form->addQuickField('Matr&iacutecula', $matricula, 200);
        $this->form->addQuickField('Data Inicio <font color=red><b>*</b></font>', $datainicio, 15);
        $this->form->addQuickField('Data Fim <font color=red><b>*</b></font>', $datafim, 15);
        $this->form->addQuickField('Tempo Ficto', $tempoficto, 10);
        $this->form->addQuickField('Num. Dias', $numerodias, 10);
        $this->form->addQuickField('Org&atilde;o <font color=red><b>*</b></font>', $orgao_id, 50);
        $this->form->addQuickField('Dias n&atilde;o Averbados', $diasvaoaverbados, 15);
        $this->form->addQuickField('Observa&ccedil&atildeo', $observacao, 60);
        $this->form->addQuickField(null, $dataalteracao, 0);
        $this->form->addQuickField(null, $usuarioalteracao, 0);
        $this->form->addQuickField(null, $campos_obrigatorios, 0);

        // cria um botao de acao
        $action1 = new TAction(array($this, 'onSave'));
        $action1->setParameter('fk', filter_input(INPUT_GET, 'fk'));

        $this->form->addQuickAction('Salvar', $action1, 'ico_save.png')->class = 'btn btn-info btnleft';

        // prepara o botao para chamar o form anterior
        $action2 = new TAction(array("ServidorForm", 'onEdit'));
        $action2->setParameter('fk', filter_input(INPUT_GET, 'fk'));

        $this->form->addQuickAction('Voltar', $action2, 'ico_datagrid.gif');

        // instancia objeto DataGrid
        $this->datagrid = new TDatagridTables;

        // instancia as colunas da DataGrid
        $dgorgao = new TDataGridColumn('nome_orgao', 'Org&atilde;o', 'left', 250);
        $dgdatainicio = new TDataGridColumn('datainicio', 'Data In&iacute;cio', 'left', 80);
        $dgdatafim = new TDataGridColumn('datafim', 'Data Fim', 'left', 80);
        $dgobservacao = new TDataGridColumn('observacao', 'Observa&ccedil;&atilde;o', 'left', 320);
        $dgtempoficto = new TDataGridColumn('tempoficto', 'Tempo Ficto', 'left', 100);
        $dgnumerodias = new TDataGridColumn('numerodias', 'Num. Dias', 'left', 100);

        // adiciona as colunas a DataGrid
        $this->datagrid->addColumn($dgorgao);
        $this->datagrid->addColumn($dgdatainicio);
        $this->datagrid->addColumn($dgdatafim);
        $this->datagrid->addColumn($dgobservacao);
        $this->datagrid->addColumn($dgtempoficto);
        $this->datagrid->addColumn($dgnumerodias);

        // instancia duas acoes da DataGrid
        $action1 = new TDataGridAction(array($this, 'onEdit'));
        $action1->setLabel('Editar');
        $action1->setImage('ico_edit.png');
        $action1->setField('id');
        $action1->setFk('servidor_id');

        $action2 = new TDataGridAction(array($this, 'onDelete'));
        $action2->setLabel('Deletar');
        $action2->setImage('ico_delete.png');
        $action2->setField('id');
        $action2->setFk('servidor_id');

        // adiciona as acoes a DataGrid
        $this->datagrid->addAction($action1);
        $this->datagrid->addAction($action2);

        // cria o modelo da DataGrid, montando sua estrutura
        $this->datagrid->createModel();

        // monta a paina atraves de uma tabela
        $panel = new TPanelForm(900, 600);
        $panel->put($this->form, 0, 0);
        $panel->put($this->datagrid, 150, 300);

        // adiciona a tabela a pagina
        parent::add($panel);
    }

    /**
     * Action to be executed when the user leaves the input_exit field
     */
    public static function onNumDiasAction($param) {
        $obj = new StdClass;
        $obj->numerodias = TUtil::diasDeDiferenca($param['datainicio'], $param['datafim']);
        // TEntry::enableField('form_averbacao', 'numerodias');

        TForm::sendData('form_averbacao', $obj);
    }

    /*
     * metodo onReload()
     * Carrega a DataGrid com os objetos do banco de dados
     */

    function onReload() {

        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instancia um repositorio da Classe
        $repository = new TRepository('AverbacaoRecord');

        // cria um criterio de selecao
        $criteria = new TCriteria;

        //filtra pelo campo selecionado pelo usuário
        $criteria->add(new TFilter('servidor_id', '=', filter_input(INPUT_GET, 'fk')));
        $criteria->setProperty('order', 'id');
        // carrega os objetos de acordo com o criterio
        $cadastros = $repository->load($criteria);

        $this->datagrid->clear();
        if ($cadastros) {
            // percorre os objetos retornados
            foreach ($cadastros as $cadastro) {
                //converter datas do form para formato Brasileiro
                $cadastro->datainicio = TDate::date2br($cadastro->datainicio);
                $cadastro->datafim = TDate::date2br($cadastro->datafim);
                // adiciona o objeto na DataGrid
                $this->datagrid->addItem($cadastro);
            }
        }


        // finaliza a transacao
        TTransaction::close();
        $this->loaded = true;
    }

    /*
     * metodo onDelete()
     * Executada quando o usuario clicar no botao excluir da datagrid
     * Pergunta ao usuario se deseja realmente excluir um registro
     */

    function onDelete($param) {
        // obtem o parametro $key
        $key = $param['key'];

        // define duas acoes
        $action1 = new TAction(array($this, 'Delete'));

        // define os parametros de cada acao
        $action1->setParameter('key', $key);

        //encaminha a chave estrangeira
        $action1->setParameter('fk', filter_input(INPUT_GET, 'fk'));
        // exibe um dialogo ao usuario
        new TQuestion('Deseja realmente excluir o registro ?', $action1);
    }

    /*
     * metodo Delete()
     * Exclui um registro
     */

    function Delete($param) {
        // obtem o parametro $key
        $key = $param['key'];
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instanicia objeto Record
        $cadastro = new AverbacaoRecord($key);

        try {
            // deleta objeto do banco de dados
            $cadastro->delete();

            // finaliza a transacao
            TTransaction::close();
        } catch (Exception $e) { // em caso de exceção
            // exibe a mensagem gerada pela exceção
            new TMessage('error', $e->getMessage());
            // desfaz todas alterações no banco de dados
            TTransaction::rollback();
            $this->form->setData($cadastro);   // fill the form with the active record data
        }

        // re-carrega a datagrid
        $this->onReload();
        // exibe mensagem de sucesso
    }

    /*
     * metodo show()
     * Exibe a pagina
     */

    function show() {
        // carrega os dados no datagrid
        $this->onReload();
        //chama o metodo show da super classe
        parent::show();
    }

    /*
     * metodo onSave()
     * Executada quando o usuario clicar no botao salvar do formulario
     */

    function onSave() {
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // obtem os dados no formulario em um objeto Record da Classe
        $cadastro = $this->form->getData('AverbacaoRecord');

        //antes de armazenar verifica se algum campo eh requerido e nao foi informado
        $dados = $cadastro->toArray();

        $msg = '';
        $icone = 'info';

        if (empty($dados['orgao_id'])) {
            $msg .= 'O orgao deve ser informado. ';
        }


        if (empty($dados['datainicio'])) {
            $msg .= 'A data de inicio deve ser informada. ';
        }


        if (empty($dados['datafim'])) {
            $msg .= 'A data de fim deve ser informada. ';
        }

        try {

            if ($msg == '') {
                // armazena o objeto
                $cadastro->store();
                $msg = 'Dados armazenados com sucesso';

                // finaliza a transacao
                TTransaction::close();
            } else {
                $icone = 'error';
                $this->form->setData($cadastro);   // fill the form with the active record data
            }

            if ($icone == 'error') {
                // exibe mensagem de erro
                new TMessage($icone, $msg);
                $this->form->setData($cadastro);   // fill the form with the active record data
            } else {

                // define array acoes
                $param = array();
                $param['fk'] = $dados['servidor_id'];

                // exibe um dialogo ao usuario
                new TMessage("info", "Registro salvo com sucesso!");
                TApplication::gotoPage('AverbacaoDetalhe', 'onReload', $param); // reload
            }
        } catch (Exception $e) { // em caso de exc
            // exibe a mensagem gerada pela exceção
            new TMessage('error', $e->getMessage());
            // desfaz todas alterações no banco de dados
            TTransaction::rollback();
            $this->form->setData($cadastro);   // fill the form with the active record data
        }
    }

    /*
     * metodo onEdit()
     * Edita os dados de um registro
     */

    function onEdit($param) {
        try {
            if (isset($param['key'])) {

                // get the parameter $key
                $key = $param['key'];

                TTransaction::open('pg_ceres');   // open a transaction with database 'samples'

                $object = new AverbacaoRecord($key);        // instantiates object City
                //converter datas do form para formato Brasileiro
                $object->datainicio = TDate::date2br($object->datainicio);
                $object->datafim = TDate::date2br($object->datafim);

//                TEntry::enableField('form_averbacao', 'numerodias');

                $this->form->setData($object);   // fill the form with the active record data

                TTransaction::close();           // close the transaction
            } else {
                $this->form->clear();
            }
        } catch (Exception $e) { // in case of exception
            // shows the exception error message
            new TMessage('error', '<b>Error</b> ' . $e->getMessage());
            // undo all pending operations
            TTransaction::rollback();
        }
    }

}

?>