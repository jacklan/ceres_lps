<?php

//ini_set('display_errors', 1);
//ini_set('display_startup_erros', 1);
//error_reporting(E_ALL);

use Adianti\Widget\Datagrid\TDatagridTables;

class ServidorQuantitativoSituacaoList extends TPage {

    private $form;     // formulario de cadastro
    private $datagrid; // listagem

    /*
     * metodo construtor
     * Cria a pagina, o formulario e a listagem
     */

    public function __construct() {
        parent::__construct();

        // instancia um formulario
        $this->form = new TForm('form_busca_Servidor_Quantitativo_Situacao');

        // instancia uma tabela
        $panel = new TPanelForm(900, 100);

        // adiciona a tabela ao formulario
        $this->form->add($panel);

        // cria um r�tulo para o t�tulo
        $titulo = new TLabel('Listagem Servidor Quantitativo Situação');
        $titulo->setFontFace('Arial');
        $titulo->setFontColor('red');
        $titulo->setFontSize(12);

        // adiciona o campo Titulo
        $panel->put($titulo, $panel->getColuna(), $panel->getLinha());

        // instancia objeto DataGrid
        $this->datagrid = new TDatagridTables;

        // instancia as colunas da DataGrid
        $dgnome_regional = new TDataGridColumn('nome_regional', 'Regional', 'center', 80);
        $dgmunicipio = new TDataGridColumn('municipio', 'Municipio', 'left', 300);
        $dgefetivos = new TDataGridColumn('link_efetivos', 'Efetivo', 'left', 50);
        $dgbolsistas = new TDataGridColumn('link_bolsistas', 'Bolsistas', 'left', 50);
        $dgadisposicao = new TDataGridColumn('link_adisposicao', 'A Disposição', 'center', 50);
        $dgcedido = new TDataGridColumn('link_cedido', 'Cedido', 'left', 50);
        $dgconveniado = new TDataGridColumn('link_conveniado', 'Conveniado', 'left', 50);
        $dgoutros = new TDataGridColumn('link_outros', 'Outros', 'left', 50);
        $dgsomaefetivosbolsistas = new TDataGridColumn('soma_efetivos_bolsistas', 'Soma Efetivos e Bolsistas', 'center', 50);
        $dgsomaetotal = new TDataGridColumn('soma_total', 'Soma Total', 'center', 50);

        // adiciona as colunas a DataGrid
        $this->datagrid->addColumn($dgnome_regional);
        $this->datagrid->addColumn($dgmunicipio);
        $this->datagrid->addColumn($dgefetivos);
        $this->datagrid->addColumn($dgbolsistas);
        $this->datagrid->addColumn($dgadisposicao);
        $this->datagrid->addColumn($dgcedido);
        $this->datagrid->addColumn($dgconveniado);
        $this->datagrid->addColumn($dgoutros);
        $this->datagrid->addColumn($dgsomaefetivosbolsistas);
        $this->datagrid->addColumn($dgsomaetotal);

        // cria o modelo da DataGrid, montando sua estrutura
        $this->datagrid->createModel();

        // monta a paina atraves de uma tabela
        $panel = new TPanelForm(900, 500);
        $panel->put($this->form, 0, 0);
        $panel->put($this->datagrid, 150, 100);

        // adiciona a tabela a pagina
        parent::add($panel);
    }

    /*
     * metodo onReload()
     * Carrega a DataGrid com os objetos do banco de dados
     */

    function onReload() {
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instancia um repositorio para Carros
        $repository = new TRepository('vw_quadro_sit_pessoalRecord');

        //obtem os dados do formulario de busca
        $campo = $this->form->getFieldData('opcao');
        $dados = $this->form->getFieldData('nome');

        // cria um criterio de selecao, ordenado pelo id
        $criteria = new TCriteria;
        $criteria->add(new TFilter('empresa_id', '=', $_SESSION['empresa_id']));
        $criteria->setProperty('order', "municipio");

        // carrega os objetos de acordo com o criterio
        $cadastros = $repository->load($criteria);

        $this->datagrid->clear();
        if ($cadastros) {
            // percorre os objetos retornados
            foreach ($cadastros as $cadastro) {
                // adiciona o objeto na DataGrid
                $this->datagrid->addItem($cadastro);
            }
        }
        // finaliza a transacao
        TTransaction::close();
        $this->loaded = true;
    }

    /*
     * metodo show()
     * Exibe a pagina
     */

    function show() {
        // carrega os dados no datagrid
        $this->onReload();
        //chama o metodo show da super classe
        parent::show();
    }

}

?>