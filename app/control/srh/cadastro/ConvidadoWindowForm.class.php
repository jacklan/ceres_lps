<?php

/**
 * ConvidadoWindowForm
 *
 * @version    1.0
 * @package    control
 * @subpackage srh
 * @author     Jackson Meires
 * @copyright  Copyright (c) 2006-2014 Adianti Solutions Ltd. (http://www.adianti.com.br)
 * @license    http://www.adianti.com.br/framework-license
 */
class ConvidadoWindowForm extends TWindow {

    private $form;

    /**
     * Class constructor
     * Creates the page
     */
    function __construct() {
        parent::__construct();
        parent::setSize(570, 350);

        // create the form using TQuickForm class
        $this->form = new TQuickForm;
        // $this->form->class = 'tform';
        $this->form->setFormTitle('<font color=red><b>Convidado</b></font>');
        $this->form->style = 'width: 500px';

        // create the form fields
        $code = new THidden('id');
        $nome = new TEntry('nome');
        $cpf = new TEntry('cpf');
        $email = new TEntry('email');
        $telefone = new TEntry('telefone');

        $nome->setProperty('style', 'text-transform: uppercase');

        // add the fields inside the form
        $this->form->addQuickField('Codigo', $code, 0);
        $this->form->addQuickField('Nome <font color=red><b>*</b></font>', $nome, 80);
        $this->form->addQuickField('CPF <font color=red><b>*</b></font>', $cpf, 80);
        $this->form->addQuickField('E-mail', $email, 80);
        $this->form->addQuickField('Telefone', $telefone, 80);
        $this->form->addQuickField(null, new Adianti\Widget\Form\TLabel("<font color=red><b>Campo Obrigatório *</b></font>"), 300);

        $action = new TAction(array($this, 'onSave'));
        $action->setParameter('fk', filter_input(INPUT_GET, 'fk'));

        // define the form action 
        $this->form->addQuickAction('Salvar', $action, 'ico_save.png')->class = 'btn btn-info';

        parent::add($this->form);
    }

    public function onSave() {
        try {
            // inicia transacao com o banco 'pg_ceres'
            TTransaction::open('pg_ceres');
            // obtem os dados no formulario em um objeto Record da Classe
            $object = $this->form->getData('ConvidadoRecord');

            $object->usuarioalteracao = $_SESSION['usuario'];
            $object->dataalteracao = date("d/m/Y H:i:s");

            //antes de armazenar verifica se algum campo eh requerido e nao foi informado
            //$dados = $object->toArray();

            $msg = '';
            $icone = 'info';

            if ($msg == '') {
                // armazena o objeto
                $object->store();
                $msg = 'Dados armazenados com sucesso';

                // finaliza a transacao
                TTransaction::close();
            } else {
                $icone = 'error';
            }

            if ($icone == 'error') {
                // exibe mensagem de erro
                new TMessage($icone, $msg);
                $this->form->setData($object);   // fill the form with the active record data
            } else {

//                $action = new TAction(array('CertificadoServidorFormList', 'onReload'));
//                $action->setParameter('fk', filter_input(INPUT_GET, 'fk'));

                \Adianti\Widget\Form\TForm::sendData('form_certificado_participante', $object);
                parent::closeWindow(); // closes the window
                // exibe um dialogo ao usuario
//                new TMessage("info", "Registro salvo com sucesso!", $action);
            }
        } catch (Exception $e) { // em caso de exc
            // exibe a mensagem gerada pela exceção
            new TMessage('error', $e->getMessage());
            // desfaz todas alterações no banco de dados
            TTransaction::rollback();
            $this->form->setData($this->form->getData());   // fill the form with the active record data
        }
    }

    public function onLoad() {
        
    }

}
