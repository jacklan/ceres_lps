<?php

/*
 * classe UnidadeServidorDetalhe
 * Cadastro de UnidadeServidor: Contem a listagem e o formulario de busca
 */

use Adianti\Database\TFilter1;
use Adianti\Widget\Datagrid\TDatagridTables;

class UnidadeServidorDetalhe extends TPage {

    private $form;     // formulario de cadastro
    private $datagrid; // listagem

    /*
     * metodo construtor
     * Cria a pagina, o formulario e a listagem
     */

    public function __construct() {
        parent::__construct();

        // instancia um formulario
        $this->form = new TQuickForm;
        $this->form->class = 'form_UnidadeServidor';
        $this->form->setFormTitle('<font color="red" size="3" face="Arial"><b>Detalhe da Unidade do Servidor</b></font>');

        // cria um rotulo para o titulo
        $titulo = new TLabel('<div style="position:floatval; width: 200px;"> <b>* Campo obrigatorio</b></div>');
        $titulo->setFontFace('Arial');
        $titulo->setFontColor('red');
        $titulo->setFontSize(10);

        // cria os campos do formulario
        $nomeunidade = new TLabel();
        $codigo = new THidden('id');
        $servidor_id = new TCombo('servidor_id');
        $setor_id = new TCombo('setor_id');
        $datainicio = new TDate('datainicio');
        $datafim = new TDate('datafim');
        $documento = new TEntry('documento');
        $usuarioalteracao = new THidden('usuarioalteracao');
        $dataalteracao = new THidden('dataalteracao');

        //cria a colecao da tabela estrangeira
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $repository = new TRepository('ServidorRecord');
        // cria um criterio de selecao, ordenado pelo id
        $criteria = new TCriteria;
        $criteria->add(new TFilter('empresa_id', '=', $_SESSION['empresa_id']) );
        $criteria->setProperty('order', 'nome');
        // carrega os objetos de acordo com o criterio
        $cadastros = $repository->load($criteria);

        //adiciona os objetos no combo
        foreach ($cadastros as $object) {
            $items[$object->id] = $object->nome;
        }

        // adiciona as opcoes na combo
        $servidor_id->addItems($items);

        // finaliza a transacao
        TTransaction::close();

        //cria a colecao da tabela estrangeira
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $repository = new TRepository('SetorRecord');
        // cria um criterio de selecao, ordenado pelo id
        $criteria = new TCriteria;
        $criteria->add(new TFilter('empresa_id', '=', $_SESSION['empresa_id']) );
        $criteria->setProperty('order', 'nome');
        // carrega os objetos de acordo com o criterio
        $cadastros = $repository->load($criteria);

        //adiciona os objetos no combo
        foreach ($cadastros as $object) {
            $items1[$object->id] = $object->nome;
        }

        // adiciona as opcoes na combo
        $setor_id->addItems($items1);

        // finaliza a transacao
        TTransaction::close();

        TTransaction::open('pg_ceres');
        $cadastro = new UnidadeOperativaRecord($_GET['fk']);
        if ($cadastro) {
            $nome2 = $cadastro->nome;
        }

        $nomeunidade->setValue($nome2);
        // finaliza a transacao
        TTransaction::close();

        //coloca o valor do relacionamento
        $unidadeoperativa_id = new THidden('unidadeoperativa_id');
        $unidadeoperativa_id->setValue($_GET['fk']);

        // define os campos
        $this->form->addQuickField(null, $unidadeoperativa_id, 1);
        $this->form->addQuickField(null, $codigo, 10);
        $this->form->addQuickField(null, $usuarioalteracao, 10);
        $this->form->addQuickField(null, $dataalteracao, 10);
        $this->form->addQuickField('Unidade', $nomeunidade, 200);
        $this->form->addQuickField('Servidor <font color=red><b>*</b></font>', $servidor_id, 40);
        $this->form->addQuickField('Setor <font color=red><b>*</b></font>', $setor_id, 40);
        $this->form->addQuickField('Data In&iacute;cio', $datainicio, 40);
        $this->form->addQuickField('Data Fim', $datafim, 40);
        $this->form->addQuickField('Documento', $documento, 40);
        $this->form->addQuickField(null, $titulo, 50);

        // define a acao do botao
        $action1 = new TAction(array($this, 'onSave'));
        $action1->setParameter('fk', $_GET['fk']);

        // cria um botao de acao
        $this->form->addQuickAction('Salvar', $action1, 'ico_save.png')->class = 'btn btn-info';
        $this->form->addQuickAction('Voltar', new TAction(array('UnidadeOperativaList', 'onReload')), 'ico_datagrid.gif');

        // instancia objeto DataGrid
        $this->datagrid = new TDatagridTables;

        // instancia as colunas da DataGrid
        $dgservidor = new TDataGridColumn('nome_servidor', 'Servidor', 'left', 200);
        $dgsetor = new TDataGridColumn('nome_setor', 'Setor', 'left', 100);
        $dgunidadeoperativa = new TDataGridColumn('nome_unidadeoperativa', 'Unidade Operativa', 'left', 250);
        $dgdatainicio = new TDataGridColumn('datainicio', 'Data In&iacutecio', 'left', 80);
        $dgdatafim = new TDataGridColumn('datafim', 'Data Fim', 'left', 90);
        $dgdocumento = new TDataGridColumn('documento', 'Documento', 'left', 100);

        // adiciona as colunas a DataGrid
        $this->datagrid->addColumn($dgservidor);
        $this->datagrid->addColumn($dgsetor);
        $this->datagrid->addColumn($dgunidadeoperativa);
        $this->datagrid->addColumn($dgdatainicio);
        $this->datagrid->addColumn($dgdatafim);
        $this->datagrid->addColumn($dgdocumento);

        // instancia duas acoes da DataGrid
        $action1 = new TDataGridAction(array($this, 'onEdit'));
        $action1->setLabel('Editar');
        $action1->setImage('ico_edit.png');
        $action1->setField('id');
        $action1->setFk('unidadeoperativa_id');

        $action2 = new TDataGridAction(array($this, 'onDelete'));
        $action2->setLabel('Deletar');
        $action2->setImage('ico_delete.png');
        $action2->setField('id');
        $action2->setFk('unidadeoperativa_id');

        // adiciona as acoes a DataGrid
        $this->datagrid->addAction($action1);
        $this->datagrid->addAction($action2);

        // cria o modelo da DataGrid, montando sua estrutura
        $this->datagrid->createModel();

        // monta a paina atraves de uma tabela
        $panel = new TPanelForm(700, 500);
        $panel->put($this->form, 0, 0);
        $panel->put($this->datagrid, 150, 250);

        // adiciona a tabela a pagina
        parent::add($panel);
    }

    /*
     * metodo onReload()
     * Carrega a DataGrid com os objetos do banco de dados
     */

    function onReload() {
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instancia um repositorio da Classe
        $repository = new TRepository('UnidadeServidorRecord');

        // cria um criterio de selecao
        $criteria = new TCriteria;


        //filtra pelo campo selecionado pelo usuario
        $criteria->add(new TFilter('unidadeoperativa_id', '=', $_GET['fk']));

        $criteria->setProperty('order', $campo);


        // carrega os objetos de acordo com o criterio
        $cadastros = $repository->load($criteria);

        $this->datagrid->clear();
        if ($cadastros) {
            // percorre os objetos retornados
            foreach ($cadastros as $cadastro) {

                //converter datas do form para formato Brasileiro
                $cadastro->datainicio = TDate::date2br($cadastro->datainicio);
                $cadastro->datafim = TDate::date2br($cadastro->datafim);

                // adiciona o objeto na DataGrid
                $this->datagrid->addItem($cadastro);
            }
        }


        // finaliza a transacao
        TTransaction::close();
        $this->loaded = true;
    }

    /*
     * metodo onDelete()
     * Executada quando o usuario clicar no botao excluir da datagrid
     * Pergunta ao usuario se deseja realmente excluir um registro
     */

    function onDelete($param) {

        // obtem o parametro $key
        $key = $param['key'];

        // define duas acoes
        $action1 = new TAction(array($this, 'Delete'));

        // define os parametros de cada acao
        $action1->setParameter('key', $key);

        //encaminha a chave estrangeira
        $action1->setParameter('fk', filter_input(INPUT_GET, 'fk'));

        // exibe um dialogo ao usuario
        new TQuestion('Deseja realmente excluir o registro ?', $action1);
    }

    /*
     * metodo Delete()
     * Exclui um registro
     */

    function Delete($param) {
        // obtem o parametro $key
        $key = $param['key'];
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instanicia objeto Record
        $cadastro = new UnidadeServidorRecord($key);

        try {
            // deleta objeto do banco de dados
            $cadastro->delete();

            // exibe um dialogo ao usuario
            new TMessage("info", "Registro deletado com sucesso!");

            // finaliza a transacao
            TTransaction::close();
        } catch (Exception $e) {

            // em caso de exceção
            // exibe a mensagem gerada pela exceção
            new TMessage('error', $e->getMessage());

            // desfaz todas alterações no banco de dados
            TTransaction::rollback();
        }

        // re-carrega a datagrid
        $this->onReload();
    }

    /*
     * metodo show()
     * Exibe a pagina
     */

    function show() {
        // carrega os dados no datagrid
        $this->onReload();
        //chama o metodo show da super classe
        parent::show();
    }

    /*
     * metodo onSave()
     * Executada quando o usuario clicar no botao salvar do formulario
     */

    function onSave() {
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // obtem os dados no formulario em um objeto Record da Classe
        $cadastro = $this->form->getData('UnidadeServidorRecord');

        //antes de armazenar verifica se algum campo eh requerido e nao foi informado
        $dados = $cadastro->toArray();

        $msg = '';
        $icone = 'info';

        if (empty($dados['servidor_id'])) {
            $msg .= 'O Servidor deve ser informado.<br/>';
        }

        if (empty($dados['setor_id'])) {
            $msg .= 'O Setor deve ser informado.<br/>';
        }

        if (empty($dados['unidadeoperativa_id'])) {
            $msg .= 'A Unidade Operativa deve ser informada.<br/>';
        }

        try {

            if ($msg == '') {
                // armazena o objeto
                $cadastro->store();
                $msg = 'Dados armazenados com sucesso';

                // finaliza a transacao
                TTransaction::close();
            } else {
                $icone = 'error';
            }

            if ($icone == 'error') {
                // exibe mensagem de erro
                new TMessage($icone, $msg);

                $this->form->setData($cadastro);   // fill the form with the active record data
            } else {
                //chama o formulario com o grid
                $param = array();
                $param['fk'] = $dados['unidadeoperativa_id'];

                new TMessage("info", "Registro salvo com sucesso!");
                TApplication::gotoPage('UnidadeServidorDetalhe', 'onReload', $param);
            }
        } catch (Exception $e) { // em caso de exc
            // exibe a mensagem gerada pela exceção
            new TMessage('error', $e->getMessage());
            // desfaz todas alterações no banco de dados
            TTransaction::rollback();

            $this->form->setData($cadastro);   // fill the form with the active record data
        }
    }

    /*
     * metodo onEdit()
     * Edita os dados de um registro
     */

    function onEdit($param) {

        try {
            if (isset($param['key'])) {

                // get the parameter $key
                $key = $param['key'];

                TTransaction::open('pg_ceres');   // open a transaction with database 'samples'

                $object = new UnidadeServidorRecord($key);        // instantiates object City
                //converter datas do form para formato Brasileiro
                $object->datainicio = TDate::date2br($object->datainicio);
                $object->datafim = TDate::date2br($object->datafim);

                $this->form->setData($object);   // fill the form with the active record data

                TTransaction::close();           // close the transaction
            } else {
                $this->form->clear();
            }
        } catch (Exception $e) { // in case of exception
            // shows the exception error message
            new TMessage('error', '<b>Error</b> ' . $e->getMessage());
            // undo all pending operations
            TTransaction::rollback();
        }
    }

}

?>