<?php

//ini_set('display_errors', 1);
//ini_set('display_startup_erros', 1);
//error_reporting(E_ALL);

/**
 * ServidorForm
 *
 * @version    2.0
 * @package    ceres
 * @subpackage app/control
 * @author     Jackson Meires
 * @copyright  Copyright (c) 2006-2014 Adianti Solutions Ltd. (http://www.adianti.com.br)
 * @license    http://www.adianti.com.br/framework-license
 */
class ServidorFormConsulta extends TPage {

    private $form; // form

    /**
     * Class constructor
     * Creates the page and the registration form
     */

    function __construct() {
        parent::__construct();

        // creates the form
        $this->form = new TForm('form_Servidor');
//        $this->form->class = 'tform'; // CSS class
//        $this->form->style = 'width: 500px';
        // creates the containers for each notebook page
        $page1 = new TTable;
        $page1->width = '100%';

        $page2 = new TTable;
        $page2->width = '100%';

        $page3 = new TTable;
        $page3->width = '100%';

        $page4 = new TTable;
        $page4->width = '100%';

        $page5 = new TTable;
        $page5->width = '100%';

        $page6 = new TTable;
        $page6->width = '100%';

        // creates a table
        $tb_btn_header = new TTable;
        $tb_btn_footer = new TTable;

        // creates the notebook
        $notebook = new TNotebook();
        //   $notebook->class = 'tform'; // CSS class
        //   $notebook->style = 'width: 100%';
        // add the notebook inside the form
        $this->form->add($notebook);

        // adds two pages in the notebook
        $notebook->appendPage('Dados Pessoais', $page1);
        $notebook->appendPage('Endere&ccedil;o', $page2);
        $notebook->appendPage('Documentos', $page3);
        $notebook->appendPage('Cargo', $page4);
        $notebook->appendPage('Dados Fisicos', $page5);
        $notebook->appendPage('Outros', $page6);

        // creates a label with the title
        $titulo = new TLabel('Formulario Servidor');
        $titulo->setFontSize(18);
        $titulo->setFontFace('Arial');
        $titulo->setFontColor('red');

        // add the table inside the form
        // $this->form->add($table);
        // create the form fields
        $codigo = new THidden('id');
        //$codigo = new TEntry('id');
        //  $codigo->setEditable(false);
        $nome = new TEntry('nome');
        $nomecracha = new TEntry('nomecracha');
        $cpf = new TEntry('cpf');
        $matricula = new TEntry('matricula');
        $endereco = new TEntry('endereco');
        $endereco2 = new TEntry('endereco2');
        $bairro = new TEntry('bairro');
        $cidade = new TEntry('cidade');
        $uf = new TCombo('uf');
        $cep = new TEntry('cep');
        $cnh = new TEntry('cnh');
        $telefone = new TEntry('telefone');
        $celular = new TEntry('celular');
        $fax = new TEntry('fax');
        $email = new TEntry('email');
        $dataadmissao = new TDate('dataadmissao');
        $datanascimento = new TDate('datanascimento');
        $sexo = new TCombo('sexo');
        $estadocivil = new TCombo('estadocivil');
        $naturalidade = new TEntry('naturalidade');
        $ufnaturalidade = new TCombo('ufnaturalidade');
        $clt = new TEntry('carteiratrabalho');
        $pispasep = new TEntry('pispasep');
        $datafgts = new TDate('dataorgaofgts');
        $rg = new TEntry('rg');
        $orgaorg = new TEntry('orgaorg');
        $numconselho = new TEntry('numconselho');
        $conselho_id = new TCombo('conselho_id');
        $cargo_id = new TCombo('cargo_id');
        $cargo_id->setEditable(false);
        $cargonovo_id = new TCombo('cargonovo_id');
        $redistribuido = new TCombo('redistribuido');
        $dataredistribuicao = new TDate('dataredistribuicao');
        $documentoredistribuicao = new TEntry('documentoredistribuicao');
        $relotado = new TCombo('relotado');
        $datarelotado = new TDate('datarelotado');
        $documentorelotado = new TEntry('documentorelotado');
        $tipofuncionario = new TCombo('tipofuncionario');
        $situacao = new TCombo('situacao');
        $cedidoorgao = new TEntry('cedidoorgao');
        $dataaposentadoria = new TDate('dataaposentadoria');
        $datacalcaposentadoria = new TDate('datacalcaposentadoria');
        $documentosaida = new TDate('documentosaida');
        $quadrosuplementar = new TCombo('quadrosuplementar');
        $dataquadrosuplementar = new TDate('dataquadrosuplementar');
        $documentoquadrosuplementar = new TEntry('documentoquadrosuplementar');
        //  $datasituacao  = new TDate('datasituacao');
        $nomepai = new TEntry('nomepai');
        $nomemae = new TEntry('nomemae');
        $nacionalidade = new TEntry('nacionalidade');
        $deficienteauditivo = new TCombo('deficienteauditivo');
        $deficientevisual = new TCombo('deficientevisual');
        $deficientefisico = new TCombo('deficientefisico');
        $cor = new TCombo('cor');
        $altura = new TEntry('altura');
        $peso = new TEntry('peso');
        $nivelsalarial_id = new TCombo('nivelsalarial_id');
        $requisitocargo = new TCombo('requisitocargo_id');
        $nivelsalarialantigo_id = new TCombo('nivelsalarialantigo_id');
        $jornada_id = new TCombo('jornada_id');
        $inf_complementares = new TText('inf_complementares');
        $nivelsalarialantigo_id->setEditable(false);

        $usuarioalteracao = new THidden('usuarioalteracao');
        $dataalteracao = new THidden('dataalteracao');

        //cria a colecao da tabela estrangeira
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $repository = new TRepository('ConselhoRecord');
        // cria um criterio de selecao, ordenado pelo id
        $criteria = new TCriteria;
        $criteria->setProperty('order', 'sigla');
        // carrega os objetos de acordo com o criterio
        $cadastros = $repository->load($criteria);

        //adiciona os objetos no combo
        foreach ($cadastros as $object) {
            $items[$object->id] = $object->sigla . "/" . $object->uf;
        }
        // adiciona as opcoes na combo
        $conselho_id->addItems($items);

        // finaliza a transacao
        TTransaction::close();


        //cria a colecao da tabela estrangeira
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $repository = new TRepository('RequisitoCargoRecord');
        // cria um criterio de selecao, ordenado pelo id
        $criteria = new TCriteria;
        $criteria->setProperty('order', 'nome');
        // carrega os objetos de acordo com o criterio
        $cadastros = $repository->load($criteria);

        //adiciona os objetos no combo
        foreach ($cadastros as $object) {
            $items9[$object->id] = $object->nome;
        }
        // adiciona as opcoes na combo
        $requisitocargo->addItems($items9);
        // finaliza a transacao
        TTransaction::close();

        $itemsCargo = array();
        //cria a colecao da tabela estrangeira
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $repository = new TRepository('CargoRecord');
        // cria um criterio de selecao, ordenado pelo id
        $criteria = new TCriteria;
        $criteria->setProperty('order', 'nome');
        // carrega os objetos de acordo com o criterio
        $cadastros = $repository->load($criteria);

        //adiciona os objetos no combo
        foreach ($cadastros as $object) {
            $itemsCargo[$object->id] = $object->nome;
        }
        // adiciona as opcoes na combo
        $cargo_id->addItems($itemsCargo);
        $cargonovo_id->addItems($itemsCargo);

        // finaliza a transacao
        TTransaction::close();

        //cria a colecao da tabela estrangeira
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $repository = new TRepository('NivelSalarialRecord');
        // cria um criterio de selecao, ordenado pelo id
        $criteria = new TCriteria;
        $criteria->setProperty('order', 'nome');
        // carrega os objetos de acordo com o criterio
        $cadastros = $repository->load($criteria);

        //adiciona os objetos no combo
        foreach ($cadastros as $object) {
            $items1[$object->id] = $object->nome;
        }
        // adiciona as opcoes na combo
        $nivelsalarial_id->addItems($items1);
        // adiciona as opcoes na combo
        $nivelsalarialantigo_id->addItems($items1);

        // finaliza a transacao
        TTransaction::close();

        $itemsj2 = array();
        //cria a colecao da tabela estrangeira
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $repository1 = new TRepository('JornadaRecord');
        // cria um criterio de selecao, ordenado pelo id
        $criteria1 = new TCriteria;
        $criteria1->setProperty('order', 'entrada1');
        // carrega os objetos de acordo com o criterio
        $cadastros1 = $repository1->load($criteria1);

        //adiciona os objetos no combo
        foreach ($cadastros1 as $object1) {

            //condicao ? ver : false;
            $textojornada = substr($object1->entrada1, 0, 5) . " á " . substr($object1->saida1, 0, 5);

            if ($object1->entrada2 != '' && $object1->saida2 != '') {

                $textojornada += $object1->entrada2 . " á " . $object1->saida2;
            }

            if ($object1->entrada3 != '' && $object1->saida3 != '') {

                $textojornada += $object1->entrada3 . " á " . $object1->saida3;
            }

            $itemsj2[$object1->id] = $textojornada;
        }
        // adiciona as opcoes na combo
        $jornada_id->addItems($itemsj2);

        // finaliza a transacao
        TTransaction::close();

        // cria um vetor com as opcoes da combo estado
        $items = array();
        $items['AC'] = 'AC';
        $items['AL'] = 'AL';
        $items['AM'] = 'AM';
        $items['AP'] = 'AP';
        $items['BA'] = 'BA';
        $items['CE'] = 'CE';
        $items['DF'] = 'DF';
        $items['ES'] = 'ES';
        $items['GO'] = 'GO';
        $items['MA'] = 'MA';
        $items['MG'] = 'MG';
        $items['MS'] = 'MS';
        $items['MT'] = 'MT';
        $items['PA'] = 'PA';
        $items['PB'] = 'PB';
        $items['PE'] = 'PE';
        $items['PI'] = 'PI';
        $items['PR'] = 'PR';
        $items['RJ'] = 'RJ';
        $items['RN'] = 'RN';
        $items['RO'] = 'RO';
        $items['RR'] = 'RR';
        $items['RS'] = 'RS';
        $items['SC'] = 'SC';
        $items['SE'] = 'SE';
        $items['SP'] = 'SP';
        $items['TO'] = 'TO';

        // adiciona as opcoes na combo
        $uf->addItems($items);
        //coloca o valor padrao no combo
        $uf->setValue('RN');


        //adiciona as opcoes na combo
        $ufnaturalidade->addItems($items);
        //coloca o valor padrao no combo
        $ufnaturalidade->setValue('RN');

        // cria um vetor com as opcoes da combo cor
        $items = array();
        $items['BRANCA'] = 'BRANCA';
        $items['NEGRA'] = 'NEGRA';
        $items['PARDA'] = 'PARDA';
        $items['BRANCA'] = 'BRANCA';
        $items['INDIGENA'] = 'INDIGENA';

        // adiciona as opcoes na combo
        $cor->addItems($items);
        //coloca o valor padrao no combo
        $cor->setValue('');

        // cria um vetor com as opcoes da combo redistribuido
        $items = array();
        $items['SIM'] = 'SIM';
        $items['NAO'] = 'NAO';

        // adiciona as opcoes na combo
        $quadrosuplementar->addItems($items);
        //coloca o valor padrao no combo
        $quadrosuplementar->setValue('NAO');

        // cria um vetor com as opcoes da combo redistribuido
        $items = array();
        $items['SIM'] = 'SIM';
        $items['NAO'] = 'NAO';

        // adiciona as opcoes na combo
        $redistribuido->addItems($items);
        //coloca o valor padrao no combo
        $redistribuido->setValue('NAO');

        // cria um vetor com as opcoes da combo relotado
        $items = array();
        $items['SIM'] = 'SIM';
        $items['NAO'] = 'NAO';

        // adiciona as opcoes na combo
        $relotado->addItems($items);
        //coloca o valor padrao no combo
        $relotado->setValue('NAO');

        // cria um vetor com as opcoes da combo DEFICIENTE AUDITIVO
        $items = array();
        $items['SIM'] = 'SIM';
        $items['NAO'] = 'NAO';

        // adiciona as opcoes na combo
        $deficienteauditivo->addItems($items);
        //coloca o valor padrao no combo
        $deficienteauditivo->setValue('NAO');

        // cria um vetor com as opcoes da combo DEFICIENTE VISUAL
        $items = array();
        $items['SIM'] = 'SIM';
        $items['NAO'] = 'NAO';

        // adiciona as opcoes na combo
        $deficientevisual->addItems($items);
        //coloca o valor padrao no combo
        $deficientevisual->setValue('NAO');

        // cria um vetor com as opcoes da combo DEFICIENTE FISICO
        $items = array();
        $items['SIM'] = 'SIM';
        $items['NAO'] = 'NAO';

        // adiciona as opcoes na combo
        $deficientefisico->addItems($items);
        //coloca o valor padrao no combo
        $deficientefisico->setValue('NAO');

        // cria um vetor com as opcoes da combo
        $items = array();
        $items['M'] = 'M';
        $items['F'] = 'F';

        // adiciona as opcoes na combo
        $sexo->addItems($items);
        //coloca o valor padrao no combo
        $sexo->setValue('M');

        // cria um vetor com as opcoes da combo
        $items = array();
        $items['CASADO(A)'] = 'CASADO(A)';
        $items['DESQUITADO(A)'] = 'DESQUITADO(A)';
        $items['DIVORCIADO(A)'] = 'DIVORCIADO(A)';
        $items['SEPARADO(A)'] = 'SEPARADO(A)';
        $items['SOLTEIRO(A)'] = 'SOLTEIRO(A)';
        $items['VIUVO(A)'] = 'VIUVO(A)';

        // adiciona as opcoes na combo
        $estadocivil->addItems($items);
        //coloca o valor padrao no combo
        $estadocivil->setValue('CASADO(A)');

        // cria um vetor com as opcoes da combo
        $items = array();
        $items['BOLSISTA'] = 'BOLSISTA';
        $items['COMISSIONADO'] = 'COMISSIONADO';
        $items['ESTAGIARIO'] = 'ESTAGIARIO';
        $items['EFETIVO'] = 'EFETIVO';
        $items['TERCEIRIZADO'] = 'TERCEIRIZADO';
        $items['CONVENIADO'] = 'CONVENIADO';

        // adiciona as opcoes na combo
        $tipofuncionario->addItems($items);
        //coloca o valor padrao no combo
        $tipofuncionario->setValue('FUNCIONARIO');

        // cria um vetor com as opcoes da combo
        $items = array();
        $items['A DISPOSICAO'] = 'CEDIDO A EMATER';
        $items['APOSENTADO(A)'] = 'APOSENTADO(A)';
        $items['CEDIDO(A)'] = 'CEDIDO A OUTROS ORGÃOS';
        $items['DEVOLVIDO(A)'] = 'DEVOLVIDO(A)';
        $items['EM ATIVIDADE'] = 'EM ATIVIDADE';
        $items['EM VACANCIA'] = 'EM VACANCIA';
        $items['EXONERADO(A)'] = 'EXONERADO(A)';
        $items['FALECIDO(A)'] = 'FALECIDO(A)';
        $items['LICENCIADO(A)'] = 'LICENCIADO(A)';
        $items['RESCINDIDO(A)'] = 'RESCINDIDO(A)';

        // adiciona as opcoes na combo
        $situacao->addItems($items);
        //coloca o valor padrao no combo
        $situacao->setValue('EM ATIVIDADE');
        ############ Fim Combos  ############
        // define os tamanhos dos campos
        ######## Dados Pessoais ########
        $matricula->setSize(70);
        $nome->setSize(180);
        $nome->setProperty('style', 'text-transform: uppercase');
        $nomecracha->setSize(150);
        $nomecracha->setProperty('style', 'text-transform: uppercase');
        $cpf->setSize(60);
        $datanascimento->setSize(50);
        $naturalidade->setSize(90);
        $ufnaturalidade->setSize(35);
        $nacionalidade->setSize(130);
        $nacionalidade->setValue('BRASILEIRO');
        $email->setSize(150);
        $sexo->setSize(30);
        $estadocivil->setSize(80);
        $telefone->setSize(60);
        $celular->setSize(60);
        $fax->setSize(60);
        $nomepai->setSize(170);
        $nomemae->setSize(170);
        ######## Endereco ########
        $endereco->setSize(180);
        $endereco2->setSize(180);
        $bairro->setSize(150);
        $cidade->setSize(150);
        $uf->setSize(35);
        $cep->setSize(70);
        ######## Documentos ########
        $rg->setSize(40);
        $orgaorg->setSize(60);
        $pispasep->setSize(90);
        $datafgts->setSize(50);
        $cnh->setSize(70);
        $clt->setSize(90);
        $conselho_id->setSize(60);
        $numconselho->setSize(70);
        $requisitocargo->setSize(90);
        ######## Cargo ########
        $dataadmissao->setSize(20);
        $cargonovo_id->setSize(70);
        $nivelsalarial_id->setSize(34);
        $jornada_id->setSize(70);
        $cargo_id->setSize(90);
        $nivelsalarialantigo_id->setSize(25);
        $relotado->setSize(15);
        $documentorelotado->setSize(90);
        $datarelotado->setSize(80);
        $quadrosuplementar->setSize(15);
        $tipofuncionario->setSize(30);
        $situacao->setSize(48);
        $cedidoorgao->setSize(100);
        $dataaposentadoria->setSize(80);
        $datacalcaposentadoria->setSize(80);
        $documentosaida->setSize(90);
        $redistribuido->setSize(15);
        $dataredistribuicao->setsize(80);
        $documentoredistribuicao->setsize(90);
        $dataquadrosuplementar->setSize(80);
        $documentoquadrosuplementar->setSize(50, 40);
        // $datasituacao->setSize(80);
        ######## Dados Fisicos ########
        $cor->setSize(60);
        $altura->setSize(50);
        $peso->setSize(50);
        $deficienteauditivo->setSize(40);
        $deficientevisual->setSize(40);
        $deficientefisico->setSize(40);
        ######## Outros ########
        $inf_complementares->setSize(180);


        //Define o auto-sugerir
        $telefone->setProperty('placeholder', '(84)1234-5678');
        $cpf->setProperty('placeholder', 'EX 11122233300');
        $datanascimento->setProperty('placeholder', '01/12/2013');
        $cpf->setProperty('placeholder', 'EX 000222999');
        $cep->setProperty('placeholder', 'EX 59000100');
        $email->setProperty('placeholder', 'exemplo@exemplo.com.br');
        $inf_complementares->setProperty('placeholder', 'Descrição...');

        //campos obrigarorios
        $nome->addValidation('Nome', new TRequiredValidator); // required field
        $email->addValidation('E-mail', new TEmailValidator); // email field
        $cpf->addValidation('CPF', new TNumericValidator); // numeric field
        $peso->addValidation('Peso', new TNumericValidator); // numeric field
        //mask fields
        $cep->setMask('999999-999');
        $altura->setMask('9.99');
        $telefone->setMask('99 9999-9999');

        // add the form fields
        ################ Pagina 01 - Dados Pessoais ################
        $page1->addRowSet('', $codigo);
        $page1->addRowSet('', $usuarioalteracao);
        $page1->addRowSet('', $dataalteracao);

        // adiciona a foto
        //mostra a foto do servidor se o metodo eh onEdit
        if ($_GET['method'] == 'onEdit') {
            $foto = new TImage('app/images/servidor/servidor_' . $_GET['fk'] . '.jpg', 'foto', 150, 150);
            $foto->style = "width: 150px; height: 200px; left: 150%; position: absolute;";
            $page1->addRowSet('', $foto)->style = 'float: left;';
            // $page1->addRowSet( new TLabel('Matsricula'),     array( $matricula,  new TLabel(''), $foto) );
        }

        $page1->addRowSet(new TLabel('Matricula'), $matricula);
        $page1->addRowSet(new TLabel('Nome'), $nome);
        $page1->addRowSet(new TLabel('Nome Cracha'), $nomecracha);
        $page1->addRowSet(new TLabel('CPF'), $cpf);
        $page1->addRowSet(new TLabel('Data Nascimento'), $datanascimento);
        $page1->addRowSet(new TLabel('Naturalidade'), $naturalidade);
        $page1->addRowSet(new TLabel('UF Naturalidade'), $ufnaturalidade);
        $page1->addRowSet(new TLabel('Nascionalidade'), $nacionalidade);
        $page1->addRowSet(new TLabel('E-mail'), $email);
        $page1->addRowSet(new TLabel('Sexo'), $sexo);
        $page1->addRowSet(new TLabel('Estado Civil'), $estadocivil);
        $page1->addRowSet(new TLabel('Telefone'), $telefone);
        $page1->addRowSet(new TLabel('Celular'), $celular);
        $page1->addRowSet(new TLabel('Fax'), $fax);
        $page1->addRowSet(new TLabel('Nome Pai'), $nomepai);
        $page1->addRowSet(new TLabel('Nome Mae'), $nomemae);
        ################ Fim Pagina 01 - Dados Pessoais ################
        ################ Inicio Pagina 02 - Enredeco ################
        $page2->addRowSet(new TLabel('Endere&ccedil;o'), $endereco);
        $page2->addRowSet(new TLabel('Endere&ccedil;o Secundario'), $endereco2);
        $page2->addRowSet(new TLabel('Bairro'), $bairro);
        $page2->addRowSet(new TLabel('Cidade'), $cidade);
        $page2->addRowSet(new TLabel('UF'), $uf);
        $page2->addRowSet(new TLabel('CEP'), $cep);
        ################ Inicio Pagina 02 - Enredeco ################
        ################ Inicio Pagina 03 - Dados Documentos ################
        $page3->addRowSet(new TLabel('RG'), $rg);
        $page3->addRowSet(new TLabel('Orgao RG'), $orgaorg);
        $page3->addRowSet(new TLabel('Pis Pasep'), $pispasep);
        $page3->addRowSet(new TLabel('Data FGTS'), $datafgts);
        $page3->addRowSet(new TLabel('CNH'), $cnh);
        $page3->addRowSet(new TLabel('Carteira de Trabalho'), $clt);
        $page3->addRowSet(new TLabel('Conselho'), $conselho_id);
        $page3->addRowSet(new TLabel('Numero Conselho'), $numconselho);
        $page3->addRowSet(new TLabel('Forma&ccedil;&atilde;o'), $requisitocargo);
        ################ Fim Pagina 03 - Dados Documentos ################
        ################ Inicio Pagina 04 - Cargo ################
        $page4->addRowSet(new TLabel('Data Admissao'), $dataadmissao);
        $page4->addRowSet(new TLabel('Cargo Novo'), $cargonovo_id);
        $page4->addRowSet(new TLabel('Nivel Salarial'), $nivelsalarial_id);
        $page4->addRowSet(new TLabel('Jornada de Trabalho'), $jornada_id);
        $page4->addRowSet(new TLabel('Cargo Antigo'), $cargo_id);
        $page4->addRowSet(new TLabel('Nivel Salarial Antigo'), $nivelsalarialantigo_id);
        $page4->addRowSet(new TLabel('Relotado'), $relotado);
        $page4->addRowSet(new TLabel('Doc. Relotado'), $documentorelotado);
        $page4->addRowSet(new TLabel('Data Relotado por Periodo'), $datarelotado);
        $page4->addRowSet(new TLabel('Quadros Suplementar'), $quadrosuplementar);
        $page4->addRowSet(new TLabel('V&iacute;nculo'), $tipofuncionario);
        $page4->addRowSet(new TLabel('Situa&ccedil;&atilde;o Funcional'), $situacao);
        $page4->addRowSet(new TLabel('Cedido Org&atilde;o'), $cedidoorgao);
        $page4->addRowSet(new TLabel('Data Saida'), $dataaposentadoria);
        $page4->addRowSet(new TLabel('Data Calc. Aposent.'), $datacalcaposentadoria);
        $page4->addRowSet(new TLabel('Doc. Saida'), $documentosaida);
        $page4->addRowSet(new TLabel('Redistribui&ccedil;&atilde;o'), $redistribuido);
        $page4->addRowSet(new TLabel('Data Distribui&ccedil;&atilde;o'), $dataredistribuicao);
        $page4->addRowSet(new TLabel('Doc. Distribui&ccedil;&atilde;o'), $documentoredistribuicao);
        $page4->addRowSet(new TLabel('Data Quadro Suplementar'), $dataquadrosuplementar);
        $page4->addRowSet(new TLabel('Doc. Quadro Suplementar'), $documentoquadrosuplementar);
        ################ Fim Pagina 04 - Cargo ################
        ################ Inicio Pagina 05 - Dados Fisicos ################
        $page5->addRowSet(new TLabel('Ra&ccedil;a / Cor'), $cor);
        $page5->addRowSet(new TLabel('Altura'), $altura);
        $page5->addRowSet(new TLabel('Peso'), $peso);
        $page5->addRowSet(new TLabel('Deficiente Auditivo'), $deficienteauditivo);
        $page5->addRowSet(new TLabel('Deficiente Visual'), $deficientevisual);
        $page5->addRowSet(new TLabel('Deficiente Fisico'), $deficientefisico);
        ################ Fim Pagina 04 - Dados Fisicos ################
        ################ Inicio Pagina 05 - Outros ################
        $page6->addRowSet(new TLabel('Informacoes Complementares'), $inf_complementares);

        ################ Fim Pagina 05 - Outros ################
        #
        // create an action button (save)
//        $save_button = new TButton('save');
//        $save_button->setAction(new TAction(array($this, 'onSave')), 'Salvar');
//        $save_button->setImage('ico_save.png');
        // create an action button (new)
        $new_button = new TButton('new');
        $new_button->setAction(new TAction(array($this, 'onEdit')), 'Novo');
        $new_button->setImage('ico_new.png');

        // create an action button (go to list)
        $goto_button = new TButton('list');
        $goto_button->setAction(new TAction(array('ServidorConsultaList', 'onReload')), 'Voltar');
        $goto_button->setImage('ico_datagrid.gif');

        ################# Botoes #################
        //botoes superiores
        //  if (!empty(filter_input(INPUT_GET, 'fk'))) {
        if (!empty($_GET['fk'])) {
            ################ BTN Action DropDown 01 ################
            // create an action servidor (lotacao)
            $action_servidor_lotacao = new TAction(array('ServidorLotacaoDetalheConsulta', 'onReload'));
            $action_servidor_lotacao->setParameter('fk', '' . filter_input(INPUT_GET, 'fk') . '');
            /*
              // create an action (Disposicao)
              $action_disposicao = new TAction(array('DisposicaoDetalhe', 'onReload'));
              $action_disposicao->setParameter('fk', '' . filter_input(INPUT_GET, 'fk') . '');

              // create an action (Experiencia)
              $action_experiencia = new TAction(array('ExperienciaDetalhe', 'onReload'));
              $action_experiencia->setParameter('fk', '' . filter_input(INPUT_GET, 'fk') . '');

              // create an action (Formacao)
              $action_formacao_servidor = new TAction(array('FormacaoServidorDetalhe', 'onReload'));
              $action_formacao_servidor->setParameter('fk', '' . filter_input(INPUT_GET, 'fk') . '');

              // create an action (Cargo)
              $action_cargo = new TAction(array('CargoServidorDetalhe', 'onReload'));
              $action_cargo->setParameter('fk', '' . filter_input(INPUT_GET, 'fk') . '');

              // create an action (Habilidade)
              $action_servidor_habilidade = new TAction(array('ServidorHabilidadeDetalhe', 'onReload'));
              $action_servidor_habilidade->setParameter('fk', '' . filter_input(INPUT_GET, 'fk') . '');

              // create an action (mandatoeletivo)
              $action_mandato_eletivo = new TAction(array('MandatoEletivoDetalhe', 'onReload'));
              $action_mandato_eletivo->setParameter('fk', '' . filter_input(INPUT_GET, 'fk') . '');


              // create an action  (Concessao)
              $action_concessao = new TAction(array('ConcessaoDetalhe', 'onReload'));
              $action_concessao->setParameter('fk', '' . filter_input(INPUT_GET, 'fk') . '');

              // create an action  (LicencaParticular)
              $action_licenca_particular = new TAction(array('LicencaParticularDetalhe', 'onReload'));
              $action_licenca_particular->setParameter('fk', '' . filter_input(INPUT_GET, 'fk') . '');

              // create an action (faltaservidor)
              $action_faltas_servidor = new TAction(array('FaltaServidorDetalhe', 'onReload'));
              $action_faltas_servidor->setParameter('fk', '' . filter_input(INPUT_GET, 'fk') . '');

              ################ fim BTN Action DropDown 01 ################
              ################ Action DropDown 02 ################
              // create an action (Requisicao Servidor)
              $action_requisicao_servidor = new TAction(array('RequisicaoServidorDetalhe', 'onReload'));
              $action_requisicao_servidor->setParameter('fk', '' . filter_input(INPUT_GET, 'fk') . '');

              // create an action (Dependentes)
              $action_dependentes = new TAction(array('DependentesDetalhe', 'onReload'));
              $action_dependentes->setParameter('fk', '' . filter_input(INPUT_GET, 'fk') . '');

              // create an action (Acidente)
              $action_acidente = new TAction(array('AcidenteDetalhe', 'onReload'));
              $action_acidente->setParameter('fk', '' . filter_input(INPUT_GET, 'fk') . '');

              // create an action (Producao)
              $action_producao = new TAction(array('ProducaoDetalhe', 'onReload'));
              $action_producao->setParameter('fk', '' . filter_input(INPUT_GET, 'fk') . '');

              // create an action (Servidor Capacitacao)
              $action_capacitacao = new TAction(array('ServidorCapacitacaoDetalhe', 'onReload'));
              $action_capacitacao->setParameter('fk', '' . filter_input(INPUT_GET, 'fk') . '');

              // create an action (Solicitacao)
              $action_solicitacao = new TAction(array('SolicitacaoDetalhe', 'onReload'));
              $action_solicitacao->setParameter('fk', '' . filter_input(INPUT_GET, 'fk') . '');

              // create an action (Averbacao)
              $action_averbacao = new TAction(array('AverbacaoDetalhe', 'onReload'));
              $action_averbacao->setParameter('fk', '' . filter_input(INPUT_GET, 'fk') . '');

              // create an action (Suspensao)
              $action_suspensao = new TAction(array('SuspensaoServidorDetalhe', 'onReload'));
              $action_suspensao->setParameter('fk', '' . filter_input(INPUT_GET, 'fk') . '');

              // create an action (Funcao)
              $action_funcao = new TAction(array('FuncaoServidorDetalhe', 'onReload'));
              $action_funcao->setParameter('fk', '' . filter_input(INPUT_GET, 'fk') . '');

              // create an action (Vacancia)
              $action_vacancia = new TAction(array('VacanciaDetalhe', 'onReload'));
              $action_vacancia->setParameter('fk', '' . filter_input(INPUT_GET, 'fk') . '');

              // create an action (Ferias)
              $action_ferias = new TAction(array('FeriasServidorDetalhe', 'onReload'));
              $action_ferias->setParameter('fk', '' . filter_input(INPUT_GET, 'fk') . '');

              // create an action (Provento)
              $action_provento = new TAction(array('ServidorProventoDetalhe', 'onReload'));
              $action_provento->setParameter('fk', '' . filter_input(INPUT_GET, 'fk') . '');
             */
            ################ fim Action DropDown 02 ################
            //Btn dropdown 01
            $dtn_dropdown01 = new TDropDown('Funcional', 'fa:list');
            $dtn_dropdown01->addAction('Lotação', $action_servidor_lotacao);
            /*
              $dtn_dropdown01->addAction('Provento', $action_provento);
              $dtn_dropdown01->addAction('Averbação', $action_averbacao);
              $dtn_dropdown01->addAction('Função', $action_funcao);
              $dtn_dropdown01->addAction('F&eacute;rias', $action_ferias);

              //Btn dropdown 02
              $dtn_dropdown02 = new TDropDown('Pessoal', 'fa:list');
              $dtn_dropdown02->addAction('Experiencia', $action_experiencia);
              $dtn_dropdown02->addAction('Titulação', $action_formacao_servidor);
              $dtn_dropdown02->addAction('Produção', $action_producao);
              $dtn_dropdown02->addAction('Capacitação', $action_capacitacao);
              $dtn_dropdown02->addAction('Perfil Habilidades', $action_servidor_habilidade);
              $dtn_dropdown02->addAction('Dependentes', $action_dependentes);

              //Btn dropdown 03
              $dtn_dropdown03 = new TDropDown('Saidas', 'fa:list');
              $dtn_dropdown03->addAction('Acidente', $action_acidente);
              $dtn_dropdown03->addAction('Concessão', $action_concessao);
              $dtn_dropdown03->addAction('Licença', $action_licenca_particular);
              $dtn_dropdown03->addAction('Penalidade', $action_suspensao);
              $dtn_dropdown03->addAction('Vacancia', $action_vacancia);
              $dtn_dropdown03->addAction('Faltas', $action_faltas_servidor);
              $dtn_dropdown03->addAction('Mandato Eletivo', $action_mandato_eletivo);

              //Btn dropdown 03
              $dtn_dropdown04 = new TDropDown('Outros', 'fa:list');
              //   $dtn_dropdown04->addAction('Cargo', $action_cargo);
              $dtn_dropdown04->addAction('Disposição', $action_disposicao);
              $dtn_dropdown04->addAction('Solicitação', $action_solicitacao);
              $dtn_dropdown04->addAction('Requisicao Servidor', $action_requisicao_servidor);
             */
            //botoes superior
            // add a row for the TVBox
            $row = $tb_btn_header->addRow();
            $row->addCell($dtn_dropdown01);
            // $row->addCell($dtn_dropdown02);
            // $row->addCell($dtn_dropdown03);
            // $row->addCell($dtn_dropdown04);
        }

        ################# Fim Botoes header #################
        //botoes inferiores
        // add a row for the TVBox
        $row = $tb_btn_footer->addRow();
        // $row->addCell($save_button)->style = 'float: left;';
        $row->addCell($goto_button)->style = 'float: left;';

        // define wich are the form fields
        $this->form->setFields(array($codigo, $nome, $nomecracha, $cpf, $matricula, $endereco,
            $endereco2, $bairro, $cidade, $uf, $cep, $cnh, $telefone, $celular,
            $fax, $email, $dataadmissao, $datanascimento, $sexo, $estadocivil,
            $naturalidade, $ufnaturalidade, $clt, $pispasep, $datafgts, $rg,
            $orgaorg, $numconselho, $conselho_id, $requisitocargo, $cargonovo_id,
            $cargo_id, $nivelsalarial_id, $nivelsalarialantigo_id, $redistribuido,
            $dataredistribuicao, $documentoredistribuicao, $relotado, $datarelotado,
            $documentorelotado, $dataquadrosuplementar, $documentoquadrosuplementar,
            $quadrosuplementar, $tipofuncionario, $situacao, $dataaposentadoria,
            $documentosaida, $nomemae, $nomepai, $nacionalidade, $cor, $altura,
            $peso, $deficienteauditivo, $deficientevisual, $deficientefisico,
            $usuarioalteracao, $dataalteracao, $jornada_id, $inf_complementares,
           $new_button, $goto_button
        ));

        // wrap the page content using vertical box
        $vbox = new TVBox;

        if (!empty($_GET['fk'])) {
            $vbox->add($tb_btn_header);
        }
        $vbox->add($this->form);
        $vbox->add($tb_btn_footer);

        parent::add($vbox);
    }

    /**
     * method onSave()
     * Executed whenever the user clicks at the save button
     */
    /*
      function onSave() {
      try {
      // open a transaction with database 'samples'
      TTransaction::open('pg_ceres');

      $object = $this->form->getData("ServidorRecord");    // get the form data into an City Active Record

      $object->store();  // stores the object
      // fill the form with the active record data
      $this->form->setData($object);

      TTransaction::close();  // close the transaction
      // shows the success message
      new TMessage('info', 'Registro Salvo com Sucesso');
      // reload the listing
      } catch (Exception $e) { // in case of exception
      // shows the exception error message
      new TMessage('error', '<b>Error</b> ' . $e->getMessage());
      // undo all pending operations
      TTransaction::rollback();
      }
      }
     * 
     */

    /**
     * method onEdit()
     * Executed whenever the user clicks at the edit button da datagrid
     */
    function onEdit($param) {
        try {
            if (isset($param['fk'])) {
                // get the parameter $key
                $key = $param['fk'];

                TTransaction::open('pg_ceres');   // open a transaction with database 'samples'
                $object = new ServidorRecord($key);        // instantiates object City
                //converter datas do form para formato Brasileiro
                $object->dataadmissao = TDate::date2br($object->dataadmissao);
                $object->datanascimento = TDate::date2br($object->datanascimento);
                $object->dataorgaofgts = TDate::date2br($object->dataorgaofgts);
                $object->dataredistribuicao = TDate::date2br($object->dataredistribuicao);
                $object->datarelotado = TDate::date2br($object->datarelotado);
                $object->dataaposentadoria = TDate::date2br($object->dataaposentadoria);
                $object->documentosaida = TDate::date2br($object->documentosaida);
                $object->dataquadrosuplementar = TDate::date2br($object->dataquadrosuplementar);

                $this->form->setData($object);   // fill the form with the active record data
                TTransaction::close();           // close the transaction
            } else {
                $this->form->clear();
            }
        } catch (Exception $e) { // in case of exception
            // shows the exception error message
            new TMessage('error', '<b>Error</b> ' . $e->getMessage());
            // undo all pending operations
            TTransaction::rollback();
        }
    }

}

?>