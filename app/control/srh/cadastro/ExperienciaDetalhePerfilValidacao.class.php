<?php

/*
 * classe ExperienciaDetalhePerfilValidacao
 * Cadastro de Experiencia: Contem a listagem e o formulario de busca
 */

ini_set('display_errors', 1);
ini_set('display_startup_erros', 1);
error_reporting(E_ALL);

use Adianti\Database\TFilter1;
use Adianti\Widget\Datagrid\TDatagridTables;

class ExperienciaDetalhePerfilValidacao extends TPage {

    private $form;     // formulario de cadastro
    private $datagrid; // listagem

    /*
     * metodo construtor
     * Cria a pagina, o formulario e a listagem
     */

    public function __construct() {
        parent::__construct();
		
		// instancia um formulario
        $this->form = new TQuickForm;
        $this->form->class = 'form_Experiencia_detalhe_validacao';
        $this->form->setFormTitle('<font color="red" size="3" face="Arial"><b>Detalhe de Experi&ecirc;ncias dos Servidores</b></font>');

        // cria um rotulo para o titulo
        $titulo = new TLabel('<div style="position:floatval; width: 200px;"> <b>* Campo obrigatorio</b></div>');
        $titulo->setFontFace('Arial');
        $titulo->setFontColor('red');
        $titulo->setFontSize(10);

        // cria os campos do formulario
        $codigo = new THidden('id');
        $status = new TCombo('status');
        $empresa = new THidden('empresa');
        $datainicio = new THidden('datainicio');
        $datafim = new THidden('datafim');
        $cargo = new THidden('cargo');
        $observacao = new TText('observacao');

        //campos hidden
        $usuarioalteracao = new THidden('usuarioalteracao');
        $dataalteracao = new THidden('dataalteracao');

        $statusalteracao = new THidden('statusalteracao');
        $datastatus = new THidden('datastatus');
		
        //lanca o default
        $statusalteracao->setValue($_SESSION['usuario']);
        $datastatus->setValue(date("d/m/Y H:i:s"));

        //coloca o valor do relacionamento
        $id = new THidden('servidor_id');
        $id->setValue($_GET['fk']);
        
         //Cria um vetor com as opcoes da combo
        $items = array();
        $items['VALIDO'] = 'VALIDO';
        $items['INVALIDO'] = 'INVALIDO';
        // adiciona as opcoes na combo situacao
        $status->addItems($items);

        //Define o auto-sugerir
        $empresa->setProperty('placeholder', 'Ex: Agro Industria LTDA...');
        $datainicio->setProperty('placeholder', '02/05/2003');
        $datafim->setProperty('placeholder', '20/10/2005');
        $cargo->setProperty('placeholder', 'Ex: Analista Financeiro...');
		
		//cria a colecao da tabela estrangeira do Cabecalho
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $cadastro = new ServidorRecord(filter_input ( INPUT_GET, 'fk' ));
        // cria um criterio de selecao, ordenado pelo id do servidor
        //adiciona os objetos na tela com as informacoes do servidor
        if ($cadastro) {
            $nome = new TLabel($cadastro->nome);
            $matricula = new TLabel($cadastro->matricula);
        }
        // finaliza a transacao
        TTransaction::close();

		// define os campos
		$this->form->addQuickField("Nome Servidor", $nome, 300);
		$this->form->addQuickField('Matr&iacute;cula', $matricula, 50 );

        if ($_REQUEST['method'] == 'onEdit') {

			// inicia transacao com o banco 'pg_ceres'
			TTransaction::open('pg_ceres');
			
			$experienciaTemp = new ExperienciaRecord( filter_input ( INPUT_GET, 'key' ) );
		
			//converter datas do form para formato Brasileiro
            $experienciaTemp->datainicio = TDate::date2br( $experienciaTemp->datainicio );
            $experienciaTemp->datafim = TDate::date2br( $experienciaTemp->datafim );
		
			$empresa_nome = new TLabel( $experienciaTemp->empresa );
			$datainicio_nome = new TLabel( $experienciaTemp->datainicio );
			$datafim_nome = new TLabel( $experienciaTemp->datafim );
			$cargo_nome = new TLabel( $experienciaTemp->cargo );
			
			// finaliza a transacao
			TTransaction::close();
	
			$this->form->addQuickField('Empresa', $empresa_nome, 400);
			$this->form->addQuickField('Data Inicio', $datainicio_nome, 100);
			$this->form->addQuickField('Data Fim', $datafim_nome, 100);
			$this->form->addQuickField('Cargo', $cargo_nome, 200);
            
        }
		
		$this->form->addQuickField('Status', $status, 30);
		$this->form->addQuickField('Observa&ccedil;&atilde;o', $observacao, 50);
			
		$this->form->addQuickField(null, $id, 10);
		$this->form->addQuickField(null, $usuarioalteracao, 10);
        $this->form->addQuickField(null, $dataalteracao, 10);
		$this->form->addQuickField(null, $statusalteracao, 50 );		
		$this->form->addQuickField(null, $datastatus, 50 );
		$this->form->addQuickField(null, $titulo, 50);
		$this->form->addQuickField(null, $codigo, 50);
		$this->form->addQuickField(null, $empresa, 50);
		$this->form->addQuickField(null, $datainicio, 50);
		$this->form->addQuickField(null, $datafim, 50);
		$this->form->addQuickField(null, $cargo, 50);

        if ($_REQUEST['method'] == 'onEdit') 
		{
			
			// define a acao do botao
			$action1 = new TAction(array($this, 'onSave'));
			$action1->setParameter('fk', $_GET['fk']);
			$action1->setParameter('key', $_GET['key']);
			
			// cria um botao de acao
			$this->form->addQuickAction('Salvar', $action1, 'ico_save.png');
		
		}
		
        $this->form->addQuickAction('Voltar', new TAction(array('ServidorCurriculoValidacaoList', 'onReload')), 'ico_datagrid.gif');

		// instancia objeto DataGrid
        $this->datagrid = new TDataGridTables;

        // instancia as colunas da DataGrid
        $dgempresa = new TDataGridColumn('empresa', 'Empresa', 'left', 300);
        $dgdatainicio = new TDataGridColumn('datainicio', 'Data Inicio', 'left', 80);
        $dgdatafim = new TDataGridColumn('datafim', 'Data Fim', 'left', 80);
        $dgcargo = new TDataGridColumn('cargo', 'Cargo', 'left', 250);
        $dgobservacao = new TDataGridColumn('observacao', 'Observa&ccedil;&atilde;o', 'left', 450);
        $dgstatus = new TDataGridColumn('status', 'Status', 'left', 250);

        // adiciona as colunas a DataGrid
        $this->datagrid->addColumn($dgempresa);
        $this->datagrid->addColumn($dgdatainicio);
        $this->datagrid->addColumn($dgdatafim);
        $this->datagrid->addColumn($dgcargo);
        $this->datagrid->addColumn($dgobservacao);
        $this->datagrid->addColumn($dgstatus);

        // instancia duas acoes da DataGrid
        $action1 = new TDataGridAction(array($this, 'onEdit'));
        $action1->setLabel('Editar');
        $action1->setImage('ico_edit.png');
        $action1->setField('id');
        $action1->setFk('servidor_id');

        $action2 = new TDataGridAction(array($this, 'onDelete'));
        $action2->setLabel('Deletar');
        $action2->setImage('ico_delete.png');
        $action2->setField('id');
        $action2->setFk('servidor_id');

        // adiciona as acoes a DataGrid
        $this->datagrid->addAction($action1);
        $this->datagrid->addAction($action2);

        // cria o modelo da DataGrid, montando sua estrutura
        $this->datagrid->createModel();

        // monta a paina atraves de uma tabela
        $panel = new TPanelForm(900, 600);
        $panel->put($this->form, 0, 0);
        if ($_REQUEST['method'] == 'onEdit') {
            $panel->put($this->datagrid, 120, 345);
        } else {
            $panel->put($this->datagrid, 120, 100);
        }

        // adiciona a tabela a pagina
        parent::add($panel);
    }

    /*
     * metodo onReload()
     * Carrega a DataGrid com os objetos do banco de dados
     */

    function onReload() {
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instancia um repositorio da Classe
        $repository = new TRepository('ExperienciaRecord');

        // cria um criterio de selecao
        $criteria = new TCriteria;

        //filtra pelo campo selecionado pelo usuário
        $criteria->add(new TFilter('servidor_id', '=', $_GET['fk']));

        //ordenar campos
        $criteria->setProperty('order', 'datafim');

        // carrega os objetos de acordo com o criterio
        $cadastros = $repository->load($criteria);

        $this->datagrid->clear();
        if ($cadastros) {
            // percorre os objetos retornados
            foreach ($cadastros as $cadastro) {
				
				//converter datas do form para formato Brasileiro
                $cadastro->datainicio = TDate::date2br($cadastro->datainicio);
                $cadastro->datafim = TDate::date2br($cadastro->datafim);
				
                // adiciona o objeto na DataGrid
                $this->datagrid->addItem($cadastro);
            }
        }

        // finaliza a transacao
        TTransaction::close();
        $this->loaded = true;
    }

    /*
     * metodo onDelete()
     * Executada quando o usuario clicar no botao excluir da datagrid
     * Pergunta ao usuario se deseja realmente excluir um registro
     */

    function onDelete($param) {
        // obtem o parametro $key
        $key = $param['key'];

        // define duas acoes
        $action1 = new TAction(array($this, 'Delete'));

        // define os parametros de cada acao
        $action1->setParameter('key', $key);

        //encaminha a chave estrangeira
        $action1->setParameter('fk', $_GET['fk']);

        // exibe um dialogo ao usuario
        new TQuestion('Deseja realmente excluir o registro ?', $action1);
    }
	 
    /*
     * metodo Delete()
     * Exclui um registro
     */
    function Delete($param) {
        // obtem o parametro $key
        $key = $param['key'];
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instanicia objeto Record
        $cadastro = new ExperienciaRecord($key);

        try {
            // deleta objeto do banco de dados
            $cadastro->delete();

            // finaliza a transacao
            TTransaction::close();
        } catch (Exception $e) { // em caso de exceção
            // exibe a mensagem gerada pela exceção
            new TMessage('error', $e->getMessage());
            // desfaz todas alterações no banco de dados
            TTransaction::rollback();
        }

        //re-carrega a datagrid
        $this->onReload();
        // exibe mensagem de sucesso
		new TMessage('info', "Registro Excluido com sucesso");

    }

    /*
     * metodo show()
     * Exibe a pagina
     */

    function show() {
        // carrega os dados no datagrid
        $this->onReload();
        //chama o metodo show da super classe
        parent::show();
    }

    /*
     * metodo onSave()
     * Executada quando o usuario clicar no botao salvar do formulario
     */

    function onSave() {
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // obtem os dados no formulario em um objeto Record da Classe
        $cadastro = $this->form->getData('ExperienciaRecord');
		
		//lanca o default
        $cadastro->usuarioalteracao = $_SESSION['usuario'];
        $cadastro->dataalteracao = date("d/m/Y H:i:s");
		
		//lanca o default
        $cadastro->statusalteracao = $_SESSION['usuario'];
        $cadastro->datastatus = date("d/m/Y H:i:s");

        //antes de armazenar verifica se algum campo eh requerido e nao foi informado
        $dados = $cadastro->toArray();

        $msg = '';
        $icone = 'info';

        if (empty($dados['empresa'])) {
            $msg .= 'A empresa deve ser informada';
        }

        if (empty($dados['datainicio'])) {
            $msg .= 'A data de inicio deve ser informada';
        }
        if (empty($dados['datafim'])) {
            $msg .= 'A data fim deve ser informada';
        }
        if (empty($dados['cargo'])) {
            $msg .= 'O cargo deve ser informado';
        }

        try {

            if ($msg == '') {
                // armazena o objeto
                $cadastro->store();
                $msg = 'Dados armazenados com sucesso';

                // finaliza a transacao
                TTransaction::close();
            } else {
                $icone = 'error';
            }

            if ($icone == 'error') {
                // exibe mensagem de erro
                new TMessage($icone, $msg);
				
				$this->form->setData($cadastro);   // fill the form with the active record data
				
            } else {
				
				$param = array();
				$param ['fk'] = $dados['servidor_id'];
				
                //chama o formulario com o grid
                // exibe um dialogo ao usuario
                new TMessage("info", "Registro salvo com sucesso!");
				TApplication::gotoPage('ExperienciaDetalhePerfilValidacao','onReload', $param); // reload
				
            }
        } catch (Exception $e) { // em caso de exc
            // exibe a mensagem gerada pela exceção
            new TMessage('error', $e->getMessage());
            // desfaz todas alterações no banco de dados
            TTransaction::rollback();
			
			$this->form->setData($cadastro);   // fill the form with the active record data
			
        }
    }

    /*
     * metodo onEdit()
     * Edita os dados de um registro
     */
    function onEdit($param) {
		
		try {
            if (isset($param['key'])) {
                // get the parameter $key
                $key = $param['key'];

                TTransaction::open('pg_ceres');   // open a transaction with database 'samples'
                $object = new ExperienciaRecord($key);        // instantiates object City
				
				//converter datas do form para formato Brasileiro
				$object->datainicio = TDate::date2br( $object->datainicio );
				$object->datafim = TDate::date2br( $object->datafim );
				
                $this->form->setData($object);   // fill the form with the active record data
				
                TTransaction::close();           // close the transaction
            } else {
                $this->form->clear();
            }
        } catch (Exception $e) { // in case of exception
            // shows the exception error message
            new TMessage('error', '<b>Error</b> ' . $e->getMessage());
            // undo all pending operations
            TTransaction::rollback();
        }
      
    }

}

?>