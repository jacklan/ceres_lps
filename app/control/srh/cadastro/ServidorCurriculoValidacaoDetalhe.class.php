<?php

/*
 * classe ServidorCurriculoValidacaoDetalhe
 * Listagem de Itens a serem validados do curriculo Servidor
 */
include_once 'app.library/funcdate.php';

class ServidorCurriculoValidacaoDetalhe extends TPage {

    private $form;     // formulario de cadastro
    private $datagrid; // listagem

    /*
     * metodo construtor
     * Cria a pagina, o formulario e a listagem
     */

    public function __construct() {
        parent::__construct();

        // instancia um formulario
        $this->form = new TForm('form_ServidorHabilidade');

        // instancia um Panel
        $panel = new TPanelForm(700, 100);

        // adiciona o panel ao formulario
        $this->form->add($panel);

        // cria um r�tulo para o t�tulo
        $titulo = new TLabel('Curriculo Servidor Valida&ccedil;&atilde;o &raquo; Valida&ccedil;&otilde;o Curriculo Servidor');
        $titulo->setFontFace('Arial');
        $titulo->setFontColor('red');
        $titulo->setFontSize(16);

        // adiciona o campo Titulo
        $panel->put($titulo, $panel->getColuna(), $panel->getLinha());

        //cria a colecao da tabela estrangeira do Cabecalho
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $cadastro = new ServidorRecord($_GET['key']);
        // cria um criterio de selecao, ordenado pelo id do servidor
        //adiciona os objetos na tela com as informacoes do servidor
        if ($cadastro) {
            $nome = new TLabel($cadastro->nome);
            $matricula = new TLabel($cadastro->matricula);
        }
        // finaliza a transacao
        TTransaction::close();

        $panel->setLinha(50);
        // adiciona o campo
        $panel->putCampo(null, 'Servidor:', 0, 0);
        $panel->put(new TLabel('Matricula:'), 0, 1);
        $panel->setLinha(70);
        $panel->put($nome, $panel->getColuna() + 160, $panel->getLinha());
        $panel->put($matricula, $panel->getColuna(), $panel->getLinha());
        $panel->put($find_button, $panel->getColuna() + 350, $panel->getLinha());
        $panel->put($new_button, $panel->getColuna() + 420, $panel->getLinha());

        // define quais sao os campos do formulario
        $this->form->setFields(array($nome, $find_button, $new_button));

        // instancia objeto DataGrid
        $this->datagrid = new TDataGridCss;

        // instancia as colunas da DataGrid
        $dgcodigo = new TDataGridColumn('id', 'Codigo', 'right', 50);
        $dgnome_formacao = new TDataGridColumn('nome_formacao', 'Nome', 'left', 250);
        $dgsituacao_formacao = new TDataGridColumn('situacao_formacao', 'Descricao', 'left', 150);
        $dgempresa_experiencia = new TDataGridColumn('empresa_experiencia', 'Descricao', 'left', 150);
        $dgtitulo_producao = new TDataGridColumn('titulo_producao', 'Descricao', 'left', 150);
        $dgnome_capacitacao = new TDataGridColumn('nome_capacitacao', 'Descricao', 'left', 150);

        // adiciona as colunas a DataGrid
        $this->datagrid->addColumn($dgcodigo);
        $this->datagrid->addColumn($dgnome_formacao);
        $this->datagrid->addColumn($dgsituacao_formacao);
        $this->datagrid->addColumn($dgempresa_experiencia);
        $this->datagrid->addColumn($dgtitulo_producao);
        $this->datagrid->addColumn($dgnome_capacitacao);

        // instancia objeto DataGrid
        $this->datagrid = new TDataGridCss;


        // instancia as colunas da DataGrid
        $dgcodigo = new TDataGridColumn('id', 'C&oacute;digo', 'left', 50);
        $dgnome_formacao = new TDataGridColumn('nome_formacao', 'Perfil', 'left', 400);
        $dgsituacao_formacao = new TDataGridColumn('situacao_formacao', 'Tipo', 'left', 150);

        // adiciona as colunas a DataGrid
        $this->datagrid->addColumn($dgcodigo);
        $this->datagrid->addColumn($dgnome_formacao);
        $this->datagrid->addColumn($dgsituacao_formacao);

        // instancia duas acoes da DataGrid
        $action1 = new TDataGridAction(array($this, 'onEdit'));
        $action1->setLabel('Editar');
        $action1->setImage('ico_edit.png');
        $action1->setAlt('Editar');
        $action1->setField('id');
        $action1->setFk('servidor_id');

        $action2 = new TDataGridAction(array($this, 'onDelete'));
        $action2->setLabel('Deletar');
        $action2->setImage('ico_delete.png');
        $action1->setAlt('Deletar');
        $action2->setField('id');
        $action2->setFk('servidor_id');

        // adiciona as acoes a DataGrid
        $this->datagrid->addAction($action1);
        $this->datagrid->addAction($action2);

        // cria o modelo da DataGrid, montando sua estrutura
        $this->datagrid->createModel();

        // monta a paina atraves de uma tabela
        $panel = new TPanelForm(700, 500);
        $panel->put($this->form, 0, 0);
        $panel->put($this->datagrid, 150, 170);


        // adiciona a tabela a pagina
        parent::add($panel);
    }

    /*
     * metodo onCancel()
     * Executada quando o usuario clicar no botao cancelar do form
     * Exibe msg ao usuario informano que a operacao foi cancelada
     */

    function onCancel() {
        // define a acao de cancelamento
        $obj = new ServidorForm;
        $action1 = new TAction(array($obj, 'onEdit'));
        $action1->setParameter('key', $_GET['fk']);

        // exibe um dialogo ao usuario
        new TConfirmation('Opera&ccedil;&atilde;o cancelada!', $action1);
    }

    /*
     * metodo onReload()
     * Carrega a DataGrid com os objetos do banco de dados
     */

    function onReload() {
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instancia um repositorio da Classe
        $repository = new TRepository('vw_servidor_curriculo');

        // cria um criterio de selecao
        $criteria = new TCriteria;

        //filtra pelo campo selecionado pelo usuário
        $criteria->add(new TFilter('servidor_id', '=', $_GET['key']));

        // carrega os objetos de acordo com o criterio
        $cadastros = $repository->load($criteria);

        $this->datagrid->clear();
        if ($cadastros) {
            // percorre os objetos retornados
            foreach ($cadastros as $cadastro) {
                // adiciona o objeto na DataGrid
                $this->datagrid->addItem($cadastro);
            }
        }
        // finaliza a transacao
        TTransaction::close();
        $this->loaded = true;
    }

    /*
     * metodo onDelete()
     * Executada quando o usuario clicar no botao excluir da datagrid
     * Pergunta ao usuario se deseja realmente excluir um registro
     */

    function onDelete($param) {
        // obtem o parametro $key
        $key = $param['key'];

        // define duas acoes
        $action1 = new TAction(array($this, 'Delete'));
        $action2 = new TAction(array($this, 'NaoDelete'));

        // define os parametros de cada acao
        $action1->setParameter('key', $key);
        $action2->setParameter('key', $key);

        //encaminha a chave estrangeira
        $action1->setParameter('fk', $_GET['fk']);
        $action2->setParameter('fk', $_GET['fk']);
        // exibe um dialogo ao usuario
        new TQuestion('Deseja realmente excluir o registro ?', $action1, $action2);
    }

    /*
     * metodo Delete()
     * Exclui um registro
     */

    function Delete($param) {
        // obtem o parametro $key
        $key = $param['key'];
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instanicia objeto Record
        $cadastro = new ServidorHabilidadeRecord($key);

        try {
            // deleta objeto do banco de dados
            $cadastro->delete();

            // finaliza a transacao
            TTransaction::close();
        } catch (Exception $e) { // em caso de exceção
            // exibe a mensagem gerada pela exceção
            new TMessage('error', $e->getMessage());
            // desfaz todas alterações no banco de dados
            TTransaction::rollback();
        }

        // re-carrega a datagrid
        $this->onReload();
        // exibe mensagem de sucesso
        new TMessage('info', "Registro Exclu&iacute;do com sucesso");
    }

    /*
     * metodo show()
     * Exibe a pagina
     */

    function show() {
        // carrega os dados no datagrid
        $this->onReload();
        //chama o metodo show da super classe
        parent::show();
    }

    /*
     * metodo onSave()
     * Executada quando o usuario clicar no botao salvar do formulario
     */

    function onSave() {
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // obtem os dados no formulario em um objeto Record da Classe
        $cadastro = $this->form->getData('ServidorHabilidadeRecord');

        //antes de armazenar verifica se algum campo eh requerido e nao foi informado
        $dados = $cadastro->toArray();

        $msg = '';
        $icone = 'info';

        if (empty($dados['habilidade_id'])) {
            $msg .= 'A habilidade deve ser informada';
        }


        try {

            if ($msg == '') {
                // armazena o objeto
                $cadastro->store();
                $msg = 'Dados armazenados com sucesso';

                // finaliza a transacao
                TTransaction::close();
            } else {
                $icone = 'error';
            }

            if ($icone == 'error') {
                // exibe mensagem de erro
                new TMessage($icone, $msg);
            } else {
                //chama o formulario com o grid
                // define duas acoes
                $action1 = new TAction(array('ServidorHabilidadeDetalhe', 'onReload'));
                $action1->setParameter('fk', $dados['servidor_id']);
                // exibe um dialogo ao usuario
                new TConfirmation($msg, $action1);
                // re-carrega listagem
                //                $this->onReload();
            }
        } catch (Exception $e) { // em caso de exc
            // exibe a mensagem gerada pela exceção
            new TMessage('error', $e->getMessage());
            // desfaz todas alterações no banco de dados
            TTransaction::rollback();
        }
    }

    /*
     * metodo onEdit()
     * Edita os dados de um registro
     */

    function onEdit($param) {
        // obtem o parametro $key
        $key = $param['key'];
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $cadastro = new ServidorHabilidadeRecord($key);
        // lanca os dados no formulario
        $this->form->setData($cadastro);

        // finaliza a transacao
        TTransaction::close();
    }

}

?>