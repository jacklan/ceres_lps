<script>

</script>
<?php

//ini_set('display_errors', 1);
//ini_set('display_startup_erros', 1);
//error_reporting(E_ALL);

/**
 * ServidorForm
 *
 * @version    2.0
 * @package    ceres
 * @subpackage app/control
 * @author     Jackson Meires
 * @copyright  Copyright (c) 2006-2014 Adianti Solutions Ltd. (http://www.adianti.com.br)
 * @license    http://www.adianti.com.br/framework-license
 */
class ServidorForm extends TPage
{

    private $form; // form

    /**
     * Class constructor
     * Creates the page and the registration form
     */

    function __construct()
    {
        parent::__construct();

        // creates the form
        $this->form = new TForm('form_Servidor');

        // creates the containers for each notebook page
        $page1 = new TTable;
        $page1->width = '100%';

        $page2 = new TTable;
        $page2->width = '100%';

        $page3 = new TTable;
        $page3->width = '100%';

        $page4 = new TTable;
        $page4->width = '100%';

        $page5 = new TTable;
        $page5->width = '100%';

        $page6 = new TTable;
        $page6->width = '100%';

        // creates a table
        $tb_btn_header = new TTable;
        $tb_btn_footer = new TTable;

        // creates the notebook
        $notebook = new TNotebook();
        // add the notebook inside the form
        $this->form->add($notebook);

        // adds two pages in the notebook
        $notebook->appendPage('Dados Pessoais', $page1);
        $notebook->appendPage('Endere&ccedil;o', $page2);
        $notebook->appendPage('Documentos', $page3);
        $notebook->appendPage('Cargo', $page4);
        $notebook->appendPage('Dados Fisicos', $page5);
        $notebook->appendPage('Outros', $page6);

        // creates a label with the title
        $titulo = new TLabel('<b>Formul&aacute;rio Servidor</b>');
        $titulo->setFontSize(12);
        $titulo->setFontFace('Arial');
        $titulo->setFontColor('red');

        // creates a label with the title
        $campObg = new TLabel('<b>* Campos obrigat&oacute;rios</b>');
        $campObg->setFontSize(10);
        $campObg->setFontFace('Arial');
        $campObg->setFontColor('red');

        // add the table inside the form
        // create the form fields
        $codigo = new THidden('id');
        $nome = new TEntry('nome');
        $nomecracha = new TEntry('nomecracha');
        $cpf = new TEntry('cpf');
        $matricula = new TEntry('matricula');
        $endereco = new TEntry('endereco');
        $endereco2 = new TEntry('endereco2');
        $bairro = new TEntry('bairro');
        $cidade = new TEntry('cidade');
        $uf = new TCombo('uf');
        $cep = new TEntry('cep');
        $cnh = new TEntry('cnh');
        $telefone = new TEntry('telefone');
        $celular = new TEntry('celular');
        $fax = new TEntry('fax');
        $email = new TEntry('email');
        $dataadmissao = new TDate('dataadmissao');
        $datanascimento = new TDate('datanascimento');
        $sexo = new TCombo('sexo');
        $estadocivil = new TCombo('estadocivil');
        $naturalidade = new TEntry('naturalidade');
        $ufnaturalidade = new TCombo('ufnaturalidade');
        $clt = new TEntry('carteiratrabalho');
        $pispasep = new TEntry('pispasep');
        $datafgts = new TDate('dataorgaofgts');
        $rg = new TEntry('rg');
        $orgaorg = new TEntry('orgaorg');
        $numconselho = new TEntry('numconselho');
        $conselho_id = new TCombo('conselho_id');
        $cargo_id = new TCombo('cargo_id');
        $cargonovo_id = new TCombo('cargonovo_id');
        $redistribuido = new TCombo('redistribuido');
        $dataredistribuicao = new TDate('dataredistribuicao');
        $documentoredistribuicao = new TEntry('documentoredistribuicao');
        $relotado = new TCombo('relotado');
        $datarelotado = new TDate('datarelotado');
        $documentorelotado = new TEntry('documentorelotado');
        $tipofuncionario = new TCombo('tipofuncionario');
        $situacao = new TCombo('situacao');
        $cedidoorgao = new TEntry('cedidoorgao');
        $dataaposentadoria = new TDate('dataaposentadoria');
        $datacalcaposentadoria = new TDate('datacalcaposentadoria');
        $documentosaida = new TEntry('documentosaida');
        $quadrosuplementar = new TCombo('quadrosuplementar');
        $dataquadrosuplementar = new TDate('dataquadrosuplementar');
        $documentoquadrosuplementar = new TEntry('documentoquadrosuplementar');
        //  $datasituacao  = new TDate('datasituacao');
        $nomepai = new TEntry('nomepai');
        $nomemae = new TEntry('nomemae');
        $nacionalidade = new TEntry('nacionalidade');
        $deficienteauditivo = new TCombo('deficienteauditivo');
        $deficientevisual = new TCombo('deficientevisual');
        $deficientefisico = new TCombo('deficientefisico');
        $cor = new TCombo('cor');
        $altura = new TEntry('altura');
        $peso = new TEntry('peso');
        $nivelsalarial_id = new TCombo('nivelsalarial_id');
        $requisitocargo = new TCombo('requisitocargo_id');
        $nivelsalarialantigo_id = new TCombo('nivelsalarialantigo_id');
        $jornada_id = new TCombo('jornada_id');
        $inf_complementares = new TText('inf_complementares');
        $tipojornada = new TCombo('tipojornada');
        $tiporegistroponto = new TCombo('tiporegistroponto');

        $tipoescala = new TRadioGroup('tipoescala');

        $tipoescala->addItems(array('1' => 'Jornada', '2' => 'Escala'));
        $tipoescala->setLayout('horizontal');
        $tipoescala->setValue('1');

        $escala_id = new \Adianti\Widget\Form\TCombo('escala_id');

        TTransaction::open('pg_ceres');
        $repository = new TRepository('EscalaRecord');
        $criteria = new TCriteria;
        $criteria->add(new TFilter('empresa_id', '=', $_SESSION['empresa_id']));
        $criteria->setProperty('order', 'nome');
        $cadastros = $repository->load($criteria);

        foreach ($cadastros as $object) {
            $items[$object->id] = $object->nome;
        }
        $escala_id->addItems($items);

        TTransaction::close();


        if (filter_input(INPUT_GET, 'fk')) {

            $cargo_id->setEditable(false);
            $nivelsalarialantigo_id->setEditable(false);

        }

        $usuarioalteracao = new THidden('usuarioalteracao');
        $dataalteracao = new THidden('dataalteracao');

        //cria a colecao da tabela estrangeira
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $repository = new TRepository('ConselhoRecord');
        // cria um criterio de selecao, ordenado pelo id
        $criteria = new TCriteria;
        //$criteria->add(new TFilter('empresa_id', '=', $_SESSION['empresa_id']) );
        $criteria->setProperty('order', 'sigla');
        // carrega os objetos de acordo com o criterio
        $cadastros = $repository->load($criteria);

        //adiciona os objetos no combo
        foreach ($cadastros as $object) {
            $items[$object->id] = $object->sigla . "/" . $object->uf;
        }
        // adiciona as opcoes na combo
        $conselho_id->addItems($items);

        // finaliza a transacao
        TTransaction::close();


        $items9 = [];
        //cria a colecao da tabela estrangeira
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $repository = new TRepository('RequisitoCargoRecord');
        // cria um criterio de selecao, ordenado pelo id
        $criteria = new TCriteria;
        //$criteria->add(new TFilter('empresa_id', '=', $_SESSION['empresa_id']) );
        $criteria->setProperty('order', 'nome');
        // carrega os objetos de acordo com o criterio
        $cadastros = $repository->load($criteria);

        //adiciona os objetos no combo
        foreach ($cadastros as $object) {
            $items9[$object->id] = $object->nome;
        }
        // adiciona as opcoes na combo
        $requisitocargo->addItems($items9);
        // finaliza a transacao
        TTransaction::close();

        $itemsCargo = array();
        //cria a colecao da tabela estrangeira
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $repository = new TRepository('CargoRecord');
        // cria um criterio de selecao, ordenado pelo id
        $criteria = new TCriteria;
        $criteria->add(new TFilter('empresa_id', '=', $_SESSION['empresa_id']));
        $criteria->setProperty('order', 'nome');
        // carrega os objetos de acordo com o criterio
        $cadastros = $repository->load($criteria);

        //adiciona os objetos no combo
        foreach ($cadastros as $object) {
            $itemsCargo[$object->id] = $object->nome;
        }
        // adiciona as opcoes na combo
        $cargo_id->addItems($itemsCargo);
        $cargonovo_id->addItems($itemsCargo);

        // finaliza a transacao
        TTransaction::close();

        //cria a colecao da tabela estrangeira
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $repository = new TRepository('NivelSalarialRecord');

        // cria um criterio de selecao, ordenado pelo id
        $criteria = new TCriteria;
        $criteria->add(new TFilter('empresa_id', '=', $_SESSION['empresa_id']));
        $criteria->setProperty('order', 'nome');
        // carrega os objetos de acordo com o criterio
        $cadastros = $repository->load($criteria);

        //adiciona os objetos no combo
        foreach ($cadastros as $object) {
            $items1[$object->id] = $object->nome;
        }
        // adiciona as opcoes na combo
        $nivelsalarial_id->addItems($items1);
        // adiciona as opcoes na combo
        $nivelsalarialantigo_id->addItems($items1);

        // finaliza a transacao
        TTransaction::close();

        $itemsj2 = array();
        //cria a colecao da tabela estrangeira
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $repository1 = new TRepository('JornadaRecord');
        // cria um criterio de selecao, ordenado pelo id
        $criteria1 = new TCriteria;
        $criteria1->add(new TFilter('empresa_id', '=', $_SESSION['empresa_id']));
        $criteria1->setProperty('order', 'entrada1');
        // carrega os objetos de acordo com o criterio
        $cadastros1 = $repository1->load($criteria1);

        //adiciona os objetos no combo
        foreach ($cadastros1 as $object1) {

            //condicao ? ver : false;
            $textojornada = substr($object1->entrada1, 0, 5) . " &aacute;s " . substr($object1->saida1, 0, 5);

            if ($object1->entrada2 != '' && $object1->saida2 != '') {

                $textojornada .= " - " . substr($object1->entrada2, 0, 5) . " &aacute;s " . substr($object1->saida2, 0, 5);
            }

            if ($object1->entrada3 != '' && $object1->saida3 != '') {

                $textojornada .= " - " . substr($object1->entrada3, 0, 5) . " &aacute;s " . substr($object1->saida3, 0, 5);
            }

            $itemsj2[$object1->id] = $textojornada;
        }
        // adiciona as opcoes na combo
        $jornada_id->addItems($itemsj2);

        // finaliza a transacao
        TTransaction::close();

        // cria um vetor com as opcoes da combo estado
        $items = array();
        $items['AC'] = 'AC';
        $items['AL'] = 'AL';
        $items['AM'] = 'AM';
        $items['AP'] = 'AP';
        $items['BA'] = 'BA';
        $items['CE'] = 'CE';
        $items['DF'] = 'DF';
        $items['ES'] = 'ES';
        $items['GO'] = 'GO';
        $items['MA'] = 'MA';
        $items['MG'] = 'MG';
        $items['MS'] = 'MS';
        $items['MT'] = 'MT';
        $items['PA'] = 'PA';
        $items['PB'] = 'PB';
        $items['PE'] = 'PE';
        $items['PI'] = 'PI';
        $items['PR'] = 'PR';
        $items['RJ'] = 'RJ';
        $items['RN'] = 'RN';
        $items['RO'] = 'RO';
        $items['RR'] = 'RR';
        $items['RS'] = 'RS';
        $items['SC'] = 'SC';
        $items['SE'] = 'SE';
        $items['SP'] = 'SP';
        $items['TO'] = 'TO';

        // adiciona as opcoes na combo
        $uf->addItems($items);
        //coloca o valor padrao no combo
        $uf->setValue('RN');

        //adiciona as opcoes na combo
        $ufnaturalidade->addItems($items);
        //coloca o valor padrao no combo
        $ufnaturalidade->setValue('RN');

        // cria um vetor com as opcoes da combo cor
        $items = array();
        $items['BRANCA'] = 'BRANCA';
        $items['NEGRA'] = 'NEGRA';
        $items['PARDA'] = 'PARDA';
        $items['BRANCA'] = 'BRANCA';
        $items['INDIGENA'] = 'INDIGENA';

        // adiciona as opcoes na combo
        $cor->addItems($items);
        //coloca o valor padrao no combo
        $cor->setValue('');

        // cria um vetor com as opcoes da combo redistribuido
        $items = array();
        $items['SIM'] = 'SIM';
        $items['NAO'] = 'NAO';

        // adiciona as opcoes na combo
        $quadrosuplementar->addItems($items);
        //coloca o valor padrao no combo
        $quadrosuplementar->setValue('NAO');

        // cria um vetor com as opcoes da combo redistribuido
        $items = array();
        $items['SIM'] = 'SIM';
        $items['NAO'] = 'NAO';

        // adiciona as opcoes na combo
        $redistribuido->addItems($items);
        //coloca o valor padrao no combo
        $redistribuido->setValue('NAO');

        // cria um vetor com as opcoes da combo relotado
        $items = array();
        $items['SIM'] = 'SIM';
        $items['NAO'] = 'NAO';

        // adiciona as opcoes na combo
        $relotado->addItems($items);
        //coloca o valor padrao no combo
        $relotado->setValue('NAO');

        // cria um vetor com as opcoes da combo DEFICIENTE AUDITIVO
        $items = array();
        $items['SIM'] = 'SIM';
        $items['NAO'] = 'NAO';

        // adiciona as opcoes na combo
        $deficienteauditivo->addItems($items);
        //coloca o valor padrao no combo
        $deficienteauditivo->setValue('NAO');

        // cria um vetor com as opcoes da combo DEFICIENTE VISUAL
        $items = array();
        $items['SIM'] = 'SIM';
        $items['NAO'] = 'NAO';

        // adiciona as opcoes na combo
        $deficientevisual->addItems($items);
        //coloca o valor padrao no combo
        $deficientevisual->setValue('NAO');

        // cria um vetor com as opcoes da combo DEFICIENTE FISICO
        $items = array();
        $items['SIM'] = 'SIM';
        $items['NAO'] = 'NAO';

        // adiciona as opcoes na combo
        $deficientefisico->addItems($items);
        //coloca o valor padrao no combo
        $deficientefisico->setValue('NAO');

        // cria um vetor com as opcoes da combo
        $items = array();
        $items['M'] = 'M';
        $items['F'] = 'F';

        // adiciona as opcoes na combo
        $sexo->addItems($items);
        //coloca o valor padrao no combo
        $sexo->setValue('M');

        // cria um vetor com as opcoes da combo
        $items = array();
        $items['CASADO(A)'] = 'CASADO(A)';
        $items['DESQUITADO(A)'] = 'DESQUITADO(A)';
        $items['DIVORCIADO(A)'] = 'DIVORCIADO(A)';
        $items['SEPARADO(A)'] = 'SEPARADO(A)';
        $items['SOLTEIRO(A)'] = 'SOLTEIRO(A)';
        $items['VIUVO(A)'] = 'VIUVO(A)';

        // adiciona as opcoes na combo
        $estadocivil->addItems($items);
        //coloca o valor padrao no combo
        $estadocivil->setValue('CASADO(A)');

        // cria um vetor com as opcoes da combo
        $items = array();
        $items['BOLSISTA'] = 'BOLSISTA';
        $items['COMISSIONADO'] = 'COMISSIONADO';
        $items['ESTAGIARIO'] = 'ESTAGIARIO';
        $items['EFETIVO'] = 'EFETIVO';
        $items['TERCEIRIZADO'] = 'TERCEIRIZADO';
        $items['CONVENIADO'] = 'CONVENIADO';

        // adiciona as opcoes na combo
        $tipofuncionario->addItems($items);
        //coloca o valor padrao no combo
        $tipofuncionario->setValue('FUNCIONARIO');

        // cria um vetor com as opcoes da combo
        $items = array();
        $items['A DISPOSICAO'] = 'A DISPOSIÇÃO';
        $items['APOSENTADO(A)'] = 'APOSENTADO(A)';
        $items['CEDIDO(A)'] = 'CEDIDO(A)';
        $items['DEVOLVIDO(A)'] = 'DEVOLVIDO(A)';
        $items['EM ATIVIDADE'] = 'EM ATIVIDADE';
        $items['EM VACANCIA'] = 'EM VACANCIA';
        $items['EXONERADO(A)'] = 'EXONERADO(A)';
        $items['FALECIDO(A)'] = 'FALECIDO(A)';
        $items['LICENCIADO(A)'] = 'LICENCIADO(A)';
        $items['RESCINDIDO(A)'] = 'RESCINDIDO(A)';

        // adiciona as opcoes na combo
        $situacao->addItems($items);
        //coloca o valor padrao no combo
        $situacao->setValue('EM ATIVIDADE');

        $itemsjornada = array();
        $itemsjornada['NORMAL'] = 'NORMAL';
        $itemsjornada['LIVRE'] = 'LIVRE';

        $tipojornada->addItems($itemsjornada);

        $itemsregistro = array();
        $itemsregistro['APPDESKTOP'] = 'APPDESKTOP';
        $itemsregistro['REDE LOCAL'] = 'REDE LOCAL';
        $itemsregistro['INTERNET'] = 'INTERNET';
        $itemsregistro['CELULAR'] = 'CELULAR';

        $tiporegistroponto->addItems($itemsregistro);
        $tiporegistroponto->setValue('REDE LOCAL');

        ############ Fim Combos  ############
        // define os tamanhos dos campos
        ######## Dados Pessoais ########
        $matricula->setSize(70);
        $nome->setSize(180);
        $nome->setProperty('style', 'text-transform: uppercase');
        $nomecracha->setSize(150);
        $nomecracha->setProperty('style', 'text-transform: uppercase');
        $cpf->setSize(70);
        $datanascimento->setSize(55);
        $naturalidade->setSize(110);
        $ufnaturalidade->setSize(35);
        $nacionalidade->setSize(130);
        $nacionalidade->setValue('BRASILEIRO');
        $email->setSize(170);
        $sexo->setSize(30);
        $estadocivil->setSize(105);
        $telefone->setSize(80);
        $celular->setSize(80);
        $fax->setSize(80);
        $nomepai->setSize(170);
        $nomemae->setSize(170);
        ######## Endereco ########
        $endereco->setSize(220);
        $endereco2->setSize(220);
        $bairro->setSize(150);
        $cidade->setSize(150);
        $uf->setSize(35);
        $cep->setSize(70);
        ######## Documentos ########
        $rg->setSize(40);
        $orgaorg->setSize(60);
        $pispasep->setSize(90);
        $datafgts->setSize(50);
        $cnh->setSize(70);
        $clt->setSize(90);
        $conselho_id->setSize(60);
        $numconselho->setSize(70);
        $requisitocargo->setSize(105);
        ######## Cargo ########
        $dataadmissao->setSize(35);
        $cargonovo_id->setSize(110);
        $nivelsalarial_id->setSize(34);
        $jornada_id->setSize(70);
        $escala_id->setSize(70);
        $tipojornada->setSize(70);
        $tiporegistroponto->setSize(70);
        $cargo_id->setSize(90);
        $nivelsalarialantigo_id->setSize(25);
        $relotado->setSize(25);
        $documentorelotado->setSize(90);
        $datarelotado->setSize(80);
        $quadrosuplementar->setSize(20);
        $tipofuncionario->setSize(58);
        $situacao->setSize(58);
        $cedidoorgao->setSize(100);
        $dataaposentadoria->setSize(80);
        $datacalcaposentadoria->setSize(80);
        $documentosaida->setSize(90);
        $redistribuido->setSize(25);
        $dataredistribuicao->setsize(80);
        $documentoredistribuicao->setsize(90);
        $dataquadrosuplementar->setSize(80);
        $documentoquadrosuplementar->setSize(50, 40);
        // $datasituacao->setSize(80);
        ######## Dados Fisicos ########
        $cor->setSize(65);
        $altura->setSize(50);
        $peso->setSize(50);
        $deficienteauditivo->setSize(45);
        $deficientevisual->setSize(45);
        $deficientefisico->setSize(45);
        ######## Outros ########
        $inf_complementares->setSize(180);


        //Define o auto-sugerir
        $telefone->setProperty('placeholder', '(84)1234-5678');
        $cpf->setProperty('placeholder', 'EX 11122233300');
        $datanascimento->setProperty('placeholder', '01/12/2013');
        $cpf->setProperty('placeholder', 'EX 000222999');
        $cep->setProperty('placeholder', 'EX 59000100');
        $email->setProperty('placeholder', 'exemplo@exemplo.com.br');
        $inf_complementares->setProperty('placeholder', 'Descri&ccedil;&atilde;o...');
        $documentosaida->setProperty('maxlength', '30');

        //campos obrigarorios
        $nome->addValidation('Nome', new TRequiredValidator); // required field
        $matricula->addValidation('Matricula', new TRequiredValidator); // required field
        $cidade->addValidation('Cidade', new TRequiredValidator); // required field
        //$cep->addValidation('CEP', new TRequiredValidator); // required field
        //$telefone->addValidation('Telefone', new TRequiredValidator); // required field
        $datanascimento->addValidation('Data Nascimento', new TRequiredValidator); // required field
        $naturalidade->addValidation('Naturalidade', new TRequiredValidator); // required field
        $rg->addValidation('RG', new TRequiredValidator); // required field
        //$email->addValidation('E-mail', new TEmailValidator); // email field
        $cpf->addValidation('CPF', new TNumericValidator); // numeric field
        $cargonovo_id->addValidation('Cargo Novo', new TNumericValidator); // numeric field
        //$peso->addValidation('Peso', new TNumericValidator); // numeric field
        //mask fields
        $cep->setMask('99999-999');
        $altura->setMask('9.99');
        // $telefone->setMask('99 9999-9999');


        // add the form fields
        ################ Pagina 01 - Dados Pessoais ################
        $page1->addRowSet('', $codigo);
        $page1->addRowSet('', $usuarioalteracao);
        $page1->addRowSet('', $dataalteracao);

        // adiciona a foto
        //mostra a foto do servidor se o metodo eh onEdit
        if (filter_input(INPUT_GET, 'method') == 'onEdit' && file_exists('app/images/servidor/servidor_' . filter_input(INPUT_GET, 'fk') . '.jpg')) {
            $foto = new TImage('app/images/servidor/servidor_' . filter_input(INPUT_GET, 'fk') . '.jpg', 'foto', 150, 150);
            $foto->style = "width: 150px; height: 200px; left: 150%; position: absolute;";
            $page1->addRowSet('', $foto)->style = 'float: left;';
            // $page1->addRowSet( new TLabel('Matsricula'),     array( $matricula,  new TLabel(''), $foto) );
        } else {
            $foto = new TImage('app/images/servidor/servidor_sem_foto.jpg', 'foto', 150, 150);
            $foto->style = "width: 150px; height: 150px; left: 150%; position: absolute;";
            $page1->addRowSet('', $foto)->style = 'float: left;';
        }

        $page1->addRowSet(new TLabel('Matricula <font color=red><b>*</b></font>'), $matricula);
        $page1->addRowSet(new TLabel('Nome <font color=red><b>*</b></font>'), $nome);
        $page1->addRowSet(new TLabel('Nome Cracha'), $nomecracha);
        $page1->addRowSet(new TLabel('CPF <font color=red><b>*</b></font>'), $cpf);
        $page1->addRowSet(new TLabel('Data Nascimento <font color=red><b>*</b></font>'), $datanascimento);
        $page1->addRowSet(new TLabel('Naturalidade <font color=red><b>*</b></font>'), $naturalidade);
        $page1->addRowSet(new TLabel('UF Naturalidade'), $ufnaturalidade);
        $page1->addRowSet(new TLabel('Nacionalidade'), $nacionalidade);
        $page1->addRowSet(new TLabel('E-mail'), $email);
        $page1->addRowSet(new TLabel('Sexo'), $sexo);
        $page1->addRowSet(new TLabel('Estado Civil'), $estadocivil);
        $page1->addRowSet(new TLabel('Telefone '), $telefone);
        $page1->addRowSet(new TLabel('Celular'), $celular);
        $page1->addRowSet(new TLabel('Fax'), $fax);
        $page1->addRowSet(new TLabel('Nome Pai'), $nomepai);
        $page1->addRowSet(new TLabel('Nome Mae'), $nomemae);
        ################ Fim Pagina 01 - Dados Pessoais ################
        ################ Inicio Pagina 02 - Enredeco ################
        $page2->addRowSet(new TLabel('Endere&ccedil;o'), $endereco);
        $page2->addRowSet(new TLabel('Endere&ccedil;o Secundario'), $endereco2);
        $page2->addRowSet(new TLabel('Bairro'), $bairro);
        $page2->addRowSet(new TLabel('Cidade <font color=red><b>*</b></font>'), $cidade);
        $page2->addRowSet(new TLabel('UF'), $uf);
        $page2->addRowSet(new TLabel('CEP'), $cep);
        ################ Inicio Pagina 02 - Enredeco ################
        ################ Inicio Pagina 03 - Dados Documentos ################
        $page3->addRowSet(new TLabel('RG <font color=red><b>*</b></font>'), $rg);
        $page3->addRowSet(new TLabel('Org&atilde;o RG'), $orgaorg);
        $page3->addRowSet(new TLabel('Pis Pasep'), $pispasep);
        $page3->addRowSet(new TLabel('Data FGTS'), $datafgts);
        $page3->addRowSet(new TLabel('CNH'), $cnh);
        $page3->addRowSet(new TLabel('Carteira de Trabalho'), $clt);
        $page3->addRowSet(new TLabel('Conselho'), $conselho_id);
        $page3->addRowSet(new TLabel('Numero Conselho'), $numconselho);
        $page3->addRowSet(new TLabel('Forma&ccedil;&atilde;o'), $requisitocargo);
        ################ Fim Pagina 03 - Dados Documentos ################
        ################ Inicio Pagina 04 - Cargo ################
        $page4->addRowSet(new TLabel('Data Admiss&atilde;o'), $dataadmissao);
        $page4->addRowSet(new TLabel('Cargo Novo <font color=red><b>*</b></font>'), $cargonovo_id);
        $page4->addRowSet(new TLabel('Nivel Salarial'), $nivelsalarial_id);
        if (Lib\Funcoes\Util::onCheckConfigFeatureProduct("srh_escala") && Lib\Funcoes\Util::onCheckConfigFeatureProduct("srh_jornada")) {
            $page4->addRowSet(new TLabel('Tipo Regime'), $tipoescala);
            $page4->addRowSet(new TLabel('Escala de Trabalho'), $escala_id);
            $page4->addRowSet(new TLabel('Jornada de Trabalho'), $jornada_id);
            $page4->addRowSet(new TLabel('Tipo da Jornada'), $tipojornada);
        } else {
            if (Lib\Funcoes\Util::onCheckConfigFeatureProduct("srh_escala")) {
                $page4->addRowSet(new TLabel('Tipo Regime'), $tipoescala);
                $page4->addRowSet(new TLabel('Escala de Trabalho'), $escala_id);
            }
            if (Lib\Funcoes\Util::onCheckConfigFeatureProduct("srh_jornada")) {
                $page4->addRowSet(new TLabel('Jornada de Trabalho'), $jornada_id);
                $page4->addRowSet(new TLabel('Tipo da Jornada'), $tipojornada);

            }
        }
        /*
         $page4->addRowSet(new TLabel('Tipo Regime'), $tipoescala);
         $page4->addRowSet(new TLabel('Jornada de Trabalho'), $jornada_id);
         $page4->addRowSet(new TLabel('Tipo da Jornada'), $tipojornada);
         $page4->addRowSet(new TLabel('Escala de Trabalho'), $escala_id);
        */
        $page4->addRowSet(new TLabel('Tipo do Registro Ponto'), $tiporegistroponto);


        $page4->addRowSet(new TLabel('Cargo Antigo'), $cargo_id);
        $page4->addRowSet(new TLabel('Nivel Salarial Antigo'), $nivelsalarialantigo_id);
        $page4->addRowSet(new TLabel('Relotado'), $relotado);
        $page4->addRowSet(new TLabel('Doc. Relotado'), $documentorelotado);
        $page4->addRowSet(new TLabel('Data Relotado por Periodo'), $datarelotado);
        $page4->addRowSet(new TLabel('Quadros Suplementar'), $quadrosuplementar);
        $page4->addRowSet(new TLabel('V&iacute;nculo<font color=red><b>*</b></font>'), $tipofuncionario);
        $page4->addRowSet(new TLabel('Situa&ccedil;&atilde;o Funcional<font color=red><b>*</b></font>'), $situacao);
        $page4->addRowSet(new TLabel('Cedido Org&atilde;o'), $cedidoorgao);
        $page4->addRowSet(new TLabel('Data Saida'), $dataaposentadoria);
        $page4->addRowSet(new TLabel('Data Calc. Aposent.'), $datacalcaposentadoria);
        $page4->addRowSet(new TLabel('Doc. Saida'), $documentosaida);
        $page4->addRowSet(new TLabel('Redistribui&ccedil;&atilde;o'), $redistribuido);
        $page4->addRowSet(new TLabel('Data Distribui&ccedil;&atilde;o'), $dataredistribuicao);
        $page4->addRowSet(new TLabel('Doc. Distribui&ccedil;&atilde;o'), $documentoredistribuicao);
        $page4->addRowSet(new TLabel('Data Quadro Suplementar'), $dataquadrosuplementar);
        $page4->addRowSet(new TLabel('Doc. Quadro Suplementar'), $documentoquadrosuplementar);
        ################ Fim Pagina 04 - Cargo ################
        ################ Inicio Pagina 05 - Dados Fisicos ################
        $page5->addRowSet(new TLabel('Ra&ccedil;a / Cor'), $cor);
        $page5->addRowSet(new TLabel('Altura'), $altura);
        $page5->addRowSet(new TLabel('Peso'), $peso);
        $page5->addRowSet(new TLabel('Deficiente Auditivo'), $deficienteauditivo);
        $page5->addRowSet(new TLabel('Deficiente Visual'), $deficientevisual);
        $page5->addRowSet(new TLabel('Deficiente Fisico'), $deficientefisico);
        ################ Fim Pagina 04 - Dados Fisicos ################
        ################ Inicio Pagina 05 - Outros ################
        $page6->addRowSet(new TLabel('Informacoes Complementares'), $inf_complementares);

        ################ Fim Pagina 05 - Outros ################
        #
        // create an action button (save)
        $save_button = new TButton('save');
        $save_button->setAction(new TAction(array($this, 'onSave')), 'Salvar');
        $save_button->setImage('ico_save.png');
        $save_button->class = 'btn btn-info';

        // create an action button (new)
        $new_button = new TButton('new');
        $new_button->setAction(new TAction(array($this, 'onEdit')), 'Novo');
        $new_button->setImage('ico_new.png');

        // create an action button (go to list)
        $action_back = new TAction(array('ServidorList', 'onSearch'));
        $action_back->setParameter('key', '' . filter_input(INPUT_GET, 'key') . '');

        $goto_button = new TButton('list');
        $goto_button->setAction($action_back, 'Voltar');
        $goto_button->setImage('ico_datagrid.gif');

        ################# Botoes #################
        //botoes superiores
        //  if (!empty(filter_input(INPUT_GET, 'fk'))) {
        if (!empty($_GET['fk'])) {
            ################ BTN Action DropDown 01 ################
            // create an action servidor (lotacao)
            $action_servidor_lotacao = new TAction(array('ServidorLotacaoDetalhe', 'onReload'));
            $action_servidor_lotacao->setParameter('fk', '' . filter_input(INPUT_GET, 'fk') . '');

            // create an action (Disposicao)
            $action_disposicao = new TAction(array('DisposicaoDetalhe', 'onReload'));
            $action_disposicao->setParameter('fk', '' . filter_input(INPUT_GET, 'fk') . '');

            // create an action (Experiencia)
            $action_experiencia = new TAction(array('ExperienciaDetalhe', 'onReload'));
            $action_experiencia->setParameter('fk', '' . filter_input(INPUT_GET, 'fk') . '');

            // create an action (Formacao)
            $action_formacao_servidor = new TAction(array('FormacaoServidorDetalhe', 'onReload'));
            $action_formacao_servidor->setParameter('fk', '' . filter_input(INPUT_GET, 'fk') . '');

            // create an action (Cargo)
            $action_cargo = new TAction(array('CargoServidorDetalhe', 'onReload'));
            $action_cargo->setParameter('fk', '' . filter_input(INPUT_GET, 'fk') . '');

            // create an action (Habilidade)
            $action_servidor_habilidade = new TAction(array('ServidorHabilidadeDetalhe', 'onReload'));
            $action_servidor_habilidade->setParameter('fk', '' . filter_input(INPUT_GET, 'fk') . '');

            // create an action (mandatoeletivo)
            $action_mandato_eletivo = new TAction(array('MandatoEletivoDetalhe', 'onReload'));
            $action_mandato_eletivo->setParameter('fk', '' . filter_input(INPUT_GET, 'fk') . '');


            // create an action  (Concessao)
            $action_concessao = new TAction(array('ConcessaoDetalhe', 'onReload'));
            $action_concessao->setParameter('fk', '' . filter_input(INPUT_GET, 'fk') . '');

            // create an action  (LicencaParticular)
            $action_licenca_particular = new TAction(array('LicencaParticularDetalhe', 'onReload'));
            $action_licenca_particular->setParameter('fk', '' . filter_input(INPUT_GET, 'fk') . '');

            // create an action (faltaservidor)
            $action_faltas_servidor = new TAction(array('FaltaServidorDetalhe', 'onReload'));
            $action_faltas_servidor->setParameter('fk', '' . filter_input(INPUT_GET, 'fk') . '');

            ################ fim BTN Action DropDown 01 ################
            ################ Action DropDown 02 ################
            // create an action (Requisicao Servidor)
            $action_requisicao_servidor = new TAction(array('RequisicaoServidorDetalhe', 'onReload'));
            $action_requisicao_servidor->setParameter('fk', '' . filter_input(INPUT_GET, 'fk') . '');

            // create an action (Dependentes)
            $action_dependentes = new TAction(array('DependentesDetalhe', 'onReload'));
            $action_dependentes->setParameter('fk', '' . filter_input(INPUT_GET, 'fk') . '');

            // create an action (Acidente)
            $action_acidente = new TAction(array('AcidenteDetalhe', 'onReload'));
            $action_acidente->setParameter('fk', '' . filter_input(INPUT_GET, 'fk') . '');

            // create an action (Producao)
            $action_producao = new TAction(array('ProducaoDetalhe', 'onReload'));
            $action_producao->setParameter('fk', '' . filter_input(INPUT_GET, 'fk') . '');

            // create an action (Servidor Capacitacao)
            $action_capacitacao = new TAction(array('ServidorCapacitacaoDetalhe', 'onReload'));
            $action_capacitacao->setParameter('fk', '' . filter_input(INPUT_GET, 'fk') . '');

            // create an action (Servidor Expertise)
            $action_expertise = new TAction(array('ServidorExpertiseDetalhe', 'onReload'));
            $action_expertise->setParameter('fk', '' . filter_input(INPUT_GET, 'fk') . '');

            // create an action (Solicitacao)
            $action_solicitacao = new TAction(array('SolicitacaoDetalhe', 'onReload'));
            $action_solicitacao->setParameter('fk', '' . filter_input(INPUT_GET, 'fk') . '');

            // create an action (Averbacao)
            $action_averbacao = new TAction(array('AverbacaoDetalhe', 'onReload'));
            $action_averbacao->setParameter('fk', '' . filter_input(INPUT_GET, 'fk') . '');

            // create an action (Suspensao)
            $action_suspensao = new TAction(array('SuspensaoServidorDetalhe', 'onReload'));
            $action_suspensao->setParameter('fk', '' . filter_input(INPUT_GET, 'fk') . '');

            // create an action (Funcao)
            $action_funcao = new TAction(array('FuncaoServidorDetalhe', 'onReload'));
            $action_funcao->setParameter('fk', '' . filter_input(INPUT_GET, 'fk') . '');

            // create an action (Vacancia)
            $action_vacancia = new TAction(array('VacanciaDetalhe', 'onReload'));
            $action_vacancia->setParameter('fk', '' . filter_input(INPUT_GET, 'fk') . '');

            // create an action (Ferias)
            $action_ferias = new TAction(array('FeriasServidorDetalhe', 'onReload'));
            $action_ferias->setParameter('fk', '' . filter_input(INPUT_GET, 'fk') . '');

            // create an action (Provento)
            $action_provento = new TAction(array('ServidorProventoDetalhe', 'onReload'));
            $action_provento->setParameter('fk', '' . filter_input(INPUT_GET, 'fk') . '');
            ################ fim Action DropDown 02 ################
            //Btn dropdown 01
            $dtn_dropdown01 = new TDropDown('Funcional', 'fa:list');
            $dtn_dropdown01->addAction('Lota&ccedil;&atilde;o', $action_servidor_lotacao);
            $dtn_dropdown01->addAction('Provento', $action_provento);
            if (Lib\Funcoes\Util::onCheckConfigFeatureProduct("averbacao")) {
                $dtn_dropdown01->addAction('Averba&ccedil;&atilde;o', $action_averbacao);
            }
            $dtn_dropdown01->addAction('Fun&ccedil;&atilde;o', $action_funcao);

            if (Lib\Funcoes\Util::onCheckConfigFeatureProduct("ferias")) {
                $dtn_dropdown01->addAction('F&eacute;rias', $action_ferias);
            }

            //Btn dropdown 02
            $dtn_dropdown02 = new TDropDown('Pessoal', 'fa:list');
            if (Lib\Funcoes\Util::onCheckConfigFeatureProduct("formacao")) {
                $dtn_dropdown02->addAction('Titula&ccedil;&atilde;o', $action_formacao_servidor);
            }
            if (Lib\Funcoes\Util::onCheckConfigFeatureProduct("experiencia_academica")) {
                $dtn_dropdown02->addAction('Experiencia', $action_experiencia);
            }
            $dtn_dropdown02->addAction('Capacita&ccedil;&atilde;o', $action_capacitacao);
            $dtn_dropdown02->addAction('Produ&ccedil;&atilde;o', $action_producao);
            $dtn_dropdown02->addAction('Expertise', $action_expertise);
            $dtn_dropdown02->addAction('Habilidades', $action_servidor_habilidade);
            if (Lib\Funcoes\Util::onCheckConfigFeatureProduct("dependentes")) {
                $dtn_dropdown02->addAction('Dependentes', $action_dependentes);
            }

            //Btn dropdown 03
            $dtn_dropdown03 = new TDropDown('Saidas', 'fa:list');
            $dtn_dropdown03->addAction('Acidente', $action_acidente);
            $dtn_dropdown03->addAction('Concess&atilde;o', $action_concessao);
            if (Lib\Funcoes\Util::onCheckConfigFeatureProduct("licenca")) {
                $dtn_dropdown03->addAction('Licen&ccedil;a', $action_licenca_particular);
            }
            $dtn_dropdown03->addAction('Penalidade', $action_suspensao);
            if (Lib\Funcoes\Util::onCheckConfigFeatureProduct("vacancia")) {
                $dtn_dropdown03->addAction('Vacancia', $action_vacancia);
            }
            $dtn_dropdown03->addAction('Faltas', $action_faltas_servidor);

            if (Lib\Funcoes\Util::onCheckConfigFeatureProduct("Mandato Eletivo")) {
                $dtn_dropdown03->addAction('Mandato Eletivo', $action_mandato_eletivo);
            }

            //Btn dropdown 03
            $dtn_dropdown04 = new TDropDown('Outros', 'fa:list');
            //   $dtn_dropdown04->addAction('Cargo', $action_cargo);
            $dtn_dropdown04->addAction('Disposi&ccedil;&atilde;o', $action_disposicao);
            $dtn_dropdown04->addAction('Solicita&ccedil;&atilde;o', $action_solicitacao);
            $dtn_dropdown04->addAction('Requisicao Servidor', $action_requisicao_servidor);

            $acaoRadio = new TAction(array($this, 'onChangeRadio'));
            $acaoRadio->setParameter('formName', $this->form->getName());
            $tipoescala->setChangeAction($acaoRadio);
            // self::onChangeRadio(array('tipoescala' => 1));

            //botoes superior
            // add a row for the TVBox
            $row = $tb_btn_header->addRow();
            $row->addCell($dtn_dropdown01);
            $row->addCell($dtn_dropdown02);
            $row->addCell($dtn_dropdown03);
            $row->addCell($dtn_dropdown04);
        }

        ################# Fim Botoes header #################
        //botoes inferiores
        // add a row for the TVBox
        $row = $tb_btn_footer->addRow();
        $row->addCell($save_button)->style = 'float: left;';
        $row->addCell($goto_button)->style = 'float: left;';

        // define wich are the form fields
        $this->form->setFields(array($codigo, $nome, $nomecracha, $cpf, $matricula, $endereco,
            $endereco2, $bairro, $cidade, $uf, $cep, $cnh, $telefone, $celular,
            $fax, $email, $dataadmissao, $datanascimento, $sexo, $estadocivil,
            $naturalidade, $ufnaturalidade, $clt, $pispasep, $datafgts, $rg,
            $orgaorg, $numconselho, $conselho_id, $requisitocargo, $cargonovo_id, $cedidoorgao,
            $cargo_id, $nivelsalarial_id, $nivelsalarialantigo_id, $redistribuido,
            $dataredistribuicao, $documentoredistribuicao, $relotado, $datarelotado,
            $documentorelotado, $dataquadrosuplementar, $documentoquadrosuplementar,
            $quadrosuplementar, $tipofuncionario, $situacao, $dataaposentadoria,
            $documentosaida, $datacalcaposentadoria, $nomemae, $nomepai, $nacionalidade, $cor, $altura,
            $peso, $deficienteauditivo, $deficientevisual, $deficientefisico,
            $usuarioalteracao, $dataalteracao, $jornada_id, $inf_complementares,
            $save_button, $new_button, $goto_button, $tipoescala, $escala_id, $tipojornada, $tiporegistroponto
        ));

        // wrap the page content using vertical box
        $vbox = new TVBox;

        $vbox->add($titulo);
        if (!empty($_GET['fk'])) {
            $vbox->add($tb_btn_header);
        }
        $vbox->add($this->form);
        $vbox->add($campObg);
        $vbox->add($tb_btn_footer);

        parent::add($vbox);
    }

    /**
     * method onSave()
     * Executed whenever the user clicks at the save button
     */
    function onSave()
    {
        try {
            // open a transaction with database 'samples'
            TTransaction::open('pg_ceres');

            $object = $this->form->getData("ServidorRecord");    // get the form data into an Servidor Active Record 
            $object->nome = strtoupper($object->nome);
            //lanca o default
            $object->usuarioalteracao = $_SESSION['usuario'];
            $object->dataalteracao = date("d/m/Y H:i:s");
            $object->empresa_id = $_SESSION['empresa_id'];

            if ($object->tipoescala == 1) {//jornada
                $object->escala_id = '';

            } else if ($object->tipoescala == 2) { //escala
                $object->jornada_id = '';
                $object->tipojornada = '';

            }
            unset($object->tipoescala);

            $this->form->validate();

            $object->store();  // stores the object

            $this->form->setData($object);

            $param = [];
            $param['key'] = $object->id;
            $param['fk'] = $object->id;

            TApplication::gotoPage('ServidorForm', 'onEdit', $param); // reload

            TTransaction::close();

            new TMessage('info', 'Registro Salvo com Sucesso');
        } catch (Exception $e) {
            new TMessage('error', '<b>Error</b> ' . $e->getMessage());
            TTransaction::rollback();
            $this->form->setData($this->form->getData());
        }
    }

    /**
     * method onEdit()
     * Executed whenever the user clicks at the edit button da datagrid
     */
    function onEdit($param)
    {
        try {
            if (isset($param['fk'])) {
                // get the parameter $key
                $key = $param['fk'];

                TTransaction::open('pg_ceres');   // open a transaction with database 'samples'
                $object = new ServidorRecord($key);        // instantiates object City
                //converter datas do form para formato Brasileiro
                $object->dataadmissao = TDate::date2br($object->dataadmissao);
                $object->datanascimento = TDate::date2br($object->datanascimento);
                $object->dataorgaofgts = TDate::date2br($object->dataorgaofgts);
                $object->dataredistribuicao = TDate::date2br($object->dataredistribuicao);
                $object->datarelotado = TDate::date2br($object->datarelotado);
                $object->dataaposentadoria = TDate::date2br($object->dataaposentadoria);
                $object->datacalcaposentadoria = TDate::date2br($object->datacalcaposentadoria);
                $object->dataquadrosuplementar = TDate::date2br($object->dataquadrosuplementar);

                if (empty($object->escala_id)) {
                    $this->onChangeRadio(['tipoescala' => '1']); //mantem selecao do combo dinamico
                    $object->tipoescala = '1';
                } else { //jornada
                    $this->onChangeRadio(['tipoescala' => '2']);
                    $object->tipoescala = '2';
                }

                $this->form->setData($object);   // fill the form with the active record data
                TTransaction::close();           // close the transaction
            } else {
                $this->form->clear();
            }
        } catch (Exception $e) { // in case of exception
            new TMessage('error', '<b>Error</b> ' . $e->getMessage());
            TTransaction::rollback();
        }
    }

    public static function onChangeRadio($param)
    {
        switch ($param['tipoescala']) {
            case '1':
                //   TDBCombo::clearField($param['formName'], 'escala_id');
                TCombo::enableField('form_Servidor', 'jornada_id');
                TCombo::enableField('form_Servidor', 'tipojornada');
                TCombo::disableField('form_Servidor', 'escala_id');

                break;

            case '2':
                //  TCombo::clearField($param['formName'], 'jornada_id');
                //TCombo::clearField($param['formName'], 'tipojornada');
                TCombo::disableField('form_Servidor', 'jornada_id');
                TCombo::disableField('form_Servidor', 'tipojornada');
                TCombo::enableField('form_Servidor', 'escala_id');

                break;

        }
    }

}