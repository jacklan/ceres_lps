<?php
/*
 * classe CapacitacaoForm
 * Cadastro de Capacitacao: Contem o formularo
 */


class CapacitacaoForm extends TPage
{
    private $form;     // formulario de cadastro
    
    /*
     * metodo construtor
     * Cria a pagina do formulario
     */
    public function __construct()
    {
        parent::__construct();

        // instancia um formulario
        $this->form = new TQuickForm;
        $this->form->class = 'form_capacitacao';
        $this->form->setFormTitle('<font color="red" size="3" face="Arial"><b>Formul&aacute;rio de Capacita&ccedil;&atilde;o</b></font>');

        // cria os campos do formulario
        $codigo     = new THidden('id');
        $codigo->setEditable(false);
        $nome  = new TEntry('nome');
        $tipo = new TCombo('tipo');
        $cargahoraria = new TEntry('cargahoraria');
        $investimento = new TEntry('investimento');
        $localrealizacao = new TEntry('localrealizacao');
        $instituicaopromotora = new TEntry('instituicaopromotora');
        $areaconhecimento_id = new TCombo('areaconhecimento_id');          
		$tematica = new TEntry('tematica');

        $usuarioalteracao = new THidden('usuarioalteracao');
        $dataalteracao = new THidden('dataalteracao');

		// cria um rotulo para o titulo
        $titulo = new TLabel('<div style="position:floatval; width: 200px;"> <b>* Campo obrigatorio</b></div>');
        $titulo->setFontFace('Arial');
        $titulo->setFontColor('red');
        $titulo->setFontSize(10);
		
        //Carrega combo area conhecimento
        TTransaction::open('pg_ceres');
        $repository = new TRepository('AreaConhecimentoRecord');
        // cria um criterio de selecao, ordenado pelo id
        $criteria = new TCriteria;
        $criteria->setProperty('order', 'nome');
        // carrega os objetos de acordo com o criterio
        $cadastros = $repository->load($criteria);

        //adiciona os objetos no combo
        foreach ($cadastros as $object){
            $items1[$object->id] = $object->nome;
        }

        // adiciona as opcoes na combo
        $areaconhecimento_id->addItems($items1);
         // finaliza a transacao
        TTransaction::close();


        //Cria um vetor com as opcoes da combo
        $items= array();
        $items['INTERNA'] = 'INTERNA';
        $items['EXTERNA'] = 'EXTERNA';

         // adiciona as opcoes na combo
        $tipo->addItems($items);
        //coloca o valor padrao no combo
        $tipo->setValue('INTERNA');


        // define os campos
        $this->form->addQuickField(null, $codigo, 10);
        $this->form->addQuickField(null, $usuarioalteracao, 10);
        $this->form->addQuickField(null, $dataalteracao, 10);
        $this->form->addQuickField('Nome <font color=red><b>*</b></font>', $nome, 50);
		$this->form->addQuickField('Tipo <font color=red><b>*</b></font>', $tipo, 50);
        $this->form->addQuickField('Carga Horaria', $cargahoraria, 50);
        $this->form->addQuickField('Investimento', $investimento, 50);
        $this->form->addQuickField('Local', $localrealizacao, 50);
        $this->form->addQuickField('Inst. Promotora', $instituicaopromotora, 50);
        $this->form->addQuickField('Area Reconhecimento <font color=red><b>*</b></font>', $areaconhecimento_id, 50);
        $this->form->addQuickField('Tematica', $tematica, 50);
        $this->form->addQuickField(null, $titulo, 50);

        $nome->setProperty('placeholder', 'Nome');
        $nome->setProperty('required', 'required');
        $tipo->setProperty('placeholder', 'Endereço');
        $tipo->setProperty('required', 'required');
        $areaconhecimento_id->setProperty('placeholder', 'Bairro');
        
		
        // cria um botao de acao
        $this->form->addQuickAction('Salvar', new TAction(array($this, 'onSave')), 'ico_save.png');
        $this->form->addQuickAction('Voltar', new TAction(array('CapacitacaoList', 'onReload')), 'ico_datagrid.gif');
        // adiciona a tabela a pagina
        parent::add($this->form);
    }
    
   
    /*
     * metodo onSave()
     * Executada quando o usuario clicar no botao salvar do formulario
     */
    function onSave()
    {
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // obtem os dados no formulario em um objeto CarroRecord
        $cadastro = $this->form->getData('CapacitacaoRecord');

        //antes de armazenar verifica se algum campo eh requerido e nao foi informado
        $dados = $cadastro->toArray();

        $msg = '';
        $icone = 'info';

        if (empty ($dados['nome'])){
           $msg .= 'O Nome deve ser informado.<br/>';
        }
        if (empty ($dados['tipo'])){
           $msg .= 'O Tipo deve ser informado.<br/>';
         }

           if (empty ($dados['areaconhecimento_id'])){
           $msg .= 'A Area Conhecimento deve ser informada.<br/>';

         }

        try{

            if ($msg == ''){
              // armazena o objeto
              $cadastro->store();
              $msg = 'Dados armazenados com sucesso';

              // finaliza a transacao
              TTransaction::close();

            }else{
                $icone = 'error';
            }
            
            if ($icone == 'error'){
                // exibe mensagem de erro
                new TMessage($icone, $msg);
				$this->form->setData($cadastro);   // fill the form with the active record data
            }else{
                //chama o formulario com o grid
                				
				new TMessage("info", "Registro salvo com sucesso!");
				TApplication::gotoPage('CapacitacaoList','onReload'); // reload
            }
        }
        catch (Exception $e) // em caso de exce��o
        {
            // exibe a mensagem gerada pela exce��o
            new TMessage('error', $e->getMessage());
            // desfaz todas altera��es no banco de dados
            TTransaction::rollback();
            $this->form->setData($cadastro);   // fill the form with the active record data
        }

    }
    
    
    /*
     * metodo onEdit()
     * Edita os dados de um registro
     */
    function onEdit($param)
    {
		try {
            if (isset($param['key'])) {

                // get the parameter $key
                $key = $param['key'];

                TTransaction::open('pg_ceres');   // open a transaction with database 'samples'

                $object = new CapacitacaoRecord($key);        // instantiates object City

                $this->form->setData($object);   // fill the form with the active record data

                TTransaction::close();           // close the transaction
            } else {
                $this->form->clear();
            }
        } catch (Exception $e) { // in case of exception
            // shows the exception error message
            new TMessage('error', '<b>Error</b> ' . $e->getMessage());
            // undo all pending operations
            TTransaction::rollback();
        }		
        
    }

}
?>