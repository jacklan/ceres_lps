<?php

/*
 * classe CertificadoForm
 * Cadastro de Certificado: Contem o formularo
 * Autor: Jackson Meires
 * Data:30/09/2016
 */

//ini_set('display_errors', 1);
//ini_set('display_startup_erros', 1);
//error_reporting(E_ALL);

class CertificadoForm extends TPage {

    private $form;     // formulario de cargo

    /*
     * metodo construtor
     * Cria a pagina do formulario
     */

    public function __construct() {

        parent::__construct();

        // instancia um formulario
        $this->form = new TQuickForm;
        $this->form->class = 'form_Certificado';
        $this->form->setFormTitle('<font color="red" size="3" face="Arial"><b>Formul&aacute;rio de Certificado</b></font>');

        // cria os campos do formulario
        $codigo = new THidden('id');
        $curso = new TEntry('curso');
        $descricao = new TText('descricao');
        $conteudo = new Adianti\Widget\Form\THtmlEditor('conteudo');
        $cargahoraria = new TEntry('cargahoraria');
        $datainicio = new TDate('datainicio');
        $datafim = new TDate('datafim');
        $municipio_id = new TDBCombo('municipio_id', 'pg_ceres', 'MunicipioRecord', 'id', 'nome', 'nome');
        $uf = new TCombo('uf');
        $assinatura_coordenador = new TCombo('assinatura_coordenador');
        $usuarioalteracao = new THidden('usuarioalteracao');
        $dataalteracao = new THidden('dataalteracao');
        $lb_obs = new TLabel('<font color=red><b>OBS: Para carregar nome do participante adicione esta tag entre o texto: {participante}</b></font>');

        // cria um vetor com as opcoes da combo estado
        $items = array();
        $items['AC'] = 'AC';
        $items['AL'] = 'AL';
        $items['AM'] = 'AM';
        $items['AP'] = 'AP';
        $items['BA'] = 'BA';
        $items['CE'] = 'CE';
        $items['DF'] = 'DF';
        $items['ES'] = 'ES';
        $items['GO'] = 'GO';
        $items['MA'] = 'MA';
        $items['MG'] = 'MG';
        $items['MS'] = 'MS';
        $items['MT'] = 'MT';
        $items['PA'] = 'PA';
        $items['PB'] = 'PB';
        $items['PE'] = 'PE';
        $items['PI'] = 'PI';
        $items['PR'] = 'PR';
        $items['RJ'] = 'RJ';
        $items['RN'] = 'RN';
        $items['RO'] = 'RO';
        $items['RR'] = 'RR';
        $items['RS'] = 'RS';
        $items['SC'] = 'SC';
        $items['SE'] = 'SE';
        $items['SP'] = 'SP';
        $items['TO'] = 'TO';

        // adiciona as opcoes na combo
        $uf->addItems($items);
        //coloca o valor padrao no combo
        $uf->setValue('RN');

        // cria um vetor com as opcoes da combo estado
        $items_assinatura = array();
        $items_assinatura['ADRIANA AMERICO DE SOUZA'] = 'Adriana Americo de Souza';
        $items_assinatura['ELTON DANTAS DE OLIVEIRA'] = 'Elton Dantas de Oliveira';
        $items_assinatura['LEILA MESQUITA'] = 'Leila Daniele F.da Silva Mesquita';
        $items_assinatura['ARIAMELIA BANDEIRA CRUZ FEITOSA'] = 'Ariamelia Bandeira Cruz Feitosa';
        // adiciona as opcoes na combo
        $assinatura_coordenador->addItems($items_assinatura);
        //coloca o valor padrao no combo
        $assinatura_coordenador->setValue('LEILA MESQUITA');

        // cria um rotulo para o titulo
        $lb_titulo = new TLabel('<div style="position:floatval; width: 200px;"> <b>* Campo obrigatorio</b></div>');
        $lb_titulo->setFontFace('Arial');
        $lb_titulo->setFontColor('red');
        $lb_titulo->setFontSize(10);

        // define os campos
        $this->form->addQuickField(null, $codigo, 10);
        $this->form->addQuickField(null, $usuarioalteracao, 10);
        $this->form->addQuickField(null, $dataalteracao, 10);
        // add the fields inside the form
        // $this->form->setFieldsByRow(2);
        $this->form->addQuickField("Curso <font color=red><b>*</b></font>", $curso, 70);
        $this->form->addQuickField("Descri&ccedil;&atilde;o <font color=red><b>*</b></font>", $descricao, 50);
        $this->form->addQuickField("", $lb_obs, 600);
        $this->form->addQuickField("Conte&uacute;do <font color=red><b>*</b></font>", $conteudo, 50);
        $this->form->addQuickField("Carga Hor&aacute;ria <font color=red><b>*</b></font>", $cargahoraria, 10);
        $this->form->addQuickField("Data Inicio", $datainicio, 15);
        $this->form->addQuickField("Data Fim", $datafim, 15);
        $this->form->addQuickField("Munic&iacute;pio", $municipio_id, 40);
        $this->form->addQuickField("UF", $uf, 20);
        $this->form->addQuickField("Assinatura Coordenador <font color=red><b>*</b></font>", $assinatura_coordenador, 35);
        $this->form->addQuickField(null, $lb_titulo, 50);

        //Campos obrigatórios
        $curso->addValidation('Curso', new TRequiredValidator); // required field
        $descricao->addValidation('Descri&ccedil;&atilde;o', new TRequiredValidator); // required field
        $conteudo->addValidation('Conte&uacute;do', new TRequiredValidator); // required field
        $cargahoraria->addValidation('Carga Hor&aacute;ria', new TRequiredValidator); // required field
        $datainicio->addValidation('Data Inicio', new TRequiredValidator); // required field
        $datafim->addValidation('Data Fim', new TRequiredValidator); // required field
        $municipio_id->addValidation('Munic&iacute;pio', new TRequiredValidator); // required field
        $uf->addValidation('UF', new TRequiredValidator); // required field

        $descricao->setSize(70, 200);
        $conteudo->setSize(600, 400);
//        $conteudo->setSize(70, 200);
        //Define o auto-sugerir
        $descricao->setProperty('placeholder', 'EX:
            Certificamos que Jose de Arimateia&#10;
            participou da Formação para Agentes de ATER,&#10;
            executada pelo Instituto de Assistência Técnica e&#10;
            Extensão Rural do Rio Grande do Norte –&#10;
            EMATER-RN, no Centro de Treinamento da&#10;
            EMATER/RN, nos dias 25 de maio a 05 de junho&#10;
            de 2015, num total de 80 horas.');
        $conteudo->setProperty('placeholder', 'EX:&#10;
            Abertura&#10;
            Diretoria da EMATER&#10;
            Apresentação dos Participantes: Dinâmica de grupo.&#10;
            Leila Mesquita e Klediógenes Fernandes- Coordenação do Treinamento EMATER 60 Anos: &#10;Desafios e Perspectivas a Partir das Novas Diretrizes Estratégicas Institucionais&#10;
            Cesar José de Oliveira - Diretor Geral da EMATER/RN Papel dos Escritórios Locais&#10;
            Cesar Oliveira, Aristides Bezerra, José Péricles - Diretoria da EMATER/RN&#10;
            A Construção Epistemológica do Conhecimento Francisco Nogueira (IFPB)&#10;
            A Agroecologia e a Construção do Conhecimento: Novos Desafios');

        // cria um botao de acao
        $this->form->addQuickAction('Salvar', new TAction(array($this, 'onSave')), 'ico_save.png')->class = 'btn btn-info';
        $this->form->addQuickAction('Voltar', new TAction(array('CertificadoList', 'onReload')), 'ico_datagrid.gif');

        // adiciona a tabela a pagina
        parent::add($this->form);
    }

    /*
     * metodo onSave()
     * Executada quando o usuario clicar no botao salvar do formulario
     */

    function onSave() {
        try {

            // inicia transacao com o banco 'pg_ceres'
            TTransaction::open('pg_ceres');

            // obtem os dados no formulario em um objeto CarroRecord
            $cargo = $this->form->getData('CertificadoRecord');

            //lanca o default
            $cargo->usuarioalteracao = $_SESSION['usuario'];
            $cargo->dataalteracao = date("d/m/Y H:i:s");
            $cargo->empresa_id = $_SESSION['empresa_id'];

            $msg = '';
            $icone = 'info';

            // form validation
            $this->form->validate();

            if ($msg == '') {

                // armazena o objeto no banco
                $cargo->store();

                $msg = 'Dados armazenados com sucesso';
            } else {
                $icone = 'error';
            }

            if ($icone == 'error') {

                // exibe mensagem de erro
                new TMessage($icone, $msg);

                $this->form->setData($cargo);   // fill the form with the active record data
            } else {

                // exibe um dialogo ao usuario
                new TMessage("info", $msg, new TAction(array('CertificadoList', 'onReload')));
            }
            TTransaction::close();           // close the transaction
        } catch (Exception $e) {

            // em caso de exc
            // exibe a mensagem gerada pela exce��o
            new TMessage('error', $e->getMessage());

            // desfaz todas altera��es no banco de dados
            TTransaction::rollback();

            $this->form->setData($this->form->getData());   // fill the form with the active record data
        }
    }

    /*
     * metodo onEdit()
     * Edita os dados de um registro
     */

    function onEdit($param) {

        try {

            if (isset($param['key'])) {

                // get the parameter $key
                $key = $param['key'];

                TTransaction::open('pg_ceres');   // open a transaction with database 'samples'

                $object = new CertificadoRecord($key);  // instantiates object City

                $object->datainicio = \Adianti\Widget\Form\TDate::date2br($object->datainicio);
                $object->datafim = \Adianti\Widget\Form\TDate::date2br($object->datafim);

                $this->form->setData($object);   // fill the form with the active record data

                TTransaction::close();           // close the transaction
            } else {

                $this->form->clear();
            }
        } catch (Exception $e) {

            // in case of exception
            // shows the exception error message
            new TMessage('error', '<b>Error</b> ' . $e->getMessage());

            // undo all pending operations
            TTransaction::rollback();
        }
    }

}
