<?php

//ini_set('display_errors', 1);
//ini_set('display_startup_erros', 1);
//error_reporting(E_ALL);


use Adianti\Database\TFilter1;
use Adianti\Widget\Datagrid\TDatagridTables;

class BairroCoordenadasDetalhe extends TPage {

    private $form;
    private $datagrid;

    public function __construct() {
        parent::__construct();

        $this->form = new TQuickForm;
        $this->form->class = 'form_BairroCoordenadas';
        $this->form->setFormTitle('<font color="red" size="3" face="Arial"><b>Detalhe das Coordenadas</b></font>');

        $titulo = new TLabel('<div style="position:floatval; width: 200px;"> <b>* Campo obrigatorio</b></div>');
        $titulo->setFontFace('Arial');
        $titulo->setFontColor('red');
        $titulo->setFontSize(10);

        $codigo = new THidden('id');
        $codigo->setEditable(false);
        $bairro_id = new THidden('bairro_id');
        $bairro_id->setValue(filter_input(INPUT_GET, 'fk'));
        $latitude = new TEntry('latitude');
        $longitude = new TEntry('longitude');

        $usuarioalteracao = new THidden('usuarioalteracao');
        $dataalteracao = new THidden('dataalteracao');

        $grausLat = new TEntry('grausLat');
        $minutosLat = new TEntry('minutosLat');
        $segundosLat = new TEntry('segundosLat');
        $grausLon = new TEntry('grausLon');
        $minutosLon = new TEntry('minutosLon');
        $segundosLon = new TEntry('segundosLon');
        $tipoCoord = new TRadioGroup('tipoCoord');

        $grausLat->setSize(95);
        $minutosLat->setSize(95);
        $segundosLat->setSize(95);
        $grausLon->setSize(95);
        $minutosLon->setSize(95);
        $segundosLon->setSize(95);

        $grausLat->setMask('00');
        $minutosLat->setMask('00');
        $segundosLat->setMask('00.000');
        $grausLon->setMask('000');
        $minutosLon->setMask('00');
        $segundosLon->setMask('00.000');

        $grausLat->disableField($this->form->getName(), 'grausLat');
        $minutosLat->disableField($this->form->getName(), 'minutosLat');
        $segundosLat->disableField($this->form->getName(), 'segundosLat');
        $grausLon->disableField($this->form->getName(), 'grausLon');
        $minutosLon->disableField($this->form->getName(), 'minutosLon');
        $segundosLon->disableField($this->form->getName(), 'segundosLon');

        $grausLat->setProperty('placeholder', 'Ex.: 0&#176; a 90&#176;');
        $minutosLat->setProperty('placeholder', 'Ex.: 0m a 59m');
        $segundosLat->setProperty('placeholder', 'Ex.: 0s a 59s');
        $grausLon->setProperty('placeholder', 'Ex.: 0&#176; a 180&#176;');
        $minutosLon->setProperty('placeholder', 'Ex.: 0m a 59m');
        $segundosLon->setProperty('placeholder', 'Ex.: 0s a 59s');

        $ajuda = new TLabel('<a href="https://support.google.com/maps/answer/2533464?hl=pt-BR" target="_blank">'
                . '<font style="font-size:15px">Como preencher as coordenadas em '
                . '<font style="color:#FF0000"><b>DD ou DMS</b></font></font></a>');
        $ajuda->setFontFace('Arial');
        $ajuda->setFontSize(10);

        $obrigatorio = new TLabel('<div style="position:floatval; width: 200px;">'
                . '<b>* Campos obrigat&oacute;rios</b></div>');
        $obrigatorio->setFontFace('Arial');
        $obrigatorio->setFontColor('red');
        $obrigatorio->setFontSize(10);

        $tipoCoord->addItems(array(
            '1' => 'Coordenadas DD (Graus decimais)',
            '2' => 'Coordenadas DMS (Graus, minutos e segundos)'
        ));
        $tipoCoord->setLayout('horizontal');
        $tipoCoord->setValue('1');
        
        TTransaction::open('pg_ceres');

        $object = new BairroRecord(filter_input(INPUT_GET, 'fk'));
        $nome_bairro = new TLabel($object->nome);
        TTransaction::close();

        $latitude->setProperty('placeholder', 'Ex.: -5,828999');
        $longitude->setProperty('placeholder', 'Ex.: -35,215468');

        $this->form->addQuickField(null, $codigo, 10);
        $this->form->addQuickField('Bairro', $nome_bairro, 100);
        $this->form->addQuickField(null, $usuarioalteracao, 10);
        $this->form->addQuickField(null, $dataalteracao, 10);
        $this->form->addQuickField(null, $bairro_id, 40);

        $this->form->addQuickField(null, $ajuda, 400);
        $this->form->addQuickField(null, $tipoCoord, 10);
        $this->form->addQuickField("Latitude", $latitude, 20);
        $this->form->addQuickField("Longitude", $longitude, 20);
        $this->form->addQuickFields('Latitude', array(
            $grausLat, new TLabel('<font size="4">&#176;</font>'),
            $minutosLat, new TLabel('<font size="4">&#39;</font>'),
            $segundosLat, new TLabel('<font size="4">&#34;S</font>')
        ));
        $this->form->addQuickFields('Longitude', array(
            $grausLon, new TLabel('<font size="4">&#176;</font>'),
            $minutosLon, new TLabel('<font size="4">&#39;</font>'),
            $segundosLon, new TLabel('<font size="4">&#34;W</font>')
        ));
        $this->form->addQuickField(null, $obrigatorio, 50);

        $latitude->addValidation('Latirude', new TRequiredValidator);
        $longitude->addValidation('Longitude', new TRequiredValidator);

        $acaoRadio = new TAction(array($this, 'onChangeRadio'));
        $acaoRadio->setParameter('formName', $this->form->getName());
        $tipoCoord->setChangeAction($acaoRadio);
        
        $action1 = new TAction(array($this, 'onSave'));
        $action1->setParameter('key', filter_input(INPUT_GET, 'key'));
        $action1->setParameter('fk', filter_input(INPUT_GET, 'fk'));
        $action1->setParameter('did', filter_input(INPUT_GET, 'did'));

        $action2 = new TAction(array('BairroDetalhe', 'onReload'));
        $action2->setParameter('fk', filter_input(INPUT_GET, 'did'));

        $this->form->addQuickAction('Salvar', $action1, 'ico_save.png');
        $this->form->addQuickAction('Voltar', $action2, 'ico_datagrid.gif');
        $this->datagrid = new TDatagridTables;

        $dglatitude = new TDataGridColumn('latitude', 'Latitude', 'left', 1350);
        $dglongitude = new TDataGridColumn('longitude', 'Longitude', 'left', 1350);

        $this->datagrid->addColumn($dglatitude);
        $this->datagrid->addColumn($dglongitude);

        $action1 = new TDataGridAction(array($this, 'onEdit'));
        $action1->setLabel('Editar');
        $action1->setImage('ico_edit.png');
        $action1->setField('id');
        $action1->setFk('bairro_id');
        $action1->setParameter('did', filter_input(INPUT_GET, 'did'));

        $action2 = new TDataGridAction(array($this, 'onDelete'));
        $action2->setLabel('Deletar');
        $action2->setImage('ico_delete.png');
        $action2->setField('id');
        $action2->setFk('bairro_id');
        $action2->setParameter('did', filter_input(INPUT_GET, 'did'));

        $this->datagrid->addAction($action1);
        $this->datagrid->addAction($action2);
        $this->datagrid->createModel();

        $panel = new TPanelForm(700, 500);
        $panel->put($this->form, 0, 0);
        $panel->put($this->datagrid, 150, 210);

        parent::add($panel);
    }

    public static function onChangeRadio($param) {

        switch ($param['tipoCoord']) {

            case '1':
                TEntry::clearField($param['formName'], 'grausLat');
                TEntry::clearField($param['formName'], 'minutosLat');
                TEntry::clearField($param['formName'], 'segundosLat');
                TEntry::clearField($param['formName'], 'grausLon');
                TEntry::clearField($param['formName'], 'minutosLon');
                TEntry::clearField($param['formName'], 'segundosLon');

                TEntry::disableField($param['formName'], 'grausLat');
                TEntry::disableField($param['formName'], 'minutosLat');
                TEntry::disableField($param['formName'], 'segundosLat');
                TEntry::disableField($param['formName'], 'grausLon');
                TEntry::disableField($param['formName'], 'minutosLon');
                TEntry::disableField($param['formName'], 'segundosLon');

                TEntry::enableField($param['formName'], 'latitude');
                TEntry::enableField($param['formName'], 'longitude');
                break;

            case '2':
                TEntry::clearField($param['formName'], 'latitude');
                TEntry::clearField($param['formName'], 'longitude');

                TEntry::disableField($param['formName'], 'latitude');
                TEntry::disableField($param['formName'], 'longitude');

                TEntry::enableField($param['formName'], 'grausLat');
                TEntry::enableField($param['formName'], 'minutosLat');
                TEntry::enableField($param['formName'], 'segundosLat');
                TEntry::enableField($param['formName'], 'grausLon');
                TEntry::enableField($param['formName'], 'minutosLon');
                TEntry::enableField($param['formName'], 'segundosLon');
                break;
        }
    }
    
    function onReload() {
        TTransaction::open('pg_ceres');

        $repository = new TRepository('BairroCoordenadasRecord');
        $criteria = new TCriteria;
        $criteria->add(new TFilter('bairro_id', '=', filter_input(INPUT_GET, 'fk')));
        $registros = $repository->count($criteria);
        $cadastros = $repository->load($criteria);

        $this->datagrid->clear();
        if ($cadastros) {
            foreach ($cadastros as $cadastro) {
                $this->datagrid->addItem($cadastro);
            }
        }
        TTransaction::close();
        $this->loaded = true;
    }
    
    
    function onDelete($param) {
    
    	// obtem o parametro $key
    	$key = $param['key'];
    
    	// define duas acoes
    	$action1 = new TAction(array($this, 'Delete'));
    	$action2 = new TAction(array($this, 'onReload'));
    
    	// define os parametros de cada acao
    	$action1->setParameter('key', $key);
    	$action2->setParameter('key', $key);
    
    	//encaminha a chave estrangeira
    	$action1->setParameter('fk', filter_input(INPUT_GET, 'fk'));
    	$action2->setParameter('fk', filter_input(INPUT_GET, 'fk'));
    
    	$action1->setParameter('did', filter_input(INPUT_GET, 'did'));
    	$action2->setParameter('did', filter_input(INPUT_GET, 'did'));
    	// exibe um dialogo ao usuario
    	new TQuestion('Deseja realmente excluir o registro?', $action1, $action2);
    }
    
    /*
     * metodo Delete()
     * Exclui um registro
     */
    
    function Delete($param) {
    	// obtem o parametro $key
    	$key = $param['key'];
    	// inicia transacao com o banco 'pg_ceres'
    	TTransaction::open('pg_ceres');
    
    	// instanicia objeto Record
    	$cadastro = new BairroCoordenadasRecord($key);
    	try {
    		// deleta objeto do banco de dados
    		$cadastro->delete();
    		// finaliza a transacao
    		TTransaction::close();
    	} catch (Exception $e) { // em caso de exceção
    		// exibe a mensagem gerada pela exceção
    		new TMessage('error', $e->getMessage());
    		// desfaz todas alterações no banco de dados
    		TTransaction::rollback();
    	}
    
    	// re-carrega a datagrid
    	$this->onReload();
    	// exibe mensagem de sucesso
    	//new TMessage('info', "Registro Exclu&iacute;do com sucesso!");
    }
    

    function show() {
        $this->onReload();
        parent::show();
    }

    function onSave() {

        try {
            $this->form->validate();
            TTransaction::open('pg_ceres');

            $cadastro = $this->form->getData('BairroCoordenadasRecord');
            $cadastro->usuarioalteracao = $_SESSION['usuario'];
            $cadastro->dataalteracao = date("d/m/Y H:i:s");

            $dados = $cadastro->toArray();
            $obj = implode(" , ", $dados);
            $msg = '';
            $icone = 'info';

            if ($msg == '') {
                if ($cadastro->tipoCoord === '2') {
                    $cadastro->latitude = convertCoord('DD_LAT', $cadastro->grausLat, $cadastro->minutosLat, $cadastro->segundosLat);
                    $cadastro->longitude = convertCoord('DD_LON', $cadastro->grausLon, $cadastro->minutosLon, $cadastro->segundosLon);
                }
                unset($cadastro->grausLat);
                unset($cadastro->minutosLat);
                unset($cadastro->segundosLat);
                unset($cadastro->grausLon);
                unset($cadastro->minutosLon);
                unset($cadastro->segundosLon);
                unset($cadastro->tipoCoord);

                $cadastro->store();
                $msg = 'Dados armazenados com sucesso';

                TTransaction::close();
            } else {
                $icone = 'error';
            }

            if ($icone == 'error') {
                new TMessage($icone, $msg);
                $this->form->setData($cadastro);
            } else {
                $param = array();
                $param['fk'] = $dados['bairro_id'];
                new TMessage("info", "Registro salvo com sucesso!");
                $param = array(
                		'fk' => filter_input(INPUT_GET, 'fk'),
                		'did' => filter_input(INPUT_GET, 'did')
                );
                TApplication::gotoPage('BairroCoordenadasDetalhe', 'onReload', $param);
            }
        } catch (Exception $e) {

        if (filter_input(INPUT_GET, 'method') === "onEdit") {
                
                $action = new TAction(array($this, 'onEdit'));
                $action->setParameter('key', filter_input(INPUT_GET, 'key'));
                $action->setParameter('fk', filter_input(INPUT_GET, 'fk'));
                $action->setParameter('did', filter_input(INPUT_GET, 'did'));
                
                new TMessage('error', $e->getMessage(), $action);
                
            } else {
                
                new TMessage('error', $e->getMessage());
            }
            TTransaction::rollback();
            self::onChangeRadio(array(
                'tipoCoord' => $cadastro->tipoCoord,
                'formName' => $this->form->getName()
            ));
            $this->form->setData($cadastro);
        }
    }
    
    
    function onEdit($param) {
    	try {
    		if (isset($param['key'])) {
    			// get the parameter $key
    			$key = $param['key'];
    
    			TTransaction::open('pg_ceres');   // open a transaction with database 'samples'
    			$object = new BairroCoordenadasRecord($key);        // instantiates object City
    			//converter datas do form para formato Brasileiro
    
    			$this->form->setData($object);   // fill the form with the active record data
    			TTransaction::close();           // close the transaction
    		} else {
    			$this->form->clear();
    		}
    	} catch (Exception $e) { // in case of exception
    		// shows the exception error message
    		new TMessage('error', '<b>Error</b> ' . $e->getMessage());
    		// undo all pending operations
    		TTransaction::rollback();
    	}
    }

}
