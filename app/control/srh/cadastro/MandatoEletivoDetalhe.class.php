<?php

/*
 * classe MandatoeletivoDetalhe
 * Cadastro de Mandatoeletivo: Contem a listagem e o formulario de busca
 */

use Adianti\Widget\Datagrid\TDatagridTables;

class MandatoEletivoDetalhe extends TPage {

    private $form;     // formulario de cadastro
    private $datagrid; // listagem

    /*
     * metodo construtor
     * Cria a pagina, o formulario e a listagem
     */

    public function __construct() {
        parent::__construct();

        // instancia um formulario
        $this->form = new TQuickForm;
        $this->form->class = 'form_licenca';
        $this->form->setFormTitle('<font color="red" size="3" face="Arial"><b>Detalhe de Servidores em Mandato Eletivo</b></font>');

		// cria um rotulo para o titulo
        $titulo = new TLabel('<div style="position:floatval; width: 200px;"> <b>* Campo obrigatorio</b></div>');
        $titulo->setFontFace('Arial');
        $titulo->setFontColor('red');
        $titulo->setFontSize(10);
		
        // cria os campos do formulario
        $codigo = new THidden('id');
        $datainicio = new TDate('datainicio');
        $datafim = new TDate('datafim');
        $tipo = new TCombo('tipo');
        $observacao = new TEntry('observacao');

        $usuarioalteracao = new THidden('usuarioalteracao');
        $dataalteracao = new THidden('dataalteracao');

        // cria um vetor com as opcoes da combo tipo
        $items = array();
        $items['ESTADUAL'] = 'ESTADUAL';
        $items['FEDERAL'] = 'FEDERAL';
        $items['MUNICIPAL'] = 'MUNICIPAL';

        // adiciona as opcoes na combo
        $tipo->addItems($items);
        //coloca o valor padrao no combo
        $tipo->setValue('ESTADUAL');

        //cria a colecao da tabela estrangeira do Cabecalho
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $cadastro = new ServidorRecord(filter_input(INPUT_GET, 'fk'));
        // cria um criterio de selecao, ordenado pelo id do servidor
        //adiciona os objetos na tela com as informacoes do servidor
        if ($cadastro) {
            $nome = new TLabel($cadastro->nome);
            $matricula = new TLabel($cadastro->matricula);
        }
        // finaliza a transacao
        TTransaction::close();

        //coloca o valor do relacionamento
        $id = new THidden('servidor_id');
        $id->setValue($_GET['fk']);

        // define os tamanhos dos campos
        $codigo->setSize(40);
        $datainicio->setSize(80);
        $datafim->setSize(80);
        $observacao->setSize(300);

        // define os campos
        $this->form->addQuickField(null, $id, 0);
        $this->form->addQuickField(null, $usuarioalteracao, 0);
        $this->form->addQuickField(null, $dataalteracao, 0);

        $this->form->addQuickField(null, $codigo, 10);
        $this->form->addQuickField('Matr&iacute;cula', $matricula, 20);
        $this->form->addQuickField('Nome Servidor', $nome, 300);
        $this->form->addQuickField('Tipo <font color=red><b>*</b></font>', $tipo, 30);
        $this->form->addQuickField('Data Inicio <font color=red><b>*</b></font>', $datainicio, 20);
        $this->form->addQuickField('Data Fim <font color=red><b>*</b></font>', $datafim, 20);
        $this->form->addQuickField('Observa&ccedil;&atilde;o', $observacao, 30);
        $this->form->addQuickField(null, $titulo, 30);

        // cria um botao salvar
        $action1 = new TAction(array($this, 'onSave'));
        $action1->setParameter('fk', filter_input(INPUT_GET, 'fk'));

        $this->form->addQuickAction('Salvar', $action1, 'ico_save.png');

        // prepara o botao para chamar o form anterior
        $action2 = new TAction(array("ServidorForm", 'onEdit'));
        $action2->setParameter('fk', filter_input(INPUT_GET, 'fk'));

        $this->form->addQuickAction('Voltar', $action2, 'ico_datagrid.gif');

        // instancia objeto DataGrid
        $this->datagrid = new TDatagridTables;

        // instancia as colunas da DataGrid
        $dgmotivo = new TDataGridColumn('tipo', 'Tipo', 'left', 250);
        $dgdatainicio = new TDataGridColumn('datainicio', 'Data In&iacute;cio', 'left', 80);
        $dgdatafim = new TDataGridColumn('datafim', 'Data Fim', 'left', 80);
        $dgobs = new TDataGridColumn('observacao', 'Observa&ccedil;&atilde;o', 'left', 1150);

        // adiciona as colunas a DataGrid
        $this->datagrid->addColumn($dgmotivo);
        $this->datagrid->addColumn($dgdatainicio);
        $this->datagrid->addColumn($dgdatafim);
        $this->datagrid->addColumn($dgobs);

        // instancia duas acoes da DataGrid
        $action1 = new TDataGridAction(array($this, 'onEdit'));
        $action1->setLabel('Editar');
        $action1->setImage('ico_edit.png');
        $action1->setField('id');
        $action1->setFk('servidor_id');

        $action2 = new TDataGridAction(array($this, 'onDelete'));
        $action2->setLabel('Deletar');
        $action2->setImage('ico_delete.png');
        $action2->setField('id');
        $action2->setFk('servidor_id');

        // adiciona as acoes a DataGrid
        $this->datagrid->addAction($action1);
        $this->datagrid->addAction($action2);

        // cria o modelo da DataGrid, montando sua estrutura
        $this->datagrid->createModel();

        // monta a paina atraves de uma tabela
        $panel = new TPanelForm(700, 500);
        $panel->put($this->form, 0, 0);
        $panel->put($this->datagrid, 150, 270);

        // adiciona a tabela a pagina
        parent::add($panel);
    }

    /*
     * metodo onReload()
     * Carrega a DataGrid com os objetos do banco de dados
     */

    function onReload() {

        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instancia um repositorio da Classe
        $repository = new TRepository('MandatoEletivoRecord');

        // cria um criterio de selecao
        $criteria = new TCriteria;

        //filtra pelo campo selecionado pelo usuário
        $criteria->add(new TFilter('servidor_id', '=', filter_input(INPUT_GET, 'fk')));

        //verifica quantos registros a consulta vai retornar
        $criteria->setProperty('order', 'id');
        // carrega os objetos de acordo com o criterio
        $cadastros = $repository->load($criteria);

        $this->datagrid->clear();
        if ($cadastros) {
            // percorre os objetos retornados
            foreach ($cadastros as $cadastro) {

                //converter datas do form para formato Brasileiro
                $cadastro->datainicio = TDate::date2br($cadastro->datainicio);
                $cadastro->datafim = TDate::date2br($cadastro->datafim);

                // adiciona o objeto na DataGrid
                $this->datagrid->addItem($cadastro);
            }
        }

        // finaliza a transacao
        TTransaction::close();
        $this->loaded = true;
    }

    /*
     * metodo onDelete()
     * Executada quando o usuario clicar no botao excluir da datagrid
     * Pergunta ao usuario se deseja realmente excluir um registro
     */

    function onDelete($param) {
        // obtem o parametro $key
        $key = $param['key'];

        // define duas acoes
        $action1 = new TAction(array($this, 'Delete'));

        // define os parametros de cada acao
        $action1->setParameter('key', $key);

        //encaminha a chave estrangeira
        $action1->setParameter('fk', filter_input(INPUT_GET, 'fk'));
		
        // exibe um dialogo ao usuario
        new TQuestion('Deseja realmente excluir o registro ?', $action1);
    }

    /*
     * metodo Delete()
     * Exclui um registro
     */

    function Delete($param) {
        // obtem o parametro $key
        $key = $param['key'];
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instanicia objeto Record
        $cadastro = new MandatoeletivoRecord($key);

        try {
            // deleta objeto do banco de dados
            $cadastro->delete();

            // finaliza a transacao
            TTransaction::close();
        } catch (Exception $e) { // em caso de exceção
            // exibe a mensagem gerada pela exceção
            new TMessage('error', $e->getMessage());
            // desfaz todas alterações no banco de dados
            TTransaction::rollback();
        }

        // re-carrega a datagrid
        $this->onReload();
        // exibe mensagem de sucesso
        new TMessage('info', "Registro Exclu&iacute;do com sucesso");
    }

    /*
     * metodo show()
     * Exibe a pagina
     */

    function show() {
        // carrega os dados no datagrid
        $this->onReload();
        //chama o metodo show da super classe
        parent::show();
    }

    /*
     * metodo onSave()
     * Executada quando o usuario clicar no botao salvar do formulario
     */
    function onSave() {
		
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
		
        // obtem os dados no formulario em um objeto Record da Classe
        $cadastro = $this->form->getData('MandatoeletivoRecord');

		$cadastro->usuarioalteracao = $_SESSION['usuario'];
        $cadastro->dataalteracao = date("d/m/Y H:i:s");
		
        //antes de armazenar verifica se algum campo eh requerido e nao foi informado
        $dados = $cadastro->toArray();

        $msg = '';
        $icone = 'info';

        if (empty($dados['tipo'])) {
            $msg .= 'O Tipo deve ser informado.';
        }


        if (empty($dados['datainicio'])) {
            $msg .= ' A Data de In&iacute;cio deve ser informada.';
        }


        if (empty($dados['datafim'])) {
            $msg .= ' A Data de Fim deve ser informada.';
        }

        try {

            if ($msg == '') {
                // armazena o objeto
                $cadastro->store();
                $msg = 'Dados armazenados com sucesso';

                // finaliza a transacao
                TTransaction::close();
            } else {
                $icone = 'error';
            }

            if ($icone == 'error') {
                // exibe mensagem de erro
                new TMessage($icone, $msg);
				
				$this->form->setData($cadastro);
				
            } else {
                // define array acoes
                $param = array();
                $param['fk'] = $dados['servidor_id'];

                // exibe um dialogo ao usuario
                new TMessage("info", "Registro salvo com sucesso!");
                TApplication::gotoPage('MandatoEletivoDetalhe', 'onReload', $param); // reload
            }
        } catch (Exception $e) { // em caso de exc
            // exibe a mensagem gerada pela exceção
            new TMessage('error', $e->getMessage());
            // desfaz todas alterações no banco de dados
            TTransaction::rollback();
			
			$this->form->setData($cadastro);
			
        }
    }

    /*
     * metodo onEdit()
     * Edita os dados de um registro
     */

    function onEdit($param) {
		
		try {
            if (isset($param['key'])) {

                // get the parameter $key
                $key = $param['key'];

                TTransaction::open('pg_ceres');   // open a transaction with database 'samples'

                $object = new MandatoeletivoRecord($key);        // instantiates object City

				//converter datas do form para formato Brasileiro
                $object->datainicio = TDate::date2br($object->datainicio);
				$object->datafim = TDate::date2br($object->datafim);
				
                $this->form->setData($object);   // fill the form with the active record data

                TTransaction::close();           // close the transaction
            } else {
                $this->form->clear();
            }
        } catch (Exception $e) { // in case of exception
            // shows the exception error message
            new TMessage('error', '<b>Error</b> ' . $e->getMessage());
            // undo all pending operations
            TTransaction::rollback();
        }		
		
    }
}

?>