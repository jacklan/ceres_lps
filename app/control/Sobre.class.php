<?php
/*
 * classe UsuarioServidorForm
 * Cadastro de UsuarioServidor: Contem o formularo
 */

class Sobre extends TPage
{
    private $form;     // formulario de cadastro

   
    /*
     * metodo construtor
     * Cria a pagina do formulario
     */
    public function __construct()
    {
        parent::__construct();

        // instancia um formulario
        $this->form = new TForm('form_Sobre');
        
        // instancia um panel
        $panel = new TPanelForm(900, 500);

        // adiciona o panel ao formulario
        $this->form->add($panel);
        
        // cria um rotulo para o titulo
        $titulo = new TLabel('Sistema Central do Cidad�o');
        $titulo->setFontFace('Arial');
        $titulo->setFontColor('red');
        $titulo->setFontSize(16);

    

        // cria os campos do formulario
        $gin      = new TLabel('Desenvolvido pelo Grupo de Informática da Emater RN - GIN');
        $fone     = new TLabel('(84)3232-2198');
        $hidel    = new TLabel('&raquo; Hideljundes Macedo  Celular:(84)9621-3111 (TIM) E-mail: hideljundes@gmail.com');
        $jonas    = new TLabel('&raquo; Jonas Jordão de Macêdo  E-mail: jonasjordao452@gmail.com');
        $maisson  = new TLabel('&raquo; Maisson Saraiva Moreira E-mail: maissonsaraiva@hotmail.com');
        $fernando = new TLabel('&raquo; Fernando Corrêa Dias de Souza E-mail: fernandocdds@hotmail.com');
        $jackson  = new TLabel('&raquo; Jackson Meires E-mail: lordjackson@gmail.com');
        $horario  = new TLabel('&raquo; Funcionamento das 07h &agrave;s 13h');
       


        $panel->setColuna2(100);

        // adiciona o campo Titulo
        $panel->put($titulo,$panel->getColuna(),$panel->getLinha());
        
        $panel->putCampo(null, null, 0, 1);

        // adiciona o campo 
        $panel->putCampo($gin, 'Sobre:', 0, 1);
        
        // adiciona o campo 
        $panel->putCampo($fone, null, 0, 1);
        
        // adiciona o campo 
        $panel->putCampo(null, null, 0, 1);

        // adiciona o campo 
        $panel->putCampo($hidel, 'Equipe:', 0, 1);
        
        // adiciona o campo 
        $panel->putCampo($jonas, null, 0, 1);
        
        // adiciona o campo 
        $panel->putCampo($maisson, null, 0, 1);
        
        // adiciona o campo 
        $panel->putCampo($fernando, null, 0, 1);

        // adiciona o campo Servidor
        $panel->putCampo($jackson, null, 0, 1);

        // adiciona o campo Servidor
        $panel->putCampo($horario, null, 0, 1);

        


        // adiciona o form na pagina
        parent::add($this->form);
    }
    
   

}
?>