<?php


/**
 * FeriadosForm
 *
 * @version    2.0
 * @package    ceres
 * @subpackage template2\app\control\IA
 * @author     Lucas Macedo - 15/08/2016
 * @copyright  Copyright (c) 2006-2014 Adianti Solutions Ltd. (http://www.adianti.com.br)
 * @license    http://www.adianti.com.br/framework-license
 */

//ini_set('display_errors', 1);
//ini_set('display_startup_erros', 1);
//error_reporting(E_ALL);

class FeriadosForm  extends TPage {

    private $form;     // formulario de cadastro

    /*
     * metodo construtor
     * Cria a pagina do formulario
     */

    public function __construct() {
        parent::__construct();

        // instancia um formulario
        $this->form = new TQuickForm;
        $this->form->class = 'form_feriado';
        $this->form->setFormTitle('<font color="red" size="3" face="Arial"><b>Formul&aacute;rio de Feriados</b></font>');

       ##===========================================================================##
       ##===========================================================================##       

        $descricao    = new TEntry('descricao');
        $dataferiado  = new TDate('dataferiado');
        $municipio_id = new TCombo('municipio_id');      

        $id = new THidden('id');
        $dataalteracao    = new THidden('dataalteracao');
        $usuarioalteracao = new THidden('usuarioalteracao');
        $empresa_id = new THidden('empresa_id');
        
        // cria um rotulo para o titulo
        $titulo = new TLabel('<div style="position:floatval; width: 200px;"> <b>* Campos obrigat&oacute;rios</b></div>');
        $titulo->setFontFace('Arial');
        $titulo->setFontColor('red');
        $titulo->setFontSize(10);


       ##=============================## TCOMBOS ##==================================##
       ##============================================================================##
    
        $municipio_id  = new TCombo('municipio_id');

        TTransaction::open('pg_ceres');
        $repository = new TRepository('MunicipioRecord');
        $criteria = new TCriteria;
        $cadastros = $repository->load($criteria);
        //adiciona os objetos no combo
        foreach ($cadastros as $object) {
            $items[$object->id] = $object->nome;

        }
    
        TTransaction::close();
        $municipio_id->addItems($items);          

       ##===========================================================================##
       ##===========================================================================##                 


        // define os campos
        $this->form->addQuickField(null, $id, 10);
        $this->form->addQuickField(null, $usuarioalteracao, 10);
        $this->form->addQuickField(null, $dataalteracao, 10); 
        $this->form->addQuickField(null, $empresa_id, 10);    
                                      
      
        $this->form->addQuickField('Descrição  <font color=red><b>*</b></font>', $descricao, 30);          
        $this->form->addQuickField('Data do Feriado <font color=red><b>*</b></font>', $dataferiado, 20); 
        $this->form->addQuickField('Município ', $municipio_id, 40);               
        $this->form->addQuickField(null, $titulo, 50);
        
        $descricao->addValidation('Descrição', new TRequiredValidator);
        $dataferiado->addValidation('Data do Feriado', new TRequiredValidator);
        
       ##===========================================================================##
       ##===========================================================================##  

        // cria um botao de acao
        $this->form->addQuickAction('Salvar', new TAction(array($this, 'onSave')), 'ico_save.png')->class = 'btn btn-info btnleft';
        $this->form->addQuickAction('Voltar', new TAction(array('FeriadosList', 'onReload')), 'ico_datagrid.gif');

        // adiciona a tabela a pagina
        parent::add($this->form);
        
    }


       ##===========================================================================##
       ##===========================================================================##    

    /*
     * metodo onSave()
     * Executada quando o usuario clicar no botao salvar do formulario
     */
    function onSave() 
    {
        
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        
        // obtem os dados no formulario em um objeto CarroRecord
        $cadastro = $this->form->getData('FeriadosRecord');

        //lanca o default
        $cadastro->usuarioalteracao = $_SESSION['usuario'];
        $cadastro->dataalteracao = date("d/m/Y H:i:s");
        $cadastro->empresa_id = $_SESSION['empresa_id'];

        try 
        {
            
            //Validade
            $this->form->validate();

            // armazena o objeto
            $cadastro->store();
                
            TTransaction::close();

            // exibe um dialogo ao usuario
            new TMessage("info", "Registro salvo com sucesso!");
            TApplication::gotoPage('FeriadosList', 'onReload'); // reload
            
        }catch( Exception $e ) 
        { 
            
            // em caso de exce��o
            // exibe a mensagem gerada pela exce��o
            new TMessage('error', $e->getMessage());
            
            // desfaz todas altera��es no banco de dados
            TTransaction::rollback();
            
        }
    }


       ##===========================================================================##
       ##===========================================================================##

    /*
     * metodo onEdit()
     * Edita os dados de um registro
     */
    function onEdit( $param ) 
    {
        
        try 
        {
            if( isset( $param['key'] ) )
            {

                // get the parameter $key
                $key = $param['key'];

                TTransaction::open( 'pg_ceres' );   // open a transaction with database 'samples'

                $object = new FeriadosRecord( $key );        // instantiates object City

                $object->dataferiado = TDate::date2br($object->dataferiado);                



                $this->form->setData( $object );   // fill the form with the active record data

                TTransaction::close();           // close the transaction
                
            }
            
        }catch( Exception $e )
        {
            
            // in case of exception
            // shows the exception error message
            new TMessage('error', '<b>Error</b> ' . $e->getMessage());
            // undo all pending operations
            TTransaction::rollback();
            
        }
        
    }

}

?>