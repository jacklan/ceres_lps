<?php

//ini_set('display_errors', 1);
//ini_set('display_startup_erros', 1);
//error_reporting(E_ALL);

include_once 'app/lib/funcdate.php';

use Adianti\Database\TFilter1;
use Adianti\Widget\Datagrid\TDatagridTables;

class JustificativaPontoList extends TPage {

    private $form;
    private $datagrid;

    public function __construct() {
        parent::__construct();

        $this->form = new TForm('form_ponto_justificativaponto');

        $panel = new TPanelForm(900, 100);

        $this->form->add($panel);

        $titulo = new TLabel('Listagem de Justificativas');
        $titulo->setFontFace('Arial');
        $titulo->setFontColor('red');
        $titulo->setFontSize(12);
        
        TTransaction::open('pg_ceres');
        $nome_servidor = new TLabel( (new ServidorRecord($_SESSION['servidor_id']))->nome);
        TTransaction::close();
        $nome_servidor->setFontFace('Arial');
//        $nome_servidor->setFontColor('blue');
        $nome_servidor->setFontSize(10);

        $panel->put($titulo, $panel->getColuna(), $panel->getLinha());
        $panel->put($nome_servidor, $panel->getColuna(), $panel->getLinha());

        $this->datagrid = new TDatagridTables;

        //$dgcodigo = new TDataGridColumn('id',  'C&oacute;digo',  'left', 1000);
        // $dgservidor = new TDataGridColumn('nome_servidor',  'Servidor',  'left', 1000);
        $dgdatabatida = new TDataGridColumn('databatida',    'Data',    'left', 150);
        $dghorajornada = new TDataGridColumn('hora_jornada',    'Jornada',    'left', 150);
        $dghoratotal = new TDataGridColumn('horatotal',    'Trabalhado',    'left', 150);
        
        $dgbatida01 = new TDataGridColumn('batida01',    'Hora 01',    'center', 150);
        $dgbatida02 = new TDataGridColumn('batida02',    'Hora 02',    'center', 150);
        $dgbatida03 = new TDataGridColumn('batida03',    'Hora 03',    'center', 150);
        $dgbatida04 = new TDataGridColumn('batida04',    'Hora 04',    'center', 150);
        $dgbatida05 = new TDataGridColumn('batida05',    'Hora 05',    'center', 150);
        $dgbatida06 = new TDataGridColumn('batida06',    'Hora 06',    'center', 150);
        
        $dgdatabatida->setTransformer('formatar_data');
        
        // $this->datagrid->addColumn($dgservidor);
        $this->datagrid->addColumn($dgdatabatida);
        $this->datagrid->addColumn($dghorajornada);
        $this->datagrid->addColumn($dghoratotal);
        
        $this->datagrid->addColumn($dgbatida01);
        $this->datagrid->addColumn($dgbatida02);
        $this->datagrid->addColumn($dgbatida03);
        $this->datagrid->addColumn($dgbatida04);
        $this->datagrid->addColumn($dgbatida05);
        $this->datagrid->addColumn($dgbatida06);
        
        $dgbatida01->setTransformer('formatar_hora2');
        $dgbatida02->setTransformer('formatar_hora2');
        $dgbatida03->setTransformer('formatar_hora2');
        $dgbatida04->setTransformer('formatar_hora2');
        $dgbatida05->setTransformer('formatar_hora2');
        $dgbatida06->setTransformer('formatar_hora2');
        
        $action1 = new TDataGridAction(['JustificativaPontoForm', 'onEdit']);
        $action1->setLabel('Justificar Ponto');
        $action1->setImage('ico_meioplanejado.png');
        $action1->setField('id');

        $this->datagrid->addAction($action1);
        
        $this->datagrid->createModel();

        $panel = new TPanelForm(700, 500);
        $panel->put($this->form, 0, 0);
        $panel->put($this->datagrid, 150, 115);

        parent::add($panel);
    }

    function onReload() {
        TTransaction::open('pg_ceres');

        $repository = new TRepository('vw_marcacao_justificativaRecord');
        $criteria = new TCriteria;
        $criteria->setProperty('order', 'databatida DESC');
        $filtro2 = $_SESSION['servidor_id'];
        $criteria->add(new TFilter('servidor_id', '=', $filtro2));
        $cadastros = $repository->load($criteria);

        $this->datagrid->clear();
        
        if ($cadastros) {
            foreach ($cadastros as $cadastro) {
                $this->datagrid->addItem($cadastro);
            }
        }

        TTransaction::close();
        $this->loaded = true;
    }

    function show() {
        $this->onReload();
        parent::show();
    }
}