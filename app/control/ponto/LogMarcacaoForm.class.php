<?php

//ini_set('display_errors', 1);
//ini_set('display_startup_erros', 1);
//error_reporting(E_ALL);


/*
* classe LogMarcacaoForm
* Cadastro de Log Marcacao: Contem o formularo
* Autor:Lucas Vicente
* Data: 06/09/2016
 */
include_once 'app/lib/funcdate.php';

class LogMarcacaoForm extends TPage
{
    private $form;     // formulario de cadastro
    
    /*
     * metodo construtor
     * Cria a pagina do formulario
     */
    public function __construct()
    {
        parent::__construct();

        $this->form = new TQuickForm;
        $this->form->class = 'form_ponto_Logmarcacao';
        $this->form->setFormTitle('<font color="red" size="3" face="Arial"><b>Log Marcação </b></font>');

        // cria os campos do formulario
        $id     = new THidden('id');
        $id->setEditable(false);
        
        $dataapuracao = new TDate('dataapuracao');
        $dataapuracao->setEditable(false);
        $dataapuracao->setValue(date('d/m/y H:i\h'));

        $usuarioalteracao = new THidden('usuarioalteracao');
        $dataalteracao = new THidden('dataalteracao');

        $dataapuracao->setSize(50);

        // cria um rotulo para o titulo
        $campo = new TLabel('<div style="position:floatval; width: 200px;"> <b>* Campo obrigatorio</b></div>');
        $campo->setFontFace('Arial');
        $campo->setFontColor('red');
        $campo->setFontSize(10);

        // define os campos
        $this->form->addQuickField(null, $id, 10);
        $this->form->addQuickField(null, $usuarioalteracao, 10);
        $this->form->addQuickField(null, $dataalteracao, 10);
        $this->form->addQuickField('Data Apuração <font color=red><b>*</b></font>', $dataapuracao, 20);
        
        $this->form->addQuickField(null, $campo, 50);
        
        // cria um botao de acao
        $this->form->addQuickAction('Salvar', new TAction(array($this, 'onSave')), 'ico_save.png')->class = 'btn btn-info';
        $this->form->addQuickAction('Voltar', new TAction(array('LogMarcacaoList', 'onReload')), 'ico_datagrid.gif');

        parent::add($this->form);
    }
    
   
    /*
     * metodo onSave()
     * Executada quando o usuario clicar no botao salvar do formulario
     */
        function onSave() {
        
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // obtem os dados no formulario em um objeto CarroRecord

        $cadastro = $this->form->getData('LogMarcacaoRecord');
        //lanca o default
        $cadastro->usuarioalteracao = $_SESSION['usuario'];
        $cadastro->dataalteracao = date("d/m/Y H:i:s");

        //antes de armazenar verifica se algum campo eh requerido e nao foi informado
        $dados = $cadastro->toArray();

        $msg = '';
        $icone = 'info';

        if (empty($dados['dataapuracao'])) {
            $msg .= 'A Data da Apuração deve ser informado.</br>';
        }

        try {
        $this->form->validate();
            if ($msg == '') {
                // armazena o objeto
                $cadastro->store();
                $msg = 'Dados armazenados com sucesso';

                // finaliza a transacao
                TTransaction::close();
            } else {
                $icone = 'error';
            }

            if ($icone == 'error') {
                // exibe mensagem de erro
                new TMessage($icone, $msg);
            } else {                
             new TMessage("info", "Registro salvo com sucesso!");
                TApplication::gotoPage('LogMarcacaoList','onReload'); // reload
                
            }
        } catch (Exception $e) { // em caso de exce��o
            // exibe a mensagem gerada pela exce��o
            new TMessage('error', $e->getMessage());
            // desfaz todas altera��es no banco de dados
            TTransaction::rollback();
        }
    }

    /*
     * metodo onEdit()
     * Edita os dados de um registro
     */

    function onEdit($param) {
        
        try {
            if (isset($param['key'])) {

                // get the parameter $key
                $key = $param['key'];

                TTransaction::open('pg_ceres');   // open a transaction with database 'samples'

                $object = new LogMarcacaoRecord($key); // instantiates object City

                $this->form->setData($object);   // fill the form with the active record data
                TTransaction::close();           // close the transaction
            } 
        } catch (Exception $e) { // in case of exception
            // shows the exception error message
            new TMessage('error', '<b>Error</b> ' . $e->getMessage());
            // undo all pending operations
            TTransaction::rollback();
        }
        
    }


}
?>