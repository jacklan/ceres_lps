<?php

include_once 'app/lib/funcdate.php';

use Adianti\Database\TFilter;
use Adianti\Widget\Datagrid\TDatagridTables;

class ConfirmarDocumentoJustificativa extends TPage
{
    private $form;
    private $datagrid;

    public function __construct(){
        parent::__construct();

        $this->form = new TQuickForm;
        $this->form->class = 'form_confirmar_documento';
        $this->form->setFormTitle('Confirmar o Recebimento do Documento Justificativa');

        $this->datagrid = new TDataGridTables;

        $dgdatabatida = new TDataGridColumn('databatida',    'Data',    'left', 300);
        $dghorajornada = new TDataGridColumn('hora_jornada',    'Jornada',    'left', 300);
        $dghoratotal = new TDataGridColumn('horatotal',    'Trabalhado',    'left', 300);
        $dgsituacao = new TDataGridColumn('situacaojustificativa', 'Situa&ccedil;&atilde;o', 'left', 300);
        $dgtipojustificativa = new TDataGridColumn('nometipojustificativa', 'Tipo Justificativa', 'left', 300);
        $dgpdf = new TDataGridColumn('justificativapdf',    'Justificativa PDF',    'left', 300);
        $dgnomeservidor = new TDataGridColumn('nome_servidor', 'Nome Servidor', 'left', 300);
        $dgmatriculaservidor = new TDataGridColumn('matricula_servidor', 'Matricula', 'left', 300);

        $dgdatabatida->setTransformer('formatar_data');

        $this->datagrid->addColumn($dgnomeservidor);
        $this->datagrid->addColumn($dgmatriculaservidor);
        $this->datagrid->addColumn($dgdatabatida);
        $this->datagrid->addColumn($dghorajornada);
        $this->datagrid->addColumn($dghoratotal);
        $this->datagrid->addColumn($dgsituacao);
        $this->datagrid->addColumn($dgtipojustificativa);
        $this->datagrid->addColumn($dgpdf);

        $action1 = new TDataGridAction(array($this, 'onConfirmar'));
        $action1->setLabel('Confirmar');
        $action1->setImage('ico_finalizarpedido.png');
        $action1->setField('id');

        $action2 = new TDataGridAction(array($this, 'onNegar'));
        $action2->setLabel('Negar');
        $action2->setImage('ico_cancelar.png');
        $action2->setField('id');


        $this->datagrid->addAction($action1);
        $this->datagrid->addAction($action2);

        $this->datagrid->createModel();
        $panel = new TPanelForm(900, 500);
        $panel->put($this->form, 0, 0);
        $panel->put($this->datagrid, 150, 100);

        parent::add($panel);
    }

    function onReload(){


        TTransaction::open('pg_ceres');

        $repository = new TRepository('vw_justificativa_acompanhamentoRecord');
        $criteria = new TCriteria;


        $criteria->setProperty('order', 'id DESC');
        $criteria->add(new TFilter('tipoautorizacao', '=', 'RH') );
        $criteria->add(new TFilter('situacaojustificativa', '=', 'AGUARDANDO') );

        $cadastros = $repository->load($criteria);

        $this->datagrid->clear();

        if ($cadastros) {
            foreach ($cadastros as $cadastro) {
                $this->datagrid->addItem($cadastro);
            }
        }


        TTransaction::close();
        $this->loaded = true;
    }

    function show(){

        $this->onReload();
        parent::show();

    }
    function onOrdenar(){

    }

    function onConfirmar( $param )
    {
        try
        {
            if( isset( $param['key'] ) ) {

                TTransaction::open('pg_ceres');

                $object = $this->form->getData('Marcacao_RelogioRecord');

                $object->id = $param['key'];
                $object->dtentrega = date("d/m/Y H:i:s");
                $object->servidorrh_id = $_SESSION['servidor_id'];
                $object->situacaojustificativa = 'DEFERIDO';

                $object->store();

                TTransaction::close();

                new TMessage("info", "Registro salvo com sucesso!");
                TApplication::gotoPage('ConfirmarDocumentoJustificativa', 'onReload');

            }
        }catch( Exception $e )
        {
            new TMessage('error', $e->getMessage());

            TTransaction::rollback();

        }
    }

    function onNegar( $param )
    {
        try
        {
            if( isset( $param['key'] ) ) {

                TTransaction::open('pg_ceres');

                $object = $this->form->getData('Marcacao_RelogioRecord');

                $object->id = $param['key'];
                $object->dtentrega = date("d/m/Y H:i:s");
                $object->servidorrh_id = $_SESSION['servidor_id'];
                $object->situacaojustificativa = 'INDEFERIDO';

                $object->store();

                TTransaction::close();

                new TMessage("info", "Registro salvo com sucesso!");
                TApplication::gotoPage('ConfirmarDocumentoJustificativa', 'onReload');

            }
        }catch( Exception $e )
        {
            new TMessage('error', $e->getMessage());

            TTransaction::rollback();

        }
    }
}