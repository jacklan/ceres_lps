<?php

/*
 * classe BatidaRelogioServidorList2
 * Cadastro de Batida Relogio Servidor: Contem a listagem e o formulario de busca
 */
include_once 'app.library/funcdate.php';

class BatidaRelogioServidorList2 extends TPage {

    private $form;     // formulario de cadastro
    private $datagrid; // listagem

    /*
     * metodo construtor
     * Cria a pagina, o formulario e a listagem
     */

    public function __construct() {
        parent::__construct();

        // instancia um formulario
        $this->form = new TForm('form_Marcacao_Relogio');

        // instancia um Panel
        $panel = new TPanelForm(1000, 100);

        // adiciona o panel ao formulario
        $this->form->add($panel);

        // cria um rotulo para o titulo
        $titulo = new TLabel('Marca&ccedil;&atilde;o Relogio');
        $titulo->setFontFace('Arial');
        $titulo->setFontColor('red');
        $titulo->setFontSize(18);
        
        
        if($_REQUEST['mes'] == 01){
            $mes_ = 'Janeiro';
        }
        if($_REQUEST['mes'] == 02){
            $mes_ = 'Fevereiro';
        }
        if($_REQUEST['mes'] == 03){
            $mes_ = 'Mar&ccedil;o';
        }
        if($_REQUEST['mes'] == 04){
            $mes_ = 'Abril';
        }
        if($_REQUEST['mes'] == 05){
            $mes_ = 'Maio';
        }
        if($_REQUEST['mes'] == 06){
            $mes_ = 'Junho';
        }
        if($_REQUEST['mes'] == 07){
            $mes_ = 'Julho';
        }
        if($_REQUEST['mes'] == 08){
            $mes_ = 'Agosto';
        }
        if($_REQUEST['mes'] == 09){
            $mes_ = 'Setembro';
        }
        if($_REQUEST['mes'] == 10){
            $mes_ = 'Outubro';
        }
        if($_REQUEST['mes'] == 11){
            $mes_ = 'Novembro';
        }
        if($_REQUEST['mes'] == 12){
            $mes_ = 'Dezembro';
        }

        $tituloGrid = new TLabel('Listagem de Marca&ccedil;&otilde;es: '.$mes_.'/'.$_REQUEST['ano']);
        $tituloGrid->setFontColor('red');
        $tituloGrid->setFontSize(18);
        
        
        

// cria os campos do formulario
        $codigo = new TEntry('id');
        $codigo->setEditable(false);
        $servidor_id = new TCombo('servidor_id');
        $mes = new TCombo('mes');
        $ano = new TCombo('ano');
        $usuarioAlteracao = new TEntry('usuarioalteracao');
        $usuarioAlteracao->setEditable(false);
        $dataAlteracao = new TDate('dataalteracao');
        $dataAlteracao->setEditable(false);

        //cria a colecao da tabela estrangeira
        //inicia transacao com o banco 'pg_ceres'
        $items = array();
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $repository = new TRepository('Servidor');
        // cria um criterio de selecao, ordenado pelo id
        $criteria = new TCriteria;
        $criteria->setProperty('order', 'nome');
        // carrega os objetos de acordo com o criterio
        $cadastros = $repository->load($criteria);
        //adiciona os objetos no combo
        foreach ($cadastros as $object) {
            $items[$object->id] = $object->nome;
        }
        // adiciona as opcoes na combo
        $servidor_id->addItems($items);
        // finaliza a transacao
        TTransaction::close();

       // define os tamanhos dos campos
        $codigo->setSize(40);
        $servidor_id->setSize(80);

        $items = array();
        $items['01'] = 'JANEIRO';
        $items['02'] = 'FEVEREIRO';
        $items['03'] = 'MARÇO';
        $items['04'] = 'ABRIL';
        $items['05'] = 'MAIO';
        $items['06'] = 'JUNHO';
        $items['07'] = 'JULHO';
        $items['08'] = 'AGOSTO';
        $items['09'] = 'SETEMBRO';
        $items['10'] = 'OUTUBRO';
        $items['11'] = 'NOVEMBRO';
        $items['12'] = 'DEZEMBRO';

        // adiciona as opcoes na combo
        $mes->addItems($items);
        //coloca o valor padrao no combo
        $mes->setValue(date('m'));

        // adiciona as opcoes na combo
        $ano->addItems(retornaAnosemZero());
        $ano->setValue(date('Y'));
        //coloca o valor padrao no combo
        //  $ano->setValue('EM ATIVIDADE');

        $servidor_id->setSize(300);
//        $ano->setSize(80);
//        $mes->setSize(80);
        // adiciona o campo titulo
        $panel->put($titulo, $panel->getColuna(), $panel->getLinha());

        // adiciona o campo do id da fk
        //$panel->putCampo($id, null, 0, 0);
        // adiciona o campo id da tabela
        $panel->putCampo($codigo, 'C&oacute;digo', 0, 1);

        // adiciona o campo Nome do Servidor
        //$panel->putCampo($servidor_id, 'Nome Servidor', 0, 1);

        // adiciona o campo Mes
        $panel->putCampo($mes, 'Mes', 0, 1);

        // adiciona o campo Nome do Servidor
        $panel->putCampo($ano, 'Ano', 0, 1);

        // cria um botao de acao (salvar)
        $save_button = new TButton('buscar');

        // define a acao do botao
        $action1 = new TAction(array($this, 'onReload'));
//        $action1->setParameter('fk', $_GET['fk']);
//        $action1->setParameter('key', $_GET['key']);
        $save_button->setAction($action1, 'Buscar');

        // adiciona a acao do formulario
        $panel->putCampo($save_button, null, -100, $panel->setLinha(150));

        // define quais sao os campos do formulario
        $this->form->setFields(array($codigo, $ano, $servidor_id, $mes, $save_button));

        $panel->setColuna(-50);
        $panel->setLinha(250);
        // adiciona o campo Nome do Servidor
        $panel->putCampo($tituloGrid, '', 0, 0);
        // instancia objeto DataGrid
        $this->datagrid = new TDataGridCss;


        // instancia as colunas da DataGrid
        $dgcodigo   = new TDataGridColumn('id',  'C&oacute;digo',  'center', 60);
        $dgservidor   = new TDataGridColumn('nome',  'Servidor',  'left', 260);
        $dgmatricula     = new TDataGridColumn('matricula',    'Matricula',    'left', 150);
        $dgdatabatida   = new TDataGridColumn('databatida',   'Data',  'left',  80);
        $dghorabatida   = new TDataGridColumn('databatida',   'Hora',  'left',  80);
        $dgfuncao = new TDataGridColumn('funcao', 'Fun&ccedil;&atilde;o', 'left', 100);
        $dgprocessado = new TDataGridColumn('processado', 'Processado', 'left', 200);


        // adiciona as colunas a DataGrid
        //$this->datagrid->addColumn($dgcodigo);
        $this->datagrid->addColumn($dgservidor);
        $this->datagrid->addColumn($dgmatricula);
        $this->datagrid->addColumn($dgdatabatida);
        $this->datagrid->addColumn($dghorabatida);
        $this->datagrid->addColumn($dgfuncao);
        $this->datagrid->addColumn($dgprocessado);

        //formata as datas
        $dgdatabatida->setTransformer('formatar_data');
        $dghorabatida->setTransformer('formatar_hora2');

        // cria o modelo da DataGrid, montando sua estrutura
        $this->datagrid->createModel();

        // monta a paina atraves de uma tabela
        $panel = new TPanelForm(1000, 500);
        $panel->put($this->form, 0, 0);
        $panel->put($this->datagrid, 50, 275);


        // adiciona a tabela a pagina
        parent::add($panel);
    }

    /*
     * metodo onCancel()
     * Executada quando o usuario clicar no botao cancelar do form
     * Exibe msg ao usuario informano que a operacao foi cancelada
     */

//    function onCancel() {
//        // define a acao de cancelamento
//        $obj = new ServidorForm;
//        $action1 = new TAction(array($obj, 'onEdit'));
//        $action1->setParameter('key', $_GET['fk']);
//
//        // exibe um dialogo ao usuario
//        new TConfirmation('Opera&ccedil;&atilde;o cancelada!', $action1);
//    }

    /*
     * metodo onReload()
     * Carrega a DataGrid com os objetos do banco de dados
     */
    function onReload() {
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instancia um repositorio da Classe
        $repository = new TRepository('vw_batida_relogio');

        // cria um criterio de selecao
        $criteria = new TCriteria;
        
        //filtra pelo campo selecionado pelo usuario
        $criteria->add(new TFilter('servidor_id', '=', $_SESSION['servidor_id']));
        $criteria->add(new TFilter('mes', '=', $_REQUEST['mes']));
        $criteria->add(new TFilter('ano', '=', $_REQUEST['ano']));

        $criteria->setProperty('order', $campo);

        // carrega os objetos de acordo com o criterio
        $cadastros = $repository->load($criteria);

        $this->datagrid->clear();
        if ($cadastros) {
            // percorre os objetos retornados
            foreach ($cadastros as $cadastro) {
                // adiciona o objeto na DataGrid
                $this->datagrid->addItem($cadastro);
            }
        }
        // finaliza a transacao
        TTransaction::close();
        $this->loaded = true;
    }

    /*
     * metodo onDelete()
     * Executada quando o usuario clicar no botao excluir da datagrid
     * Pergunta ao usuario se deseja realmente excluir um registro
     */

    function onDelete($param) {
        // obtem o parametro $key
        $key = $param['key'];

        // define duas acoes
        $action1 = new TAction(array($this, 'Delete'));
        $action2 = new TAction(array($this, 'NaoDelete'));

        // define os parametros de cada acao
        $action1->setParameter('key', $key);
        $action2->setParameter('key', $key);

        //encaminha a chave estrangeira
        $action1->setParameter('fk', $_GET['fk']);
        $action2->setParameter('fk', $_GET['fk']);
        // exibe um dialogo ao usuario
        new TQuestion('Deseja realmente excluir o registro ?', $action1, $action2);
    }

    /*
     * metodo Delete()
     * Exclui um registro
     */

    function Delete($param) {
        // obtem o parametro $key
        $key = $param['key'];
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instanicia objeto Record
        $cadastro = new Marcacao_RelogioRecord($key);

        try {
            // deleta objeto do banco de dados
            $cadastro->delete();

            // finaliza a transacao
            TTransaction::close();
        } catch (Exception $e) { // em caso de exceção
            // exibe a mensagem gerada pela exceção
            new TMessage('error', $e->getMessage());
            // desfaz todas alterações no banco de dados
            TTransaction::rollback();
        }

        // re-carrega a datagrid
        $this->onReload();
        // exibe mensagem de sucesso
        new TMessage('info', "Registro Exclu&iacute;do com sucesso");
    }

    /*
     * metodo show()
     * Exibe a pagina
     */

    function show() {
        // carrega os dados no datagrid
        $this->onReload();
        //chama o metodo show da super classe
        parent::show();
    }

    /*
     * metodo onSave()
     * Executada quando o usuario clicar no botao salvar do formulario
     */

    function onSave() {
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // obtem os dados no formulario em um objeto Record da Classe
        $cadastro = $this->form->getData('Marcacao_RelogioRecord');

        //antes de armazenar verifica se algum campo eh requerido e nao foi informado
        $dados = $cadastro->toArray();

        $msg = '';
        $icone = 'info';

        if (empty($dados['servidor_id'])) {
            $msg .= 'O servidor deve ser informada.';
        }

        if (empty($dados['mes'])) {
            $msg .= 'O Mes deve ser informado.';
        }

        if (empty($dados['ano'])) {
            $msg .= 'Ano deve ser informada.';
        }
        try {
            if ($msg == '') {
                // armazena o objeto
                $cadastro->store();
                $msg = 'Dados armazenados com sucesso';

                // finaliza a transacao
                TTransaction::close();
            } else {
                $icone = 'error';
            }

            if ($icone == 'error') {
                // exibe mensagem de erro
                new TMessage($icone, $msg);
            } else {
                //chama o formulario com o grid
                // define duas acoes
                $action1 = new TAction(array('Marcacao_RelogioDetalhe', 'onReload'));
                // $action1->setParameter('fk', $dados['servidor_id']);
                // exibe um dialogo ao usuario
                new TConfirmation($msg, $action1);
                // re-carrega listagem
                //                $this->onReload();
            }
        } catch (Exception $e) { // em caso de exc
            // exibe a mensagem gerada pela exceção
            new TMessage('error', $e->getMessage());
            // desfaz todas alterações no banco de dados
            TTransaction::rollback();
        }
    }

    /*
     * metodo onEdit()
     * Edita os dados de um registro
     */

    function onEdit($param) {
        // obtem o parametro $key
        $key = $param['key'];
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $cadastro = new Marcacao_RelogioRecord($key);
        // lanca os dados no formulario
        $this->form->setData($cadastro);

        // finaliza a transacao
        TTransaction::close();
    }

}

?>