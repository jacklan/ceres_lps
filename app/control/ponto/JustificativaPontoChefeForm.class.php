<?php

//ini_set('display_errors', 1);
//ini_set('display_startup_erros', 1);
//error_reporting(E_ALL);

include_once 'app/lib/funcdate.php';

//teste

class JustificativaPontoChefeForm extends TPage {

    private $form;

    public function __construct() {
        parent::__construct();

        $this->form = new TQuickForm;
        $this->form->class = 'form_ponto_escala';
        $this->form->setFormTitle('<font color="red" size="3" face="Arial"><b>Processamento de Justificativa
    </b></font>');

        $codigo = new THidden('id');

        $tipojustificativa_id = new TCombo('tipojustificativaponto_id');
        $tipojustificativa_id->setEditable(false);
        $justificativa = new TText('justificativa');
        $justificativa->setEditable(FALSE);
        $databatida = new TDate('databatida');
        $dataautorizacao = new TDate('dataautorizacao');
        $dataautorizacao->setEditable(false);
        $dataautorizacao->setValue(date('d/m/Y'));
        $situacaojustificativa = new TCombo('situacaojustificativa');
        $chefe_id = new THidden('chefe_id');


        //$usuarioAlteracao = new THidden('usuarioalteracao');
        //$dataAlteracao = new THidden('dataalteracao');

        $campo = new TLabel('<div style="position:floatval; width: 200px;"> <b>* Campo obrigatorio</b></div>');
        $campo->setFontFace('Arial');
        $campo->setFontColor('red');
        $campo->setFontSize(10);


        $items = [];
        TTransaction::open('pg_ceres');

        $repository = new TRepository('TipoJustificativaPontoRecord');
        $criteria = new TCriteria;
        $criteria->setProperty('order', 'nome');

        $collection = $repository->load($criteria);

        foreach ($collection as $object) {
            $items[$object->id] = $object->nome;
        }

        $tipojustificativa_id->addItems($items);

        TTransaction::close();

        $key = filter_input(INPUT_GET, 'key');

        TTransaction::open('pg_ceres');
        $cadastro = new Marcacao_RelogioRecord($key);

        if ($cadastro) {
            $cadastro->databatida = TDate::date2br($cadastro->databatida);
            $databatida = new TLabel($cadastro->databatida);
        }
        TTransaction::close();

        $items = [];
        $items['DEFERIDO'] = 'DEFERIDO';
        $items['INDEFERIDO'] = 'INDEFERIDO';
        $situacaojustificativa->addItems($items);

        $this->form->addQuickField(null, $codigo, 10);
        $this->form->addQuickField(null, $chefe_id, 10);
        //$this->form->addQuickField(null, $usuarioAlteracao, 10);
        //$this->form->addQuickField(null, $dataAlteracao, 10);
        $this->form->addQuickField('Data da Batida', $databatida, 80);
        $this->form->addQuickField('Tipo da Justificativa', $tipojustificativa_id, 50);
        $this->form->addQuickField('Justificativa', $justificativa, 50);
        $this->form->addQuickField('Data Autorização', $dataautorizacao, 50);
        $this->form->addQuickField('Situação da Justificativa <font color=red><b>*</b></font>', $situacaojustificativa, 50);

        $this->form->addQuickField(null, $campo, 50);

        $justificativa->setSize(450, 70);

        $actionSave = new TAction(array($this, 'onSave'));

        $actionSave->setParameter("key", filter_input(INPUT_GET, "key"));
        $actionSave->setParameter("fk", filter_input(INPUT_GET, "fk"));

        $this->form->addQuickAction('Salvar', $actionSave, 'ico_save.png')->class = 'btn btn-info';


        $this->form->addQuickAction('Voltar', new TAction(array($this, 'onCancel')), 'ico_back.gif');

        parent::add($this->form);
    }

    function onSave() {

        TTransaction::open('pg_ceres');

        $msg = '';
        $icone = 'info';

        $object = $this->form->getData('Marcacao_RelogioRecord');

        //$object->usuarioalteracao = $_SESSION['usuario'];
        //$object->dataalteracao = date("d/m/Y H:i:s");

        $object->chefe_id = $_SESSION['servidor_id'];


        try {

            if ($this->form->getFieldData('situacaojustificativa') == '') {
                $msg .= 'O campo Situacao Justificativa deve ser preenchido.';
            }

            if ($msg == '') {
                $object->store();

                $msg = 'Dados armazenados com sucesso';
            } else {

                $icone = 'error';
            }
            if ($icone == 'error') {
                $this->form->setData($object);
                new TMessage($icone, $msg);
            } else {
                new TMessage("info", "Registro salvo com sucesso!");
                TApplication::gotoPage('JustificativaPontoChefeList', 'onReload');
            }
        } catch (Exception $e) {
            new TMessage('error', $e->getMessage());
            $this->form->setData($this->form->getData());
            TTransaction::rollback();
        }

        TTransaction::close();
    }

    function onCancel() {

        TApplication::gotoPage('JustificativaPontoChefeList', 'onReload');
    }

    function onEdit($param) {
        $key = $param['key'];
        TTransaction::open('pg_ceres');
        $cadastro = new Marcacao_RelogioRecord($key);


        $this->form->setData($cadastro);

        TTransaction::close();
    }

}

?>