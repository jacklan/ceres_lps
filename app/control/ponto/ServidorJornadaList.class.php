<?php

/*
 * classe ServidorJornada
 * Cadastro de ServidorJornada: Contem a listagem e o formulario de busca
 */
//ini_set('display_errors', 1);
//ini_set('display_startup_erros', 1);
//error_reporting(E_ALL);
include_once 'app/lib/funcdate.php';

//include_once 'app.pdf/RelatorioServidorPDF.php';
use Adianti\Database\TFilter1;
use Adianti\Widget\Datagrid\TDatagridTables;

class ServidorJornadaList extends TPage {

    private $form;     // formulario de cadastro
    private $datagrid; // listagem

    /*
     * metodo construtor
     * Cria a pagina, o formulario e a listagem
     */

    public function __construct() {
        parent::__construct();

        // instancia um formulario
        $this->form = new TForm('form_busca_jornada');

        // instancia uma tabela
        $panel = new TPanelForm(900, 100);

        // adiciona a tabela ao formulario
        $this->form->add($panel);

        TTransaction::open('pg_ceres'); // inicia transacao com o banco 'pg_ceres'

        $object = new JornadaRecord($_GET['jornada_id']);
        $lb_jornada = "Entrada 1: <font style='color:blue;'>" . $object->entrada1 . "</font> Saida 1: <font style='color:blue;'>" . $object->saida1 . "</font> Entrada 2: <font style='color:blue;'>" . $object->entrada2 . "</font> Saida 2: <font style='color:blue;'>" . $object->saida2 . "</font> Entrada 3:<font style='color:blue;'>" . $object->entrada3 . "</font> Saida 3: <font style='color:blue;'>" . $object->saida3."</font>";

        TTransaction::close(); // finaliza a transacao
        // cria um rótulo para o título
        $titulo = new TLabel('Jornada ' . $lb_jornada);
        $titulo->setFontFace('Arial');
        $titulo->setFontColor('red');
        $titulo->setFontSize(12);
        
         $btn_back = new TButton('novo');

        ##===========================================================================##
        ##===========================================================================##          
        // define a acao do botao cadastrar
        $btn_back->setAction(new TAction(array('JornadaList', 'onReload')), 'Voltar');

        // adiciona o campo Titulo
        $panel->put($titulo, $panel->getColuna(), $panel->getLinha());
        $panel->put($btn_back, $panel->getColuna(), $panel->getLinha());

        // define quais sao os campos do formulario
        $this->form->setFields(array($titulo, $btn_back));

        // instancia objeto DataGrid
        $this->datagrid = new TDatagridTables;
        // instancia as colunas da DataGrid
        $dgnome = new TDataGridColumn('nome', 'Nome', 'left', 300);
        $dgmatricula = new TDataGridColumn('matricula', 'Matr&iacute;cula', 'left', 100);

        // adiciona as colunas a DataGrid
        $this->datagrid->addColumn($dgnome);
        $this->datagrid->addColumn($dgmatricula);

        // cria o modelo da DataGrid, montando sua estrutura
        $this->datagrid->createModel();

        // monta a paina atraves de uma tabela
        $panel = new TPanelForm(700, 30);
        $panel->put($this->form, 0, 0);
        $panel->put($this->datagrid, 150, 80);

        // adiciona a tabela a pagina
        parent::add($panel);
    }

    function onReload() {
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instancia um repositorio para Carros
        $repository = new TRepository('vw_servidor_jornadaRecord');

        $criteria = new TCriteria;
        $jornada_id = $_GET['jornada_id'];
        if ($jornada_id) {
            $criteria->add(new TFilter('jornada_id', '=', $jornada_id));
        }
        // carrega os objetos de acordo com o criterio
        $cadastros = $repository->load($criteria);

        $this->datagrid->clear();
        if ($cadastros) {
            // percorre os objetos retornados
            foreach ($cadastros as $cadastro) {

                // adiciona o objeto na DataGrid
                $this->datagrid->addItem($cadastro);
            }
        }
        // finaliza a transacao
        TTransaction::close();
        $this->loaded = true;
    }

    /*
     * metodo show()
     * Exibe a pagina
     */

    function show() {
        // carrega os dados no datagrid
        $this->onReload();
        //chama o metodo show da super classe
        parent::show();
    }

}

?>