<?php
//ini_set('display_errors', 1);
//ini_set('display_startup_erros', 1);
//error_reporting(E_ALL);

class JustificativaPontoForm extends TPage {

    private $form;

    public function __construct() {
        parent::__construct();
        
        $this->form = new TQuickForm;
        $this->form->class = 'form_ponto_justificativaponto';
        $this->form->setFormTitle('<font color="red" size="3" face="Arial"><b>Formul&aacute;rio de Justificativa
    </b></font>');
        
        $codigo = new THidden('id');
        $codigo->setEditable(false);

        $tipojustificativa_id = new TCombo('tipojustificativaponto_id');
        $justificativa = new TText('justificativa');
        $situacaojustificativa = new TEntry('situacaojustificativa');
        $situacaojustificativa->setValue('AGUARDANDO');
        $situacaojustificativa->setEditable(false);
        $justificativapdf = new TFile('justificativapdf');


        TTransaction::open('pg_ceres');
        $repository = new TRepository('TipoJustificativaPontoRecord');
        $criteria = new TCriteria;
        $criteria->setProperty('order', 'nome');
        $collection = $repository->load($criteria);
        foreach ($collection as $object){
            $items[$object->id] = $object->nome;
        }
        $tipojustificativa_id->addItems($items);
        TTransaction::close();

        TTransaction::open('pg_ceres');
        $cadastro = new Marcacao_RelogioRecord(filter_input(INPUT_GET, 'key'));

        if ($cadastro){
            $cadastro->databatida = TDate::date2br($cadastro->databatida);
            $databatida = new TLabel($cadastro->databatida);

        }

        TTransaction::close();
        
        $campo = new TLabel('<div style="position:floatval; width: 200px;"> <b>* Campo obrigatorio</b></div>');
        $campo->setFontFace('Arial');
        $campo->setFontColor('red');
        $campo->setFontSize(10);


        $this->form->addQuickField(null, $codigo, 10);

        $this->form->addQuickField('Situação <font color=red><b>*</b></font>', $situacaojustificativa, 50);
        $this->form->addQuickField('Data Batida Operativa <font color=red><b>*</b></font>', $databatida, 100);
        $this->form->addQuickField('Tipo Justificativa <font color=red><b>*</b></font>', $tipojustificativa_id, 50);
        $this->form->addQuickField('Justificativa <font color=red><b>*</b></font>', $justificativa, 100);
        $this->form->addQuickField('Justificativa PDF', $justificativapdf, 500);

        $this->form->addQuickField(null, $campo, 50);

        $justificativa->setSize(400,100);


        $this->form->addQuickAction('Salvar', new TAction(array($this, 'onSave')), 'ico_save.png')->class = 'btn btn-info';
        $this->form->addQuickAction('Voltar', new TAction(array('JustificativaPontoList', 'onReload')), 'ico_datagrid.gif');

        parent::add($this->form);

    }

    function onSave() {

        TTransaction::open('pg_ceres');

        $cadastro = $this->form->getData('Marcacao_RelogioRecord');

        $dados = $cadastro->toArray();


        $msg = '';
        $icone = 'info';

        $source_file = 'tmp/' . $cadastro->justificativapdf;
     

         $extensao = strtolower(pathinfo(strtolower($cadastro->justificativapdf), PATHINFO_EXTENSION));
/*
        if ($extensao != 'pdf' ) {

             $msg .= 'Somente arquivos PDF';

        }
*/

        if (empty($dados['tipojustificativaponto_id'])) {
            $msg .= 'O Tipo da Justificativa deve ser informado.</br>';
        }

        if (empty($dados['justificativa'])) {
            $msg .= 'A Justificativa deve ser informada.</br>';
        }
        if (empty($dados['situacaojustificativa'])) {
            $msg .= 'A SituaÃ§Ã£o deve ser informado.</br>';
        }

        try {
            $this->form->validate();

            if ($msg == '') {

                unset($cadastro->justificativapdf);

                $cadastro->dtjustificativa = date("d/m/Y");

                $cadastro->store();

                $caminho = 'app/output/justificativaponto/justificativapdf_' . $cadastro->id . '.pdf';
			
				chmod($source_file, 0775);
				
				rename($source_file, $caminho);
			
			
                $msg = 'Dados armazenados com sucesso';

                TTransaction::close();
            } else {
                $icone = 'error';
            }

            if ($icone == 'error') {
                new TMessage($icone, $msg);
            } else {
               new TMessage("info", "Registro salvo com sucesso!");
                TApplication::gotoPage('JustificativaPontoList','onReload');

            }
        } catch (Exception $e) {
            new TMessage('error', $e->getMessage());
            TTransaction::rollback();
        }
    }

    function onEdit($param) {

        try {
            if (isset($param['key'])) {

                $key = $param['key'];

                TTransaction::open('pg_ceres');

                $object = new Marcacao_RelogioRecord($key); 
                $object->databatida = TDate::date2br($object->databatida);
                $this->form->setData($object);

                TTransaction::close();
            } 
        } catch (Exception $e) { 
            new TMessage('error', '<b>Error</b> ' . $e->getMessage());
            TTransaction::rollback();
        }
        
    }

}

?>