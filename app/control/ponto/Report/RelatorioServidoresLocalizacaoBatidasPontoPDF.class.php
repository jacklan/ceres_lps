<?php

ini_set('display_errors', 1);
ini_set('display_startup_erros', 1);
error_reporting(E_ALL);
header('Content-type: application/pdf; charset=utf-8');
/*
 * Classe RelatorioServidoresLocalizacaoBatidasPontoPDF
 * Relatorio Localizacao das Batidas Servidor
 * Autor: Jackson Meires
 * Data:  24/04/2017
 */

use Adianti\library\funcdate;
use Adianti\Database\TTransaction;
use Adianti\Database\TRepository;
use Adianti\Database\TCriteria;
use Adianti\Database\TFilter;

class RelatorioServidoresLocalizacaoBatidasPontoPDF extends FPDF {

    function Header() {
        $this->Image("app/images/logo_relatorio.jpg", 8, 11, 26, 18);

        $this->SetFont('Arial', 'B', 12);
        $this->SetX("25");
        $this->Cell(0, 5, utf8_decode("GOVERNO DO ESTADO DO RIO GRANDE DO NORTE"), 0, 1, 'C');

        $this->SetFont('Arial', 'B', 10);
        $this->SetX("35");
        $this->Cell(0, 5, utf8_decode($_SESSION['empresa_nome']), 0, 1, 'J');

        $this->SetX("25");
        $this->Cell(0, 5, utf8_decode("SISTEMA DE PONTO"), 0, 1, 'C');
        $this->SetX("25");
        $this->Cell(0, 5, utf8_decode(" MES " . (($_REQUEST['mes'] == '0' ) ? "TODOS" : $_REQUEST['mes']) . " / ANO " . $_REQUEST['ano']), 0, 1, 'C');
        $this->Ln(5);
        $this->ColumnHeader();
    }

    function ColumnHeader() {
        $this->SetFont('arial', 'B', 10);
        $this->SetFillColor(255, 255, 255);
        $this->SetX("10");
        $this->Cell(0, 5, utf8_decode("Matricula"), 0, 0, 'L', 1);

        $this->SetX("28");
        $this->Cell(0, 5, utf8_decode("Servidor"), 0, 0, 'L', 1);

        $this->SetX("100");
        $this->Cell(0, 5, ("Endereco Local"), 0, 0, 'L', 1);

        $this->SetX("165");
        $this->Cell(0, 5, utf8_decode("Batidas"), 0, 1, 'L', 1);
        $this->Cell(0, 0, '', 1, 1, 'L');
    }

    function ColumnDetail() {

        $matricula = $_REQUEST['matricula'];
        $ano = $_REQUEST['ano'];
        $mes = $_REQUEST['mes'];

        TTransaction::open('pg_ceres');

        $repository = new TRepository('vw_ponto_mapa_ipsRecord');

        $criteria = new TCriteria;

        if (!empty($matricula)) {
            $criteria->add(new TFilter('matricula', '=', $matricula));
        }

        if ($mes > '0') {
            $criteria->add(new TFilter('mes', '=', $mes));
        }

        if ($ano) {
            $criteria->add(new TFilter('ano', '=', $ano));
        }
        $criteria->add(new TFilter('empresa_id', '=', $_SESSION['empresa_id']));

        $rows = $repository->load($criteria);

        if ($rows) {

            $i = 0;
            $total_servidor = 0;
            $total_servidor_nbatidas = 0;
            $nome_servidor = '';
            foreach ($rows as $row) {

                $this->SetFont('arial', '', 9);
                if ($nome_servidor != $row->nome_servidor) {
                    $this->SetFont('arial', 'B', 10);
                    if ($total_servidor != 0) {
                        $this->Cell(0, 0, '', 1, 1, 'L');
                        $this->SetFillColor(255, 255, 255);
                        $this->SetX("10");
                        $this->Cell(0, 5, utf8_decode("Total Batidas Locais: " . $total_servidor . "    Total Batidas: " . $total_servidor_nbatidas), 0, 1, 'L', 1);
                        $this->Ln(3);
                    }
                    $this->SetFillColor(235, 235, 235);
                    $this->SetX("10");
                    $this->Cell(0, 5, utf8_decode($row->matricula), 0, 0, 'L', 1);
                    $this->SetX("28");
                    $this->Cell(0, 5, utf8_decode($row->nome_servidor), 0, 0, 'L', 1);
                    $this->SetFont('arial', '', 9);
                    $nome_servidor = $row->nome_servidor;
                    $total_servidor = 0;
                    $total_servidor_nbatidas = 0;
                }
                if ($i % 2 == 0) {
                    $this->SetFillColor(235, 235, 235);
                } else {
                    $this->SetFillColor(255, 255, 255);
                }

                $this->SetX("100");
                $this->Cell(0, 5, utf8_decode($row->enderecolocal), 0, 0, 'L', 1);

                $this->SetX("165");
                $this->Cell(0, 5, utf8_decode($row->nbatidas), 0, 1, 'L', 1);
                $total_servidor_nbatidas +=$row->nbatidas;

                $i++;
                $total_servidor++;
            }
            $this->Cell(0, 0, '', 1, 1, 'L');
            $this->SetFont('arial', 'B', 10);
            $this->SetX("10");
            $this->Cell(0, 5, utf8_decode("Total Batidas Locais: " . $total_servidor . "    Total Batidas: " . $total_servidor_nbatidas), 0, 1, 'L');
            $this->Ln(3);
            $this->SetX("10");
            $this->Cell(0, 5, utf8_decode("Total Geral: " . $i), 0, 1, 'L');
        }
        TTransaction::close();

        $this->Cell(0, 0, '', 0, 1, 'L');
    }

    function Footer() {
        $this->SetY(-15);
        $this->SetFont('Arial', 'I', 8);
        $data = date("d/m/Y H:i:s");
        $conteudo = "impresso em " . $data;
        $texto = $_SESSION['empresa_nome'];
        $this->Cell(0, 0, '', 1, 1, 'L');

        $this->Cell(0, 5, $texto, 0, 0, 'L');
        $this->Cell(0, 5, 'Pag. ' . $this->PageNo() . ' de ' . '{nb}' . ' - ' . $conteudo, 0, 0, 'R');
        $this->Ln();
    }

}

$pdf = new RelatorioServidoresLocalizacaoBatidasPontoPDF("P", "mm", "A4");

$pdf->SetTitle("Relatorio Localizacao Batidas Ponto Servidore");
$pdf->SetSubject("Relatorio Localizacao Batidas Ponto Servidore");

$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times', '', 12);
$pdf->ColumnDetail();
$file = "app/reports/RelatorioServidoresLocalizacaoBatidasPontoPDF" . $_SESSION['servidor_id'] . ".pdf";

$pdf->Output($file);
$pdf->openFile($file);
