<?php

use Adianti\Database\TTransaction;
use Adianti\Database\TRepository;
use Adianti\Database\TCriteria;
use Adianti\Database\TFilter;
use Lib\Funcoes\Util;


class RelatorioDocumentoJustificativaPDF extends FPDF
{

    function Header()
    {
        $servidor = $_REQUEST['servidor_id'];

        $nome_servidor = NULL;
        $matricula_servidor = NULL;

        TTransaction::open('pg_ceres');


        if ($servidor != NULL){

            $servidor_record = new ServidorRecord($servidor);
            $nome_servidor = $servidor_record->nome;
            $matricula_servidor = $servidor_record->matricula;

        }

        TTransaction::close();

        //endereco da imagem,posicao X(horizontal),posicao Y(vertical), tamanho altura, tamanho largura
        $this->Image("app/images/logo_relatorio.jpg", 8, 11, 26, 18);

        $this->Image("app/images/logo_relatorio.jpg", 8, 11, 26, 18);

        $this->SetFont('Arial', 'B', 12);
        $this->SetX("25");
        $this->Cell(0, 5, utf8_decode("GOVERNO DO ESTADO DO RIO GRANDE DO NORTE"), 0, 1, 'C');

        $this->SetFont('Arial', 'B', 10);
        $this->SetX("35");
        $this->Cell(0, 5, utf8_decode($_SESSION['empresa_nome']), 0, 1, 'J');

        $this->SetX("25");
        $this->Cell(0, 5, utf8_decode("SISTEMA DE PONTO"), 0, 1, 'C');

        $this->SetX("25");
        $this->Cell(0, 5, utf8_decode("RELATÓRIO DE ACOMPANHAMENTO JUSTIFICATIVA"), 0, 1, 'C');

        if ($nome_servidor == NULL){

            $this->SetX("25");
            $this->Cell(0, 5, utf8_decode("SOLICITAÇÕES GERAIS DE SERVIDORES"), 0, 1, 'C');

        } else{

            $this->SetX("25");
            $this->Cell(0, 5, utf8_decode("SERVIDOR: ".$nome_servidor. " - MATRÍCULA: " .$matricula_servidor), 0, 1, 'C');

        }

        // coluna abaixo para poder dar o espaço entre o cabeçalho e a tabela
        $this->SetX("25");
        $this->Cell(0, 5, utf8_decode(""), 0, 1, 'C');

        $this->ColumnHeader();

    }

    function ColumnHeader()
    {
        $servidor = $_REQUEST['servidor_id'];

        //define a fonte a ser usada
        $this->SetFont('Arial', 'B', 12);
        $this->SetFillColor(235, 235, 235);

        $this->SetX("10");
        $this->Cell(0, 5, utf8_decode("Batida"), 1, 0, 'L', 1);

        $this->SetX("35");
        $this->Cell(0, 5, utf8_decode("Jornada"), 1, 0, 'L', 1);

        $this->SetX("55");
        $this->Cell(0, 5, utf8_decode("Apurado"), 1, 0, 'L', 1);

        $this->SetX("75");
        $this->Cell(0, 5, utf8_decode("Situação"), 1, 0, 'L', 1);

        $this->SetX("110");
        $this->Cell(0, 5, utf8_decode("Tipo"), 1, 0, 'L', 1);

        if ($servidor == NULL){

            $this->SetX("155");
            $this->Cell(0, 5, utf8_decode("Entregue"), 1, 0, 'L', 1);

            $this->SetX("178");
            $this->Cell(0, 5, utf8_decode("Matrícula"), 1, 1, 'L', 1);
        } else{

            $this->SetX("172");
            $this->Cell(0, 5, utf8_decode("Entregue"), 1, 1, 'L', 1);
        }

    }

    function ColumnDetail()
    {

        $servidor = $_REQUEST['servidor_id'];
        $situacao = $_REQUEST['situacaojustificativa'];
        $mes = $_REQUEST['mes'];
        $ano = $_REQUEST['ano'];

        $this->SetX("20");

        TTransaction::open('pg_ceres');

        $repository = new TRepository('vw_justificativa_acompanhamentoRecord');
        $criteria = new TCriteria;

        if ($servidor != NULL){

            $criteria->add(new TFilter('servidor_id', '=', $servidor));

            $criteria->setProperty('order', 'id DESC');
        }

        if ($situacao != NULL){

            $criteria->add(new TFilter('situacaojustificativa', '=', $situacao));
        }

        if ($mes != NULL){

            $criteria->add(new TFilter('mejustificativa', '=', $mes));
        }

        if ($ano != NULL){

            $criteria->add(new TFilter('anojustificativa', '=', $ano));
        }

        if ($servidor == NULL){

            $criteria->setProperty('order', 'servidor_id, databatida ASC');
        }


        $rows = $repository->load($criteria);
        if ($rows) {
            foreach ($rows as $row) {

                $this->SetFont('Arial', '', 10);
                $this->SetFillColor(255, 255, 255);


                $this->SetX("10");
                $this->Cell(0, 5, utf8_decode(Util::formatar_data($row->databatida)), 1, 0, 'L', 1);

                $this->SetX("35");
                $this->Cell(0, 5, utf8_decode(Util::formatar_hora($row->hora_jornada)), 1, 0, 'L', 1);

                $this->SetX("55");
                $this->Cell(0, 5, utf8_decode(Util::formatar_hora($row->horatotal)), 1, 0, 'L', 1);

                $this->SetX("75");
                $this->Cell(0, 5, utf8_decode($row->situacaojustificativa), 1, 0, 'L', 1);

                if ($servidor == NULL){

                    $this->SetX("110");
                    $this->Cell(0, 5, utf8_decode(substr($row->nometipojustificativa, 0, 17)), 1, 0, 'L', 1);

                    if ($row->dataautorizacao != NULL){
                        $this->SetX("155");
                        $this->Cell(0, 5, utf8_decode(Util::formatar_data($row->dataautorizacao)), 1, 0, 'L', 1);
                    } else{
                        $this->SetX("155");
                        $this->Cell(0, 5, utf8_decode(('NÃO')), 1, 0, 'L', 1);
                    }

                    $this->SetX("178");
                    $this->Cell(0, 5, utf8_decode(Util::formatar_data($row->matricula_servidor)), 1, 1, 'L', 1);

                } else{

                    $this->SetX("110");
                    $this->Cell(0, 5, utf8_decode(substr($row->nometipojustificativa, 0, 28)), 1, 0, 'L', 1);

                    if ($row->dataautorizacao != NULL){
                        $this->SetX("172");
                        $this->Cell(0, 5, utf8_decode(Util::formatar_data($row->dataautorizacao)), 1, 1, 'L', 1);
                    } else{
                        $this->SetX("172");
                        $this->Cell(0, 5, utf8_decode(('NÃO')), 1, 1, 'L', 1);
                    }
                }

            }
        }


        TTransaction::close();
    }

    //Page footer
    function Footer()
    {

        //Position at 1.5 cm from bottom
        $this->SetY(-15);
        //Arial italic 8
        $this->SetFont('Arial', 'I', 8);
        //Page number
        //data atual
        $data = date("d/m/Y H:i:s");
        $conteudo = "impresso em " . $data;
        $texto = $_SESSION['empresa_sitio'];
        //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
        $this->Cell(0, 0, '', 1, 1, 'L');

        //imprime uma celula... largura,altura, texto,borda,quebra de linha, alinhamento
        $this->Cell(0, 5, $texto, 0, 0, 'L');
        $this->Cell(0, 5, $conteudo, 0, 0, 'R');
        $this->Ln();
    }
}


$pdf = new RelatorioDocumentoJustificativaPDF("P", "mm", "A4");

//define o titulo
$pdf->SetTitle(utf8_decode("Relatorio das Justificativas"));

//assunto
$pdf->SetSubject(utf8_decode("Relatorio das Justificativas"));

$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times', '', 12);
$pdf->ColumnDetail();
$file = "app/reports/RelatorioDocumentoJustificativaPDF". ".pdf";

//abrir pdf
$pdf->Output($file);
$pdf->openFile($file);

?>
