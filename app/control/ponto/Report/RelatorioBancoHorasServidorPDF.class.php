<?php

use Adianti\Database\TTransaction;
use Adianti\Database\TRepository;
use Adianti\Database\TCriteria;
use Adianti\Database\TFilter;

include_once 'app/lib/funcdate.php';

class RelatorioBancoHorasServidorPDF extends FPDF
{
    function Header() {

        $servidor = $_REQUEST['servidor_id'];

        $nome_servidor = NULL;
        $matricula_servidor = NULL;

        TTransaction::open('pg_ceres');

        $servidor_record = new ServidorRecord($servidor);
        $nome_servidor = $servidor_record->nome;
        $matricula_servidor = $servidor_record->matricula;

        TTransaction::close();

        $mesTemp = $_REQUEST['mes'];
        $anoTemp = $_REQUEST['ano'];


        $this->Image("app/images/logo_relatorio.jpg", 8, 11, 26, 18);

        $this->SetFont('Arial', 'B', 12);
        $this->SetX("25");
        $this->Cell(0, 5, utf8_decode("GOVERNO DO ESTADO DO RIO GRANDE DO NORTE"), 0, 1, 'C');

        $this->SetFont('Arial', 'B', 10);
        $this->SetX("35");
        $this->Cell(0, 5, utf8_decode($_SESSION['empresa_nome']), 0, 1, 'C');

        $this->SetX("25");
        $this->Cell(0, 5, utf8_decode("SISTEMA DE PONTO - RELATÓRIO BANCO DE HORAS"), 0, 1, 'C');

        $this->SetX("25");
        $this->Cell(0, 5, utf8_decode("SERVIDOR: ".$nome_servidor. " - MATRÍCULA: " .$matricula_servidor), 0, 1, 'C');

        if ($mesTemp != null && $mesTemp != '00'){
            $mesTemp = retornaMes($mesTemp);

            $this->SetX("25");
            $this->Cell(0, 5, utf8_decode($mesTemp." de ".$anoTemp), 0, 1, 'C');
        }

        $this->Ln();

        $this->ColumnHeader();

    }

    function ColumnHeader() {

        $this->SetFont('Arial', 'B', 11);
        $this->SetFillColor(235, 235, 235);

        $this->SetX("10");
        $this->Cell(0, 5, utf8_decode("Mês"), 1, 0, 'L', 1);

        $this->SetX("30");
        $this->Cell(0, 5, utf8_decode("Ano"), 1, 0, 'L', 1);

        $this->SetX("50");
        $this->Cell(0, 5, utf8_decode("Minutos Prev."), 1, 0, 'L', 1);

        $this->SetX("80");
        $this->Cell(0, 5, utf8_decode("Minutos Trab."), 1, 0, 'L', 1);

        $this->SetX("110");
        $this->Cell(0, 5, utf8_decode("Horas Prev."), 1, 0, 'L', 1);

        $this->SetX("140");
        $this->Cell(0, 5, utf8_decode("Horas Trab."), 1, 0, 'L', 1);

        $this->SetX("170");
        $this->Cell(0, 5, utf8_decode("Saldo"), 1, 1, 'L', 1);

    }

    function ColumnDetail() {

        $servidorTemp = $_REQUEST['servidor_id'];
        $mesTemp = $_REQUEST['mes'];
        $anoTemp = $_REQUEST['ano'];
        $saldoTemp = $_REQUEST['saldo'];


        TTransaction::open('pg_ceres');

        $repository = new TRepository('VwServidorBancoHoras');

        $criteria = new TCriteria;

        $criteria->add(new TFilter('servidor_id', '=',$servidorTemp));
        $criteria->add(new TFilter('ano', '=',$anoTemp));

        if ($mesTemp != null && $mesTemp != '00'){

            $criteria->add(new TFilter('mes', '=',$mesTemp));

        }

        if ($saldoTemp != null){

            switch ($saldoTemp) {

                case 'POSITIVO':
                    $criteria->add(new TFilter('saldo', '>', '00:00:00'));
                    break;

                case 'NEGATIVO':
                    $criteria->add(new TFilter('saldo', '<', '00:00:00'));
                    break;

                case 'ZERADO':
                    $criteria->add(new TFilter('saldo', '=', '00:00:00'));
                    break;

            }
        }

        $rows = $repository->load($criteria);

        if ($rows) {

            foreach ($rows as $row) {

                $this->SetFont('arial', '', 10);

                $this->SetX("10");
                $this->Cell(0, 5, utf8_decode(retornaMes($row->mes)), 1, 0, 'L');

                $this->SetX("30");
                $this->Cell(0, 5, utf8_decode($row->ano), 1, 0, 'L');

                $this->SetX("50");
                $this->Cell(0, 5, utf8_decode($row->minutosprevisto), 1, 0, 'L');

                $this->SetX("80");
                $this->Cell(0, 5, utf8_decode($row->minutostrabalhado), 1, 0, 'L');

                $this->SetX("110");
                $this->Cell(0, 5, utf8_decode($row->prevtotal), 1, 0, 'L');

                $this->SetX("140");
                $this->Cell(0, 5, utf8_decode($row->trabtotal), 1, 0, 'L');

                $this->SetX("170");
                $this->Cell(0, 5, utf8_decode($row->saldo), 1, 1, 'L');


            }

            $this->Ln();
            $this->Ln();
            $this->Ln();
            $this->Ln();

            $servidor = new ServidorRecord($row->servidor_id);


            $this->SetX("0");
            $this->SetFont('arial', 'B', 10);
            $this->Cell(0, 5, utf8_decode("____________________________________________"), 0, 1, 'C');
            //$this->Ln();
            $this->Cell(0, 5, utf8_decode($servidor->nome), 0, 1, 'C');

            $this->SetX("0");
            $this->Cell(0, 5, utf8_decode($servidor->matricula), 0, 1, 'C');

            $this->Ln();
            $this->Ln();

            $this->SetX("0");
            $this->SetFont('arial', 'B', 10);
            $this->Cell(0, 5, utf8_decode("____________________________________________"), 0, 1, 'C');
            //$this->Ln();
            $this->Cell(0, 5, utf8_decode("ASSINATURA DO CHEFE IMEDIATO"), 0, 1, 'C');

            $this->SetX("0");
            $this->Cell(0, 5, utf8_decode("MATRÍCULA:"), 0, 1, 'C');


        } else{
            $this->Ln();

            $this->SetX("0");
            $this->SetFont('arial', 'I', 12);
            $this->Cell(0, 5, utf8_decode("NÃO HÁ DADOS A SEREM EXIBIDOS"), 0, 1, 'C');

        }
        TTransaction::close();

        $this->Cell(0, 0, '', 0, 1, 'L');
        $this->Cell(0, 0, '', 0, 1, 'L');
        $this->Cell(0, 0, '', 0, 1, 'L');
    }

    function Footer() {
        $this->SetY(-15);
        $this->SetFont('Arial', 'I', 8);
        $data = date("d/m/Y H:i:s");
        $conteudo = "impresso em " . $data;
        $texto = $_SESSION['empresa_nome'];
        $this->Cell(0, 0, '', 1, 1, 'L');

        $this->Cell(0, 5, $texto, 0, 0, 'L');
        $this->Cell(0, 5, 'Pag. ' . $this->PageNo() . ' de ' . '{nb}' . ' - ' . $conteudo, 0, 0, 'R');
        $this->Ln();
    }

}

$pdf = new RelatorioBancoHorasServidorPDF("P", "mm", "A4");

$pdf->SetTitle("Relatorio de Banco de Horas");

$pdf->SetSubject("Relatorio de Banco de Horas");

$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times', '', 12);
$pdf->ColumnDetail();
$file = "app/reports/RelatorioBancoHorasServidorPDF" . $_SESSION['servidor_id'] . ".pdf";

$pdf->Output($file);
$pdf->openFile($file);