<?php

//ini_set('display_errors', 1);
//ini_set('display_startup_erros', 1);
//error_reporting(E_ALL);

use Adianti\Database\TTransaction;
use Adianti\Database\TRepository;
use Adianti\Database\TCriteria;
use Adianti\Database\TFilter;

class RelatorioBatidasPontoServidorGeralPDF extends FPDF 
{

    function Header() 
    {
    	
        $this->Image("app/images/logo_relatorio.jpg", 8, 8, 20, 18);

        $this->SetFont('Arial', 'B', 10);
        $this->SetX("25");
        $this->Cell(0, 5, utf8_decode("GOVERNO DO ESTADO DO RIO GRANDE DO NORTE"), 0, 1, 'C');

        $this->SetX("35");
        $this->Cell(0, 5, utf8_decode($_SESSION['empresa_nome']), 0, 1, 'J');

        $items = array();
        $items['01'] = 'JANEIRO';
        $items['02'] = 'FEVEREIRO';
        $items['03'] = 'MARÇO';
        $items['04'] = 'ABRIL';
        $items['05'] = 'MAIO';
        $items['06'] = 'JUNHO';
        $items['07'] = 'JULHO';
        $items['08'] = 'AGOSTO';
        $items['09'] = 'SETEMBRO';
        $items['10'] = 'OUTUBRO';
        $items['11'] = 'NOVEMBRO';
        $items['12'] = 'DEZEMBRO';
        
        $geral = $_REQUEST['tipo_funcionario'] . ' - ' . $items[$_REQUEST['mes']] . '/' . $_REQUEST['ano'];
        
        $this->SetX("25");
        $this->Cell(0, 5, utf8_decode("SISTEMA DE PONTO: " . $geral ), 0, 1, 'C');
        
    }

    function ColumnDetail() 
    {

    	$mes = $_REQUEST['mes'];
    	$ano = $_REQUEST['ano'];
    	$tipoFuncionario = $_REQUEST['tipo_funcionario'];
    	$municipio_id = $_REQUEST['municipio_id'];
    	$setor_id = $_REQUEST['setor_id'];
    	
        $this->SetX("20");

        TTransaction::open('pg_ceres');

        $repository = new TRepository('vw_marcacao_relogio_chefeRecord');

        $criteria = new TCriteria;
        
        $criteria->setProperty('order', 'nome, databatida DESC');
        
        $criteria->add(new TFilter('mes', '=', $mes));
        $criteria->add(new TFilter('ano', '=', $ano));
        $criteria->add(new TFilter('empresa_id', '=', $_SESSION['empresa_id']));
        
        if( $setor_id ) {
        
        	$criteria->add(new TFilter('setor_id', '=', $setor_id));
        
        }
        
        if( !empty($municipio_id) ) {

        	$criteria->add(new TFilter('regional_id', '=', $_SESSION['regional_id']));
        	 
        	if ( $municipio_id != 'TODOS' ) {
        		 
        		$criteria->add(new TFilter('municipio_id', '=', $municipio_id));
        		 
        	}
        	
        }
        
        if( $tipoFuncionario == 'BOLSISTA FUNCITERN' )
        {
        	
        	//Bolsistas FUNCITERN não começam com a matricula 992
        	$criteria->add(new TFilter('matricula', 'NOT LIKE', '992%' ));
        	$criteria->add(new TFilter('tipofuncionario', '=', 'BOLSISTA'));
        	
        }elseif( $tipoFuncionario == 'BOLSISTA FGD' )
        {
        	
        	//Bolsistas FGD começam com a matricula 992
        	$criteria->add(new TFilter('matricula', 'LIKE', '992%' ));
        	$criteria->add(new TFilter('tipofuncionario', '=', 'BOLSISTA'));
        	
        }else
        {
        	
        	$criteria->add(new TFilter('tipofuncionario', '=', $tipoFuncionario));
        	
        }

        $rows = $repository->load($criteria);

        $previsao = 0;
        $executado = 0;
        $matricula = '';
        
        if ($rows) 
        {

            $i = 0;
            
            foreach ($rows as $row) 
            {

                if ($matricula != $row->matricula) 
                {
                	
                	if( $matricula != '' )
                	{
                		
                		$this->SetX("7");
                		$this->SetFont('Arial', 'B', 10);
                		$this->SetX("7");
                		
                		$tmphora = ((int) ($previsao / 60)) . 'h' . ($previsao % 60);
                		$tmphora1 = ((int) ($executado / 60)) . 'h' . ($executado % 60);
                		
                		if ($executado > $previsao) 
                		{
                			
                			$tmphora2 = '    Saldo:' . ((int) (($executado - $previsao) / 60)) . 'h' . (($executado - $previsao) % 60);
                		
                		} else 
                		{
                			
                			$tmphora2 = '    Débito:' . ((int) (($previsao - $executado) / 60)) . 'h' . (($previsao - $executado) % 60);
                		
                		}
                		
                		$this->Cell(0, 10, (utf8_decode("Dias úteis: ") . utf8_decode(substr($i, 0, 4)) . (utf8_decode("   Previsto: ") . utf8_decode($tmphora)) . (utf8_decode("    Trabalhado: ") . utf8_decode($tmphora1)) . utf8_decode($tmphora2)), 0, 1, 'L');
                		
                		$previsao = 0;
                		$executado = 0;
                		$i = 0;
                		
                		$this->AddPage();
                	
                	}

                    $matricula = $row->matricula;

                    $this->Ln();
                    $this->Ln();

                    $this->SetX("10");
                    $this->SetFont('arial', 'B', 10);
                    $this->Cell(0, 5, utf8_decode("Matricula: " . $row->matricula), 0, 0, 'L');

                    $this->SetX("43");
                    $this->SetFont('arial', 'B', 10);
<<<<<<< .mine
                    $this->Cell(0, 5, utf8_decode("Nome: " . $row->nome_servidor ), 0, 1, 'L');
                 
                    $this->SetX("43");
                    $this->SetFont('arial', 'B', 10);
                    $this->Cell(0, 5, utf8_decode("LOTAÇÃO: " . $row->lotacao), 0, 1, 'L');
||||||| .r311
                    $this->Cell(0, 5, utf8_decode("Nome: " . $row->nome_servidor . "    LOTAÇÃO: " . $row->lotacao), 0, 1, 'L');
=======
                    $this->Cell(0, 5, utf8_decode("Nome: " . $row->nome . "    LOTAÇÃO: " . $row->lotacao), 0, 1, 'L');
>>>>>>> .r322

                    $this->SetX("35");
                    $this->Cell(0, 5, utf8_decode("Turno 1"), 0, 0, 'L');

                    $this->SetX("72");
                    $this->Cell(0, 5, utf8_decode("Turno 2"), 0, 0, 'L');

                    $this->SetX("107");
                    $this->Cell(0, 5, utf8_decode("Turno 3"), 0, 1, 'L');

                    $this->SetFont('Arial', 'B', 10);

                    $this->SetX("10");
                    $this->Cell(0, 4, utf8_decode("Data"), 0, 0, 'L');

                    $this->SetX("33");
                    $this->Cell(0, 5, utf8_decode("Inicio/Fim"), 0, 0, 'L');

                    $this->SetX("55");
                    $this->Cell(0, 5, utf8_decode("Total"), 0, 0, 'L');

                    $this->SetX("70");
                    $this->Cell(0, 5, utf8_decode("Inicio/Fim"), 0, 'L');

                    $this->SetX("90");
                    $this->Cell(0, 5, utf8_decode("Total"), 0, 0, 'L');

                    $this->SetX("105");
                    $this->Cell(0, 5, utf8_decode("Inicio/Fim"), 0, 'L');

                    $this->SetX("125");
                    $this->Cell(0, 5, utf8_decode(" Total"), 0, 0, 'L');

                    $this->SetX("140");
                    $this->Cell(0, 5, utf8_decode("Compareceu"), 0, 0, 'L');

                    $this->SetX("178");
                    $this->Cell(0, 5, utf8_decode("Prev."), 0, 0, 'L');

                    $this->SetX("190");
                    $this->Cell(0, 5, utf8_decode("Trab."), 0, 1, 'L');
                    $this->Cell(0, 0, '', 1, 1, 'L');
                    
                }
                
                $this->SetFont('arial', 'B', 10);

                $this->SetX("10");
                $this->Cell(0, 4, utf8_decode(formatar_data($row->databatida)), 0, 0, 'L');

                $this->SetX("33");
                if ($row->batida01) {
                    $this->Cell(0, 5, utf8_decode(formatar_hora2($row->batida01) . "/"), 0, 0, 'L');
                } else {
                    $this->Cell(0, 5, utf8_decode($row->batida01), 0, 0, 'L');
                }

                $this->SetX("43");
                if ($row->batida02) {
                    $this->Cell(0, 5, utf8_decode(formatar_hora2($row->batida02) . "  - "), 0, 0, 'L');
                } else {
                    $this->Cell(0, 5, utf8_decode($row->batida02), 0, 0, 'L');
                }

                $this->SetX("57");
                if ($row->hora01) {
                    $this->Cell(0, 5, utf8_decode(formatar_hora($row->hora01)), 0, 0, 'L');
                } else {
                    $this->Cell(0, 5, utf8_decode($row->hora01), 0, 0, 'L');
                }

                $this->SetX("70");
                if ($row->batida03) {
                    $this->Cell(0, 5, utf8_decode(formatar_hora2($row->batida03) . "/"), 0, 0, 'L');
                } else {
                    $this->Cell(0, 5, utf8_decode($row->batida03), 0, 0, 'L');
                }

                $this->SetX("80");
                if ($row->batida04) {
                    $this->Cell(0, 5, utf8_decode(formatar_hora2($row->batida04) . "  - "), 0, 0, 'L');
                } else {
                    $this->Cell(0, 5, utf8_decode($row->batida04), 0, 0, 'L');
                }

                $this->SetX("95");
                if ($row->hora02) {
                    $this->Cell(0, 5, utf8_decode(formatar_hora($row->hora02)), 0, 0, 'L');
                } else {
                    $this->Cell(0, 5, utf8_decode($row->hora02), 0, 0, 'L');
                }

                $this->SetX("115");
                if ($row->batida05) {
                    $this->Cell(0, 5, utf8_decode(formatar_hora2($row->batida05) . "/"), 0, 0, 'L');
                } else {
                    $this->Cell(0, 5, utf8_decode($row->batida05), 0, 0, 'L');
                }

                $this->SetX("125");
                if ($row->batida06) {
                    $this->Cell(0, 5, utf8_decode(formatar_hora2($row->batida06) . "  - "), 0, 0, 'L');
                } else {
                    $this->Cell(0, 5, utf8_decode($row->batida06), 0, 0, 'L');
                }

                $this->SetX("140");
                $this->Cell(0, 5, utf8_decode(substr($row->compareceu,0,15)), 0, 0, 'L');

                $this->SetX("150");
                if ($row->hora03) {
                    $this->Cell(0, 5, utf8_decode(formatar_hora($row->hora03)), 0, 0, 'L');
                } else {
                    $this->Cell(0, 5, utf8_decode($row->hora03), 0, 0, 'L');
                }


                $this->SetX("178");
                $this->Cell(0, 5, utf8_decode(formatar_hora($row->hora_jornada)), 0, 0, 'L');

                $previsao = ($previsao + $row->minutojornada);
                $executado = ($executado + $row->minutototal);

                $this->SetX("190");
                $this->Cell(0, 5, utf8_decode(formatar_hora($row->horatotal)), 0, 1, 'L');
                $this->Cell(0, 0, '', 1, 1, 'L');

                $i = $i + 1;

                $this->Ln();
               
            }
            
            $this->SetX("7");
            $this->SetFont('Arial', 'B', 10);
            $this->SetX("7");
            
            $tmphora = ((int) ($previsao / 60)) . 'h' . ($previsao % 60);
            $tmphora1 = ((int) ($executado / 60)) . 'h' . ($executado % 60);
            
            if ($executado > $previsao)
            {
            	 
            	$tmphora2 = '    Saldo:' . ((int) (($executado - $previsao) / 60)) . 'h' . (($executado - $previsao) % 60);
            
            } else
            {
            	 
            	$tmphora2 = '    Débito:' . ((int) (($previsao - $executado) / 60)) . 'h' . (($previsao - $executado) % 60);
            
            }
            
            $this->Cell(0, 10, (utf8_decode("Dias úteis: ") . utf8_decode(substr($i, 0, 4)) . (utf8_decode("   Previsto: ") . utf8_decode($tmphora)) . (utf8_decode("    Trabalhado: ") . utf8_decode($tmphora1)) . utf8_decode($tmphora2)), 0, 1, 'L');

        }else
        {
        	
        	$this->SetFont('arial', 'B', 12);
        	$this->Ln(15);
        	$this->SetX("50");
        	$this->Cell(0, 5, utf8_decode("Nenhum dado encontrado para os filtros informados."), 0, 1, 'L');
        	
        }
        
        TTransaction::close();

        $this->Cell(0, 0, '', 0, 1, 'L');
        $this->Cell(0, 0, '', 0, 1, 'L');
        $this->Cell(0, 0, '', 0, 1, 'L');
        
    }

    function Footer() 
    {
    	
        $this->SetY(-15);
        $this->SetFont('Arial', 'I', 8);
        $data = date("d/m/Y H:i:s");
        $conteudo = "impresso em " . $data;
        $texto = $_SESSION['empresa_nome'];
        $this->Cell(0, 0, '', 1, 1, 'L');

        $this->Cell(0, 5, $texto, 0, 0, 'L');
        $this->Cell(0, 5, 'Pag. ' . $this->PageNo() . ' de ' . '{nb}' . ' - ' . $conteudo, 0, 0, 'R');
        $this->Ln();
        
    }

}

$pdf = new RelatorioBatidasPontoServidorGeralPDF("P", "mm", "A4");

$pdf->SetTitle("Relatorio de Marcacao do Servidores Geral");

$pdf->SetSubject("Relatorio de Marcacao do Servidores Geral");

$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times', '', 12);
$pdf->ColumnDetail();
$file = "app/reports/RelatorioBatidasPontoServidorGeralPDF" . $_SESSION['servidor_id'] . ".pdf";

$pdf->Output($file);
$pdf->openFile($file);

?>