<?php

use Adianti\Database\TTransaction;
use Adianti\Database\TRepository;
use Adianti\Database\TCriteria;
use Adianti\Database\TFilter;
use Lib\Funcoes\Util;

ini_set('display_errors', 1);
ini_set('display_startup_erros', 1);
error_reporting(E_ALL);
/*
 * Bater Ponto Servidor
 * Autor:Jackson Meires
 * Data: 23/07/2018
 */

//include_once 'app/lib/funcdate.php';

class RelatorioBatidaServidorChefePDF extends FPDF
{

    private $servidor_jornada_registro;
    private $servidor_escala_registro;

    function Header()
    {

        $this->servidor_escala_registro = false;

        TTransaction::open('pg_ceres');
        $objServidor = new ServidorRecord($_REQUEST['servidor_id']);
        TTransaction::close();

        $this->Image("app/images/logo_relatorio.jpg", 8, 11, 24, 18);

        $this->SetFont('Arial', 'B', 12);
        $this->SetX("15");
        $this->Cell(0, 5, utf8_decode("GOVERNO DO ESTADO DO RIO GRANDE DO NORTE"), 0, 1, 'C');

        $this->SetFont('Arial', 'B', 10);
        $this->SetX("25");
        $this->Cell(0, 5, utf8_decode($_SESSION['empresa_nome']), 0, 1, 'C');

        $this->SetX("15");
        $this->Cell(0, 5, utf8_decode("SISTEMA DE PONTO"), 0, 1, 'C');

        $this->SetX("15");
        $this->SetFont('arial', 'B', 10);
        $this->MultiCell(0, 5, utf8_decode("Nome: " . $objServidor->nome . " Matricula: " . $objServidor->matricula . "\n" . " CPF: " . $objServidor->cpf . " Situação: " . $objServidor->situacao . " Mês: " . Util::retornaMes($_REQUEST['mes']) . " Ano: " . $_REQUEST['ano']), 0, 'C');

        TTransaction::open('pg_ceres');

        if (!empty($objServidor->jornada_id)) {
            $repository = new TRepository('vw_servidor_jornada_completaRecord');
            $criteria = new TCriteria;

            $criteria->add(new TFilter('servidor_id', '=', $objServidor->id));

            $objects = $repository->load($criteria);

            foreach ($objects as $row) {

                $this->servidor_jornada_registro = $row;

                $textoHorario = '';
                $textoEntrada = '';
                $textoSaida = '';

                if (!empty($row->entrada3)) {

                    $textoHorario .= "Turno 01: " . formatar_hora($row->entrada1) . " às " . formatar_hora($row->saida1) . " - Turno 02: " . formatar_hora($row->entrada2) . " às " . formatar_hora($row->saida2) . " - Turno 03: " . formatar_hora($row->entrada3) . " às " . formatar_hora($row->saida3);
                    $textoEntrada .= "Entrada 01: " . formatar_hora($row->entrada1inicio) . " até " . formatar_hora($row->entrada1fim) . " - Entrada 02: " . formatar_hora($row->entrada2inicio) . " até " . formatar_hora($row->entrada2fim) . " - Entrada 03: " . formatar_hora($row->entrada3inicio) . " até " . formatar_hora($row->entrada3fim);
                    $textoSaida .= "Saida 01: " . formatar_hora($row->saida1inicio) . " até " . formatar_hora($row->saida1fim) . " - Saida 02: " . formatar_hora($row->saida2inicio) . " até " . formatar_hora($row->saida2fim) . " - Saida 03: " . formatar_hora($row->saida3inicio) . " até " . formatar_hora($row->saida3fim);

                } else if (!empty($row->entrada2)) {

                    $textoHorario .= "Turno 01: " . formatar_hora($row->entrada1) . " às " . formatar_hora($row->saida1) . " - Turno 02: " . formatar_hora($row->entrada2) . " às " . formatar_hora($row->saida2);
                    $textoEntrada .= "Entrada 01: " . formatar_hora($row->entrada1inicio) . " até " . formatar_hora($row->entrada1fim) . " - Entrada 02: " . formatar_hora($row->entrada2inicio) . " até " . formatar_hora($row->entrada2fim);
                    $textoSaida .= "Saída 01: " . formatar_hora($row->saida1inicio) . " até " . formatar_hora($row->saida1fim) . " - Saida 02: " . formatar_hora($row->saida2inicio) . " até " . formatar_hora($row->saida2fim);

                } else if (!empty($row->entrada1)) {

                    $textoHorario .= "Horário: " . formatar_hora($row->entrada1) . " às " . formatar_hora($row->saida1);
                    $textoEntrada .= "Entrada: " . formatar_hora($row->entrada1inicio) . " até " . formatar_hora($row->entrada1fim);
                    $textoSaida .= "Saida: " . formatar_hora($row->saida1inicio) . " até " . formatar_hora($row->saida1fim);

                }

                $this->SetX("15");
                $this->MultiCell(0, 5, utf8_decode($textoHorario), 0, 'C');

                $this->SetX("15");
                $this->MultiCell(0, 5, utf8_decode($textoEntrada), 0, 'C');

                $this->SetX("15");
                $this->MultiCell(0, 5, utf8_decode($textoSaida), 0, 'C');

            }
        } else {
            $objEscala = new EscalaRecord($objServidor->escala_id);

            $this->SetX("15");
            $this->MultiCell(0, 5, ("Escala\n" . $objEscala->nome), 0, 'C');

            $this->SetX("15");
            $this->MultiCell(0, 5, utf8_decode("Horário Início " . $objEscala->iniciohorario), 0, 'C');

            $this->servidor_escala_registro = true;
        }

        TTransaction::close();

        $this->Ln(5);

        $this->ColumnHeader();
    }

    function ColumnHeader()
    {

        $this->SetFont('Arial', 'B', 12);

        $this->SetX("10");
        $this->Cell(0, 5, utf8_decode("Data"), 0, 0, 'L');

        $this->SetX("30");
        $this->Cell(0, 5, utf8_decode("Entrada"), 0, 0, 'L');

        $this->SetX("60");
        $this->Cell(0, 5, utf8_decode("Saída"), 0, 1, 'L');

    }

    function ColumnDetail()
    {
        TTransaction::open('pg_ceres');

        $servidor_id = $_REQUEST['servidor_id'];
        $mes = $_REQUEST['mes'];
        $ano = $_REQUEST['ano'];

        $objServidor = new ServidorRecord($servidor_id);

        if (empty($objServidor->escala_id)) {

            $repository = new TRepository('BatidaRecord');
            $criteria = new TCriteria;

            if (!empty($mes)) {
                $criteria->add(new TFilter(('date_part(\'month\', databatida)'), '=', $mes));
            }
            if (!empty($ano)) {
                $criteria->add(new TFilter('date_part(\'YEAR\', databatida)', '=', $ano));
            }
            $criteria->setProperty('order', 'databatida desc');

        } else {
            $repository = new TRepository('vw_servidor_escala_batidaRecord');
            $criteria = new TCriteria;

            if (!empty($mes)) {
                $criteria->add(new TFilter('mes', '=', $mes));
            }
            if (!empty($ano)) {
                $criteria->add(new TFilter('ano', '=', $ano));
            }
        }

        $criteria->add(new TFilter('matricula', '=', $objServidor->matricula));
        $objects = $repository->load($criteria);
        $count = count($objects);

        if ($objects) {

            $databatida = '';
            $coutBV = 0;
            $coutBINV = 0;

            if (empty($objServidor->escala_id)) {
                foreach ($objects as $row) {

                    $countBD = $this->qtdBatidaRepetidaEntradaSaida($objServidor->matricula, Util::formatar_data($row->databatida), $row->funcao);
                    if ($countBD > 1) {
                        $this->Ln(5);
                    }

                    $this->SetFont('Arial', '', 10);

                    if ($databatida != Util::formatar_data($row->databatida)) {

                        if ($databatida != '')
                            $this->Ln(5);

                        $this->SetX(10);
                        $this->Cell(0, 0, '', 1, 1, 'L');

                        $this->SetX(10);
                        $this->Cell(0, 5, Util::formatar_data($row->databatida), 0, 0, 'L');

                        $databatida = Util::formatar_data($row->databatida);
                    }
                    if ($row->funcao == "ENTRADA") {
                        $this->SetX(30);
                        if ($this->validarIntervaloBatida($servidor_id, ($row->databatida), "ENTRADA")) {
                            $this->SetTextColor(65, 171, 1);
                            $this->SetFont('Arial', 'I', 10);
                            $coutBV++;
                        } else {
                            $this->SetTextColor(255, 0, 0);
                            $this->SetFont('Arial', 'B', 10);
                            $coutBINV++;

                        }

                        $this->Cell(0, 5, Util::timeStampToTime($row->databatida), 0, 0, 'L');
                        $this->SetTextColor(0, 0, 0);
                    }
                    if ($row->funcao == "SAIDA") {
                        $this->SetX(60);
                        if ($this->validarIntervaloBatida($servidor_id, ($row->databatida), "SAIDA")) {
                            $this->SetFont('Arial', 'I', 10);
                            $this->SetTextColor(65, 171, 1);
                            $coutBV++;
                        } else {
                            $this->SetTextColor(255, 0, 0);
                            $this->SetFont('Arial', 'B', 10);
                            $coutBINV++;
                        }

                        $this->Cell(0, 5, Util::timeStampToTime($row->databatida), 0, 0, 'L');
                        $this->SetTextColor(0, 0, 0);
                    }


                }
            } else {
                foreach ($objects as $row) {
                    if ($row->databatida_inicio) {
                        $this->SetX(10);
                        $this->Cell(0, 5, Util::formatar_data($row->databatida_inicio), 0, 0, 'L');
                        $this->SetX(30);
                        if ($row->validar_entrada) {
                            $this->SetTextColor(65, 171, 1);
                            $this->SetFont('Arial', 'I', 10);
                            $coutBV++;
                        } else {
                            $this->SetTextColor(255, 0, 0);
                            $this->SetFont('Arial', 'B', 10);
                            $coutBINV++;
                        }

                        $this->Cell(0, 5, Util::timeStampToTime($row->databatida_inicio), 0, 0, 'L');
                        $this->SetTextColor(0, 0, 0);

                        $this->SetX(60);
                        if ($row->validar_saida) {
                            $this->SetFont('Arial', 'I', 10);
                            $this->SetTextColor(65, 171, 1);
                            $coutBV++;
                        } else {
                            $this->SetTextColor(255, 0, 0);
                            $this->SetFont('Arial', 'B', 10);
                            $coutBINV++;
                        }

                        $this->Cell(0, 5, Util::timeStampToTime($row->databatida_fim), 0, 1, 'L');
                        $this->SetTextColor(0, 0, 0);
                        $this->SetFont('Arial', '', 10);
                    }
                }
            }

            $this->Ln(5);
            $this->SetFont('Arial', 'B', 10);
            $this->Cell(0, 0, '', 1, 1, 'L');
            $this->SetX("10");
            $this->MultiCell(0, 5, utf8_decode("Total de Batidas: " . $count . "\nVálidas: " . $coutBV . "\nInválidas: " . $coutBINV));

        } else {
            $this->SetX("10");
            $this->Cell(0, 5, utf8_decode("Nenhum registro encontrado para os parametros informado"), 0, 1, 'C');

            if ($this->servidor_escala_registro) {
                $this->SetX("10");
                $this->Cell(0, 5, utf8_decode("Nenhum registros de escala definido para as batidas de ponto deste servidor"), 0, 1, 'C');
            }
        }

        TTransaction::close();

    }

    function qtdBatidaRepetidaEntradaSaida($matricula, $databatida, $funcao)
    {
        TTransaction::open('pg_ceres');

        $repository = new TRepository('BatidaRecord');
        $criteria = new TCriteria;
        $criteria->add(new TFilter('matricula', '=', $matricula));
        $criteria->add(new TFilter('date(databatida)', '=', $databatida));
        $criteria->add(new TFilter('funcao', '=', $funcao));

        $count = $repository->count($criteria);

        return $count;

        TTransaction::close();
    }

    function validarIntervaloBatida($servidor_id, $batida, $funcao)
    {
        TTransaction::open('pg_ceres');

        $batida = Util::timeStampToTime($batida);

        $repository = new TRepository('vw_servidor_jornada_completaRecord');
        $criteria = new TCriteria;

        $criteria->add(new TFilter('servidor_id', '=', $servidor_id));

        $objects = $repository->load($criteria);

        $count = 0;
        foreach ($objects as $row) {
            if (empty($row->entrada2)) {
                if ($funcao == "ENTRADA") {
                    $criteria->add(new TFilter("'" . $batida . "'", 'BETWEEN', "" . ($row->entrada1inicio) . "", "" . ($row->entrada1fim) . ""));
                    $count += $repository->count($criteria);
                } else {
                    $criteria->add(new TFilter("'" . $batida . "'", 'BETWEEN', "" . ($row->saida1inicio) . "", "" . ($row->saida1fim) . ""));
                    $count += $repository->count($criteria);

                }

            } else if (empty($row->entrada3)) {
                if ($funcao == "ENTRADA") {
                    $criteria->add(new TFilter("'" . $batida . "'", 'BETWEEN', "" . ($row->entrada1inicio) . "", "" . ($row->entrada1fim) . ""));
                    $count += $repository->count($criteria);

                    $criteria = new TCriteria;
                    $criteria->add(new TFilter("'" . $batida . "'", 'BETWEEN', "" . ($row->entrada2inicio) . "", "" . ($row->entrada2fim) . ""));
                    $count += $repository->count($criteria);

                } else {
                    $criteria->add(new TFilter("'" . $batida . "'", 'BETWEEN', "" . ($row->saida1inicio) . "", "" . ($row->saida1fim) . ""));
                    $count += $repository->count($criteria);

                    $criteria = new TCriteria;
                    $criteria->add(new TFilter("'" . $batida . "'", 'BETWEEN', "" . ($row->saida2inicio) . "", "" . ($row->saida2fim) . ""));
                    $count += $repository->count($criteria);
                }

            } else if (!empty($row->entrada3)) {
                if ($funcao == "ENTRADA") {

                    $criteria->add(new TFilter("'" . $batida . "'", 'BETWEEN', "" . ($row->entrada1inicio) . "", "" . ($row->entrada1fim) . ""));
                    $count += $repository->count($criteria);

                    $criteria->add(new TFilter("'" . $batida . "'", 'BETWEEN', "" . ($row->entrada2inicio) . "", "" . ($row->entrada2fim) . ""));
                    $count += $repository->count($criteria);

                    $criteria->add(new TFilter("'" . $batida . "'", 'BETWEEN', "" . ($row->entrada3inicio) . "", "" . ($row->entrada3fim) . ""));
                    $count += $repository->count($criteria);
                } else {
                    $criteria->add(new TFilter("'" . $batida . "'", 'BETWEEN', "" . ($row->saida1inicio) . "", "" . ($row->saida1fim) . ""));
                    $count += $repository->count($criteria);

                    $criteria->add(new TFilter("'" . $batida . "'", 'BETWEEN', "" . ($row->saida2inicio) . "", "" . ($row->saida2fim) . ""));
                    $count += $repository->count($criteria);

                    $criteria->add(new TFilter("'" . $batida . "'", 'BETWEEN', "" . ($row->saida3inicio) . "", "" . ($row->saida3fim) . ""));
                    $count += $repository->count($criteria);

                }
            }
        }
        $valida = false;
        if ($count > 0) {
            $valida = true;
        }
        TTransaction::close();
        return $valida;
    }


    function Footer()
    {
        $this->SetY(-15);
        $this->SetFont('Arial', 'I', 8);
        $this->Cell(0, 0, '', 1, 1, 'L');
        $this->Cell(0, 5, $_SESSION['empresa_sitio'], 0, 0, 'L');
        $this->Cell(0, 5, "impresso em " . date("d / m / Y H:i:s"), 0, 0, 'R');
        $this->Ln();
    }
}


$pdf = new RelatorioBatidaServidorChefePDF("P", "mm", "A4");

//define o titulo
$pdf->SetTitle(utf8_decode("Relatorio Batida Servidor"));

//assunto
$pdf->SetSubject(utf8_decode("Relatorio Batida Servidor"));

$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times', '', 12);
$pdf->ColumnDetail();
$file = "app/reports/RelatorioBatidaServidorChefePDF" . $_SESSION['servidor_id'] . ".pdf";

//abrir pdf
$pdf->Output($file);
$pdf->openFile($file);