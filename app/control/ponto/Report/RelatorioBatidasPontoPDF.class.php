<?php

//ini_set('display_errors', 1);
//ini_set('display_startup_erros', 1);
//error_reporting(E_ALL);

use Adianti\Database\TTransaction;
use Adianti\Database\TRepository;
use Adianti\Database\TCriteria;
use Adianti\Database\TFilter;

//Atualizada

class RelatorioBatidasPontoPDF extends FPDF {

    function Header() {
        //endereco da imagem,posicao X(horizontal),posicao Y(vertical), tamanho altura, tamanho largura
        $this->Image("app/images/logo_relatorio.jpg", 8, 11, 26, 18);

        //Arial bold 15
        $this->SetFont('Arial', 'B', 12);
        $this->SetY("12");
        $this->SetX("25");
        $this->Cell(0, 5, utf8_decode("GOVERNO DO ESTADO DO RIO GRANDE DO NORTE"), 0, 1, 'C');

        $this->SetFont('Arial','B',10);
        $this->SetY("17");
        $this->SetX("35");
        $this->Cell(0,5,utf8_decode( $_SESSION['empresa_nome'] ),0,1,'J');

        $this->SetY("22");
        $this->SetX("25");
        $this->Cell(0, 5, utf8_decode("SISTEMA DE PONTO"), 0, 1, 'C');


        $this->ColumnHeader();
    }

    function ColumnHeader() {

        //define a fonte a ser usada
        $this->SetFont('Arial', 'B', 10);

        //$this->SetX("7");
        //$this->Cell(0, 5, utf8_decode("Servidor:"), 0, 0, 'L');
        //$this->Cell(0,5,utf8_decode("Teste:".$_SESSION["usuario"]),0,0,'L');

        //$this->SetX("100");
        //$this->Cell(0, 5, utf8_decode("Matricula"), 0, 0, 'L');

        $this->SetY("51");
        $this->SetX("10");
        $this->Cell(0, 5, utf8_decode("Data"), 0, 0, 'L');

        $this->SetY("46");
        $this->SetX("33");
        $this->MultiCell(0, 5, utf8_decode(" Turno 01 \nInicio/Fim"), 0, 'L');

        $this->SetY("51");
        $this->SetX("55");
        $this->Cell(0, 5, utf8_decode("Total"), 0, 0, 'L');

        $this->SetY("46");
        $this->SetX("80");
        $this->MultiCell(0, 5, utf8_decode(" Turno 02\nInicio/Fim"), 0, 'L');

        $this->SetY("51");
        $this->SetX("100");
        $this->Cell(0, 5, utf8_decode("Total"), 0, 0, 'L');

        $this->SetY("46");
        $this->SetX("125");
        $this->MultiCell(0, 5, utf8_decode(" Turno 03\nInicio/Fim"), 0, 'L');

        $this->SetY("51");
        $this->SetX("145");
        $this->Cell(0, 5, utf8_decode(" - Total"), 0, 0, 'L');

//        $this->SetY("53");
//        $this->SetX("190");
//        $this->Cell(0, 5, utf8_decode("Batida 06"), 0, 0, 'L');
//
//        $this->SetY("53");
//        $this->SetX("210");
//        $this->Cell(0, 5, utf8_decode("Calc Horas 03"), 0, 0, 'L');

        $this->SetY("51");
        $this->SetX("165");
        $this->Cell(0, 5, utf8_decode("Previsto dia"), 0, 0, 'L');

        $this->SetY("51");
        $this->SetX("186");
        $this->Cell(0, 5, utf8_decode("Trabalhado"), 0, 1, 'L');
        $this->Cell(0, 0, '', 1, 1, 'L');
    }

    function ColumnDetail() {

        $this->SetX("20");
        $this->Ln(10);


        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instancia um repositorio para Carros
        $repository = new TRepository('vw_marcacao_relogioRecord');
        // cria um criterio de selecao, ordenado pelo id
        

        $criteria = new TCriteria;

        $criteria->add(new TFilter('servidor_id', '=', $_REQUEST['servidor_id']));
        $criteria->add(new TFilter('mes', '=', $_REQUEST['mes']));
        $criteria->add(new TFilter('ano', '=', $_REQUEST['ano']));
        //$criteria->add(new TFilter('matricula', '=', $_SESSION["matriculata"]));
       // $criteria->add(new TFilter('mes', '=', $$_SESSION["mes"]));
        //$criteria->add(new TFilter('ano', '=', $$_SESSION["ano"]));

        //$criteria->setProperty('order', 'databatida DESC');

        // carrega os objetos de acordo com o criterio
        $rows = $repository->load($criteria);

        $previsao = 0;
        $previsaop = 0;
        $executado = 0;
        $executadop = 0;
        $parcial = 0;
        $horatotal = 0;
        $diahoje = date("d/m/Y");
        $matricula = '';
        $nome_servidor = '';
        if ($rows) {
            $i = 0;
            // percorre os objetos retornados
            foreach ($rows as $row) {

                if ($_SESSION["usuario"] == '') {
                    //define a fonte a ser usada
                    $this->SetFont('arial', '', 15);
                    $this->SetX("15");
                    $this->Cell(0, 5, utf8_decode("Nao existem dados cadastrados para os  parametros desta consulta"), 0, 1, 'J');
                    $this->Ln();
                }

                if ($matricula != $row->matricula) {
                    $matricula = $row->matricula;
                    $this->SetY("41");
                    $this->SetX("10");
                    $this->SetFont('arial', 'B', 10);
                    $this->Cell(0, 5, utf8_decode("Matricula: " . $row->matricula), 0, 0, 'L');

                    if ($nome_servidor != $row->nome_servidor) {
                        $nome_servidor = $row->nome_servidor;
                        $this->SetX("43");
                        //  $this->Sety("30");
                        $this->SetFont('arial', 'B', 10);
                        $this->Cell(0, 5, utf8_decode("Nome: " . $row->nome_servidor . "    LOTAÇÃO: " . $row->lotacao), 0, 1, 'L');

                        $this->SetX("180");
                        $this->Cell(0, 5, $row->matricula, 0, 0, 'L');

                        $this->Ln();
                    }


//                    $this->SetX("205");
//                    $this->Cell(0, 5, utf8_decode($row->semestre), 0, 0, 'J');
//
//                    $this->SetX("210");
//                    $this->Cell(0, 5, utf8_decode($row->ano), 0, 0, 'J');

                    $this->Ln();
                }

//                $this->SetX("110");
//                $this->Cell(0, 5, utf8_decode(substr($row->matricula, 0, 35)), 0, 1, 'L');

                $this->SetX("10");
                $this->Cell(0, 5, utf8_decode(formatar_data($row->databatida)), 0, 0, 'L');

                $this->SetX("33");
                if ($row->batida01) {
                    $this->Cell(0, 5, utf8_decode(formatar_hora2($row->batida01) . "/"), 0, 0, 'L');
                } else {
                    $this->Cell(0, 5, utf8_decode($row->batida01), 0, 0, 'L');
                }

                $this->SetX("43");
                if ($row->batida02) {
                    $this->Cell(0, 5, utf8_decode(formatar_hora2($row->batida02) . "  - "), 0, 0, 'L');
                } else {
                    $this->Cell(0, 5, utf8_decode($row->batida02), 0, 0, 'L');
                }

                $this->SetX("57");
                if ($row->hora01) {
                    $this->Cell(0, 5, utf8_decode(formatar_hora($row->hora01)), 0, 0, 'L');
                } else {
                    $this->Cell(0, 5, utf8_decode($row->hora01), 0, 0, 'L');
                }

                $this->SetX("80");
                if ($row->batida03) {
                    $this->Cell(0, 5, utf8_decode(formatar_hora2($row->batida03) . "/"), 0, 0, 'L');
                } else {
                    $this->Cell(0, 5, utf8_decode($row->batida03), 0, 0, 'L');
                }

                $this->SetX("90");
                if ($row->batida04) {
                    $this->Cell(0, 5, utf8_decode(formatar_hora2($row->batida04) . "  - "), 0, 0, 'L');
                } else {
                    $this->Cell(0, 5, utf8_decode($row->batida04), 0, 0, 'L');
                }

                $this->SetX("105");
                if ($row->hora02) {
                    $this->Cell(0, 5, utf8_decode(formatar_hora($row->hora02)), 0, 0, 'L');
                } else {
                    $this->Cell(0, 5, utf8_decode($row->hora02), 0, 0, 'L');
                }

                $this->SetX("125");
                if ($row->batida05) {
                    $this->Cell(0, 5, utf8_decode(formatar_hora2($row->batida05) . "/"), 0, 0, 'L');
                } else {
                    $this->Cell(0, 5, utf8_decode($row->batida05), 0, 0, 'L');
                }

                $this->SetX("135");
                if ($row->batida06) {
                    $this->Cell(0, 5, utf8_decode(formatar_hora2($row->batida06) . "  - "), 0, 0, 'L');
                } else {
                    $this->Cell(0, 5, utf8_decode($row->batida06), 0, 0, 'L');
                }

                $this->SetX("150");
                if ($row->hora03) {
                    $this->Cell(0, 5, utf8_decode(formatar_hora($row->hora03)), 0, 0, 'L');
                } else {
                    $this->Cell(0, 5, utf8_decode($row->hora03), 0, 0, 'L');
                }

                $this->SetX("170");
                $this->Cell(0, 5, utf8_decode(formatar_hora($row->hora_jornada)), 0, 0, 'L');

                if ($diahoje > $row->databatida) {
                    $previsaop = ($previsaop + $row->minutojornada);
                    $executadop = ($executadop + $row->minutototal);
                }

                $previsao = ($previsao + $row->minutojornada);
                $executado = ($executado + $row->minutototal);

                $horatotal = ($horatotal + $row->horatotal);

                $this->SetX("187");
                $this->Cell(0, 5, utf8_decode(formatar_hora($row->horatotal)), 0, 1, 'L');
                $this->Cell(0, 0, '', 1, 1, 'L');

//                $this->SetY("230");
//                $this->SetX("10");
//                $this->MultiCell(0, 5, utf8_decode(" Turno 02\nInicio/Fim"), 0, 'L');

                $i = $i + 1;
            }
            $this->SetX("7");
            //        $this->Cell(0, 0, '', 1, 1, 'L');
            $this->SetFont('Arial', 'B', 13);
            $this->SetX("7");
            $tmphora = ((int) ($previsao / 60)) . 'h' . ($previsao % 60);
            $tmphora1 = ((int) ($executado / 60)) . 'h' . ($executado % 60);
            $tmphorap = ((int) ($previsaop / 60)) . 'h' . ($previsaop % 60);
            $tmphora1p = ((int) ($executadop / 60)) . 'h' . ($executadop % 60);
//            $parcialPrecisao = ($previsao / 60);
//            $parcialExecultado = ($previsao % 60);
//            $parcial1 = ($executado / 60);
//            $parcial2 = ($executado % 60);

//            $this->SetX("10");
//            $this->Cell(0, 5, utf8_decode($parcialPrecisao), 0, 1, 'L');
//
//            $this->SetX("15");
//            $this->Cell(0, 5, utf8_decode($horatotal), 0, 1, 'L');
//
//            $this->SetX("10");
//            $this->Cell(0, 5, utf8_decode($parcial1), 0, 1, 'L');
//
//            $this->SetX("10");
//            $this->Cell(0, 5, utf8_decode($parcial2), 0, 1, 'L');

//            $parcialt = $parcialh + $parcialm;
            if ($executado > $previsao) {
                $tmphora2 = '    Saldo:' . ((int) (($executado - $previsao) / 60)) . 'h' . (($executado - $previsao) % 60);
            } else {
                $tmphora2 = '    Débito:' . ((int) (($previsao - $executado) / 60)) . 'h' . (($previsao - $executado) % 60);
            }
            if ($executadop > $previsaop) {
                $tmphora2p = '    Saldo:' . ((int) (($executadop - $previsaop) / 60)) . 'h' . (($executadop - $previsaop) % 60);
            } else {
                $tmphora2p = '    Débito:' . ((int) (($previsaop - $executadop) / 60)) . 'h' . (($previsaop - $executadop) % 60);
            }

            $this->Cell(0, 10, (utf8_decode("Dias úteis: ") . utf8_decode(substr($i, 0, 4)) . (utf8_decode("    Previsto: ") . utf8_decode($tmphora)) . (utf8_decode("    Trabalhado: ") . utf8_decode($tmphora1)) . utf8_decode($tmphora2)), 0, 1, 'L');
            $this->Cell(0, 10, ((utf8_decode("Parcial:   Previsto: ") . utf8_decode($diahoje) . " / " . utf8_decode($tmphorap)) . (utf8_decode("    Trabalhado: ") . utf8_decode($tmphora1p)) . utf8_decode($tmphora2p)), 0, 1, 'L');
        }
        TTransaction::close();

        $this->Cell(0, 0, '', 0, 1, 'L');
        $this->Cell(0, 0, '', 0, 1, 'L');
        $this->Cell(0, 0, '', 0, 1, 'L');

        $this->Ln(10);

        //$this->Cell(0, 10, utf8_decode($nome_servidor), 0, 1, 'L');
        $this->SetX("10");
        $this->MultiCell(0, 5, utf8_decode("________________________________________\n" . substr($nome_servidor, 0, 40) . utf8_decode("\nMatricula: " . $matricula)), 0, 'C');
        //$this->MultiCell(0, 5, utf8_decode("________________________________________\n".substr($nome_servidor,0,40).utf8_decode("\nMatricula: " . $matricula). utf8_decode("________________________________________\nAssinatura do Chefe Imediado").utf8_decode("Matricula: " . $matricula)),0, 1, 'C');
        $this->Ln(10);
        $this->MultiCell(0, 5, utf8_decode("________________________________________\n".$row->servidor_lotacao."\nChefe Imediado"), 0, 'C');
        $this->SetX("10");
        $this->SetX("115");
        //  $this->MultCell(0, 5, utf8_decode("Assinatura do Chefe Imediado:\n_____________________________"), 0, 'L');
        //$this->MultiCell(0, 5, utf8_decode("_______________________________\nAssinatura do Chefe Imediado"), 0, 'C');
    }

//Page footer

    function Footer() {
        //Position at 1.5 cm from bottom
        $this->SetY(-15);
        //Arial italic 8
        $this->SetFont('Arial','I',8);
        //Page number
        //data atual
        $data=date("d/m/Y H:i:s");
        $conteudo="impresso em ".$data;
        $texto= $_SESSION['empresa_nome'];
        //imprime uma linha... largura,altura, texto,borda,quebra de linha, alinhamento
        $this->Cell(0,0,'',1,1,'L');

        //imprime uma celula... largura,altura, texto,borda,quebra de linha, alinhamento
        $this->Cell(0,5,$texto,0,0,'L');
        $this->Cell(0,5,'Pag. '.$this->PageNo().' de '.'{nb}'.' - '.$conteudo,0,0,'R');
        $this->Ln();
    }
}



$pdf = new RelatorioBatidasPontoPDF("P", "mm", "A4");

$pdf->SetTitle("Relatorio de Marcacoes do Ponto");

$pdf->SetSubject("Relatorio de Marcacoes do Ponto");

$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times', '', 12);
$pdf->ColumnDetail();
$file = "app/reports/RelatorioBatidasPontoPDF".$_SESSION['servidor_id'].".pdf";

//abrir pdf
$pdf->Output($file);
$pdf->openFile($file);
?>