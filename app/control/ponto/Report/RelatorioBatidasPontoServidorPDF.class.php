<?php

//ini_set('display_errors', 1);
//ini_set('display_startup_erros', 1);
//error_reporting(E_ALL);

use Adianti\Database\TTransaction;
use Adianti\Database\TRepository;
use Adianti\Database\TCriteria;
use Adianti\Database\TFilter;

class RelatorioBatidasPontoServidorPDF extends FPDF {

    function Header() {
        $this->Image("app/images/logo_relatorio.jpg", 8, 11, 26, 18);

        $this->SetFont('Arial', 'B', 12);
        $this->SetX("25");
        $this->Cell(0, 5, utf8_decode("GOVERNO DO ESTADO DO RIO GRANDE DO NORTE"), 0, 1, 'C');

        $this->SetFont('Arial', 'B', 10);
        $this->SetX("55");
        $this->Cell(0, 5, utf8_decode($_SESSION['empresa_nome']), 0, 1, 'J');

        $this->SetX("25");
        $this->Cell(0, 5, utf8_decode("SISTEMA DE PONTO"), 0, 1, 'C');


        $this->ColumnHeader();
    }

    function ColumnHeader() {
        
    }

    function ColumnDetail() {

        $this->SetX("20");

        TTransaction::open('pg_ceres');
        $servidor_id = isset($_REQUEST['servidor_id']) ? $_REQUEST['servidor_id'] : $_SESSION['servidor_id'];
        $servidorexplode = explode("::",$servidor_id);

        $repository2 = new TRepository('vw_ponto_resumo_mesRecord');
        $criteria2 = new TCriteria;
        $criteria2->add(new TFilter('mes', '=', $_REQUEST['mes']));
        $criteria2->add(new TFilter('ano', '=', $_REQUEST['ano']));
        $criteria2->add(new TFilter('tipo', '=', 'PARCIAL'));
        $criteria2->add(new TFilter('servidor_id', '=', $servidorexplode[0]));

        $rows2 = $repository2->load($criteria2);

        $repository = new TRepository('vw_marcacao_relogioRecord');

        $criteria = new TCriteria;
        $criteria->add(new TFilter('mes', '=', $_REQUEST['mes']));
        $criteria->add(new TFilter('ano', '=', $_REQUEST['ano']));

        $criteria->add(new TFilter('servidor_id', '=',$servidorexplode[0]));
     

        $rows = $repository->load($criteria);

        $previsao = 0;
        $previsaop = 0;
        $executado = 0;
        $executadop = 0;
        $parcial = 0;
        $horatotal = 0;
        $diahoje = date("d/m/Y");
        $matricula = '';
        $nome_servidor = '';
        $flag = true;
        if ($rows) {

            $i = 0;
            foreach ($rows as $row) {

                if ($matricula != $row->matricula) {


                    $matricula = $row->matricula;

                    $this->Ln();
                    $this->Ln();

                    $this->SetX("10");
                    $this->SetFont('arial', 'B', 10);
                    $this->Cell(0, 5, utf8_decode("Matricula: " . $row->matricula), 0, 0, 'L');

                    $this->SetX("43");
                    $this->SetFont('arial', 'B', 10);
                    $this->Cell(0, 5, utf8_decode("Nome: " . $row->nome_servidor), 0, 1, 'L');

                    $this->SetX("10");
                    $this->SetFont('arial', 'B', 10);
                    $this->Cell(0, 5, utf8_decode("LOTAÇÃO:: " . $row->lotacao), 0, 1, 'L');


                    $this->SetX("35");
                    $this->Cell(0, 5, utf8_decode("Turno 1"), 0, 0, 'L');

                    $this->SetX("72");
                    $this->Cell(0, 5, utf8_decode("Turno 2"), 0, 0, 'L');

                    $this->SetX("107");
                    $this->Cell(0, 5, utf8_decode("Turno 3"), 0, 1, 'L');

                    $this->SetFont('Arial', 'B', 10);

                    $this->SetX("10");
                    $this->Cell(0, 4, utf8_decode("Data"), 0, 0, 'L');

                    $this->SetX("33");
                    $this->Cell(0, 5, utf8_decode("Inicio/Fim"), 0, 0, 'L');

                    $this->SetX("55");
                    $this->Cell(0, 5, utf8_decode("Total"), 0, 0, 'L');

                    $this->SetX("68");
                    $this->Cell(0, 5, utf8_decode("Inicio/Fim"), 0, 'L');

                    $this->SetX("90");
                    $this->Cell(0, 5, utf8_decode("Total"), 0, 0, 'L');

                    $this->SetX("103");
                    $this->Cell(0, 5, utf8_decode("Inicio/Fim"), 0, 'L');

                    $this->SetX("125");
                    $this->Cell(0, 5, utf8_decode(" Total"), 0, 0, 'L');

                    $this->SetX("145");
                    $this->Cell(0, 5, utf8_decode("Compareceu"), 0, 0, 'L');

                    $this->SetX("178");
                    $this->Cell(0, 5, utf8_decode("Prev."), 0, 0, 'L');

                    $this->SetX("190");
                    $this->Cell(0, 5, utf8_decode("Trab."), 0, 1, 'L');
                    $this->Cell(0, 0, '', 1, 1, 'L');
                }

                $this->SetX("10");
                $this->Cell(0, 4, utf8_decode(formatar_data($row->databatida)), 0, 0, 'L');

                $this->SetX("33");
                if ($row->batida01) {
                    $this->Cell(0, 5, utf8_decode(formatar_hora2($row->batida01) . "/"), 0, 0, 'L');
                } else {
                    $this->Cell(0, 5, utf8_decode($row->batida01), 0, 0, 'L');
                }

                $this->SetX("43");
                if ($row->batida02) {
                    $this->Cell(0, 5, utf8_decode(formatar_hora2($row->batida02) . " -"), 0, 0, 'L');
                } else {
                    $this->Cell(0, 5, utf8_decode($row->batida02), 0, 0, 'L');
                }

                $this->SetX("55");
                if ($row->hora01) {
                    $this->Cell(0, 5, utf8_decode(formatar_hora($row->hora01)), 0, 0, 'L');
                } else {
                    $this->Cell(0, 5, utf8_decode($row->hora01), 0, 0, 'L');
                }

                $this->SetX("68");
                if ($row->batida03) {
                    $this->Cell(0, 5, utf8_decode(formatar_hora2($row->batida03) . "/"), 0, 0, 'L');
                } else {
                    $this->Cell(0, 5, utf8_decode($row->batida03), 0, 0, 'L');
                }

                $this->SetX("78");
                if ($row->batida04) {
                    $this->Cell(0, 5, utf8_decode(formatar_hora2($row->batida04) . " -"), 0, 0, 'L');
                } else {
                    $this->Cell(0, 5, utf8_decode($row->batida04), 0, 0, 'L');
                }

                $this->SetX("90");
                if ($row->hora02) {
                    $this->Cell(0, 5, utf8_decode(formatar_hora($row->hora02)), 0, 0, 'L');
                } else {
                    $this->Cell(0, 5, utf8_decode($row->hora02), 0, 0, 'L');
                }

                $this->SetX("103");
                if ($row->batida05) {
                    $this->Cell(0, 5, utf8_decode(formatar_hora2($row->batida05) . "/"), 0, 0, 'L');
                } else {
                    $this->Cell(0, 5, utf8_decode($row->batida05), 0, 0, 'L');
                }

                $this->SetX("113");
                if ($row->batida06) {
                    $this->Cell(0, 5, utf8_decode(formatar_hora2($row->batida06) . " -"), 0, 0, 'L');
                } else {
                    $this->Cell(0, 5, utf8_decode($row->batida06), 0, 0, 'L');
                }

                $this->SetX("125");
                if ($row->hora03) {
                    $this->Cell(0, 5, utf8_decode(formatar_hora($row->hora03)), 0, 0, 'L');
                } else {
                    $this->Cell(0, 5, utf8_decode($row->hora03), 0, 0, 'L');
                }

                $this->SetX("145");
                $this->Cell(0, 5, utf8_decode(substr($row->compareceu,0,15)), 0, 0, 'L');


                $this->SetX("178");
                $this->Cell(0, 5, utf8_decode(formatar_hora($row->hora_jornada)), 0, 0, 'L');


                if ($diahoje > $row->databatida) {
                    $previsaop = ($previsaop + $row->minutojornada);
                    $executadop = ($executadop + $row->minutototal);
                }

                $previsao = ($previsao + $row->minutojornada);
                $executado = ($executado + $row->minutototal);

                $horatotal = ($horatotal + $row->horatotal);

                $this->SetX("190");
                $this->Cell(0, 5, utf8_decode(formatar_hora($row->horatotal)), 0, 1, 'L');
                $this->Cell(0, 0, '', 1, 1, 'L');

                $i = $i + 1;

                $this->Ln();
            }

            $this->SetX("7");
            $this->SetFont('Arial', 'B', 10);
            $this->SetX("7");
            $tmphora = ((int) ($previsao / 60)) . 'h' . ($previsao % 60);
            $tmphora1 = ((int) ($executado / 60)) . 'h' . ($executado % 60);
            $tmphorap = ((int) ($previsaop / 60)) . 'h' . ($previsaop % 60);
            $tmphora1p = ((int) ($executadop / 60)) . 'h' . ($executadop % 60);

            if ($executado > $previsao) {
                $tmphora2 = '    Saldo:' . ((int) (($executado - $previsao) / 60)) . 'h' . (($executado - $previsao) % 60);
            } else {
                $tmphora2 = '    Débito:' . ((int) (($previsao - $executado) / 60)) . 'h' . (($previsao - $executado) % 60);
            }
            if ($executadop > $previsaop) {
                $tmphora2p = '    Saldo:' . ((int) (($executadop - $previsaop) / 60)) . 'h' . (($executadop - $previsaop) % 60);
            } else {
                $tmphora2p = '    Débito:' . ((int) (($previsaop - $executadop) / 60)) . 'h' . (($previsaop - $executadop) % 60);
            }

            $this->Cell(0, 10, (utf8_decode("Dias úteis: ") . utf8_decode(substr($i, 0, 4)) . (utf8_decode("   Previsto: ") . utf8_decode($tmphora)) . (utf8_decode("    Trabalhado: ") . utf8_decode($tmphora1)) . utf8_decode($tmphora2)), 0, 1, 'L');
            //$this->Cell(0, 10, ((utf8_decode("Parcial:   Previsto: ") . utf8_decode($diahoje) . " / " . utf8_decode($tmphorap)) . (utf8_decode("    Trabalhado: ") . utf8_decode($tmphora1p)) . utf8_decode($tmphora2p)), 0, 1, 'L');
         
            if(($_REQUEST['mes'] == date("m"))&&($_REQUEST['ano'] == date("Y")) ){
            foreach ($rows2 as $row2) {
              if ($row2->prevtotal < $row2->trabtotal){
                   $this->Cell(0, 10, (utf8_decode("Extrato até o dia ". date("d/m/Y")) . (utf8_decode("    Previsto: ") . utf8_decode($row2->prevtotal)) . (utf8_decode("    Trabalhado: ") . utf8_decode($row2->trabtotal)) . (utf8_decode("    Saldo: ") . utf8_decode($row2->saldo)) ), 0, 1, 'L');
              }else{
                   $this->Cell(0, 10, (utf8_decode("Extrato até o dia ". date("d/m/Y")) . (utf8_decode("   Previsto: ") . utf8_decode($row2->prevtotal)) . (utf8_decode("    Trabalhado: ") . utf8_decode($row2->trabtotal)) . (utf8_decode("    Débito: ") . utf8_decode($row2->saldo)) ), 0, 0, 'L');
              }
            }
        }

                    $this->Ln();
                    $this->Ln();

                    $this->SetX("0");
                    $this->SetFont('arial', 'B', 10);
                    $this->Cell(0, 5, utf8_decode("_______________________________________"), 0, 1, 'C');
                    //$this->Ln();
                    $this->Cell(0, 5, utf8_decode($row->nome_servidor), 0, 1, 'C');

                    $this->SetX("0");
                    $this->Cell(0, 5, utf8_decode($row->matricula), 0, 1, 'C');

                    $this->Ln();
                    $this->Ln();

                    $this->SetX("0");
                    $this->SetFont('arial', 'B', 10);
                    $this->Cell(0, 5, utf8_decode("_______________________________________"), 0, 1, 'C');
                    //$this->Ln();
                    $this->Cell(0, 5, utf8_decode("ASSINATURA DO CHEFE IMEDIATO"), 0, 1, 'C');

                    $this->SetX("0");
                    $this->Cell(0, 5, utf8_decode("MATRÍCULA:"), 0, 1, 'C');


        }
        TTransaction::close();




        $this->Cell(0, 0, '', 0, 1, 'L');
        $this->Cell(0, 0, '', 0, 1, 'L');
        $this->Cell(0, 0, '', 0, 1, 'L');
    }

    function Footer() {
        $this->SetY(-15);
        $this->SetFont('Arial', 'I', 8);
        $data = date("d/m/Y H:i:s");
        $conteudo = "impresso em " . $data;
        $texto = $_SESSION['empresa_nome'];
        $this->Cell(0, 0, '', 1, 1, 'L');

        $this->Cell(0, 5, $texto, 0, 0, 'L');
        $this->Cell(0, 5, 'Pag. ' . $this->PageNo() . ' de ' . '{nb}' . ' - ' . $conteudo, 0, 0, 'R');
        $this->Ln();
    }

}

$servidor_id = $_REQUEST['servidor_id'];
$servidorexplode = explode("::",$servidor_id);

$pdf = new RelatorioBatidasPontoServidorPDF("P", "mm", "A4");

$pdf->SetTitle("Relatorio de Marcacao do servidor");

$pdf->SetSubject("Relatorio de Marcacao do servidor");

$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times', '', 12);
$pdf->ColumnDetail();
$file = "app/reports/RelatorioBatidasPontoServidorPDF" . $servidorexplode[0] . ".pdf";

$pdf->Output($file);
$pdf->openFile($file);
?>