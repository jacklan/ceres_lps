<?php

class HorarioEspecialForm extends TPage
{
    private $form;     // formulario de cadastro

    /*
     * metodo construtor
     * Cria a pagina do formulario
     */
    public function __construct()
    {
        parent::__construct();

        // instancia um formulario
         $this->form = new TQuickForm;
        $this->form->class = 'form_HorarioEspecial';
        $this->form->setFormTitle('<font color="red" size="3" face="Arial"><b>Formul&aacute;rio de Horário Especial </b></font>');
        

        $titulo = new TLabel('<div style="position:floatval; width: 200px;"> <b>* Campos obrigat&oacute;rios</b></div>');
        $titulo->setFontFace('Arial');
        $titulo->setFontColor('red');
        $titulo->setFontSize(10);

        // cria os campos do formulario
        $codigo = new THidden('id');
        $codigo->setEditable(false);
        $nome = new TEntry('nome');
        $unidadeoperativa_id = new TCombo('unidadeoperativa_id');
        $unidadeoperativa_id->setValue($_SESSION['unidadeoperativa_id']);
        $datahorario = new TDate('datahorario');
        $horarioinicio = new TEntry('horarioinicio');
        $horariofim = new TEntry('horariofim');

        $usuarioalteracao = new THidden('usuarioalteracao');
        $dataalteracao = new THidden('dataalteracao');

       
        TTransaction::open('pg_ceres');

        $repository = new TRepository('UnidadeOperativaRecord');
        $criteria = new TCriteria;
        $criteria->setProperty('order', 'nome');
        $criteria->add( new TFilter( 'empresa_id', '=', $_SESSION['empresa_id'] ) );
        $cadastros = $repository->load($criteria);
        //adiciona os objetos no combo
        foreach ($cadastros as $object) {
            $items[$object->id] = $object->nome;

        }
        //TTransaction::close();
        $unidadeoperativa_id->addItems($items);

        TTransaction::close();

        //define os campos obrigatorios
        $nome->setProperty('required', 'required');
        $datahorario->setProperty('required', 'required');
        $horarioinicio->setProperty('required', 'required');
        $horariofim->setProperty('required', 'required');
        
        $nome->placeholder = "Nome"; 
        $horarioinicio->placeholder = "07:00";  
        $horariofim->placeholder = "10:00"; 
        $datahorario->placeholder = "10/10/2016";
        
        // Adicionando os campos ao formulario

        $this->form->addQuickField(null, $codigo, 10);
        $this->form->addQuickField(null, $usuarioalteracao, 10);
        $this->form->addQuickField(null, $dataalteracao, 10);
        $this->form->addQuickField('Unidade Operativa <font color=red><b>*</b></font>', $unidadeoperativa_id, 30);
        $this->form->addQuickField('Nome <font color=red><b>*</b></font>', $nome, 30);        
        $this->form->addQuickField('Data Horário Esp. <font color=red><b>*</b></font>', $datahorario, 30);
        $this->form->addQuickField('Horário Início <font color=red><b>*</b></font>', $horarioinicio, 30);
        $this->form->addQuickField('Horário Final <font color=red><b>*</b></font>', $horariofim, 30);
        
        $horarioinicio->setMask('99:99');
        $horariofim->setMask('99:99');
        
        $this->form->addQuickField(null, $titulo, 50);
        
        $nome->addValidation('Nome', new TRequiredValidator);
        $unidadeoperativa_id->addValidation('Unidade Operativa', new TRequiredValidator);
        $datahorario->addValidation('Data Horario Esp.', new TRequiredValidator);
        $horarioinicio->addValidation('Horario Inicio', new TRequiredValidator);
        $horariofim->addValidation('Horario Fim', new TRequiredValidator);
        
              
        // cria um botao de acao
        $this->form->addQuickAction('Salvar', new TAction(array($this, 'onSave')), 'ico_save.png')->class = 'btn btn-info btnleft';
        $this->form->addQuickAction('Voltar', new TAction(array('HorarioEspecialList', 'onReload')), 'ico_datagrid.gif');

        // adiciona a tabela a pagina
        parent::add($this->form);
}


    /*
     * metodo onSave()
     * Executada quando o usuario clicar no botao salvar do formulario
     */
    function onSave()
    {
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        $msg = '';
        $icone = 'info';

        // obtem os dados no formulario em um objeto Record da Classe
        $cadastro = $this->form->getData('HorarioEspecialRecord');
        
        $cadastro->usuarioalteracao = $_SESSION['usuario'];
        $cadastro->dataalteracao = date("d/m/Y H:i:s");
        $cadastro->empresa_id = $_SESSION['empresa_id'];

        if ( $cadastro->horarioinicio < $cadastro->horariofim  ) {

            try {
            
            //Validade
            $this->form->validate();

            // armazena o objeto
            $cadastro->store();
                
            TTransaction::close();

            // exibe um dialogo ao usuario
            new TMessage("info", "Registro salvo com sucesso!");
            TApplication::gotoPage('HorarioEspecialList', 'onReload');
                      
            }
            catch (Exception $e) // em caso de exce??o
            {
                // exibe a mensagem gerada pela exce??o
                new TMessage('error', $e->getMessage());
                // desfaz todas altera??es no banco de dados
                TTransaction::rollback();
            }

        }else{
            new TMessage('error', 'Hora Inicio deve ser menor que hora Fim'  );
        }     
        

    }

    function onEdit($param)
    {
        try 
        {
            if( isset( $param['key'] ) )
            {
        // obtem o parametro $key
         $key=$param['key'];
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $cadastro = new HorarioEspecialRecord($key);

        $cadastro->datahorario = TDate::date2br($cadastro->datahorario);
        $cadastro->horarioinicio = substr( $cadastro->horarioinicio, 0, 5 );
        $cadastro->horariofim = substr( $cadastro->horariofim, 0, 5 );

        // lanca os dados no formulario
        $this->form->setData($cadastro);

        // finaliza a transacao
        TTransaction::close();
    }
    } catch( Exception $e )
        {
            
            // in case of exception
            // shows the exception error message
            new TMessage('error', '<b>Error</b> ' . $e->getMessage());
            // undo all pending operations
            TTransaction::rollback();
        }
    }
}

?>