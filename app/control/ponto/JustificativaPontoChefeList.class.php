<?php
//ini_set('display_errors', 1);
//ini_set('display_startup_erros', 1);
//error_reporting(E_ALL);
include_once 'app/lib/funcdate.php';
//teste

use Adianti\Database\TFilter1;
use Adianti\Widget\Datagrid\TDatagridTables;

class JustificativaPontoChefeList extends TPage
{
	
    private $form;
    private $datagrid;

    public function __construct(){
        parent::__construct();

        $this->form = new TForm('form_JustificativaPontoChefe');

        $panel = new TPanelForm(1000, 200);
        $this->form->add($panel);

        $titulo = new TLabel('Justificativas');
        $titulo->setFontFace('Arial');
        $titulo->setFontColor('red');
        $titulo->setFontSize(12);

        $panel->put($titulo,$panel->getColuna(),$panel->getLinha());

        $this->datagrid = new TDataGridTables;

        $dgservidor = new TDataGridColumn('nome_servidor',  'Servidor',  'left', 700);
        $dgdatabatida = new TDataGridColumn('databatida',    'Data',    'left', 750);
        $dghorajornada = new TDataGridColumn('hora_jornada',    'Jornada',    'left', 650);
        $dghoratotal = new TDataGridColumn('horatotal',    'Trabalhado',    'left', 450);
        $dgsituacao = new TDataGridColumn('situacaojustificativa', 'Situa&ccedil;&atilde;o', 'left', 350);
        $dgdataautorizacao = new TDataGridColumn('dataautorizacao', 'Data Autoriza&ccedil;&atilde;o', 'left', 250);
        $dgpdf = new TDataGridColumn('justificativapdf',    'Justificativa PDF',    'left', 500);

        $dgdatabatida->setTransformer('formatar_data');
        $dgdataautorizacao->setTransformer('formatar_data');
        
        $this->datagrid->addColumn($dgservidor);
        $this->datagrid->addColumn($dgdatabatida);
        $this->datagrid->addColumn($dghorajornada);
        $this->datagrid->addColumn($dghoratotal);
        $this->datagrid->addColumn($dgsituacao);
        $this->datagrid->addColumn($dgdataautorizacao);
        $this->datagrid->addColumn($dgpdf);

        $action1 = new TDataGridAction(array('JustificativaPontoChefeForm', 'onEdit'));
        $action1->setLabel('Alterar');
        $action1->setImage('ico_edit.png');
        $action1->setField('id');
        $action1->setFk('id');
        
        $this->datagrid->addAction($action1);

        $this->datagrid->createModel();

        $panel = new TPanelForm(1000, 500);
        $panel->put($this->form, 0, 0);
        $panel->put($this->datagrid, 0, 50);

        parent::add($panel);
    }

    function onReload(){
        TTransaction::open('pg_ceres');

        $repository = new TRepository('vw_justificativa_acompanhamentoRecord');

        $criteria = new TCriteria;

        $criteria->add(new TFilter('chefe_id', '=', $_SESSION['servidor_id']));
        $criteria->add(new TFilter('situacaojustificativa', '=', 'AGUARDANDO'));
        //$criteria->add(new TFilter('servidor_id', '=', '1198'));
        //filtra pelo campo selecionado pelo jornada 
        //$criteria->add($expression)
        //$criteria->add(new TFilter('horatotal', '<', 'horajornada'));
        //filtra pelo campo selecionado pelo 
        //$criteria->add(new TFilter('justificativa', '=', null));
        

        $criteria->setProperty('order', 'databatida desc');
        $cadastros = $repository->load($criteria);

        $this->datagrid->clear();
        if ($cadastros){
            foreach ($cadastros as $cadastro){
                $this->datagrid->addItem($cadastro);
            }
        }

        TTransaction::close();
        $this->loaded = true;
    }

    function show(){
        $this->onReload();
        parent::show();

    }
}
?>