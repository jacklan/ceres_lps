<?php


use Adianti\Database\TFilter1;
use Adianti\Widget\Datagrid\TDatagridTables;

class ConfirmarTrocaEscalaServidor extends TPage
{
    private $form;
    private $datagrid;

    public function __construct()
    {

        parent::__construct();

        $this->form = new TQuickForm('list_confirmar_troca');
        //$this->form = new BootstrapFormWrapper(new TQuickForm('list_confirmar_troca'));


        //DATAGRID ------------------------------------------------------------------------------------------
        $this->datagrid = new TDatagridTables;

        $dgid = new TDataGridColumn('id', '', 'left', 200);
        $dgnome_servidor = new TDataGridColumn('nome_servidororiginal', 'Servidor Solicitante', 'left', 300);
        $dgdatabatida = new TDataGridColumn('nome_servidordestino', 'Servidor Destino ', 'left', 300);
        $dghora_jornada = new TDataGridColumn('data_original', 'Data Origem', 'left', 200);
        $dghoratotal = new TDataGridColumn('data_destino', 'Data Destino', 'left', 200);
        $dgsituacaojustificativa = new TDataGridColumn('situacao', 'Situação', 'left', 200);
        $dgdataautorizacao = new TDataGridColumn('dataautorizacao', 'Data Autorização', 'left', 200);

        //$this->datagrid->addColumn($dgid);
        $this->datagrid->addColumn($dgnome_servidor);
        $this->datagrid->addColumn($dgdatabatida);
        $this->datagrid->addColumn($dghora_jornada);
        $this->datagrid->addColumn($dghoratotal);
        $this->datagrid->addColumn($dgsituacaojustificativa);
        $this->datagrid->addColumn($dgdataautorizacao);

        $actionSave = new TDataGridAction(array($this, 'onSave'));
        $actionSave->setLabel('Confirmar');
        $actionSave->setImage( "fa:fw fa-check-circle green fa-lg" );
        $actionSave->setField('id');

        $actionNegar = new TDataGridAction(array($this, 'onNegar'));
        $actionNegar->setLabel('Negar');
        $actionNegar->setImage( "fa:fw fa-times-circle red fa-lg" );
        $actionNegar->setField('id');


        $this->datagrid->addAction($actionSave);
        $this->datagrid->addAction($actionNegar);

        $this->datagrid->createModel();

        //FIM DATAGRID -----------------------------------------------------------------------------------------

        $container = new TVBox();
        $container->style = "width: 100%";
        $container->add( TPanelGroup::pack( 'Troca de Escalas à Serem Confirmadas', $this->form ) );
        $container->add( TPanelGroup::pack( NULL, $this->datagrid ) );

        parent::add( $container );

    }

    public function onReload( $param = NULL )
    {

        try {

            TTransaction::open('pg_ceres');

            $repository = new TRepository('ServidorEscalaTrocaRecord');

            $criteria = new TCriteria();

            $criteria->setProperty('order', 'id DESC');
            $criteria->add(new TFilter('situacao', '=', 'AGUARDANDO'), TExpression::OR_OPERATOR);

            $objects = $repository->load($criteria);

            $this->datagrid->clear();




            if ( !empty( $objects ) ) {
                foreach ( $objects as $object ) {
                    $object->dataautorizacao = TDate::date2br($object->dataautorizacao);
                    $this->datagrid->addItem( $object );
                }

            }

            $criteria->resetProperties();


            TTransaction::close();

        } catch ( Exception $ex ) {

            TTransaction::rollback();

            new TMessage( "error",  $ex->getMessage()  );

        }

    }

    public function onSave( $param )
    {
        try
        {
            if( isset( $param['key'] ) ) {

                TTransaction::open('pg_ceres');

                $object = new ServidorEscalaTrocaRecord;

                $object->id = $param['key'];
                $object->dataautorizacao = date("d/m/Y H:i:s");
                $object->servidorautorizacao_id = TSession::getValue('vinculo_id');
                $object->situacao = 'DEFERIDO';


                //var_dump($object);
                //exit();

                $object->store();

                TTransaction::close();

                new TMessage("info", "Registro salvo com sucesso!");
                TApplication::gotoPage('ConfirmarTrocaEscalaServidor', 'onReload');

            }
        }catch( Exception $e )
        {
            new TMessage('error', $e->getMessage());

            TTransaction::rollback();

        }
    }

    public function onNegar( $param )
    {
        try
        {
            if( isset( $param['key'] ) ) {

                TTransaction::open('pg_ceres');

                $object = new ServidorEscalaTrocaRecord;

                $object->id = $param['key'];
                //$object->dataautorizacao = date("d/m/Y H:i:s");
                $object->servidorautorizacao_id = TSession::getValue('vinculo_id');
                $object->situacao = 'INDEFERIDO';


                //var_dump($object);
                //exit();

                $object->store();

                TTransaction::close();

                new TMessage("info", "Registro salvo com sucesso!");
                TApplication::gotoPage('ConfirmarTrocaEscalaServidor', 'onReload');

            }
        }catch( Exception $e )
        {
            new TMessage('error', $e->getMessage());

            TTransaction::rollback();

        }
    }

    public function show()
    {

        $this->onReload();

        parent::show();

    }
}