<?php

ini_set('display_errors', 1);
ini_set('display_startup_erros', 1);
error_reporting(E_ALL);

use Adianti\Database\TFilter1;
use Adianti\Widget\Datagrid\TDatagridTables;


class TrocarEscalaList extends TPage
{

    private $form;
    private $datagrid;

    public function __construct()
    {
        parent::__construct();

        $this->form =  new BootstrapFormWrapper(new TQuickForm('trocar_escala'));

        // instancia uma tabela
        $panel = new TPanelForm(900, 100);

        // adiciona a tabela ao formulario
        $this->form->add($panel);

        // cria um r�tulo para o t�tulo
        $titulo = new TLabel('Listagem de Escala');
        $titulo->setFontFace('Arial');
        $titulo->setFontColor('red');
        $titulo->setFontSize(12);

        // adiciona o campo Titulo
        $panel->put($titulo, $panel->getColuna(), $panel->getLinha());

        $this->datagrid = new TDatagridTables;

        // instancia as colunas da DataGrid
        $dgescala = new TDataGridColumn('nome_escala', 'Escala', 'left', 1300);
        $dgdata = new TDataGridColumn('datainicio', 'Data', 'left', 1300);
        $dghorainicio = new TDataGridColumn('datainicio2', 'Hora Início', 'left', 1300);
        $dghorafim = new TDataGridColumn('datafim', 'Hora Fim', 'left', 1300);

        // adiciona as colunas a DataGrid

        $this->datagrid->addColumn($dgescala);
        $this->datagrid->addColumn($dgdata);
        $this->datagrid->addColumn($dghorainicio);
        $this->datagrid->addColumn($dghorafim);

        $this->datagrid->disableDefaultClick();

        $actionEdit = new TDataGridAction(array('TrocarEscalaDetalhe', 'onEdit'));
        $actionEdit->setLabel('Editar');
        $actionEdit->setImage( "fa:pencil-square-o blue fa-lg" );
        $actionEdit->setField('id');

        $this->datagrid->addAction($actionEdit);

        $this->datagrid->createModel();

        // monta a paina atraves de uma tabela
        $panel = new TPanelForm(700, 500);
        $panel->put($this->form, 0, 0);
        $panel->put($this->datagrid, 150, 115);

        // adiciona a tabela a pagina
        parent::add($panel);

    }

    public function onReload( $param = NULL )
    {

        try {

            TTransaction::open('pg_ceres');

            $repository = new TRepository('ServidorEscalaRecord');

            $criteria = new TCriteria();

            $criteria->add(new TFilter('servidor_id', '=', $_SESSION['servidor_id']));

            $objects = $repository->load( $criteria, FALSE );

            $this->datagrid->clear();

            if ( !empty( $objects ) ) {
                foreach ( $objects as $object ) {
                    $datainicioauxiliar = $object->datainicio;
                    $object->datainicio = TDate::date2br($object->datainicio);
                    $object->datainicio2 = substr($datainicioauxiliar,11,5);
                    $object->datafim = substr($object->datafim,11,5);
                    $this->datagrid->addItem( $object );
                }
            }

            $criteria->resetProperties();

            TTransaction::close();

        } catch ( Exception $ex ) {

            TTransaction::rollback();

            new TMessage( "error",  $ex->getMessage()  );

        }

    }

    public function show()
    {

        $this->onReload();

        parent::show();

    }

}