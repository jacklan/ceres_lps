<?php 
//ini_set('display_errors', 1);
//ini_set('display_startup_erros', 1);
//error_reporting(E_ALL);

include_once 'app/lib/funcdate.php';

use Adianti\Widget\Datagrid\TDatagridTables;

class MarcacaoRelogioDetalhe extends TPage{

    private $form; 
    private $datagrid;

    public function __construct(){
        parent::__construct();

        $this->form = new TQuickForm('form_mercacao_relogio');
        $this->form->style = 'width: 40%';
        $this->form->setFormTitle('<font color="red" size="3px" face="Arial"><b>Marca&ccedil;&atilde;o Relogio</b></font>');

        $criteria_servidor = new TCriteria;
        $criteria_servidor->add(new TFilter('situacao', '=', 'EM ATIVIDADE'), TExpression::OR_OPERATOR);
        $criteria_servidor->add(new TFilter('situacao', '=', 'A DISPOSICAO'), TExpression::OR_OPERATOR);

        $criteria3 = new TCriteria;
        if ($_SESSION['empresa_id'] == 1) {
            $criteria3->add(new TFilter('empresa_id', '=', $_SESSION['empresa_id']));
        }
        $criteria3->add($criteria_servidor);
        /* ---------- fim criacao criterio de selecao ---------- */

        /* --------- montar campo --------- */
        $servidor_id = new TDBMultiSearch('servidor_id', 'pg_ceres', 'vw_servidor_multisearchRecord', 'servidor_id', 'nome_matricula_servidor', 'nome_matricula_servidor', $criteria3);
        $servidor_id->setProperty('placeholder', 'Nome ou Matricula ou CPF');
        $servidor_id->setMinLength(1); 
        $servidor_id->setMaxSize(1);
            

        $mes = new TCombo('mes'); 
        $ano = new TCombo('ano');

        $items = array();
        $items['01'] = 'JANEIRO';
        $items['02'] = 'FEVEREIRO';
        $items['03'] = 'MAR&Ccedil;O';
        $items['04'] = 'ABRIL';
        $items['05'] = 'MAIO';
        $items['06'] = 'JUNHO';
        $items['07'] = 'JULHO';
        $items['08'] = 'AGOSTO';
        $items['09'] = 'SETEMBRO';
        $items['10'] = 'OUTUBRO';
        $items['11'] = 'NOVEMBRO';
        $items['12'] = 'DEZEMBRO';

        $mes->addItems($items);
        $mes->setValue(date('m'));

        $ano->addItems(retornaAnosemZero());
        $ano->setValue(date('Y'));


        $servidor_id->addValidation('Servidor', new TRequiredValidator);
        $mes->addValidation('M&ecirc;s', new TRequiredValidator);
        $ano->addValidation('Ano', new TRequiredValidator); 

        $obs = new TLabel('<font color=red><b>Pesquisar nome do Servidor com "CAPS LOCK" Habilitado</b></font>');

        $this->form->addQuickField('Servidor <font color=red><b>*</b></font>', $servidor_id, 250);
        $this->form->addQuickField('M&ecirc;s <font color=red><b>*</b></font>', $mes, 70);
        $this->form->addQuickField('Ano <font color=red><b>*</b></font>', $ano, 70);
        $this->form->addQuickField('<font color=red><b>OBS:</b></font>',$obs, 70);

        $this->form->addQuickAction('Buscar', new TAction(array($this, 'onSearch')), 'fa:search');
        $this->form->addQuickAction('Gerar Relat&oacute;rio', new TAction(array($this, 'onGenerate')), 'fa:file-pdf-o');

        $this->datagrid = new TDataGridTables;

        $dgdatabatida = new TDataGridColumn('databatida', 'Data Batida', 'left', 200);
        $dgbatida01   = new TDataGridColumn('batida01', 'Entrada 01', 'left', 80);
        $dgbatida02   = new TDataGridColumn('batida02', 'Saida 01', 'left', 80);
        $dghoras01    = new TDataGridColumn('hora01', 'Calc Horas 01', 'left', 80);
        $dgbatida03   = new TDataGridColumn('batida03', 'Entrada 02', 'left', 80);
        $dgbatida04   = new TDataGridColumn('batida04', 'Saida 02', 'left', 80);
        $dghoras02    = new TDataGridColumn('hora02', 'Calc Horas 02', 'left', 80);
        $dgbatida05   = new TDataGridColumn('batida05', 'Entrada 03', 'left', 80);
        $dgbatida06   = new TDataGridColumn('batida06', 'Saida 03', 'left', 80);
        $dghoras03    = new TDataGridColumn('hora03', 'Calc Horas 03', 'left', 80);
        $dgprevisto   = new TDataGridColumn('hora_jornada', 'Previsto', 'left', 80);
        $dgtrabalhado = new TDataGridColumn('horatotal', 'Trabalhado', 'left', 80);

        $this->datagrid->addColumn($dgdatabatida);
        $this->datagrid->addColumn($dgbatida01);
        $this->datagrid->addColumn($dgbatida02);
        $this->datagrid->addColumn($dghoras01);
        $this->datagrid->addColumn($dgbatida03);
        $this->datagrid->addColumn($dgbatida04);
        $this->datagrid->addColumn($dghoras02);
        $this->datagrid->addColumn($dgbatida05);
        $this->datagrid->addColumn($dgbatida06);
        $this->datagrid->addColumn($dghoras03);
        $this->datagrid->addColumn($dgprevisto);
        $this->datagrid->addColumn($dgtrabalhado);

        $dgdatabatida->setTransformer('formatar_data');
        $dgbatida01  ->setTransformer('formatar_hora2');
        $dgbatida02  ->setTransformer('formatar_hora2');
        $dgbatida03  ->setTransformer('formatar_hora2');
        $dgbatida04  ->setTransformer('formatar_hora2');
        $dgbatida05  ->setTransformer('formatar_hora2');
        $dgbatida06  ->setTransformer('formatar_hora2');
        $dgprevisto  ->setTransformer('formatar_hora');
        $dghoras01   ->setTransformer('formatar_hora');
        $dghoras02   ->setTransformer('formatar_hora');
        $dghoras03   ->setTransformer('formatar_hora');
        $dgtrabalhado->setTransformer('formatar_hora');

        $this->datagrid->disableDefaultClick();

        $this->datagrid->createModel();

        $panel = new TPanelForm(700, 500);
        $panel->put($this->form, 0, 0);
        $panel->put($this->datagrid, 150, 115);

        parent::add($panel);

    }

    public function onSearch(){

        try{

            TTransaction::open('pg_ceres');
            $this->form->validate();

            $repository = new TRepository('vw_marcacao_relogioRecord');
            $servidorTemp = $this->form->getFieldData('servidor_id');
            $servidorexplode = explode("::",$servidorTemp);
     

            $mesTemp = $this->form->getFieldData('mes');
            $anoTemp = $this->form->getFieldData('ano');

            $criteria = new TCriteria;
            $criteria->add(new TFilter('servidor_id', '=', $servidorexplode[0]));
            

            if($anoTemp){
                if($anoTemp != 'TODOS'){
                    $criteria->add(new TFilter('ano', '=', $anoTemp));
                }
            }
            
            if($mesTemp){
                if($mesTemp != 'TODOS'){
                    $criteria->add(new TFilter('mes', '=', $mesTemp));
                }
            }   

            $criteria->setProperty('order', 'servidor_id');

            $objects = $repository->load($criteria);
            $this->datagrid->clear();
            
            if ($objects){

                foreach ($objects as $object){
                    $this->datagrid->addItem($object);
                }
            }

            TTransaction::close();
        }
        catch (Exception $e){
            new TMessage('erro', $e->getMessage());
            TTransaction::rollback();
        }
    }


    function onGenerate(){

        try{
            $this->form->validate();
            
          //  new RelatorioBatidasPontoPDF();
            new RelatorioBatidasPontoServidorPDF();
            
        }catch( Exception $e ){
            new TMessage( 'error', $e->getMessage() );
            
        }
    }
}