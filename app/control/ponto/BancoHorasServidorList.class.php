<?php

include_once 'app/lib/funcdate.php';
use Adianti\Widget\Datagrid\TDatagridTables;

class BancoHorasServidorList extends TPage{

    private $form;
    private $datagrid;

    public function __construct(){
        parent::__construct();

        $this->form = new TQuickForm('form_banco_horas_servidorlist');
        $this->form->style = 'width: 40%';
        $this->form->setFormTitle('<font color="red" size="3px" face="Arial"><b>Banco de Horas</b></font>');

        $codigo = new THidden('id');
        $mes = new TCombo('mes');
        $ano = new TCombo('ano');
        $saldo = new TCombo('saldo');


        $items = array();
        $items['00'] = 'TODOS';
        $items['01'] = 'JANEIRO';
        $items['02'] = 'FEVEREIRO';
        $items['03'] = 'MAR&Ccedil;O';
        $items['04'] = 'ABRIL';
        $items['05'] = 'MAIO';
        $items['06'] = 'JUNHO';
        $items['07'] = 'JULHO';
        $items['08'] = 'AGOSTO';
        $items['09'] = 'SETEMBRO';
        $items['10'] = 'OUTUBRO';
        $items['11'] = 'NOVEMBRO';
        $items['12'] = 'DEZEMBRO';

        $items2 = array();
        $items2['POSITIVO'] = 'POSITIVO';
        $items2['NEGATIVO'] = 'NEGATIVO';
        $items2['ZERADO'] = 'ZERADO';

        $mes->addItems($items);
        $mes->setValue(date('m'));

        $ano->addItems(retornaAnosemZero());
        $ano->setValue(date('Y'));

        $saldo->addItems($items2);


        TTransaction::open('pg_ceres');
        $servidor_id = new THidden('servidor_id');
        $servidor_id->setValue($_SESSION['servidor_id']);
        $nomeservidor = new ServidorRecord($_SESSION['servidor_id']);
        TTransaction::close();

        $ano->addValidation('Ano', new TRequiredValidator);

        $this->form->addQuickField(null, $codigo, 10);
        $this->form->addQuickField(null, $servidor_id, 10);
        $this->form->addQuickField('Servidor: ', new TLabel($nomeservidor->nome), 300);
        $this->form->addQuickField('Mês', $mes, 90);
        $this->form->addQuickField('Ano <font color=red><b>*</b></font>', $ano, 70);
        $this->form->addQuickField('Saldo de Horas', $saldo, 70);


        $this->form->addQuickAction('Buscar', new TAction(array($this, 'onSearch')), 'fa:search');
        $this->form->addQuickAction('Gerar Relat&oacute;rio', new TAction(array($this, 'onGenerate')), 'fa:file-pdf-o');

        $this->datagrid = new TDataGridTables;

        $dgminutoprev = new TDataGridColumn('minutosprevisto', 'Minutos Prev.', 'left');
        $dgminutotrab   = new TDataGridColumn('minutostrabalhado', 'Minutos Trab.', 'left');
        $dgtrabalhoprev   = new TDataGridColumn('prevtotal', 'Horas Prev.', 'left');
        $dgtrabalhototal   = new TDataGridColumn('trabtotal', 'Horas Trab.', 'left');
        $dgsaldo    = new TDataGridColumn('saldo', 'Saldo', 'left');
        $dgmes    = new TDataGridColumn('mes', 'Mês', 'left');
        $dgano    = new TDataGridColumn('ano', 'Ano', 'left');
        $dgtipo   = new TDataGridColumn('tipo', 'Tipo', 'left');


        $this->datagrid->addColumn($dgmes);
        $this->datagrid->addColumn($dgano);
        $this->datagrid->addColumn($dgminutoprev);
        $this->datagrid->addColumn($dgminutotrab);
        $this->datagrid->addColumn($dgtrabalhoprev);
        $this->datagrid->addColumn($dgtrabalhototal);
        $this->datagrid->addColumn($dgsaldo);
        $this->datagrid->addColumn($dgtipo);


        $this->datagrid->disableDefaultClick();

        $this->datagrid->createModel();

        $panel = new TPanelForm(700, 500);
        $panel->put($this->form, 0, 0);
        $panel->put($this->datagrid, 150, 115);

        parent::add($panel);

    }

    public function onSearch(){

        try{

            $mesTemp = $this->form->getFieldData('mes');
            $anoTemp = $this->form->getFieldData('ano');
            $saldoTemp = $this->form->getFieldData('saldo');


            $this->form->validate();


            TTransaction::open('pg_ceres');

            $repository = new TRepository('VwServidorBancoHoras');



            $criteria = new TCriteria;
            $criteria->add(new TFilter('servidor_id', '=', $_SESSION['servidor_id']));
            $criteria->add(new TFilter('ano', '=', $anoTemp));

            if($mesTemp){
                if($mesTemp != '00'){
                    $criteria->add(new TFilter('mes', '=', $mesTemp));
                }
            }

            if ($saldoTemp != null) {

                switch ($saldoTemp) {

                    case 'POSITIVO':
                        $criteria->add(new TFilter('saldo', '>', '00:00:00'));
                        break;

                    case 'NEGATIVO':
                        $criteria->add(new TFilter('saldo', '<', '00:00:00'));
                        break;

                    case 'ZERADO':
                        $criteria->add(new TFilter('saldo', '=', '00:00:00'));
                        break;

                }
            }

            $criteria->setProperty('order ASC', 'mes');

            $objects = $repository->load($criteria);
            $this->datagrid->clear();

            if ($objects){

                foreach ($objects as $object){
                    $object->mes = retornaMes($object->mes);
                    $this->datagrid->addItem($object);
                }
            }

            TTransaction::close();
        }
        catch (Exception $e){
            new TMessage('erro', $e->getMessage());
            TTransaction::rollback();
        }
    }


    function onGenerate(){

        try{
            $this->form->validate();

            new RelatorioBancoHorasServidorPDF();

            $this->onSearch();

        }catch( Exception $e ){
            new TMessage( 'error', $e->getMessage() );

        }
    }
}