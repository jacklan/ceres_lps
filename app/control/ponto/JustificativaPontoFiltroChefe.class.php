<?php

include_once 'app/lib/funcdate.php';

use Adianti\Widget\Datagrid\TDatagridTables;

class JustificativaPontoFiltroChefe extends TPage
{
    private $form;
    private $datagrid;

    public function __construct() {
        parent::__construct();

        $this->form = new TQuickForm('form_ponto_justificativaponto_filtrolist');

        $this->form->style = 'width: 40%';
        $this->form->setFormTitle('<font color="red" size="3px" face="Arial"><b>Documento Justificativa</b></font>');

        $criteria_servidor = new TCriteria;
        $criteria_servidor->add(new TFilter('situacao', '=', 'EM ATIVIDADE'), TExpression::OR_OPERATOR);
        $criteria_servidor->add(new TFilter('situacao', '=', 'A DISPOSICAO'), TExpression::OR_OPERATOR);

        $criteria3 = new TCriteria;
        if ($_SESSION['empresa_id'] == 1) {
            $criteria3->add(new TFilter('empresa_id', '=', $_SESSION['empresa_id']));
        }
        $criteria3->add($criteria_servidor);
        /* ---------- fim criacao criterio de selecao ---------- */

        /* --------- montar campo --------- */
        $servidor_id = new TDBMultiSearch('servidor_id', 'pg_ceres', 'vw_servidor_multisearchRecord', 'servidor_id', 'nome_matricula_servidor', 'nome_matricula_servidor', $criteria3);
        $servidor_id->setProperty('placeholder', 'Nome ou Matricula ou CPF');
        $servidor_id->setMinLength(1); 
        $servidor_id->setMaxSize(1);

        $situacao = new TCombo('situacaojustificativa');
        $mes = new TCombo('mes');
        $ano = new TCombo('ano');

        $items = array();
        $items['01'] = 'JANEIRO';
        $items['02'] = 'FEVEREIRO';
        $items['03'] = 'MAR&Ccedil;O';
        $items['04'] = 'ABRIL';
        $items['05'] = 'MAIO';
        $items['06'] = 'JUNHO';
        $items['07'] = 'JULHO';
        $items['08'] = 'AGOSTO';
        $items['09'] = 'SETEMBRO';
        $items['10'] = 'OUTUBRO';
        $items['11'] = 'NOVEMBRO';
        $items['12'] = 'DEZEMBRO';

        $items2 = array();
        $items2['DEFERIDO'] = 'DEFERIDO';
        $items2['INDEFERIDO'] = 'INDEFERIDO';
        $items2['AGUARDANDO'] = 'AGUARDANDO';

        $situacao->addItems($items2);

        $mes->addItems($items);
        $mes->setValue(date('m'));

        $ano->addItems(retornaAnosemZero());
        $ano->setValue(date('Y'));
        $obs = new TLabel('<font color=red><b>Pesquisar nome do Servidor com "CAPS LOCK" Habilitado</b></font>');


        $this->form->addQuickField('Servidor <font color=red><b>*</b></font>', $servidor_id,250);
        $this->form->addQuickField('Situação', $situacao, 70);
        $this->form->addQuickField('M&ecirc;s', $mes, 70);
        $this->form->addQuickField('Ano', $ano, 70);
        $this->form->addQuickField('<font color=red><b>OBS:</b></font>',$obs, 70);

        $servidor_id->addValidation('Servidor', new TRequiredValidator);

        $this->form->addQuickAction('Buscar', new TAction(array($this, 'onSearch')), 'fa:search');



 /*       TTransaction::open('pg_ceres');
        $nome_servidor = new TLabel( (new ServidorRecord($_SESSION['servidor_id']))->nome);
        TTransaction::close();
        $nome_servidor->setFontFace('Arial');
//        $nome_servidor->setFontColor('blue');
        $nome_servidor->setFontSize(10);

        $panel->put($titulo, $panel->getColuna(), $panel->getLinha());
        $panel->put($nome_servidor, $panel->getColuna(), $panel->getLinha());*/

        $this->datagrid = new TDatagridTables;

        //$dgcodigo = new TDataGridColumn('id',  'C&oacute;digo',  'left', 1000);
        // $dgservidor = new TDataGridColumn('nome_servidor',  'Servidor',  'left', 1000);
        $dgservidor = new TDataGridColumn('nome_servidor',    'Servidor',    'left', 200);
        $dgmatricula = new TDataGridColumn('matricula_servidor',    'Matrícula',    'left', 80);
        $dgdata = new TDataGridColumn('databatida',    'Data',    'left', 80);
        $dgtipojustificativa = new TDataGridColumn('nome_tipojustificativa',    'Tipo Justificativa',    'left', 150);
        $dgjustificativa = new TDataGridColumn('justificativa', 'Justificativa','left', 500);
        $dgsituacao = new TDataGridColumn('situacaojustificativa',    'Situação',    'left', 80);
        $dganexo = new TDataGridColumn('justificativapdf',    'Anexo',    'left', 80);
        $dgautorizacao = new TDataGridColumn('dataautorizacao',    'Autorização',    'left', 80);
        $dgchefeautorizacao = new TDataGridColumn('nome_chefe',    'Chefe Autorização',    'left', 80);


        $dgdata->setTransformer('formatar_data');
        $dgautorizacao->setTransformer('formatar_data');

        $this->datagrid->addColumn($dgservidor);
        $this->datagrid->addColumn($dgmatricula);
        $this->datagrid->addColumn($dgdata);
        $this->datagrid->addColumn($dgtipojustificativa);
        $this->datagrid->addColumn($dgjustificativa);
        $this->datagrid->addColumn($dgsituacao);
        $this->datagrid->addColumn($dganexo);
        $this->datagrid->addColumn($dgautorizacao);
        $this->datagrid->addColumn($dgchefeautorizacao);


        $this->datagrid->createModel();

        $panel = new TPanelForm(700, 500);
        $panel->put($this->form, 0, 0);
        $panel->put($this->datagrid, 150, 115);

        parent::add($panel);
    }

    function onReload() {

        /*TTransaction::open('pg_ceres');

        $repository = new TRepository('Marcacao_RelogioRecord');
        $criteria = new TCriteria;
        $criteria->setProperty('order', 'databatida DESC');
        $criteria->add(new TFilter('servidor_id', '=', $_SESSION['servidor_id']));
        $criteria->add(new TFilter('tipojustificativaponto_id', 'IS NOT', NULL));
        $cadastros = $repository->load($criteria);

        $this->datagrid->clear();

        if ($cadastros) {
            foreach ($cadastros as $cadastro) {

                if ($cadastro->dataautorizacao == null){
                    $cadastro->dataautorizacao = 'Não';
                }

                $this->datagrid->addItem($cadastro);
            }
        }

        TTransaction::close();
        $this->loaded = true;*/
    }

    public function onSearch(){

        try{

            $this->form->validate();

            $situacaoTemp = $this->form->getFieldData('situacaojustificativa');
            $servidorTemp = $this->form->getFieldData('servidor_id');
            $servidorexplode = explode("::",$servidorTemp);
            $mesTemp = $this->form->getFieldData('mes');
            $anoTemp = $this->form->getFieldData('ano');

            TTransaction::open('pg_ceres');

            $repository = new TRepository('Marcacao_RelogioRecord');

            $criteria = new TCriteria;

            $criteria->setProperty('order', 'databatida DESC');

            $criteria->add(new TFilter('servidor_id', '=', $servidorexplode[0]));

            $criteria->add(new TFilter('tipojustificativaponto_id', 'IS NOT', NULL));


            if($situacaoTemp != NULL){
                $criteria->add(new TFilter('situacaojustificativa', '=', $situacaoTemp));
            }

            if($anoTemp != NULL){
                $criteria->add(new TFilter('date_part(\'YEAR\', databatida)', '=', $anoTemp));
            }

            if($mesTemp != NULL){
                $criteria->add(new TFilter(('date_part(\'month\', databatida)'), '=', $mesTemp));
            }

            $objects = $repository->load($criteria);
            $this->datagrid->clear();

            if ($objects){

                foreach ($objects as $object){

                    $this->datagrid->addItem($object);
                }
            }

            TTransaction::close();
        }
        catch (Exception $e){
            new TMessage('erro', $e->getMessage());
            TTransaction::rollback();
        }
    }

    function show() {
        $this->onReload();
        parent::show();
    }
}