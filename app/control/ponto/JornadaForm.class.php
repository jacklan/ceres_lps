<?php

/**
 * JornadaForm
 *
 * @version    2.0
 * @package    ceres
 * @subpackage template2\app\control\PONTO
 * @author     Lucas Macedo - 06/09/2016
 * @copyright  Copyright (c) 2006-2014 Adianti Solutions Ltd. (http://www.adianti.com.br)
 * @license    http://www.adianti.com.br/framework-license
 */
//ini_set('display_errors', 1);
//ini_set('display_startup_erros', 1);
//error_reporting(E_ALL);

class JornadaForm extends TPage
{
    private $form;     // formulario de cadastro

    /*
     * metodo construtor
     * Cria a pagina do formulario
     */

    public function __construct()
    {
        parent::__construct();

        // instancia um formulario
        $this->form = new TQuickForm('form_Jornada');
        $this->form->setFormTitle('<font color="red" size="3" face="Arial"><b>Formul&aacute;rio de Jornada</b></font>');

        ##===========================================================================##
        ##===========================================================================##  
        // cria os campos do formulario

        $descricao = new TEntry('descricao');

        $entrada1 = new TEntry('entrada1');
        $saida1 = new TEntry('saida1');

        $entrada2 = new TEntry('entrada2');
        $saida2 = new TEntry('saida2');

        $entrada3 = new TEntry('entrada3');
        $saida3 = new TEntry('saida3');

        $entrada1inicio = new TEntry('entrada1inicio');
        $entrada1fim = new TEntry('entrada1fim');

        $saida1inicio = new TEntry('saida1inicio');
        $saida1fim = new TEntry('saida1fim');

        $entrada2inicio = new TEntry('entrada2inicio');
        $entrada2fim = new TEntry('entrada2fim');

        $saida2inicio = new TEntry('saida2inicio');
        $saida2fim = new TEntry('saida2fim');

        $entrada3inicio = new TEntry('entrada3inicio');
        $entrada3fim = new TEntry('entrada3fim');

        $saida3inicio = new TEntry('saida3inicio');
        $saida3fim = new TEntry('saida3fim');

        $id = new THidden('id');
        $dataalteracao = new THidden('dataalteracao');
        $usuarioalteracao = new THidden('usuarioalteracao');

        // cria um rotulo para o titulo
        $titulo = new TLabel('<div style="position:floatval; width: 200px;"> <b>* Campos obrigat&oacute;rios</b></div>');
        $titulo->setFontFace('Arial');
        $titulo->setFontColor('red');
        $titulo->setFontSize(10);

        // cria um rotulo para o titulo
        $titulo2 = new TLabel('<div style="position:floatval; width: 200px;"> <b>* Todos os campos são do tipo horário (HH:mm).</b></div>');
        $titulo2->setFontFace('Arial');
        $titulo2->setFontColor('green');
        $titulo2->setFontSize(10);


        ##===========================================================================##
        ##===========================================================================## 
        // define os campos
        $this->form->addQuickField(null, $titulo2, 300);

        $this->form->addQuickField('1ª Entrada <font color=red><b>*</b></font>', $entrada1, 20);
        $this->form->addQuickField('1ª Saída   <font color=red><b>*</b></font>', $saida1, 20);

        $this->form->addQuickField('2ª Entrada ', $entrada2, 20);
        $this->form->addQuickField('2ª Saída   ', $saida2, 20);

        $this->form->addQuickField('3ª Entrada ', $entrada3, 20);
        $this->form->addQuickField('3ª Saída   ', $saida3, 20);


        $this->form->addQuickField('Limite Início da 1ª Entrada ', $entrada1inicio, 20);
        $this->form->addQuickField('Limite Fim da 1ª Entrada    ', $entrada1fim, 20);
        $this->form->addQuickField('Limite Início da 1ª Saída   ', $saida1inicio, 20);
        $this->form->addQuickField('Limite Fim da 1ª Saída      ', $saida1fim, 20);

        $this->form->addQuickField('Limite Início da 2ª Entrada ', $entrada2inicio, 20);
        $this->form->addQuickField('Limite Fim da 2ª Entrada    ', $entrada2fim, 20);
        $this->form->addQuickField('Limite Início da 2ª Saída   ', $saida2inicio, 20);
        $this->form->addQuickField('Limite Fim da 2ª Saída      ', $saida2fim, 20);

        $this->form->addQuickField('Limite Início da 3ª Entrada ', $entrada3inicio, 20);
        $this->form->addQuickField('Limite Fim da 3ª Entrada    ', $entrada3fim, 20);
        $this->form->addQuickField('Limite Início da 3ª Saída   ', $saida3inicio, 20);
        $this->form->addQuickField('Limite Fim da 3ª Saída      ', $saida3fim, 20);


        $entrada1->setMask('00:00');
        $saida1->setMask('00:00');

        $entrada2->setMask('00:00');
        $saida2->setMask('00:00');

        $entrada3->setMask('00:00');
        $saida3->setMask('00:00');

        $entrada1inicio->setMask('00:00');
        $entrada1fim->setMask('00:00');
        $saida1inicio->setMask('00:00');
        $saida1fim->setMask('00:00');

        $entrada2inicio->setMask('00:00');
        $entrada2fim->setMask('00:00');
        $saida2inicio->setMask('00:00');
        $saida2fim->setMask('00:00');

        $entrada2inicio->setMask('00:00');
        $entrada2fim->setMask('00:00');
        $saida2inicio->setMask('00:00');
        $saida2fim->setMask('00:00');


        $this->form->addQuickField(null, $id, 10);
        $this->form->addQuickField(null, $usuarioalteracao, 10);
        $this->form->addQuickField(null, $dataalteracao, 10);
        $this->form->addQuickField(null, $titulo, 50);


        $entrada1->addValidation('1ª Entrada', new TRequiredValidator);
        $saida1->addValidation('1ª Saída', new TRequiredValidator);


        ##===========================================================================##
        ##===========================================================================##  
        // cria um botao de acao
        $this->form->addQuickAction('Salvar', new TAction(array($this, 'onSave')), 'ico_save.png')->class = 'btn btn-info btnleft';
        $this->form->addQuickAction('Voltar', new TAction(array('JornadaList', 'onReload')), 'ico_datagrid.gif');

        // adiciona a tabela a pagina
        parent::add($this->form);
    }

    /*
     * metodo onSave()
     * Executada quando o usuario clicar no botao salvar do formulario
     */

    function onSave()
    {

        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // obtem os dados no formulario em um objeto CarroRecord
        $cadastro = $this->form->getData('JornadaRecord');

        //lanca o default
        $cadastro->usuarioalteracao = $_SESSION['usuario'];
        $cadastro->dataalteracao = date("d/m/Y H:i:s");
        $cadastro->empresa_id = $_SESSION['empresa_id'];

        try
        {

            //Validade
            $this->form->validate();

            // armazena o objeto
            $cadastro->store();

            TTransaction::close();

            // exibe um dialogo ao usuario
            new TMessage("info", "Registro salvo com sucesso!");
            TApplication::gotoPage('JornadaList', 'onReload'); // reload
        }
        catch (Exception $e)
        {

            // em caso de exce��o
            // exibe a mensagem gerada pela exce��o
            new TMessage('error', $e->getMessage());

            // desfaz todas altera��es no banco de dados
            TTransaction::rollback();
        }
    }

    /*
     * metodo onEdit()
     * Edita os dados de um registro
     */

    function onEdit($param)
    {

        try
        {
            if (isset($param['key']))
            {

                // get the parameter $key
                $key = $param['key'];

                TTransaction::open('pg_ceres');   // open a transaction with database 'samples'

                $object = new JornadaRecord($key);        // instantiates object City

                $object->entrada1 = formatar_hora( $object->entrada1 );
                $object->saida1 = formatar_hora( $object->saida1 );

                $object->entrada2 = formatar_hora( $object->entrada2 );
                $object->saida2 = formatar_hora( $object->saida2 );

                $object->entrada3 = formatar_hora( $object->entrada3 );
                $object->saida3 = formatar_hora( $object->saida3 );
                
                $object->entrada1inicio = formatar_hora( $object->entrada1inicio );
                $object->entrada1fim = formatar_hora( $object->entrada1fim );
                $object->saida1inicio = formatar_hora( $object->saida1inicio );
                $object->saida1fim = formatar_hora( $object->saida1fim );

                $object->entrada2inicio = formatar_hora( $object->entrada2inicio );
                $object->entrada2fim = formatar_hora( $object->entrada2fim );
                $object->saida2inicio = formatar_hora( $object->saida2inicio );
                $object->saida2fim = formatar_hora( $object->saida2fim );

                $object->entrada3inicio = formatar_hora( $object->entrada3inicio );
                $object->entrada3fim = formatar_hora( $object->entrada3fim );
                $object->saida3inicio = formatar_hora( $object->saida3inicio );
                $object->saida3fim = formatar_hora( $object->saida3fim );
                
                $this->form->setData($object);   // fill the form with the active record data

                TTransaction::close();           // close the transaction
            }
        }
        catch (Exception $e)
        {

            // in case of exception
            // shows the exception error message
            new TMessage('error', '<b>Error</b> ' . $e->getMessage());
            // undo all pending operations
            TTransaction::rollback();
        }
    }

}