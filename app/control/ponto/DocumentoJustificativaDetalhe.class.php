<?php

include_once 'app/lib/funcdate.php';

use Adianti\Widget\Datagrid\TDatagridTables;

class DocumentoJustificativaDetalhe extends TPage
{
    private $form;
    private $datagrid;

    public function __construct(){
        parent::__construct();

        $this->form = new TQuickForm('form_documento_justificativa');
        $this->form->style = 'width: 40%';
        $this->form->setFormTitle('<font color="red" size="3px" face="Arial"><b>Documento Justificativa</b></font>');

     $criteria_servidor = new TCriteria;
        $criteria_servidor->add(new TFilter('situacao', '=', 'EM ATIVIDADE'), TExpression::OR_OPERATOR);
        $criteria_servidor->add(new TFilter('situacao', '=', 'A DISPOSICAO'), TExpression::OR_OPERATOR);

        $criteria3 = new TCriteria;
        if ($_SESSION['empresa_id'] == 1) {
            $criteria3->add(new TFilter('empresa_id', '=', $_SESSION['empresa_id']));
        }
        $criteria3->add($criteria_servidor);
        /* ---------- fim criacao criterio de selecao ---------- */

        /* --------- montar campo --------- */
        $servidor_id = new TDBMultiSearch('servidor_id', 'pg_ceres', 'vw_servidor_multisearchRecord', 'servidor_id', 'nome_matricula_servidor', 'nome_matricula_servidor', $criteria3);
        $servidor_id->setProperty('placeholder', 'Nome ou Matricula ou CPF');
        $servidor_id->setMinLength(1); 
        $servidor_id->setMaxSize(1);


        $situacao = new TCombo('situacaojustificativa');
        $mes = new TCombo('mes');
        $ano = new TCombo('ano');

        $items = array();
        $items['01'] = 'JANEIRO';
        $items['02'] = 'FEVEREIRO';
        $items['03'] = 'MAR&Ccedil;O';
        $items['04'] = 'ABRIL';
        $items['05'] = 'MAIO';
        $items['06'] = 'JUNHO';
        $items['07'] = 'JULHO';
        $items['08'] = 'AGOSTO';
        $items['09'] = 'SETEMBRO';
        $items['10'] = 'OUTUBRO';
        $items['11'] = 'NOVEMBRO';
        $items['12'] = 'DEZEMBRO';

        $items2 = array();
        $items2['DEFERIDO'] = 'DEFERIDO';
        $items2['INDEFERIDO'] = 'INDEFERIDO';
        $items2['AGUARDANDO'] = 'AGUARDANDO';

        $situacao->addItems($items2);

        $mes->addItems($items);
        $mes->setValue(date('m'));

        $ano->addItems(retornaAnosemZero());
        $ano->setValue(date('Y'));
        $obs = new TLabel('<font color=red><b>Pesquisar nome do Servidor com "CAPS LOCK" Habilitado</b></font>');


        $this->form->addQuickField('Servidor',$servidor_id, 250);
        $this->form->addQuickField('Situação', $situacao, 70);
        $this->form->addQuickField('M&ecirc;s', $mes, 70);
        $this->form->addQuickField('Ano', $ano, 70);
        $this->form->addQuickField('<font color=red><b>OBS:</b></font>',$obs, 70);

        $this->form->addQuickAction('Buscar', new TAction(array($this, 'onSearch')), 'fa:search');
        $this->form->addQuickAction('Gerar Relat&oacute;rio', new TAction(array($this, 'onGenerate')), 'fa:file-pdf-o');

        $this->datagrid = new TDataGridTables;

        $dgnomeservidor = new TDataGridColumn('nome_servidor', 'Nome Servidor', 'left', 100);
        $dgdatabatida = new TDataGridColumn('databatida', 'Data Batida', 'left', 80);
        $dgdatajustificativa = new TDataGridColumn('dtjustificativa', 'Data Justificativa', 'left', 80);
        $horajornada   = new TDataGridColumn('hora_jornada', 'Jornada', 'left', 80);
        $horatotal   = new TDataGridColumn('horatotal', 'Apurado', 'left', 80);
        $situacaojustificativa    = new TDataGridColumn('situacaojustificativa', 'Situação', 'left', 80);
        $nometipojustificativa   = new TDataGridColumn('nometipojustificativa', 'Tipo', 'left', 200);
        $dataautorizacao   = new TDataGridColumn('dataautorizacao', 'Data Autorização', 'left', 80);


        $this->datagrid->addColumn($dgnomeservidor);
        $this->datagrid->addColumn($dgdatabatida);
        $this->datagrid->addColumn($dgdatajustificativa);
        $this->datagrid->addColumn($horajornada);
        $this->datagrid->addColumn($horatotal);
        $this->datagrid->addColumn($situacaojustificativa);
        $this->datagrid->addColumn($nometipojustificativa);
        $this->datagrid->addColumn($dataautorizacao);

        $dgdatabatida->setTransformer('formatar_data');
        $dataautorizacao->setTransformer('formatar_data');
        $horajornada  ->setTransformer('formatar_hora');
        $horatotal  ->setTransformer('formatar_hora');
        $dgdatajustificativa  ->setTransformer('formatar_data');


        $this->datagrid->disableDefaultClick();

        $this->datagrid->createModel();

        $panel = new TPanelForm(700, 500);
        $panel->put($this->form, 0, 0);
        $panel->put($this->datagrid, 150, 115);

        parent::add($panel);

    }

    public function onSearch(){

        try{

            $situacaoTemp = $this->form->getFieldData('situacaojustificativa');
            $servidorTemp = $this->form->getFieldData('servidor_id');
            $servidorexplode = explode("::",$servidorTemp);
            $mesTemp = $this->form->getFieldData('mes');
            $anoTemp = $this->form->getFieldData('ano');

            TTransaction::open('pg_ceres');

            $repository = new TRepository('vw_justificativa_acompanhamentoRecord');

            $criteria = new TCriteria;

            if ($servidorTemp != NULL){

                $criteria->add(new TFilter('servidor_id', '=', $servidorexplode[0]));
            }

            if($situacaoTemp != NULL){
                $criteria->add(new TFilter('situacaojustificativa', '=', $situacaoTemp));
            }

            if($anoTemp != NULL){
                $criteria->add(new TFilter('anojustificativa', '=', $anoTemp));
            }

            if($mesTemp != NULL){
                $criteria->add(new TFilter('mejustificativa', '=', $mesTemp));
            }

            $criteria->setProperty('order', 'servidor_id');

            $objects = $repository->load($criteria);
            $this->datagrid->clear();

            //var_dump($objects);
            //exit();

            if ($objects){

                foreach ($objects as $object){

                    $this->datagrid->addItem($object);
                }
            }

            TTransaction::close();
        }
        catch (Exception $e){
            new TMessage('erro', $e->getMessage());
            TTransaction::rollback();
        }
    }


    function onGenerate(){

        try{

            new RelatorioDocumentoJustificativaPDF();


        }catch( Exception $e ){
            new TMessage( 'error', $e->getMessage() );

        }
    }

}