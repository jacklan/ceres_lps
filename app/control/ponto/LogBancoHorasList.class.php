<?php

include_once 'app/lib/funcdate.php';

setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');

use Adianti\Widget\Datagrid\TDatagridTables;

class LogBancoHorasList extends TPage
{
    private $form;
    private $datagrid;

    public function __construct()
    {
        parent::__construct();

        $this->form = new TQuickForm;
        $this->form->class = 'list_banco_horas';
        $this->form->setFormTitle('<font color="red" size="3" face="Arial"><b>Processar Banco de Horas </b></font>');

        $id     = new THidden('id');
        $mes = new TCombo('mesbase');
        $ano = new TCombo('anobase');

        $id->setEditable(false);

        $dataapuracao = new TDate('dataapuracao');
        $dataapuracao->setEditable(false);
        $dataapuracao->setValue(date('d/m/y H:i\h'));

        $items = array();
        $items['01'] = 'JANEIRO';
        $items['02'] = 'FEVEREIRO';
        $items['03'] = 'MAR&Ccedil;O';
        $items['04'] = 'ABRIL';
        $items['05'] = 'MAIO';
        $items['06'] = 'JUNHO';
        $items['07'] = 'JULHO';
        $items['08'] = 'AGOSTO';
        $items['09'] = 'SETEMBRO';
        $items['10'] = 'OUTUBRO';
        $items['11'] = 'NOVEMBRO';
        $items['12'] = 'DEZEMBRO';


        $mes->addItems($items);
        $mes->setValue(date('m', strtotime('-1 months', strtotime(date('Y-m-d')))));

        $ano->addItems(retornaAnosemZero());
        $ano->setValue(date('Y'));

        $usuarioalteracao = new THidden('usuarioalteracao');
        $dataalteracao = new THidden('dataalteracao');

        $dataapuracao->setSize(50);

        $campo = new TLabel('<div style="position:floatval; width: 200px;"> <b>* Campos obrigat&oacute;rios</b></div>');
        $campo->setFontFace('Arial');
        $campo->setFontColor('red');
        $campo->setFontSize(10);

        $mes->addValidation('Mês', new TRequiredValidator);
        $ano->addValidation('Ano', new TRequiredValidator);

        $this->form->addQuickField(null, $id, 10);
        $this->form->addQuickField(null, $usuarioalteracao, 10);
        $this->form->addQuickField(null, $dataalteracao, 10);
        $this->form->addQuickField('Data Processamento <font color=red><b>*</b></font>', $dataapuracao, 30);
        $this->form->addQuickField('Mês Base <font color=red><b>*</b></font>', $mes, 30);
        $this->form->addQuickField('Ano Base <font color=red><b>*</b></font>', $ano, 30);

        $this->form->addQuickField(null, $campo, 50);

        $this->form->addQuickAction('Processar', new TAction(array($this, 'onProcessar')), 'ico_save.png')->class = 'btn btn-info';

        $this->datagrid = new TDataGridTables;

        $dgdataapuracacao     = new TDataGridColumn('dataapuracao',    'Dia do Processamento',    'left', 1200);
        $dgmesbase            = new TDataGridColumn('mesbase',    'Mês Base',    'left', 1200);
        $dganobase            = new TDataGridColumn('anobase',    'Ano Base',    'left', 1200);

        $this->datagrid->addColumn($dgdataapuracacao);
        $this->datagrid->addColumn($dgmesbase);
        $this->datagrid->addColumn($dganobase);

        $dgdataapuracacao->setTransformer('formatar_data');


        $action2 = new TDataGridAction(array($this, 'onDelete'));
        $action2->setLabel('Deletar');
        $action2->setImage('ico_delete.png');
        $action2->setField('id');

        //$this->datagrid->addAction($action1);
        $this->datagrid->addAction($action2);


        $this->datagrid->createModel();

        $panel = new TPanelForm(700, 500);
        $panel->put($this->form, 0, 0);
        $panel->put($this->datagrid, 150, 115);

        parent::add($panel);
    }

    function onReload() {

        TTransaction::open('pg_ceres');

        $repository = new TRepository('LogBancoHorasRecord');
        $criteria = new TCriteria;

        $criteria->setProperty('order', 'anobase DESC');
        $criteria->add(new TFilter('empresa_id', '=', $_SESSION['empresa_id']));

        $cadastros = $repository->load($criteria);


        $this->datagrid->clear();

        if ($cadastros) {
            foreach ($cadastros as $cadastro) {

                if ($cadastro->mesbase != null){
                    $cadastro->mesbase = ucwords(strftime('%B', mktime(0, 0, 0, $cadastro->mesbase, 1)));

                }

                $this->datagrid->addItem($cadastro);
            }
        }

        TTransaction::close();
        $this->loaded = true;
    }
    function onProcessar() {

        $mesAtual = date('m', strtotime('-1 months', strtotime(date('Y-m-d'))));
        $anoAtual = date('Y');
        $mesTemp = $this->form->getFieldData('mesbase');
        $anoTemp = $this->form->getFieldData('anobase');

        $this->form->validate();


        if ($mesTemp <= $mesAtual && $anoTemp == $anoAtual){

            TTransaction::open('pg_ceres');

            $cadastro = $this->form->getData('LogBancoHorasRecord');
            $cadastro->usuarioalteracao = $_SESSION['usuario'];
            $cadastro->dataalteracao = date("d/m/Y H:i:s");
            $cadastro->empresa_id = $_SESSION['empresa_id'];

            $dados = $cadastro->toArray();

            $msg = '';
            $icone = 'info';

            if (empty($dados['dataapuracao'])) {
                $msg .= 'A Data da Apuração deve ser informado.</br>';
            }

            try {

                $this->form->validate();
                if ($msg == '') {
                    $cadastro->store();
                    $msg = 'Dados armazenados com sucesso';

                    TTransaction::close();
                } else {
                    $icone = 'error';
                }

                if ($icone == 'error') {
                    new TMessage($icone, $msg);
                } else {
                    new TMessage("info", "Registro salvo com sucesso!");
                    TApplication::gotoPage('LogBancoHorasList','onReload');

                }
            } catch (Exception $e) {
                new TMessage('error', $e->getMessage());
                TTransaction::rollback();
            }

        }else if ($anoTemp < $anoAtual) {

            TTransaction::open('pg_ceres');

            $cadastro = $this->form->getData('LogBancoHorasRecord');
            $cadastro->usuarioalteracao = $_SESSION['usuario'];
            $cadastro->dataalteracao = date("d/m/Y H:i:s");
            $cadastro->empresa_id = $_SESSION['empresa_id'];

            $dados = $cadastro->toArray();

            $msg = '';
            $icone = 'info';

            if (empty($dados['dataapuracao'])) {
                $msg .= 'A Data da Apuração deve ser informado.</br>';
            }

            try {

                $this->form->validate();
                if ($msg == '') {
                    $cadastro->store();
                    $msg = 'Dados armazenados com sucesso';

                    TTransaction::close();
                } else {
                    $icone = 'error';
                }

                if ($icone == 'error') {
                    new TMessage($icone, $msg);
                } else {
                    new TMessage("info", "Registro salvo com sucesso!");
                    TApplication::gotoPage('LogBancoHorasList','onReload');

                }
            } catch (Exception $e) {
                new TMessage('error', $e->getMessage());
                TTransaction::rollback();
            }

        }else{

            new TMessage("error", "Não é possível processar este mês, favor selecionar mês anterior.");
        }
    }

    function onDelete($param) {

        $key = $param['key'];

        $action1 = new TAction( array( $this, 'Delete' ) );

        $action1->setParameter('key', $key);
        //$action1->setParameter('fk', filter_input ( INPUT_GET, 'fk' ) );
        //$action1->setParameter('did', filter_input ( INPUT_GET, 'did' ) );

        new TQuestion( 'Deseja realmente excluir o registro ?', $action1 );

    }

    function Delete($param) {
        $key = $param['key'];

        TTransaction::open('pg_ceres');

        $cadastro = new LogBancoHorasRecord($key);

        try {

            $cadastro->delete();

            new TMessage("info", "Registro deletado com sucesso!");

            TTransaction::close();
        } catch (Exception $e) {

            new TMessage('error', $e->getMessage());
            TTransaction::rollback();
        }

        $this->onReload();
    }

    function show(){

        $this->onReload();
        parent::show();

    }
}