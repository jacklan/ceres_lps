<?php
include_once 'app/lib/funcdate.php';

use Adianti\Widget\Datagrid\TDatagridTables;

class LogMarcacaoList extends TPage{

    private $form;
    private $datagrid;

    public function __construct()
    {
        parent::__construct();

        $this->form = new TQuickForm;
        $this->form->class = 'form_ponto_Logmarcacao';
        $this->form->setFormTitle('<font color="red" size="3" face="Arial"><b>Processar Marcações </b></font>');

        $id     = new THidden('id');
        $id->setEditable(false);
        
        $dataapuracao = new TDate('dataapuracao');
        $dataapuracao->setEditable(false);
        $dataapuracao->setValue(date('d/m/y H:i\h'));

        $usuarioalteracao = new THidden('usuarioalteracao');
        $dataalteracao = new THidden('dataalteracao');

        //$dataapuracao->setSize(50);

        $campo = new TLabel('<div style="position:floatval; width: 200px;"> <b>* Campos obrigat&oacute;rios</b></div>');
        $campo->setFontFace('Arial');
        $campo->setFontColor('red');
        $campo->setFontSize(10);

        $this->form->addQuickField(null, $id, 10);
        $this->form->addQuickField(null, $usuarioalteracao, 10);
        $this->form->addQuickField(null, $dataalteracao, 10);
        $this->form->addQuickField('Data Processamento <font color=red><b>*</b></font>', $dataapuracao, 30);
        
        $this->form->addQuickField(null, $campo, 50);
        
        $this->form->addQuickAction('Processar', new TAction(array($this, 'onProcessar')), 'ico_save.png')->class = 'btn btn-info';

        $this->datagrid = new TDataGridTables;

        $dgdataapuracacao     = new TDataGridColumn('dataapuracao',    'Dia do Processamento',    'left', 1200);

        $this->datagrid->addColumn($dgdataapuracacao);
        
        $dgdataapuracacao->setTransformer('formatar_data');
        
        $action1 = new TDataGridAction(array($this, 'onReload'));
        $action1->setLabel('Alterar');
        $action1->setImage('ico_edite.png');
        $action1->setField('id');
        $action1->setFk('id');
        
        $this->datagrid->addAction($action1);

        $this->datagrid->createModel();

        $panel = new TPanelForm(700, 500);
        $panel->put($this->form, 0, 0);
        $panel->put($this->datagrid, 150, 115);

        parent::add($panel);
    }

    	function onReload() {
        TTransaction::open('pg_ceres');

        $repository = new TRepository('LogMarcacaoRecord');
        $criteria = new TCriteria;

        $criteria->setProperty('order', 'dataapuracao DESC');
        $criteria->add(new TFilter('empresa_id', '=', $_SESSION['empresa_id']));

        $cadastros = $repository->load($criteria);


        $this->datagrid->clear();
        
        if ($cadastros) {
            foreach ($cadastros as $cadastro) {

                $this->datagrid->addItem($cadastro);
            }
        }

        TTransaction::close();
        $this->loaded = true;
    }
    function onProcessar() {

        TTransaction::open('pg_ceres');

        $cadastro = $this->form->getData('LogMarcacaoRecord');
        $cadastro->usuarioalteracao = $_SESSION['usuario'];
        $cadastro->dataalteracao = date("d/m/Y H:i:s");
        $cadastro->empresa_id = $_SESSION['empresa_id'];

        $dados = $cadastro->toArray();

        $msg = '';
        $icone = 'info';

        if (empty($dados['dataapuracao'])) {
            $msg .= 'A Data da Apuração deve ser informado.</br>';
        }

        try {
            $this->form->validate();
            if ($msg == '') {
                $cadastro->store();
                $msg = 'Dados armazenados com sucesso';

                TTransaction::close();
            } else {
                $icone = 'error';
            }

            if ($icone == 'error') {
                new TMessage($icone, $msg);
            } else {                
               new TMessage("info", "Registro salvo com sucesso!");
               TApplication::gotoPage('LogMarcacaoList','onReload'); 

           }
       } catch (Exception $e) { 
        new TMessage('error', $e->getMessage());
        TTransaction::rollback();
    }
}

    function show(){

        $this->onReload();
        parent::show();

    }
}
?>