<?php

/*
 * classe SetorGrupoDetalhe
 * Cadastro de Setor Grupo Detalhe: Contem a listagem e o formulario de busca
 */

//ini_set('display_errors', 1);
//ini_set('display_startup_erros', 1);
//error_reporting(E_ALL);

use Adianti\Widget\Datagrid\TDatagridTables;

class EscalaGrupoDetalhe extends TPage {

    private $form;     // formulario de cadastro
    private $datagrid; // listagem

    /*
     * metodo construtor
     * Cria a pagina, o formulario e a listagem
     */

    public function __construct() {
        parent::__construct();

        // instancia um formulario
        $this->form = new TQuickForm;
        $this->form->class = 'form_Cha';
        $this->form->setFormTitle('<font color="red" size="3" face="Arial"><b>Escala dos Servidores</b></font>');

        //cria a colecao da tabela estrangeira do Cabecalho
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $cadastro = new EscalaRecord($_REQUEST['key']);

        if ($cadastro) {
            $nome_setor = $cadastro->nome;

        }
        // finaliza a transacao
        TTransaction::close();

        // cria um rotulo para o titulo
        $titulo = new TLabel($nome_setor);
        $titulo->setFontFace('Arial');
        $titulo->setFontColor('red');
        $titulo->setFontSize(12);

        $criteria_servidor = new TCriteria;
        $criteria_servidor->add(new TFilter('situacao', '=', 'EM ATIVIDADE'), TExpression::OR_OPERATOR);
        $criteria_servidor->add(new TFilter('situacao', '=', 'A DISPOSICAO'), TExpression::OR_OPERATOR);

        $criteriaS = new TCriteria;
        $criteriaS->add(new TFilter('empresa_id', '=', $_SESSION['empresa_id']));
        $criteriaS->add($criteria_servidor);

        /* ---------- fim criacao criterio de selecao ---------- */
        $servidor_id = new TDBMultiSearch('servidor_id', 'pg_ceres', 'vw_servidor_multisearchRecord', 'servidor_id', 'nome_matricula_servidor', 'nome_matricula_servidor', $criteriaS);
        $servidor_id->style = "text-transform: uppercase;";
        $servidor_id->setProperty('placeholder', 'Nome ou Matricula ou CPF');
        $servidor_id->setMinLength(1);
        $servidor_id->setMaxSize(1);

        $this->form->addQuickField('Escala:', $titulo, 200);

        $this->form->addQuickField('Servidor <b style="color:red;">*</b>', $servidor_id, 450);
        $this->form->addQuickField("<b style='color: red;'>OBS: </b>", new TLabel("<b style='color: red;'>Pesquisar nome do servidor em caixa alta, \"CAPS LOCK\" Habilitado</b>"), 600);
        $this->form->addQuickField("", new TLabel("<b style='color: red;'>Campos Obrigatórios com *</b>"), 600);

        $salvar = new TAction(array($this, 'Salvar'));
        $salvar->setParameter('key', $_REQUEST['key']);
        $this->form->addQuickAction('Salvar', $salvar, 'ico_save.png')->class = 'btn btn-info';

        $this->form->addQuickAction('Voltar', new TAction(array('EscalaList', 'onReload')), 'ico_datagrid.gif');

        // instancia objeto DataGrid
        $this->datagrid = new TDatagridTables;

        // instancia as colunas da DataGrid
        $dgservidor = new TDataGridColumn('nome_servidor', 'Servidor', 'left', 1200);
        $dgmatricula = new TDataGridColumn('matricula', 'Matricula', 'left', 300);
        $dglotacao = new TDataGridColumn('lotacao', 'Lotação', 'left', 300);

        // adiciona as colunas a DataGrid
        $this->datagrid->addColumn($dgservidor);
        $this->datagrid->addColumn($dgmatricula);
        $this->datagrid->addColumn($dglotacao);
        // cria o modelo da DataGrid, montando sua estrutura
        $this->datagrid->createModel();

        // monta a paina atraves de uma tabela
        $panel = new TPanelForm(700, 500);
        $panel->put($this->form, 0, 0);
        $panel->put($this->datagrid, 150, 80);


        // adiciona a tabela a pagina
        parent::add($panel);
    }

    /*
     * metodo onReload()
     * Carrega a DataGrid com os objetos do banco de dados
     */

    function onReload() {
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instancia um repositorio da Classe
        $repository = new TRepository('vw_servidorRecord');

        // cria um criterio de selecao
        $criteria = new TCriteria;
        $criteria->add(new TFilter('empresa_id', '=', $_SESSION['empresa_id']) );

        //filtra pelo campo selecionado pelo usuario
        $criteria->add(new TFilter('escala_id', '=', $_REQUEST['key']));

        //$criteria->setProperty('order', "$campo");
        // carrega os objetos de acordo com o criterio
        $cadastros = $repository->load($criteria);

        $this->datagrid->clear();
        if ($cadastros) {
            // percorre os objetos retornados
            foreach ($cadastros as $cadastro) {
                // adiciona o objeto na DataGrid
                $this->datagrid->addItem($cadastro);
            }
        }

        // finaliza a transacao
        TTransaction::close();
        $this->loaded = true;
    }

    function Salvar($param){

        $key = $param ['key'];

        TTransaction::open('pg_ceres');

        $std = $this->form->getData();
        $id = key($std->servidor_id);
        $servidor = new ServidorRecord($id);
 
        $salvar = new TAction(array($this, 'onSave'));
         
        $salvar->setParameter('key', $key);  
        $salvar->setParameter('id', $id);

        if($servidor->jornada_id == NULL && $servidor->escala_id != NULL ){

          new TQuestion("Servidor com escala existente, deseja continuar operação?",$salvar);

        }

        if($servidor->jornada_id != NULL){
          new TQuestion("Servidor com jornada existente, deseja continuar operação?",$salvar);
        }

        TTransaction::close();
    }

    function onSave($param) {

        TTransaction::open('pg_ceres');

        $servidor = new ServidorRecord($param['id']);
       
        //lanca o default
        $servidor->escala_id = $_GET['key'];
        $servidor->jornada_id = NULL;

        $msg = '';
        $icone = 'info';

        $param['key'] =  $servidor->escala_id;

        try {
        $this->form->validate();
            if ($msg == '') {
                // armazena o objeto
                $servidor->store();
                $msg = 'Dados armazenados com sucesso';

                // finaliza a transacao
                TTransaction::close();
            } else {
                $icone = 'error';
            }

            if ($icone == 'error') {
                // exibe mensagem de erro
                new TMessage($icone, $msg);
            } else {
             new TMessage("info", "Registro salvo com sucesso!");
                TApplication::gotoPage('EscalaGrupoDetalhe','onReload',$param); // reload

            }
        } catch (Exception $e) { // em caso de exce��o
            // exibe a mensagem gerada pela exce��o
            new TMessage('error', $e->getMessage());
            // desfaz todas altera��es no banco de dados
            TTransaction::rollback();
        }
    }


    function show() {
        // carrega os dados no datagrid
        $this->onReload();
        //chama o metodo show da super classe
        parent::show();
    }

}

?>
