<?php

/*
* classe TipoJustificativaPontoForm
* Cadastro de Tipo de Justificativa: Contem o formularo
* Autor:Lucas Vicente
* Data: 31/08/2016 teste
 */

class EscalaForm extends TPage {

    private $form;     // formulario de cadastro

    /*
     * metodo construtor
     * Cria a pagina do formulario
     */

    public function __construct() {
        parent::__construct();
        
        // instancia um formulario
        $this->form = new TQuickForm;
        $this->form->class = 'form_ponto_escala';
        $this->form->setFormTitle('<font color="red" size="3" face="Arial"><b>Formul&aacute;rio de Escala
            </b></font>');
        
        // cria os campos do formulario
        $tipoescala = new TRadioGroup('tipoescala');
        
        
        $codigo = new THidden('id');
        $codigo->setEditable(false);
        $nome = new TEntry('nome');
        $diastrabalhados = new TEntry('diastrabalhados');
        $diasfolgasemana = new TEntry('diasfolgasemana');
        $horafolga = new TEntry('horafolga');
        $horatrabalhada = new TEntry('horatrabalhada');
        $iniciohorario = new TEntry('iniciohorario');
        $duracaoescala = new TEntry('duracaoescala');
        $diasemanainicio = new TCombo('diasemanainicio');
        

        $usuarioalteracao = new THidden('usuarioalteracao');
        $dataalteracao = new THidden('dataalteracao');
        
        
        $horafolga->disableField($this->form->getName(), 'horafolga');
        $horatrabalhada->disableField($this->form->getName(),'horatrabalhada');

        $itemdia = array();
        $itemdia['1'] = 'DOMINGO';
        $itemdia['2'] = 'SEGUNDA';
        $itemdia['3'] = 'TERÇA';
        $itemdia['4'] = 'QUARTA';
        $itemdia['5'] = 'QUINTA';
        $itemdia['6'] = 'SEXTA';
        $itemdia['7'] = 'SÁBADO';

        $diasemanainicio->addItems($itemdia);        
        
        
        //$nome->setProperty('placeholder', 'EX.:CAPACITAÇAO');
        
        // cria um rotulo para o titulo
        $campo = new TLabel('<div style="position:floatval; width: 200px;"> <b>* Campos obrigat&oacute;rios</b></div>');
        $campo->setFontFace('Arial');
        $campo->setFontColor('red');
        $campo->setFontSize(10);
        
        $tipoescala->addItems(array('1'=>'Dias', '2'=>'Horas'));
        $tipoescala->setLayout('horizontal');
        $tipoescala->setValue(1);

        // define os campos
        
        $this->form->addQuickField(null, $codigo, 10);
        $this->form->addQuickField(null, $usuarioalteracao, 10);
        $this->form->addQuickField(null, $dataalteracao, 10);
        $this->form->addQuickField('Nome <font color=red><b>*</b></font>', $nome, 50);
        $this->form->addQuickField('Tipo de Escala <font color=red><b>*</b></font>',    $tipoescala, 50);
        $this->form->addQuickField('Dias Trabalhados', $diastrabalhados, 50);
        $this->form->addQuickField('Dias de Folga', $diasfolgasemana, 50);
        $this->form->addQuickField('Horas Trabalhadas', $horatrabalhada, 50);
        $this->form->addQuickField('Horas de Folga', $horafolga, 50);
        $this->form->addQuickField('Horário Inicio', $iniciohorario, 50);
        $this->form->addQuickField('Dia Inicio', $diasemanainicio, 50);
        $this->form->addQuickField('Duração Escala', $duracaoescala, 50);
        
        $this->form->addQuickField(null, $campo, 50);

        $iniciohorario->setProperty('placeholder', '08:00');
        $duracaoescala->setProperty('placeholder', '12:00');
        $diastrabalhados->setProperty('placeholder', 'Apenas n&uacute;meros');
        $diasfolgasemana->setProperty('placeholder', 'Apenas n&uacute;meros');        
        $horatrabalhada->setProperty('placeholder', 'Apenas n&uacute;meros');
        $horafolga->setProperty('placeholder', 'Apenas n&uacute;meros');

        $iniciohorario->setMask('99:99');
        $duracaoescala->setMask('99:99');
        
        $acaoRadio = new TAction(array($this, 'onChangeRadio'));
        $acaoRadio->setParameter('formName', $this->form->getName());
        $tipoescala->setChangeAction($acaoRadio);
        
        // cria um botao de acao
        $this->form->addQuickAction('Salvar', new TAction(array($this, 'onSave')), 'ico_save.png')->class = 'btn btn-info';
        $this->form->addQuickAction('Voltar', new TAction(array('EscalaList', 'onReload')), 'ico_datagrid.gif');
        
        
        
        
        // adiciona a tabela a pagina
        parent::add($this->form);
    }
    
    
    public static function onChangeRadio($param)
    {
        switch ($param['tipoescala'])
        {
            case '1':
            
            TEntry::clearField($param['formName'], 'horafolga');
            TEntry::clearField($param['formName'], 'horatrabalhada');
            
            TEntry::disableField($param['formName'], 'horafolga');
            TEntry::disableField($param['formName'], 'horatrabalhada');
            
            TEntry::enableField($param['formName'], 'diastrabalhados');
            TEntry::enableField($param['formName'], 'diasfolgasemana');
            
            break;
        
            case '2':
            
            TEntry::clearField($param['formName'], 'diastrabalhados');
            TEntry::clearField($param['formName'], 'diasfolgasemana');
            
            TEntry::disableField($param['formName'], 'diastrabalhados');
            TEntry::disableField($param['formName'], 'diasfolgasemana');
            
            TEntry::enableField($param['formName'], 'horafolga');
            TEntry::enableField($param['formName'], 'horatrabalhada');     
 
            break;
        }
    }

    /*
     * metodo onSave()
     * Executada quando o usuario clicar no botao salvar do formulario
     */

    function onSave() {
        
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // obtem os dados no formulario em um objeto CarroRecord

        $cadastro = $this->form->getData('EscalaRecord');
        //lanca o default
        $cadastro->usuarioalteracao = $_SESSION['usuario'];
        $cadastro->dataalteracao = date("d/m/Y H:i:s");
        $cadastro->empresa_id = $_SESSION['empresa_id'];
        
        unset($cadastro->tipoescala);
        
        //antes de armazenar verifica se algum campo eh requerido e nao foi informado
        $dados = $cadastro->toArray();

        $msg = '';   
        $icone = 'info';

        if (empty($dados['nome'])) {
            $msg .= 'O Nome deve ser informado.</br>';
        }

        
        try {
        $this->form->validate();
            if ($msg == '') {
                // armazena o objeto
                $cadastro->store();
                $msg = 'Dados armazenados com sucesso';

                // finaliza a transacao
                TTransaction::close();
            } else {
                $icone = 'error';
            }

            if ($icone == 'error') {
                // exibe mensagem de erro
                new TMessage($icone, $msg);
            } else {                
             new TMessage("info", "Registro salvo com sucesso!");
                TApplication::gotoPage('EscalaList','onReload'); // reload
                
            }
        } catch (Exception $e) { // em caso de exce��o
            // exibe a mensagem gerada pela exce��o
            new TMessage('error', $e->getMessage());
            // desfaz todas altera��es no banco de dados
            TTransaction::rollback();
        }
    }

    /*
     * metodo onEdit()
     * Edita os dados de um registro
     */

    function onEdit($param) {
        
        try {
            if (isset($param['key'])) {

                // get the parameter $key
                $key = $param['key'];

                TTransaction::open('pg_ceres');   // open a transaction with database 'samples'

                $object = new EscalaRecord($key); // instantiates object City

                $this->form->setData($object);   // fill the form with the active record data

                TTransaction::close();           // close the transaction
            } 
        } catch (Exception $e) { // in case of exception
            // shows the exception error message
            new TMessage('error', '<b>Error</b> ' . $e->getMessage());
            // undo all pending operations
            TTransaction::rollback();
        }
        
    }
    
    
    
    
    
    
}

?>