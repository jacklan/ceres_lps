<?php

ini_set('display_errors', 1);
ini_set('display_startup_erros', 1);
error_reporting(E_ALL);

include_once 'app/lib/funcdate.php';

class GeraRelatorioResumoBatidaServidorEmpresa extends TPage 
{

	private $form;
	
    public function __construct() 
    {

        parent::__construct();

        $this->form = new TQuickForm;
        $this->form->class = 'form_ServidoresSemBatida';
        $this->form->setFormTitle('<font color="red" size="3px" face="Arial"><b>Relat&oacute;rio Resumido de Batidas por Servidor</b></font>');
        
        $ano = new TCombo('ano');
        $mes = new TCombo('mes');
        
        $items= array();
        $items['1'] = 'JANEIRO';
        $items['2'] = 'FEVEREIRO';
        $items['3'] = 'MARCO';
        $items['4'] = 'ABRIL';
        $items['5'] = 'MAIO';
        $items['6'] = 'JUNHO';
        $items['7'] = 'JULHO';
        $items['8'] = 'AGOSTO';
        $items['9'] = 'SETEMBRO';
        $items['10'] = 'OUTUBRO';
        $items['11'] = 'NOVEMBRO';
        $items['12'] = 'DEZEMBRO';

        $mes->addItems($items);
        $mes->setValue( date('m') );
        
        $ano->addItems( retornaAnosemZero() );
        $ano->setValue( date('Y') );
        
        $this->form->addQuickField("Ano", $ano, 20 );
        $this->form->addQuickField("Mês", $mes, 20 );
        
        $this->form->addQuickAction('Gerar Relat&oacute;rio', new TAction(array( $this, 'onGerar')), '');
        
        $panel = new TPanelForm(700, 500);
        $panel->put($this->form, 0, 0);
        
        parent::add($panel);

    }
    
    function onGerar()
    {
    	
    	new RelatorioResumoBatidaServidorEmpresaPDF();
    	
    }

}