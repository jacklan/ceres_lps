<?php

//ini_set('display_errors', 1);
//ini_set('display_startup_erros', 1);
//error_reporting(E_ALL);

/*
 * Relatorio de Confirmacao Presenca Servidor
 * Autor: Jackson Meires
 * Data:  20/03/2017
 */

include_once 'app/lib/funcdate.php';

use Adianti\Widget\Datagrid\TDatagridTables;

class ConfirmacaoPresencaServidorList extends TPage {

    private $form;
    private $datagrid;

    public function __construct() {
        parent::__construct();

        $this->form = new \Adianti\Widget\Wrapper\TQuickForm('form');
        $this->form->setFormTitle('<font color="red" size="3px" face="Arial"><b>Confirma&ccedil;&atilde;o de Presen&ccedil;as</b></font>');

        $databatida = new TDate('databatida');
        $databatida->setValue(date('d/m/Y'));

        $this->form->addQuickField("Data Batida", $databatida, 20);

        $this->form->addQuickAction('Ver', new TAction(array($this, 'onSearch')), 'fa:search')->class = 'btn btn-info btnleft';

        $databatida->addValidation('Data Batida', new TRequiredValidator); // required field

        $this->datagrid = new TDataGridTables;

        $dgservidor = new TDataGridColumn('nome_servidor', 'Servidor', 'left', 700);
        $dgdatabatida = new TDataGridColumn('databatida', 'Data', 'left', 750);
        $dgnome_setor = new TDataGridColumn('nome_setor', 'Setor', 'left', 450);
        $dgcompareceu = new TDataGridColumn('compareceu', 'Compareceu', 'left', 350);

        $dgdatabatida->setTransformer('formatar_data');

        $this->datagrid->addColumn($dgservidor);
        $this->datagrid->addColumn($dgdatabatida);
        $this->datagrid->addColumn($dgnome_setor);
        $this->datagrid->addColumn($dgcompareceu);

        $action1 = new TDataGridAction(array($this, 'onConfirmar'));
        $action1->setLabel('Alternar entre Sim ou Não');
        $action1->setImage('icon_shuffle.png');
        $action1->setField('id');
        $action1->setFk('chefe_id');
        $action1->setDid('databatida');

        $this->datagrid->addAction($action1);

        $this->datagrid->createModel();

        $panel = new TPanelForm(1000, 500);
        $panel->put($this->form, 0, 0);
        $panel->put($this->datagrid, 0, 50);

        parent::add($panel);
    }

    function onSearch($param) {
        try {
            $data = $this->form->getData();
            $this->form->setData($data);

            if (!empty($param['did'])) {
                $obj = new StdClass;
                $obj->databatida = TDate::date2br($param['did']); // will fire change action
                $this->form->setData($obj);
            }

            TTransaction::open('pg_ceres');

            $repository = new TRepository('vw_confirmacao_presencaRecord');

            $criteria = new TCriteria;

            $criteria->add(new TFilter('chefe_id', '=', $_SESSION['servidor_id']));
            $criteria->add(new TFilter('databatida', '=', !empty($data->databatida) ? $data->databatida : TDate::date2br($param['did'])));

            $criteria->setProperty('order', 'databatida desc');

            $cadastros = $repository->load($criteria);

            $this->datagrid->clear();
            if ($cadastros) {
                foreach ($cadastros as $cadastro) {
                    $this->datagrid->addItem($cadastro);
                }
            }

            TTransaction::close();
            $this->loaded = true;
        } catch (Exception $e) { // in case of exception
            // shows the exception error message
            new TMessage('error', '<b>Error</b> ' . $e->getMessage());
            $this->form->setData($this->form->getData());
            // undo all pending operations
            TTransaction::rollback();
        }
    }

    /*
     * metodo Confirmar()
     * Salvar registro Confirmacao de comparecimento
     */

    function onConfirmar($param) {
        try {
            if (isset($param['fk'])) {
                // get the parameter $key
                $key = $param['key'];

                TTransaction::open('pg_ceres');   // open a transaction with database

                $object = new Marcacao_RelogioRecord($key);        // instantiates object 
                if ($object->compareceu != "SIM") {
                    $object->compareceu = 'SIM';
                } else if ($object->compareceu != "NAO") {
                    $object->compareceu = 'NAO';
                }

                $object->cehfe_id_compareceu = $_SESSION['servidor_id'];
                $object->store();

                TTransaction::close();           // close the transaction
                $this->datagrid->clear();

                new TMessage('info', "Altera&ccedil;&atilde;o para <b><font color=red>" . $object->compareceu . " </b></font>realizada com Sucesso! ");

                $this->onSearch($param);
            }
        } catch (Exception $e) { // in case of exception
            // shows the exception error message
            new TMessage('error', '<b>Error</b> ' . $e->getMessage());
            $this->form->setData($this->form->getData());
            // undo all pending operations
            TTransaction::rollback();
        }
    }

}
