<?php 

//ini_set('display_errors', 1);
//ini_set('display_startup_erros', 1);
//error_reporting(E_ALL);

include_once 'app/lib/funcdate.php';

class GeraMarcacaoRelogioGeral extends TPage
{

    private $form; 

    public function __construct()
    {
    	
        parent::__construct();

        $this->form = new TQuickForm('form_gera_mercacao_relogio_geral');
        $this->form->style = 'width: 40%';
        $this->form->setFormTitle('<font color="red" size="3px" face="Arial"><b>Relat&oacute;rio de Marca&ccedil;ões de Rel&oacute;gio Geral</b></font>');

        $tipo_funcionario = new TCombo('tipo_funcionario');
        $mes = new TCombo('mes'); 
        $ano = new TCombo('ano');
        $setor = new TCombo('setor_id');
        
        $items = [];
        $items['01'] = 'JANEIRO';
        $items['02'] = 'FEVEREIRO';
        $items['03'] = 'MAR&Ccedil;O';
        $items['04'] = 'ABRIL';
        $items['05'] = 'MAIO';
        $items['06'] = 'JUNHO';
        $items['07'] = 'JULHO';
        $items['08'] = 'AGOSTO';
        $items['09'] = 'SETEMBRO';
        $items['10'] = 'OUTUBRO';
        $items['11'] = 'NOVEMBRO';
        $items['12'] = 'DEZEMBRO';

        $mes->addItems($items);
        $mes->setValue(date('m'));

        $ano->addItems(retornaAnosemZero());
        $ano->setValue(date('Y'));
        
        $items = [];
        $items['EFETIVO'] = 'EFETIVO';
        $items['BOLSISTA FUNCITERN'] = 'BOLSISTA FUNCITERN';
        $items['BOLSISTA FGD'] = 'BOLSISTA FGD';
        $items['CONVENIADO'] = 'CONVENIADO';
        $items['COMISSINADO'] = 'COMISSINADO';
        $items['ESTAGIARIO'] = 'ESTAGIARIO';
        
        $tipo_funcionario->addItems($items);
        
        //---------------------
        TTransaction::open('pg_ceres');
        
        $repository = new TRepository('SetorRecord');
        
        $criteria = new TCriteria;
        
        $criteria->setProperty('order', 'nome');
        
        $criteria->add(new TFilter('empresa_id', '=', $_SESSION['empresa_id']));
        $criteria->add(new TFilter('situacao', '=', 'ATIVO'));

        $dados = $repository->load($criteria);
        
        $items = [];
        foreach( $dados as $dado ) {
        	
        	$items[$dado->id] = $dado->nome;
        	
        }
        
        TTransaction::close();
        
        $setor->addItems($items);
        
        //---------------------
        
        $setor->addValidation('Tipo de Funcion&aacute;rio', new TRequiredValidator);
        $tipo_funcionario->addValidation('Tipo de Funcion&aacute;rio', new TRequiredValidator);
        $ano->addValidation('Ano', new TRequiredValidator);
        $mes->addValidation('M&ecirc;s', new TRequiredValidator);
        
        $this->form->addQuickField('Setor <font color=red><b>*</b></font>', $setor, 80);
        $this->form->addQuickField('Tipo de Funcion&aacute;rio <font color=red><b>*</b></font>', $tipo_funcionario, 80);
        $this->form->addQuickField('Ano <font color=red><b>*</b></font>', $ano, 80);
        $this->form->addQuickField('M&ecirc;s <font color=red><b>*</b></font>', $mes, 80);
        
        $this->form->addQuickAction('Gerar Relat&oacute;rio', new TAction(array($this, 'onGenerate')), 'fa:file-pdf-o');

        $panel = new TPanelForm(700, 500);
        $panel->put($this->form, 0, 0);

        parent::add($panel);

    }

    function onGenerate()
    {

        try {
        	
            $this->form->validate();

            new RelatorioBatidasPontoServidorGeralPDF();
            
        }catch( Exception $e ) {
        	
            new TMessage( 'error', $e->getMessage() );
            
        }
        
    }
    
}