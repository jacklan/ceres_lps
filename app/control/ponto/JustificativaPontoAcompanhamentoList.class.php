<?php

include_once 'app/lib/funcdate.php';

use Adianti\Database\TFilter;
use Adianti\Widget\Datagrid\TDatagridTables;

class JustificativaPontoAcompanhamentoList extends TPage{
    private $form;
    private $datagrid;

    public function __construct(){
        parent::__construct();

        $this->form = new TForm('form_JustificativaPontoAcompanhamento');

        $panel = new TPanelForm(900, 200);
        $this->form->add($panel);

        $titulo = new TLabel('Acompanhamento das Justificativas do Ponto');
        $titulo->setFontFace('Arial');
        $titulo->setFontColor('red');
        $titulo->setFontSize(12);

        $panel->put($titulo,$panel->getColuna(),$panel->getLinha());

        $this->datagrid = new TDataGridTables;

        $dgdatabatida = new TDataGridColumn('databatida',    'Data',    'left', 150);
        $dghorajornada = new TDataGridColumn('hora_jornada',    'Jornada',    'left', 150);
        $dghoratotal = new TDataGridColumn('horatotal',    'Trabalhado',    'left', 150);
        $dgsituacao = new TDataGridColumn('situacaojustificativa', 'Situa&ccedil;&atilde;o', 'left', 300);
        $dgpdf = new TDataGridColumn('justificativapdf',    'Justificativa PDF',    'left', 300);

        $dgdatabatida->setTransformer('formatar_data');
        
        $this->datagrid->addColumn($dgdatabatida);
        $this->datagrid->addColumn($dghorajornada);
        $this->datagrid->addColumn($dghoratotal);
        $this->datagrid->addColumn($dgsituacao);
        $this->datagrid->addColumn($dgpdf);

        $action1 = new TDataGridAction(array($this, 'onOrdenar'));
        $action1->setLabel('Justificar');
        $action1->setImage('ico_valid.png');
        $action1->setField('id');


        $this->datagrid->addAction($action1);
        
        $this->datagrid->createModel();
        $panel = new TPanelForm(900, 500);
        $panel->put($this->form, 0, 0);
        $panel->put($this->datagrid, 150, 100);

        parent::add($panel);
    }

    function onReload(){
        TTransaction::open('pg_ceres');
        $repository = new TRepository('vw_justificativa_acompanhamentoRecord');

        $criteria = new TCriteria;
        $criteria->add(new TFilter('servidor_id', '=', $_SESSION['servidor_id']));

        $criteria->setProperty('order', 'databatida DESC');

        $cadastros = $repository->load($criteria);
        $this->datagrid->clear();

        if ($cadastros) {
            foreach ($cadastros as $cadastro) {
                $this->datagrid->addItem($cadastro);
            }
        }
        TTransaction::close();
        $this->loaded = true;
    }

    function show(){

        $this->onReload();
        parent::show();

    }
    function onOrdenar(){
    	
    }
}