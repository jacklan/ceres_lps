<?php 
//ini_set('display_errors', 1);
//ini_set('display_startup_erros', 1);
//error_reporting(E_ALL);

include_once 'app/lib/funcdate.php';

use Adianti\Widget\Datagrid\TDatagridTables;

class GerarEscala extends TPage{

    private $form; 
    private $datagrid;

    public function __construct(){
        parent::__construct();

        $this->form = new TQuickForm('form_gerar_escala');
        $this->form->style = 'width: 40%';
        $this->form->setFormTitle('<font color="red" size="3px" face="Arial"><b>Gerar Escala</b></font>');

        
        $id = new THidden('id');
        $servidor_id = new THidden('servido_id');

        $empresa_id = new TCombo('empresa_id');
        $setor = new TCombo('setor_id');

        $dia_inicio = new TDate('datainicio');            
        $dia_fim = new TDate('datafim');

        $datageracao = new THidden('datageracao');  

        TTransaction::open('pg_ceres');

        //$setor = new TDBCombo('setor','pg_ceres','SetorRecord','id','nome','nome'); 

        $empresa_id = new TDBCombo('empresa_id','pg_ceres','EmpresaRecord','id','nome','nome');    
        
        TTransaction::close(); 

        $empresa_id->addValidation('Empresa', new TRequiredValidator);
        $dia_inicio->addValidation('Dia Inicio', new TRequiredValidator);
        $dia_fim->addValidation('Dia Fim', new TRequiredValidator);
        

        $this->form->addQuickField('Empresa <font color=red><b>*</b></font>', $empresa_id, 90);
        $this->form->addQuickField('Dia Inicio <font color=red><b>*</b></font>', $dia_inicio, 50);
        $this->form->addQuickField('Dia Fim <font color=red><b>*</b></font>', $dia_fim, 50);

        $this->form->addQuickAction('Salvar', new TAction(array($this, 'onSave')), 'ico_save.png')->class = 'btn btn-info btnleft';

        $panel = new TPanelForm(700, 500);
        $panel->put($this->form, 0, 0);

        parent::add($panel);

    }

    function onSave(){

        try
        {
            $this->form->validate();
            
       
            // inicia transacao com o banco 'pg_ceres'
            TTransaction::open('pg_ceres');

            // obtem os dados no formulario em um objeto CarroRecord
            $object = $this->form->getData('LogEscalaRecord');

            //lanca o default
            $object->servidor_id = $_SESSION['usuario'];
            $object->datageracao = date("d/m/Y H:i:s");
            $object->empresa_id = $_SESSION['empresa_id'];
            
            //Validade
            $this->form->validate();

            // armazena o objeto
            $object->store();
                
            TTransaction::close();

            // exibe um dialogo ao usuario
            new TMessage("info", "Registro salvo com sucesso!");
            TApplication::gotoPage('GerarEscala', 'onReload'); // reload
            
        }catch( Exception $e ) 
        { 
            
            // em caso de exce��o
            // exibe a mensagem gerada pela exce��o
            new TMessage('error', $e->getMessage());
            
            // desfaz todas altera��es no banco de dados
            TTransaction::rollback();
            
        }
    }

     function onReload(){
     }



 
}
?>