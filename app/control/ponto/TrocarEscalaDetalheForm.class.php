<?php


class TrocarEscalaDetalheForm extends TWindow
{
    private $form;

    public function __construct()
    {
        parent::__construct();
        parent::setTitle( "Cadastro de Troca de Escala" );
        parent::setSize( 0.600, 0.800 );

        $this->form = new TQuickForm('form_trocar-escala');
        //$this->form = new BootstrapFormBuilder( "form_trocar-escala" );
        //$this->form->class = "tform";

        $id           = new THidden( "id" );
        $escalaorigem_id = new THidden('escalaoriginal_id');
        $escaladestino_id = new THidden('escaladestino_id');
        $servidororigem_id = new THidden('servidororiginal_id');
        $servidordestino_id = new THidden('servidordestino_id');
        $escalaorigem = new TEntry('escalaorigem');
        $escaladestino = new TEntry('escaladestino');
        $servidororigem = new TEntry('servidororigem');
        $servidordestino = new TEntry('servidordestino');
        $servidororigem_matricula = new TEntry('servidororigem_matricula');
        $servidordestino_matricula = new TEntry('servidordestino_matricula');


        $escalaorigem->setSize( "38%" );
        $escalaorigem->setEditable( false );
        $escaladestino->setSize( "38%" );
        $escaladestino->setEditable( false );
        $servidororigem->setSize( "60%" );
        $servidororigem->setEditable( false );
        $servidordestino->setSize( "60%" );
        $servidordestino->setEditable( false );
        $servidororigem_matricula->setSize( "38%" );
        $servidororigem_matricula->setEditable( false );
        $servidordestino_matricula->setSize( "38%" );
        $servidordestino_matricula->setEditable( false );


        $this->form->addQuickFields(new TLabel ('Servidor Origem: <font color=red><b>*</b></font>'), array($servidororigem));
        $this->form->addQuickFields(new TLabel ('Matricula: <font color=red><b>*</b></font>'), array($servidororigem_matricula));
        $this->form->addQuickFields(new TLabel ('Servidor Destino: <font color=red><b>*</b></font>'), array($servidordestino));
        $this->form->addQuickFields(new TLabel ('Matricula: <font color=red><b>*</b></font>'), array($servidordestino_matricula));
        $this->form->addQuickFields(new TLabel ('Escala Origem: <font color=red><b>*</b></font>'), array($escalaorigem));
        $this->form->addQuickFields(new TLabel ('Escala Destino: <font color=red><b>*</b></font>'), array($escaladestino));

        $this->form->addQuickFields(new TLabel (''), array($id));
        $this->form->addQuickFields(new TLabel (''), array($escalaorigem_id));
        $this->form->addQuickFields(new TLabel (''), array($escaladestino_id));
        $this->form->addQuickFields(new TLabel (''), array($servidororigem_id));
        $this->form->addQuickFields(new TLabel (''), array($servidordestino_id));


        $this->form->addQuickAction('Salvar', new TAction(array($this, 'onSave')), 'fa:floppy-o');
        /*
        $this->form->addFields( [ $id ] );
        $this->form->addFields( [ $escalaorigem_id ] );
        $this->form->addFields( [ $escaladestino_id ] );
        $this->form->addFields( [ $servidororigem_id ] );
        $this->form->addFields( [ $servidordestino_id ] );

        $this->form->addAction( "Salvar", new TAction( [ $this, "onSave" ] ), "fa:floppy-o" );*/

        $panel = new TPanelForm(700, 500);
        $panel->put($this->form, 0, 0);
        $panel->put($this->datagrid, 150, 115);

        // adiciona a tabela a pagina
        parent::add($panel);

    }


    public function onSave( $param = null)
    {

            try {
                TTransaction::open('pg_ceres');
                $object = $this->form->getData('ServidorEscalaTrocaRecord');
                $object->datatroca = date("d/m/Y H:i:s");

                if ($object->servidororigem == $object->servidordestino) {
                    throw new Exception('Impossível trocar escala com o servidor de origem');
                }

                if($object->escalaorigem == $object->escaladestino){
                    throw new Exception('Impossível trocar escala para o mesmo dia');
                }

                unset($object->escalaorigem);
                unset($object->escaladestino);
                unset($object->servidororigem);
                unset($object->servidordestino);
                unset($object->servidororigem_matricula);
                unset($object->servidordestino_matricula);

                $object->store();

                TTransaction::close();

                $action_ok = new TAction(['TrocarEscalaList', "onReload"]);

                new TMessage("info", "Registro salvo com sucesso!", $action_ok);
                }

        catch (Exception $e) {

            new TMessage('error', $e->getMessage());

            TTransaction::rollback();

            }
        }


    public function onEdit( $param = null )
    {
        try {

            /*
            Object = id da escala selecionada de origem
            Object2 = id da escala selecionada de destino
            Object3 = seleciona servidor de origem de acordo com servidor_id cadastrado na escala
            Object4 = seleciona servidor de destino de acordo com servidor_id cadastrado na escala

            No início da tela de trocar escala é passado o id selecionado, quando seleciono a escala
            do servidor que quero trocar horário, também salvo na url esse valor, sendo fk = id origem e
            key = id de destino da escala seleciona.
            Após, neste método edit, pego esses valores e faço os selects no banco, pelos ids da url e
            depois set os valores no form, lembrando, que no onsave, limpo os valores que não são necessários.
            */

            TTransaction::open( "pg_ceres" );

            $object = new ServidorEscalaRecord($param['fk']);

            $object2 = new ServidorEscalaRecord($param['key']);

            $object3 = new ServidorRecord($object->servidor_id);

            $object4 = new ServidorRecord($object2->servidor_id);

            $obj = new stdClass();

            $obj->escalaorigem = TDate::date2br($object->datainicio);
            $obj->escaladestino = TDate::date2br($object2->datainicio);
            $obj->escalaoriginal_id = $param['fk'];
            $obj->escaladestino_id = $param['key'];

            $obj->servidororigem = $object3->nome;
            $obj->servidororigem_matricula = $object3->matricula;
            $obj->servidordestino = $object4->nome;
            $obj->servidordestino_matricula = $object4->matricula;
            $obj->servidororiginal_id = $object->servidor_id;
            $obj->servidordestino_id = $object2->servidor_id;


            $this->form->setData($obj);

            TTransaction::close();


        } catch ( Exception $ex ) {

            TTransaction::rollback();

            new TMessage( "error", "Ocorreu um erro ao tentar carregar o registro para edição!<br><br>" . $ex->getMessage() );
        }
    }

}