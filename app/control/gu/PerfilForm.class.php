<?php
/*
 * classe PerfilForm
 * Cadastro de Perfil: Contem o formularo
 */

 use Adianti\Database\TFilter1;


class PerfilForm extends TPage
{
    private $form;     // formulario de cadastro
    
    /*
     * metodo construtor
     * Cria a pagina do formulario
     */
    public function __construct()
    {
        parent::__construct();

        $this->form = new TQuickForm;
        $this->form->class = 'form_Perfil';
        $this->form->setFormTitle('<font color="red" size="3" face="Arial"><b>Formul&aacute;rio de Perfil</b></font>');
        
        // cria os campos do formulario
        $codigo     = new THidden('id');
        $nome  = new TEntry('nome');
        $modulo_id  = new TCombo('modulo_id');

        $usuarioalteracao = new THidden('usuarioalteracao');
        $dataalteracao = new THidden('dataalteracao');
		
		// cria um rotulo para o titulo
        $titulo = new TLabel('<div style="position:floatval; width: 200px;"> <b>* Campo obrigatorio</b></div>');
        $titulo->setFontFace('Arial');
        $titulo->setFontColor('red');
        $titulo->setFontSize(10);

        //cria a colecao da tabela estrangeira
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $repository = new TRepository('ModuloRecord');
        // cria um criterio de selecao, ordenado pelo id
        $criteria = new TCriteria;
        $criteria->setProperty('order', 'nome');
        // carrega todos os objetos
        $collection = $repository->load($criteria);
        //adiciona os objetos no combo
        foreach ($collection as $object){
            $items[$object->id] = $object->nome;
        }

        // adiciona as opcoes na combo
        $modulo_id->addItems($items);

        // finaliza a transacao
        TTransaction::close();

        // define os tamanhos dos campos
		$this->form->addQuickField(null, $usuarioalteracao, 1);
        $this->form->addQuickField(null, $dataalteracao, 1);
        $this->form->addQuickField(null, $codigo, 10);
        $this->form->addQuickField('Nome <font color=red><b>*</b></font>', $nome, 40);
        $this->form->addQuickField('Módulo <font color=red><b>*</b></font>', $modulo_id, 40);
		$this->form->addQuickField(null, $titulo, 50);
       
        $nome->setProperty('required', 'required');
        $nome->setProperty('placeholder', 'Preencha o nome');

        // define a acao do botao
        $this->form->addQuickAction('Salvar', new TAction(array($this, 'onSave')), 'ico_save.png');
	    $this->form->addQuickAction('Voltar', new TAction(array('PerfilList', 'onReload')), 'ico_datagrid.gif');

        parent::add($this->form);
    }
   
    /*
     * metodo onSave()
     * Executada quando o usuario clicar no botao salvar do formulario
     */
    function onSave()
    {
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // obtem os dados no formulario em um objeto Record
        $cadastro = $this->form->getData('PerfilRecord');

		 //lanca o default
        $cadastro->usuarioalteracao = $_SESSION['usuario'];
        $cadastro->dataalteracao = date("d/m/Y H:i:s");
		
        //antes de armazenar verifica se algum campo eh requerido e nao foi informado
        $dados = $cadastro->toArray();

        $msg = '';
        $icone = 'info';

        if (empty ($dados['nome'])){
           $msg .= 'O Nome deve ser informado.';
        }

        if (empty ($dados['modulo_id'])){
           $msg .= 'O M&oacute;dulo deve ser informado.';
        }

        try{

            if ($msg == ''){
              // armazena o objeto
              $cadastro->store();
              $msg = 'Dados armazenados com sucesso';

              // finaliza a transacao
              TTransaction::close();

            }else{
                $icone = 'error';
            }
            
            if ($icone == 'error'){
                // exibe mensagem de erro
                new TMessage($icone, $msg);
				$this->form->setData($cadastro);   // fill the form with the active record data
            }else{
				
               // exibe um dialogo ao usuario
                new TMessage("info", "Registro salvo com sucesso!");
				TApplication::gotoPage('PerfilList','onReload'); // reload
				$this->form->setData($cadastro);   // fill the form with the active record data
				
            }
        }
        catch (Exception $e) // em caso de exce��o
        {
            // exibe a mensagem gerada pela exce��o
            new TMessage('error', $e->getMessage());
            // desfaz todas altera��es no banco de dados
            TTransaction::rollback();
			
        }

    }
    
    
    /*
     * metodo onEdit()
     * Edita os dados de um registro
     */
    function onEdit($param)
    {
		
		try {
            if (isset($param['key'])) {

                // get the parameter $key
                $key = $param['key'];

                TTransaction::open('pg_ceres');   // open a transaction with database 'samples'

                $object = new PerfilRecord($key);        // instantiates object City

                $this->form->setData($object);   // fill the form with the active record data

                TTransaction::close();           // close the transaction
            } else {
                $this->form->clear();
            }
        } catch (Exception $e) { // in case of exception
            // shows the exception error message
            new TMessage('error', '<b>Error</b> ' . $e->getMessage());
            // undo all pending operations
            TTransaction::rollback();
        }
		
    }

}
?>