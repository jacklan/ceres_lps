<?php
/*
 * classe TerritorioForm
 * Cadastro de Territorio: Contem o formularo
 */
include_once 'app/lib/funcdate.php';

class TerritorioForm extends TPage
{
    private $form;     // formulario de cadastro
    
    /*
     * metodo construtor
     * Cria a pagina do formulario
     */
    public function __construct()
    {
        parent::__construct();

		// instancia um formulario
		$this->form = new TQuickForm;
        $this->form->class = 'form_Territorio';
        $this->form->setFormTitle('<font color="red" size="3" face="Arial"><b>Formul&aacute;rio de Território</b></font>');

        // cria os campos do formulario
        $codigo = new THidden('id');
        $nome  = new TEntry('nome');
        $cormapa = new TEntry('cormapa');

        $usuarioalteracao = new THidden('usuarioalteracao');
        $dataalteracao = new THidden('dataalteracao');
		
		// cria um rotulo para o titulo
        $titulo = new TLabel('<div style="position:floatval; width: 200px;"> <b>* Campo obrigatorio</b></div>');
        $titulo->setFontFace('Arial');
        $titulo->setFontColor('red');
        $titulo->setFontSize(10);

		// define os campos
		$this->form->addQuickField(null, $codigo, 10);
		$this->form->addQuickField('Nome <font color=red><b>*</b></font>', $nome, 40);
		$this->form->addQuickField('Cor Mapa', $cormapa, 10);
		$this->form->addQuickField(null, $titulo, 50);
		$this->form->addQuickField(null, $usuarioalteracao, 1);
        $this->form->addQuickField(null, $dataalteracao, 1);
		
        // define os tamanhos dos campos
        $nome->setProperty('required', 'required');
        $nome->setProperty('placeholder', 'Preencha o nome');
        $cormapa->setProperty('required', 'required');
        $cormapa->setProperty('placeholder', 'Ex.: #FF0011');

		 // cria um botao de acao
		$this->form->addQuickAction('Salvar', new TAction(array($this, 'onSave')), 'ico_save.png');
		$this->form->addQuickAction('Voltar', new TAction(array('TerritorioList', 'onReload')), 'ico_datagrid.gif');

        // adiciona a tabela a pagina
        parent::add($this->form);
    }
  
    /*
     * metodo onSave()
     * Executada quando o usuario clicar no botao salvar do formulario
     */
    function onSave()
    {
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // obtem os dados no formulario em um objeto CarroRecord
        $cadastro = $this->form->getData('TerritorioRecord');

		 //lanca o default
        $cadastro->usuarioalteracao = $_SESSION['usuario'];
        $cadastro->dataalteracao = date("d/m/Y H:i:s");
		
        //antes de armazenar verifica se algum campo eh requerido e nao foi informado
        $dados = $cadastro->toArray();

        $msg = '';
        $icone = 'info';

        if (empty ($dados['nome'])){
           $msg .= 'O Nome deve ser informado.';
        }

        try{

            if ($msg == ''){
              // armazena o objeto
              $cadastro->store();
              $msg = 'Dados armazenados com sucesso';

              // finaliza a transacao
              TTransaction::close();

            }else{
                $icone = 'error';
            }
            
            if ($icone == 'error'){
                // exibe mensagem de erro
                new TMessage($icone, $msg);
				
				$this->form->setData($cadastro);
				
            }else{
  
				// exibe um dialogo ao usuario
                new TMessage("info", "Registro salvo com sucesso!");
				TApplication::gotoPage('TerritorioList','onReload'); // reload
				
            }
        }
        catch (Exception $e) // em caso de exce��o
        {
            // exibe a mensagem gerada pela exce��o
            new TMessage('error', $e->getMessage());
            // desfaz todas altera��es no banco de dados
            TTransaction::rollback();
        }

    }
    
    /*
     * metodo onEdit()
     * Edita os dados de um registro
     */
    function onEdit($param)
    {
		
		try {
            if (isset($param['key'])) {

                // get the parameter $key
                $key = $param['key'];

                TTransaction::open('pg_ceres');   // open a transaction with database 'samples'

                $object = new TerritorioRecord($key);        // instantiates object City

                $this->form->setData($object);   // fill the form with the active record data

                TTransaction::close();           // close the transaction
            } else {
                $this->form->clear();
            }
        } catch (Exception $e) { // in case of exception
            // shows the exception error message
            new TMessage('error', '<b>Error</b> ' . $e->getMessage());
            // undo all pending operations
            TTransaction::rollback();
        }
		
    }
}
?>