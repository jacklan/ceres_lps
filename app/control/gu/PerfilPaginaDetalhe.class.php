<?php

//ini_set('display_errors', 1);
//ini_set('display_startup_erros', 1);
//error_reporting(E_ALL);

/*
 * classe PerfilPaginaDetalhe
 * Cadastro de PerfilPagina: Contem a listagem e o formulario de busca
 */

 use Adianti\Database\TFilter1;
use Adianti\Widget\Datagrid\TDatagridTables;

class PerfilPaginaDetalhe extends TPage
{
    private $form;     // formulario de cadastro
    private $datagrid; // listagem

    /*
     * metodo construtor
     * Cria a pagina, o formulario e a listagem
     */
    public function __construct()
    {
        parent::__construct();

        $this->form = new TQuickForm;
        $this->form->class = 'form_Perfil';
        $this->form->setFormTitle('<font color="red" size="3" face="Arial"><b>Detalhe das Paginas do Perfil</b></font>');

        // cria os campos do formulario
        $codigo     = new THidden('id');
        $pagina_id  = new TCombo('pagina_id');

		$usuarioalteracao = new THidden('usuarioalteracao');
        $dataalteracao = new THidden('dataalteracao');
		
		// cria um rotulo para o titulo
        $titulo = new TLabel('<div style="position:floatval; width: 200px;"> <b>* Campo obrigatorio</b></div>');
        $titulo->setFontFace('Arial');
        $titulo->setFontColor('red');
        $titulo->setFontSize(10);
		
        //cria a colecao da tabela estrangeira
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $repository = new TRepository('PaginaRecord');
        // cria um criterio de selecao, ordenado pelo id
        $criteria = new TCriteria;
        $criteria->setProperty('order', 'nome');
        // carrega os objetos de acordo com o criterio
        $cadastros = $repository->load($criteria);

        //adiciona os objetos no combo
        foreach ($cadastros as $object){
            $items[$object->id] = $object->nome." / ".$object->nome_grupomenu." / ".$object->nome_modulo." / ".$object->arquivo;
        }

        // adiciona as opcoes na combo
        $pagina_id->addItems($items);
		
		//$perfil_id->setValue(1);
		$pagina_id->setProperty('required', 'required');
		
		$pagina_id->addValidation('pagina_id',new TRequiredValidator);
        // finaliza a transacao
        TTransaction::close();

        //cria a colecao da tabela estrangeira do Cabecalho
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $cadastro = new PerfilRecord($_GET['fk']);
        // cria um criterio de selecao, ordenado pelo id do perfil
        //adiciona os objetos na tela com as informacoes do perfil
        if ($cadastro){
            $nomeperfil = new TLabel($cadastro->nome);
            $nomemodulo = new TLabel($cadastro->nome_modulo);
        }
        // finaliza a transacao
        TTransaction::close();

        //coloca o valor do relacionamento
        $perfil_id = new THidden('perfil_id');
        $perfil_id->setValue($_GET['fk']);
			

		// define os tamanhos dos campos
		$this->form->addQuickField(null, $usuarioalteracao, 10);
        $this->form->addQuickField(null, $dataalteracao, 10);
        $this->form->addQuickField("Perfil", $nomeperfil, 200);
		$this->form->addQuickField("Modulo", $nomemodulo, 200);
		$this->form->addQuickField(null, $codigo, 10);
		$this->form->addQuickField(null, $perfil_id, 10);
        $this->form->addQuickField('Pagina <font color=red><b>*</b></font>', $pagina_id, 75);
		$this->form->addQuickField(null, $titulo, 50);
		
        $pagina_id->setProperty('required', 'required');
	
	  // cria um botao de acao (cadastrar)
       $new_button = new TButton('novo');
	   
	   
	 //  $new_button->addFunction("alert('action 1');");
//$new_button->onclick = "Adianti.waitMessage = 'Carregando';__adianti_post_data('form_Aluno', 'class=AlunoForm&method=onSave');return false;";
		// define a acao do botao cadastrar
     // $new_button->setAction(new TAction(array($this, 'onSave'), 'Novo');
	//	$this->form->addQuickField(null, $new_button, 50);
        // define a acao do botao
	//	$new_button->setProperty('onclick', "Adianti.waitMessage = 'Carregando';__adianti_post_data('form_Aluno', 'class=AlunoForm&method=onSave');return false;");
        $this->form->addQuickAction('Salvar', new TAction(array($this, 'onSave')), 'ico_save.png');
	  //	$this->form->addQuickField(null, $new_button, 50);
	    $this->form->addQuickAction('Voltar', new TAction(array('PerfilList', 'onReload')), 'ico_datagrid.gif');
     
        // instancia objeto DataGrid
        $this->datagrid = new TDatagridTables;
     
        // instancia as colunas da DataGrid
        $dgcodigo   = new TDataGridColumn('id',  'C&oacute;digo',  'center', 70);
        $dgnome     = new TDataGridColumn('nome_pagina',    'P&aacute;gina',    'left',1200);
        $dgarquivo     = new TDataGridColumn('nome_arquivo',    'Arquivo',    'left', 400);
        $dgnomegrupo = new TDataGridColumn('nome_grupomenu', 'Grupo Menu', 'left', 100);

        // adiciona as colunas a DataGrid
        //$this->datagrid->addColumn($dgcodigo);
        $this->datagrid->addColumn($dgnome);
        $this->datagrid->addColumn($dgarquivo);
        $this->datagrid->addColumn($dgnomegrupo);
        
        // instancia duas acoes da DataGrid
        $action1 = new TDataGridAction(array($this, 'onEdit'));
        $action1->setLabel('Editar');
        $action1->setImage('ico_edit.png');
        $action1->setField('id');
        $action1->setFk('perfil_id');
        
        $action2 = new TDataGridAction(array($this, 'onDelete'));
        $action2->setLabel('Deletar');
        $action2->setImage('ico_delete.png');
        $action2->setField('id');
        $action2->setFk('perfil_id');
        
        // adiciona as acoes a DataGrid
        $this->datagrid->addAction($action1);
        $this->datagrid->addAction($action2);
        
        // cria o modelo da DataGrid, montando sua estrutura
        $this->datagrid->createModel();

        // monta a paina atraves de uma tabela
        $panel = new TPanelForm(700, 500);
        $panel->put($this->form, 0, 0);
        $panel->put($this->datagrid, 150, 170);

        // adiciona a tabela a pagina
        parent::add($panel);
    }
    
    /*
     * metodo onReload()
     * Carrega a DataGrid com os objetos do banco de dados
     */
    function onReload()
    {
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instancia um repositorio da Classe
        $repository = new TRepository('PerfilPaginaRecord');

        // cria um criterio de selecao
        $criteria = new TCriteria;
		$criteria->setProperty('order', 'id DESC');
		
        //filtra pelo campo selecionado pelo usuario
        $criteria->add(new TFilter('perfil_id', '=', $_GET['fk']));
		

        // carrega os objetos de acordo com o criterio
        $cadastros = $repository->load($criteria);

        $this->datagrid->clear();
        if ($cadastros)
        {
            // percorre os objetos retornados
            foreach ($cadastros as $cadastro)
            {
                // adiciona o objeto na DataGrid
                $this->datagrid->addItem($cadastro);
            }
        }

        // finaliza a transacao
        TTransaction::close();
        $this->loaded = true;
    }
    
    
    /*
     * metodo onDelete()
     * Executada quando o usuario clicar no botao excluir da datagrid
     * Pergunta ao usuario se deseja realmente excluir um registro
     */
    function onDelete($param)
    {
        // obtem o parametro $key
        $key=$param['key'];
        
        // define duas acoes
        $action1 = new TAction(array($this, 'Delete'));
        
        // define os parametros de cada acao
        $action1->setParameter('key', $key);

        //encaminha a chave estrangeira
        $action1->setParameter('fk', $_GET['fk']);
		
        // exibe um dialogo ao usuario
        new TQuestion('Deseja realmente excluir o registro ?', $action1, $action2);
		
    }
    
    /*
     * metodo Delete()
     * Exclui um registro
     */
    function Delete($param)
    {
        // obtem o parametro $key
        $key=$param['key'];
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instanicia objeto Record
        $cadastro = new PerfilPaginaRecord($key);

        try{
            // deleta objeto do banco de dados
            $cadastro->delete();
			
			// exibe um dialogo ao usuario
            new TMessage("info", "Registro deletado com sucesso!");

            // finaliza a transacao
            TTransaction::close();
        }
        catch (Exception $e) // em caso de exce��o
        {
            // exibe a mensagem gerada pela exce��o
            new TMessage('error', $e->getMessage());
            // desfaz todas altera��es no banco de dados
            TTransaction::rollback();
        }

        // re-carrega a datagrid
        $this->onReload();
		
    }
    
    /*
     * metodo show()
     * Exibe a pagina
     */
    function show()
    {
		
        // carrega os dados no datagrid
        $this->onReload();
        //chama o metodo show da super classe
        parent::show();

    }

    /*
     * metodo onSave()
     * Executada quando o usuario clicar no botao salvar do formulario
     */
    function onSave()
    {
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // obtem os dados no formulario em um objeto Record da Classe
        $cadastro = $this->form->getData('PerfilPaginaRecord');

		 //lanca o default
        $cadastro->usuarioalteracao = $_SESSION['usuario'];
        $cadastro->dataalteracao = date("d/m/Y H:i:s");
		
        //antes de armazenar verifica se algum campo eh requerido e nao foi informado
        $dados = $cadastro->toArray();

        $msg = '';
        $icone = 'info';

        if (empty ($dados['perfil_id'])){
           $msg .= 'O Perfil deve ser informado.';
        }
		/*
        if (empty ($dados['pagina_id'])){
           $msg .= 'A Pagina deve ser informada.';
        }
		*/

        try{

            if ($msg == ''){
              // armazena o objeto
              $cadastro->store();
              $msg = 'Dados armazenados com sucesso';

              // finaliza a transacao
              TTransaction::close();

            }else{
                $icone = 'error';
            }

            if ($icone == 'error'){
                // exibe mensagem de erro
                new TMessage($icone, $msg);
				// lanca os dados no formulario
				//$this->form->setData($cadastro);
				var_dump($cadastro);
				// carrega os dados no datagrid
				//$this->onReload();
				
            }else{
                
				$param = array();
				$param['fk'] = $dados['perfil_id'];
				
				// exibe um dialogo ao usuario
                new TMessage("info", "Registro salvo com sucesso!");
				TApplication::gotoPage('PerfilPaginaDetalhe','onReload', $param); // reload
				$this->form->setData($cadastro);   // fill the form with the active record data
				
            }
        }
        catch (Exception $e) // em caso de exce��o
        {
            // exibe a mensagem gerada pela exce��o
            new TMessage('error', $e->getMessage());
            // desfaz todas altera��es no banco de dados
            TTransaction::rollback();
			
			// lanca os dados no formulario
			$this->form->setData($cadastro);
			
        }

    }

    /*
     * metodo onEdit()
     * Edita os dados de um registro
     */
    function onEdit($param)
    {
		
		try {
            if (isset($param['key'])) {

                // get the parameter $key
                $key = $param['key'];

                TTransaction::open('pg_ceres');   // open a transaction with database 'samples'

                $object = new PerfilPaginaRecord($key);        // instantiates object City

                $this->form->setData($object);   // fill the form with the active record data

                TTransaction::close();           // close the transaction
            } else {
                $this->form->clear();
            }
        } catch (Exception $e) { // in case of exception
            // shows the exception error message
            new TMessage('error', '<b>Error</b> ' . $e->getMessage());
            // undo all pending operations
            TTransaction::rollback();
        }
		
    }

}
?>