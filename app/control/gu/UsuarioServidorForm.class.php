<?php

/*
 * classe UsuarioServidorForm
 * Cadastro de UsuarioServidor: Contem o formularo
 */

//ini_set('display_errors', 1);
//ini_set('display_startup_erros', 1);
//error_reporting(E_ALL);


class UsuarioServidorForm extends TPage {

    private $form;     // formulario de cadastro

    /*
     * metodo construtor
     * Cria a pagina do formulario
     */

    public function __construct() {
        parent::__construct();

        // instancia um formulario
        $this->form = new \Adianti\Widget\Wrapper\TQuickForm;
        //  $this->form->class = 'form_UsuarioServidor';
        $this->form->setFormTitle('<font color="red" size="3" face="Arial"><b>Formul&aacute;rio do Usuario Servidor</b></font>');

        // cria os campos do formulario
        $codigo = new THidden('id');
        /* ------------------ MultiSearch ------------------ */

        /* ---------- cria um criterio de selecao ---------- */
        $criteria_servidor = new TCriteria;
        $criteria_servidor->add(new TFilter('situacao', '=', 'EM ATIVIDADE'), TExpression::OR_OPERATOR);
        $criteria_servidor->add(new TFilter('situacao', '=', 'A DISPOSICAO'), TExpression::OR_OPERATOR);

        $criteria3 = new TCriteria;
        if ($_SESSION['empresa_id'] == 1) {
            $criteria3->add(new TFilter('empresa_id', '=', $_SESSION['empresa_id']));
        }
        $criteria3->add($criteria_servidor);
        /* ---------- fim criacao criterio de selecao ---------- */

        /* --------- montar campo --------- */
        $servidor_id = new TDBMultiSearch('servidor_id', 'pg_ceres', 'vw_servidor_multisearchRecord', 'servidor_id', 'nome_matricula_servidor', 'nome_matricula_servidor', $criteria3);
        $servidor_id->style = "text-transform: uppercase;";
        $servidor_id->setProperty('placeholder', 'Nome ou Matricula ou CPF');
        $servidor_id->setMinLength(1);
        $servidor_id->setMaxSize(1);
        /*  --------- fim montar campo --------- */

        /* ------------------ MultiSearch Servidor ------------------ */
        $login = new TEntry('login');
        $senha = new TPassword('senha');
        $expira = new TCombo('expira');
        $dataexpira = new TDate('dataexpira');
        $ativo = new TCombo('ativo');
        $empresa_id = new TCombo('empresa_id');
        $regional_id = new TCombo('regional_id');
        $abrangencia = new TCombo('abrangencia');

        $usuarioalteracao = new THidden('usuarioalteracao');
        $dataalteracao = new THidden('dataalteracao');

        // cria um rotulo para o titulo
        $titulo = new TLabel('<div style="position:floatval; width: 200px;"> <b>* Campo obrigatorio</b></div>');
        $titulo->setFontFace('Arial');
        $titulo->setFontColor('red');
        $titulo->setFontSize(10);

        //cria a colecao da tabela estrangeira
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $repository = new TRepository('EmpresaRecord');
        // carrega todos os objetos
        $criteria = new TCriteria;
        $criteria->setProperty('order', 'sigla');
        // carrega os objetos de acordo com o criterio
        $collection = $repository->load($criteria);
        //adiciona os objetos no combo
        $items2 = array();
        foreach ($collection as $object) {
            $items2[$object->id] = $object->sigla;
        }

        // adiciona as opcoes na combo
        $empresa_id->setValue('0');
        $empresa_id->addItems($items2);

        // finaliza a transacao
        TTransaction::close();


        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $repository = new TRepository('RegionalRecord');
        // carrega todos os objetos
        $criteria = new TCriteria;
        $criteria->setProperty('order', 'nome');
        // carrega os objetos de acordo com o criterio
        $collection = $repository->load($criteria);
        //adiciona os objetos no combo
        $items = array();
        $items['0'] = 'SEM REGIONAL';
        foreach ($collection as $object) {
            $items[$object->id] = $object->nome;
        }

        // adiciona as opcoes na combo
        $regional_id->setValue('0');
        $regional_id->addItems($items);

        // finaliza a transacao
        TTransaction::close();

        // cria um vetor com as opcoes da combo
        $items = array();
        $items['S'] = 'S';
        $items['N'] = 'N';

        // adiciona as opcoes na combo
        $expira->addItems($items);
        //coloca o valor padrao no combo
        $expira->setValue('N');

        $ativo->addItems($items);
        //coloca o valor padrao no combo     
        $ativo->setValue('S');

        //Combo de abrangencia
        $itemsAbragencia = array();
        $itemsAbragencia['L'] = 'LOCAL';
        $itemsAbragencia['R'] = 'REGIONAL';
        $itemsAbragencia['E'] = 'ESTADUAL';

        $abrangencia->addItems($itemsAbragencia);

        //coloca o valor padrao no combo
        $abrangencia->setValue('L');

        // define os campos
        $this->form->addQuickField(null, $usuarioalteracao, 10);
        $this->form->addQuickField(null, $dataalteracao, 10);
        $this->form->addQuickField(null, $codigo, 50);
        $this->form->addQuickField('Servidor <font color=red><b>*</b></font>', $servidor_id, 450);
        $this->form->addQuickField('Empresa <font color=red><b>*</b></font>', $empresa_id, 40);
        $this->form->addQuickField('Abrang&ecirc;ncia <font color=red><b>*</b></font>', $abrangencia, 40);
        $this->form->addQuickField('Login <font color=red><b>*</b></font>', $login, 40);
        $this->form->addQuickField('Senha <font color=red><b>*</b></font>', $senha, 40);
        $this->form->addQuickField('Expira', $expira, 40);
        $this->form->addQuickField('Ativo <font color=red><b>*</b></font>', $ativo, 40);
        $this->form->addQuickField('Data Expira', $dataexpira, 40);
        $this->form->addQuickField(null, $titulo, 50);

        //campos obrigarorios
        $servidor_id->addValidation('Servidor', new TRequiredValidator); // required field
        $abrangencia->addValidation('Abrangência', new TRequiredValidator); // required field
        $login->addValidation('Login', new TRequiredValidator); // required field
        $senha->addValidation('Senha', new TRequiredValidator); // required field
        $ativo->addValidation('Ativo', new TRequiredValidator); // required field
        $empresa_id->addValidation('Empresa', new TRequiredValidator); // required field
        //
        //define o auto-sugerir        
        $login->setProperty('placeholder', 'Matricula');
        $senha->setProperty('placeholder', 'CPF');

        TTransaction::open('pg_ceres');
        if (!empty($_GET['method'])) {
            $action = new \Adianti\Control\TAction(array('UsuarioServidorList', 'onSearch'));
            $action->setParameter('nome', (new UsuarioRecord(filter_input(INPUT_GET, 'key')))->login);
        } else {
            $action = new \Adianti\Control\TAction(array('UsuarioServidorList', 'onLoad'));
        }
        TTransaction::close(); // finaliza a transacao
        // cria um botao de acao
        $this->form->addQuickAction('Salvar', new TAction(array($this, 'onSave')), 'ico_save.png')->class = 'btn btn-info';
        $this->form->addQuickAction('Voltar', $action, 'ico_datagrid.gif');
        //$this->form->addQuickAction('Voltar', new \Adianti\Control\TAction(array('UsuarioServidorList', 'onLoad')), 'ico_datagrid.gif');
        // adiciona o form na pagina
        parent::add($this->form);
    }

    /*
     * metodo onSave()
     * Executada quando o usuario clicar no botao salvar do formulario
     */

    function onSave() {
        try {

            // inicia transacao com o banco 'pg_ceres'
            TTransaction::open('pg_ceres');
            // obtem os dados no formulario em um objeto Record da Classe
            $object = $this->form->getData('UsuarioRecord');

            //lanca o default
            $object->servidor_id = key($object->servidor_id); // retorna o key do array
            $object->usuarioalteracao = $_SESSION['usuario'];
            $object->dataalteracao = date("d/m/Y H:i:s");

            // form validation
            $this->form->validate();

            $msg = '';
            $icone = 'info';

            if ($msg == '') {

                // armazena o objeto
                $object->store();
                $msg = 'Dados armazenados com sucesso';
            } else {
                $icone = 'error';
            }

            if ($icone == 'error') {
                // exibe mensagem de erro
                new TMessage($icone, $msg);
            } else {

                // exibe um dialogo ao usuario
                new TMessage("info", "Registro salvo com sucesso!");

                $param = [];
                $param['nome'] = $object->login;

                TApplication::gotoPage('UsuarioServidorList', 'onSearch', $param); // reload
            }
            // finaliza a transacao
            TTransaction::close();
        } catch (Exception $e) { // em caso de exce��o
            // exibe a mensagem gerada pela exce��o
            new TMessage('error', $e->getMessage());
            // desfaz todas altera��es no banco de dados
            TTransaction::rollback();
            $this->form->setData($this->form->getData());
        }
    }

    /*
     * metodo onEdit()
     * Edita os dados de um registro
     */

    function onEdit($param) {

        try {
            if (isset($param['key'])) {

                // get the parameter $key
                $key = $param['key'];

                TTransaction::open('pg_ceres');   // open a transaction with database 'samples'

                $object = new UsuarioRecord($key);        // instantiates object City

                $objServidor = new ServidorRecord($object->servidor_id);
                $object->servidor_id = [$objServidor->id => $objServidor->nome . " - " . $objServidor->matricula . " - " . $objServidor->cpf];

                $object->dataexpira = TDate::date2br($object->dataexpira);
                $this->form->setData($object);   // fill the form with the active record data

                TTransaction::close();           // close the transaction
            } else {
                //$this->form->clear();
            }
        } catch (Exception $e) { // in case of exception
            // shows the exception error message
            new TMessage('error', '<b>Error</b> ' . $e->getMessage());
            // undo all pending operations
            TTransaction::rollback();
        }
    }

}
