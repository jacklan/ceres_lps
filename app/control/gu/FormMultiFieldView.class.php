<?php

/**
 * FormMultiFieldView
 *
 * @version    1.0
 * @package    samples
 * @subpackage tutor
 * @author     Pablo Dall'Oglio
 * @copyright  Copyright (c) 2006-2014 Adianti Solutions Ltd. (http://www.adianti.com.br)
 * @license    http://www.adianti.com.br/framework-license
 */
class FormMultiFieldView extends TPage {

    private $form;

    /**
     * Class constructor
     * Creates the page
     */
    function __construct() {
        parent::__construct();

        // create the notebook
        $notebook = new TNotebook(520, 320);

        // create the form
        $this->form = new TForm;

        // creates the notebook page
        $table = new TTable;

        // add the notebook inside the form
        $this->form->add($table);

        // adds the notebook page
        $notebook->appendPage('Multi field component', $this->form);

        // create the form fields
        $multifield = new TMultiField('contacts');
        $multifield->setOrientation('horizontal');

        // $servidor_id = new TEntry('servidor_id');
//        $servidor_id->setValue($_GET['fk']);
        //  $servidor_id->setValue('1198');

        $subfield1 = new TEntry('name');
        $subfield2 = new TEntry('phone');
        $subfield3 = new TComboCombined('type_id', 'type_value');

        $subfield3->addItems(array(1 => 'Cellphone', 2 => 'Landline'));
        $subfield3->setSize(160);

        $multifield->setHeight(140);
        //     $multifield->addField('servidor_id', 'Servidor', $servidor_id, 150);
        $multifield->addField('name', 'Name', $subfield1, 160, TRUE);
        $multifield->addField('phone', 'Phone', $subfield2, 120, TRUE);
        $multifield->addField('type_id', 'Type', $subfield3, 150);

        $subfield1->setSize(120);
        $subfield2->setSize(120);

        // add a row for one field
        $row = $table->addRow();
        $row->addCell($lbl = new TLabel('Multifield object:'));
        $lbl->setFontStyle('b');
        $row = $table->addRow();
        $row->addCell($multifield);

        //  $row = $table->addRow();
        //     $row->addCell($servidor_id);
        // creates the action button
        $button1 = new TButton('action1');
        // define the button action
        $button1->setAction(new TAction(array($this, 'onSave')), 'Save');
        $button1->setImage('ico_save.png');
        // add a row for the button
        $row = $table->addRow();
        $row->addCell($button1);

        // define wich are the form fields
        $this->form->setFields(array($multifield, $button1));

        // wrap the page content using vertical box
        $vbox = new TVBox;
        //$vbox->add(new TXMLBreadCrumb('menu.xml', __CLASS__));
        $vbox->add($notebook);

        parent::add($vbox);
    }

    /**
     * Simulates an save button
     * Show the form content
     */
    public function onSave($param) {
        /*
          $data = $this->form->getData(); // optional parameter: active record class

          // put the data back to the form
          $this->form->setData($data);
          $message = '';

          // get the contacts
          if ($data->contacts)
          {
          foreach ($data->contacts as $contact)
          {
          $message .= $contact->name . ' - ' . $contact->phone . ' - ' . $contact->type_id . ' - ' . $contact->type_value . '<br>';
          }
          }

          // show the message
          new TMessage('info', $message);

          */
        
         

        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // obtem os dados no formulario em um objeto CarroRecord
        $cadastro = $this->form->getData();
        var_dump($cadastro);
//        exit();
        //antes de armazenar verifica se algum campo eh requerido e nao foi informado
        // $dados = $cadastro->toArray();
        // $obj = implode(" , ", $dados); //converte os dados do array para string

        $msg = '';
        $icone = 'info';

        try {

            if ($msg == '') {
                // get the contacts
                if ($cadastro->contacts) {
                    foreach ($cadastro->contacts as $contact) {
                        $teste = new testemultfieldRecord();

                        //   $teste->name = $contact->name . ' - ' . $contact->phone . ' - ' . $contact->type_id . ' - ' . $contact->type_value . '<br>';
                        $teste->name = $contact->name;
                        $teste->phone = $contact->phone;
                        $teste->type_id = $contact->type_id;
                        $teste->type_value = $contact->type_value;
                        //  $teste->servidor_id = $contact->servidor_id;
                        $teste->store();
                    }
                }
                // armazena o objeto
                $msg = 'Dados armazenados com sucesso';
                //  $this->form->setData($cadastro);   // fill the form with the active record data
                TTransaction::close();
            } else {
                $icone = 'error';
            }

            if ($icone == 'error') {
                // exibe mensagem de erro
                new TMessage($icone, $msg);
                $this->form->setData($cadastro);   // fill the form with the active record data
            } else {
                // exibe um dialogo ao usuario
                new TMessage("info", "Registro salvo com sucesso!");
                TApplication::gotoPage('FormMultiFieldView'); // reload
            }
        } catch (Exception $e) { // em caso de exce��o
            // exibe a mensagem gerada pela exce��o
            new TMessage('error', $e->getMessage());
            // desfaz todas altera��es no banco de dados
            TTransaction::rollback();
            $this->form->setData($cadastro);   // fill the form with the active record data
        }
       
    }

}

?>