<?php

/*
 * classe UsuarioServidorForm
 * Cadastro de UsuarioServidor: Contem o formularo
 */

//ini_set('display_errors', 1);
//ini_set('display_startup_erros', 1);
//error_reporting(E_ALL);

use Adianti\Database\TFilter1;
use Adianti\Widget\Datagrid\TDatagridTables;

class AlteraSenhaForm extends TPage {

    private $form;     // formulario de cadastro

    /*
     * metodo construtor
     * Cria a pagina do formulario
     */

    public function __construct() {
        parent::__construct();

        $this->form = new TQuickForm;
        $this->form->class = 'form_AlterarSenha';
        $this->form->setFormTitle('Alterar Senha');

        // cria um rotulo para o titulo
        $titulo = new TLabel('Alterar Senha de Usu&aacute;rio');
        $titulo->setFontFace('Arial');
        $titulo->setFontColor('red');
        $titulo->setFontSize(12);

        // session_start();
        // cria os campos do formulario
        $codigo = new THidden('id');
        $codigo->setValue($_SESSION["usuario_id"]);
        $codigo->setEditable(false);
        $servidor_id = new TEntry('servidor_id');
        $servidor_id->setValue($_SESSION["servidor_id"]);
        $servidor_id->setEditable(false);
        $login = new TEntry('login');
        $login->setValue($_SESSION["usuario"]);
        $login->setEditable(false);
        $senha = new TPassword('senha');
        $confirmasenha = new TPassword('confirmasenha');

        // cria um rotulo para o titulo
        $titulo = new TLabel('<div style="position:floatval; width: 200px;"> <b>* Campo obrigatorio</b></div>');
        $titulo->setFontFace('Arial');
        $titulo->setFontColor('red');
        $titulo->setFontSize(10);

        // finaliza a transacao
        //TTransaction::close();
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $cadastro = new ServidorRecord($_SESSION["servidor_id"]);
        // cria um criterio de selecao, ordenado pelo id do servidor
        //adiciona os objetos na tela com as informacoes do servidor
        if ($cadastro) {
            $nome = new TLabel($cadastro->nome);
            $matricula = new TLabel($cadastro->matricula);
        }
        // finaliza a transacao
        TTransaction::close();

        // define os campos
        $this->form->addQuickField(null, $codigo, 10);
        $this->form->addQuickField('Servidor', $servidor_id, 40);
        $this->form->addQuickField('Nome', $nome, 400);
        $this->form->addQuickField('Matricula', $matricula, 40);
        $this->form->addQuickField('Login', $login, 40);
        $this->form->addQuickField('Senha <font color=red><b>*</b></font>', $senha, 40);
        $this->form->addQuickField('Confirmar senha', $confirmasenha, 40);
        $this->form->addQuickField(null, $titulo, 50);

        // cria um botao de acao (salvar)
        $this->form->addQuickAction('Salvar', new TAction(array($this, 'onSave')), 'ico_save.png');
        //$this->form->addQuickAction('Voltar', new TAction(array('MeuPerfilList', 'onReload')), 'ico_datagrid.gif');
        // adiciona o form na pagina
        parent::add($this->form);
    }

    /*
     * metodo onSave()
     * Executada quando o usuario clicar no botao salvar do formulario
     */

    function onSave() {
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // obtem os dados no formulario em um objeto Record da Classe
        $cadastro = $this->form->getData('UsuarioRecord');

        $cadastro->senha = strtoupper($cadastro->senha);

        //antes de armazenar verifica se algum campo eh requerido e nao foi informado
        $dados = $cadastro->toArray();

        $msg = '';
        $icone = 'info';

        if (empty($dados['senha'])) {
            $msg .= 'A Senha deve ser informada.';
        }

        //if (empty ($dados['confirmasenha'])){
        //$msg .= 'A Senha deve ser confirmada.';
        // }
        //if(strcmp($dados['senha'], $dados['confirmasenha']) != 0){
        //$msg .= 'As senhas digitadas não conferem. tente novamente!';
        //}

        try {

            if ($msg == '') {
                // armazena o objeto
                $novo = new UsuarioRecord($cadastro->id);
                $novo->senha = $cadastro->senha;

                $novo->store();
                $msg = 'Senha modificada com sucesso';

                // finaliza a transacao
                TTransaction::close();
            } else {
                $icone = 'error';
            }

            if ($icone == 'error') {
                // exibe mensagem de erro
                new TMessage($icone, $msg);
            } else {

                // exibe um dialogo ao usuario
                new TMessage("info", "Registro salvo com sucesso!");
                TApplication::gotoPage('AlteraSenhaForm', 'onReload'); // reload
            }
        } catch (Exception $e) { // em caso de exce��o
            // exibe a mensagem gerada pela exce��o
            new TMessage('error', $e->getMessage());
            // desfaz todas altera��es no banco de dados
            TTransaction::rollback();
        }
    }

    /*
     * metodo onEdit()
     * Edita os dados de um registro
     */

    function onEdit($param) {
        // obtem o parametro $key
        $key = $param['key'];
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $cadastro = new UsuarioRecord($key);
        // lanca os dados no formulario
        $this->form->setData($cadastro);

        // finaliza a transacao
        TTransaction::close();
    }

}

?>