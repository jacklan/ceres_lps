<?php

/*
 * classe EmpresaForm
 * Cadastro de Empresa: Contem o formularo
 */

class EmpresaForm extends TPage {

    private $form;     // formulario de cadastro

    /*
     * metodo construtor
     * Cria a pagina do formulario
     */

    public function __construct() {
        parent::__construct();

        // instancia um formulario
        $this->form = new TQuickForm;
        $this->form->class = 'form_empresa';
        $this->form->setFormTitle('<font color="red" size="3" face="Arial"><b>Formul&aacute;rio de Empresa</b></font>');

        // cria os campos do formulario
        $codigo = new THidden('id');
        $codigo->setEditable(false);
        $nome = new TEntry('nome');
        $endereco = new TEntry('endereco');
        $bairro = new TEntry('bairro');
        $cidade = new TEntry('cidade');
        $uf = new TCombo('uf');
        $cep = new TEntry('cep');
        $telefone = new TEntry('telefone');
        $fax = new TEntry('fax');
        $email = new TEntry('email');
        $sigla = new TEntry('sigla');

        $usuarioalteracao = new THidden('usuarioalteracao');
        $dataalteracao = new THidden('dataalteracao');

        // cria um rotulo para o titulo
        $titulo = new TLabel('<div style="position:floatval; width: 200px;"> <b>* Campo obrigatorio</b></div>');
        $titulo->setFontFace('Arial');
        $titulo->setFontColor('red');
        $titulo->setFontSize(10);

        // cria um vetor com as opcoes da combo
        $items = array();
        $items['RN'] = 'RN';

        // adiciona as opcoes na combo
        $uf->addItems($items);

        //coloca o valor padrao no combo
        $uf->setValue('RN');

        $cep->setMask('88888-888');
        $telefone->setMask('(88)8888-8888');
        $fax->setMask('(88)8888-8888');

        // define os campos
        $this->form->addQuickField(null, $codigo, 10);
        $this->form->addQuickField(null, $usuarioalteracao, 10);
        $this->form->addQuickField(null, $dataalteracao, 10);
        $this->form->addQuickField('Nome <font color=red><b>*</b></font>', $nome, 50);
        $this->form->addQuickField('Sigla <font color=red><b>*</b></font>', $sigla, 50);
        $this->form->addQuickField('Endereço', $endereco, 50);
        $this->form->addQuickField('Bairro', $bairro, 50);
        $this->form->addQuickField('Cidade', $cidade, 50);
        $this->form->addQuickField('UF', $uf, 50);
        $this->form->addQuickField('CEP', $cep, 50);
        $this->form->addQuickField('Telefone', $telefone, 50);
        $this->form->addQuickField('FAX', $fax, 50);
        $this->form->addQuickField('E-mail', $email, 50);
        $this->form->addQuickField(null, $titulo, 50);

        $nome->setProperty('placeholder', 'Nome');
        $nome->setProperty('required', 'required');
        $endereco->setProperty('placeholder', 'Endereço');
        $endereco->setProperty('required', 'required');
        $cidade->setProperty('required', 'required');
        $bairro->setProperty('required', 'required');
        $endereco->setProperty('required', 'required');
        $cidade->setProperty('placeholder', 'Cidade');
        $bairro->setProperty('placeholder', 'Bairro');
        $cep->setProperty('placeholder', '00000-000');
        $telefone->setProperty('placeholder', '(00)0000-0000');
        $fax->setProperty('placeholder', '(00)0000-0000');
        $email->setProperty('placeholder', 'exemplo@exemplo.com');
        $sigla->setProperty('placeholder', 'Sigla');
        $sigla->setProperty('required', 'required');

        // cria um botao de acao
        $this->form->addQuickAction('Salvar', new TAction(array($this, 'onSave')), 'ico_save.png');
        $this->form->addQuickAction('Voltar', new TAction(array('EmpresaList', 'onReload')), 'ico_datagrid.gif');

        // adiciona a tabela a pagina
        parent::add($this->form);
    }

    /*
     * metodo onSave()
     * Executada quando o usuario clicar no botao salvar do formulario
     */

    function onSave() {
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // obtem os dados no formulario em um objeto CarroRecord
        $cadastro = $this->form->getData('EmpresaRecord');

        //lanca o default
        $cadastro->usuarioalteracao = $_SESSION['usuario'];
        $cadastro->dataalteracao = date("d/m/Y H:i:s");

        //antes de armazenar verifica se algum campo eh requerido e nao foi informado
        $dados = $cadastro->toArray();
        $obj = implode(" , ", $dados); //converte os dados do array para string

        $msg = '';
        $icone = 'info';

        if (empty($dados['nome'])) {
            $msg .= 'O Nome deve ser informado.';
        }

        if (empty($dados['sigla'])) {
            $msg .= 'A Sigla deve ser informada.';
        }

        try {

            if ($msg == '') {
                // armazena o objeto
                $cadastro->store();
                $msg = 'Dados armazenados com sucesso';

                //criando log 
//                TTransaction::setLogger(new TLoggerTXT('tmp/log.txt'));
//                TTransaction::Log(' ---- Insert ---- '. $obj);
                // finaliza a transacao
                TTransaction::close();
            } else {
                $icone = 'error';
            }

            if ($icone == 'error') {
                // exibe mensagem de erro
                new TMessage($icone, $msg);
                $this->form->setData($cadastro);   // fill the form with the active record data
            } else {
                // exibe um dialogo ao usuario
                new TMessage("info", "Registro salvo com sucesso!");
                TApplication::gotoPage('EmpresaList', 'onReload'); // reload
            }
        } catch (Exception $e) { // em caso de exce��o
            // exibe a mensagem gerada pela exce��o
            new TMessage('error', $e->getMessage());
            // desfaz todas altera��es no banco de dados
            TTransaction::rollback();
            $this->form->setData($cadastro);   // fill the form with the active record data
        }
    }

    /*
     * metodo onEdit()
     * Edita os dados de um registro
     */

    function onEdit($param) {
        try {
            if (isset($param['key'])) {

                // get the parameter $key
                $key = $param['key'];

                TTransaction::open('pg_ceres');   // open a transaction with database 'samples'

                $object = new EmpresaRecord($key);        // instantiates object City

                $this->form->setData($object);   // fill the form with the active record data

                TTransaction::close();           // close the transaction
            } else {
                $this->form->clear();
            }
        } catch (Exception $e) { // in case of exception
            // shows the exception error message
            new TMessage('error', '<b>Error</b> ' . $e->getMessage());
            // undo all pending operations
            TTransaction::rollback();
        }
    }

}

?>