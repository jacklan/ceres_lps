<?php
/*
 * classe Gerenteregional
 * Cadastro de ServidorUnidade: Contem a listagem e o formulario de busca
 */
use Adianti\Database\TFilter1;
use Adianti\Widget\Datagrid\TDatagridTables;

class GerenteregionalDetalhe extends TPage
{
    private $form;     // formulario de cadastro
    private $datagrid; // listagem

    /*
     * metodo construtor
     * Cria a pagina, o formulario e a listagem
     */
    public function __construct()
    {
        parent::__construct();
		
		// instancia um formulario
		$this->form = new TQuickForm;
        $this->form->class = 'form_Gerenteregional';
        $this->form->setFormTitle('<font color="red" size="3" face="Arial"><b>Formul&aacute;rio Gerente Regional</b></font>');
        
        // cria os campos do formulario
        $codigo         = new THidden('id');
        $codigo->setEditable(false);
        $servidor_id   = new TCombo('servidor_id');
        $datainicio  = new TDate('datainicio');
        $datafim  = new TDate('datafim');
    
        //cria a colecao da tabela estrangeira
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $repository = new TRepository('ServidorRecord');
        // cria um criterio de selecao, ordenado pelo id
        $criteria = new TCriteria;
        $criteria->setProperty('order', 'nome');
        // carrega os objetos de acordo com o criterio
        $cadastros = $repository->load($criteria);

        //adiciona os objetos no combo
        foreach ($cadastros as $object){
            $items[$object->id] = $object->nome;
        }

        // adiciona as opcoes na combo
        $servidor_id->addItems($items);

        // finaliza a transacao
        TTransaction::close();

    
        //cria a colecao da tabela estrangeira do Cabecalho
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $cadastro = new RegionalRecord($_GET['fk']);

        if ($cadastro){
            $nome = new TLabel($cadastro->nome);
        }
        // finaliza a transacao
        TTransaction::close();


        //coloca o valor do relacionamento
        $regional_id = new THidden('regional_id');
        $regional_id->setValue($_GET['fk']);
		
		// define os campos
		$this->form->addQuickField('Nome', $servidor_id, 40);
		$this->form->addQuickField('Data Inicio', $datainicio, 40);
		$this->form->addQuickField('Data Fim', $datafim, 40);
		
		// cria um botao de acao
		$this->form->addQuickAction('Salvar', new TAction(array($this, 'onSave')), 'ico_save.png');
		$this->form->addQuickAction('Voltar', new TAction(array('RegionalList', 'onReload')), 'ico_datagrid.gif');
		     
        // instancia objeto DataGrid
        $this->datagrid = new TDataGridTables;
     
        // instancia as colunas da DataGrid
        $dgcodigo   = new TDataGridColumn('id',  'C&oacute;digo',  'center', 50);
        $dgnomeservidor  = new TDataGridColumn('nome_servidor', 'Servidor', 'left', 1200);
  
        // adiciona as colunas a DataGrid
        $this->datagrid->addColumn($dgcodigo);
        $this->datagrid->addColumn($dgnomeservidor);
        
        // instancia duas acoes da DataGrid
        $action1 = new TDataGridAction(array($this, 'onEdit'));
        $action1->setLabel('Editar');
        $action1->setImage('ico_edit.png');
        $action1->setField('id');
        $action1->setFk('regional_id');
        
        $action2 = new TDataGridAction(array($this, 'onDelete'));
        $action2->setLabel('Deletar');
        $action2->setImage('ico_delete.png');
        $action2->setField('id');
        $action2->setFk('regional_id');
        
        // adiciona as acoes a DataGrid
        $this->datagrid->addAction($action1);
        $this->datagrid->addAction($action2);
        
        // cria o modelo da DataGrid, montando sua estrutura
        $this->datagrid->createModel();

        // monta a paina atraves de uma tabela
        $panel = new TPanelForm(700, 500);
        $panel->put($this->form, 0, 0);
        $panel->put($this->datagrid, 150, 200);

        // adiciona a tabela a pagina
        parent::add($panel);
    }
    
    /*
     * metodo onReload()
     * Carrega a DataGrid com os objetos do banco de dados
     */
    function onReload()
    {
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instancia um repositorio da Classe
        $repository = new TRepository('GerenteregionalRecord');

        // cria um criterio de selecao
        $criteria = new TCriteria;

        //filtra pelo campo selecionado pelo usuario
        $criteria->add(new TFilter('regional_id', '=', $_GET['fk']));

        // carrega os objetos de acordo com o criterio
        $cadastros = $repository->load($criteria);

        $this->datagrid->clear();
        if ($cadastros)
        {
            // percorre os objetos retornados
            foreach ($cadastros as $cadastro)
            {
                // adiciona o objeto na DataGrid
                $this->datagrid->addItem($cadastro);
            }
        }


        // finaliza a transacao
        TTransaction::close();
        $this->loaded = true;
    }
    
    
    /*
     * metodo onDelete()
     * Executada quando o usuario clicar no botao excluir da datagrid
     * Pergunta ao usuario se deseja realmente excluir um registro
     */
    function onDelete($param)
    {
        // obtem o parametro $key
        $key=$param['key'];
        
        // define duas acoes
        $action1 = new TAction(array($this, 'Delete'));
                
        // define os parametros de cada acao
        $action1->setParameter('key', $key);
        
        //encaminha a chave estrangeira
        $action1->setParameter('fk', $_GET['fk']);
        
        // exibe um dialogo ao usuario
        new TQuestion('Deseja realmente excluir o registro ?', $action1, $action2);
    }
    
    /*
     * metodo Delete()
     * Exclui um registro
     */
    function Delete($param)
    {
        // obtem o parametro $key
        $key=$param['key'];
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instanicia objeto Record
        $cadastro = new GerenteregionalRecord($key);

        try{
            // deleta objeto do banco de dados
            $cadastro->delete();
			// exibe um dialogo ao usuario
                  new TMessage("info", "Registro deletado com sucesso!");
		
            // finaliza a transacao
            TTransaction::close();
        }
        catch (Exception $e) // em caso de exce��o
        {
            // exibe a mensagem gerada pela exce��o
            new TMessage('error', $e->getMessage());
            // desfaz todas altera��es no banco de dados
            TTransaction::rollback();
        }

        // re-carrega a datagrid
        $this->onReload();
        
    }
    
    /*
     * metodo show()
     * Exibe a pagina
     */
    function show()
    {
        // carrega os dados no datagrid
        $this->onReload();
        //chama o metodo show da super classe
        parent::show();

    }


    /*
     * metodo onSave()
     * Executada quando o usuario clicar no botao salvar do formulario
     */
    function onSave()
    {
        
        if(empty ($dados['regional_id']) || empty ($dados['servidor_id'])){

        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // obtem os dados no formulario em um objeto Record da Classe
        $cadastro = $this->form->getData('GerenteregionalRecord');
		//lanca o default
        $cadastro->usuarioalteracao = $_SESSION['usuario'];
        $cadastro->dataalteracao = date("d/m/Y H:i:s");

        //antes de armazenar verifica se algum campo eh requerido e nao foi informado
        $dados = $cadastro->toArray();

        $msg = '';
        $icone = 'info';

        if (empty ($dados['regional_id'])){
           $msg .= 'A regional deve ser informada.';
        }

        if (empty ($dados['servidor_id'])){
           $msg .= 'O gerente deve ser informado.';
        }

        try{

            if ($msg == ''){
              // armazena o objeto
              $cadastro->store();
              $msg = 'Dados armazenados com sucesso';

              // finaliza a transacao
              TTransaction::close();

            }else{
                $icone = 'error';
            }

            if ($icone == 'error'){
                // exibe mensagem de erro
                new TMessage($icone, $msg);
            }else{
				
				$param = array();
				$param ['fk'] = $dados['regional_id'];
				
				//chama o formulario com o grid
                // exibe um dialogo ao usuario
                new TMessage("info", "Registro salvo com sucesso!");
				TApplication::gotoPage('GerenteregionalDetalhe','onReload', $param); // reload
				
            }
        }
        catch (Exception $e) // em caso de exce��o
        {
            // exibe a mensagem gerada pela exce��o
            new TMessage('error', $e->getMessage());
            // desfaz todas altera��es no banco de dados
            TTransaction::rollback();
        }

    }

    }

    /*
     * metodo onEdit()
     * Edita os dados de um registro
     */
    function onEdit($param)
    {
		
		try {
            if (isset($param['key'])) {
				
                // get the parameter $key
                $key = $param['key'];

                TTransaction::open('pg_ceres');   // open a transaction with database 'samples'
				
                $object = new GerenteregionalRecord($key);        // instantiates object City
				
                $this->form->setData($object);   // fill the form with the active record data
				
                TTransaction::close();           // close the transaction
				
            } else {
                $this->form->clear();
            }
        } catch (Exception $e) { // in case of exception
            // shows the exception error message
            new TMessage('error', '<b>Error</b> ' . $e->getMessage());
            // undo all pending operations
            TTransaction::rollback();
        }
        
    }


}
?>