<?php

/*
ini_set('display_errors', 1);
ini_set('display_startup_erros', 1);
error_reporting(E_ALL);
*/

/*
 * classe PaginaForm
 * Cadastro de Pagina: Contem o formularo
 */

use Adianti\Database\TFilter1;
use Adianti\Widget\Datagrid\TDatagridTables;

class PaginaForm extends TPage {

    private $form;     // formulario de cadastro

    /*
     * metodo construtor
     * Cria a pagina do formulario
     */

    public function __construct() {
        parent::__construct();

        // instancia um formulario
        $this->form = new TQuickForm;
        $this->form->class = 'form_Pagina';
        $this->form->setFormTitle('<font color="red" size="3" face="Arial"><b>Formul&aacute;rio de Página</b></font>');

        // cria os campos do formulario
        $codigo = new THidden('id');
        $nome = new TEntry('nome');
        $arquivo = new TEntry('arquivo');
        $modulo_id = new TCombo('modulo_id');
        $grupomenu_id = new TCombo('grupomenu_id');
        $situacao = new TCombo('situacao');
        $novajanela = new TCombo('novajanela');
        $arquivoleitura = new TCombo('arquivoleitura');

		$nome->setProperty('placeholder', 'ex.: Produtor');
        $nome->setProperty('required', 'required');
        $arquivo->setProperty('placeholder', 'ex.: ProdutorList');
        $arquivo->setProperty('required', 'required');
		
        // cria um rotulo para o titulo
        $titulo = new TLabel('<div style="position:floatval; width: 200px;"> <b>* Campo obrigatorio</b></div>');
        $titulo->setFontFace('Arial');
        $titulo->setFontColor('red');
        $titulo->setFontSize(10);


        // cria um vetor com as opcoes da combo
        $items = array();
        $items['ATIVO'] = 'ATIVO';
        $items['INATIVO'] = 'INATIVO';

        // adiciona as opcoes na combo
        $situacao->addItems($items);
        //coloca o valor padrao no combo
        $situacao->setValue('ATIVO');

        // cria um vetor com as opcoes da combo
        $items = array();
        $items['NAO'] = 'NAO';
        $items['SIM'] = 'SIM';

        // adiciona as opcoes na combo
        $arquivoleitura->addItems($items);
        //coloca o valor padrao no combo
        $arquivoleitura->setValue('NAO');

        // adiciona as opcoes na combo
        $novajanela->addItems($items);
        //coloca o valor padrao no combo
        $novajanela->setValue('NAO');

        $usuarioalteracao = new THidden('usuarioalteracao');
        $dataalteracao = new THidden('dataalteracao');
		
        //cria a colecao da tabela estrangeira
        $items = array();
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $repository = new TRepository('ModuloRecord');
        $criteria = new TCriteria;
        $criteria->setProperty('order', 'nome');
        // carrega todos os objetos
        $collection = $repository->load($criteria);
        //adiciona os objetos no combo
        foreach ($collection as $object) {
            $items[$object->id] = $object->nome;
        }

        // adiciona as opcoes na combo
        $modulo_id->addItems($items);

        // finaliza a transacao
        TTransaction::close();

        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia objeto da Classe Record
        $repository = new TRepository('GrupoMenuRecord');
        $criteria = new TCriteria;
        $criteria->setProperty('order', 'nome');
        // carrega todos os objetos
        $collection = $repository->load($criteria);
        //adiciona os objetos no combo
        foreach ($collection as $object) {
            $items1[$object->id] = $object->nome;
        }

        // adiciona as opcoes na combo
        $grupomenu_id->addItems($items1);

        // finaliza a transacao
        TTransaction::close();

        // define os campos
		$this->form->addQuickField(null, $usuarioalteracao, 1);
        $this->form->addQuickField(null, $dataalteracao, 1);
        $this->form->addQuickField(null, $codigo, 10);
        $this->form->addQuickField('Nome <font color=red><b>*</b></font>', $nome, 40);
        $this->form->addQuickField('Arquivo <font color=red><b>*</b></font>', $arquivo, 40);
        $this->form->addQuickField('Módulo <font color=red><b>*</b></font>', $modulo_id, 40);
        $this->form->addQuickField('Grupo Menu <font color=red><b>*</b></font>', $grupomenu_id, 40);
        $this->form->addQuickField('Situação <font color=red><b>*</b></font>', $situacao, 40);
        $this->form->addQuickField('Nova Janela <font color=red><b>*</b></font>', $novajanela, 40);
        $this->form->addQuickField(null, $titulo, 50);

        // cria um botao de acao
        $this->form->addQuickAction('Salvar', new TAction(array($this, 'onSave')), 'ico_save.png');
        $this->form->addQuickAction('Voltar', new TAction(array('PaginaList', 'onReload')), 'ico_datagrid.gif');

        // adiciona a tabela a pagina
        parent::add($this->form);
    }

    /*
     * metodo onSave()
     * Executada quando o usuario clicar no botao salvar do formulario
     */

    function onSave() {
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // obtem os dados no formulario em um objeto Record
        $cadastro = $this->form->getData('PaginaRecord');

		 //lanca o default
        $cadastro->usuarioalteracao = $_SESSION['usuario'];
        $cadastro->dataalteracao = date("d/m/Y H:i:s");
		
        //antes de armazenar verifica se algum campo eh requerido e nao foi informado
        $dados = $cadastro->toArray();

        $msg = '';
        $icone = 'info';

        if ($cadastro->arquivoleitura == 'SIM') {
            $cadastro->arquivo = 'app.docs/manuais/' . $cadastro->arquivo;
        }

        if (empty($dados['arquivo'])) {
        	$msg .= 'O Arquivo deve ser informado.<br/>';
        }
        
        if (empty($dados['nome'])) {
            $msg .= 'O Nome deve ser informado.<br/>';
        }

        if (empty($dados['modulo_id'])) {
            $msg .= 'O M&oacute;dulo deve ser informado.<br/>';
        }

        try {

            if ($msg == '') {
                // armazena o objeto
                $cadastro->store();
                $msg = 'Dados armazenados com sucesso';

                // finaliza a transacao
                TTransaction::close();
            } else {
                $icone = 'error';
            }

            if ($icone == 'error') {
                // exibe mensagem de erro
                new TMessage($icone, $msg);
                // lanca os dados no formulario
                $this->form->setData($cadastro);
            } else {
                // exibe um dialogo ao usuario
                new TMessage("info", "Registro salvo com sucesso!");
				TApplication::gotoPage('PaginaList','onReload'); // reload
                // exibe um dialogo ao usuario
				
            }
        
        }catch( Exception $e ) 
        { 
        	
        	// em caso de exceção
            // exibe a mensagem gerada pela exce��o
            new TMessage('error', $e->getMessage());
            
            // lanca os dados no formulario
            $this->form->setData($cadastro);
            
            // desfaz todas altera��es no banco de dados
            TTransaction::rollback();
            
        }
        
    }

    /*
     * metodo onEdit()
     * Edita os dados de um registro
     */

    function onEdit($param) {
        
		try {
            if (isset($param['key'])) {

                // get the parameter $key
                $key = $param['key'];

                TTransaction::open('pg_ceres');   // open a transaction with database 'samples'

                $object = new PaginaRecord($key);        // instantiates object City

                $this->form->setData($object);   // fill the form with the active record data

                TTransaction::close();           // close the transaction
            } else {
              //  $this->form->clear();
            }
        } catch (Exception $e) { // in case of exception
            // shows the exception error message
            new TMessage('error', '<b>Error</b> ' . $e->getMessage());
            // undo all pending operations
            TTransaction::rollback();
        }
		
    }

}

?>