-- SQL Manager for PostgreSQL 5.3.0.1
-- ---------------------------------------
-- Host      : localhost
-- Database  : db_modelo_adianti
-- Version   : PostgreSQL 9.2.4, compiled by Visual C++ build 1600, 32-bit



SET check_function_bodies = false;
--
-- Definition for function unaccent_string (OID = 2322688) : 
--
SET search_path = public, pg_catalog;
CREATE FUNCTION public.unaccent_string (
  text
)
RETURNS text
AS 
$body$
SELECT translate(
    $1,
    'áàãâäéèêëíìîïóòõôöçÁÀÃÂÄÉÈÊËÍÌÎÏÓÒÕÔÖÚÙÛÜÇ',
    'aaaaaeeeeiiiiooooocAAAAAEEEEIIIIOOOOOUUUUC'
);
$body$
LANGUAGE sql
IMMUTABLE STRICT;
--
-- Definition for function special_like (OID = 2322689) : 
--
CREATE FUNCTION public.special_like (
  text,
  text
)
RETURNS boolean
AS 
$body$
SELECT unaccent_string($1::text) ilike '%' || unaccent_string($2::text) || '%';
$body$
LANGUAGE sql
IMMUTABLE STRICT;
--
-- Structure for table usuario (OID = 2298112) : 
--
CREATE TABLE public.usuario (
    id bigserial NOT NULL,
    servidor_id bigint,
    login varchar(20) NOT NULL,
    senha varchar(20) NOT NULL,
    expira varchar(3),
    dataexpira date,
    dataalteracao timestamp without time zone,
    usuarioalteracao varchar(20),
    ativo varchar(1) DEFAULT 'S'::character varying,
    regional_id bigint DEFAULT 0,
    empresa_id bigint
)
WITH (oids = false);
--
-- Structure for table acesso_servidor (OID = 2298120) : 
--
CREATE TABLE public.acesso_servidor (
    id bigint NOT NULL,
    servidor_id bigint NOT NULL,
    ultimoacesso timestamp(0) without time zone,
    dataacesso date,
    horaacesso time(0) without time zone
)
WITH (oids = false);
--
-- Structure for table servidor (OID = 2298139) : 
--
CREATE TABLE public.servidor (
    id bigserial NOT NULL,
    nome varchar(50) NOT NULL,
    cpf varchar(20) NOT NULL,
    matricula varchar(20),
    endereco varchar(200),
    bairro varchar(50),
    cidade varchar(50),
    uf char(2),
    cep varchar(9),
    telefone varchar(30),
    fax varchar(30),
    celular varchar(30),
    email varchar(100),
    dataadmissao timestamp without time zone,
    datanascimento timestamp without time zone,
    sexo char(1),
    estadocivil varchar(20),
    naturalidade varchar(50),
    carteiratrabalho varchar(50),
    pispasep varchar(50),
    dataorgaofgts timestamp without time zone,
    rg varchar(50),
    orgaorg varchar(20),
    tipofuncionario varchar(20),
    situacao varchar(20),
    datasituacao timestamp without time zone,
    dataalteracao timestamp without time zone,
    usuarioalteracao varchar(20),
    numconselho varchar(40),
    banco varchar(20),
    agencia varchar(20),
    contacorrente varchar(20),
    nomepai varchar(100),
    nomemae varchar(100),
    nacionalidade varchar(30),
    deficienteauditivo varchar(3),
    deficientevisual varchar(3),
    deficientefisico varchar(3),
    cor varchar(20),
    altura varchar(5),
    peso varchar(5),
    datainclusao timestamp without time zone,
    ufnaturalidade char(2),
    conselho_id bigint,
    cargo_id bigint,
    redistribuido char(3),
    relotado char(3),
    nivelsalarial_id bigint,
    quadrosuplementar char(3),
    aceitaplano char(3) DEFAULT 'NAO'::bpchar,
    dataaposentadoria timestamp without time zone,
    protocoloadesao bigint,
    dataadesao timestamp(0) without time zone,
    nivelsalarialantigo_id bigint,
    documentosaida varchar(30),
    dataredistribuicao timestamp without time zone,
    documentoredistribuicao varchar(30),
    cargonovo_id bigint,
    requisitocargo_id bigint,
    datarelotado timestamp without time zone,
    dataquadrosuplementar timestamp without time zone,
    documentorelotado varchar(30),
    documentoquadrosuplementar varchar(30),
    jornada_id bigint,
    cnh varchar(50),
    categoriacnh varchar(10),
    validadecnh timestamp(0) without time zone,
    endereco2 varchar(80),
    bairro2 varchar(50),
    cidade2 varchar(50),
    cep2 varchar(20),
    inf_complementares text,
    cedidoorgao varchar(100),
    nomecracha varchar(50)
)
WITH (oids = false);
--
-- Structure for table usuarioperfil (OID = 2298151) : 
--
CREATE TABLE public.usuarioperfil (
    id bigserial NOT NULL,
    perfil_id bigint NOT NULL,
    usuario_id bigint NOT NULL,
    dataalteracao timestamp without time zone,
    usuarioalteracao varchar(20)
)
WITH (oids = false);
--
-- Structure for table perfil (OID = 2298159) : 
--
CREATE TABLE public.perfil (
    id bigserial NOT NULL,
    modulo_id bigint NOT NULL,
    nome varchar(50) NOT NULL,
    dataalteracao timestamp without time zone,
    usuarioalteracao varchar(20)
)
WITH (oids = false);
--
-- Structure for table modulo (OID = 2298167) : 
--
CREATE TABLE public.modulo (
    id bigserial NOT NULL,
    nome varchar(50) NOT NULL,
    dataalteracao timestamp without time zone,
    usuarioalteracao varchar(20)
)
WITH (oids = false);
--
-- Definition for view vw_usuarioperfil (OID = 2298173) : 
--
CREATE VIEW public.vw_usuarioperfil
AS
SELECT DISTINCT u.id AS usuario_id, u.login, u.senha, s.nome, s.cpf,
    s.matricula, 'EMATER'::text AS tipo, s.id AS servidor_id, m.nome AS modulo
FROM ((((usuario u JOIN servidor s ON ((u.servidor_id = s.id))) JOIN
    usuarioperfil up ON ((u.id = up.usuario_id))) JOIN perfil p ON ((up.perfil_id = p.id))) JOIN modulo m ON ((p.modulo_id = m.id)))
ORDER BY u.id, u.login, u.senha, s.nome, s.cpf, s.matricula, s.id, m.nome;

--
-- Structure for table pagina (OID = 2298180) : 
--
CREATE TABLE public.pagina (
    id bigserial NOT NULL,
    modulo_id bigint NOT NULL,
    nome varchar(50) NOT NULL,
    situacao varchar(20) NOT NULL,
    arquivo varchar(100),
    dataalteracao timestamp without time zone,
    usuarioalteracao varchar(20),
    grupomenu_id bigint,
    novajanela varchar(3) DEFAULT 'NAO'::character varying,
    arquivoleitura char(3) DEFAULT 'NAO'::bpchar
)
WITH (oids = false);
--
-- Structure for table perfilpagina (OID = 2298191) : 
--
CREATE TABLE public.perfilpagina (
    id bigserial NOT NULL,
    perfil_id bigint NOT NULL,
    pagina_id bigint NOT NULL,
    dataalteracao timestamp without time zone,
    usuarioalteracao varchar(20)
)
WITH (oids = false);
--
-- Structure for table grupomenu (OID = 2298199) : 
--
CREATE TABLE public.grupomenu (
    id bigserial NOT NULL,
    nome varchar(50) NOT NULL,
    dataalteracao timestamp without time zone,
    usuarioalteracao varchar(20),
    icone varchar(100) DEFAULT 'fa fa-bars'::character varying,
    ordem bigint
)
WITH (oids = false);
--
-- Definition for view vw_grupomenuusuario (OID = 2298208) : 
--
CREATE VIEW public.vw_grupomenuusuario
AS
SELECT DISTINCT u.id AS usuario_id, gp.id, gp.nome AS grupo, m.nome AS
    modulo, gp.icone, COALESCE(gp.ordem, (99)::bigint) AS ordem
FROM (((((((usuario u JOIN servidor s ON ((u.servidor_id = s.id))) JOIN
    usuarioperfil up ON ((u.id = up.usuario_id))) JOIN perfil p ON ((up.perfil_id = p.id))) JOIN modulo m ON ((p.modulo_id = m.id))) JOIN perfilpagina pp ON ((pp.perfil_id = p.id))) JOIN pagina pg ON ((pp.pagina_id = pg.id))) JOIN grupomenu gp ON ((gp.id = pg.grupomenu_id)))
WHERE ((pg.situacao)::text = 'ATIVO'::text)
ORDER BY COALESCE(gp.ordem, (99)::bigint), u.id, gp.id, gp.nome, m.nome;

--
-- Structure for table empresa (OID = 2298215) : 
--
CREATE TABLE public.empresa (
    id bigserial NOT NULL,
    nome varchar(100) NOT NULL,
    endereco varchar(80),
    bairro varchar(50),
    cidade varchar(50),
    uf char(2),
    cep varchar(9),
    telefone varchar(30),
    fax varchar(30),
    email varchar(100),
    sigla varchar(20),
    dataalteracao timestamp without time zone,
    usuarioalteracao varchar(20)
)
WITH (oids = false);
--
-- Definition for view vw_selecaoempresa (OID = 2298224) : 
--
CREATE VIEW public.vw_selecaoempresa
AS
SELECT u.login, u.servidor_id, u.empresa_id, e.nome AS nome_empresa,
    e.sigla, u.id AS usuario_id
FROM (usuario u JOIN empresa e ON ((e.id = u.empresa_id)))
ORDER BY u.login, e.sigla;

--
-- Definition for view vw_usuariopaginagrupo (OID = 2306292) : 
--
CREATE VIEW public.vw_usuariopaginagrupo
AS
SELECT u.id AS usuario_id, u.login, u.senha, s.nome, s.cpf, s.matricula,
    'EMATER'::character varying AS tipo, s.id AS servidor_id, p.nome AS perfil, m.nome AS modulo, pg.id AS pagina_id, pg.nome AS pagina, pg.arquivo, gp.id AS grupo_id, gp.nome AS grupo, pg.novajanela, pg.arquivoleitura
FROM (((((((usuario u JOIN servidor s ON ((u.servidor_id = s.id))) JOIN
    usuarioperfil up ON ((u.id = up.usuario_id))) JOIN perfil p ON ((up.perfil_id = p.id))) JOIN modulo m ON ((p.modulo_id = m.id))) JOIN perfilpagina pp ON ((pp.perfil_id = p.id))) JOIN pagina pg ON ((pp.pagina_id = pg.id))) JOIN grupomenu gp ON ((gp.id = pg.grupomenu_id)))
WHERE ((pg.situacao)::text = 'ATIVO'::text);

--
-- Structure for table municipio (OID = 2306299) : 
--
CREATE TABLE public.municipio (
    id bigserial NOT NULL,
    territorio_id bigint NOT NULL,
    nome varchar(200),
    dataalteracao timestamp without time zone,
    usuarioalteracao varchar(20),
    regional_id bigint,
    latitude varchar(30),
    longitude varchar(30),
    qtdhabitantes bigint,
    nome_abreviado varchar(15)
)
WITH (oids = false);
--
-- Definition for view vw_perfil (OID = 2314486) : 
--
CREATE VIEW public.vw_perfil
AS
SELECT p.id AS perfil_id, p.nome AS nome_perfil, m.id AS modulo_id, m.nome
    AS nome_modulo
FROM (perfil p JOIN modulo m ON ((m.id = p.modulo_id)));

--
-- Definition for view vw_pagina (OID = 2322675) : 
--
CREATE VIEW public.vw_pagina
AS
SELECT p.id, p.nome, p.situacao, p.arquivo, m.nome AS nome_modulo, g.nome
    AS nome_grupomenu
FROM ((pagina p JOIN modulo m ON ((m.id = p.modulo_id))) JOIN grupomenu g
    ON ((g.id = p.grupomenu_id)))
ORDER BY p.nome;

--
-- Structure for table paginadependencia (OID = 2322681) : 
--
CREATE TABLE public.paginadependencia (
    id bigserial NOT NULL,
    pagina_id bigint NOT NULL,
    paginadependente_id bigint,
    arquivo varchar(100)
)
WITH (oids = false);
--
-- Data for table public.usuario (OID = 2298112) (LIMIT 0,2)
--
INSERT INTO usuario (id, servidor_id, login, senha, expira, dataexpira, dataalteracao, usuarioalteracao, ativo, regional_id, empresa_id)
VALUES (1, 1, '001', '001', 'N', '2010-08-19', NULL, NULL, 'S', 0, 1);

INSERT INTO usuario (id, servidor_id, login, senha, expira, dataexpira, dataalteracao, usuarioalteracao, ativo, regional_id, empresa_id)
VALUES (2, NULL, '001', '001', 'N', NULL, '2015-11-18 10:09:38', '001', 'S', 0, 1);

--
-- Data for table public.acesso_servidor (OID = 2298120) (LIMIT 0,1)
--
INSERT INTO acesso_servidor (id, servidor_id, ultimoacesso, dataacesso, horaacesso)
VALUES (1, 1, NULL, NULL, NULL);

--
-- Data for table public.servidor (OID = 2298139) (LIMIT 0,1)
--
INSERT INTO servidor (id, nome, cpf, matricula, endereco, bairro, cidade, uf, cep, telefone, fax, celular, email, dataadmissao, datanascimento, sexo, estadocivil, naturalidade, carteiratrabalho, pispasep, dataorgaofgts, rg, orgaorg, tipofuncionario, situacao, datasituacao, dataalteracao, usuarioalteracao, numconselho, banco, agencia, contacorrente, nomepai, nomemae, nacionalidade, deficienteauditivo, deficientevisual, deficientefisico, cor, altura, peso, datainclusao, ufnaturalidade, conselho_id, cargo_id, redistribuido, relotado, nivelsalarial_id, quadrosuplementar, aceitaplano, dataaposentadoria, protocoloadesao, dataadesao, nivelsalarialantigo_id, documentosaida, dataredistribuicao, documentoredistribuicao, cargonovo_id, requisitocargo_id, datarelotado, dataquadrosuplementar, documentorelotado, documentoquadrosuplementar, jornada_id, cnh, categoriacnh, validadecnh, endereco2, bairro2, cidade2, cep2, inf_complementares, cedidoorgao, nomecracha)
VALUES (1, 'ADMIN', '1', '001', NULL, 'A', NULL, 'RN', NULL, NULL, NULL, NULL, 'A', '2007-01-15 00:00:00', '2011-01-01 00:00:00', 'M', 'CASADO(A)', NULL, NULL, NULL, NULL, '1', '1', 'EFETIVO', 'DEVOLVIDO(A)', NULL, '2012-10-22 17:09:53.494', '1946005', NULL, NULL, NULL, NULL, 'A', 'A', 'A', 'NAO', 'NAO', 'NAO', NULL, NULL, NULL, '2011-04-05 14:54:49.467', 'RN', 1, NULL, 'NAO', 'NAO', 188, 'NAO', 'NAO', '2011-04-05 00:00:00', NULL, NULL, NULL, 'OFICIO 028/2011-DIGER/UIRH', NULL, NULL, 31, 50, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

--
-- Data for table public.usuarioperfil (OID = 2298151) (LIMIT 0,1)
--
INSERT INTO usuarioperfil (id, perfil_id, usuario_id, dataalteracao, usuarioalteracao)
VALUES (1, 1, 1, NULL, NULL);

--
-- Data for table public.perfil (OID = 2298159) (LIMIT 0,1)
--
INSERT INTO perfil (id, modulo_id, nome, dataalteracao, usuarioalteracao)
VALUES (1, 1, 'ADMINISTRADOR - GU', NULL, NULL);

--
-- Data for table public.modulo (OID = 2298167) (LIMIT 0,1)
--
INSERT INTO modulo (id, nome, dataalteracao, usuarioalteracao)
VALUES (1, 'GU', NULL, NULL);

--
-- Data for table public.pagina (OID = 2298180) (LIMIT 0,5)
--
INSERT INTO pagina (id, modulo_id, nome, situacao, arquivo, dataalteracao, usuarioalteracao, grupomenu_id, novajanela, arquivoleitura)
VALUES (1, 1, 'Modulo', 'ATIVO', 'ModuloList', NULL, NULL, 1, 'NAO', NULL);

INSERT INTO pagina (id, modulo_id, nome, situacao, arquivo, dataalteracao, usuarioalteracao, grupomenu_id, novajanela, arquivoleitura)
VALUES (2, 1, 'Pagina', 'ATIVO', 'PaginaList', NULL, NULL, 1, 'NAO', NULL);

INSERT INTO pagina (id, modulo_id, nome, situacao, arquivo, dataalteracao, usuarioalteracao, grupomenu_id, novajanela, arquivoleitura)
VALUES (4, 1, 'Perfil', 'ATIVO', 'PerfilList', NULL, NULL, 1, 'NAO', NULL);

INSERT INTO pagina (id, modulo_id, nome, situacao, arquivo, dataalteracao, usuarioalteracao, grupomenu_id, novajanela, arquivoleitura)
VALUES (5, 1, 'Usuarios', 'ATIVO', 'UsuarioServidorList', NULL, NULL, 1, 'NAO', NULL);

INSERT INTO pagina (id, modulo_id, nome, situacao, arquivo, dataalteracao, usuarioalteracao, grupomenu_id, novajanela, arquivoleitura)
VALUES (7, 1, 'Grupo Menu', 'ATIVO', 'GrupoMenuList', NULL, NULL, 1, 'NAO', NULL);

--
-- Data for table public.perfilpagina (OID = 2298191) (LIMIT 0,7)
--
INSERT INTO perfilpagina (id, perfil_id, pagina_id, dataalteracao, usuarioalteracao)
VALUES (1, 1, 1, NULL, NULL);

INSERT INTO perfilpagina (id, perfil_id, pagina_id, dataalteracao, usuarioalteracao)
VALUES (2, 1, 2, NULL, NULL);

INSERT INTO perfilpagina (id, perfil_id, pagina_id, dataalteracao, usuarioalteracao)
VALUES (3, 1, 3, NULL, NULL);

INSERT INTO perfilpagina (id, perfil_id, pagina_id, dataalteracao, usuarioalteracao)
VALUES (4, 1, 4, NULL, NULL);

INSERT INTO perfilpagina (id, perfil_id, pagina_id, dataalteracao, usuarioalteracao)
VALUES (5, 1, 5, NULL, NULL);

INSERT INTO perfilpagina (id, perfil_id, pagina_id, dataalteracao, usuarioalteracao)
VALUES (6, 1, 6, NULL, NULL);

INSERT INTO perfilpagina (id, perfil_id, pagina_id, dataalteracao, usuarioalteracao)
VALUES (7, 1, 7, NULL, NULL);

--
-- Data for table public.grupomenu (OID = 2298199) (LIMIT 0,20)
--
INSERT INTO grupomenu (id, nome, dataalteracao, usuarioalteracao, icone, ordem)
VALUES (2, 'RELATORIOS', NULL, NULL, 'fa fa-print', NULL);

INSERT INTO grupomenu (id, nome, dataalteracao, usuarioalteracao, icone, ordem)
VALUES (3, 'GRAFICOS', NULL, NULL, 'fa fa-signal', NULL);

INSERT INTO grupomenu (id, nome, dataalteracao, usuarioalteracao, icone, ordem)
VALUES (4, 'SERVICOS', NULL, NULL, 'fa fa-cogs', NULL);

INSERT INTO grupomenu (id, nome, dataalteracao, usuarioalteracao, icone, ordem)
VALUES (10, 'MAPAS', '2012-04-16 09:37:05', '1503', 'fa fa-map-marker', NULL);

INSERT INTO grupomenu (id, nome, dataalteracao, usuarioalteracao, icone, ordem)
VALUES (13, 'EIXO', '2012-11-20 10:51:18', '1503', 'fa fa-sitemap', NULL);

INSERT INTO grupomenu (id, nome, dataalteracao, usuarioalteracao, icone, ordem)
VALUES (18, 'PLANILHAS', '2013-09-10 12:23:54', '1503', 'fa fa-table', NULL);

INSERT INTO grupomenu (id, nome, dataalteracao, usuarioalteracao, icone, ordem)
VALUES (19, 'TRANSPORTE', '2013-12-12 13:21:10', '1979973', 'fa fa-truck', NULL);

INSERT INTO grupomenu (id, nome, dataalteracao, usuarioalteracao, icone, ordem)
VALUES (6, 'TUTORIAL', '2011-05-05 08:41:52', '0001', 'fa fa-bars', NULL);

INSERT INTO grupomenu (id, nome, dataalteracao, usuarioalteracao, icone, ordem)
VALUES (8, 'BENEFICIARIO', '2011-12-06 08:45:59', '0001', 'fa fa-bars', NULL);

INSERT INTO grupomenu (id, nome, dataalteracao, usuarioalteracao, icone, ordem)
VALUES (11, 'PERMISSOES', '2012-08-27 08:32:48', '2210', 'fa fa-bars', NULL);

INSERT INTO grupomenu (id, nome, dataalteracao, usuarioalteracao, icone, ordem)
VALUES (12, 'DASHBOARD', '2012-06-20 12:13:32', '1979973', 'fa fa-bars', NULL);

INSERT INTO grupomenu (id, nome, dataalteracao, usuarioalteracao, icone, ordem)
VALUES (14, 'ESTRUTURA PPA', '2013-05-08 09:38:52', '1979973', 'fa fa-bars', NULL);

INSERT INTO grupomenu (id, nome, dataalteracao, usuarioalteracao, icone, ordem)
VALUES (15, 'PROG/PROJETOS', '2013-05-08 09:54:13', '1979973', 'fa fa-bars', NULL);

INSERT INTO grupomenu (id, nome, dataalteracao, usuarioalteracao, icone, ordem)
VALUES (17, 'PLANEJAMENTO', '2013-05-08 10:40:25', '1979973', 'fa fa-bars', NULL);

INSERT INTO grupomenu (id, nome, dataalteracao, usuarioalteracao, icone, ordem)
VALUES (20, 'SIAP', '2013-12-12 13:21:24', '1979973', 'fa fa-bars', NULL);

INSERT INTO grupomenu (id, nome, dataalteracao, usuarioalteracao, icone, ordem)
VALUES (5, 'MOVIMENTACAO', '2010-09-21 09:19:56', '0001', 'fa fa-refresh', NULL);

INSERT INTO grupomenu (id, nome, dataalteracao, usuarioalteracao, icone, ordem)
VALUES (16, 'AUTORIZACAO', '2013-05-08 09:55:08', '1503', 'fa fa-gavel', NULL);

INSERT INTO grupomenu (id, nome, dataalteracao, usuarioalteracao, icone, ordem)
VALUES (7, 'PROJETOS', '2011-10-27 09:00:35', '1979973', 'fa fa-bars', 2);

INSERT INTO grupomenu (id, nome, dataalteracao, usuarioalteracao, icone, ordem)
VALUES (9, 'ACOMPANHAMENTO', '2014-09-08 14:24:51', '1979973', 'fa fa-bars', 3);

INSERT INTO grupomenu (id, nome, dataalteracao, usuarioalteracao, icone, ordem)
VALUES (1, 'CADASTROS', '2014-09-08 14:25:07', '1979973', 'fa fa-file-text-o', 1);

--
-- Data for table public.empresa (OID = 2298215) (LIMIT 0,1)
--
INSERT INTO empresa (id, nome, endereco, bairro, cidade, uf, cep, telefone, fax, email, sigla, dataalteracao, usuarioalteracao)
VALUES (1, 'INSTITUTO DE ASSISTENCIA TECNICA E EXTENSAO RURAL DO RIO GRANDE DO NORTE', 'BR 101 KM 94 – BL 5 - CENTRO ADMINISTRATIVO, S/N', 'LAGOA NOVA', 'NATAL', 'RN', '59064901', '(84)3232-2207', NULL, NULL, 'EMATER-RN', NULL, NULL);

--
-- Data for table public.municipio (OID = 2306299) (LIMIT 0,167)
--
INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (45, 6, 'FELIPE GUERRA', '2013-12-05 20:01:16', '1976488', 5, '-5.598347', '-37.695916', 5734, 'F.GUERRA');

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (128, 5, 'SAO JOAO DO SABUGI', '2013-12-19 09:48:26', '1742256', 2, '-6.717812', '-37.202915', 5922, 'S.J.CAMPESTRE');

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (155, 2, 'TENENTE ANANIAS', '2013-11-05 09:44:03', '1742256', 6, '-6.464185', '-38.179228', 9883, 'T.L.CRUZ');

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (147, 7, 'SAO BENTO DO TRAIRI', '2013-11-05 08:13:57', '1742256', 10, '-6.342837', '-36.084552', 3905, 'S.B.DO TRAIRI');

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (83, 2, 'MARCELINO VIEIRA', '2013-11-05 09:41:49', '1742256', 6, '-6.295048', '-38.166418', 8265, 'M.VIEIRA');

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (86, 6, 'MESSIAS TARGINO', '2013-12-02 11:13:06', '1742256', 8, '-6.077278', '-37.512817', 4188, 'M.TARGINO');

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (62, 6, 'JANDUIS', '2012-05-09 11:03:19', '1503', 8, '-6.016789', '-37.406055', 5345, NULL);

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (132, 2, 'SAO MIGUEL', '2012-05-09 12:23:10', '1503', 6, '-6.212954', '-38.496888', 22157, 'S.MIGUEL');

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (85, 3, 'MAXARANGUAPE', '2012-05-09 11:34:11', '1503', 3, '-5.518163', '-35.259429', 10441, 'MAXARANG.');

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (114, 6, 'RAFAEL GODEIRO', '2013-12-02 11:14:49', '1742256', 8, '-6.075608', '-37.717524', 3063, 'R.GODEIRO');

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (59, 6, 'ITAU', '2013-12-04 07:57:36', '1742256', 8, '-5.841084', '-37.992289', 5564, NULL);

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (167, 2, 'VICOSA', '2013-12-04 08:02:43', '1742256', 8, '-5.99148', '-37.946059', 1618, NULL);

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (9, 2, 'ALMINO AFONSO', '2013-12-04 08:19:04', '1742256', 8, '-6.154102', '-37.764816', 4871, 'A.AFONSO');

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (25, 3, 'CAICARA DO NORTE', '2012-05-09 10:34:45', '1503', 3, '-5.061833', '-36.050681', 6016, 'C.DO NORTE');

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (1, 7, 'NATAL', '2014-01-07 07:33:04', '2050498', 9, '-5.794547', '-35.210946', 803739, NULL);

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (44, 7, 'EXTREMOZ', '2012-05-09 10:46:30', '1503', 3, '-5.704537', '-35.293579', 24569, NULL);

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (23, 5, 'BODO', '2014-01-17 14:13:38', '2050498', 4, '-5.987639', '-36.413187', 2425, NULL);

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (78, 8, 'LAJES PINTADAS', '2013-11-05 08:11:44', '1742256', 10, '-6.149494', '-36.116738', 4612, 'L.PINTADA');

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (10, 1, 'ALTO DO RODRIGUES', '2013-12-10 08:01:19', '1742256', 1, '-5.289169', '-36.757979', 12305, 'A.RODRIGUES');

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (131, 5, 'SAO JOSE DO SERIDO', '2014-03-28 12:09:26', '2050498', 2, '-6.448802', '-36.879677', 4231, 'S.J.DO SERIDO');

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (27, 5, 'CAICO', '2014-05-06 11:30:59', '1946137', 2, '-6.459878', '-37.098974', 62709, NULL);

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (48, 2, 'FRANCISCO DANTAS', '2014-03-05 13:22:01', '2050498', 6, '-6.088208', '-38.123492', 2874, 'F.DANTAS');

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (154, 7, 'TANGARA', '2014-02-20 08:47:57', '2050498', 10, '-6.199435', '-35.800946', 14175, NULL);

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (93, 5, 'OURO BRANCO', '2014-02-03 12:24:47', '2050498', 2, '-6.701125', '-36.946174', 4699, 'O.BRANCO');

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (70, 5, 'JUCURUTU', '2014-02-20 08:31:20', '2050498', 2, '-6.034864', '-37.021158', 17692, NULL);

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (77, 10, 'LAJES', '2015-04-21 17:08:30', '1979973', 1, '-5.698003', '-36.242834', 10381, NULL);

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (43, 9, 'ESPIRITO SANTO', '2014-03-06 10:18:08', '2050498', 11, '-6.332723', '-35.310295', 10475, 'ESP. SANTO');

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (102, 2, 'PAU DOS FERROS', '2014-03-06 10:00:49', '2050498', 6, '-6.120372', '-38.206233', 27745, 'P.FERROS');

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (165, 9, 'VERA CRUZ', '2014-02-20 08:46:01', '2050498', 11, '-6.046024', '-35.430382', 10719, NULL);

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (129, 8, 'SAO JOSE DE CAMPESTRE', '2015-04-21 17:10:56', '1979973', 10, '-6.316114', '-35.712706', 12356, 'S.J.DO SABUGI');

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (79, 2, 'LUCRECIA', '2014-06-04 12:34:31', '1946137', 8, '-6.116937', '-37.814169', 3633, NULL);

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (74, 4, 'LAGOA DE VELHOS', '2014-05-06 11:26:52', '1946137', 7, '-6.00358', '-35.870394', 2668, 'L.DE VELHOS');

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (2, 4, 'BOM JESUS', '2014-03-05 13:19:47', '2050498', 7, '-5.985089', '-35.583837', 9440, NULL);

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (145, 7, 'SITIO NOVO', '2014-02-20 08:48:26', '2050498', 10, '-6.104882', '-35.91023', 5020, NULL);

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (65, 5, 'JARDIM DE PIRANHAS', '2014-03-06 09:32:59', '2050498', 2, '-6.379149', '-37.35078', 13506, 'J.DE PIRANHAS');

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (20, 4, 'BARCELONA', '2014-03-05 13:19:09', '2050498', 7, '-5.950388', '-35.928061', 3950, NULL);

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (71, 9, 'JUNDIA', '2014-03-05 13:22:50', '2050498', 11, '-6.242114', '-35.347127', 3582, NULL);

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (52, 6, 'GOVERNADOR DIX SEPT ROSADO', '2014-04-15 15:37:39', '1976488', 5, '-5.459328', '-37.519555', 12374, 'G.D.S.ROSADO');

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (106, 9, 'PEDRO VELHO', '2014-03-25 08:37:45', '2050498', 11, '-6.438258', '-35.220151', 14114, 'P.VELHO');

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (99, 8, 'PASSA E FICA', '2015-04-21 17:10:42', '1979973', 10, '-6.435507', '-35.6435', 11100, NULL);

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (95, 6, 'PARAU', '2013-12-04 14:54:58', '1742256', 1, '-5.776316', '-37.100744', 3859, NULL);

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (103, 3, 'PEDRA GRANDE', '2012-05-09 11:57:16', '1503', 3, '-5.150348', '-35.877979', 3521, 'P.GRANDE');

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (81, 7, 'MACAU', '2012-05-09 11:30:04', '1503', 1, '-5.112851', '-36.634963', 28954, NULL);

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (80, 2, 'LUIZ GOMES', '2012-05-09 11:28:43', '1503', 6, '-6.415549', '-38.38681', 9610, NULL);

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (139, 7, 'SERRA DE SAO BENTO', '2012-05-09 12:13:40', '1503', 10, '-6.419249', '-35.705738', 5743, 'S.SAO BENTO');

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (34, 3, 'CEARA-MIRIM', '2012-05-08 10:34:01', '1503', 3, '-5.635205', '-35.42034', 68141, NULL);

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (133, 3, 'SAO MIGUEL DO GOSTOSO', '2012-05-09 12:21:16', '1503', 3, '-5.12319', '-35.634617', 8670, 'S.M.TOUROS');

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (54, 7, 'GUAMARE', '2012-05-09 10:56:44', '1503', 3, '-5.107348', '-36.319669', 12404, NULL);

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (37, 2, 'CORONEL JOAO PESSOA', '2012-05-09 10:42:06', '1503', 6, '-6.261188', '-38.44319', 4772, 'CEL JOAO PES');

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (149, 7, 'SAO GONCALO DO AMARANTE', '2012-05-09 12:31:37', '1503', 3, '-5.793435', '-35.328619', 87668, 'S.G.AMARANTE');

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (108, 2, 'PILOES', '2014-01-07 07:35:38', '2050498', 6, '-6.269538', '-38.042811', 3453, NULL);

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (157, 1, 'TIBAU', '2013-12-05 20:03:37', '1976488', 5, '-4.840383', '-37.255003', 3687, NULL);

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (94, 2, 'PARANA', '2013-11-05 09:42:09', '1742256', 6, '-6.483726', '-38.312132', 3952, NULL);

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (14, 1, 'AREIA BRANCA', '2013-12-05 19:59:06', '1976488', 5, '-4.954998', '-37.130034', 25315, 'A.BRANCA');

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (53, 1, 'GROSSOS', '2013-12-05 20:01:45', '1976488', 5, '-4.981142', '-37.153552', 9393, NULL);

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (136, 5, 'SAO VICENTE', '2013-11-27 19:21:07', '1742256', 4, '-6.217556', '-36.685023', 6028, NULL);

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (109, 3, 'POCO BRANCO', '2012-05-09 12:48:26', '1503', 3, '-5.621175', '-35.662587', 13949, 'P.BRANCO');

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (50, 7, 'GALINHOS', '2012-05-09 10:52:00', '1503', 3, '-5.092943', '-36.275112', 2159, NULL);

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (146, 3, 'SAO BENTO DO NORTE', '2012-05-09 12:25:31', '1503', 3, '-5.064345', '-36.040242', 2975, 'S.B.DO NORTE');

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (3, 7, 'MACAIBA', '2014-03-05 13:23:06', '2050498', 3, '-5.890627', '-35.365677', 69467, NULL);

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (49, 2, 'FRUTUOSO GOMES', '2014-06-04 12:34:05', '1946137', 8, '-6.159468', '-37.8423', 4233, 'F.GOMES');

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (11, 10, 'ANGICOS', '2015-04-21 17:08:01', '1979973', 1, '-5.666594', '-36.60078', 11549, NULL);

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (18, 9, 'SENADOR GEORGINO AVELINO', '2014-02-20 08:51:50', '2050498', 11, '-6.162518', '-35.122679', 3924, 'S.G.AVELINO');

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (169, 9, 'TIBAU DO SUL', '2014-02-20 08:47:37', '2050498', 11, '-6.188673', '-35.089989', 11385, 'T.DO SUL');

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (88, 9, 'MONTE ALEGRE', '2014-03-06 10:15:28', '2050498', 11, '-6.067095', '-35.334091', 20685, 'M.ALEGRE');

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (73, 9, 'LAGOA DE PEDRAS', '2014-03-31 09:46:41', '2050498', 11, '-6.151179', '-35.43668', 6989, 'L.DE PEDRAS');

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (166, 9, 'VILA FLOR', '2014-03-05 13:27:16', '2050498', 11, '-6.315554', '-35.075623', 2872, NULL);

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (150, 4, 'SAO PEDRO', '2014-03-05 13:30:13', '2050498', 7, '-5.89592', '-35.634751', 6235, NULL);

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (138, 7, 'SERRA CAIADA', '2014-02-20 08:51:16', '2050498', 7, '-6.104637', '-35.710952', 8768, 'S.CAIADA');

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (105, 10, 'PEDRO AVELINO', '2015-04-21 17:08:45', '1979973', 1, '-5.520811', '-36.386558', 7171, 'P.AVELINO');

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (8, 2, 'ALEXANDRIA', '2014-03-05 13:18:22', '2050498', 6, '-6.415016', '-38.012867', 13507, NULL);

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (40, 2, 'DOUTOR SEVERIANO', '2014-03-06 09:55:47', '2050498', 6, '-6.0951', '-38.376644', 6492, 'DR.SEVERIAN');

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (7, 2, 'AGUA NOVA', '2014-03-05 13:18:14', '2050498', 6, '-6.206986', '-38.292675', 2980, NULL);

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (46, 10, 'FERNANDO PEDROZA', '2015-04-21 17:08:21', '1979973', 1, '-5.697523', '-36.531558', 2854, 'F.PEDROZA');

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (12, 2, 'ANTONIO MARTINS', '2014-03-05 13:18:45', '2050498', 8, '-6.216084', '-37.889518', 6907, 'A.MARTINS');

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (72, 9, 'LAGOA DANTA', '2014-03-25 08:37:14', '2050498', 11, '-6.370848', '-35.616962', 6227, 'L.DANTA');

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (55, 4, 'IELMO MARINHO', '2014-03-05 13:22:29', '2050498', 7, '-5.821872', '-35.552262', 12171, 'I.MARINHO');

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (125, 9, 'SANTO ANTONIO', '2014-04-29 23:33:07', '1946137', 11, '-6.313635', '-35.478866', 22216, 'S.ANTONIO');

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (58, 1, 'ITAJA', '2014-05-06 11:32:31', '1946137', 1, '-5.637682', '-36.87089', 6932, NULL);

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (17, 9, 'BAIA FORMOSA', '2014-03-05 13:18:57', '2050498', 11, '-6.370966', '-35.006003', 8573, 'B.FORMOSA');

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (64, 7, 'JARDIM DE ANGICOS', '2012-05-09 11:04:36', '1503', 3, '-5.651401', '-35.970311', 2607, 'J.DE ANGICOS');

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (67, 3, 'JOAO CAMARA', '2012-05-09 11:07:04', '1503', 3, '-5.537481', '-35.813541', 32227, 'J.CAMARA');

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (96, 3, 'PARAZINHO', '2012-05-09 11:49:40', '1503', 3, '-5.225644', '-35.840213', 4845, NULL);

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (84, 2, 'MARTINS', '2012-05-09 11:33:06', '1503', 8, '-6.088699', '-37.911265', 8218, NULL);

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (112, 3, 'PUREZA', '2012-05-09 12:46:03', '1503', 3, '-5.468054', '-35.556167', 8424, NULL);

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (31, 6, 'CARAUBAS', '2013-12-05 20:00:30', '1976488', 5, '-5.785218', '-37.559724', 19576, 'CARAUBAS');

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (110, 2, 'PORTALEGRE', '2014-01-07 07:36:03', '2050498', 6, '-6.025005', '-37.987289', 7320, NULL);

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (101, 6, 'PATU', '2013-12-19 07:40:00', '1742256', 8, '-6.110195', '-37.638645', 11964, NULL);

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (113, 2, 'RAFAEL FERNANDES', '2013-11-05 09:42:46', '1742256', 6, '-6.189281', '-38.223935', 4692, 'R.FERNANDES');

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (119, 6, 'RODOLFO FERNANDES', '2013-12-04 08:00:23', '1742256', 8, '-5.791292', '-38.060267', 4418, 'R.FERNANDES');

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (19, 1, 'BARAUNA', '2013-12-05 19:58:43', '1976488', 5, '-5.079189', '-37.617381', 24182, NULL);

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (162, 6, 'UPANEMA', '2013-12-05 20:03:59', '1976488', 5, '-5.643266', '-37.259037', 12992, NULL);

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (115, 2, 'RIACHO DA CRUZ', '2013-12-04 08:00:51', '1742256', 8, '-5.936579', '-37.948848', 3165, 'R.DA CRUZ');

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (107, 1, 'PENDENCIAS', '2013-12-04 14:55:51', '1742256', 1, '-5.257237', '-36.718111', 13432, NULL);

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (148, 2, 'SAO FRANCISCO DO OESTE', '2014-01-07 07:39:40', '2050498', 6, '-5.999504', '-38.169047', 3874, 'S.F.DO OESTE');

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (116, 2, 'RIACHO DE SANTANA', '2012-05-09 12:42:38', '1503', 6, '-6.264259', '-38.314626', 4156, 'R.DE SANTANA');

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (98, 7, 'PARNAMIRIM', '2012-05-09 11:51:19', '1503', 11, '-5.909922', '-35.254354', 202456, NULL);

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (153, 3, 'TAIPU', '2012-05-09 12:08:28', '1503', 3, '-5.619392', '-35.596572', 11836, NULL);

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (82, 2, 'MAJOR SALES', '2012-05-09 11:30:41', '1503', 6, '-6.406945', '-38.323327', 3536, 'M.SALES');

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (118, 3, 'RIO DO FOGO', '2012-05-09 12:41:31', '1503', 3, '-5.271745', '-35.382854', 10059, NULL);

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (111, 1, 'PORTO DO MANGUE', '2012-05-09 12:47:18', '1503', 1, '-5.068203', '-36.779566', 5217, 'P. DO MANGUE');

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (97, 5, 'PARELHAS', '2013-12-12 07:23:19', '1742256', 2, '-6.686442', '-36.658083', 20354, NULL);

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (163, 9, 'VARZEA', '2014-02-20 08:46:47', '2050498', 11, '-6.348691', '-35.375333', 5236, NULL);

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (142, 9, 'SERRINHA', '2014-02-20 08:48:59', '2050498', 11, '-6.277494', '-35.500683', 6581, NULL);

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (135, 4, 'SAO TOME', '2014-05-06 11:28:09', '1946137', 7, '-5.974808', '-36.073796', 10827, NULL);

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (51, 9, 'GOIANINHA', '2014-03-05 13:21:45', '2050498', 11, '-6.26724', '-35.207534', 22481, NULL);

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (61, 3, 'JANDAIRA', '2014-03-05 13:21:14', '2050498', 3, '-5.356106', '-36.127338', 6801, NULL);

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (91, 9, 'NISIA FLORESTA', '2014-03-05 13:25:41', '2050498', 11, '-6.09064', '-35.207598', 23784, 'N.FLORESTA');

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (21, 3, 'BENTO FERNANDES', '2014-03-05 13:19:35', '2050498', 3, '-5.692729', '-35.82073', 5113, 'B.FERNANDES');

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (122, 4, 'SANTA MARIA', '2014-05-06 11:31:28', '1946137', 7, '-5.841703', '-35.69193', 4762, NULL);

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (69, 2, 'JOSE DA PENHA', '2014-03-05 13:23:22', '2050498', 6, '-6.319447', '-38.285347', 5868, 'J.DA PENHA');

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (130, 9, 'SAO JOSE DE MIPIBU', '2014-03-05 13:31:26', '2050498', 11, '-6.072696', '-35.239946', 39776, 'S.J.DE MIPIBU');

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (4, 4, 'SAO PAULO DO POTENGI', '2014-05-06 11:27:39', '1946137', 7, '-5.893727', '-35.766479', 15843, 'S.P.POTENGI');

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (15, 9, 'ARES', '2014-02-20 08:40:36', '2050498', 11, '-6.196694', '-35.161979', 12924, NULL);

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (104, 10, 'PEDRA PRETA', '2015-04-21 17:08:41', '1979973', 1, '-5.581614', '-36.104604', 2590, 'P.PRETA');

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (60, 8, 'JACANA', '2015-04-21 17:10:40', '1979973', 10, '-6.426488', '-36.202161', 7925, NULL);

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (35, 5, 'CERRO CORA', '2014-03-06 09:29:44', '2050498', 4, '-6.047464', '-36.349683', 10916, NULL);

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (28, 6, 'CAMPO GRANDE', '2012-05-09 10:36:23', '1503', 1, '-5.862622', '-37.311888', 9289, 'C.GRANDE');

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (13, 6, 'APODI', '2013-12-05 19:58:32', '1976488', 5, '-5.661779', '-37.800586', 34763, NULL);

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (56, 1, 'IPANGUACU', '2013-12-04 14:54:05', '1742256', 1, '-5.510698', '-36.86029', 13856, NULL);

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (140, 1, 'SERRA DO MEL', '2013-12-05 20:02:53', '1976488', 5, '-5.173823', '-37.041425', 10287, 'S.DO MEL');

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (152, 2, 'TABOLEIRO GRANDE', '2013-12-04 08:02:00', '1742256', 8, '-5.928255', '-38.044603', 2317, 'T.GRANDE');

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (156, 5, 'TENENTE LAURENTINO CRUZ', '2013-12-05 06:58:21', '1742256', 4, '-6.147776', '-36.718926', 5406, 'T.ANANIAS');

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (143, 2, 'SERRINHA DOS PINTOS', '2014-01-07 07:42:57', '2050498', 8, '-6.112248', '-37.956761', 4540, 'S.DOS PINTOS');

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (164, 2, 'VENHA VER', '2014-01-07 07:44:54', '2050498', 6, '-6.323307', '-38.488337', 3821, NULL);

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (159, 3, 'TOUROS', '2012-05-09 12:03:31', '1503', 3, '-5.196668', '-35.46567', 31089, NULL);

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (89, 7, 'MONTE DAS GAMELEIRAS', '2012-05-09 11:40:20', '1503', 10, '-6.439623', '-35.785539', 2261, 'M.GAMELEIRAS');

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (168, 1, 'MOSSORO', '2014-01-15 11:39:25', '1976488', 5, '-5.249598', '-37.277985', 259815, NULL);

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (63, 7, 'JAPI', '2013-11-05 08:10:58', '1742256', 10, '-6.465966', '-35.946429', 5522, NULL);

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (68, 2, 'JOAO DIAS', '2013-12-19 07:39:41', '1742256', 8, '-6.275895', '-37.796413', 2601, NULL);

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (121, 8, 'SANTA CRUZ', '2014-01-22 12:01:14', '2050498', 10, '-6.227038', '-36.021938', 35797, NULL);

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (87, 9, 'MONTANHAS', '2014-04-26 01:37:24', '1946137', 11, '-6.486657', '-35.28697', 11413, NULL);

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (22, 7, 'BOA SAUDE', '2014-02-20 08:41:57', '2050498', 7, '-6.156278', '-35.605273', 9011, 'B.SAUDE');

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (100, 9, 'PASSAGEM', '2014-03-10 08:14:41', '2050498', 11, '-6.279595', '-35.377522', 2895, NULL);

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (76, 9, 'LAGOA SALGADA', '2014-03-31 09:47:25', '2050498', 11, '-6.120841', '-35.476742', 7564, 'L.SALGADA');

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (26, 10, 'CAICARA DO RIO DOS VENTOS', '2015-04-21 17:08:18', '1979973', 7, '-5.757131', '-36.000871', 3308, 'C.RIO DO VENTO');

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (117, 4, 'RIACHUELO', '2014-05-06 11:25:40', '1946137', 7, '-5.810217', '-35.823219', 7067, NULL);

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (90, 9, 'NOVA CRUZ', '2014-04-02 07:44:15', '1946137', 11, '-6.479494', '-35.432024', 35490, NULL);

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (6, 10, 'AFONSO BEZERRA', '2015-04-21 17:08:06', '1979973', 1, '-5.500681', '-36.505251', 10844, 'A.BEZERRA');

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (161, 6, 'UMARIZAL', '2014-04-22 10:31:24', '1946137', 8, '-5.988493', '-37.818439', 10659, NULL);

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (124, 5, 'SANTANA DO SERIDO', '2014-05-19 17:19:08', '1946137', 2, '-6.770211', '-36.733367', 2526, 'S.DO SERIDO');

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (57, 5, 'IPUEIRA', '2014-03-06 09:33:24', '2050498', 2, '-6.814668', '-37.199224', 2077, NULL);

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (141, 5, 'SERRA NEGRA DO NORTE', '2014-03-06 09:37:13', '2050498', 2, '-6.664672', '-37.398255', 7770, 'S.N.DO NORTE');

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (144, 6, 'SEVERIANO MELO', '2014-03-31 09:46:19', '2050498', 5, '-5.782059', '-37.957764', 5752, 'S.MELO');

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (30, 9, 'CANGUARETAMA', '2014-03-05 13:20:07', '2050498', 11, '-6.380428', '-35.128956', 30916, 'CANGUARETA.');

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (120, 4, 'RUY BARBOSA', '2014-03-05 13:32:03', '2050498', 7, '-5.881064', '-35.934992', 3595, NULL);

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (16, 1, 'ASSU', '2014-05-06 11:31:45', '1946137', 1, '-5.577400', '-36.91493', 53227, NULL);

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (24, 9, 'BREJINHO', '2014-02-20 08:42:29', '2050498', 11, '-6.194912', '-35.361557', 11577, NULL);

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (29, 8, 'CAMPO REDONDO', '2015-04-21 17:10:34', '1979973', 10, '-6.241714', '-36.183193', 10266, 'C.REDONDO');

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (38, 5, 'CRUZETA', '2014-02-19 12:38:02', '2050498', 4, '-6.412031', '-36.78758', 7967, NULL);

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (123, 5, 'SANTANA DO MATOS', '2014-03-06 09:27:49', '2050498', 4, '-5.95043', '-36.651329', 13809, 'S.DOS MATOS');

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (134, 1, 'SAO RAFAEL', '2014-05-06 11:32:10', '1946137', 1, '-5.802857', '-36.885417', 8111, NULL);

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (5, 5, 'ACARI', '2014-03-28 12:14:45', '2050498', 4, '-6.433247', '-36.639855', 11035, NULL);

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (42, 5, 'EQUADOR', '2014-02-19 12:35:45', '2050498', 2, '-6.945491', '-36.717135', 5822, NULL);

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (158, 5, 'TIMBAUBA DOS BATISTAS', '2014-02-19 12:36:52', '2050498', 2, '-6.463897', '-37.273446', 2295, 'T.BATISTA');

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (47, 5, 'FLORANIA', '2014-02-19 12:39:17', '2050498', 4, '-6.123722', '-36.812954', 8959, NULL);

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (32, 5, 'CARNAUBA DOS DANTAS', '2014-02-19 12:40:15', '2050498', 4, '-6.555922', '-36.592669', 7429, 'C.DOS DANTAS');

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (126, 5, 'SAO FERNANDO', '2014-02-19 12:40:47', '2050498', 2, '-6.377976', '-37.185953', 3401, 'S.FERNANDO');

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (92, 6, 'OLHO DAGUA DO BORGES', '2014-02-24 10:12:01', '2050498', 8, '-5.956545', '-37.707556', 4295, 'O.DAGUA');

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (33, 1, 'CARNAUBAIS', '2014-03-05 13:20:19', '2050498', 1, '-5.339015', '-36.834133', 9762, NULL);

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (41, 2, 'ENCANTO', '2014-03-05 13:22:12', '2050498', 6, '-6.113534', '-38.311676', 5231, NULL);

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (160, 6, 'TRIUNFO POTIGUAR', '2014-03-06 09:25:19', '2050498', 1, '-5.866795', '-37.187734', 3368, 'T.POTIGUAR');

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (137, 4, 'SENADOR ELOI DE SOUZA', '2014-03-06 10:11:37', '2050498', 7, '-6.036304', '-35.692922', 5637, 'S.E.SOUZA');

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (75, 5, 'LAGOA NOVA', '2014-03-12 10:14:14', '2050498', 4, '-6.093883', '-36.47212', 13983, 'L.NOVA');

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (66, 5, 'JARDIM DO SERIDO', '2014-04-02 13:12:06', '2050498', 2, '-6.590306', '-36.773944', 12113, 'J.DO SERIDO');

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (39, 5, 'CURRAIS NOVOS', '2014-05-09 15:27:45', '1946137', 4, '-6.263001', '-36.517611', 42652, 'C.NOVOS');

INSERT INTO municipio (id, territorio_id, nome, dataalteracao, usuarioalteracao, regional_id, latitude, longitude, qtdhabitantes, nome_abreviado)
VALUES (36, 8, 'CORONEL EZEQUIEL', '2015-04-21 17:10:35', '1979973', 10, '-6.384629', '-36.215208', 5405, 'CEL EZEQUIEL');

--
-- Definition for index usuario_pkey (OID = 2298118) : 
--
ALTER TABLE ONLY usuario
    ADD CONSTRAINT usuario_pkey
    PRIMARY KEY (id);
--
-- Definition for index acesso_servidor_pkey (OID = 2298123) : 
--
ALTER TABLE ONLY acesso_servidor
    ADD CONSTRAINT acesso_servidor_pkey
    PRIMARY KEY (id);
--
-- Definition for index servidor_pkey (OID = 2298147) : 
--
ALTER TABLE ONLY servidor
    ADD CONSTRAINT servidor_pkey
    PRIMARY KEY (id);
--
-- Definition for index usuarioperfil_pkey (OID = 2298155) : 
--
ALTER TABLE ONLY usuarioperfil
    ADD CONSTRAINT usuarioperfil_pkey
    PRIMARY KEY (id, perfil_id, usuario_id);
--
-- Definition for index perfil_pkey (OID = 2298163) : 
--
ALTER TABLE ONLY perfil
    ADD CONSTRAINT perfil_pkey
    PRIMARY KEY (id);
--
-- Definition for index modulo_pkey (OID = 2298171) : 
--
ALTER TABLE ONLY modulo
    ADD CONSTRAINT modulo_pkey
    PRIMARY KEY (id);
--
-- Definition for index pagina_pkey (OID = 2298186) : 
--
ALTER TABLE ONLY pagina
    ADD CONSTRAINT pagina_pkey
    PRIMARY KEY (id);
--
-- Definition for index perfilpagina_pkey (OID = 2298195) : 
--
ALTER TABLE ONLY perfilpagina
    ADD CONSTRAINT perfilpagina_pkey
    PRIMARY KEY (id, perfil_id, pagina_id);
--
-- Definition for index grupomenu_pkey (OID = 2298204) : 
--
ALTER TABLE ONLY grupomenu
    ADD CONSTRAINT grupomenu_pkey
    PRIMARY KEY (id);
--
-- Definition for index empresa_pkey (OID = 2298222) : 
--
ALTER TABLE ONLY empresa
    ADD CONSTRAINT empresa_pkey
    PRIMARY KEY (id);
--
-- Definition for index municipio_pkey (OID = 2306303) : 
--
ALTER TABLE ONLY municipio
    ADD CONSTRAINT municipio_pkey
    PRIMARY KEY (id);
--
-- Definition for index municipio_nome_key (OID = 2306305) : 
--
ALTER TABLE ONLY municipio
    ADD CONSTRAINT municipio_nome_key
    UNIQUE (nome);
--
-- Definition for index paginadependencia_pkey (OID = 2322685) : 
--
ALTER TABLE ONLY paginadependencia
    ADD CONSTRAINT paginadependencia_pkey
    PRIMARY KEY (id);
--
-- Data for sequence public.usuario_id_seq (OID = 2298110)
--
SELECT pg_catalog.setval('usuario_id_seq', 2, true);
--
-- Data for sequence public.servidor_id_seq (OID = 2298137)
--
SELECT pg_catalog.setval('servidor_id_seq', 1, false);
--
-- Data for sequence public.usuarioperfil_id_seq (OID = 2298149)
--
SELECT pg_catalog.setval('usuarioperfil_id_seq', 1, false);
--
-- Data for sequence public.perfil_id_seq (OID = 2298157)
--
SELECT pg_catalog.setval('perfil_id_seq', 1, false);
--
-- Data for sequence public.modulo_id_seq (OID = 2298165)
--
SELECT pg_catalog.setval('modulo_id_seq', 1, false);
--
-- Data for sequence public.pagina_id_seq (OID = 2298178)
--
SELECT pg_catalog.setval('pagina_id_seq', 1, false);
--
-- Data for sequence public.perfilpagina_id_seq (OID = 2298189)
--
SELECT pg_catalog.setval('perfilpagina_id_seq', 6, true);
--
-- Data for sequence public.grupomenu_id_seq (OID = 2298197)
--
SELECT pg_catalog.setval('grupomenu_id_seq', 21, true);
--
-- Data for sequence public.empresa_id_seq (OID = 2298213)
--
SELECT pg_catalog.setval('empresa_id_seq', 1, false);
--
-- Data for sequence public.municipio_id_seq (OID = 2306297)
--
SELECT pg_catalog.setval('municipio_id_seq', 1, false);
--
-- Data for sequence public.paginadependencia_id_seq (OID = 2322679)
--
SELECT pg_catalog.setval('paginadependencia_id_seq', 1, false);
--
-- Comments
--
COMMENT ON SCHEMA public IS 'standard public schema';
