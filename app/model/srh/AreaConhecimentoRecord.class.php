<?php

/*
 * classe AreaConhecimentoRecord
 * Active Record para tabela Areaconhecimento
 */

class AreaConhecimentoRecord extends TRecord {

    const TABLENAME = 'areaconhecimento';
    const PRIMARYKEY = 'id';
    const IDPOLICY = 'serial'; // {max, serial}

}
?>

