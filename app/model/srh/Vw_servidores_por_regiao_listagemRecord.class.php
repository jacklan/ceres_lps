<?php
/*
 * classe Vw_servidores_por_regiao_listagemRecord
 * Active Record para a view Vw_servidores_por_regiao_listagem
 */
class Vw_servidores_por_regiao_listagemRecord extends TRecord
{
    const TABLENAME = 'Vw_servidores_por_regiao_listagem';
    const PRIMARYKEY = 'id';
    const IDPOLICY = 'serial'; // {max, serial}
}
?>
