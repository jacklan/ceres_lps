<?php
/*
 * classe ServidorCapacitacaoRecord
 * Active Record para tabela Servidorcapacitacao
 */
class ServidorCapacitacaoRecord extends TRecord
{
	
	const TABLENAME = 'servidorcapacitacao';
    const PRIMARYKEY = 'id';
    const IDPOLICY = 'serial'; // {max, serial}
	
   private $capacitacao;

    function get_nome_capacitacao()
    {
        //instancia capacitacaoRecord
        //carrega na memoria a empresa de codigo $this->empresa_id
        if (empty ($this->capacitacao)){
           $this->capacitacao = new CapacitacaoRecord($this->capacitacao_id);
        }
        //retorna o objeto instanciado
        return $this->capacitacao->nome;
    }

}
?>