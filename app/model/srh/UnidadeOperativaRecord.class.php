<?php

class UnidadeOperativaRecord extends TRecord
{

       const TABLENAME = 'unidadeoperativa';
       const PRIMARYKEY = 'id';
       const IDPOLICY = 'serial'; // {max, serial}

       private $regional;
       private $municipio;
       private $responsavel;

    /*
     * metodo get_nome_regional()
     * executado sempre que for acessada a propriedade nome_servidor
     */
    function get_nome_regional()
    {
        //instancia regionalRecord
        //carrega na memoria o regional
        if (empty ($this->regional)){
           $this->regional = new RegionalRecord($this->regional_id);
        }
        //retorna o objeto instanciado
        return $this->regional->nome;
    }

        function get_nome_municipio()
    {
        //instancia regionalRecord
        //carrega na memoria o regional
        if (empty ($this->municipio)){
           $this->municipio = new MunicipioRecord($this->municipio_id);
        }
        //retorna o objeto instanciado
        return $this->municipio->nome;
    }

    /*
     * metodo get_nome_responsavel()
     * executado sempre que for acessada a propriedade nome_responsavel
     */
    function get_nome_responsavel()
    {
        //instancia servidorRecord
        //carrega na memoria o responsavel
        if (empty ($this->responsavel)){
           $this->responsavel = new ServidorRecord($this->responsavel_id);
        }
        //retorna o objeto instanciado
        return $this->responsavel->nome;
    }

    /*
     * metodo get_email_responsavel()
     * executado sempre que for acessada a propriedade email_responsavel
     */
    function get_email_responsavel()
    {
        //instancia servidorRecord
        //carrega na memoria o responsavel
        if (empty ($this->responsavel)){
           $this->responsavel = new ServidorRecord($this->responsavel_id);
        }
        //retorna o objeto instanciado
        return $this->responsavel->email;
    }


}
?>

