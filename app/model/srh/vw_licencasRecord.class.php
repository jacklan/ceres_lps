<?php
/*
 * classe AcaoRecord
 * Active Record para tabela Curso
 */
class vw_licencasRecord extends TRecord
{
	const TABLENAME = 'vw_licencas';
	const PRIMARYKEY = 'id';
	const IDPOLICY = 'serial'; // {max, serial}
}
?>