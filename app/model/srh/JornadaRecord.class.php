<?php

/*
 * classe JornadaRecord
 * Active Record para tabela Jornada
 */

class JornadaRecord extends TRecord {

    const TABLENAME = 'jornada';
    const PRIMARYKEY = 'id';
    const IDPOLICY = 'serial'; // {max, serial}

    function get_link() {

        $arquivo = '<a href="?class=ServidorJornadaList&jornada_id=' . $this->id . '" target=_blank>(Visualizar)</a>';

        return $arquivo;
    }

}
?>

