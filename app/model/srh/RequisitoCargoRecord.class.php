<?php
/*
 * classe RequisitoCargoRecord
 * Active Record para tabela Curso
 */
class RequisitoCargoRecord extends TRecord
{
const TABLENAME = 'requisitocargo';
    const PRIMARYKEY= 'id';
    const IDPOLICY =  'max'; // {max, serial}
}
?>