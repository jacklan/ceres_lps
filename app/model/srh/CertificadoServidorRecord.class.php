<?php

/*
 * classe CertificadoServidorRecord
 * Active Record para tabela CertificadoServidor
 * Autor: Jackson Meires
 */

class CertificadoServidorRecord extends TRecord {

    const TABLENAME = 'srh_certificado_servidor';
    const PRIMARYKEY = 'id';
    const IDPOLICY = 'serial'; // {max, serial}

    private $participante;
    private $certificado;

    /*
     * metodo get_nome_servidor()
     * executado sempre que for acessada a propriedade nome_servidor
     */

    function get_nome_participante() {
        //instancia servidorRecord
        //carrega na memoria a servidor de codigo $this->servidor_id
        if (empty($this->participante)) {
            if (!empty($this->servidor_id)) {
                $this->participante = new ServidorRecord($this->servidor_id);
            } else if (!empty($this->produtor_id)) {
                $this->participante = new ProdutorRecord($this->produtor_id);
            } else if (!empty($this->srh_convidado_id)) {
                $this->participante = new ConvidadoRecord($this->srh_convidado_id);
            }
        }
        //retorna o objeto instanciado
        return $this->participante->nome;
    }
    
    function get_cpf_participante() {
        //instancia servidorRecord
        //carrega na memoria a servidor de codigo $this->servidor_id
        if (empty($this->participante)) {
            if (!empty($this->servidor_id)) {
                $this->participante = new ServidorRecord($this->servidor_id);
            } else if (!empty($this->produtor_id)) {
                $this->participante = new ProdutorRecord($this->produtor_id);
            } else if (!empty($this->srh_convidado_id)) {
                $this->participante = new ConvidadoRecord($this->srh_convidado_id);
            }
        }
        //retorna o objeto instanciado
        return $this->participante->cpf;
    }

    /*
     * metodo get_nome_curso()
     * executado sempre que for acessada a propriedade nome_curso
     */

    function get_nome_curso() {
        //instancia CertificadoRecord
        //carrega na memoria a certificado de codigo $this->srh_certificado_id
        if (empty($this->certificado)) {
            $this->certificado = new CertificadoRecord($this->srh_certificado_id);
        }
        //retorna o objeto instanciado
        return $this->certificado->curso;
    }

    /*
     * metodo get_datainicio()
     * executado sempre que for acessada a propriedade datainicio
     */

    function get_datainicio() {
        //instancia CertificadoRecord
        //carrega na memoria a certificado de codigo $this->srh_certificado_id
        if (empty($this->certificado)) {
            $this->certificado = new CertificadoRecord($this->srh_certificado_id);
        }
        //retorna o objeto instanciado
        return TDate::date2br($this->certificado->datainicio);
    }

    /*
     * metodo get_datafim()
     * executado sempre que for acessada a propriedade datafim
     */

    function get_datafim() {
        //instancia CertificadoRecord
        //carrega na memoria a certificado de codigo $this->srh_certificado_id
        if (empty($this->certificado)) {
            $this->certificado = new CertificadoRecord($this->srh_certificado_id);
        }
        //retorna o objeto instanciado
        return TDate::date2br($this->certificado->datafim);
    }

    /*
     * metodo get_cargahoraria()
     * executado sempre que for acessada a propriedade cargahoraria
     */

    function get_cargahoraria() {
        //instancia CertificadoRecord
        //carrega na memoria a certificado de codigo $this->srh_certificado_id
        if (empty($this->certificado)) {
            $this->certificado = new CertificadoRecord($this->srh_certificado_id);
        }
        //retorna o objeto instanciado
        return $this->certificado->cargahoraria;
    }

    /*
     * metodo get_nome_municipio()
     * executado sempre que for acessada a propriedade nome_municipio
     */

    function get_nome_municipio() {
        //instancia CertificadoRecord
        //carrega na memoria a certificado de codigo $this->srh_certificado_id
        if (empty($this->certificado)) {
            $this->certificado = new CertificadoRecord($this->srh_certificado_id);
        }
        //retorna o objeto instanciado
        return $this->certificado->nome_municipio;
    }

}
