<?php

/*
 * classe CertificadoRecord
 * Active Record para tabela Certificado
 * Autor: Jackson Meires
 */

class CertificadoRecord extends TRecord {

    const TABLENAME = 'srh_certificado';
    const PRIMARYKEY = 'id';
    const IDPOLICY = 'serial'; // {max, serial}

    private $municipio;

    /*
     * metodo get_nome_municipio()
     * executado sempre que for acessada a propriedade nome_municipio
     */

    function get_nome_municipio() {
        //instancia servidorRecord
        //carrega na memoria a servidor de codigo $this->servidor_id
        if (empty($this->municipio)) {
            $this->municipio = new MunicipioRecord($this->municipio_id);
        }
        //retorna o objeto instanciado
        return $this->municipio->nome;
    }

    /**
     * Delete the object and its aggregates
     * @param $id object ID
     */
    public function delete($id = NULL) {

        // delete the related CustomerSkill objects
        $id = isset($id) ? $id : $this->id;
        
        TTransaction::open('pg_ceres');
        $criteria = new TCriteria;
        $criteria->add(new TFilter('srh_certificado_id', '=', $id));
        
        $repository = new TRepository('CertificadoServidorRecord');
        // delete Record related objects
        $repository->delete($criteria);

        // delete the object itself
        parent::delete($id);
    }

}
