<?php
/*
 * classe CapacitacaoRecord
 * Active Record para tabela Capacitacao
 */
class CapacitacaoRecord extends TRecord
{
	const TABLENAME = 'capacitacao';
    const PRIMARYKEY = 'id';
    const IDPOLICY = 'serial'; // {max, serial}
}
?>