<?php
use Adianti\Database\TRecord;

/*
 * classe vw_servidor_quantitativo_situacaoRecord
 * Active Record para view servidor_quantitativo_situacao
 */
class vw_servidor_quantitativo_situacaoRecord extends TRecord {

	const TABLENAME = 'vw_servidor_quantitativo_situacao';
    const PRIMARYKEY = 'id';
    const IDPOLICY = 'serial'; // {max, serial}

}
?>