<?php
/*
 * classe vw_servidor_tempo_servicoRecord
 * Active Record para a view vw_servidor_tempo_servico
 */
class Vw_servidor_tempo_servicoRecord extends TRecord
{
    const TABLENAME = 'Vw_servidor_tempo_servico';
	const PRIMARYKEY = 'id';
	const IDPOLICY = 'serial'; // {max, serial}
}
?>
