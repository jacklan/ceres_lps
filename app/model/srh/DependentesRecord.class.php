<?php

/*
 * classe Vw_servidores_por_cargosRecord
 * Active Record para a view vw_servidores_por_cargos
 */

class DependentesRecord extends TRecord {

    const TABLENAME = 'dependentes';
    const PRIMARYKEY = 'id';
    const IDPOLICY = 'max'; // {max, serial}

}

?>