<?php

/*
 * classe vw_setorgrupoRecord
 * Active Record para tabela Setor Grupo
 */

class vw_setorgrupoRecord extends TRecord {

	const TABLENAME  = 'vw_setorgrupo';
	const PRIMARYKEY = 'id';
	const IDPOLICY   = 'serial'; // {max, serial}

    private $servidor;

     public function get_nome_servidor() {
        if (empty($this->servidor)) {
            $this->servidor = new ServidorRecord($this->servidor_id);
        }
        return $this->servidor->nome;
    }

}

?>