<?php
/*
 * classe vw_servidorsetorRecord
 * Active Record para view vw_servidorsetor
 */
class vw_servidorsetorRecord extends TRecord
{
	
	const TABLENAME = 'vw_servidorsetor';
    const PRIMARYKEY= 'id';
    const IDPOLICY =  'max'; // {max, serial}
	
}
?>