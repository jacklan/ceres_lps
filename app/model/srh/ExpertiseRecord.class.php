<?php

/*
 * classe ExpertiseRecord
 * Active Record para tablea drpprodutor
 * Autor:Jackson Meires
 * Data:07/06/2016
 */

class ExpertiseRecord extends TRecord {

    const TABLENAME = 'expertise';
    const PRIMARYKEY = 'id';
    const IDPOLICY = 'serial'; // {max, serial}

}
