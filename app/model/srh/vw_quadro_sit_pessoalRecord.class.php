<?php

/*
 * classe vw_quadro_sit_pessoalRecord
 * Active Record para a view vw_quadro_sit_pessoal
 */

class vw_quadro_sit_pessoalRecord extends TRecord {
    
	const TABLENAME = 'vw_quadro_sit_pessoal';
	const PRIMARYKEY = 'id';
	const IDPOLICY = 'serial'; // {max, serial}
	
    /*
     * metodo get_soma_efetivos_bolsistas()
     * executado para somar efetivos e bolsistas
     */

    public function get_soma_efetivos_bolsistas() {
        return $this->efetivos + $this->bolsistas;
    }
        
    /*
     * metodo get_soma_total()
     * executado para somar todos os tipos de funcionarios
     */
    public function get_soma_total() {
        return $this->efetivos + $this->bolsistas + $this->outros + $this->adisposicao + $this->cedido + $this->ceonveniado;
    }
    
     //adicionar link servidores efetivos para emitir relatorio 
    function get_link_efetivos(){
    //    return '<a alt="Servidores Efetivos" title="Servidores Efetivos" target="_blank" href="index.php?class=RelatorioQuantitativoServidorSituacaoPDF&method=onReload&nome_regional='.$this->nome_regional.'&municipio='.$this->municipio.'&tipofuncionario=EFETIVO&situacao=EM ATIVIDADE">'.$this->efetivos.'</a>';
    return '<a alt="Servidores Efetivos" title="Servidores Efetivos" target="_blank"  href="index.php?class=RelatorioQuantitativoServidorSituacaoPDF&nome_regional='.$this->nome_regional.'&municipio='.$this->municipio.'&tipofuncionario=EFETIVO&situacao=EM ATIVIDADE">'.$this->efetivos.'</a>';
    
    }
    
    //adicionar link bolsistas para emitir relatorio
    function get_link_bolsistas(){
        return '<a alt="Bolsistas" title="Bolsistas" target="_blank"   href="index.php?class=RelatorioQuantitativoServidorSituacaoPDF&method=onReload&nome_regional='.$this->nome_regional.'&municipio='.$this->municipio.'&tipofuncionario=BOLSISTA&situacao=EM ATIVIDADE">'.$this->bolsistas.'</a>';
    }
    //adicionar link tipo outros para emitir relatorio
     function get_link_outros(){
        return '<a alt="Outros vinculos" title="Outros vinculos" target="_blank"  href="index.php?class=RelatorioQuantitativoServidorSituacaoPDF&method=onReload&nome_regional='.$this->nome_regional.'&municipio='.$this->municipio.'&tipofuncionario=OUTROS&situacao=EM ATIVIDADE">'.$this->outros.'</a>';
    }
    //adicionar link tipo adisposicao para emitir relatorio
     function get_link_adisposicao(){
        return '<a alt="Vinculo A disposição" title="Vinculo A disposição" target="_blank"   href="index.php?class=RelatorioQuantitativoServidorSituacaoPDF&method=onReload&nome_regional='.$this->nome_regional.'&municipio='.$this->municipio.'&tipofuncionario=EFETIVO&situacao=A DISPOSICAO">'.$this->adisposicao.'</a>';
    }
    
    //adicionar link tipo cedido para emitir relatorio
     function get_link_cedido(){
        return '<a alt="Vinculo Cedido" title="Vinculo Cedido" target="_blank" href="index.php?class=RelatorioQuantitativoServidorSituacaoPDF&method=onReload&nome_regional='.$this->nome_regional.'&municipio='.$this->municipio.'&tipofuncionario=EFETIVO&situacao=CEDIDO(A)">'.$this->cedido.'</a>';
    }
    
    //adicionar link tipo conveniado para emitir relatorio
     function get_link_conveniado(){
        return '<a alt="Vinculo Conveniado" title="Vinculo Conveniado" target="_blank"  href="index.php?class=RelatorioQuantitativoServidorSituacaoPDF&method=onReload&nome_regional='.$this->nome_regional.'&municipio='.$this->municipio.'&tipofuncionario=CONVENIADO&situacao=EM ATIVIDADE">'.$this->conveniado.'</a>';
    }
    
}

?>