<?php
/*
 * classe Vw_aceitaplano_listagemRecord
 * Active Record para a view Vw_aceitaplano_listagem
 */
class Vw_aceitaplano_listagemRecord extends TRecord
{
    const TABLENAME = 'Vw_aceitaplano_listagem';
	const PRIMARYKEY = 'id';
	const IDPOLICY = 'serial'; // {max, serial}
}
?>
