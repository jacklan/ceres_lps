<?php

/*
 * classe vw_servidor_com_formacao
 * Active Record para a view servidores_por_cargos
 */

class vw_servidor_com_formacaoRecord extends TRecord {

    const TABLENAME = 'vw_servidor_com_formacao';
    const PRIMARYKEY = 'id';
    const IDPOLICY = 'serial'; // {max, serial}

}