<?php
/*
 * classe vw_servidor_curriculo_validacaoRecord
 * Active Record para tabela vw_servidor_curriculo_validacao
 */

class vw_servidor_curriculo_validacaoRecord extends TRecord
{
	
	const TABLENAME = 'vw_servidor_curriculo_validacao';
    const PRIMARYKEY = 'id';
    const IDPOLICY = 'serial'; // {max, serial}
	
    //adicionar link qtd_formano grid 
    function get_link_formacao(){
        return '<a alt="Validar Formação" title="Validar Formação"  href="index.php?class=FormacaoServidorDetalhePerfilValidacao&method=onReload&fk='.$this->servidor_id.'">'.$this->qtd_formacao.'</a>';
    }
    
    //adicionar link qtd_formano grid 
    function get_link_experiencia() {
        return '<a alt="Validar Experiência" title="Validar Experiência" href="index.php?class=ExperienciaDetalhePerfilValidacao&method=onReload&fk=' . $this->servidor_id . '">' . $this->qtd_experiencia . '</a>';
    }
    //adicionar link qtd_capacitacao grid 
    function get_link_capacitacao() {
        return '<a alt="Validar Capacitação" title="Validar Capacitação" href="index.php?class=ServidorCapacitacaoDetalhePerfilValidacao&method=onReload&fk=' . $this->servidor_id . '">' . $this->qtd_capacitacao . '</a>';
    }
    //adicionar link qtd_capacitacao grid 
    function get_link_producao() {
        return '<a alt="Validar Produção" title="Validar Produção" href="index.php?class=ProducaoDetalhePerfilValidacao&method=onReload&fk=' . $this->servidor_id . '">' . $this->qtd_producao . '</a>';
    }

}
?>