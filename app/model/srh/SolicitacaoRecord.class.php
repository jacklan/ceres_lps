<?php

/*
 * classe SolicitacaoRecord
 * Active Record para tabela Solicitacao
 */

class SolicitacaoRecord extends TRecord {

    const TABLENAME = 'solicitacao';
    const PRIMARYKEY = 'id';
    const IDPOLICY = 'serial'; // {max, serial}

    private $tiposolicitacao;

    /*
     * metodo get_nome_tiposolicitacao()
     * executado sempre que for acessada a propriedade nome_tiposolicitacao
     */

    function get_nome_tiposolicitacao() {
        //instancia tiposolicitacaoRecord
        //carrega na memoria a empresa de codigo $this->empresa_id
        if (empty($this->tiposolicitacao)) {
            $this->tiposolicitacao = new tiposolicitacaoRecord($this->tiposolicitacao_id);
        }
        //retorna o objeto instanciado
        return $this->tiposolicitacao->nome;
    }

}

?>