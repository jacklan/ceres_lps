<?php

/*
 * classe vw_servidor_municipio
 * Active Record para view vw_servidor_municipio
 */

class vw_servidor_municipioRecord extends TRecord {

	const TABLENAME = 'vw_servidor_municipio';
	const PRIMARYKEY= 'id';
	const IDPOLICY =  'max'; // {max, serial}

}
?>