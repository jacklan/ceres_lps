<?php

/*
 * classe vw_servidor_formacao
 * Active Record para vw servidor_formacao
 * Autor:Jackson Meires
 * Data:015/11/2016
 */

class vw_servidor_formacaoRecord extends TRecord {

    const TABLENAME = 'vw_servidor_formacao';
    const PRIMARYKEY = 'id';
    const IDPOLICY = 'serial'; // {max, serial}

}