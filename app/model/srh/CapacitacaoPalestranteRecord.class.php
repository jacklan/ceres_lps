<?php
/*
 * classe CapacitacaoPalestranteRecord
 * Active Record para tabela CapacitacaoPalestrante
 */
class CapacitacaoPalestranteRecord extends TRecord
{

    const TABLENAME = 'capacitacaopalestrante';
    const PRIMARYKEY = 'id';
    const IDPOLICY = 'serial'; // {max, serial}

    private $capacitacao;
    private $palestrante;


    /*
     * metodo get_nome_capacitacao()
     * executado sempre que for acessada a propriedade nome_capacitacao
     */
    function get_nome_capacitacao() {
    //instancia capacitacaoRecord
    //carrega na memoria o capacitacao de codigo $this->capacitacao_id
        if (empty ($this->capacitacao)) {
            $this->capacitacao = new CapacitacaoRecord($this->capacitacao_id);
        }
        //retorna o objeto instanciado
        return $this->capacitacao->nome;
    }

    function get_nome_palestrante() {
    //instancia capacitacaoRecord
    //carrega na memoria o capacitacao de codigo $this->capacitacao_id
        if (empty ($this->palestrante)) {
            $this->palestrante = new palestranteRecord($this->palestrante_id);
        }
        //retorna o objeto instanciado
        return $this->palestrante->nome;
    }

}
?>

