<?php
/*
 * classe SetorGrupoRecord
 * Active Record para tabela Setor Grupo
 */
class SetorGrupoRecord extends TRecord
{
	
	const TABLENAME = 'setorgrupo';
    const PRIMARYKEY = 'id';
    const IDPOLICY = 'serial'; // {max, serial}
 
    private $servidor;
    private $setor;
       /*
     * metodo get_nome_metodo()
     * executado sempre que for acessada a propriedade nome_metodo
     */
    function get_nome_servidor()
    {
        //instancia ServidorRecord e
        //carrega na memoria a acao do codigo $this->servidor_id
        if (empty ($this->servidor)){
           $this->servidor = new ServidorRecord($this->servidor_id);
        }
        //retorna o objeto instanciado
        return $this->servidor->nome;
    }
    
    
     function get_nome_setor()
    {
        //instancia ServidorRecord e
        //carrega na memoria a acao do codigo $this->servidor_id
        if (empty ($this->setor)){
           $this->setor = new SetorRecord($this->setor_id);
        }
        //retorna o objeto instanciado
        return $this->setor->nome;
    }
    
    function get_matricula(){
        if(empty ($this->servidor)){
            $this->servidor = new ServidorRecord($this->servidor_id);
        }
        return $this->servidor->matricula;
    }
    
    
}
?>