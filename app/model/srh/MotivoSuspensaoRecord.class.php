<?php
/*
 * classe MotivosuspensaoRecord
 * Active Record para tabela Motivosuspensao
 */
class MotivosuspensaoRecord extends TRecord
{
	
	const TABLENAME = 'motivosuspensao';
    const PRIMARYKEY = 'id';
    const IDPOLICY = 'serial'; // {max, serial}
	
}
?>

