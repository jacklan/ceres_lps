<?php

/*
 * classe vw_regional_servidorRecord
 * Active Record para a view vw_regional_servidor
 */

class vw_regional_servidorRecord extends TRecord {

    const TABLENAME = 'vw_regional_servidor';
    const PRIMARYKEY = 'id';
    const IDPOLICY = 'serial'; // {max, serial}

}