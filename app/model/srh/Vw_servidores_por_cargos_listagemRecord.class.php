<?php
/*
 * classe Vw_servidores_por_cargos_listagemRecord
 * Active Record para a view vw_servidores_por_cargos_listagem
 */
class Vw_servidores_por_cargos_listagemRecord extends TRecord
{
    const TABLENAME = 'vw_servidores_por_cargos_listagem';
    const PRIMARYKEY = 'id';
    const IDPOLICY = 'serial'; // {max, serial}
}
?>
