<?php
/*
 * classe UnidadeServidorRecord
 * Active Record para tabela Unidadeservidor
 */
class UnidadeServidorRecord extends TRecord
{
       const TABLENAME = 'unidadeservidor';
       const PRIMARYKEY = 'id';
       const IDPOLICY = 'serial'; // {max, serial}

       private $setor;
       private $unidadeoperativa;
       private $servidor;

           /*
     * metodo get_nome_setor()
     * executado sempre que for acessada a propriedade nome_servidor
     */
    function get_nome_setor()
    {
        //instancia setorRecord
        //carrega na memoria o setor
        if (empty ($this->setor)){
           $this->setor = new setorRecord($this->setor_id);
        }
        //retorna o objeto instanciado
        return $this->setor->nome;
    }

        function get_nome_unidadeoperativa()
    {
        //instancia setorRecord
        //carrega na memoria o setor
        if (empty ($this->unidadeoperativa)){
           $this->unidadeoperativa = new unidadeoperativaRecord($this->unidadeoperativa_id);
        }
        //retorna o objeto instanciado
        return $this->unidadeoperativa->nome;
    }

     function get_nome_servidor()
    {
        //instancia servidorRecord
        //carrega na memoria o servidor
        if (empty ($this->servidor)){
           $this->servidor = new servidorRecord($this->servidor_id);
        }
        //retorna o objeto instanciado
        return $this->servidor->nome;
    }

}
?>