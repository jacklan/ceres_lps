<?php

/*
 * classe OrgaoRecord
 * Active Record para tabela Orgao
 */

class OrgaoRecord extends TRecord {

    const TABLENAME = 'orgao';
    const PRIMARYKEY = 'id';
    const IDPOLICY = 'serial'; // {max, serial}

}
?>