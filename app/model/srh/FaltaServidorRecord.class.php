<?php

/*
 * classe FaltaservidorRecord
 * Active Record para tabela Falta Servidor
 */

class FaltaservidorRecord extends TRecord {

    const TABLENAME = 'faltaservidor';
    const PRIMARYKEY = 'id';
    const IDPOLICY = 'serial'; // {max, serial}

}

?>