<?php

/*
 * classe LicencaParticularRecord
 * Active Record para tabela Licenca Particular
 */

class LicencaParticularRecord extends TRecord {

    const TABLENAME = 'licencaparticular';
    const PRIMARYKEY = 'id';
    const IDPOLICY = 'serial'; // {max, serial}

    private $tipolicenca;

    /*
     * metodo get_nome_tipolicenca()
     * executado sempre que for acessada a propriedade nome_tipolicenca
     */

    function get_nome_tipolicenca() {
        //instancia tipolicencasuspensaoRecord
        //carrega na memoria o tipolicencasuspensao de codigo $this->tipolicencasuspensao_id
        if (empty($this->tipolicenca)) {
            $this->tipolicenca = new TipoLicencaRecord($this->tipolicenca_id);
        }
        //retorna o objeto instanciado
        return $this->tipolicenca->nome;
    }

}

?>