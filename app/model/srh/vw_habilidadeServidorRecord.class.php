<?php

/*
 * classe vw_habilidadeServidorRecord
 * Active Record para a view vw_habilidadeServidorRecord
 */

class vw_habilidadeServidorRecord extends TRecord {

    const TABLENAME = 'vw_habilidadeservidor';
    const PRIMARYKEY = 'id';
    const IDPOLICY = 'serial'; // {max, serial}

}

?>