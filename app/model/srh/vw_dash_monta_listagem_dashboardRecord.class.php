<?php

/*
 * classe vw_dash_monta_listagem_dashboard
 * Active Record para tabela vw_dash_monta_listagem_dashboardRecord
 */

class vw_dash_monta_listagem_dashboardRecord extends TRecord {

    const TABLENAME = 'vw_dash_monta_listagem_dashboard';
    const PRIMARYKEY = 'id';
    const IDPOLICY = 'serial'; // {max, serial}

}

?>