<?php
/*
 * classe ServidorProventoRecord
 * Active Record para tabela ServidorProvento
 */
class ServidorProventoRecord extends TRecord
{
    const TABLENAME = 'servidorprovento';
    const PRIMARYKEY = 'id';
    const IDPOLICY = 'serial'; // {max, serial}
}
?>