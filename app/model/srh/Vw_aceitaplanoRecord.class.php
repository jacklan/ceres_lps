<?php
/*
 * classe Vw_aceitaplanoRecord
 * Active Record para a view vw_servidores_por_cargos
 */
class Vw_aceitaplanoRecord extends TRecord
{
   	const TABLENAME = 'Vw_aceitaplano';
	const PRIMARYKEY = 'id';
	const IDPOLICY = 'serial'; // {max, serial}

}
?>
