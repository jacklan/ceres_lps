<?php
/*
 * classe vw_aniversariantesRecord
 * Active Record para view vw_aniversariantes
 */
class vw_aniversariantesRecord extends TRecord
{
	
	const TABLENAME = 'vw_aniversariantes';
    const PRIMARYKEY = 'id';
    const IDPOLICY = 'serial'; // {max, serial}
}
?>