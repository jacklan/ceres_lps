<?php
/*
 * classe TipoMaterialRecord
 * Active Record para tabela Tipomaterial
 */
class TipoMaterialRecord extends TRecord
{
    const TABLENAME = 'tipomaterial';
    const PRIMARYKEY = 'id';
    const IDPOLICY = 'serial'; // {max, serial}

}
?>

