<?php

/*
 * classe DisposicaoRecord
 * Active Record para tabela Disposicao
 */

class DisposicaoRecord extends TRecord {

    const TABLENAME = 'disposicao';
    const PRIMARYKEY = 'id';
    const IDPOLICY = 'max'; // {max, serial}

    private $orgao;
    private $motivoafastamento;

    /*
     * metodo get_nome_orgao()
     * executado sempre que for acessada a propriedade nome_orgao
     */

    function get_nome_orgao() {
        //instancia orgaoRecord
        //carrega na memoria a empresa de codigo $this->empresa_id
        if (empty($this->orgao)) {
            $this->orgao = new orgaoRecord($this->orgao_id);
        }
        //retorna o objeto instanciado
        return $this->orgao->nome;
    }

    /*
     * metodo get_nome_motivoafastamento()
     * executado sempre que for acessada a propriedade nome_motivoafastamento
     */

    function get_nome_motivoafastamento() {
        //instancia orgaoRecord
        //carrega na memoria a empresa de codigo $this->motivoafastamento_id
        if (empty($this->motivoafastamento)) {
            $this->motivoafastamento = new MotivoAfastamentoRecord($this->motivoafastamento_id);
        }
        //retorna o objeto instanciado
        return $this->motivoafastamento->nome;
    }

}

?>