<?php
/*
 * classe vw_servidor_expertiseRecord
 * Active Record para vw servidor_expertise
 */
class vw_servidor_expertiseRecord extends TRecord
{
    const TABLENAME = 'vw_servidor_expertise';
    const PRIMARYKEY = 'expertise_id';
    const IDPOLICY = 'serial'; // {max, serial}
}