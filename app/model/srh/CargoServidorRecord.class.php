<?php

/*
 * classe CargoServidorRecord
 * Active Record para tabela Cargoservidor
 */

class CargoServidorRecord extends TRecord {

    const TABLENAME = 'formacao';
    const PRIMARYKEY = 'id';
    const IDPOLICY = 'serial'; // {max, serial}

    private $cargo;
    private $nivelsalarial;

    /*
     * metodo get_nome_cargo()
     * executado sempre que for acessada a propriedade nome_cargo
     */

    function get_nome_cargo() {
        //instancia cargoRecord
        //carrega na memoria a empresa de codigo $this->empresa_id
        if (empty($this->cargo)) {
            $this->cargo = new CargoRecord($this->cargo_id);
        }
        //retorna o objeto instanciado
        return $this->cargo->nome;
    }

    function get_nome_nivelsalarial() {
        //instancia nivelsalarialRecord
        //carrega na memoria a empresa de codigo $this->empresa_id
        if (empty($this->nivelsalarial)) {
            $this->nivelsalarial = new nivelsalarialRecord($this->nivelsalarial_id);
        }
        //retorna o objeto instanciado
        return $this->nivelsalarial->nome;
    }

}

?>