<?php
/*
 * classe vw_servidores_licenca
 * Active Record para tabela vw_servidores_licenca
 */
class vw_servidores_licencaRecord extends TRecord
{
	
	const TABLENAME = 'vw_servidores_licenca';
	const PRIMARYKEY = 'id';
	const IDPOLICY = 'serial'; // {max, serial}

}
?>