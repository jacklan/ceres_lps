<?php
/*
 * classe vw_servidores_em_ferias
 * Active Record para tabela vw_servidores_em_ferias
 */
class vw_servidores_em_feriasRecord extends TRecord
{
		
	const TABLENAME = 'vw_servidores_em_ferias';
	const PRIMARYKEY = 'id';
	const IDPOLICY = 'serial'; // {max, serial}

}
?>