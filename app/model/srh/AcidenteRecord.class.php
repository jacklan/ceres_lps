<?php

/*
 * classe AcidenteRecord
 * Active Record para a table Acidente
 */

class AcidenteRecord extends TRecord {

    const TABLENAME = 'acidente';
    const PRIMARYKEY = 'id';
    const IDPOLICY = 'max'; // {max, serial}

}

?>