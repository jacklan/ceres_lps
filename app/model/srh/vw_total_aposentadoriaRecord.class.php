<?php
/*
 * classe vw_total_aposentadoriaRecord
 * Active Record para a view vw_total_aposentadoria
 */
class vw_total_aposentadoriaRecord extends TRecord
{
    const TABLENAME = 'vw_total_aposentadoria';
    const PRIMARYKEY= 'id';
    const IDPOLICY =  'serial'; // {max, serial}
}