<?php
/*
 * classe ChaRecord
 * Active Record para tabela Cha
 */
class ChaRecord extends TRecord
{
    private $cargo;
    private $tipohabilidade;
	
	const TABLENAME = 'cha';
    const PRIMARYKEY= 'id';
    const IDPOLICY =  'max'; // {max, serial}
    
        function get_nome_cargo() {
    //instancia habilidadeRecord
    //carrega na memoria o habilidade de codigo $this->habilidade_id
        if (empty ($this->cargo)) {
            $this->cargo = new CargoRecord($this->cargo_id);
        }
        //retorna o objeto instanciado
        return $this->cargo->nome;
    }

        function get_nome_tipohabilidade() {
    //instancia habilidadeRecord
    //carrega na memoria o habilidade de codigo $this->habilidade_id
        if (empty ($this->tipohabilidade)) {
            $this->tipohabilidade = new TipoHabilidadeRecord($this->tipohabilidade_id);
        }
        //retorna o objeto instanciado
        return $this->tipohabilidade->nome;
    }
}
?>

