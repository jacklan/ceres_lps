<?php
/*
 * classe UsuarioRecord
 * Active Record para tabela Usuario
 */
class UsuarioRecord extends TRecord
{
    
    const TABLENAME = 'usuario';
    const PRIMARYKEY = 'id';
    const IDPOLICY = 'serial'; // {max, serial}
    
    private $servidor;
    private $colaboradorleite;
    private $agentebancario;
    private $regional;
    private $banco;

    /*
     * metodo get_nome_servidor()
     * executado sempre que for acessada a propriedade nome_servidor
     */
    function get_nome_servidor()
    {
        //instancia ServidorRecord
        //carrega na memoria o servidor
        if (empty ($this->servidor)){
           $this->servidor = new ServidorRecord($this->servidor_id);
        }
        //retorna o objeto instanciado
        return $this->servidor->nome;
    }

    /*
     * metodo get_nome_agentebancario()
     * executado sempre que for acessada a propriedade nome_agentebancario
     */
    function get_nome_agentebancario()
    {
        //instancia AgenteBancarioRecord
        //carrega na memoria o agentebancario
        if (empty ($this->agentebancario)){
           $this->agentebancario = new AgenteBancarioRecord($this->agentebancario_id);
        }
        //retorna o objeto instanciado
        return $this->agentebancario->nome;
    }
    /*
     * metodo get_situacao_agentebancario()
     * executado sempre que for acessada a propriedade situacao_agentebancario
     */
    function get_situacao_agentebancario()
    {
        //instancia AgenteBancarioRecord
        //carrega na memoria o agentebancario
        if (empty ($this->agentebancario)){
           $this->agentebancario = new AgenteBancarioRecord($this->agentebancario_id);
        }
        //retorna o objeto instanciado
        return $this->agentebancario->situacao;
    }
    /*
     * metodo get_banco_agentebancario()
     * executado sempre que for acessada a propriedade bancoagentebancario
     */
    function get_banco_agentebancario()
    {
        //carrega na memoria o agentebancario
        if (!empty ($this->agentebancario)){
           $this->agentebancario = new AgenteBancarioRecord($this->agentebancario_id);
           return $this->agentebancario->nome_banco; //retorna o objeto instanciado
        }
    }
    /*
     * metodo get_cpf_agentebancario()
     * executado sempre que for acessada a propriedade cpf_agentebancario
     */
    function get_cpf_agentebancario()
    {
        //instancia AgenteBancarioRecord
        //carrega na memoria o agentebancario
        if (empty ($this->agentebancario)){
           $this->agentebancario = new AgenteBancarioRecord($this->agentebancario_id);
        }
        //retorna o objeto instanciado
        return $this->agentebancario->cpf;
    }
    
    function get_nome_colaboradorleite()
    {
        //instancia AgenteBancarioRecord
        //carrega na memoria o agentebancario
        if (empty ($this->colaboradorleite)){
           $this->colaboradorleite = new ColaboradorLeiteRecord($this->colaboradorleite_id);
        }
        //retorna o objeto instanciado
        return $this->colaboradorleite->nome;
    }
    
    
    /*
     * metodo get_nome_regional()
     * executado sempre que for acessada a propriedade nome_regional
     */
    function get_nome_regional()
    {
        //instancia RegionalRecord
        //carrega na memoria o regional
        if (empty ($this->regional)){
           $this->regional = new RegionalRecord($this->regional_id);
        }
        //retorna o objeto instanciado
        return $this->regional->nome;
    }
    
     /**
     * Delete the object and its aggregates
     * @param $id object ID
     */
    public function delete($id = NULL) {

        // delete the related CustomerSkill objects
        $id = isset($id) ? $id : $this->id;
        
        TTransaction::open('pg_ceres');
        $criteria = new TCriteria;
        $criteria->add(new TFilter('usuario_id', '=', $id));
        
        $repository = new TRepository('UsuarioPerfilRecord');
        // delete Record related objects
        $repository->delete($criteria);

        // delete the object itself
        parent::delete($id);
    }

}