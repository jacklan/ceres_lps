<?php
/*
 * classe AcaoestruturanteRecord
 * Active Record para tabela Acaoestruturante
 */
class vw_avaliacao_servidoresRecord extends TRecord
{
    const TABLENAME = 'vw_avaliacao_servidores';
    const PRIMARYKEY = 'id';
    const IDPOLICY = 'serial'; // {max, serial}

}
?>