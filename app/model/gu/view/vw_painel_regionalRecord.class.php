<?php
/*
 * classe vw_painel_regionalRecord
 * Active Record para view vw_painel_regional
 */
class vw_painel_regionalRecord extends TRecord
{
    //put your code here
    const TABLENAME = 'vw_painel_regional';
    const PRIMARYKEY = 'id';
    const IDPOLICY = 'serial'; // {max, serial}
}
?>