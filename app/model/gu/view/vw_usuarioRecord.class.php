<?php
/*
 * classe Vw_usuarioRecord
 * Active Record para a view vw_usuario
 */

class vw_usuarioRecord extends TRecord
{   
    
    const TABLENAME = 'vw_usuario';
    const PRIMARYKEY = 'usuario_id';
    const IDPOLICY = 'serial'; // {max, serial}
    //
    //put your code here
    private $municipio;
    
    public function get_nome_municipio() {
        if(empty($this->municipio)){
            $this->municipio = new MunicipioRecord($this->municipio_id);
        }
        return $this->municipio->nome;
        
    }
    public function getPaginas()
    {
        try {
            TTransaction::open('pg_ceres');

            $repositoryCP = new TRepository('FeaturePage');

            $criteriaCP = new TCriteria();
            $criteriaCP->setProperty('order', 'name');

            $objects = $repositoryCP->load($criteriaCP);
            TTransaction::close();
            if ($objects) {

                $arrayPage = [];
                TTransaction::open('pg_ceres');
                $conn = TTransaction::get();
                foreach ($objects as $object) {

                    $sth = $conn->prepare("SELECT p.nome, p.arquivo FROM pagina p WHERE p.arquivo = ?");

                    $sth->execute([$object->controller]);

                    while ($row = $sth->fetchObject()) {
                        $arrayPage[$row->arquivo] = $row->nome;
                    }
                }

                $sthGU = $conn->prepare("SELECT p.arquivo, p.nome FROM pagina p where p.modulo_id IN (select m.id from modulo m where m.nome = 'GU')");

                $sthGU->execute();

                while ($rowGU = $sthGU->fetchObject()) {
                    $arrayPage[$rowGU->arquivo] = $rowGU->nome;
                }

                TTransaction::close();

                return $arrayPage;
            }
        } catch (Exception $e) {
            new TMessage('error', $e->getMessage());
        }

    }
}