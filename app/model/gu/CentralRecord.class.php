<?php

/*
 * classe CentralRecord
 * Active Record para tabela central
 */

class CentralRecord extends TRecord {

    const TABLENAME = 'central';
    const PRIMARYKEY = 'id';
    const IDPOLICY = 'serial'; // {max, serial}
}
?>