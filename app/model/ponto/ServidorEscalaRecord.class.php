<?php

class ServidorEscalaRecord extends TRecord
{
    const TABLENAME = 'servidorescala';
    const PRIMARYKEY = 'id';
    const IDPOLICY =  'serial';

    private $escala;

    function get_nome_escala() {

        if (empty($this->escala)) {
            $this->escala = new EscalaRecord($this->escala_id);
        }

        return $this->escala->nome;
    }
}