<?php
/*
 * classe AlunoRecord
 * Active Record para tabela Aluno
 */
class FeriadosRecord extends TRecord{


    const TABLENAME = 'feriados';
    const PRIMARYKEY= 'id';
    const IDPOLICY =  'serial'; // {max, serial}

    private $municipio;

      function get_nome_municipio()
    {
        //instancia tipofaseRecord
        //carrega na memoria a empresa de codigo $this->empresa_id
        if (empty ($this->municipio)){
           $this->municipio = new MunicipioRecord($this->municipio_id);
        }
        //retorna o objeto instanciado
        return $this->municipio->nome;
    }
}
?>

