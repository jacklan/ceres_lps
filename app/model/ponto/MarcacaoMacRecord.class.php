<?php
/*
 * classe feriadosRecord
 * Active Record para tabela feriados
 */
class MarcacaoMacRecord extends TRecord
{
    const TABLENAME = 'marcacaomac';
    const PRIMARYKEY = 'id';
    const IDPOLICY = 'serial'; // {max, serial}
    
    function get_unidadeoperativa_nome()
    {
        //instancia regionalRecord
        //carrega na memoria o regional
        if (empty ($this->unidade)){
           $this->unidade = new UnidadeOperativaRecord($this->unidadeoperativa_id);
        }
        //retorna o objeto instanciado
        return $this->unidade->nome;
    }

}
?>

