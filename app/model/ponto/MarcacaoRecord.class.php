<?php
/*
 * classe marcacaoRecord
 * Active Record para tabela marcacao
 */
class MarcacaoRecord extends TRecord
{
	const TABLENAME = 'marcacao';
    const PRIMARYKEY = 'id';
    const IDPOLICY = 'serial'; // {max, serial}
    
     private $servidor;

     function get_nome_servidor(){
     if(empty ($this->servidor)){
         $this->servidor = new ServidorRecord($this->servidor_id);
         }
         return $this->servidor->nome;
        
     }
}
?>

