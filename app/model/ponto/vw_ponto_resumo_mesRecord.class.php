<?php

/*
 * classe vw_ponto_resumo_mesRecord
 * Active Record para view vw_ponto_resumo_mes
 */

class vw_ponto_resumo_mesRecord extends TRecord {

    const TABLENAME = 'vw_ponto_resumo_mes';
    const PRIMARYKEY = 'id';
    const IDPOLICY = 'serial'; // {max, serial}
    
}