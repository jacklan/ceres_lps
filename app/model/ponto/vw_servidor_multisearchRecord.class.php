<?php

/*
 * classe vw_servidor_multisearchRecord
 * Active Record para view deservidor_multisearch
 * Autor:Jackson Meires
 * Data:17/03/2016
 */

class vw_servidor_multisearchRecord extends TRecord {

    const TABLENAME = 'vw_servidor_multisearch';
    const PRIMARYKEY = 'servidor_id';
    const IDPOLICY = 'serial'; // {max, serial}

}
