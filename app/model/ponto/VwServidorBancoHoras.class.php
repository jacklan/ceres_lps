<?php

class VwServidorBancoHoras extends TRecord
{
    const TABLENAME = 'vw_servidor_banco_horas';
    const PRIMARYKEY = 'servidor_id';
    const IDPOLICY = 'serial';

    private $servidor_matricula;

    function get_servidor_matricula() {
        if (empty($this->servidor_matricula)) {
            $this->servidor_matricula = new ServidorRecord($this->servidor_id);
        }
        return $this->servidor_matricula->matricula;
    }
}