<?php
/*
 * classe vw_marcacao_justificativa
 * Active Record para tabela vw_marcacao_justificativa
 */
class vw_marcacao_justificativaRecord extends TRecord
{

    const TABLENAME = 'vw_marcacao_justificativa';
    const PRIMARYKEY = 'id';
    const IDPOLICY = 'serial'; // {max, serial}
    
    function get_justificativapdf() {
        if(!empty($this->justificativapdf)){
            return '<a target="_blank" data-toggle="modal" href="app/images/justificativaponto/justificativapdf_' . $this->id .'.pdf" > Clique para visualizar PDF </a>';
        }
    }
}