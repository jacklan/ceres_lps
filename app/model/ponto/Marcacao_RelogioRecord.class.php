<?php
/*
 * classe Marcacao_RelogioRecord2
 * Active Record para tabela Marcacao_Relogio
 */
class Marcacao_RelogioRecord extends TRecord
{

    const TABLENAME = 'marcacao_relogio';
    const PRIMARYKEY = 'id';
    const IDPOLICY = 'serial'; // {max, serial}

    private $nome_tipojustificativa;
    private $nome_servidor;
    private $matricula_servidor;
    private $nome_chefe;

    function get_nome_tipojustificativa()
    {
        if (empty ($this->nome_tipojustificativa)){
            $this->nome_tipojustificativa = new TipoJustificativaPontoRecord($this->tipojustificativaponto_id);
        }
        return $this->nome_tipojustificativa->nome;
    }

    function get_nome_servidor()
    {
        if (empty ($this->nome_servidor)){
            $this->nome_servidor = new ServidorRecord($this->servidor_id);
        }
        return $this->nome_servidor->nome;
    }

    function get_matricula_servidor()
    {
        if (empty ($this->matricula_servidor)){
            $this->matricula_servidor = new ServidorRecord($this->servidor_id);
        }
        return $this->matricula_servidor->matricula;
    }

    function get_nome_chefe()
    {
        if (empty ($this->nome_chefe)){
            $this->nome_chefe = new ServidorRecord($this->chefe_id);
        }   
        return $this->nome_chefe->nome;
    }

    function get_justificativapdf_url() {

        if (file_exists('app/output/justificativaponto/justificativapdf_' . $this->id . '.pdf')) {
            return '<a target="_blank" style="text-decoration:none;" data-toggle="modal" href="app/output/justificativaponto/justificativapdf_' . $this->id . '.pdf" ><font color="blue"><b> Clique para visualizar</b></font></a>';
        } else {
            return '<b>Sem Anexo</b>';
        }
    }

}