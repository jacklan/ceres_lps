<?php

/*
 * classe Marcacao_RelogioRecord
 * Active Record para tabela Marcacao_Relogio
 */

class vw_marcacao_relogioRecord extends TRecord {

    const TABLENAME = 'vw_marcacao_relogio';
    const PRIMARYKEY = 'id';
    const IDPOLICY = 'serial'; // {max, serial}
    
    private $servidor;

    function get_nome_servidor() {
        if (empty($this->servidor)) {
            $this->servidor = new ServidorRecord($this->servidor_id);
        }
        return $this->servidor->nome;
    }
    
    function get_link() {        
        return '<a target="_blank" href="?class=RelatorioBatidasServidorPDF&matricula='.$this->matricula.'&databatida='.$this->databatida.'" >(ver)</a>';
    }

}