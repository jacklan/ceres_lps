<?php

//ini_set('display_errors', 1);
//ini_set('display_startup_erros', 1);
//error_reporting(E_ALL);

chdir('../../');

require_once 'init.php';

use Adianti\Database\TTransaction;
use Adianti\Database\TRepository;
use Adianti\Database\TCriteria;
use Adianti\Database\TFilter;

/*
 * Classe ServicoWebservice
 * Autor: Jonas Jord�o
 * Data: 27/10/2015
 */

class ServicoWebservice 
{

    static public function getServico()
    {

        $successTag = "success";
        $errorTag = "error";
        $dadosTag = "dados";
        
        $servicoIdTag = "servico_id";
        $gruposervicoIdTag = "gruposervico_id";
        $empresaIdTag = "empresa_id";

        if( filter_input( INPUT_POST, $servicoIdTag ) ) 
        {

            $servico_id = filter_input( INPUT_POST, $servicoIdTag );

            try 
            {

                $response = array();

                TTransaction::open('database_connection');
                
                $servico = new ServicoRecord( $servico_id );

                if( $servico ) 
                {
                	
                	$tempServicoArray['id'] = $servico->id;
                    $tempServicoArray['nome'] = $servico->nome;
                    $tempServicoArray['instrucoes'] = $servico->instrucoes;
                    $tempServicoArray['nomeimagem'] = "http://" . $_SERVER["SERVER_NAME"] . "/centralcidadao/app/images/servicos/" . $servico->nomeimagem;
                    $tempServicoArray['empresa_nome'] = $servico->nome_empresa;
                    $tempServicoArray['linkonline'] = $servico->linkonline;
                    $tempServicoArray['valorservico'] = $servico->valorservico == null ? 'Gratuito' : 'R$ ' .  $servico->valorservico ;
                    
                    
                    $parsed = date_parse( $servico->tempopadrao );
                    $tempServicoArray['tempopadrao'] = ( $parsed['minute'] == 0 ? $parsed['minute'] . '0' : $parsed['minute']  ) . 'min';
                    
                    
                    $tempServicoArray['abreviacao'] = $servico->abreviacao;
                    $tempServicoArray['situacao'] = $servico->situacao;
                    $tempServicoArray['gruposervico_id'] = $servico->gruposervico_id;

                    $response[$successTag] = 1;
                    $response[$dadosTag] = $tempServicoArray;
                    
                }else 
                {

                    $response[$successTag] = 2;
                    $response[$errorTag] = "Nenhum Servico encontrado com este ID = " + $servico_id + ".";
                
                }

                TTransaction::close();
                
            }catch( Exception $e ) 
            {

                $response[$successTag] = 0;
                $response[$errorTag] = $e->getMessage();

                TTransaction::rollback();
                
            }
            
        }else if ( filter_input( INPUT_POST, $gruposervicoIdTag ) ) 
        {
				$gruposervico_id = filter_input( INPUT_POST, $gruposervicoIdTag );
        	
	        try 
	        {

                $response = array();

                TTransaction::open('database_connection');

                $repository = new TRepository('ServicoRecord');

                $criteria = new TCriteria;

                $criteria->setProperty('order', 'nome');
                $criteria->add( new TFilter( 'gruposervico_id', '=', $gruposervico_id ) );
                $criteria->add( new TFilter( 'situacao', '=', 'ATIVO' ) );

                $collection = $repository->load($criteria);

                if( $collection ) 
                {
                	
                    $response[$successTag] = 1;

                    $i = 0;

                    foreach( $collection as $object ) 
                    {
                    	
                        $temp = array();
                        $temp["id"] = $object->id;
                        $temp["nome"] = $object->nome;
                        $temp['nomeimagem'] = "http://" . $_SERVER["SERVER_NAME"] . "/centralcidadao/app/images/servicos/" . $object->nomeimagem;
                        
                        $response[$dadosTag][$i++] = $temp;
                        
                    }
                    
                }else 
                {

                    $response[$successTag] = 2;
                    $response[$errorTag] = "Nenhum Servico encontrado.";
                
                }

                TTransaction::close();
                
            }catch( Exception $e ) 
            {

                $response[$successTag] = 0;
                $response[$errorTag] = $e->getMessage();

                TTransaction::rollback();
                
            }
            
        }else if ( filter_input( INPUT_POST, $empresaIdTag ) ) 
        {
				$empresa_id = filter_input( INPUT_POST, $empresaIdTag );
				
        	
	        try 
	        {

                $response = array();

                TTransaction::open('database_connection');

   				$empresa = new EmpresaRecord( $empresa_id );

                if( $empresa ) 
                {
                                        
                    $response[$successTag] = 1;
					$tempServicoArray['id'] = $empresa->nome;
                    $tempServicoArray['nome'] = $empresa->nome;

                    $response[$dadosTag][0] = $tempServicoArray;
                    
                }else 
                {

                    $response[$successTag] = 2;
                    $response[$errorTag] = "Nenhum Servico encontrado.";
                
                }

                TTransaction::close();
                
            }catch( Exception $e ) 
            {

                $response[$successTag] = 0;
                $response[$errorTag] = $e->getMessage();

                TTransaction::rollback();
                
            }
            
        } else {
        	$response[$successTag] = 0;
        	$response[$errorTag] = "Parametro incorreto.";
        }

        echo json_encode($response);
        
    }

}

ServicoWebservice::getServico();