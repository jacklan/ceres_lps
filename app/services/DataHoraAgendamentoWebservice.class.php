<?php

ini_set('display_errors', 1);
ini_set('display_startup_erros', 1);
error_reporting(E_ALL);

chdir('../../');

require_once 'init.php';

use Adianti\Database\TTransaction;
use Adianti\Database\TRepository;
use Adianti\Database\TCriteria;
use Adianti\Database\TFilter;

/*
 * Classe ServicoWebservice
 * Autor: Jonas Jord�o
 * Data: 27/10/2015
 */
class DataHoraAgendamentoWebservice 
{

	private $actionTag = "action";
	
	private $successTag = "success";
	private $errorTag = "error";
	private $dadosTag = "dados";
	
	private $servicoIdTag = "servico_id";
	private $empresaIdTag = "central_id";
	private $dataTag = "data";
	
	private $response;
	
    function __construct()
    {
    	
    	switch( filter_input( INPUT_POST, $this->actionTag ) )
    	{
    		case 1:
    			$this->getData();
    			break;
    		case 2:
    			$this->getHora();
    			break;
    	}
    	
        if( $this->response == null )
            $this->response[$this->successTag] = 0;

        echo json_encode( $this->response );

    }
    
    private function getData()
    {
    	
    	if( filter_input( INPUT_POST, $this->servicoIdTag ) && filter_input( INPUT_POST, $this->empresaIdTag ) )
    	{
    	
    		$servicoId = filter_input( INPUT_POST, $this->servicoIdTag );
    		$empresaId = filter_input( INPUT_POST, $this->empresaIdTag );
    	
    		try
    		{
    	
    			$this->response = array();
    	
    			TTransaction::open('database_connection');
    	
    			$repository = new TRepository('vw_data_agendamentoRecord');
    	
    			$criteria = new TCriteria;
    	
    			$criteria->add( new TFilter( 'servico_id', '=', $servicoId ) );
    			$criteria->add( new TFilter( 'central_id', '=', $empresaId ) );
    	
    			$collection = $repository->load( $criteria );
    	
    			if( $collection )
    			{
    	
    				$this->response[$this->successTag] = 1;
    	
    				$i = 0;
    	
    				foreach( $collection as $object )
    				{
    					 
    					$temp = array();
    					$temp["data"] = TDate::date2br( $object->dataagendamento );
    					$temp["dia"] = $object->diaextenso;
    	
    					$this->response[$this->dadosTag][$i++] = $temp;
    	
    				}
    	
    			}else
    			{
    	
    				$this->response[$this->successTag] = 2;
    				$this->response[$this->errorTag] = "Nenhuma Data encontrada.";
    	
    			}
    	
    			TTransaction::close();
    	
    		}catch( Exception $e )
    		{
    	
    			$this->response[$this->successTag] = 0;
    			$this->response[$this->errorTag] = $e->getMessage();
    	
    			TTransaction::rollback();
    	
    		}
    	
    	}
    	
    }
    
    private function getHora()
    {
    	
    	if ( filter_input( INPUT_POST, $this->dataTag ) )
    	{
    		 
    		$data = filter_input( INPUT_POST, $this->dataTag );
    		 
    		try
    		{
    	
    			$this->response = array();
    	
    			TTransaction::open('database_connection');
    	
    			$repository = new TRepository('vw_hora_agendamentoRecord');
    	
    			$criteria = new TCriteria;
    	
    			$criteria->add( new TFilter( 'dataagendamento', '=', $data ) );
    	
    			$collection = $repository->load($criteria);
    	
    			if( $collection )
    			{
    				 
    				$this->response[$this->successTag] = 1;
    	
    				$i = 0;
    	
    				foreach( $collection as $object )
    				{
    					 
    					$temp = array();
    	
    					$temp["hora"] = $object->horaagendamento;
    	
    					$this->response[$this->dadosTag][$i++] = $temp;
    	
    				}
    	
    			}else
    			{
    	
    				$this->response[$this->successTag] = 2;
    				$this->response[$this->errorTag] = "Nenhum Servico encontrado.";
    	
    			}
    	
    			TTransaction::close();
    	
    		}catch( Exception $e )
    		{
    	
    			$this->response[$this->successTag] = 0;
    			$this->response[$this->errorTag] = $e->getMessage();
    	
    			TTransaction::rollback();
    	
    		}
    	
    	}
    	
    }

}

new DataHoraAgendamentoWebservice();

?>