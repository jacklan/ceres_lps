<?php

//ini_set('display_errors', 1);
//ini_set('display_startup_erros', 1);
//error_reporting(E_ALL);

chdir('../../');

require_once 'init.php';

use Adianti\Database\TTransaction;
use Adianti\Database\TRepository;
use Adianti\Database\TCriteria;
use Adianti\Database\TFilter;

class CentralWebservice
{

	private $response;
	
	private $actionTag = "action";
	
	private $successTag         = "success";
	private $errorTag           = "error";
	private $dadosTag           = "dados";
	
	private $municipioIdTag     = "municipio_id";
	private $servicoIdTag 		= "servico_id";
	private $municipio_nomeTag  = "municipio_nome";
	private $centralIdTag       = "central_id";
	private $central_nomeTag    = "nome_central";
	
	function __construct()
	{
		
		switch( filter_input( INPUT_POST, $this->actionTag ) )
		{
			
			case 1:
				$this->getAllCentralFilter();
				break;
			case 2:
				$this->getAllCentralMunicipio();
				break;
			case 3:
				$this->getCentral();
				break;
				
		}
		
		if( $this->response == null )
			$this->response[$this->successTag] = 0;
		
			echo json_encode( $this->response );
		
	}
	
	private function getAllCentralFilter()
	{
		
		if( filter_input(INPUT_POST, $this->municipioIdTag ) && filter_input(INPUT_POST, $this->servicoIdTag ) )
		{
		
			$municipio_id = filter_input(INPUT_POST, $this->municipioIdTag );
			$servico_id = filter_input(INPUT_POST, $this->servicoIdTag );
			
			try 
			{

                $this->response = array();

                TTransaction::open('pg_ceres');

                $repository = new TRepository('vw_central_cidadaoRecord');

                $criteria = new TCriteria;

                $criteria->setProperty('order', 'nome_central');

                $criteria->add( new TFilter( 'municipio_id', '=', $municipio_id ) );
                

              	$criteria->add( new TFilter( 'servico_id', '=', $servico_id ) );

                $collection = $repository->load( $criteria );

                if( $collection ) 
                {
                	
                    $this->response[$this->successTag] = 1;

                    $i = 0;

                    foreach ($collection as $object) {
                        
                        $temp = array();
                        $temp["id"] = $object->central_id;
                        $temp["nome"] = $object->nome_central;
                        $temp["nomearquivo"] = "http://" . $_SERVER["SERVER_NAME"] . "/centralcidadao/app/images/central/" . $object->nomearquivo;
                        
                        $this->response[$this->dadosTag][$i++] = $temp;
                    }
                    
                }else 
                {

                    $this->response[$this->successTag] = 2;
                    $this->response[$this->errorTag] = "Nenhuma Central do Cidadão foi encontrada.";
                    
                }

                TTransaction::close();
                
            }catch (Exception $e) 
            {

                $this->response[$this->successTag] = 0;
                $this->response[$this->errorTag] = $e->getMessage();

                TTransaction::rollback();
                
            }
            
        }
			
	}
	
	private function getAllCentralMunicipio()
	{
		
		if( filter_input(INPUT_POST, $this->municipioIdTag ) )
		{
		
			$municipio_id = filter_input(INPUT_POST, $this->municipioIdTag );
			
			try
			{
			
				$this->response = array();
			
				TTransaction::open('pg_ceres');
			
				$repository = new TRepository('CentralRecord');
			
				$criteria = new TCriteria;
				
				$criteria->add( new TFilter( 'municipio_id', '=', $municipio_id ) );
			
				$criteria->setProperty('order', 'nome');

				$collection = $repository->load( $criteria );
			
				if( $collection )
				{
					 
					$this->response[$this->successTag] = 1;
			
					$i = 0;
			
					foreach ($collection as $object) 
					{
			
						$temp = array();
						$temp["id"] = $object->id;
						$temp["nome"] = $object->nome;
						$temp["nomearquivo"] = "http://" . $_SERVER["SERVER_NAME"] . "/centralcidadao/app/images/central/" . $object->nomearquivo;
			
						$this->response[$this->dadosTag][$i++] = $temp;
						
					}
			
				}else
				{
			
					$this->response[$this->successTag] = 2;
					$this->response[$this->errorTag] = "Nenhuma Central do Cidadão foi encontrada.";
			
				}
			
				TTransaction::close();
			
			}catch (Exception $e)
			{
			
				$this->response[$this->successTag] = 0;
				$this->response[$this->errorTag] = $e->getMessage();
			
				TTransaction::rollback();
			
			}
		
		}
		
	}
	
	private function getCentral()
	{
		
		if( filter_input(INPUT_POST, $this->centralIdTag ) ) 
		{
		
			$central_id = filter_input(INPUT_POST, $this->centralIdTag);
		
			try {
		
				$this->response = array();
		
				TTransaction::open('pg_ceres');
		
				$repository = new TRepository('vw_central_cidadaoRecord');
		
				$criteria = new TCriteria;
		
				$criteria->setProperty('order', 'nome_central');
		
				$criteria->add(new TFilter('central_id', '=', $central_id));
				$criteria->add(new TFilter('situacao', '=', 'ATIVO'));
		
				$collection = $repository->load($criteria);
		
				if ($collection) 
				{
					
					$this->response[$this->successTag] = 1;
		
					$i = 0;
		
					foreach ($collection as $object) 
					{
		
						$temp = array();
						$temp["id"]             = $object->central_id;
						$temp["nome"]           = $object->nome_central;
						$temp["municipio_nome"] = $object->municipio_nome;
						$temp["endereco"]       = $object->endereco;
						$temp["bairro"]         = $object->bairro;
						$temp["telefone"]       = $object->telefone;
						$temp["cep"]            = $object->cep;
						$temp["email"]          = $object->email;
						$temp["situacao"]       = $object->situacao;
						$temp["latitude"]       = $object->latitude;
						$temp["longitude"]      = $object->longitude;
						$temp["nomearquivo"] = "http://" . $_SERVER["SERVER_NAME"] . "/centralcidadao/app/images/central/" . $object->nomearquivo;

						$tempCentral = $temp;
						
					}
					
					$this->response[$this->successTag] = 1;
					$this->response[$this->dadosTag][0] = $tempCentral;
		
				}else 
				{
		
					$this->response[$this->successTag] = 2;
					$this->response[$this->errorTag] = "Nenhuma Central do Cidadão foi encontrada com este ID = " + $central_id + ".";
				
				}
		
				TTransaction::close();
				
			}catch( Exception $e )
			{
		
				$this->response[$this->successTag] = 0;
				$this->response[$this->errorTag] = $e->getMessage();
		
				TTransaction::rollback();
		
			}
		
		}	
		
	}

}

new CentralWebservice();
