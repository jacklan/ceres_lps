<?php

//ini_set('display_errors', 1);
//ini_set('display_startup_erros', 1);
//error_reporting(E_ALL);

header('Content-Type: application/json; charset=utf-8');

chdir('../../');

require_once 'init.php';

use Adianti\Database\TTransaction;
use Adianti\Database\TRepository;
use Adianti\Database\TCriteria;
use Adianti\Database\TFilter;


class GrupoServicoWebservice
{

	static public function getGrupoServico()
	{
		
		$successTag = "success";
		$errorTag = "error";
		$dadosTag = "dados";
		$moduloTag = "modulo";
		
		try 
		{
			
			
			$response = array();

			TTransaction::open('pg_ceres');
			
			$repository = new TRepository('GrupoServicoRecord');
			
			$criteria = new TCriteria;	
			$criteria->setProperty('order', 'nome');
			$criteria->add(new TFilter('situacao', '=', 'ATIVO'));
			$collection = $repository->load( $criteria );
			
			
			if( $collection )
			{
				
				$response[$successTag] = 1;
				
				$i = 0;
				
				foreach( $collection as $object )
				{
				
					$tempGrupoServico = array();
					
					$tempGrupoServico["id"] = $object->id;
					$tempGrupoServico["nome"] = $object->nome;
					$tempGrupoServico["abreviacao"] = $object->abreviacao;
					$tempGrupoServico["nomeimagem"] = "http://" . $_SERVER["SERVER_NAME"] . "/centralcidadao/app/images/servicos/" . $object->nomeimagem;
					
					
					$response[$dadosTag][$i++] = $tempGrupoServico; 
				
				}
				
			}else
			{
				
				$response[$successTag] = 2; //Não tem nenhum dado = 2
				
			}
			
			TTransaction::close();
			
		}catch( Exception $e ) 
		{
		
			$response[$successTag] = 0;
			$response[$errorTag] = $e->getMessage();
		
			TTransaction::rollback();
			
		}
		
        echo json_encode( $response );

	}
	
}

GrupoServicoWebservice::getGrupoServico();

?>