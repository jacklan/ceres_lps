<?php

ini_set( 'display_errors', 1 );
ini_set( 'display_startup_erros', 1 );
error_reporting( E_ALL );

chdir( '../../' );

require_once 'init.php';

use Adianti\Database\TTransaction;
use Adianti\Database\TRepository;
use Adianti\Database\TCriteria;
use Adianti\Database\TFilter;

class AgendamentoWebservice 
{
	
	private $response;
	
	private $successTag   = "success";
	private $errorTag     = "error";
	private $dadosTag     = "dados";
	
	private $cidadaoIdTag = "cidadao_id";
	private $servicoIdTag = "servico_id";
	private $centralIdTag = "central_id";
	private $dataTag 	  = "data";
	private $horaTag 	  = "hora";   
    private $agendamentoTag = "agendamento_id";
	
	private $actionTag 	  = "action";
	
    function __construct()
    {
    	
        switch( filter_input( INPUT_POST, $this->actionTag ) )
        {
        	case 1:
        		$this->agendarCidadao();
        		break;
            case 2:
                $this->getAgendamento();
                break;
            case 3:
            	$this->cancelarAgendamento();
            	break;
        }
        
        if( $this->response == null )
        	$this->response[$this->successTag] = 0;

        echo json_encode( $this->response );
        
    }

    private function getAgendamento(){


        if( filter_input( INPUT_POST, $this->agendamentoTag ) )
        {

            $agendamento_id = filter_input(INPUT_POST, $this->agendamentoTag);

            try
            {
        
                $this->response = array();
        
                TTransaction::open('database_connection');
        
                $repository = new TRepository('vw_agendamentoRecord');
        
                $criteria = new TCriteria;
                $criteria->add( new TFilter( 'agendamento_id', '=', $agendamento_id ) );
            
                $collection = $repository->load( $criteria );
                
                if( $collection )
                {
                     
                    $this->response[$this->successTag] = 1;
                     
                    $i = 0;
                     
                    foreach( $collection as $object )
                    {
        
	                    $tempAgendamento = array();
	                    
	                    $tempAgendamento["agendamento_id"]  = $object->agendamento_id;
	                    $tempAgendamento["numficha"]        = $object->numficha;
	                    $tempAgendamento["dataagendamento"] = TDate::date2br( $object->dataagendamento );
	                    $tempAgendamento["horaagendamento"] = $object->horaagendamento;
	                    $tempAgendamento["servico_id"]      = $object->servico_id;
	                    $tempAgendamento["nome_servico"]    = $object->nome_servico;
	                    
	                    $parsed = date_parse( $object->tempopadrao );
	                    $tempAgendamento["tempopadrao"]     = ( $parsed['minute'] == 0 ? $parsed['minute'] . '0' : $parsed['minute']  ) . 'min';
	                    
	                    $tempAgendamento["linkonline"]      = $object->linkonline;
	                    $tempAgendamento["valorservico"]    = $object->valorservico == null ? 'Gratuito' : 'R$ ' .  $object->valorservico ;
	                    $tempAgendamento["cidadao_id"]      = $object->cidadao_id;
	                    $tempAgendamento["cidadao_nome"]    = $object->cidadao_nome;
	                    $tempAgendamento["central_id"]      = $object->central_id;
	                    $tempAgendamento["central_nome"]    = $object->central_nome;
	                    $tempAgendamento["empresa_id"]      = $object->empresa_id;
	                    $tempAgendamento["empresa_nome"]    = $object->empresa_nome;
	                    $tempAgendamento["instrucoes"]      = $object->instrucoes; 
	                    $tempAgendamento["diaextenso"]      = $object->diaextenso != 'S�BADO' ? $object->diaextenso.'-FEIRA' : $object->diaextenso;
	                    
	                    $parsed = date_parse( $object->chegada );

	                    $tempAgendamento["chegada"] = $parsed['hour'] . 'h ' . ( $parsed['minute'] == 0 ? $parsed['minute'] . '0' : $parsed['minute']  ) . 'min';
	                    
	                    $parsed = date_parse( $object->inicioatendimento );
	                    
	                    $tempAgendamento["inicioatendimento"] = $parsed['hour'] . 'h ' . ( $parsed['minute'] == 0 ? $parsed['minute'] . '0' : $parsed['minute']  ) . 'min';
	                    
	                    $parsed = date_parse( $object->fimatendimento );
	                    
	                    $tempAgendamento["fimatendimento"]    = $parsed['hour'] . 'h ' . ( $parsed['minute'] == 0 ? $parsed['minute'] . '0' : $parsed['minute']  ) . 'min';
	                    
	                    $tempAgendamento["servidor_nome"]     = $object->servidor_nome;
	                     
	                    $tempAgendamento["agendamento_situacao"] = $object->agendamento_situacao;
	                    
	                    $this->response[$this->dadosTag][$i++] = $tempAgendamento; 
                
                    }

                    TTransaction::close();

                }else 
                {

                    $this->response[$this->successTag] = 2; //Não tem nenhum dado = 2
                
                }

            }catch( Exception $e ) 
            {
            
                $this->response[$this->successTag] = 0;
                $this->response[$this->errorTag] = $e->getMessage();
            
                TTransaction::rollback();
                
            }

        }

    }

    private function agendarCidadao()
    {    

    	if( filter_input( INPUT_POST, $this->cidadaoIdTag ) &&
    			filter_input( INPUT_POST, $this->servicoIdTag ) &&
    			filter_input( INPUT_POST, $this->centralIdTag ) &&
    			filter_input( INPUT_POST, $this->dataTag ) &&
    			filter_input( INPUT_POST, $this->horaTag ) )
    	{
    	
    		$cidadao_id = filter_input( INPUT_POST, $this->cidadaoIdTag );
    		$servico_id = filter_input( INPUT_POST, $this->servicoIdTag );
    		$central_id = filter_input( INPUT_POST, $this->centralIdTag );
    		$data 	 	= filter_input( INPUT_POST, $this->dataTag );
    		$hora 	    = filter_input( INPUT_POST, $this->horaTag );
    	
    		try
    		{
    	
    			$this->response = array();
    			
    			TTransaction::open('database_connection');
    			$conn = TTransaction::get(); // obt�m a conex�o
    			
    			$sth = $conn->prepare('
    					SELECT a.id AS agendamento_id 
    					FROM agendamento a 
    						JOIN box b ON b.id = a.box_id 
    						JOIN central c ON c.id = b.central_id 
    					WHERE 
    						a.servico_id=\'' . $servico_id . '\' AND ' .
    						'c.id=\'' . $central_id . '\' AND ' .
    						'a.dataagendamento=\'' . $data . '\' AND ' .
    						'a.horaagendamento=\'' . $hora . '\' AND ' .
    						'a.cidadao_id IS NULL;');
    			
    			$sth->execute();
    			
    			$result = $sth->fetchAll();
    			
    			if( $result )
    			{
    				 
    				$this->response[$this->successTag] = 1;
    				 
    				$i = 0;
    				 
    				foreach( $result as $object )
    				{
    					
    					$agendamento = new AgendamentoRecord( $object['agendamento_id'] );
    	
    					$agendamento->cidadao_id = $cidadao_id;
    					$agendamento->dataalteracao = date("d/m/Y H:i:s");
    					$agendamento->usuarioalteracao = 'c_' . $cidadao_id;
    	
    					$agendamento->store();
    	
    					$temp = array();
    	
    					$temp["id"] = $agendamento->id;
    	
    					$this->response[$this->dadosTag][$i++] = $temp;
    	
    					break;
    	
    				}
    				 
    			}else
    			{
    				 
    				$this->response[$this->successTag] = 2;
    				$this->response[$this->errorTag] = "Nenhum Agendamento encontrado.";
    				 
    			}
    	
    			TTransaction::close();
    	
    		}catch( Exception $e )
    		{
    	
    			$this->response[$this->successTag] = 3;
    			
    			$exceptionMsg = substr( $e->getMessage() , 42 );
    			
    			$this->response[$this->errorTag] = $exceptionMsg;
    	
    			TTransaction::rollback();
    	
    		}
    		 
    	}
    	
    }

    private function cancelarAgendamento()
    {
    	
    	if( filter_input( INPUT_POST, $this->agendamentoTag ) )
        {   
        	
            $agendamento_id = filter_input( INPUT_POST, $this->agendamentoTag );  

            $this->response = array();
            
            try
            {

	            TTransaction::open('database_connection');
	         
	            $agendamentoRecord =  new AgendamentoRecord( $agendamento_id );
	
	            if( $agendamentoRecord )
	            {
	
	                $cancelamentoRecord = new CancelamentoRecord();
	
	                $cancelamentoRecord->cidadao_id       = $agendamentoRecord->cidadao_id;
	                $cancelamentoRecord->agendamento_id   = $agendamentoRecord->id;
	                $cancelamentoRecord->datacancelamento = date( "d/m/Y" );
	
	                $cancelamentoRecord->store();

	                $agendamentoRecord->cidadao_id = '';
	                $agendamentoRecord->store();
	
	                $this->response[$this->successTag] = 1;
	
	            }

                TTransaction::close();

        	}catch( Exception $e ) 
            {

                $this->response[$this->successTag] = 0;
                $this->response[$this->errorTag] = $e->getMessage();

                TTransaction::rollback();

            }
            

        }

    }
    
}

new AgendamentoWebservice();