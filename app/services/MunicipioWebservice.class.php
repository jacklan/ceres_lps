<?php

//ini_set('display_errors', 1);
//ini_set('display_startup_erros', 1);
//error_reporting(E_ALL);

header('Content-Type: application/json; charset=utf-8');

chdir('../../');

require_once 'init.php';

use Adianti\Database\TTransaction;
use Adianti\Database\TRepository;
use Adianti\Database\TCriteria;
use Adianti\Database\TFilter;


class MunicipioWebservice
{

	private $response;
	
	private $actionTag = "action";
	
	private $successTag = "success";
	private $errorTag = "error";
	private $dadosTag = "dados";
	
	private $servicoIdTag = 'servico_id';
	
	function __construct()
	{
		
		switch( filter_input( INPUT_POST, $this->actionTag ) )
		{
				
			case 1:
				$this->getAllMunicipiosFiltro();
				break;
			case 2:
				$this->getAllMunicipios();
				break;
		
		}
		
		if( $this->response == null )
			$this->response[$this->successTag] = 0;
		
			echo json_encode( $this->response );
		
	}

	private function getAllMunicipiosFiltro()
	{

		if( filter_input(INPUT_POST, $this->servicoIdTag ) )
		{
			
			$servico_id = filter_input(INPUT_POST, $this->servicoIdTag );
			
			try 
			{
					
				$this->response = array();
		
				TTransaction::open('pg_ceres');
					
				$repository = new TRepository('vw_municipio_centralRecord');
					
				$criteria = new TCriteria;	
					
				$criteria->setProperty( 'order', 'nome' );
					
				$criteria->add( new TFilter( 'servico_id', '=', $servico_id ) );
					
				$collection = $repository->load( $criteria );
					
				if( $collection )
				{
						
					$this->response[$this->successTag] = 1;
						
					$i = 0;
						
					foreach( $collection as $object )
					{
						
						$tempMunicipio = array();
							
						$tempMunicipio["id"] = $object->id;
						$tempMunicipio["nome"] = $object->nome;
		
						$this->response[$this->dadosTag][$i++] = $tempMunicipio; 
						
					}
						
				}else
				{
						
					$this->response[$this->successTag] = 2; //Não tem nenhum dado = 2
						
				}
				
				TTransaction::close();
					
			}catch( Exception $e ) 
			{
				
				$this->response[$this->successTag] = 0;
				$this->response[$this->errorTag] = $e->getMessage();
				
				TTransaction::rollback();
					
			}

		}
		
	}
	
	private function getAllMunicipios()
	{
		
		try
		{
				
			$this->response = array();
		
			TTransaction::open('pg_ceres');
				
			$repository = new TRepository('vw_municipio_centralRecord');
				
			$criteria = new TCriteria;
				
			$criteria->setProperty( 'order', 'nome' );

			$collection = $repository->load( $criteria );
				
			if( $collection )
			{
		
				$this->response[$this->successTag] = 1;
		
				$i = 0;
		
				foreach( $collection as $object )
				{
		
					$tempMunicipio = array();
						
					$tempMunicipio["id"] = $object->id;
					$tempMunicipio["nome"] = $object->nome;
		
					$this->response[$this->dadosTag][$i++] = $tempMunicipio;
		
				}
		
			}else
			{
		
				$this->response[$this->successTag] = 2; //Não tem nenhum dado = 2
		
			}
		
			TTransaction::close();
				
		}catch( Exception $e )
		{
		
			$this->response[$this->successTag] = 0;
			$this->response[$this->errorTag] = $e->getMessage();
		
			TTransaction::rollback();
				
		}
		
		
	}
	
}

new MunicipioWebservice();

?>