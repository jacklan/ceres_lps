<?php

ini_set('display_errors', 1);
ini_set('display_startup_erros', 1);
error_reporting(E_ALL);

chdir('../../');

require_once 'init.php';

use Adianti\Database\TTransaction;
use Adianti\Database\TRepository;
use Adianti\Database\TCriteria;
use Adianti\Database\TFilter;

class CidadaoWebservice
{

    private   $actionTag         = "action";
    private   $successTag        = "success";
    private   $errorTag          = "error";    
    private   $dadosTag          = "dados";

    private   $filtroTag         = "filtro";

    private   $cidadaoIdTag      = "id";
    private   $cidadaoTag        = "cidadao";
    private   $nome_cidadaoTag   = "nome";
    private   $cpf_cidadaoTag    = "cpf";
    private   $senha_cidadaoTag  = "senha";
    private   $telefonefixoTag   = "telefone";
    private   $dddfixoTag        = "dddfixo";
    private   $celularTag        = "celular";
    private   $dddcelularTag     = "dddcelular";
    private   $emailTag          = "email";
    private   $novasenhaTag      = "novasenha";
    
    function __construct()
    {
        
        switch( filter_input( INPUT_POST, $this->actionTag ) )
        {
            case 1:
               $this->cidadaoLogin();
               break;
            case 2:
               $this->getCidadao();
               break;
            case 3:
               $this->insertCidadao();
               break;
            case 4:
               $this->getAgendamento();
               break;
            case 5:
                $this->alterarSenha();
                break;
            case 6:
               	$this->updateCidadao();
               	break;
        }
        
        if( $this->response == null )
            $this->response[$this->successTag] = 0;

        echo json_encode( $this->response );
        
    }

    private function cidadaoLogin() 
    {

        if (filter_input(INPUT_POST, $this->cpf_cidadaoTag ) && filter_input(INPUT_POST, $this->senha_cidadaoTag )) 
        {

            $RegistrosCidadaoCPF   = filter_input(INPUT_POST, $this->cpf_cidadaoTag);
            $RegistrosCidadaoSenha = md5( filter_input( INPUT_POST, $this->senha_cidadaoTag ) );
               
            try 
            {

               $this->response = array();

                TTransaction::open('pg_ceres');

                $repository = new TRepository('CidadaoRecord');

                $criteria = new TCriteria;

                $criteria->setProperty('order', 'nome');

                $criteria->add(new TFilter('cpf',   '=', $RegistrosCidadaoCPF));
                $criteria->add(new TFilter('senha', '=', $RegistrosCidadaoSenha));

                $collection = $repository->load($criteria);

                if ($collection) 
                {
                	
                    $this->response[$this->successTag] = 1;

                    $i = 0;

                    foreach ($collection as $object) 
                    {
                        
                        $temp = array();
                        $temp["id"]           = $object->id;
                        $temp["nome"]         = $object->nome;
                        $temp["cpf"]          = $object->cpf;
                        $temp["telefonefixo"] = $object->telefonefixo;
                        $temp["dddfixo"]      = $object->dddfixo;
                        $temp["celular"]      = $object->celular;
                        $temp["dddcelular"]   = $object->dddcelular;
                        $temp["email"]        = $object->email;

                       $this->response[$this->dadosTag][$i++] = $temp;
                       
                    }
                    
                }else 
                {
                	
                   $this->response[$this->successTag] = 2;
                   $this->response[$this->errorTag] = "Nenhum Cidadão foi encontrado com as seguintes informações nos Registros da Central do Cidadão.";
                
                }

                TTransaction::close();

            }catch (Exception $e) 
            {

              $this->response[$this->successTag] = 0;
              $this->response[$this->errorTag] = $e->getMessage();

                TTransaction::rollback();
            }
            
        }
        
    }

    private function getCidadao() 
    {

          if (filter_input(INPUT_POST, $this->cidadaoIdTag)) 
          {

            $cidadao_id = filter_input(INPUT_POST, $this->cidadaoIdTag);

            try {

                $this->response = array();

                TTransaction::open('pg_ceres');

                $repository = new TRepository('CidadaoRecord');

                $criteria = new TCriteria;

                $criteria->setProperty('order', 'nome');

                $criteria->add(new TFilter('id', '=', $cidadao_id));

                $collection = $repository->load($criteria);

        //____________________________________________________________________________________________________________________\\

                if ($collection) {
                	
                   $this->response[$this->successTag] = 1;

                    $i = 0;

                   foreach ($collection as $object) {

                        $temp = array();
                        $temp["id"]           = $object->id;
                        $temp["nome"]         = $object->nome;
                        $temp["cpf"]          = $object->cpf;
                        $temp["telefonefixo"] = $object->telefonefixo;
                        $temp["dddfixo"]      = $object->dddfixo;
                        $temp["celular"]      = $object->celular;
                        $temp["dddcelular"]   = $object->dddcelular;
                        $temp["email"]        = $object->email;

                        $tempCidadao = $temp;
                        
                    }
                    $this->response[$this->successTag] = 1;
                    $this->response[$this->dadosTag][0] = $tempCidadao;
                    
                } 
        //____________________________________________________________________________________________________________________\\

                else {

                    $this->response[$this->successTag] = 2;
                    $this->response[$this->errorTag] = "Nenhum Cidadão foi encontrado com as seguintes informações nos Registros da Central do Cidadão." + $central_id + ".";
                }



                TTransaction::close();

            }catch( Exception $e ) 
            {

                $this->response[$this->successTag] = 0;
                $this->response[$this->errorTag] = $e->getMessage();

                TTransaction::rollback();

            }
        
        }
        
    }

    private function insertCidadao() 
    {
    	
        if( filter_input( INPUT_POST, $this->cidadaoTag ) )
        {

            $cidadao = json_decode( filter_input( INPUT_POST, $this->cidadaoTag ), true);
            
             try 
             {

             	TTransaction::open( 'pg_ceres' );
             	
             	$repository = new TRepository( 'CidadaoRecord' );
             	
             	$criteria = new TCriteria;
             	
             	$criteria->add( new TFilter( 'cpf',   '=', $cidadao['cpf'] ) );
             	
             	$collection = $repository->load( $criteria );

                $this->response = array();

                if( !$collection )
                {
 
                	
                	
	                $cidadaoRecord = new CidadaoRecord();
	
	                $cidadaoRecord->nome = $cidadao['nome'];
	                $cidadaoRecord->cpf = $cidadao['cpf'];
	                $cidadaoRecord->senha = md5( $cidadao['senha'] );
	                $cidadaoRecord->dddfixo = $cidadao['dddfixo'];
	                $cidadaoRecord->telefonefixo = $cidadao['telefonefixo'];
	                $cidadaoRecord->celular = $cidadao['celular'];
	                $cidadaoRecord->dddcelular = $cidadao['dddcelular'];
	                $cidadaoRecord->email = $cidadao['email'];         
	
	                $cidadaoRecord->store();
	
	                $this->response[$this->successTag] = 1;
                
                }else
                {
                	
                	$this->response[$this->successTag] = 3;
                
                }
                
                TTransaction::close();

            }catch( Exception $e ) 
            {

                $this->response[$this->successTag] = 0;
                $this->response[$this->errorTag] = $e->getMessage();

                TTransaction::rollback();

            }
            
        }
        
    }
    
    private function updateCidadao()
    {
    
    	if( filter_input( INPUT_POST, $this->cidadaoIdTag ) && filter_input( INPUT_POST, $this->cidadaoTag ) )
    	{
    
    		$cidadaoId = filter_input( INPUT_POST, $this->cidadaoIdTag );
    		$cidadaoUpdate = json_decode( filter_input(INPUT_POST, $this->cidadaoTag ), true);

    		try
    		{
    
    			TTransaction::open( 'pg_ceres' );
    
    			$cidadao = new CidadaoRecord( $cidadaoId );
    
    			$this->response = array();
    
    			if( $cidadao )
    			{

    				$cidadao->dddfixo = $cidadaoUpdate['dddfixo'];
    				$cidadao->telefonefixo = $cidadaoUpdate['telefone'];
    				$cidadao->dddcelular = $cidadaoUpdate['dddcelular'];
    				$cidadao->celular = $cidadaoUpdate['celular'];
    				$cidadao->email = $cidadaoUpdate['email'];
    
    				$cidadao->store();
    
    				$this->response[$this->successTag] = 1;
    
    			}else
    			{
    				 
    				$this->response[$this->successTag] = 3;
    
    			}
    
    			TTransaction::close();
    
    		}catch( Exception $e )
    		{
    
    			$this->response[$this->successTag] = 0;
    			$this->response[$this->errorTag] = $e->getMessage();
    
    			TTransaction::rollback();
    
    		}
    
    	}
    
    }
    
    private function getAgendamento() 
    {

         if( filter_input( INPUT_POST, $this->cidadaoIdTag )  && filter_input(INPUT_POST, $this->filtroTag)  )
         {

                $cidadoId = filter_input( INPUT_POST, $this->cidadaoIdTag); 
                $filtro   = filter_input(INPUT_POST, $this->filtroTag);

                try 
                {

                    TTransaction::open( 'pg_ceres' );
                    
                    $repository = new TRepository( 'vw_agendamento_cidadaoRecord' );
                    
                    $criteria = new TCriteria;
                    
                    $criteria->add( new TFilter( 'cidadao_id', '=', $cidadoId ) ); 
                    $criteria->setProperty('order', 'dataagendamento DESC, agendamento_situacao ASC, horaagendamento DESC');

                    $collection = $repository->load( $criteria );

                    $this->response = array();

                    if ($collection)
                    {
                        
                       $this->response[$this->successTag] = 1;

                        $i = 0;

                       foreach ($collection as $object) 
                       {

                            $tempCidadao = array();

                            $tempCidadao["agendamento_id"]    = $object->agendamento_id;
                            $tempCidadao["servico_nome"]      = $object->servico_nome;
                            $tempCidadao["dataagendamento"]   = $object->dataagendamento;
                            $tempCidadao["horaagendamento"]   = $object->horaagendamento;
                            $tempCidadao["central_nome"]      = $object->central_nome;

                            if( $filtro == "all" ) 
                            {
                               
                                $tempCidadao["cidadao_id"]              = $object->cidadao_id;
                                $tempCidadao["box_id"]                  = $object->box_id;
                                $tempCidadao["boxusuario_id"]           = $object->boxusuario_id;
                                $tempCidadao["servico_id"]              = $object->servico_id;
                                $tempCidadao["chegada"]                 = $object->chegada;
                                $tempCidadao["inicioatendimento"]       = $object->inicioatendimento;
                                $tempCidadao["fimatendimento"]          = $object->fimatendimento;
                                $tempCidadao["observacao"]              = $object->observacao;
                                $tempCidadao["numficha"]                = $object->numficha;
                                $tempCidadao["box_nome"]                = $object->box_nome;
                                $tempCidadao["servidor_nome"]           = $object->servidor_nome;
                                $tempCidadao["abreviacao"]              = $object->abreviacao;
                                $tempCidadao["instrucoes"]              = $object->instrucoes;
                                $tempCidadao["linkonline"]              = $object->linkonline;
                                $tempCidadao["valorservico"]            = $object->valorservico;
                                $tempCidadao["empresa_nome"]            = $object->empresa_nome;
                                $tempCidadao["datacancelamento"]        = $object->datacancelamento;
                                $tempCidadao["diaextenso_cancelamento"] = $object->diaextenso_cancelamento;
                                $tempCidadao["diaextenso"]              = $object->diaextenso;
                                $tempCidadao["agendamento_situacao"]    = $object->agendamento_situacao;

                            }

                            $this->response[$this->successTag] = 1;
                            $this->response[$this->dadosTag][$i++] = $tempCidadao;

                        }

                    }else 
                    {
                    	
                    	$this->response[$this->successTag] = 2;
                    	
                    }
                    

            }catch( Exception $e ) 
            {

                $this->response[$this->successTag] = 0;
                $this->response[$this->errorTag] = $e->getMessage();

                TTransaction::rollback();

            }
            
        }

    }

    private function alterarSenha()
    {
        
         if( filter_input( INPUT_POST, $this->senha_cidadaoTag ) && 
         	 filter_input( INPUT_POST, $this->cidadaoIdTag ) && 
         	 filter_input( INPUT_POST, $this->novasenhaTag ) )
        {

         $cidadaoid  = filter_input( INPUT_POST, $this->cidadaoIdTag );
         $senhaAtual = md5( filter_input( INPUT_POST, $this->senha_cidadaoTag ) );
         $novaSenha  = md5( filter_input( INPUT_POST, $this->novasenhaTag ) );

         try
         {

            TTransaction::open( 'pg_ceres' );

             $this->response = array();

             $cidadaoRecord  = new CidadaoRecord( $cidadaoid );
             
             if( $cidadaoRecord && $cidadaoRecord->senha == $senhaAtual )
             {

                $cidadaoRecord->senha = $novaSenha;

                $cidadaoRecord ->store();
                
                $this->response[$this->successTag] = 1;
                
                TTransaction::close();

            }else
            {

                $this->response[$this->successTag] = 2;

            }


            }catch( Exception $e ) 
            {

                $this->response[$this->successTag] = 0;
                $this->response[$this->errorTag] = $e->getMessage();

                TTransaction::rollback();
            }    
        } 
    }
}

new CidadaoWebservice();

?>