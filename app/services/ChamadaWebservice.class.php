<?php

ini_set('display_errors', 1);
ini_set('display_startup_erros', 1);
error_reporting(E_ALL);

header('Content-Type: application/json; charset=utf-8');

chdir('../../');

require_once 'init.php';

use Adianti\Database\TTransaction;
use Adianti\Database\TRepository;
use Adianti\Database\TCriteria;
use Adianti\Database\TFilter;

/*
 * Classe ServicoWebservice
 * Autor: Jonas Jord�o
 * Data: 27/10/2015
 */
class ChamadaWebservice 
{

	private $boxIdTag = "box_id";
	
	private $response;
	
    function __construct()
    {
    	
    	$this->getChamadas();

    	echo json_encode( $this->response );
    		
    }
    
    function getChamadas()
    {
    	
    	if( filter_input( INPUT_POST, $this->boxIdTag ) )
    	{
    		
    		$boxId = filter_input( INPUT_POST, $this->boxIdTag );
    		
    		try
    		{
    			
    			$this->response = array();
    			 
    			TTransaction::open('database_connection');
    			 
    			$repository = new TRepository('vw_chamada_agendamentoRecord');
    			 
    			$criteria = new TCriteria;
    			 
    			$criteria->add( new TFilter( 'box_id', '=', $boxId ) );
    			 
    			$collection = $repository->load( $criteria );
    			 
    			if( $collection )
    			{

    				$i = 0;
    				 
    				foreach( $collection as $object )
    				{
    			
    					$temp = array();

    					$temp["hora"] = $object->horaagendamento;
    					$temp["cidadao"] = $object->cidadao_nome;
    					$temp["servico"] = $object->servico_nome;
    					$temp["box"] = $object->box_nome;

    					$this->response[$i++] = $temp;
    					 
    				}
    				 
    			}
    			 
    			TTransaction::close();
    			
    		}catch ( Exception $e )
    		{
    			 
    			TTransaction::rollback();
    			
    		}
    		
    	}
    	
    }

}

new ChamadaWebservice();