<?php
/**
 * Register LOG in TXT files
 *
 * @version    2.0
 * @package    log
 * @author     Pablo Dall'Oglio
 * @copyright  Copyright (c) 2006-2014 Adianti Solutions Ltd. (http://www.adianti.com.br)
 * @license    http://www.adianti.com.br/framework-license
 */
class TGlobalLogger extends TLogger
{
    private static $database;
    
    /**
     * Writes an message in the LOG file
     * @param  $message Message to be written
     */
    public function write($message)
    {
        $level = 'Debug';
        $parts = explode(':', $message);
        if (count($parts) == 2)
        {
            $level   = $parts[0];
            $message = $parts[1];
        }
        
        $dbinfo = TTransaction::getDatabaseInfo();
        
        $time = date("Y-m-d H:i:s");
        // define the LOG content
        $text = "$level: $time - $message\n";
        // add the message to the end of file
        $handler = fopen('/tmp/global.txt', 'a');
        if (self::$database !== $dbinfo['name'])
        {
            fwrite($handler, 'Database: '. $dbinfo['name'] . "\n");
            self::$database = $dbinfo['name'];
        }
        fwrite($handler, $text);
        fclose($handler);
    }
}
