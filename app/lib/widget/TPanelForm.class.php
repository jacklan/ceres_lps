<?php
/**
 * classe TPanelForm
 * Painel de posicoes fixas sem Borda e cor
 */
class TPanelForm extends TElement
{

    //define o espacamento entre as linhas
    private $espacamento;
    //define a linha
    private $linha;
    //define a coluna do Label
    private $coluna;
    //define a coluna do campo
    private $coluna2;

    /**
     * metodo __construct()
     * instancia objeto TPanelForm.
     * @param  $width   = largura do painel
     * @param  $height  = altura do painel
     */
    public function __construct($width, $height)
    {
        // instancia objeto TStyle
        // para definir as caracter�sticas do painel
        $painel_style = new TStyle('tpanelform');
        $painel_style->position         = 'relative';
        $painel_style->width            = $width;
        $painel_style->height           = $height;

        //define o valor do espacamento entre as linhas
        $this->espacamento = 25;
        //define da linha
        $this->linha = 10;
        //define da coluna
        $this->coluna = 150;
        //define da coluna dos campos
        $this->coluna2 = 100;

        // exibe o estilo na tela
        $painel_style->show();
        
        parent::__construct('div');
        $this->class = 'tpanelform';
    }



    /**
     * metodo put()
     * posiciona um objeto no painel
     * @param  $widget = objeto a ser inserido no painel
     * @param  $col    = coluna em pixels.
     * @param  $row    = linha em pixels.
     */
    public function put($widget, $col, $row)
    {
        // cria uma camada para o widget
        $camada = new TElement('div');
        // define a posi��o da camada
        $camada->style = "left:{$col}px; top:{$row}px;";
        // adiciona o objeto (widget) a camada rec�m-criada
        $camada->add($widget);
        
        // adiciona widget no array de elementos
        parent::add($camada);

    }

    /*
     * metodo putCampo()
     * Inclui os campos em um panel
     * $painel -> Panel onde os campos deverao ser incluidos
     * $campo -> objeto Field
     * $capitonLabel -> Rotulo do Objeto
     * $deslocamento -> Valor do deslocamento em pixel
     */
    public function putCampo($campo, $captionLabel, $deslocamento, $quebraLinha)
    {
        if ($quebraLinha == 1){
           $this->linha = ($this->linha + $this->espacamento);
        }
        if ($captionLabel != null){
           $this->put(new TLabel($captionLabel),$this->coluna, $this->linha);
        }
        if ($campo != null){
           $this->put($campo,$this->coluna+$this->coluna2+$deslocamento,$this->linha);
        }
    }


 public function putCampo2($campo, $captionLabel, $deslocamentototal, $deslocamento, $quebraLinha)
    {
        if ($quebraLinha == 1){
           $this->linha = ($this->linha + $this->espacamento);
        }
        if ($captionLabel != null){
           $this->put(new TLabel($captionLabel),$this->coluna+$deslocamentototal, $this->linha);
        }
        if ($campo != null){
           $this->put($campo,$this->coluna+$this->coluna2+$deslocamento+$deslocamentototal,$this->linha);
        }
    }
    /*
     * metodo setEspacamento()
     * Define o espacamento das linhas no panel
     * $valor -> Valor do espacamento em pixel
     */
    public function setEspacamento($valor)
    {
        $this->espacamento = $valor;
    }

    /*
     * metodo getEspacamento()
     * Retorna o espacamento das linhas no panel
     */
    public function getEspacamento()
    {
        return $this->espacamento;
    }

    /*
     * metodo setLinha()
     * Define a posicao da linha no panel
     * $valor -> Valor da posicao da linha em pixel
     */
    public function setLinha($valor)
    {
        $this->linha = $valor;
    }

    /*
     * metodo getLinha()
     * Retorna a posicao da linha no panel
     */
    public function getLinha()
    {
        return $this->linha;
    }

    /*
     * metodo setColuna()
     * Define a posicao da primeira coluna no panel
     * $valor -> Valor da primeira coluna em pixel
     */
    public function setColuna($valor)
    {
        $this->coluna = $valor;
    }

    /*
     * metodo getColuna()
     * Retorna a posicao da primeira coluna no panel
     */
    public function getColuna()
    {
        return $this->coluna;
    }

    /*
     * metodo setColuna2()
     * Define a posicao da segunda coluna no panel
     * $valor -> Valor da segunda coluna em pixel
     */
    public function setColuna2($valor)
    {
        $this->coluna2 = $valor;
    }

    /*
     * metodo getColuna2()
     * Retorna a posicao da segunda coluna no panel
     */
    public function getColuna2()
    {
        return $this->coluna2;
    }


}
?>