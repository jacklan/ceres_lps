<?php
/**
 * classe TConfirmation
 * Exibe perguntas ao usuario
 */
class TConfirmation
{
    /**
     * metodo construtor
     * exibe a mensagem final
     * @param  $message = mensagem para o usuario
     * @param  $action_yes = acao para a message
     */
    function __construct($message, TAction $action_yes)
    {
        $style = new TStyle('tconfirmation');
        $style->position     = 'absolute';
        $style->left         = '50%';
        $style->margin_left  = '-200px';
        $style->top          = '30%';
        $style->width        = '400';
        $style->color        = 'black';
        $style->background   = '#FFFFFF';
        $style->padding      = '15px 30px 15px 15px';
        $style->border       = '1px solid #AAAAAA';
        $style->border_radius= '15px 0 15px 15px';
        $style->box_shadow   = '0px 0px 10px #000000, 0px 0px 30px #CCCCCC inset';
        $style->z_index      = 1000;
        
        // converte os nomes de metodos em URL's
        $url_yes = $action_yes->serialize();
        
        // exibe o estilo na tela
        $style->show();
        
        // instancia o painel para exibir o dialogo
        $painel = new TElement('div');
        $painel->class = "tconfirmation";
        
        // cria um botao para a resposta positiva
        $button1 = new TElement('input');
        $button1->type = 'button';
        $button1->value = 'Ok';
        $button1->onclick="javascript:location='$url_yes'";
        
        // cria um tabela para organizar o layout
        $table = new TTable;
        $table->align = 'left';
        $table->cellspacing = 10;
        // cria uma linha para o icone e a mensagem
        $row=$table->addRow();
        $row->addCell("<img src=\"app.images/ico_close.png\" width=\"18\" height=\"18\" onclick=\"javascript:location='$url_yes';\" style=\"position:absolute; left:436px; top:-10px; cursor:pointer\" />");
        $row->addCell(new TImage('app.images/info.png'));
        $row->addCell('<b style=\'padding:10px 0; font-size:14px;\'>'.$message.'</b>');
        
        // adiciona a tabela ao painel
        $painel->add($table);
        // exibe o painel
        $painel->show();
    }
}
?>