<?php

/**
 * classe TForm
 * Classe para construcao de formularios
 */
class TForm {

    protected $fields;      // array de objetos contidos pelo form
    private $action;
    private $param;
    private $name;        // nome do formulario
    private $label;        // nome do formulario
    private $enctype;        // define o enctype
    private $newWindow = false;

    /**
     * metodo construtor
     * instancia o formulario
     * @param  $name = nome do formulario
     */
    public function __construct($name = 'my_form', $action = '#') {
        $this->setName($name);
        $this->setAction($action);
    }

    /**
     * metodo setAction
     * define a acao do botao (funcao a ser executada)
     * @param  $class  = acao do botao
     * @param  $label   = rotulo do botao
     */
    public function setAction($class) {
        $this->action = "index.php?class=" . $class . "&method=onSave" . "&fk=".$_GET['fk']. "&did=".$_GET['did'];
    }

    /**
     * metodo setNewWindow
     * define se o botao vai criar uma nova janela
     * @param  $nova
     */
    public function setNewWindow($nova) {
        $this->newWindow = $nova;
    }

    /**
     * metodo setName()
     * define o nome do formulario
     * @param  $name    = nome do formulario
     */
    public function setName($name) {
        $this->name = $name;
    }

    /**
     * metodo setEditable()
     * Define se o formulario podera ser editado
     * @param  $bool = TRUE ou FALSE
     */
    public function setEditable($bool) {
        if ($this->fields) {
            foreach ($this->fields as $object) {
                $object->setEditable($bool);
            }
        }
    }

    /**
     * metodo setFields()
     * Define quais s�o os campos do formulario
     * @param  $fields = array de objetos TField
     */
    public function setFields($fields) {
        foreach ($fields as $field) {
            if ($field instanceof TField) {
                $name = $field->getName();
                $this->fields[$name] = $field;

                if ($field instanceof TButton) {
                    $field->setFormName($this->name);
                }
            }
        }
    }

    /**
     * metodo getField()
     * Retorna um campo do formulario por seu nome
     * @param  $name    = nome do campo
     */
    public function getField($name) {
        return $this->fields[$name];
    }

    /**
     * metodo setData()
     * Atribui dados aos campos do formulario
     * @param  $object = objeto com dados
     */
    public function setData($object) {
        foreach ($this->fields as $name => $field) {
            if ($name) { // labels Nao possuem nome
                $field->setValue($object->$name);
            }
        }
    }

    /**
     * metodo getData()
     * Retorna os dados do formulario em forma de objeto
     */
    public function getData($class = 'StdClass') {
        $object = new $class;
        foreach ($_POST as $key => $val) {
            if (get_class($this->fields[$key]) == 'TCombo') {
                if ($val !== '0') {
                    $object->$key = $val;
                }
            } else {
                if ($key != 'save') {
                    $object->$key = $val;
                }
            }
        }

        // percorre os arquivos de upload
        foreach ($_FILES as $key => $content) {
            $object->$key = $content['tmp_name'];
        }

        return $object;
    }

    /**
     * metodo getFieldData()
     * Retorna o valor de um campo do formulario por seu nome
     * @param  $name    = nome do campo
     */
    public function getFieldData($name) {
        foreach ($_POST as $key => $val) {
            if ($this->fields[$key] == $this->fields[$name]) {
                return $val;
            }
        }
    }

    /**
     * metodo add()
     * Adiciona um objeto no formulario
     * @param  $object = objeto a ser adicionado
     */
    public function add($object) {
        $this->child = $object;
    }

    /**
     * metodo setEnctype()
     * Define o enctype
     * @param  $valor = valor do enctype
     */
    public function setEnctype($valor) {
        $this->enctype = $valor;
    }

    /**
     * metodo setParameter()
     * acrescenta um parametro ao metodo a ser executdao
     * @param  $param = nome do parametro
     * @param  $value = valor do parametro
     */
    public function setParameter($param, $value) {
        $this->param[$param] = $value;
    }

    /**
     * metodo serialize()
     * transforma a acao em uma string do tipo URL
     */
    public function serialize() {
        // verifica se a acao a um metodo
        if (is_array($this->action)) {
            // obtem o nome da classe
            $url['class'] = get_class($this->action[0]);
            if ($url['class'] == '0') {
                $url['class'] = $this->action[0];
            }
            // obtem o nome do metodo
            $url['method'] = $this->action[1];
        } else if (is_string($this->action)) { // a uma string
            // obtem o nome da funcao
            $url['method'] = $this->action;
        }

        // verifica se ha parametros
        if ($this->param) {
            $url = array_merge($url, $this->param);
        }

        // monta a URL
        return '?' . http_build_query($url);
    }

    /**
     * metodo show()
     * Exibe o formulario na tela
     */
    public function show() {

        //  $url = $this->action->setParameter('class',$url);
        // instancia TAG de formulario
        $tag = new TElement('form');
        $tag->name = $this->name; // nome do formulario
        $tag->action = $this->action; // nome do formulario
        $tag->method = 'post';      // metodo de transfer�ncia
        if ($this->newWindow == true) {
            $tag->target = '_blank';
        }
        // se o campo enctype apresenta valor
        if (isset($this->enctype)) {
            $tag->enctype = $this->enctype;
        }
        // define a acao do botao
        //$this->tag->action = "teste.php";
        // adiciona o objeto filho ao formulario
        $tag->add($this->child);
        // exibe o formulario
        $tag->show();
    }

}

?>