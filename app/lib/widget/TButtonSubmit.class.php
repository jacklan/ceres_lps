<?php
/* classe TButtonSubmit
 * responsavel por exibir um botaos
 */
class TButtonSubmit extends TField
{
    private $action;
    private $label;
     
    /**
     * metodo setAction
     * define a acao do botao (funcao a ser executada)
     * @param  $action  = acao do botao
     * @param  $label   = rotulo do botao
     */
    public function setAction($action, $label)
    {
        $this->action = $action;
        $this->label  = $label;
    }
    
     /**
     * metodo setFormName
     * define o nome do formulario para a acao botao
     * @param  $name = nome do formulario
     */
    public function setFormName($name)
    {
        $this->formName = $name;
    }
    /**
     * metodo show()
     * exibe o botao
     */
    public function show()
    {
       // $url = $this->action->serialize();
        
        // define as propriedades do botao
        $this->tag->name    = $this->name;    // nome da TAG
        $this->tag->type    = 'submit';       // tipo de input
        $this->tag->value   = $this->label;   // rotulo do botao   
        $this->tag->style   = 'width:100px; '.
                              'float:right';
        $this->tag->class   = 'btn btn-sm btn-success';
        
              
        // se o campo Nao a editavel
        if (!parent::getEditable())
        {
            $this->tag->disabled = "1";
            $this->tag->class = 'tfield_disabled'; // classe CSS
        }
        
        // define a acao do botao
//        $this->tag->onclick = "document.{$this->formName}.action='{$url}';"; //.
//                              // "document.{$this->formName}.submit()";
        // exibe o botao
        $this->tag->show();
    }
}
?>