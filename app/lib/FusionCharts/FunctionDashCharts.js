
function updateChart01(factoryIndex) {

    var strURL = 'app.control/DashBeneficiarioGraficoMunicipio.class.php?regional_id=' + factoryIndex;
    // alert ("Esta é uma caixa de diálogo ALERT do JavaScript! "+ factoryIndex);
    $.ajax({
        type: 'POST',
        url: strURL,
        success: function (msg) {
            $('#ChartBenefMunicipio').show().html(msg);
        }
    });
}

function updateChart02(factoryIndex) {

    var strURL1 = 'app.control/DashBeneficiarioTipoMunicipio.class.php?municipio_id=' + factoryIndex;
    // alert ("Esta é uma caixa de diálogo ALERT do JavaScript! "+ factoryIndex);
    $.ajax({
        type: 'POST',
        url: strURL1,
        success: function (msg) {
            $('#ChartBenefTipoMunic').show().html(msg);
        }
    });
    var strURL2 = 'app.control/DashBeneficiarioSexoMunicipio.class.php?municipio_id=' + factoryIndex;
    // alert ("Esta é uma caixa de diálogo ALERT do JavaScript! "+ factoryIndex);
    $.ajax({
        type: 'POST',
        url: strURL2,
        success: function (msg) {
            $('#ChartBenefSexoMunic').show().html(msg);
        }
    });
    var strURL3 = 'app.control/DashBeneficiarioGraficoPosto.class.php?municipio_id=' + factoryIndex;
    // alert ("Esta é uma caixa de diálogo ALERT do JavaScript! "+ factoryIndex);
    $.ajax({
        type: 'POST',
        url: strURL3,
        success: function (msg) {
            $('#ChartBenefPorPosto').show().html(msg);
        }
    });

}

function updateChart03(factoryIndex) {

    var strURL = 'app.control/DashBeneficiarioLeitePostoEntrega.class.php?id=' + factoryIndex;

    // alert ("Esta é uma caixa de diálogo ALERT do JavaScript! "+ factoryIndex);
    $.ajax({
        type: 'POST',
        url: strURL,
        success: function (msg) {
            $('#ChartPostoEntrega').show().html(msg);
        }
    });
}
////////////////////////////////// Inicio DashboardProjetoSituacaoSIAP   //////////////////////////////////////////////
function updateChart01DashboardProjetosSituacaoSIAP(factoryIndex, factoryIndex2, ano) {
    // carrega grafico 01 classe DashboardProjetoSituacaoSIAP
    var strURL0 = 'app.control/DashboardEficienciaContratacaoProjetosSIAP.class.php?regional_id=' + factoryIndex + '&dash=' + factoryIndex2 + '&ano=' + ano + '&ano=' + ano;
    $.ajax({
        type: 'POST',
        url: strURL0,
        success: function (msg) {
            $('#DashProjSituacaoSIAP01').show().html(msg);
        }
    });

    // carrega grafico 02 classe DashboardProjetoSituacaoSIAP
    var strURL4 = 'app.control/DashboardProjetosTipoAtividadeRegionalSIAP.class.php?regional_id=' + factoryIndex + '&dash=' + factoryIndex2 + '&ano=' + ano;
    // alert ("Esta é uma caixa de diálogo ALERT do JavaScript! "+ factoryIndex);
    $.ajax({
        type: 'POST',
        url: strURL4,
        success: function (msg) {
            $('#DashProjTipoAtivRegionalSIAP01').show().html(msg);
        }
    });
    // carrega grafico 03 classe DashboardProjetoSituacaoSIAP
    var strURL5 = 'app.control/DashboardProjetosTipoAtividadeInvestRegionalSIAP.class.php?regional_id=' + factoryIndex + '&dash=' + factoryIndex2 + '&ano=' + ano;
    // alert ("Esta é uma caixa de diálogo ALERT do JavaScript! "+ factoryIndex);
    $.ajax({
        type: 'POST',
        url: strURL5,
        success: function (msg) {
            $('#DashProjTipoAtivInvestRegionalSIAP01').show().html(msg);
        }
    });
    // carrega grafico 04 classe DashboardProjetoSituacaoSIAP
    var strURL6 = 'app.control/DashboardProjetosTipoAtividadeCusteioRegionalSIAP.class.php?regional_id=' + factoryIndex + '&dash=' + factoryIndex2 + '&ano=' + ano;
    $.ajax({
        type: 'POST',
        url: strURL6,
        success: function (msg) {
            $('#DashProjTipoAtivCusteioRegionalSIAP01').show().html(msg);
        }
    });

    // carrega grafico 05 classe DashboardProjetoSituacaoSIAP
    var strURL1 = 'app.control/DashboardProjetosTipoProjetoRegionalSIAP.class.php?regional_id=' + factoryIndex + '&ano=' + ano;
    $.ajax({
        type: 'POST',
        url: strURL1,
        success: function (msg) {
            $('#DashProjTipoProjRegionalSIAP01').show().html(msg);
        }
    });
    // carrega grafico 06 classe DashboardProjetoSituacaoSIAP
    var strURL2 = 'app.control/DashboardProjetosEnquadramentoRegionalSIAP.class.php?regional_id=' + factoryIndex + '&ano=' + ano;
    // alert ("Esta é uma caixa de diálogo ALERT do JavaScript! "+ factoryIndex);
    $.ajax({
        type: 'POST',
        url: strURL2,
        success: function (msg) {
            $('#DashbProjEnquadramentoRegionalSIAP01').show().html(msg);
        }
    });
    // carrega grafico 07 classe DashboardProjetoSituacaoSIAP
    var strURL3 = 'app.control/DashboardProjetosLinhaCreditoRegionalSIAP.class.php?regional_id=' + factoryIndex + '&ano=' + ano;
    $.ajax({
        type: 'POST',
        url: strURL3,
        success: function (msg) {
            $('#DashProjLinhaCredRegionalSIAP').show().html(msg);
        }
    });
    // carrega grafico 08 classe DashboardProjetoSituacaoSIAP
    var strURL = 'app.control/DashboardProjetosSituacaoSIAPMunicipio.class.php?regional_id=' + factoryIndex + '&ano=' + ano;
    $.ajax({
        type: 'POST',
        url: strURL,
        success: function (msg) {
            $('#DashProjSituacaoSIAPMunicipio').show().html(msg);
        }
    });
}

function updateChart02DashboardProjetosSituacaoSIAP(factoryIndex, ano) {
    //chama grafico 09 classe DashboardProjetoSituacaoSIAP
    var strURL = 'app.control/DashboardProjetosSituacaoSIAPMunicipioSituacao.class.php?municipio_id=' + factoryIndex + '&ano=' + ano;
    // alert ("Esta é uma caixa de diálogo ALERT do JavaScript! "+ factoryIndex);
    $.ajax({
        type: 'POST',
        url: strURL,
        success: function (msg) {
            $('#DashProjSituacaoSIAPMunSituacao').show().html(msg);
        }
    });
    //chama grafico 10 classe DashboardProjetoSituacaoSIAP
    var strURL2 = 'app.control/DashboardProjetosSituacaoSIAPMunicipioContratados.class.php?municipio_id=' + factoryIndex + '&ano=' + ano;
    // alert ("Esta é uma caixa de diálogo ALERT do JavaScript! "+ ano);
    $.ajax({
        type: 'POST',
        url: strURL2,
        success: function (msg) {
            $('#DashProjSituacaoSIAPMunContratados').show().html(msg);
        }
    });
    //chama grafico 11 classe DashboardProjetoSituacaoSIAP
    var strURL3 = 'app.control/DashboardProjetosSituacaoSIAPMunicipioTecnico.class.php?municipio_id=' + factoryIndex + '&ano=' + ano;
    // alert ("Esta é uma caixa de diálogo ALERT do JavaScript! "+ factoryIndex);
    $.ajax({
        type: 'POST',
        url: strURL3,
        success: function (msg) {
            $('#DashProjSituacaoSIAPMunTecnico').show().html(msg);
        }
    });
}
//chama grafico 12 classe DashboardProjetoSituacaoSIAP
function updateChart06DashboardProjetosSituacaoSIAP(factoryIndex, factoryIndex2, ano) {

    var strURL6 = 'app.control/DashboardProjetosSituacaoSIAPMunicipioSituacaoTec.class.php?tecnico_id=' + factoryIndex + "&municipio_id=" + factoryIndex2 + '&ano=' + ano;
    // alert ("Esta é uma caixa de diálogo ALERT do JavaScript! "+ factoryIndex);
    $.ajax({
        type: 'POST',
        url: strURL6,
        success: function (msg) {
            $('#DashProjSituacaoSIAPMunSituacaoTec').show().html(msg);
        }
    });
}
////////////////////////////////// Fim  DashboardProjetoSituacaoSIAP   //////////////////////////////////////////////

////////////////////////////////// Inicio DashboardProjetoRendimentoSIAP   //////////////////////////////////////////////
function updateChart01DashboardProjetosRendimentoSIAP(factoryIndex, ano) {
//carrega grafico 04 DashboardRendimentoFinanceiroProjetosSIAP
    var strURL01 = 'app.control/DashboardProjetosTipoAtividadeRendRegionalSIAP.class.php?regional_id=' + factoryIndex + '&ano=' + ano;
    // alert ("Esta é uma caixa de diálogo ALERT do JavaScript! "+ factoryIndex);
    $.ajax({
        type: 'POST',
        url: strURL01,
        success: function (msg) {
            $('#ChartsProjTipoAtivRendRegional').show().html(msg);
        }
    });
    //carrega grafico 05 DashboardRendimentoFinanceiroProjetosSIAP
    var strURL02 = 'app.control/DashboardProjetosTipoAtividadeInvestRendRegionalSIAP.class.php?regional_id=' + factoryIndex + '&ano=' + ano;
    // alert ("Esta é uma caixa de diálogo ALERT do JavaScript! "+ factoryIndex);
    $.ajax({
        type: 'POST',
        url: strURL02,
        success: function (msg) {
            $('#ChartTipoAtivInvestRendRegional').show().html(msg);
        }
    });
    //carrega grafico 06
    var strURL03 = 'app.control/DashboardProjetosTipoAtividadeCusteioRegionalRendSIAP.class.php?regional_id=' + factoryIndex + '&ano=' + ano;
    // alert ("Esta é uma caixa de diálogo ALERT do JavaScript! "+ factoryIndex);
    $.ajax({
        type: 'POST',
        url: strURL03,
        success: function (msg) {
            $('#ChartTipoAtivCustRegionalRend').show().html(msg);
        }
    });
//carrega grafico 07
    var strURL1 = 'app.control/DashboardProjetosTipoProjetoRendRegionalSIAP.class.php?regional_id=' + factoryIndex + '&ano=' + ano;
    // alert ("Esta é uma caixa de diálogo ALERT do JavaScript! "+ factoryIndex);
    $.ajax({
        type: 'POST',
        url: strURL1,
        success: function (msg) {
            $('#ChartTipoProjetoRendRegionalSIAP').show().html(msg);
        }
    });
    //carrega grafico 08
    var strURL2 = 'app.control/DashboardProjetosEnquadramentoRendRegionalSIAP.class.php?regional_id=' + factoryIndex + '&ano=' + ano;
    // alert ("Esta é uma caixa de diálogo ALERT do JavaScript! "+ factoryIndex);
    $.ajax({
        type: 'POST',
        url: strURL2,
        success: function (msg) {
            $('#ChartProjEnqRendRegionalSIAP').show().html(msg);
        }
    });
    //carrega grafico 09
    var strURL3 = 'app.control/DashboardProjetosLinhaCreditoRendRegionalSIAP.class.php?regional_id=' + factoryIndex + '&ano=' + ano;
    // alert ("Esta é uma caixa de diálogo ALERT do JavaScript! "+ factoryIndex);
    $.ajax({
        type: 'POST',
        url: strURL3,
        success: function (msg) {
            $('#ChartProjLinhaCredRendRegionalSIAP').show().html(msg);
        }
    });
    //carrega grafico 10
    var strURL = 'app.control/DashboardProjetosRendSituacaoSIAPMunicipio.class.php?regional_id=' + factoryIndex + '&ano=' + ano;
    // alert ("Esta é uma caixa de diálogo ALERT do JavaScript! "+ factoryIndex);
    $.ajax({
        type: 'POST',
        url: strURL,
        success: function (msg) {
            $('#ChartProjRendSituacaoSIAPMunicipio').show().html(msg);
        }
    });
}

function updateChart02DashboardProjetosRendimentoSIAP(factoryIndex, ano) {
    //carrega grafico 11
    var strURL = 'app.control/DashboardProjetosRendSituacaoSIAPMunicipioContratados.class.php?municipio_id=' + factoryIndex + '&ano=' + ano;
    // alert ("Esta é uma caixa de diálogo ALERT do JavaScript! "+ factoryIndex);
    $.ajax({
        type: 'POST',
        url: strURL,
        success: function (msg) {
            $('#ChartProjRendSituacaoSIAPMunicContratados').show().html(msg);
        }
    });

//carrega grafico 12
    var strURL2 = 'app.control/DashboardProjetosRendSituacaoSIAPMunicipioTecnico.class.php?municipio_id=' + factoryIndex + '&ano=' + ano;
    // alert ("Esta é uma caixa de diálogo ALERT do JavaScript! "+ factoryIndex);
    $.ajax({
        type: 'POST',
        url: strURL2,
        success: function (msg) {
            $('#ChartProjRendSituacaoSIAPMunicTecnico').show().html(msg);
        }
    });
}

function updateChart01DashboardQuantitativoVeiculos(factoryIndex) {

    var strURL = 'app.control/DashboardQuantitativoTipoVeiculo.class.php?regional_id=' + factoryIndex;
    // alert ("Esta é uma caixa de diálogo ALERT do JavaScript! "+ factoryIndex);
    $.ajax({
        type: 'POST',
        url: strURL,
        success: function (msg) {
            $('#Chart02').show().html(msg);
        }
    });


    var strURL2 = 'app.control/DashboardQuantitativoVeiculosMunicipio.class.php?regional_id=' + factoryIndex;
    // alert ("Esta é uma caixa de diálogo ALERT do JavaScript! "+ factoryIndex);
    $.ajax({
        type: 'POST',
        url: strURL2,
        success: function (msg) {
            $('#Chart03').show().html(msg);
        }
    });

}

function updateChart01DashboardEngAgronomo(factoryIndex) {

    var strURL = 'app.control/DashboardServidoresPercentualEngenheirosAgronomoMun.class.php?regional_id=' + factoryIndex;
    // alert ("Esta é uma caixa de diálogo ALERT do JavaScript! "+ factoryIndex);
    $.ajax({
        type: 'POST',
        url: strURL,
        success: function (msg) {
            $('#Chart001').show().html(msg);
        }
    });

}

function updateChart01DashboardTecAgricola(factoryIndex) {

    var strURL = 'app.control/DashboardServidoresPercentualTecAgricolaMun.class.php?regional_id=' + factoryIndex;
    // alert ("Esta é uma caixa de diálogo ALERT do JavaScript! "+ factoryIndex);
    $.ajax({
        type: 'POST',
        url: strURL,
        success: function (msg) {
            $('#ChartSerPerTecAgriMun').show().html(msg);
        }
    });
}

function updateChart01DashboardPrevisaoAposentadoria(factoryIndex) {

    var strURL = 'index.php?class=DashboardPrevisaoAposentadoriaPorRegional&ano=' + factoryIndex;
    $.ajax({
        type: 'POST',
        url: strURL,
        success: function (msg) {
            $('#chart2').show().html(msg);
            alert("Esta é uma caixa de diálogo ALERT do JavaScript! " + factoryIndex);
        }
    });

    var strURL2 = 'index.php?class=DashboardPrevisaoAposentadoriaPorFormacao&ano=' + factoryIndex;
    // alert ("Esta é uma caixa de diálogo ALERT do JavaScript! "+ factoryIndex);
    $.ajax({
        type: 'POST',
        url: strURL2,
        success: function (msg) {
            $('#chart3').show().html(msg);
        }
    });

}


function updateChartDashboardAposentadoriaGraficoRegional(factoryIndex) {
    alert(factoryIndex);
    var strURL = 'app.control/DashAposentadoriaSubMunicipio.class.php?regional_id=' + factoryIndex;
    // alert ("Esta é uma caixa de diálogo ALERT do JavaScript! "+ factoryIndex);
    $.ajax({
        type: 'POST',
        url: strURL,
        success: function (msg) {
            $('#Chart02').show().html(msg);
        }
    });

}
