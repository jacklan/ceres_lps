<?php

Namespace Adianti\Widget\Menu;

//ini_set('display_errors', 1);
//ini_set('display_startup_erros', 1);
//error_reporting(E_ALL);


use Adianti\Widget\Menu\TMenuItem;
use Adianti\Widget\Base\TElement;
use SimpleXMLElement;
use Adianti\Database\TTransaction;
use Adianti\Database\TRepository;
use Adianti\Database\TCriteria;
use Adianti\Database\TFilter;

//use App\Model\Acesso_servidor\Acesso_servidorRecord;
//use App\Model\Acesso_servidorRecord;
//use App\Model\Acesso_Servidor\Acesso_servidorRecord;
//use App\Model\vw_usuario;
//use App\Model\vw_usuarioperfil;
use App\Model\Acesso_Servidor\Acesso_ServidorRecord;

/**
 * Menu Widget
 *
 * @version    2.0
 * @package    widget
 * @subpackage menu
 * @author     Pablo Dall'Oglio
 * @copyright  Copyright (c) 2006-2014 Adianti Solutions Ltd. (http://www.adianti.com.br)
 * @license    http://www.adianti.com.br/framework-license
 */
class TMenu extends TElement
{

    private $items;
    private $menu_class;
    private $item_class;
    private $menu_level;

    /**
     * Class Constructor
     * @param $xml SimpleXMLElement parsed from XML Menu
     */
    public function __construct($xml, $permission_callback = NULL, $menu_level = 1, $menu_class = 'dropdown-menu', $item_class = '')
    {
        parent::__construct('ul');
        $this->items = array();

        $this->{'class'} = $menu_class . " level-{$menu_level}";
        $this->menu_class = $menu_class;
        $this->menu_level = $menu_level;
        $this->item_class = $item_class;

        if ($xml instanceof SimpleXMLElement) {
            $this->parse($xml, $permission_callback);
        }
    }

    /**
     * Add a MenuItem
     * @param $menuitem A TMenuItem Object
     */
    public function addMenuItem(TMenuItem $menuitem)
    {
        $this->items[] = $menuitem;
    }

    /**
     * Return the menu items
     */
    public function getMenuItems()
    {
        return $this->items;
    }

    /**
     * Parse a XMLElement reading menu entries
     * @param $xml A SimpleXMLElement Object
     * @param $permission_callback check permission callback
     */
    public function parse($xml, $permission_callback = NULL)
    {
        $i = 0;
        foreach ($xml as $xmlElement) {
            $atts = $xmlElement->attributes();
            $label = (string)$atts['label'];
            $action = (string)$xmlElement->action;
            $icon = (string)$xmlElement->icon;
            $menu = NULL;
            $menuItem = new TMenuItem($label, $action, $icon);

            if ($xmlElement->menu) {
                $menu = new TMenu($xmlElement->menu->menuitem, $permission_callback, $this->menu_level + 1, $this->menu_class, $this->item_class);
                $menuItem->setMenu($menu);
            }

            // just child nodes have actions
            if ($action) {
                if (!empty($action) AND $permission_callback) {
                    // check permission
                    $parts = explode('#', $action);
                    $className = $parts[0];
                    if (call_user_func($permission_callback, $className)) {
                        $this->addMenuItem($menuItem);
                    }
                } else {
                    // menus without permission check
                    $this->addMenuItem($menuItem);
                }
            } // parent nodes are shown just when they have valid children (with permission)
            else if (isset($menu) AND count($menu->getMenuItems()) > 0) {
                $this->addMenuItem($menuItem);
            }

            $i++;
        }
    }

    static public function validaClasse($classe)
    {
        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');
        // instancia um repositorio da Classe
        $repository = new TRepository('Vw_usuariopaginagrupo');

        // cria um criterio de selecao
        $criteria = new TCriteria;
        //filtra pelo campo arquivo
        $criteria->add(new TFilter('arquivo', '=', $classe));
        // carrega os objetos de acordo com o criterio
        $usuarios = $repository->load($criteria);
        if ($usuarios) {
            // percorre os objetos retornados
            return TRUE;
        } else {
            return FALSE;
        }
    }

    static public function usuarioAtivo($usuario_id)
    {
//        // inicia transacao com o banco 'pg_ceres'
//        TTransaction::open('pg_ceres');
//        // instancia um repositorio da Classe
//        $acesso = new Acesso_servidorRecord($usuario_id);
//        $acesso->dataacesso = date('d/m/Y');
//        $acesso->store();
//        // finaliza a transacao
//        TTransaction::close();

        return TRUE;
    }

    static public function montaMenuTop($verificaModulo)
    {
        $menutop = '';
        $menutop .= " <ul class='right-icons' id='step3'>";
        if ($verificaModulo) {
            $menutop .= " <li>
                <a href='index.php?modulo=PONTO&class=MarcacaoList' class='lock' title='Bater o Ponto'> <i class='fa fa-clock-o'></i>  </a>
            </li>";
            $menutop .= " <li>
                            <a href='#' class='lock'> <i class='fa fa-wrench'></i>   </a>
                            <ul class='dropdown'>
                                <li><a href='index.php?class=TutorialList' ><i class='fa fa-video-camera'></i>Videoaulas</a></li>
                                <li><a href='index.php?modulo=SUPORTE&class=ChamadoList' title='Abrir Chamado'><i class='fa fa-desktop'></i>Abrir Chamado</a></li>
                            </ul>
            </li>
            ";
        }
        $menutop .= "  <li>
            <a href='#' class='lock'> <i class='fa fa-info'></i>  </a>
            <ul class='dropdown'>
                <li ><a href='index.php?class=Sobre' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sobre</a></li>
            </ul>
        </li>

        <li>
            <a href='#' class='lock' title='DER'> <i class='fa fa-book'></i>   </a>
            <ul class='dropdown'>
                <!-- <li ><a href='http://servicos.emater.rn.gov.br/wikiceres/doku.php' target='_blank' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Wiki</a></li> -->
                <li ><a href='index.php?modulo=DER&class=LoadPage&page=ProdutorDERList'  ><i class='fa fa-pinterest-square'></i>Produtor</a></li>
                <li ><a href='index.php?modulo=DER&class=LoadPage&page=SaidaVeiculoLocalList'  ><i class='fa fa-car'></i>Saída Veiculo</a></li>
                <li ><a href='index.php?modulo=DER&class=LoadPage&page=AtividadeDerList'  ><i class='fa fa-edit'></i>Atividade</a></li>
            </ul>
        </li>";
        if ($verificaModulo) {
            $menutop .= " <li>
                <a href='#' class='lock'> <i class='fa fa-comments'></i> <div class='notify' style='visibility: hidden'></div>  </a>
                <ul class='dropdown' id='listAviso' style='left: -175%'>

                </ul>";
        }
        $menutop .= "<li>
            <a href='index.php?class=MeuPerfilList' class='lock'> <i class='fa fa-user'></i>   </a>
            <ul class='dropdown'>
                <li ><a href='index.php?class=MeuPerfilList' >&nbsp;&nbsp;&nbsp;&nbsp;Meu perfil</a></li>
            </ul>
        </li>
        <li>
            <a href='index.php?class=LoginForm&method=onLogout&static=1'class='lock'> <i class='fa fa-power-off'></i>  </a>
            <ul class='dropdown'>
                <li ><a href='index.php?class=LoginForm&method=onLogout&static=1' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sair</a></li>
            </ul>
        </li>

    </ul>";
        return $menutop;
    }

    //static public function montaModulo($usuario_id) {
    static public function montaModulo($cpf, $empresa_id)
    {

        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        // instancia um repositorio da Classe
        $repository1 = new TRepository('vw_usuarioperfil');

        // cria um criterio de selecao
        $criteria1 = new TCriteria;
        //filtra pelo campo usuario_id
        $criteria1->add(new TFilter('cpf', '=', $cpf));
        $criteria1->add(new TFilter('empresa_id', '=', $empresa_id));

        //$criteria->add(new TFilter('usuario_id', '=', $usuario_id));
        $criteria1->add(new TFilter('versao', '=', 2));
        $criteria1->setProperty('order', 'modulo');

        // var_dump($criteria1->dump());
        // carrega os objetos de acordo com o criterio
        $usuariosModulo = $repository1->load($criteria1);
        // var_dump($usuariosModulo);
        $menu = "";
        //<p id="servidor" class='. $_SESSION['servidor_id'].' style="visibility: hidden;"></p>
        $menu .= "<option id='servidor' class=" . $_SESSION['servidor_id'] . ">SELECIONE O MODULO</option>";

        $servidor_id = 0;

        if ($usuariosModulo) {
            $_SESSION['MODULO_PONTO'] = false;
            // percorre os objetos retornados
            foreach ($usuariosModulo as $usuario) {

                // adiciona os dados do perfil do usuario no menu
                $menu .= "<option ";
                if ($_SESSION['modulo'] === $usuario->modulo) {
                    $menu .= ' selected ';
                }
                $menu .= "value='?modulo=" . $usuario->modulo . "' data-image=\"app/images/modulos/icon_" . $usuario->modulo . ".png\" data-title=" . $usuario->modulo . ">" . $usuario->modulo . "</option>";

                $servidor_id = $usuario->servidor_id;

                $idusuario = $usuario->usuario_id;
                $idservidor = $usuario->servidor_id;
                if ($usuario->modulo == "PONTO") {
                    $_SESSION['MODULO_PONTO'] = true;
                }
            }
        }

        // finaliza a transacao
        TTransaction::close();
        try {
            TTransaction::open('pg_ceres');
            // instancia um repositorio da Classe
            //$repository = new TRepository('Acesso_ServidorRecord');
            //$criteria = new TCriteria;
            //$criteria->add(new TFilter('usuario_id', '=', $idusuario));
            //$usuarioAcesso = $repository->load($criteria);
            $acesso = new \Acesso_servidorRecord();
            $acesso->id = $idusuario;

            if (file_exists('app/images/servidor/servidor_' . $idservidor . '.jpg')) {
                $acesso->temfoto = '1';
            }

            $acesso->servidor_id = $idservidor;
            $acesso->dataacesso = date('d/m/Y');
            $acesso->store();

            // finaliza a transacao
            TTransaction::close();
        } catch (Exception $e) {

        }
        return $menu;
    }

    /*
      static public function montaMenu($modulo, $usuario_id) {

      // inicia transacao com o banco 'pg_ceres'
      TTransaction::open('pg_ceres');

      // instancia um repositorio da Classe
      $repository = new TRepository('vw_usuariopagina');

      // cria um criterio de selecao
      $criteria = new TCriteria;
      //filtra pelo campo usuario_id
      $criteria->add(new TFilter('usuario_id', '=', $usuario_id));
      //filtra pelo campo modulo
      $criteria->add(new TFilter('modulo', '=', $modulo));
      //filtra pelo campo modulo
      //$criteria->add(new TFilter('versao', '=', 2));


      // carrega os objetos de acordo com o criterio
      $usuarios = $repository->load($criteria);
      $menu='';
      if ($usuarios) {
      $menu = '<ul id="nav">';
      // percorre os objetos retornados
      foreach ($usuarios as $usuario) {
      // adiciona os dados do perfil do usuario no menu
      $menu .= "<li><a href=\"#\" OnClick=\"document.location='?class=".$usuario->arquivo."'\">".$usuario->pagina."</a></li>";
      }
      $menu .= '</ul>';
      }
      // finaliza a transacao
      TTransaction::close();

      return $menu;

      }
     */

    static public function montaMenu($modulo, $usuario_id)
    {

        // inicia transacao com o banco 'pg_ceres'
        TTransaction::open('pg_ceres');

        $repositorygrupo = new TRepository('vw_grupomenuusuario');

        $criteria1 = new TCriteria;

        //filtra pelo campo usuario_id
        $criteria1->add(new TFilter('usuario_id', '=', $usuario_id));
        //filtra pelo campo modulo
        $criteria1->add(new TFilter('modulo', '=', $modulo));

        $criteria1->setProperty('order', 'grupo');

        $grupos = $repositorygrupo->load($criteria1);

        $menu = '';
        if ($grupos) {
            $id = '';
            $menu .= '<div id="test-list"> 



            <ul class="menu list">';
            foreach ($grupos as $grupo) {
                $id = $grupo->id;


                // adiciona os dados do perfil do usuario no menu
                $menu .= "<li class=\"parent green\" ><a href=\"index.html\"><span class=\"menu-icon\"><i class='" . $grupo->icone . "'></i></span><span class=\"menu-text\">" . $grupo->grupo . "</span></a>";

                // instancia um repositorio da Classe
                $repository = new TRepository('vw_usuariopaginagrupo_new');

                // cria um criterio de selecao
                $criteria = new TCriteria;
                //filtra pelo campo usuario_id
                $criteria->add(new TFilter('usuario_id', '=', $usuario_id));
                //filtra pelo campo modulo
                $criteria->add(new TFilter('modulo', '=', $modulo));
                //filtra pelo campo
                $criteria->setProperty('order', 'grupo');
                $criteria->setProperty('order', 'pagina');

                // carrega os objetos de acordo com o criterio
                $usuarios = $repository->load($criteria);
                if ($usuarios) {

                    // percorre os objetos retornados
                    $menu .= '<ul class="child">';
                    foreach ($usuarios as $usuario) {
                        if ($usuario->grupo_id == $id) {
                            // adiciona os dados do perfil do usuario no menu
                            if ($usuario->arquivo == 'AtualizaForm') {
                                $menu .= "<li><a href=\"#\" OnClick=\"document.location='?class=" . $usuario->arquivo . "&method=onEdit&key=" . $_SESSION['servidor_id'] . "&fk=" . $_SESSION['servidor_id'] . "'\">" . $usuario->pagina . "</a></li>";
                            } else {
                                if ($usuario->arquivoleitura == 'SIM' && $usuario->novajanela == 'SIM') {
                                    // $menu .= "<li><a target=\"_blank\" href=\"http://servicos.emater.rn.gov.br/ceres/" . $usuario->arquivo . "\">" . $usuario->pagina . "</a></li>";
                                    $menu .= "<li><a target=\"_blank\" href='http://" . $_SERVER ['HTTP_HOST'] . "/ceres/" . $usuario->arquivo . "'\">" . $usuario->pagina . "</a></li>";
                                } else {
                                    if ($usuario->novajanela == 'NAO') {
                                        $menu .= "<li><a class=\"user-title\" href=\"#\" OnClick=\"document.location='?class=" . $usuario->arquivo . "'\">" . $usuario->pagina . "</a></li>";
                                    } else {
                                        $menu .= "<li><a class=\"user-title\" href=\"#\" OnClick=\"javascript:window.open('?class=" . $usuario->arquivo . "');\">" . $usuario->pagina . "</a></li>";
                                    }
                                }
                            }
                        }
                    }

                    $menu .= '</ul>';
                }

                $menu .= "</li>";
            }

            $menu .= '</ul>  </div>';
        }
        // finaliza a transacao
        TTransaction::close();

        return $menu;
    }

    static public function montaError()
    {

        return 'teste';
    }

    static public function montaMural()
    {
        $mural = "";

        if (filter_input(INPUT_GET, 'modulo') == 'DIRECAO') {
            TTransaction::open('pg_ceres');

            // instancia um repositorio para aniversariantes
            $repository = new TRepository('vw_dash_monta_listagem_dashboardRecord');

            // cria um criterio de selecao, ordenado pelo id
            $criteria = new TCriteria;

            //filtra pelo campo avaliador_id
            $criteria->add(new TFilter('login', '=', $_SESSION['usuario']));

            $results = $repository->load($criteria);

            if ($results) {
                $contador = 0;
                $mural .= "<br><h2 align='center'>DASHBOARDS</h2><br><table cellpadding='0' cellspacing='0' border='0' class='display' id='example' style='margin-left:10px;'>";
                // percorre os objetos retornados
                foreach ($results as $result) {
                    if (($contador == 0) || (($contador % 4) == 0)) {
                        $mural .= "<tr id='tritensdashboard'>";
                    }
                    // $mural .= "";
                    $mural .= "
                        <div class='col-xs-12 col-sm-12 col-md-4 col-lg-4'>
                        <div class='box social-stats' style='align: center'>
                            <div class='title-bar'>
                                <i class='fa fa-users'></i>{$result->modulo}
                                <div class='close-box'>
                                    <a href='#'><i class='fa fa-times-circle-o'></i></a>
                                </div>
                            </div>
                            <ul>
                                <li>
                                <a style='text-decoration:none;color:#000; font-weight:bold; font-size:13px; width: 170px; height: 165px;' href='index.php?class={$result->arquivo}&dash={$result->grafico_id}'>
                                        <div id='dash'>
                                          <center>
                                            <img src='app/images/dash_tipo_{$result->tipo}.png' width='120' height='110' title='{$result->nome}' alt='{$result->nome}'/>
                                            <img src='app/images/icon_{$result->modulo}.png' width='50' height='50'/>
                                            <p>{$result->nome}</p>
                                          </center>
                                        </div>
                                   </a>
                                </li>
                            </ul>
                        </div>
                        </div>";


                    $contador++;
                }
                $mural .= '</table>';
            }

            /*

              $mural .= "<br><br><table cellpadding='0' cellspacing='0' border='0' class='display' id='example' style='margin-left:10px;'>
              <tr id='tritensdashboard'>
              <td id='tditemdashboard' align='center' valign='middle'>
              <a href='index.php?class=DashBeneficiarioGraficoList'><img src='app/images/bar_chart.png' width='250' height='200' alt='dashboard_sales'/>
              <p>Dashboard Teste 01</p></a>
              </td>
              <td id='tditemdashboard' align='center' valign='middle'>
              <img onclick='alert('Dashboard 02')' src='app/images/bar_chart.png' width='250' height='200' alt='dashboard_sales'/>
              <p>Dashboard Teste 02</p>
              </td>
              <td id='tditemdashboard' align='center' valign='middle'>
              <img onclick='alert('Dashboard 03')' src='app/images/bar_chart.png' width='250' height='200' alt='dashboard_3'/>
              <p>Dashboard Teste 03</p>
              </td>
              </tr>
              <tr id='tritensdashboard'>
              <td id='tditemdashboard' align='center' valign='middle'>
              <img onclick='alert('Dashboard 04')' src='app/images/bar_chart.png' width='250' height='200' alt='dashboard_sales'/>
              <p>Dashboard Teste 04</p>
              </td>
              <td id='tditemdashboard' align='center' valign='middle'>
              <img onclick='alert('Dashboard 05')' src='app/images/bar_chart.png' width='250' height='200' alt='dashboard_sales'/>
              <p>Dashboard Teste 05</p>
              </td>
              <td id='tditemdashboard' align='center' valign='middle'>
              <img onclick='alert('Dashboard 06')' src='app/images/bar_chart.png' width='250' height='200' alt='dashboard_3'/>
              <p>Dashboard Teste 06</p>
              </td>
              </tr>
              </table>";

              /* $modulo = file_get_contents('dashboard.html');
              echo str_replace('#dashboards#', null, $modulo); */
        } else if (filter_input(INPUT_GET, 'modulo') == 'TRANSPORTE_CIDADAO') {

            $mural = "<center> <img src='/ceres/app/images/transporte_cidadao_background.png' alt='Foto de Exibição' /><br /><center/>";

        } else if ($_SESSION["tipousuario"] == 'LATICINIO') {

            $mural = '<br><br><table cellpadding="0" cellspacing="0" border="0" class="display" id="example">';

            ########### noticia com alerta Laticinio  ###########
            if (!filter_input(INPUT_GET, 'modulo') && $_SESSION['modulo'] == 'EMATER') {
                // $mural .= TMenu::aviso();
            }
            ########### noticia Laticinio ###########
            $mural .= "<thead>";
            $mural .= "<center><b><h3>AVISOS</h3></b></center>";
            $mural .= '<tr><th>Data</th><th>Aviso</th><th>Link</th></tr>';
            $mural .= "</thead>";
            $mural .= "<tr class=\"gradeA\"><td><font color='ff0000'>08/10/10</font></td><td>Video da NF</td><td><font color='ff0000'><a href='video/nflaticinio/'>NF Laticinio</a></font></td></tr>";
            $mural .= "<tr class=\"gradeA\"><td><font color='ff0000'>23/11/10</font></td><td><font color='ff0000'><b>SISTEMA LIBERADO PARA ALIMENTAÇÃO DAS NOTAS</b></font></td><td><font color='ff0000'><a href=''></a></font></td></tr>";
            $mural .= '</table><br><br>';
            $mural .= "<b>Telefones Suporte:</b>";
            $mural .= "<br><b>LEITE: 3232-1126/1127</br>GIN: 3232-2198</b>";
            $mural .= "<br><b>EMAIL: novoprogramadoleite@rn.gov.br</b>";
        } else if ($_SESSION["tipousuario"] == 'COLABORADORLEITE') {
            $mural = "<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
                        <!-- Boxed Start -->
                        <div class='boxed'>
                          <div class='title-bar' style=\"background-color: #f86868 !important;\">
                            <h4>AVISOS</h4>
                            <ul class='actions'>
                              <li><a class='close-box' href='#'><i class='fa fa-chevron-up'></i></a></li>
                              <li><a class='remove-box' href='#'><i class='fa fa-times-circle-o'></i></a></li>
                            </ul>
                          </div>

                          <div class='inner'>
                            <center><b>Telefones Suporte:</b>
                              <b>LEITE: 3232-1813/5633</br> GIN: 3232-1162</b><br>
                              <b>EMAIL: <a href='mailto:plp.sethas@gmail.com'> plp.sethas@gmail.com</a></b>
                            </center>
                          </div>
                        </div>
                        <!-- Boxed End -->
                      </div>
                      <!-- box dois logo programa do leite --->
                        <div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
                                    <!-- Boxed Start -->
                                    <div class='boxed'>
                                      <div class='title-bar'>
                                        <h4>Colaborador Leite</h4>
                                        <ul class='actions'>
                                          <li><a class='close-box' href='#'><i class='fa fa-chevron-up'></i></a></li>
                                          <li><a class='remove-box' href='#'><i class='fa fa-times-circle-o'></i></a></li>
                                        </ul>
                                      </div>
                                      <div class='inner'>
                                        <center>
                                          <div class=\"user-avatar\"><img src=\"app/images/nova_logo_programa_leite_rn.jpg\" style=\"max-width: 35%;\" alt=\"Logo Programa do Leite RN\"></div>
                                        </center>
                                      </div>
                                    </div>
                                    <!-- Boxed End -->
                                  </div>          
            ";
        } else {

            TTransaction::open('pg_ceres');

            // instancia um repositorio para DASHBOARDS
            $repository = new TRepository('vw_dash_monta_listagem_dashboardRecord');

            // cria um criterio de selecao, ordenado pelo id
            $criteria = new TCriteria;

            //filtra pelo campo USUARIO
            $criteria->add(new TFilter('login', '=', $_SESSION['usuario']));

            //if (isset($_SESSION['modulo'])) {
            //$criteria->add(new TFilter('modulo', '=', $_SESSION['modulo']));
            $criteria->add(new TFilter('modulo', '=', filter_input(INPUT_GET, 'modulo')));

            $results = $repository->load($criteria);

            //}
            if ($results) {

                $contador = 0;
                $mural .= "<br><h2 align='center'>DASHBOARDS</h2><br><table cellpadding='0' cellspacing='0' border='0' class='display' id='example' style='margin-left:10px;'>";
                // percorre os objetos retornados
                foreach ($results as $result) {
                    if (($contador == 0) || (($contador % 4) == 0)) {
                        $mural .= "<tr id='tritensdashboard'>";
                    }
                    $mural .= "
                         <div class='col-xs-12 col-sm-12 col-md-4 col-lg-4'>
                        <div class='boxed no-padding'>
                        <!-- Title Bart Start -->
                        <div class='title-bar white'>
                            <h4>{$result->modulo}</h4>
                                <ul class='actions'>
                                    <li><a href='#' class='close-box'><i class='fa fa-chevron-up'></i></a></li>
                                    <li><a href='#' class='remove-box'><i class='fa fa-times-circle-o'></i></a></li>
				</ul>
			</div>
                        <!-- Title Bart End -->
									
                        <div class='inner'>
                            <!-- Google Maps Example End -->
                            <div class='dashboard-container'>
                            <ul>
                                <li>
                                <a style='text-decoration:none;color:#000; font-weight:bold; font-size:13px; width: 170px; height: 165px;' href='index.php?class={$result->arquivo}&dash={$result->grafico_id}'>
                                        <div id='dash'>
                                           <center>
                                            <img src='app/images/dash_tipo_{$result->tipo}.png' width='120' height='110' title='{$result->nome}' alt='{$result->nome}'/>
                                            <img src='app/images/icon_{$result->modulo}.png' width='50' height='50'/>
                                            <p>{$result->nome}</p>
                                           </center>    
                                        </div>
                                   </a>
                                </li>
                            </ul>
                            </div>
                            <!-- Google Maps Example End -->
                        </div>
                    </div></div>";
                    $contador++;
                }
                $mural .= '</table>';
            } else {

                TTransaction::open('pg_ceres');

                ################# inicio vw_aniversariantes_do_mesRecord #################
                // instancia um repositorio para aniversariantes
                $repository = new TRepository('vw_aniversariantes_do_mesRecord');
                // $repository = new TRepository('vw_servidor_por_cidades');
                // cria um criterio de selecao, ordenado pelo id
                $criteria = new TCriteria;

                $criteria->setProperty('order', 'nome');
                $criteria->add(new TFilter('empresa_id', '=', $_SESSION['empresa_id']));
                //$criteria->setProperty('order', 'qtd');
                // carrega os objetos de acordo com o criterio
                $aniversariantes = $repository->load($criteria);
                /*
                //CHAT AVISO
                $repositoryAviso = new TRepository('Vw_chat_avisoRecord');
                $criteriaAviso = new TCriteria();
                $criteriaAviso->add(new TFilter('servidor_id_recipient', '=', $_SESSION['servidor_id']));

                $avisosChat = $repositoryAviso->load($criteriaAviso);

                $arrayChat = array();

                if ($avisosChat) {

                    foreach ($avisosChat as $avisoC) {

                        $arrayChat[$avisoC->servidor_id] = '-true';
                    }
                }

//
//CHAT AVISO
                //$repository3 = new TRepository('vw_ultimos_acessosRecord');
                $repository3 = new TRepository('vw_acesso_ServidorRecord');

                $criteria3 = new TCriteria();
                $criteria3->add(new TFilter('online', '=', '1'));
                $criteria3->add(new TFilter('usermain', '=', 'PRINCIPAL'));
                $criteria3->setProperty('order', 'online DESC, nome, dataacesso, horaacesso   ');
                //$online_info = $repository3->load($criteria3);

                $online_info = $repository3->load($criteria3);

                $online = '

				<!-- INICIO ULTIMOS ACESSOS -->

				<div style="height:300px; overflow: auto" >
							<ul class="list">';

                $onlineaviso = '';
                $onlinesem = '';
                if ($online_info) {
                    foreach ($online_info as $onlines) {
                        //verificar se o usuario esta logado nao abrir o chat para ele mesmo
                        if ($_SESSION['servidor_id'] != $onlines->servidor_id) {

                            if ($onlines->online == 1) {
                                $statuschat = 'online';
                            } else {
                                $statuschat = 'offline';
                            }

                            if (file_exists('app/images/servidor/servidor_' . $onlines->servidor_id . '.jpg')) {
                                $fotoservidor = $onlines->servidor_id;
                            } else {
                                $fotoservidor = 'avatar';
                            }

                            if ($arrayChat[$onlines->servidor_id]) {

                                $onlineaviso .= '<li>
                            <div>              
                                         <div id=' . $onlines->servidor_id . ' class="notify-chat' . $arrayChat[$onlines->servidor_id] . '" > <i class="fa fa-envelope-o"></i> </div>
                                        <div class="user-avatar"><img width="23" src="app/images/servidor/servidor_' . $fotoservidor . '.jpg" /> </div>
                                        <span class="username" OnClick="re();"><a  href=/cereschat/index.php?username=' . $_SESSION['servidor_id'] . '&recipient=' . $onlines->servidor_id . ' target=_blank ><span class="user-title"> ' . substr($onlines->nomecurto, 0, 22) . '.</span></a></span>
                                        <span class="status ' . $statuschat . '">&nbsp;</span>

                            </div>
                                       </li>';
                            } else {

                                $onlinesem .= '<li>
                            <div>              
                                         <div id=' . $onlines->servidor_id . ' class="notify-chat' . $arrayChat[$onlines->servidor_id] . '" > <i class="fa fa-envelope-o"></i> </div>
                                        <div class="user-avatar"><img width="23" src="app/images/servidor/servidor_' . $fotoservidor . '.jpg" /> </div>
                                        <span class="username" OnClick="re();"><a  href=/cereschat/index.php?username=' . $_SESSION['servidor_id'] . '&recipient=' . $onlines->servidor_id . ' target=_blank ><span class="user-title"> ' . substr($onlines->nomecurto, 0, 22) . '.</span></a></span>
                                        <span class="status ' . $statuschat . '">&nbsp;</span>

                            </div>
                                       </li>';
                            }
                        }
                    }
                }
                $online .= $onlineaviso;
                $online .= $onlinesem;
                $online .= '</ul>
							</div>
				<!-- FIM ULTIMOS ACESSOS -->';
                */
                $mural = '';

                //if ($aniversariantes) {

                $mural = '<div class="row">';
                $mural .= '<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">';
                $mural .= '<div class="row" id="step4" >';

                ########### noticia com alerta ao entrar no sistema ###########
                if (!filter_input(INPUT_GET, 'modulo') && $_SESSION['modulo'] == 'EMATER') {
                    // $mural .= TMenu::aviso();
                }
                /*
                ########### noticia ###########
                $mural .= '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">';
                $mural .= '
							<!-- Google Maps Start -->
							<div class="boxed no-padding">

									<!-- Title Bart Start -->
									<div class="title-bar white">
									  <h4>Previsão do Tempo</h4>
									  <ul class="actions">
										<li><a href="#" class="close-box"><i class="fa fa-chevron-up"></i></a></li>
										<li><a href="#" class="remove-box"><i class="fa fa-times-circle-o"></i></a></li>
									  </ul>
									</div>
									<!-- Title Bart End -->


								<div class="inner" >
							<!-- Widget Previs&atilde;o de Tempo e Ondas CPTEC/INPE -->
                                                        <iframe allowtransparency="true" marginwidth="0" marginheight="0" hspace="0" vspace="0" frameborder="0" scrolling="no"
                                                        src="http://www.cptec.inpe.br/widget/widget.php?p=' . $_SESSION["cptec_id"] . '&w=h&c=748ccc&f=ffffff" height="200px" width="430px">
                                                        </iframe>
                                                        <!-- <noscript>Previs&atilde;o de <a href="http://www.cptec.inpe.br/cidades/tempo/' . ""   $_SESSION["cptec_id"]  . '">' . '' $_SESSION["nomemunicipio"]  . '</a> oferecido por <a href="http://www.cptec.inpe.br">CPTEC/INPE</a></noscript>
                                                        <img style="margin-top:-200px; margin-left:-200px; " src="http://www7.cptec.inpe.br/seloweb/CreateImgOndasOneDay24h?cidade=' . ''  $_SESSION["cptec_id"]  . '&op=1&lang=pt" NAME="theimg" width="190" height="190" border="0" usemap="#Map" /> -->
                                                        <!-- Widget Previs&atilde;o de Tempo CPTEC/INPE -->
									<!-- Google Maps Example End -->
									<!--<div class="google-maps-container"><div id="map-canvas"></div></div> -->
									<!-- Google Maps Example End -->

								</div>
							</div>';
                        */
                $mural .= '</div>';

                $mural .= '<!-- Aniversariantes Start -->
							<div class="box social-stats">
								
								<div class="title-bar">
									<i class="fa fa-users"></i>Aniversariantes do Dia 
									  <div class="close-box">
										<a href="#"><i class="fa fa-times-circle-o"></i></a>
									  </div><div style="float: right; margin-right: 15px">' . date("d/m/Y") . '</div>
								</div>';
                $mural .= '<ul>';

                foreach ($aniversariantes as $aniversariante) {
                    if (date("d") == $aniversariante->dia) {
                        $mural .= '<li><i class="fa fa-arrow-right"></i>' . $aniversariante->nome . '<span>' . $aniversariante->cidade . '</span></li>';
                    }
                }
                $mural .= '</ul>';

                $mural .= '</div>
							<!-- Aniversariantes End -->';
                $mural .= '<!-- Daily Sales Start -->
                <div class="box tasks">
               <!-- Title Bar Start -->
                  <div class="title-bar">
                    <i class="fa fa-comments"></i>Governo do RN
                      <div class="close-box">
                        <a href="#"><i class="fa fa-times-circle-o"></i></a>
                      </div>
                  </div>
                  <!-- Title Bar End -->
                 ';
                try {
                    $mural .= "<ul>";
                    $feed = file_get_contents('https://news.google.com.br/news?pz=1&cf=all&ned=pt-BR_br&hl=pt-BR&output=rss&q=%22governo+rn%22+OR+%22sistema+ceres+rn&tbs=qdr:y');
                    $rss = new SimpleXmlElement($feed);
                    foreach ($rss->channel->item as $entrada) {
                        $mural .= " <li><a target=\"_blank\" href=\"$entrada->link\" title=\"$entrada->title\"><font color='green'>$entrada->title</font></a><span>$entrada->pubDate</span></li>";
                    }
                } catch (Exception $ex) {

                }

                $mural .= "</ul>";

                $mural .= '
                  <!-- Sales List End -->

              </div>
              <!-- Daily Sales End -->
';

                $mural .= '</div>';

                $mural .= '</div>';

                $mural .= '</div>';

                $mural .= '<!-- Right Sidebar Start -->
				<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
				<div class="row">

				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				
					<!-- Open Tasks Start -->
					<div class="box projects">

					<!-- Title Bar Start -->
					<div class="title-bar">
						<i class="fa fa-twitter"></i>@GovernodoRN
						<div class="close-box">
                        <a href="#"><i class="fa fa-times-circle-o"></i></a>
                      </div>
					</div>
					<!-- Title Bar End -->

					<!-- Stats List Start -->';


                $mural .= '
					<a target="_blank" class="twitter-timeline" href="https://twitter.com/governodorn" data-widget-id="463667206736183296">Tweets de @GovernodoRN <img src="app/images/loading.gif"/></a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?\'http\':\'https\';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
					';
                /*
                $mural .= '
					<!-- Stats List End -->

					</div>
					<!-- Open Tasks End -->
				</div>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6"></div>


				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                <!-- Whos online Start -->
                <div id="test-list" class="box whos-online">

					<!-- Title Bar Start -->
					<div class="title-bar" style="border: 0px; padding: -20px;"><i class="fa fa-user"></i>Últimos Acessos (' . date("d/m/Y") . ') 
									  <div class="close-box">
										<a href="#"><i class="fa fa-times-circle-o"></i></a>
									  </div>



					</div>

					<div class="divBuscar" style="margin-bottom: 5px;">
                               
		                                <input type="text" id="txtBusca" class="fuzzy-search" placeholder="Buscar usuario..."/>
		                                <img src="http://ceres.rn.gov.br/chatbox/images/search.png" id="btnBusca" alt="Buscar"/>
		                               
		            </div>
					<!-- Title Bar End -->
                                                   

                 ';

                // $mural .= $online;
                */
                $mural .= '</div>';
                $mural .= '</div>';
                $mural .= '</div>';
                $mural .= '</div>';
                $mural .= '</div>';
                //}
                // finaliza a transacao
                TTransaction::close();
            }
        }

        $negado = '<div class="error-404 text-center">
            <i class="fa fa-frown-o"></i>
            <h1>Whooops!</h1>
            <h4>Você não tem permissão para acessar esta página</h4>
            
            <p>para continuar <a href="index.php">clique aqui</a></p>
          </div>';


        if (isset($_REQUEST['acesso']) == 'negado') {
            return $negado;
        } else {
            return $mural;
        }
    }

    /**
     * @author   Jackson Mieres
     *
     * return string retorna um alert com o aviso
     */
    static function aviso()
    {

        $str = '';
        $str .= '
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js" integrity="sha512-K1qjQ+NcF2TYO/eI3M6v8EiNYZfA95pQumfvcVrTHtwQVDG+aHRqLi/ETn2uB+1JqwYqVG3LIvdm9lj6imS/pQ==" crossorigin="anonymous"></script>

<script src="https://github.com/makeusabrew/bootbox/releases/download/v4.4.0/bootbox.min.js"></script>

                <script>
                bootbox.alert("<h3>AVISO!</h3>"+
                            "<p style=\'padding: 10px; text-align: justify\'>O computador do Sistema Ceres sofreu algumas atualizações entre a sexta, dia 06/04/18 das 14h45 até o sábado 07/04/18 às 14h. Estas atualizações não obtiveram sucesso, e ainda comprometeram os dados inseridos durante este período. Então, <b style=\'color: red;\'>PEDIMOS A TODOS QUE FIZERAM USO DO SISTEMA NESTE PERÍODO, QUE REFAÇAM, POIS OS DADOS FORAM PERDIDOS</b>.  Sabemos que o transtorno causado não tem preço, por isso pedimos humildemente desculpas e agradecemos a compreensão. Neste momento o sistema apresenta boa estabilidade. Avisaremos com antecedência qual será o período da próxima atualização. <br><br>Atenciosamente,<br>Equipe do sistema Ceres. </p>");
                </script>';

        return $str;
    }

    static public function montaValidacao()
    {

        TTransaction::open('pg_ceres');

        // instancia um repositorio para aniversariantes
        $repository = new TRepository('vw_validacaoRecord');

        // cria um criterio de selecao, ordenado pelo id
        $criteria = new TCriteria;
        $criteria->setProperty('order', 'nome');
        // carrega os objetos de acordo com o criterio
        $results = $repository->load($criteria);

        $validar = '';

        if ($results) {

            $validar = '<table id="validacao">';
            $validar .= '<center><b><h3>CAPACITACOES PARA VALIDAR</h3></b></center>';
            $validar .= '<tr><th>Nome Servidor</th><th>Capacitacao</th1><th>Data de Conclusao</th><th>Situacao</th></tr>';
            // percorre os objetos retornados
            foreach ($results as $result) {

                $validar .= "<tr><td>" . $result->nome . '</td><td>' . $result->capacitacao . '</td><td>' . $result->datafim . '</td><td>' . $result->situacao . "</td></tr>";
            }
            $validar .= '</table>';
        }

        $validar = '';

        // finaliza a transacao
        TTransaction::close();

        return $validar;
    }

    static public function montaAvaliacao()
    {

        TTransaction::open('pg_ceres');

        // instancia um repositorio para aniversariantes
        $repository = new TRepository('vw_avaliacao_servidoresRecord');

        // cria um criterio de selecao, ordenado pelo id
        $criteria = new TCriteria;
        //filtra pelo campo avaliador_id
        $criteria->add(new TFilter('avaliador_id', '=', $_SESSION['servidor_id']));
        // carrega os objetos de acordo com o criterio
        $results = $repository->load($criteria);

        $avaliacao = '';

        if ($results) {

            $avaliacao = '<table cellpadding="0" cellspacing="0" border="0" class="display" id="example2">';
            $avaliacao .= "<thead>";
            $avaliacao .= '<center><b><h3>AVALIAÇÕES PENDENTES</h3></b></center>';
            $avaliacao .= '<tr><th>Avaliação</th><th>Servidor Avaliado</th><th>Lotação</th><th>Data de Conclusão da Avaliação</th><th>Responder</th></tr>';
            $avaliacao .= "</thead>";
            // percorre os objetos retornados
            foreach ($results as $result) {


                $avaliacao .= "<tr class=\"gradeC\"><td>" . $result->nomeavaliacao . '</td><td>' . $result->servidoravaliado . '</td><td>' . $result->lotacaoavaliado . '</td><td>' . $result->datafim . "</td><td><font color='ff0000'><a href='index.php?class=QuestionarioAvaliacaoServidorForm&avaliacao_id=" . $result->avaliacao_id . "&avaliacaoservidor_id=" . $result->avaliacaoservidor_id . "&ordem=1'>Responder Avaliação</a></font></td></tr>";
            }
            $avaliacao .= '</table>';
        }

        //$avaliacao = '';
        // finaliza a transacao
        TTransaction::close();

        return $avaliacao;
    }

    /**
     * Shows the widget at the screen
     */
    public function show()
    {
        if ($this->items) {
            foreach ($this->items as $item) {
                parent::add($item);
            }
        }
        parent::show();
    }

}
