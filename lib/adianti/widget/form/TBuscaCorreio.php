<?php
/**
 * TBuscaCorreio  
 *
 * @author     Ademilson Nunes
 * @version    1.0
 * Copyright (c) 2013-2014 Ademilson Nunes
 */
Namespace Adianti\Widget\Form;

//chdir('../../../');

//require_once 'init.php';


use Adianti\Registry\TSession;
use Adianti\Widget\Form\AdiantiWidgetInterface;
use Adianti\Widget\Base\TElement;

class TBuscaCorreio extends TElement implements AdiantiWidgetInterface
{      
    private $cookieFile;
    
    /**
     * Class constructor
     */
    public function __construct() 
    {                    
        new TSession;
        TSession::setValue('id', session_id());
        $cookie = TSession::getValue('id');
        
        $this->cookieFile = 'app/output/'.$cookie;
        if(!file_exists($this->cookieFile))
	{
		$file = fopen($this->cookieFile, 'w');
		fclose($file);
	}	
	$ch = curl_init('http://www.buscacep.correios.com.br/servicos/dnec/menuAction.do?Metodo=menuCep');
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_COOKIEJAR, $this->cookieFile);
       
    }
    
    public function setName($name){
    	
    }
    public function getName(){
    	
    }
    public function setValue($value){
    	
    }
    public function getValue(){
    	
    }
    public function show(){
    	
    }
    /**
     * getData()
     * @param string $cep 
     * @return array Data CEP values
     */
    public function getData($cep)
    {   
        // data post
	$post = array
	(
                'CEP' => "{$cep}",
		'Metodo' => 'listaLogradouro',
                'TipoConsulta' => 'cep',
                'StartRow' => 1,
                'EndRow'  =>10		            
	
	);
	
	$data = http_build_query($post, NULL, '&');	
	$cookie = array('flag' => 1);	
	$ch = curl_init('http://www.buscacep.correios.com.br/servicos/dnec/consultaLogradouroAction.do');
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	curl_setopt($ch, CURLOPT_COOKIEFILE, $this->cookieFile);
	curl_setopt($ch, CURLOPT_COOKIEJAR,  $this->cookieFile);
	curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:8.0) Gecko/20100101 Firefox/8.0');
	curl_setopt($ch, CURLOPT_COOKIE, http_build_query($cookie, NULL, '&'));
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
	curl_setopt($ch, CURLOPT_MAXREDIRS, 3);
	curl_setopt($ch, CURLOPT_REFERER, 'http://www.buscacep.correios.com.br/servicos/dnec/menuAction.do?Metodo=menuCep');
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$html = curl_exec($ch);
	curl_close($ch);
        
        $dom = new DomDocument();
	$dom->loadHTML($html);
	$nodes = $dom->getElementsByTagName('td');
	//$length = $nodes->length;
	$campos = array();
        for($i = 6; $i <= 10; $i++)
	{
		$node_value = trim($nodes->item($i)->nodeValue);		
                $campos[$i] = $node_value;               
                
        }
        
        $retorno = array('Logradouro' =>$campos[6],
                         'Bairro'     =>$campos[7],
                         'Cidade'     =>$campos[8],
                         'UF'         =>$campos[9]);
       
        return $retorno;    
        
    }   
    
}